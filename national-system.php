<?php

// Set the page title  -- GENERAL TEMPLATE 2

$page_title = 'A National System';

// Set the page keywords

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description

$page_description = 'The National Wild and Scenic Rivers System.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'general';

// Includes the meta data that is common to all pages

include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="open-content">

<p>"It is hereby declared to be the policy of the United States that certain selected rivers of the Nation which, with their immediate environments, possess outstandingly remarkable scenic, recreational, geologic, fish and wildlife, historic, cultural or other similar values, shall be preserved in free-flowing condition, and that they and their immediate environments shall be protected for the benefit and enjoyment of present and future generations.  The Congress declares that the established national policy of dams and other construction at appropriate sections of the rivers of the United States needs to be complemented by a policy that would preserve other selected rivers or sections thereof in their free-flowing condition to protect the water quality of such rivers and to fulfill other vital national conservation purposes." (Wild &amp; Scenic Rivers Act, October 2, 1968)</p>

<hr style="float: center" width="50%">

<figure style="margin: 0px 0px 0px 0px"><img src="images/rogue.jpg" align="center" alt="Rogue River, One of the Original Eight Designated Rivers" height="403" vspace="10" title="Rogue River, One of the Original Eight Designated Rivers" width="535" /><figcaption style="font-size:12px; font-style:italic; color:#1B4C8C; margin: 0px 0px 0px 0px">The Rogue River, One of the Original Eight Designations; Photo by American Rivers</figcaption></figure>

<hr style="float: center" width="50%">

<p>By the 1950s decades of damming, development and diversion had taken their toll on our country's rivers.  During the 1960s the country began to recognize the damage we were inflicting on wildlife, the landscape, our drinking water and our legacy.  Recognition of this fact finally led to action by Congress to preserve the beauty and free-flowing nature of some of our most precious waterways.</p>

<p>Proposed by such environmental legends as John and Frank Craighead and Olaus Murie, and championed through Congress by the likes of Senators Frank Church and Walter Mondale, the National Wild and Scenic Rivers System was created by Congress in 1968 (Public Law 90-542; 16 U.S.C. 1271 et seq.) to preserve certain rivers with outstanding natural, cultural and recreational values in a free-flowing condition for the enjoyment of present and future generations.</p>

<p>Rivers in the National Wild and Scenic Rivers System capture the essence of all waterways that surge, ramble, gush, wander and weave through our country.  From the remote rivers of Alaska, Idaho and Oregon to rivers threading through the rural countryside of New Hampshire, Ohio and Massachusetts, each preserves a part of the American story and heritage.</p>

<p>As of December 2014 (the last designation), the National System protects 12,734 miles of 208 rivers in 40 states and the Commonwealth of Puerto Rico; this is a little more than one-quarter of one percent of the nation's rivers. By comparison, more than 75,000 large dams across the country have modified at least 600,000 miles, or about 17%, of American rivers.</p>

</div>
<!-- End #open-content -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>