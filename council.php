<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Interagency Coordinating Council';

// Set the page keywords
$page_keywords = 'Interagency Wild and Scenic Rivers Coordinating Council';

// Set the page description
$page_description = 'Interagency Coordinating Council';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Protecting Our River Heritage</h2>
<p>For more than four decades, the National Wild and Scenic Rivers Act has protected much of our river heritage. Rivers have defined our country and ourselves. In 1968, Congress recognized that many of our rivers were imperiled and the National Wild and Scenic Rivers System was born. Although an immediate success, for the first 25 years designated rivers have been managed differently by each agency, and many issues and questions related to the National Wild and Scenic Rivers Act have remained unresolved.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/council-img.jpg" alt="" width="565px" height="208px" /></center>

<div id="lower-content">

<div id="lc-left">
<h2 style="line-height:150%;">Development of the Interagency Wild &amp; Scenic Rivers Coordinating Council Charter</h2>
<p>In 1993, at the celebration marking the 25th anniversary of the National Wild and Scenic Rivers System, conservation organizations issued a challenge to the land management agencies&#8212;to establish an interagency council to address administration of our wild and scenic rivers. A few months later in Portland, Oregon, river planners from the Bureau of Land Management, National Park Service, and U.S. Forest Service met to draft a rough outline of what such a council would look like and what it could do. In April of 1995, heads of these agencies and the U.S. Fish and Wildlife Service, at a ceremony in Washington, D.C., signed the Interagency Wild &amp; Scenic Rivers Coordinating Council Charter. Department of the Interior Assistant Secretaries George Frampton, Jr., and Robert Armstrong, and Department of Agriculture Undersecretary James Lyons then approved the Charter, thereby changing 25 years of managing wild and scenic rivers.</p>
<p>The overriding goal of the Council is to improve interagency coordination in administering the Wild and Scenic Rivers Act, thereby improving service to the American public and enhancing protection of important river resources. The Council addresses a broad range of issues, from management concerns on rivers presently in the national system to potential additions listed on the Nationwide Rivers Inventory, from state designations to the provision of technical assistance to other governments and non-profits organizations.</p>
<p>The Council consists of representatives of the four wild and scenic rivers administering agencies&#8212;the Bureau of Land Management, National Park Service, U.S. Fish and Wildlife Service, and U.S. Forest Service. Other federal agencies with river interests have key contacts and participate in discussions affecting their interests. The public has an opportunity to provide input at all Council meetings; their support is crucial to the Council's success.</p>
<p>For more information on the Wild & Scenic Rivers Council or wild and scenic rivers, don't hesitate to <a href="mailto:rivers@fws.gov">Contact the Council</a> at <a href="mailto:rivers@fws.gov">rivers@fws.gov</a>.</p>
</div>
<!--END #lc-left -->

<div id="block-quote">
<h4>Interagency Wild and Scenic Rivers Coordinating Council Members:<br /><a href="documents/council-members.pdf">Current Council Members (PDF)</a></h4>
</div>
<!--END #block-quote -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>