<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Wyoming';
// Set the page keywords
$page_keywords = 'Wyoming';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Wyoming.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'WY';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Wyoming has approximately 108,767 miles of river, of which 408 miles are designated as wild &amp; scenic&#8212;less than 4/10ths of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/snake-hw.php" title="Snake River Headwaters">Snake River Headwaters</a></li>
<li><a href="rivers/yellowstone.php" title="Yellowstone River (Clark's Fork)">Yellowstone River (Clark's Fork)</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>