<cfinclude template="cfm-header.html">

<h1>Contact Us</h1>

<!--TITLE is set at the top of the page. For SEO purposes we want the <title> and Page Title to match. -->

<div id="content-wrapper">

<div id="content-page">
<h1>Contact Us</h1>

<p>If you have a question about wild and scenic rivers, we would be happy to try to answer it. Please note that we can only answer questions related to the Wild &amp; Scenic Rivers Act or the National Wild &amp; Scenic Rivers System.</p>

<p>If you have a question, please send it to:<blockquote>Dan Haas<br />U.S. Fish and Wildlife Service<br />64 Maple Street<br />Burbank, Washington 99323<br />Telephone: (509) 546-8333<br />Email: <a href="mailto:rivers@fws.gov">rivers@fws.gov</a></blockquote></p>

<p><strong style="color:#F36">To submit a question, request, or comment with this form, please be sure to type in the keywords at the bottom before sending.</strong> We get a tremendous amount of spam, and this is one way to eliminate it, thereby allowing us to better serve those of you with legitimate inquiries. Besides, believe it or not, using this form helps to translate books into online text.</p>

<hr>

<div id="forms-box">
<!---  <form name="Questions" action="http://pacific.fws.gov/script/rivers/act_contact.cfm" method="post"> --->
<form name="Questions" action="act_contact.cfm" method="post">
<input type="hidden" name="org_form" value="comments">
<label>My comments and/or questions are:</label>
<textarea name="Comments" cols="68" rows="10"></textarea>

<div class="formElement">
<label>I would like an e-mail response sent to:</label>
<input type="text" name="Comments_Email" size="90" value="" maxlength="50">
</div>

<center>
<input type="submit" value="Send Comments">
<input name="reset" value="Reset This Form" type="reset">
</center>

<div class="clear"></div>
<br />
<h2>Would you like a brochure about the National Wild and Scenic Rivers System?</h2>

<div class="clear"></div>
<input class="radio" type="radio" name="addtolist" value="No" checked></td>
<p>Don't send brochure</p>

<div class="clear"></div>
<input type="radio" class="radio" name="addtolist" value="Yes">
<p>Please send me a brochure (You must complete the address section below.)</p>
<p><b>Please note that by policy we cannot require you to provide us with your name, address, telephone number, or other contact information. However, if you choose not to provide us with this information, we cannot contact you or send you a brochure. Rest assured that we do not share any of this information with anyone else! For more information, we invite you to follow the Privacy Policy link in the bottom left corner of the page.</b></p>
<br />

<div class="clear"></div>

<div class="formElement">
<label>First Name:</label>
<input type="text" name="First_Name" size="90" value="" maxlength="50">
</div>

<div class="formElement">
<label>Last Name:</label>
<input type="text" name="Last_Name" size="90" value="" maxlength="50">
</div>

<div class="formElement">
<label>Organization:</label>
<input type="text" name="Organization" size="90" value="" maxlength="50">
</div>

<div class="formElement">
<label>Address:</label>
<input type="text" name="Address_1" size="90" value="" maxlength="50"></td>
</div>

<div class="formElement">
<label>&nbsp;</label>
<input type="text" name="Address_2" size="90" value="" maxlength="50"></td>
</div>

<div class="formElement">
<label>City:</label>
<input type="text" name="City" size="90" value="" maxlength="50">
</div>

<div class="clear"></div>
<label>State:</label>
<select name="State">
	<option value="00" selected>State</option>
	<option value="AL">Alabama</option>
	<option value="AK">Alaska</option>
	<option value="AZ">Arizona</option>
	<option value="AR">Arkansas</option>
	<option value="CA">California</option>
	<option value="CO">Colorado</option>
	<option value="CT">Connecticut</option>
	<option value="DE">Delaware</option>
	<option value="DC">District of Columbia</option>
	<option value="FL">Florida</option>
	<option value="GA">Georgia</option>
	<option value="HI">Hawaii</option>
	<option value="ID">Idaho</option>
	<option value="IL">Illinois</option>
	<option value="IN">Indiana</option>
	<option value="IA">Iowa</option>
	<option value="KS">Kansas</option>
	<option value="KY">Kentucky</option>
	<option value="LA">Louisiana</option>
	<option value="ME">Maine</option>
	<option value="MD">Maryland</option>
	<option value="MA">Massachusetts</option>
	<option value="MI">Michigan</option>
	<option value="MN">Minnesota</option>
	<option value="MS">Mississippi</option>
	<option value="MO">Missouri</option>
	<option value="MT">Montana</option>
	<option value="NE">Nebraska</option>
	<option value="NV">Nevada</option>
	<option value="NH">New Hampshire</option>
	<option value="NJ">New Jersey</option>
	<option value="NM">New Mexico</option>
	<option value="NY">New York</option>
	<option value="NC">North Carolina</option>
	<option value="ND">North Dakota</option>
	<option value="OH">Ohio</option>
	<option value="OK">Oklahoma</option>
	<option value="OR">Oregon</option>
	<option value="PA">Pennsylvania</option>
	<option value="RI">Rhode Island</option>
	<option value="SC">South Carolina</option>
	<option value="SD">South Dakota</option>
	<option value="TN">Tennessee</option>
	<option value="TX">Texas</option>
	<option value="UT">Utah</option>
	<option value="VT">Vermont</option>
	<option value="VI">Virginia</option>
	<option value="WA">Washington</option>
	<option value="WV">West Virginia</option>
	<option value="WI">Wisconsin</option>
	<option value="WY">Wyoming</option>
</select>

<div class="clear"></div>

<div class="formElement">
<label>Zip Code:</label>
<input type="text" name="Zip_Code" size="90" value="" maxlength="50">
</div>

<div class="formElement">
<label>Telephone:</label>
<input type="text" name="Telephone" size="90" value="" maxlength="50">
</label>
</div>

<div class="formElement">
<label>E-mail:</label>
<input type="text" name="Email" size="90" value="" maxlength="50">
</div>
<br />

<div class="clear"></div>
<br />
<center>
<input type="submit" value="Send Comments">
<input name="reset" value="Reset This Form" type="reset">
</center>


<div class="clear"></div>
<br />
<center>
<cf_recaptcha
privateKey="#Request.PrivateKey#"
publicKey="#Request.PublicKey#"
SSL="YES">
</center>
</form>
</div>
<!-- END Forms Box -->

</div>
</div>

<cfinclude template="cfm-footer.html">