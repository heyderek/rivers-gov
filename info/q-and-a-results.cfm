<cfinclude template="cfm-header.html">


         <h1>Wild &amp; Scenic River Questions &amp; Answers</h1>

         
            <cfinclude template="qry_qanda.cfm">
            <!--- Table for results --->
            
            <cfif get_questions.recordcount EQ '0'>
              
			  <h2>There were no questions that matched your query.</h2>
              
			  <p>Please select another question</p>
			  
             
            <cfelse>
             
            <!--- Output query results --->
            <cfoutput query="get_questions">
              
			  <div class="qa-result">
			  
			  <h2>Q: <a href="q-and-a-answers.cfm?id=#id#">#question#</a></h2>
		
			  <h3>A: <strong>#left(answer,195)# ... <a href="q-and-a-answers.cfm?id=#id#" style="color:003366;font-weight:bold;text-decoration:none;">More...</a></strong></h3>
	
			  <h6>Keywords: <a href="q-and-a-answers.cfm?id=#id#">#keyword#</a></h6>
			
			  </div>
            </cfoutput>
            </cfif>
             
			  <form><input type="Button" onClick="history.go(-1)" value="Try Another Question"></form>
			  
			  
<cfinclude template="cfm-footer.html">