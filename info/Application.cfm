<!--- This set of files does not require any form fields to contain data --->

<!--- Change this if the Datasource is changed. --->
 <cfset ds="rivers">  

<!--- Set the email contact to be notified when comments are left. --->
<cfif CGI.SERVER_NAME contains "bluedragon">
 	<cfset notify_email="daniel_haas_fws.gov@justplainfishermen.com"> 
 <cfelse>
	<cfset notify_email="daniel_haas@fws.gov"> 
</cfif>
<!--- Set show debug to yes for testing, no for production --->
<cfsetting enablecfoutputonly="No" showdebugoutput="yes">

<!--- Instantiate a bunch of variables --->
<cfparam name="select_me" default="0">
<cfparam name="sort_by" default="ID">
<cfparam name="Comments_on" default="">
<cfparam name="other_comments" default="">
<cfparam name="comments_from" default="">
<cfparam name="first_name" default="">
<cfparam name="last_name" default="">
<cfparam name="organization" default="">
<cfparam name="address_1" default="">
<cfparam name="address_2" default="">
<cfparam name="city" default="">
<cfparam name="state" default="">
<cfparam name="zip_code" default="">
<cfparam name="comments_email" default="#notify_email#">
<cfparam name="email" default="#notify_email#"> 
<cfparam name="TOEMAIL" default="#notify_email#">
<cfparam name="from_email" default="#notify_email#"> 

<cfparam name="telephone" default="">
<cfparam name="display" default="">
<cfparam name="comments" default="">
<cfparam name="keyword" default="">
<cfparam name="question" default="">
<cfparam name="answer" default="">
<cfparam name="display_name" default="">
<cfparam name="ACTIVITY" default="">
<cfparam name="OTHERACTIVITIES" default="">
<cfset Request.privateKey="6LdDoQgAAAAAAEJrRf23z1x7K4BXdo6HWquv86HE">
<cfset Request.publicKey="6LdDoQgAAAAAABRNPiuJvS5Ra-yvfQj17Z9R-vax"> 

<!-----
   Template: Application.cfm
   
   Author: James Hopkins, jim@fivek.com

   
	Purpose: Expected by CF, if there isn't one, it goes looking
   
   Use: In this case, sets datasource name globally, and iterates certain varibles.
   
   Date Created: 02/21/06
   
    
----->