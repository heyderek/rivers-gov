<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Unalakleet River, Alaska';

// Set the page keywords 
$page_keywords = 'Unalakleet River, Alaska';

// Set the page description
$page_description = 'Unalakleet River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('50');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?><?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Anchorage Field Office</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment of the main stem from the headwaters in T12S, R3W, Kateel River meridian, downstream to the western boundary of T18S, R8W.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 80.0 miles; Total &#8212; 80.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/unalakleet.jpg" alt="Unalakleet River" title="Unalakleet River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/ak/st/en/prog/nlcs/unalakleet_nwr.html" alt="Unalakleet River (Bureau of Land Management)" target="_blank">Unalakleet River (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Erin Johnson</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Unalakleet River</h2>

<p>The clear, smooth waters of the Unalakleet River originate in the rolling Tulato Hills, which divide rainfall and snowmelt between the Norton Sound and the Yukon River basin. The river starts out swift and channelized. Downstream of the designated section, it meanders across the arctic tundra 10 miles to the seaside village of Unalakleet, located on Norton Sound. For most of its length, the river has a varying pool/riffle nature, which offers a great diversity of river characteristics, boating skills and fishing opportunities.</p>

<p>"Unalakleet" is Inupiat for "place where the east wind blows," named by a people who have lived in the area for centuries. Early settlements were often located on rivers or along the coast because fishing and hunting opportunities were abundant, and rivers provided an excellent way to travel between distant villages. The Unalakleet River was a major avenue of trade in the 19th century, connecting coastal Eskimos, Yukon River interior peoples and Russian merchants. In 1898, reindeer herders from Lapland settled along the river, and shortly thereafter, prospectors seeking gold on the nearby Seward Peninsula traveled over the Kaltag Portage and downriver to the coast. Subsequent changes included a telegraph line and associated cabins along the river and establishment of a mail route.</p>

<p>The Iditarod National Historic Trail, which runs alongside the Unalakleet River to the Bering Sea coast, follows the trail once used by Alaska Native hunters, Russian explorers and gold seekers.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The scenery along the Unalakleet is subdued. The Tulato Hills are low and round-topped; prominent hills can be seen through breaks in the vegetation and along the long, straight sections. Largely, the views are pleasing combinations of plants, clear water, gravel bars, cut banks, inflowing streams and driftwood. Old Woman Mountain is the most dominant feature for five to six miles above and below the confluence with the Old Woman River. The aesthetic qualities of the diverse plant communities add immeasurably to the overall river environment.</p>

<p><strong><em>Fish and Wildlife</em></strong></p>

<p>The Unalakleet River supports a salmon fishery which in the past has supported 100,000 fish. Chinook, coho, pink and chum salmon spawn here. Annual runs provide income for both local inhabitants and sportfishing businesses. Arctic grayling, arctic char and whitefish are important subsistence species. 
Wildlife are important as commercial, subsistence and recreation resources. Moose, caribou and bear are the primary species of interest for the sport hunting trade. Trapping of marten, lynx, fox, wolf, beaver and muskrat supports the local economy.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>