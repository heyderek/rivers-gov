<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'North Carolina';
// Set the page keywords
$page_keywords = 'North Carolina';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - North Carolina.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'NC';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>North Carolina has approximately 37,853 miles of river, of which 144.5 miles are designated as wild &amp; scenic&#8212;less than 4/10ths of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>


<ul>
<li><a href="rivers/chattooga.php" title="Chattooga River">Chattooga River</a></li>
<li><a href="rivers/horsepasture.php" title="Horsepasture River">Horsepasture River</a></li>
<li><a href="rivers/lumber.php" title="Lumber River">Lumber River</a></li>
<li><a href="rivers/new.php" title="New River">New River</a></li>
<li><a href="rivers/wilson.php" title="Wilson Creek">Wilson Creek</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>