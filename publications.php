<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'Wild &amp; Scenic River Publications';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'Publications on wild &amp; scenic rivers.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->
<!-- JS that controls the accordion -->
<script type="text/javascript">
$(document).ready(function(){
$(".toggle_container").hide();
$("h2.trigger").click(function(){
$(this).toggleClass("active").next().slideToggle("slow");
});
});
</script>
<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Publications Related To Wild &amp; Scenic Rivers</h2>
<p>Most of the publications listed here are written and produced by the <a href="council.php">Interagency Wild &amp; Scenic Rivers Council</a> (Council).  Where an additional paper or reference is provided, the source is specifically noted. The Council does not endorse or ensure accuracy of these additional sources. Caveat emptor.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/northern-shovelers.jpg" alt="Northern Shovelers" height="366" title="Northern Shovelers - Pat Gaines" width="565" /><br /><a href="http://www.flickr.com/photos/33403047@N00/" target="_blank"><em style="font-size:11px">Photo by Pat Gaines</em></a></center>

<div id="lower-content">

<div id="lc-left">
<h2>You Need Acrobat</h2>
<p>Most of these publications are available here in PDF format; if you don't have Adobe Acrobat Reader 9.0 or higher, please visit <a href="http://www.adobe.com">www.adobe.com</a> to download a free version. If you would like the original WordPerfect file (version 12.0), please contact <a href="mailto:rivers@fws.gov">us</a> at <a href="mailto:rivers@fws.gov">rivers@fws.gov</a>.</p>
</div>
<!--END #lc-left -->

<div id="block-quote">
<h4><em>The face of the river . . . was not a book to be read once and thrown aside, for it had a new story to tell every day.</em> &#8212; Mark Twain</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<div id="accords">
<h2 class="trigger">Council White Papers</h2>

<div class="toggle_container">

<div class="block">
<p><em><a href="https://www.rivers.gov/documents/wsr-primer.pdf" title="An Introduction to Wild &amp; Scenic Rivers" target="_blank">An Introduction to Wild &amp; Scenic Rivers (76 KB PDF)</a></em> &#8212; A concise primer on wild and scenic rivers and what designation means to you.</p>
<p><em><a href="https://www.rivers.gov/documents/q-a.pdf" title="A Compendium of Questions &amp; Answers Relating to Wild &amp; Scenic Rivers" target="_blank">A Compendium of Questions &amp; Answers Relating to Wild & Scenic Rivers (317 KB PDF)</a></em> &#8212; Everything you wanted to know about wild and scenic rivers in a Q&amp;A format.  These Q&amp;As can also be accessed through a <a href="information.php">searchable data base</a>.</p>
<p><em><a href="documents/river-manager-core-competencies.pdf" title="Core Competencies for River Management" target="_blank">Core Competencies for River Management (220 KB PDF)</a></em> &#8212; What are the knowledge, skills and abilities to look for in river management positions? Here are some guidelines.</p>
<p><em><a href="https://www.rivers.gov/documents/2aii.pdf" title="Designating Rivers Through Section 2(a)(ii) of the Wild and Scenic Rivers Act" target="_blank">Designating Rivers Through Section 2(a)(ii) of the Wild and Scenic Rivers Act (416 KB PDF)</a></em> &#8212; This paper describes a process for designating rivers into the National System at the request of a state.</p>
<p><em><a href="https://www.rivers.gov/documents/boundaries.pdf" title="Establishment of Wild &amp; Scenic River Boundaries" target="_blank">Establishment of Wild &amp; Scenic River Boundaries (52 KB PDF)</a></em></p>
<p><em><a href="https://www.rivers.gov/documents/wsr-act-evolution.pdf" title="Evolution of the Wild and Scenic Rivers Act: A History of Substantive Amendments 1968-2013" target="_blank">Evolution of the Wild and Scenic Rivers Act: A History of Substantive Amendments 1968-2013 (585 KB PDF)</a></em> &#8212; This paper looks at all of the amendments to the Wild &amp; Scenic Rivers Act since its inception and explores the impacts of those amendments.</p>
<p><a href="record.php" title="Evolution of the Wild and Scenic Rivers Act: A History of Substantive Amendments 1968-2013">Congressional Record for <em>Evolution of the Wild and Scenic Rivers Act: A History of Substantive Amendments 1968-2013</a> paper</em> &#8212; This page presents the congressional history that was used in this paper.</p>
<p><em><a href="https://www.rivers.gov/documents/federal-agency-roles.pdf" title="Implementing the Wild &amp; Scenic Rivers Act:  Authorities and Roles of Key Federal Agencies" target="_blank">Implementing the Wild &amp; Scenic Rivers Act:  Authorities and Roles of Key Federal Agencies (167 KB PDF)</a></em> &#8212; What responsibilities do other agencies have for wild and scenic rivers?  Here's the answer.</p>
<p><em><a href="https://www.rivers.gov/documents/crmp-steps.pdf" title="Interim Management and Steps to Develop a CRMP" target="_blank">Interim Management and Steps to Develop a CRMP</a></em> &#8212; This paper provides guidance for interim management of a newly designated wild and scenic river and generalized steps to develop a comprehensive river management plan.  It expands the content of Appendix A of the Wild &amp; Scenic River Management Responsibilities paper below.</p>
<p><em><a href="https://www.rivers.gov/documents/non-federal-lands-protection.pdf" title="Protecting Resource Values on Non-Federal Lands" target="_blank">Protecting Resource Values on Non-Federal Lands (193 KB PDF)</a></em> &#8212; How wild and scenic rivers are protected where the federal government doesn't manage the surrounding area.</p>
<p><strong>NEW&#8212;February 6, 2018:</strong>&nbsp;&nbsp;<em><a href="https://www.rivers.gov/documents/user-capacities.pdf" title="Steps to Address User Capacities for Wild and Scenic Rivers" target="_blank">Steps to Address User Capacities for Wild and Scenic Rivers (305 KB PDF)</a></em> &#8212; Every river management plan is required to address user capacities. This paper explains how to determine those capacities.</p>
<p><em><a href="https://www.rivers.gov/documents/water.pdf" title="Water Quantity and Quality as Related to the Management of Wild &amp; Scenic Rivers" target="_blank">Water Quantity and Quality as Related to the Management of Wild &amp; Scenic Rivers (580 KB PDF)</a></em> &#8212; How to protect water quality and instream flows.</p>
<p><em><a href="https://www.rivers.gov/documents/management.pdf" title="Wild &amp; Scenic River Management Responsibilities" target="_blank">Wild &amp; Scenic River Management Responsibilities (254 KB PDF)</a></em> &#8212; Considerations in managing&#8212;and developing management plans for&#8212;wild and scenic rivers.
	<dl><dd><em><a href="https://www.rivers.gov/documents/crmp-steps.pdf" title="Interim Management and Steps to Develop a CRMP" target="_blank">Interim Management and Steps to Develop a CRMP</a></em> &#8212; This paper provides guidance for interim management of a newly designated wild and scenic river and generalized steps to develop a comprehensive river management plan.  It expands the content of Appendix A of the Wild &amp; Scenic River Management Responsibilities paper above.</dd></dl></p>
<p><em><a href="https://www.rivers.gov/documents/study-process.pdf" title="The Wild &amp; Scenic River Study Process" target="_blank">The Wild &amp; Scenic River Study Process (250 KB PDF)</a></em> &#8212; This paper explains the wild and scenic river study process for congressionally authorized and agency-identified study rivers.</p>
<p><em><a href="https://www.rivers.gov/documents/section-7.pdf" title="The Wild &amp; Scenic Rivers Act:  Section 7" target="_blank">The Wild &amp; Scenic Rivers Act:  Section 7 (563 KB PDF)</a></em> &#8212; This paper describes the standards and procedures used in evaluating the effects of proposed water resources projects.</p>
<p><em><a href="https://www.rivers.gov/documents/eminent-domain.pdf" title="Wild &#38; Scenic Rivers and the Use of Eminent Domain" target="_blank">Wild &#38; Scenic Rivers and the Use of Eminent Domain (115 KB PDF)</a></em></p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Reference</h2>

<div class="toggle_container">

<div class="block">
<p><em><a href="https://www.rivers.gov/documents/rivers-table.pdf" target="_blank">Wild &amp; Scenic Rivers Table (150 KB PDF)</a></em> &#8212; The full listing of designated wild and scenic rivers, complete with mileage, classifications and managing agencies.</p>
<p><em><a href="https://www.rivers.gov/documents/wsr-act.pdf" target="_blank">Wild &amp; Scenic Rivers Act Abridged (108 KB PDF)</a></em> &#8212; The full act minus the rivers listed in Sections 3(a) and 5(a).  The complete text, as well as all the amendments, is available in the section labeled WSR Act &amp; Amendments.  Not a Council product.</p>
<p><em><a href="https://www.rivers.gov/documents/landowners.pdf" target="_blank">Wild and Scenic Rivers Guide For Riverfront Property Owners (3.02 KB PDF - Color Brochure)</a></em> &#8212; A concise guide for landowners along designated rivers that outlines their responsibility for management, how Section 7 of the Wild and Scenic Rivers Act applies to their lands, and how best to protect the value of their lands as well as the values of the river.</p>
<p><em><a href="https://www.rivers.gov/documents/glossary.pdf" target="_blank">Abbreviations &amp; Glossary (51 KB PDF)</a></em> &#8212; The abbreviations and glossary of terms used in our publications.</p>
<p><em><a href="https://www.rivers.gov/documents/bibliography.pdf" target="_blank">Selected Bibliography of Wild &amp; Scenic River Publications (46 KB PDF)</a></em> &#8212; Some of the publications we think are the best at explaining the National Wild and Scenic Rivers System and how river protection is accomplished in this country.</p>
<p><a href="https://www.rivers.gov/documents/mileage.zip">Agency-By-Agency, River and State Mileage/Management Spreadsheet (123 KB)</a> &#8212; This is a zipped Excel file of the complete National Wild and Scenic Rivers System broken down several different ways&#8212;miles of river, miles per state, rivers per state, miles per agency, etc. Sorry, due to security restrictions on our server, we can only provide a zipped file.</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">National Wild &amp; Scenic Rivers Policies</h2>

<div class="toggle_container">

<div class="block">

<p><em><a href="https://www.rivers.gov/documents/guidelines.pdf" target="_blank">Departments of the Interior and Agriculture Guidelines For Eligibility, Classification and Management (1982) - PDF Version</em> (From <em>Federal Register</em>)</a> &#8212; Not a Council product.</p>
<p><em><a href="https://www.rivers.gov/documents/blm-policy-manual.pdf" target="_blank">Bureau of Land Management Policy and Program Direction for Identification, Evaluation, Planning and Management of Wild &amp; Scenic Rivers - PDF Version</a></em> &#8212; Not a Council product.</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Section 7 Determinations</h2>

<div class="toggle_container">

<div class="block">
<p>In response to requests from river program managers, Council members have selected examples of Section 7 determinations for common types of water resources projects. Each is an actual determination made by river-administering agency staff from across the country. In some cases, clarifying user notes are included in individual determinations. No single example is best; however, in reviewing the range of examples provided, the practitioner will gain an understanding of how to apply the procedures outlined in the technical report.</p>
<p>Also, don't forget to make use of <em><a href="https://www.rivers.gov/documents/section-7.pdf" target="_blank">The Wild &amp; Scenic Rivers Act:  Section 7 (563 KB PDF)</a></em>. This paper describes the standards and procedures used in evaluating the effects of proposed water resources projects and is found in the Council White Papers section.</p>
<p><a href="https://www.rivers.gov/documents/section7/section7-introduction.pdf" target="_blank">Introduction to Section 7 Examples (1.04 MB PDF)</a></p>
<p>Hydropower Licensing
  <ol type="A">
  <li>Klamath Project</li>
    <ol>
    <li><a href="https://www.rivers.gov/documents/section7/klamath-ferc-comment-letter.pdf" target="_blank">Section 7 Determination Transmittal Letter (717 KB PDF)</a></li>
    <li><a href="https://www.rivers.gov/documents/section7/klamath-map.pdf" target="_blank">Klamath River Section 7 Map (285 KB PDF)</a></li>
    <li><a href="https://www.rivers.gov/documents/section7/klamath-section7-report.pdf" target="_blank">Klamath River Hydropower Project Section 7 Report (441 KB PDF)</a></li>
    <li><a href="https://www.rivers.gov/documents/section7/klamath-section7-determination.pdf" target="_blank">Klamath River Hydropower Project Section 7 Determination (2.49 MB PDF)</a></li>
    </ol>
  <li>Hells Canyon Complex Project</li>
    <ol>
    <li><a href="https://www.rivers.gov/documents/section7/hellscanyon-section7-report.pdf" target="_blank">Hells Canyon Hydropower Project Section 7 Determination &amp; Report (268 KB PDF)</a></li>
    </ol>
  <li>North Umpqua Project</li>
    <ol>
    <li><a href="https://www.rivers.gov/documents/section7/north-umpqua-section7-application-determination.pdf" target="_blank">North Umpqua Hydropower Project Section 7 Determination on License Application (3.25 MB PDF)</a></li>
    <li><a href="https://www.rivers.gov/documents/section7/north-umpqua-section7-deis-determination.pdf" target="_blank">North Umpqua Hydropower Project Section 7 Determination on Draft EIS (634 KB PDF)</a></li>
    <li><a href="https://www.rivers.gov/documents/section7/north-umpqua-section7-feis-determination.pdf" target="_blank">North Umpqua Hydropower Project Section 7 Determination on Final EIS (727 KB PDF)</a></li>
    </ol>
  </ol>
</p>
<p>Bridge Construction or Replacement
  <ol start="4" type="A">
  <li><a href="https://www.rivers.gov/documents/section7/county-road-36-section7-determination.pdf" target="_blank">Big Darby Creek County Road 36 Bridge Replacement (1.41 MB PDF)</a></li>
  <li><a href="https://www.rivers.gov/documents/section7/imnaha-section7-determination.pdf" target="_blank">Imnaha River Road Bridge Replacement (5.82 MB PDF)</a></li>
  <li><a href="https://www.rivers.gov/documents/section7/sturgeon-section7-determination.pdf" target="_blank">Sturgeon River Bridge Replacement (7.06 MB PDF)</a></li>
  </ol>
</p>
<p>Stabilization Projects
  <ol start="7" type="A">
  <li><a href="https://www.rivers.gov/documents/section7/section14-study-erosion-project.pdf" target="_blank">Little Miami River Section 14 Study Erosion Project (3.54 MB PDF)</a></li>
  <li><a href="https://www.rivers.gov/documents/section7/garnett-bank-stabilization-section7-determination.pdf" target="_blank">Imnaha River Garnett Bank Protection Project (5.40 MB PDF)</a></li>
  <li><a href="https://www.rivers.gov/documents/section7/mills-bioengineering-project.pdf" target="_blank">Upper Deschutes River Mills Bioengineering Project (2.81 MB PDF)</a></li>
  </ol>
</p>
<p>Infrastructure Projects
  <ol start="10" type="A">
  <li><a href="https://www.rivers.gov/documents/section7/gehr-boat-dock-section7-determination.pdf" target="_blank">Great Egg Harbor River Boat Dock (66 KB PDF)</a></li>
  <li><a href="https://www.rivers.gov/documents/section7/forest-school-outfall-pipe-section7-determination.pdf" target="_blank">Allegheny River Forest School Outfall Pipe (3.39 MB PDF)</a></li>
  <li><a href="https://www.rivers.gov/documents/section7/boyle-boat-ramp-section7-determination.pdf" target="_blank">Allegheny River Boyle Boat Ramp and Dock (911 KB PDF)</a></li>
  </ol>
</p>
<p><strong>Section 7 Flowcharts &#8212;</strong> In response to requests from river managers, Council members have developed three flowcharts to guide practitioners in determining if a project proposal is a water resources project subject to Section 7(a) and, if so, which evaluative standard applies. The flowcharts also reference the appropriate detailed evaluative process in the Council's Section 7 technical report.</p>
<p><a href="https://www.rivers.gov/documents/section7/flowchart-introduction.pdf" target="_blank">Introduction To Flowcharts</a></p>
<p><a href="https://www.rivers.gov/documents/section7/process-flowchart.pdf" target="_blank">Section 7 Process Schematic</a></p>
<p><a href="https://www.rivers.gov/documents/section7/process-flowchart-within-corridor.pdf" target="_blank">Water Resources Projects Within A WSR Corridor</a></p>
<p><a href="https://www.rivers.gov/documents/section7/process-flowchart-outside-corridor.pdf" target="_blank">Water Resources Projects Outside A WSR Corridor</a></p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Non-Council Publications</h2>
<div class="toggle_container">

<div class="block">
<p><em><a href="https://www.rivers.gov/documents/partnerships.pdf" target="_blank">The River Partnership Paradigm (384 KB PDF)</a></em> &#8212; Prepared by the U.S. Department of Agriculture.</p>
<p><em><a href="https://www.rivers.gov/documents/crs-water-rights-2008.pdf" target="_blank">The Wild &amp; Scenic Rivers Act and Federal Water Rights - 2008 (124 KB PDF)</a></em> &#8212; Prepared by the Congressional Research Service.</p>
<p><em><a href="https://www.rivers.gov/documents/crs-water-rights-2009.pdf" target="_blank">The Wild &amp; Scenic Rivers Act and Federal Water Rights - 2009 (155 KB PDF)</a></em> &#8212; Prepared by the Congressional Research Service.</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">About The Interagency Council</h2>
<div class="toggle_container">

<div class="block">
<p><em><a href="https://www.rivers.gov/documents/council-members.pdf" target="_blank">Interagency Wild &amp; Scenic Rivers Coordinating Council Members (50 KB PDF)</a></em></p>
<p><em><a href="https://www.rivers.gov/documents/council-q-a.pdf" target="_blank">Questions &amp; Answers on the Interagency Council (48 KB PDF)</a></em> &#8212; Quick answers to some of the questions we've been asked about ourselves.</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Graphics</h2>
<div class="toggle_container">
<div class="block">
<p><strong>National Wild &amp; Scenic Rivers System Logos</strong>
<p>The logos below come in both color and black-and-white. The Illustrator and EPS files are vector files that can be scaled to any size. If you're not used to working with vector files, there are the standard TIF and JPG files. The PNG files have transparent backgrounds for use in Powerpoints, web pages, etc.
<dl>
  <dt>
  <dd><a href="https://www.rivers.gov/documents/wsr-sign-standards.pdf" title="WSR Logo - Sign Standards">WSR Sign Standards PDF (Color, Size, Etc. - 157 KB PDF)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo.ai" title="WSR Logo - AI">WSR Color Logo AI (1.6 MB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo.eps" title="WSR Logo - EPS">WSR Color Logo EPS (1.8 MB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo.tif" title="WSR Logo - TIF">WSR Color Logo TIF (5.4 MB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo.jpg" title="WSR Logo - JPG">WSR Color Logo JPG (839 KB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo.eps" title="WSR Logo - PNG">WSR Color Logo PNG (70 KB Zip File)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo-bw.ai" title="WSR Logo - AI">WSR B&amp;W Logo AI (1.6 MB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo-bw.eps" title="WSR Logo - EPS">WSR B&amp;W Logo EPS (1.8 MB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo-bw.tif" title="WSR Logo - TIF">WSR B&amp;W Logo TIF (1.3 MB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo-bw.jpg" title="WSR Logo - JPG">WSR B&amp;W Logo JPG (430 KB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logo-bw.eps" title="WSR Logo - PNG">WSR B&amp;W Logo PNG (64 KB Zip File)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/images/logos/wsr-logos.zip" title="WSR Logo - ZIP">All WSR Logos ZIP (6.8 MB)</a></dd>
  <dd><br /><a href="https://www.rivers.gov/wsr50/files/50th-wsr-logo.zip" title="WSR Logo - 50th">WSR 50th Anniversary Logos ZIP (2.9 MB)</a></dd>
  <dd><br /><a href="wsr50/files/50th-wsr-logo-spanish.zip" title="WSR Spanish Logo - 50th">WSR 50th Anniversary Spanish Logos ZIP (5.9 MB)</a></dd>
  <dt>
  <dl>
</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

</div>
<!--END #accords -->

<div class="clear"></div> <!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>