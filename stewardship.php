<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Stewardship';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, conservation, stewardship, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'The National Wild and Scenic Rivers System - Stewardship.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>
 
<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Stewardship</h2>
<p><em>Boundaries don't protect rivers, people do.</em> &#8212; Brad Arrowsmith, Landowner along the Niobrara National Scenic River, Nebraska</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/cleanup.jpg" alt="" width="565px" height="378px" /></center>

<div id="lower-content">

<div id="lc-left">
<h2>Volunteering</h2>
<p>While these words were spoken in a different context than you might think, the sentiment and the accuracy of the statement couldn't be more true. It is not just government agencies that need to be responsible for protecting and manging our national wild &amp; scenic rivers. You are every bit as critical.</p>
<p>Many agencies could use your help in protecting our nation's river treasures. If you have the time, please consider volunteering with your local agency on one of this country's great rivers.</p>
<p>Check out the video below to see what you're missing.</p>
<p>&nbsp;</p>
<p align="center"><center><iframe src="https://player.vimeo.com/video/247214136" width="560" height="420" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></center></p>
</div>
<!--END #lc-left -->

<div id="block-quote">
<h4><em>The activist is not the man who says the river is dirty. The activist is the man who cleans up the river.</em> &#8212; Ross Perot</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>