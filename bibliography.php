<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Selected Bibliography';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, books, reference, bibliography';

// Set the page description
$page_description = 'Wild and Scenic Rivers bibliography.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<!--<div id="intro-box">
<h2>Please note:</h2>
<p>Most of these publications must be purchased or borrowed from a library.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><div style="margin-top:75px"><img src="images/bibliography-img.jpg" alt="" width="565px" height="156px" /></div></center>

<div id="lower-content">

<div id="lc-left">
<p style="margin-left:.5in;text-indent:-.5in"><strong>Bates, Sarah F., David H. Getches, Lawrence J. MacDonnell, and Charles F. Wilkerson.</strong> 1993. <em>Searching Out the Headwaters:  Change and Rediscovery in Western Water Policy.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Boling, David M. and River Network.</strong> 1994. <em>How to Save a River.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Clemings, Russell.</strong> 1996. <em>Mirage: The False Promise of Desert Agriculture.</em> Sierra Club Books, San Francisco, California.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Coyle, Kevin J.</strong> 1988. <em>The American Rivers Guide to Wild and Scenic River Designation: A Primer on National River Conservation.</em> American Rivers, Inc., Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Curtis, Christopher.</strong> 1992. <em>Grassroots River Protection: Saving Rivers Under the National Wild and Scenic Rivers Act Through Community-Based Protection Strategies and State Action.</em> American Rivers, Inc., Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Diamant, Rolf J., Glenn J. Eugster and Christopher J. Duerksen.</strong> 1984. <em>A Citizen's Guide to River Conservation.</em> The Conservation Foundation, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Echeverria, John D., Pope Barrow, and Richard Roos-Collins.</strong> 1989. <em>Rivers at Risk: The Concerned Citizen's Guide to Hydropower.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Federal Interagency Floodplain Management Task Force.</strong> 1996. <em>Protecting Floodplain Resources: A Guidebook for Communities.</em> Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Frost, Peter M. K.</strong> 1993. "Protecting and Enhancing Wild and Scenic Rivers in the West." Idaho Law Review, vol. 29, no. 2. College of Law, University of Idaho, Moscow, Idaho.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Getches, David H.</strong> 1990. <em>Water Law in a Nutshell.</em> Nutshell Series, West Publishing Company, St. Paul, Minnesota.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>High Country News, eds.</strong> 1987. <em>Western Water Made Simple.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Hoose, Phillip M.</strong> 1981. <em>Building an Ark: Tools for the Preservation of Natural Diversity Through Land Protection.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Hunt, Constance Elizabeth.</strong> 1988. <em>Down by the River: The Impact of Federal Water Projects and Policies on Biological Diversity.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Miller, Arthur P., Jr., and Marjorie L. Miller.</strong> 1991. <em>Park Ranger Guide to Rivers and Lakes.</em> Stackpole Books, Harrisburg, Pennsylvania.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>National Park Service.</strong> 1989. <em>Economic Impacts of Protecting Rivers, Trails, and Greenway Corridors.</em> San Francisco, California.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Palmer, Tim.</strong> 1996. <em>America By Rivers.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Palmer, Tim.</strong> 1986. <em>Endangered Rivers and the Conservation Movement.</em> University of California Press, Berkeley, California.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Palmer, Tim.</strong> 1994. <em>Lifelines: The Case for River Conservation.</em> Island Press, Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Palmer, Tim.</strong> 2017. <em>Wild and Scenic Rivers: An American Legacy.</em> Oregon State University Press, Corvallis, Oregon.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Pollak, Daniel.</strong> 2005. <em><a href="documents/impacts-california-wild-scenic-rivers-act.pdf" target="_blank">Impacts of the California Wild &amp; Scenic Rivers Act.</a> (Downloadable PDF)</em> California Research Bureau, Sacramento, California.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>River Conservation Fund.</strong> 1977. <em>Flowing Free: A Citizen's Guide for Protecting Wild and Scenic Rivers.</em> Washington, District of Columbia.</p>
<p style="margin-left:.5in;text-indent:-.5in"><strong>Whitaker, Doug, Bo Shelby, William Jackson, and Robert Beschta.</strong> 1993. <em>Instream Flows for Recreation: A Handbook on Concepts and Research Methods.</em> National Park Service, Washington, District of Columbia, and Oregon State University, Corvallis, Oregon.</p>
</div>
<!--END #lc-left -->

<div id="block-quote">
<h4>Please note that most of these publications must be purchased or borrowed from a library.</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>
