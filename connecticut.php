<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Connecticut';
// Set the page keywords
$page_keywords = 'Connecticut';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Connecticut.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

// Create a postal code ID for checking against.
$state_code = 'CT';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Connecticut has approximately 5,828 miles of river, of which 39.3 miles are designated as wild &amp; scenic&#8212;less than 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/eightmile.php" title="Eightmile River">Eightmile River</a></li>
<li><a href="rivers/farmington.php" title="Farmington River">Farmington River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>