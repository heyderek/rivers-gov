<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'River and Water Facts';

// Set the page keywords 
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection';

// Set the page description
$page_description = 'Water and river trivia.';

// Set the region for Sidebar Images 
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
 ?>
 
	<!-- BEGIN page specific CSS and Scripts -->	
	
		<!-- JS that controls the accordion -->
			<script type="text/javascript">
					$(document).ready(function(){
						
						$(".toggle_container").hide();

						$("h2.trigger").click(function(){
							$(this).toggleClass("active").next().slideToggle("slow");
						});

					});
			</script>
		
	<!-- END page specific CSS and Scripts -->

		
			<?php 
			// includes the TEMPLATE HEADER CODING -- #content-page		
			include ("includes/header.php") 
			?>
			
			<?php 
			// includes the content page top		
			include ("includes/content-head.php") 
			?>
					
						<div id="intro-box">
						<h2>River and Water Trivia</h2>
						
						<p>Over time, we've collected numerous facts on rivers, which we're willing to share. Since we haven't always kept track of the source, be sure to cross check the fact if you use it. However, we believe them to be accurate.</p>

<p>Many of these interesting facts were borrowed from the Brita water filter site (www.brita.com). That site has many more interesting bits of water lore.</p>

<p>These statistics are presented here to illustrate the importance of protecting our precious waters. Please do not send us requests for more trivia.</p>

						</div><!--END #intro-box -->	
						
						<!--<div id="lower-content">-->	
						
							<div class="clear"></div> <!-- Allows for content above to be flexible -->
						
						<!--</div> <!--END #lower-content -->
                        
                        <!-- Insert an image placeholder sized at 565 x 121 -->
						<center>
						<img src="images/waterfacts-img.jpg" alt="" width="565px" height="178px" />
						</center>	
						
							<div id="accords" style="margin-bottom:130px;">
							
							<h2 class="trigger">River Protection</h2>
							<div class="toggle_container">
								<div class="block" style="margin-left:0px;">
									<!--<h2>PARAGRAPH HEADING</h2>-->

<p>The National Wild and Scenic Rivers System has only 12,598 river miles in it&#8212;less than one-quarter of one percent of our rivers are protected through this designation.<br /><br />

Currently, 600,000 miles of our rivers lie behind an estimated 60,000 to 80,000 dams.<br /><br />

The United States has over 3,660,000 miles of rivers. The 600,000 miles of rivers lying behind dams amounts to fully 17% of our river mileage. (Source: Environmental Protection Agency 1998 National Water Quality Report)<br /><br />

At least 9.6 million households and $390 billion in property lie in flood prone areas in the United States. The rate of urban growth in floodplains is approximately twice that of the rest of the country.</p>


Water Volume

<p>Water covers nearly three-fourths of the earth's surface.<br /><br />

The overall amount of water on our planet has remained the same for two billion years.<br /><br />

The earth's total allotment of water has a volume of about 344 million cubic miles. Of this:
<ul style="list-style-type:disc; line-height:150%;">
<li>315 million cubic miles (93%) is sea water!</li>
<li>9 million cubic miles (2.5%) is in aquifers deep below the earth's surface.</li>
<li>7 million cubic miles (2%) is frozen in polar ice caps.</li>
<li>53,000 cubic miles of water pass through the planet's lakes and streams.</li>
<li>4,000 cubic miles of water is atmospheric moisture.</li>
<li>3,400 cubic miles of water are locked within the bodies of living things.</li>
</ul><br /><br />

If all the water in the Great Lakes was spread evenly across the continental U.S., the ground would be covered with almost 10 feet of water.<br /><br />

One gallon of water weighs 8.34 pounds.</p>
					
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            
                            <h2 class="trigger">Water Availability</h2>
							<div class="toggle_container">
								<div class="block" style="margin-left:0px;">
									<!--<h2>PARAGRAPH HEADING</h2>-->

<p>If all the world's water were fit into a gallon jug, the fresh water available for us to use would equal only about one tablespoon.<br /><br />

Most of the earth's surface water is permanently frozen or salty.<br /><br />

Approximately 70% of the world's supply of fresh water is located in Antarctica, locked in 90% of the world's ice. (Source: Gulf of Maine Research Institute)<br /><br />

It doesn't take much salt to make water "salty." If one-thousandth (or more) of the weight of water is from salt, then the water is "saline."<br /><br />

Saline water can be desalinated for use as drinking water by going through a process to remove the salt from the water. The process costs so much that it isn't done on a very large scale. The cost of desalting sea water in the U.S. ranges from $1 to $16 per 1000 gallons.<br />br />

Each day almost 10,000 children under the age of 5 in Third World countries die as a result of illnesses contracted by use of impure water.<br /><br />

Most of the world's people must walk at least 3 hours to fetch water.<br /><br />

By 2025, 52 countries&#8212;with two-thirds of the world's population&#8212;will likely have water shortages.<br /><br />

1.2 Billion &#8212; Number of people worldwide who do not have access to clean water.<br />6.8 Billion &#8212; Gallons of water Americans flush down their toilets every day.</p>					
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            
                            <h2 class="trigger">Water Use</h2>
							<div class="toggle_container">
								<div class="block" style="margin-left:0px;">
									<!--<h2>PARAGRAPH HEADING</h2>-->

<p>The United States consumes water at twice the rate of other industrialized nations.<br /><br />

1.2 Billion &#8212; Number of people worldwide who do not have access to clean water.<br />6.8 Billion &#8212; Gallons of water Americans flush down their toilets every day.<br /><br />

The average single-family home uses 80 gallons of water <em>per person</em> each day in the winter and 120 gallons in the summer. Showering, bathing and using the toilet account for about two-thirds of the average family's water usage.<br /><br />

The average person needs 2 quarts of water a day.<br /><br />

During the 20th century, water use increased at double the rate of population growth; while the global population tripled, water use per capita increased by six times.<br /><br />

On a global average, most freshwater withdrawls&#8212;69%&#8212;are used for agriculture, while industry accounts for 23% and municipal use (drinking water, bathing and cleaning, and watering plants and grass) just 8%.<br /><br />

Water used around the house for such things as drinking, cooking, bathing, toilet flushing, washing clothes and dishes, watering lawns and gardens, maintaining swimming pools, and washing cars accounts for only 1% of all the water used in the U.S. each year.<br /><br />

Eighty percent of the fresh water we use in the U.S. is for irrigating crops and generating thermoelectric-power.<br /><br />

More than 87% of the water consumed in Utah is used for agriculture and irrigation.<br /><br />

Per capita water use in the western U.S. is much higher than in any other region, because of agricultural needs in this arid region. In 1985, daily per capita consumption in Idaho was 22,200 gallons versus 152 gallons in Rhode Island.<br /><br />

A corn field of one acre gives off 4,000 gallons of water per day in evaporation.<br /><br />

It takes about 6 gallons of water to grow a single serving of lettuce. More than 2,600 gallons is required to produce a single serving of steak.<br /><br />

It takes almost 49 gallons of water to produce just one eight-ounce glass of milk. That includes water consumed by the cow and to grow the food she eats, plus water used to process the milk.<br /><br />

About 6,800 gallons of water is required to grow a day's food for a family of four.<br /><br />

The average American consumes 1,500 pounds of food each year; 1,000 gallons of water are required to grow and process each pound of that food&#8212;1.5 million gallons of water is invested in the food eaten by just one person! This 200,000-cubic-feet-plus of water-per-person would be enough to cover a football field four feet deep.<br /><br />

About 39,090 gallons of water is needed to make an automobile, tires included.</p>				
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            
                            
                            <h2 class="trigger">Habitats &amp; Wildlife</h2>
							<div class="toggle_container">
								<div class="block" style="margin-left:0px;">
									<!--<h2>PARAGRAPH HEADING</h2>-->

<p>Only 7% of the country's landscape is in a riparian zone, only 2% of which still supports riparian vegetation.<br /><br />

The U.S. Fish and Wildlife Service estimate that 70% of the riparian habitat nationwide has been lost or altered.<br /><br />

More than 247 million acres of United States' wetlands have been filled, dredged or channelized&#8212;an area greater than the size of California, Nevada and Oregon combined.<br /><br />

Over 90% of the nearly 900,000 acres of riparian areas on Bureau of Land Management lands are in degraded condition due to livestock grazing.<br /><br />

Riparian areas in the West provide habitat for more species of birds than all other western vegetation combined; 80% of neotropical migrant species (mostly songbirds) depend on riparian areas for nesting or migration.<br /><br />

Fully 80% of all vertebrate wildlife in the Southwest depend on riparian areas for at least half of their life.<br /><br />

Of the 1,200+ species listed as threatened or endangered, 50% depend on rivers and streams.<br /><br />

One fifth of the world's freshwater fish&#8212;2,000 of 10,000 species identified&#8212;are endangered, vulnerable, or extinct. In North America, the continent most studied, 67% of all mussels, 51% of crayfish, 40% of amphibians, 37% of fish, and 75% of freshwater mollusks are rare, imperiled, or already gone.<br /><br />

At least 123 freshwater species became extinct during the 20th century. These include 79 invertebrates, 40 fishes, and 4 amphibians. (There may well have been other species that were never identified.)<br /><br />

Freshwater animals are disappearing five times faster than land animals.<br /><br />

In the Pacific Northwest, over 100 stocks and subspecies of salmon and trout have gone extinct and another 200 are at risk due to a host of factors, dams and the loss of riparian habitat being prime factors.<br /><br />

A 1982 study showed that areas cleared of riparian vegetation in the Midwest had erosion rates of 15 to 60 tons per year.<br /><br />

One mature tree in a riparian area can filter as much as 200 pounds of nitrates runoff per year.</p>				
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
								
							</div> <!--END #accords -->
                            
<div style="padding: 25px 10px 0px 10px;">


<hr style="margin-top:50px; margin-bottom:20px;"/>                       
<h2>River Length &amp; Volume</h2>

<p>It's not so easy to define how long a river is. If a number of tributaries merge to form a larger river, how would you define where the river actually begins? Here, we define river length as the distance to the outflow point from the original headwaters where the name defines the complete length. (Source: Statistical Abstract of the U.S., 1986)</p>



<p style="font-size:12px"><strong>Note:</strong> In order to see the relative lengths of the rivers below, you must have your browser set to 800x600 or higher.</p>


<br />
<p><strong>UNITED STATES</strong></p>



<p><img src="images/riverline.gif" width="254" height="8"> <strong>Missouri</strong>:  2,540 miles<br /><img src="images/riverline.gif" width="234" height="8"> <strong>Mississippi</strong>:  2,340 miles<br /><img src="images/riverline.gif" width="198" height="8"> <strong>Yukon</strong>:  1,980 miles<br /><img src="images/riverline.gif" width="190" height="8"> <strong>Rio Grande</strong>:  1,900 miles<br /><img src="images/riverline.gif" width="190" height="8"> <strong>St. Lawrence</strong>:  1,900 miles<br /><img src="images/riverline.gif" width="146" height="8"> <strong>Arkansas</strong>:  1,460 miles<br /><img src="images/riverline.gif" width="145" height="8"> <strong>Colorado</strong>:  1,450 miles<br /><img src="images/riverline.gif" width="142" height="8"> <strong>Atchafalaya</strong>:  1,420 miles<br /><img src="images/riverline.gif" width="131" height="8"> <strong>Ohio</strong>:  1,310 miles<br /><img src="images/riverline.gif" width="129" height="8"> <strong>Red</strong>:  1,290 miles<br /><img src="images/riverline.gif" width="128" height="8"> <strong>Brazos</strong>:  1,280 miles<br /><img src="images/riverline.gif" width="124" height="8"> <strong>Columbia</strong>:  1,240 miles<br /><img src="images/riverline.gif" width="104" height="8"> <strong>Snake</strong>:  1,040 miles<br /><img src="images/riverline.gif" width="99" height="8"> <strong>Platte</strong>:  990 miles<br /><img src="images/riverline.gif" width="93" height="8"> <strong>Pecos</strong>:  926 miles<br /><img src="images/riverline.gif" width="91" height="8"> <strong>Canadian</strong>:  906 miles<br /><img src="images/riverline.gif" width="87" height="8"> <strong>Tennessee</strong>:  886 miles<br /><img src="images/riverline.gif" width="86" height="8"> <strong>Colorado (Texas)</strong>:  862 miles<br /><img src="images/riverline.gif" width="80" height="8"> <strong>North Canadian</strong>:  800 miles<br /><img src="images/riverline.gif" width="77" height="8"> <strong>Mobile</strong>:  774 miles<br /><img src="images/riverline.gif" width="74" height="8"> <strong>Kansas</strong>:  743 miles<br /><img src="images/riverline.gif" width="72" height="8"> <strong>Kuskokwim</strong>:  724 miles<br /><img src="images/riverline.gif" width="69" height="8"> <strong>Yellowstone</strong>:  692 miles<br /><img src="images/riverline.gif" width="66" height="8"> <strong>Tanana</strong>:  659 miles<br /><img src="images/riverline.gif" width="63" height="8"> <strong>Milk</strong>:  625 miles<br />

<p>
<I>(Source:  Kammerer, J.C., Largest Rivers in the United States,<br />U.S. Geological Survey Fact Sheet OFR 87-242 rev. 1990)</I></p>

<br /><br />

<p><strong>WORLD</strong></p>



<p>Estimates for the length of the world's rivers vary wildly depending on season of the year, who is doing the measuring, the capabilities of the cartographer and his equipment and sources.  However, the biggest cause of disagreeing measurements is the inclusion or exclusion of tributaries.  For example, many sources lump the Mississippi and Missouri Rivers into one river system, making it one of the longest in the world.  The same is true of rivers such as the Ob-Irtysh system in Asia.  Considered as a whole, it is one of the ten longest rivers in the world.  Removing the Irytish drops the Ob down to 15th position&#8212;assuming the rivers ahead of it also weren't measured with massive tributaries included.  Here, we have tried to seperate the major tributaries.  You can easily find other sources that disagree with these numbers; please do not send us further questions on this.</p>


<p>These numbers were taken from the Encyclopedia Britannica and tributaries were seperated out with help from sources like Comptons Encyclopedia and others.</p>

<br />
<p>
<strong>Nile (Africa):</strong> 4,132 miles<br />
<img src="images/riverline.gif" width="413" height="8"><br /><br />

<strong>Amazon (South America):</strong> 4,087 miles<br />
<img src="images/riverline.gif" width="408" height="8"> <br /><br />

<strong>Yangtze (Asia):</strong> 3,915 miles<br />
<img src="images/riverline.gif" width="391" height="8"><br /><br /> 

<strong>Huang He, aka Yellow (Asia):</strong> 3,395 miles<br />
<img src="images/riverline.gif" width="340" height="8"> <br /><br />

<img src="images/riverline.gif" width="303" height="8"> <strong>Parana (South America):</strong> 3,032 miles<br /><img src="images/riverline.gif" width="290" height="8"> <strong>Congo (Africa):</strong> 2,900 miles<br /><img src="images/riverline.gif" width="276" height="8"> <strong>Amur (Asia):</strong> 2,761 miles<br /><img src="images/riverline.gif" width="273" height="8"> <strong>Lena (Asia):</strong> 2,734 miles<br /><img src="images/riverline.gif" width="270" height="8"> <strong>Mekong (Asia):</strong> 2,700 miles<br /><img src="images/riverline.gif" width="264" height="8"> <strong>Mackenzie (Canada):</strong> 2,635 miles<br /><img src="images/riverline.gif" width="260" height="8"> <strong>Niger (Africa):</strong> 2,600 miles<br /><img src="images/riverline.gif" width="254" height="8"> <strong>Yenisey (Russia):</strong> 2,543 miles<br /><img src="images/riverline.gif" width="254" height="8"> <strong>Missouri (United States):</strong> 2,540 miles<br /><img src="images/riverline.gif" width="234" height="8"><strong>Mississippi (United States):</strong> 2,340 miles<br /><img src="images/riverline.gif" width="227" height="8"> <strong>Ob (Russia):</strong> 2,268 miles<br /><img src="images/riverline.gif" width="220" height="8"> <strong>Zambezi (Africa):</strong> 2,200 miles<br /><img src="images/riverline.gif" width="219" height="8"> <strong>Volga (Europe):</strong> 2,193 miles<br /><img src="images/riverline.gif" width="200" height="8"> <strong>Purus (Brazil):</strong> 1,995 miles<br /><img src="images/riverline.gif" width="199" height="8"> <strong>Yukon (United States/Canada):</strong> 1,980 miles<br /><img src="images/riverline.gif" width="190" height="8"> <strong>Rio Grande (United States/Mexico):</strong> 1,900 miles<br /><img src="images/riverline.gif" width="190" height="8"> <strong>St. Lawrence (United States/Canada):</strong> 1,900 miles<br /><img src="images/riverline.gif" width="181" height="8"> <strong>Sao Francisco (Brazil):</strong> 1,811 miles<br /><img src="images/riverline.gif" width="180" height="8"> <strong>Brahmaputra (India):</strong> 1,800 miles<br /><img src="images/riverline.gif" width="180" height="8"> <strong>Indus (India):</strong> 1,800 miles<br /><img src="images/riverline.gif" width="177" height="8"> <strong>Danube (Europe):</strong> 1,770 miles</p>
                            
</div>
                            
                      

						
						<div class="clear"></div> <!-- Allows for content above to be flexible -->
						
						
			<?php 
			// includes the content page bottom		
			include ("includes/content-foot.php") 
			?>			
					
<?php 
// includes the TEMPLATE FOOTER CODING -- </html> 		
include ("includes/footer.php") 
?>
				