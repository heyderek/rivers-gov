<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Gulkana River, Alaska';

// Set the page keywords
$page_keywords = 'Gulkana River, Wrangell Mountains, Alaska';

// Set the page description
$page_description = 'Gulkana River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('49');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?><?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Glennallen Field Office</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The main stem from the outlet of Paxson Lake in T12N, R2W, Copper River meridian, to the confluence with Sourdough Creek. The South Branch of the West Fork from the outlet of an unnamed lake in sections 10 and 15, T10N, R7W, Copper River meridian, to the confluence with the West Fork. The North Branch from the outlet of two unnamed lakes, one in sections 24 and 25, the second in sections 9 and 10, T11N, R8W, Copper River meridian, to the confluence with the West Fork. The West Fork from its confluence with the North and South Branches downstream to its confluence with the main stem. The Middle Fork from the outlet of Dickey Lake in T13N, R5W, Copper River meridian, to the confluence with the main stem.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 181.0 miles; Total &#8212; 181.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/gulkana.jpg" alt="Gulkana River" title="Gulkana River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/ak/st/en/prog/nlcs/gulkana_nwr.html" alt="Gulkana River (Bureau of Land Management)" target="_blank">Gulkana River (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: BLM Alaska</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Gulkana River</h2>

<p>The Gulkana River begins in the Alaska Range near Summit Lake and flows south into the Copper River. The three forks (including Middle Fork and West Fork) comprise the largest clearwater river system in the Copper River Basin, draining approximately 2,140 square miles in Southcentral Alaska before it meets the Copper River and eventually empties into Prince William Sound near Cordova. It is less than a five-hour drive from Fairbanks (population 75,000) and Anchorage (population 250,000), making it among the most popular recreation resources in south-central Alaska.</p>

<p>The three forks of the Gulkana flow through the rolling valleys and low ridges of an upland spruce-dominated forest, rich with lakes and ponds. Several hundred lakes and ponds are scattered throughout the spruce-dominated forest of the Gulkana River watershed, providing abundant nesting areas for trumpeter swans and waterfowl.</p>

<p>The Gulkana River has played an important role in the lives of the Ahtna, providing access to subsistence resources throughout history and pre-history. During winter months, the frozen Gulkana River has been an important travel route from the Copper River to the Tangle Lakes and what is now known as the Denali Highway area.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Gulkana is closely flanked by low rolling hills, with the Wrangell Mountains and Alaska Range in the background, and features high-quality scenic vistas. It offers viewers and photographers opportunities to observe and photograph many aspects of nature&#8212;wild flowers, a variety of birds and animals are all present in abundance.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Gulkana provides a variety of water conditions. The Gulkana is one of a handful of road-accessible rivers in the state of Alaska, but it also provides opportunities for a remote and primitive experience, particularly on the West Gulkana. While the three forks are not considered whitewater rivers for most of their length, each contains rapids, including a quarter mile of Class III-IV rapids in the canyon on the main stem. The corridor provides a remote setting for recreation and subsistence activities, such as boating, fishing, hunting, trapping, camping, hiking, snowmachining, skiing, photography, wildlife viewing and dogsledding. The Sourdough section is accessible to powerboats.</p>

<p><strong><em>Fish</em></strong></p>

<p>The Gulkana is one of the most popular sportfishing rivers in Alaska, providing rich habitat for rainbow trout, arctic grayling, king salmon, red salmon, whitefish, longnose suckers and lamprey. It is the leading king (Chinook) and red (sockeye) salmon spawning stream in the Copper River basin. Grayling, rainbow trout and steelhead are resident species.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Gulkana is home to a great diversity of wildlife and provides outstanding viewing opportunities. The over 60 species of birds in the area include bald eagles and trumpeter swans. More than 30 species of mammals can be found, including black and brown bears, moose, caribou, wolves, martens, wolverines, otters, weasels, minks, foxes, coyotes, lynxes, beavers and muskrats.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>