<div class="mainframe">

	<div id="largephoto">

		<div id="loader"></div>

			<div id="largetrans">

			</div>

		</div>

	</div>

<div id="thumbnails">

<div id="slideshow-text">

<h1>DID YOU KNOW...</h1>
<p>Less than 1/4 of 1% of our rivers are protected under the National Wild & Scenic Rivers System.</p>

</div>

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Owyhee-River-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Owyhee-River.jpg" class="large_image" alt="Owyhee River - Bureau of Land Management" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Old-North-Bridge-Concord-River-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Old-North-Bridge-Concord-River.jpg" class="large_image" alt="Old North Bridge Concord River - Jeanette Runyon" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Alligator-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Alligator.jpg" class="large_image" alt="Alligator - U.S. Fish & Wildlife Service" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Bruneau-River-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Bruneau-River.jpg" class="large_image" alt="Bruneau River - Kevin Lewis" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Trinity-River-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Trinity-River.jpg" class="large_image" alt="Trinity River - Bureau of Land Management" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Beaver-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Beaver.jpg" class="large_image" alt="Beaver - Pat Gaines" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Snake-River-Headwaters-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Snake-River-Headwaters.jpg" class="large_image" alt="Snake River Headwaters - U.S. Forest Service" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Wilson-Creek-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Wilson-Creek.jpg" class="large_image" alt="Wilson Creek - Dan Royall" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Rogue-River-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Rogue-River.jpg" class="large_image" alt="Rogue River - American Rivers" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Kern-River-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Kern-River.jpg" class="large_image" alt="Kern River - John Ellis/American Rivers" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Grizzly-Bear-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Grizzly-Bear.jpg" class="large_image" alt="Grizzly Bear - Judi Len" />

			</div>

		</div>

	</div>

<!--end entry-->

<!-- start entry-->

	<div class="thumbnailimage">

		<div class="thumb_container">

			<div class="large_thumb">

			<img src="images/thumbnails/Upper-Missouri-River-thumb.jpg" class="large_thumb_image" alt="thumb" />

			<img src="images/large/Upper-Missouri-River.jpg" class="large_image" alt="Upper Missouri River - Bureau of Land Management" />

			</div>

		</div>

	</div>

<!--end entry-->

</div>