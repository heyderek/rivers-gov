<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Clarion River, Pennsylvania';

// Set the page keywords
$page_keywords = 'Allegheny National Forest, Clarion River, Pennsylvania';

// Set the page description
$page_description = 'Clarion River, Pennsylvania';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('157');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->


<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Allegheny National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 19, 1996. From the Allegheny National Forest/State Game Lands Number 44 boundary, approximately 0.7 miles downstream from the Ridgway Borough limit, to an unnamed tributary at the backwaters of Piney Dam, approximately 0.6 miles downstream from Blyson Run.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 17.1 miles; Recreational &#8212; 34.6 miles; Total &#8212; 51.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/clarion.jpg" alt="Clarion River" title="Clarion River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/attmain/allegheny/specialplaces/" alt="Clarion River (U.S. Forest Service)" target="_blank">Clarion River (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Clarion River</h2>
<p>Several factors contribute to the special scenic value of the Clarion River. The unique landform (unique in the region of the Allegheny River Basin) of the Clarion River Valley contributes a feeling of intimacy to the river. The sinuous, relatively narrow river valley with steep sides and little floodplain provides little opportunity for long, focused views. The steeply forested hillsides of almost continuous mature deciduous and coniferous vegetation contribute to a feeling of remoteness in many places along the river.</p>
<p>The scenery, the feeling of remoteness, accessibility and the variety of recreation activities possible and ease of canoeing of the Clarion River combine to provide a significant recreation experience in this region. The Clarion River has a longer floating (canoeing and tubing) season than other rivers in the area and is relatively accessible to the public. The Clarion is also an attraction for picnicking, sightseeing, camping, watching wildlife, birding, fishing, biking and hiking. A variety of recreational facilities in the Allegheny National Forest, Cook Forest State Park and Clear Creek State Park support the river-based recreation.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>