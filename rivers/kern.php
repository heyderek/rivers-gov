<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Kern River, California';

// Set the page keywords
$page_keywords = 'Sequoia National Forest, Sequoia National Park, Kern River, Golden Trout Wilderness, Inyo National Forest, Domelands Wilderness, California';

// Set the page description
$page_description = 'Kern River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('64');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Sequoia/Kings Canyon National Parks<br />
U.S. Forest Service, Sequoia National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 24, 1987. The North Fork from the Tulare-Kern County line to its headwaters in Sequoia National Park. The South Fork from its headwaters in the Inyo National Forest to the southern boundary of the Domelands Wilderness in the Sequoia National Forest.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 123.1 miles; Scenic &#8212; 7.0 miles; Recreational &#8212; 20.9 miles; Total &#8212; 151.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/kern.jpg" alt="Kern River" title="Kern River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.fs.usda.gov/recarea/sequoia/recreation/recarea/?recid=79576" alt="Kern River &#8211; U.S. Forest Service" target="_blank">Kern River &#8211; U.S. Forest Service</a></p>
<p><a href="../documents/plans/kern-plan.pdf" alt="Kern River Management Plan (6.9 MB PDF)" target="_blank">Kern River Management Plan (6.9 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: John Ellis, American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Kern River</h2>
<p>The North Fork and South Fork of the Kern Wild and Scenic River is located within a four-hour drive of more than one-third of the population of southern California. With its range of elevation, topography and vegetation, it offers a broad spectrum of recreation opportunities for all seasons of the year. Principal outdoor recreation activities include fishing, hiking, camping and whitewater boating.</p>
<p>The North Fork flows through Sequoia National Park and the Sequoia National Forest, past post-pile formations, spiked-granite protrusions and sharp rock ledges. The North Fork Kern River Canyon within the Golden Trout Wilderness may be the longest, linear glacially-sculpted valley in the world. It contains regionally unique features referred to as Kernbuts and Kerncols. These rounded to elongated (parallel to the axis of the canyon) granitic knobs (Kernbuts) and the depressions between them (Kerncols) were first identified and named in the Kern Canyon.</p>
<p>The North Fork River corridor also includes regionally uncommon wetland habitat at Kern Lakes and the alkaline seep at the Forks of the Kern. The wetland habitat contains several uncommon aquatic and marsh species; the alkaline seep also supports several uncommon plants. The river's deep pool habitat supports a population of wild trout and also vividly colored hybrid trout.</p>
<p>The South Fork Kern River flows through a diverse landscape, including whitewater, waterfalls, large granite outcrops interspersed with open areas, and open meadows with extensive vistas. The segment in the Domeland Wilderness flows by numerous granitic domes and through a rugged and steep granitic gorge where whitewater rapids are common.</p>
<p>With a gradient of 30 feet per mile, the North Fork Kern is one of the steepest and wildest whitewater rivers in North America. The Forks Run is a nearly continuous series of Class IV and V rapids and waterfalls. The Upper Kern is a popular stretch of river for whitewater boating, camping and fishing. The Lower Kern runs 32 miles from Isabella Dam to the canyon mouth above Bakersfield, California.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>