<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Owyhee River (North Fork), Oregon';

// Set the page keywords
$page_keywords = 'North Fork Owyhee River, Oregon';

// Set the page description
$page_description = 'Owyhee River (North Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('91');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Vale District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the Oregon-Idaho state line to its confluence with the Owyhee River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 9.6 miles; Total &#8212; 9.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/owyhee-nf-or.jpg" alt="North Fork Owyhee River" title="North Fork Owyhee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.recreation.gov/recreationalAreaDetails.do?contractCode=NRSO&recAreaId=1834" alt="Owyhee River (Bureau of Land Management)" target="_blank">Owyhee River (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Owyhee River (North Fork)</h2>

<p>The North Fork of the Owyhee River is a tributary of the Owyhee River and is located in Malheur County, Oregon, and Owyhee County, Idaho. Its source is on the east flank of the Owyhee Mountains in Idaho, and it flows generally southwest to meet the main stem Owyhee at Three Forks, Oregon. The nine-mile (14.5 km) segment in Oregon, which flows from the Idaho-Oregon border to the main stem Owyhee River, was designated in 1988 and classified as "wild." The North Fork flows through a deep canyon rimmed with basalt.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Recreational</em></strong></p>

<p>The North Fork of the Owyhee River offers very high-quality backpacking opportunities and excellent early season kayaking for experts on the 12-mile section between North Fork Crossing and Three Forks.  Hunting, camping and wildlife viewing are available in this area.</p>

<p><strong><em>Scenic</em></strong></p>

<p>This designated river corridor contains a subtle diversity of land forms and vegetation. Interesting erosional patterns and colorful rock strata mix with large stands of juniper. Canyon bottoms are overshadowed by steep, rugged canyon walls. Two notable side canyons laced with juniper trees add complexity to the landscape, and the remoteness and solitude found here contribute to the scenic value.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>A community of western juniper provides hiding and thermal cover for wildlife. Mule deer make use of the North Fork Canyon as a migration corridor to wintering ranges in the main Owyhee Canyon. Other species include cougar, pronghorn antelope and white tailed jackrabbit. The abundance of small mammals on the plateau contributes to the large and diverse population of raptors. Numerous bird species utilize the junipers throughout the year. It is common to see the threatened sage grouse, as well as a number of nongame birds.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>