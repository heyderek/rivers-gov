<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wekiva River, Florida';

// Set the page keywords
$page_keywords = 'Wekiva River, Florida';

// Set the page description
$page_description = 'Wekiva River, Florida';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('162');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Southeast Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>October 13, 2000. The Wekiva River from its confluence with the St. Johns River to Wekiwa Springs. Rock Springs Run from its headwaters at Rock Springs to the confluence with the Wekiva Springs Run. Black Water Creek from the outflow from Lake Norris to the confluence with the Wekiva River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 31.4 miles; Scenic &#8212; 2.1 miles; Recreational &#8212; 8.1 miles; Total &#8212; 41.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wekiva.jpg" alt="Wekiva River" title="Wekiva River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/wekiva_pwsr_sub.htm" alt="Wekiva River (National Park Service)" target="_blank">Wekiva River (National Park Service)</a></p>
<p><a href="http://www.wekivawildandscenicriversystem.com" alt="Wekiva River System Advisory Management Committee" target="_blank">Wekiva River System Advisory Management Committee</a></p>
<p><a href="http://www.friendsofwekiva.org" alt="Friends of the Wekiva River" target="_blank">Friends of the Wekiva River</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wekiva River</h2>
<p>The Wekiva River Basin is a complex ecological system of rivers, springs, seepage areas, lakes, streams, sinkholes, wetland prairies, hardwood hammocks, pine flatwoods and sand pine scrub communities. Water quality is exhibited in two ways. Several streams are clear due to being spring-fed. Others are blackwater; blackwater streams receive most of their flow from precipitation resulting in annual rainy season over-bank flows. The Wekiva and its tributaries are in superb ecological condition. The basin is almost entirely within Florida State lands and supports many species of plant and animal life, some of which are endangered, threatened, or of special concern. Elevations range from sea level to about 35 feet above sea level. The climate is subtropical, with an average annual temperature of around 72 degrees. Mean annual rainfall over the Wekiva basin is 52 inches, most of which occurs during the June-October rainy season.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>