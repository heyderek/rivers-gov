<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Zigzag River, Oregon';

// Set the page keywords
$page_keywords = 'Zigzag River, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Zigzag River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('175');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From its headwaters to the Mt. Hood Wilderness boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.3 miles; Total &#8212; 4.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/zigzag.jpg" alt="Zigzag River" title="Zigzag River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/yell/" alt="Yellowstone National Park (National Park Service)" target="_blank">Yellowstone National Park (National Park Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Zigzag River</h2>
<p>The Zigzag River arises from the base of Zigzag Glacier at approximately the 5,000-foot elevation on Mt. Hood in Oregon's Cascade Mountain Range. The 4.3-mile segment of the river from its headwaters to the Mt. Hood Wilderness boundary is administered as a wild river.</p>
<p>The river flows steeply over mud and pyroclastic flows through a sparsely vegetated area in a narrow canyon. The canyon rim itself and beyond is well-forested. There are two waterfalls within the segment adding to the diversity of the river. Intrusive rocks within the corridor are responsible for the waterfalls and other structures. This type of geology is found on other volcanic peaks throughout the region, as well as other locations on Mt. Hood, but is limited essentially to the higher elevations of those peaks, making it relatively unique in comparison to other rivers in the region. The river itself is glacial in origin and has a relatively even flow throughout the year, though it varies during spring runoff and rainfall events.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>