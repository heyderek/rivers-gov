<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Bautista Creek, California';

// Set the page keywords
$page_keywords = 'Bautista Creek, California, San Bernardino National Forest';

// Set the page description
$page_description = 'Bautista Creek, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('203');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, San Bernardino National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the San Bernardino National Forest boundary in Section 36, Township 6 South, Range 2 East to the San Bernardino National Forest boundary in Section 2, Township 6 South, Range 1 East.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 9.8 miles; Total &#8212; 9.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/ausable.jpg" alt="" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/sbnf/recreation/natureviewing/recarea/?recid=74100&actid=62" alt="Bautista Creek (U.S. Forest Service)" target="_blank">Bautista Creek (U.S. Forest Service)</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Bautista Creek</h2>
<p>Bautista Creek drains the San Jacinto Mountains and provides an important migration corridor for birds passing from the desert to the valley. The creek and its surrounding riparian area shelter endangered arroyo toads, southwestern willow flycatchers, Quino checkerspot butterfly, the slender-horned spineflower and many other sensitive species. It contains the largest number of endangered wildlife species on the San Bernardino National Forest.</p>
<p>Famed explorer Juan Bautista de Anza followed the route through what is now Bautista Canyon Road to reach San Francisco, and this is a designated part of a National Historic Trail.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>