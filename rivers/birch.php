<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Birch Creek, Alaska';

// Set the page keywords
$page_keywords = 'Birch Creek, Steese National Conservation Area, Alaska';

// Set the page description
$page_description = 'Birch Creek, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('46');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Fairbanks District Office</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The main stem from the south side of Steese Highway in T7N, R10E, Fairbanks Meridian, downstream to the south side of Steese Highway in T19N, R16E.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 126.0 miles; Total &#8212; 126.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/birch-creek.jpg" alt="Birch Creek" title="Birch Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/ak/st/en/prog/nlcs/birchcrk_nwr.html" target="_blank" alt="Birch Creek (Bureau of Land Management)" target="_blank">Birch Creek (Bureau of Land Management)</a><br />
<a href="../documents/plans/birch-creek-plan.pdf" title="Birch Creek Management Plan" target="_blank">Birch Creek Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Craig McCaa</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Birch Creek</h2>

<p>Birch Creek flows from the windswept ridges and alpine tundra of the Steese National Conservation Area into the broad expanse of the Yukon Flats in central Alaska. The river offers one-week float trips notable not only for scenery and remoteness but for convenience&#8212;floaters can access both ends of the wild river segment from BLM recreation sites along the Steese Highway. Although visited primarily in summer, Birch Creek offers many winter activities for fans of primitive backcountry experiences.</p>

<p>After leaving the designated area, the river continues through state and private lands and the Yukon Flats National Wildlife Refuge for a total of 344 miles before emptying into the Yukon River about halfway between Fort Yukon and Beaver.</p>

<p>Birch Creek was designated as part of the Alaska National Lands Conservation Act in 1980. The outstandingly remarkable values were not identified at the time of designation, but as part of the Eastern Interior Resource Management Plan.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>Birch Creek schist is one of the oldest bedrocks in the state. Although this schist is reported to underlie 70 to 80 percent of the state, much of the original geologic study of this formation was conducted along Birch Creek, hence its namesake. Spectacular examples of this formation are found here in rock outcroppings on adjacent hillsides and in the river, where sheer rock walls have resisted the erosive action of the water. The striations and coloration of this exposed bedrock is striking.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The river provides an exceptional experience for floaters. In only a very few places in the state is such a primitive segment of river accessible by road. While canoeing is the most popular recreational activity, hunting, fishing, trapping, hiking, primitive camping, photography, rock hounding and nature study are all reasons to visit. Popular winter activities along the frozen river include dog mushing, trapping and cross-country skiing. For a few days each February, the Yukon Quest International Sled Dog Race between Fairbanks and Whitehorse attracts dog teams and mushers, and snowmobiling along the creek is popular in late winter and early spring.</p>

<p><strong><em>Scenic</em></strong></p>

<p>For almost its entire length, no mark of man can be seen from along the river, with the exception of several log cabins. The broad canyon lands, patchwork forests and rock outcroppings are outstanding.</p>

<p><strong><em>Vegetation</em></strong></p>

<p>Vegetation within the Birch Creek area ranges from alpine tundra to white spruce-paper birch forests. They are especially noteworthy because existing plant communities reflect little evidence of man's activity.  The mosaic-like nature of the plant associations within the immediate environment reflect past fire history, slope, aspect and the presence or absence of permafrost.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>