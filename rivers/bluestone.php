<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Bluestone River, West Virginia';

// Set the page keywords
$page_keywords = 'National Park Service, Bluestone River, New River Gorge National River, Bluestone State Park, Pipestem State Park, West Virginia';

// Set the page description
$page_description = 'Bluestone River, West Virginia';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('65');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, New River Gorge National River</p>
<br />
<h3>Designated Reach:</h3>
<p>October 26, 1988. From a point two miles upstream of the Summers and Mercer County lines down to Bluestone Lake.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 10.0 miles; Total &#8212; 10.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/bluestone.jpg" alt="Bluestone River" title="Bluestone River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/blue/" alt="Bluestone River (National Park Service)" target="_blank">Bluestone River (National Park Service)</a></p>
<p><a href="http://www.nps.gov/gari/" alt="Gauley River National Recreation Area (National Park Service)" target="_blank">Gauley River National Recreation Area (National Park Service)</a></p>
<p><a href="http://www.nps.gov/neri/" alt="New River Gorge National River (National Park Service)" target="_blank">New River Gorge National River (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Dave Bieri</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Bluestone River</h2>
<p>The lower 10 miles of the designated segment flow through an 800-foot deep gorge and offer warmwater fishing, whitewater boating when water level permits, and hiking along the river on a trail between Bluestone and Pipestem State Parks. Spectacular views of the river gorge may be seen from overlooks at Pipestem. A major portion of the lands are managed by the state to provide hunting opportunities; wild turkey is the featured species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>