<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Nowitna River, Alaska';

// Set the page keywords
$page_keywords = 'Nowitna River, Alaska';

// Set the page description
$page_description = 'Nowitna River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('40');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Fish &amp; Wildlife Service, Nowitna National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment from the point where the river crosses the west limit of T18S, R22E, Kateel River meridian, to its confluence with the Yukon River within the boundaries of the Nowitna National Wildlife Refuge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 225.0 miles; Total &#8212; 225.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/nowitna.jpg" alt="Nowitna River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://nowitna.fws.gov" alt="Nowitna National Wildlife Refuge (U.S. Fish &amp; Wildlife Service)" target="_blank">Nowitna National Wildlife Refuge (U.S. Fish &amp; Wildlife Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Rob Campellone</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Nowitna River</h2>
<p>The Nowitna River&#8212;located within the Nowitna National Wildlife Refuge&#8212;was designated as a wild river by the Alaska National Interest Lands Conservation Act. Of the river's 283-mile length, 223 miles is designated as a wild river, and the river corridor includes approximately 142,400 acres. The river was designated to protect its water quality, wildlife, geology and primitive setting, and like all wild and scenic rivers, to protect its natural, free-flowing condition. With the exception of the 15-mile canyon area, where mountains up to 2,100 feet border the river, the surrounding terrain is flat, or has low, rolling hills.</p>
<p>Numerous lakes and sloughs provide excellent fish and wildlife habitat adjacent to the river. Riparian vegetation consists of willow, alder, aspen, birch and spruce. The abundant fish species that utilize the river include sheefish, whitefish, sucker, northern pike, king salmon, chum salmon, burbot and arctic grayling. The lower portion of the river meanders through one of the most productive waterfowl nesting areas in the state.</p>
<p>The Nowitna River is some 200 miles from the nearest road access and population centers. Although the potential for significant impacts to the river exist, for the near future the U.S. Fish &amp; Wildlife Service expects limited increases in public use or other activities such as gold mining that may affect water quality and the integrity of the wild river designation. The Nowitna Wild River is an excellent example of a free-flowing, interior-Alaska river that has seen limited development and which is expected to remain largely unchanged for many years into the future.</p>
<p>The Nowitna River continues to provide many of the same opportunities, and in much the same way, as it has for many years. Trapping is a major public use&#8212;mainly marten, beaver, wolf, fox and wolverine throughout the winter. Trappers often spend much of the winter living in small cabins and working traplines that can extend for 300 miles. Nine trapline cabins&#8212;generally less than 250 square feet&#8212;are located within the river corridor. The river is also used extensively to hunt moose; hunting takes place during September and accounts for approximately 1,000 days of public use in the river corridor. One or two commercial guides provide fishing and floating trips on short segments of the river each summer. Several gold mines operate each summer in tributaries flowing into the Nowitna. However, activity at the mines, and the number in operation, varies according to the price of gold. Access to the river is mainly by river boat or float plane in the summer. Although an occasional dog sled is used, most winter travel takes place with snowmachine and airplanes equipped with skis.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>