<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wallowa River, Oregon';

// Set the page keywords
$page_keywords = 'Wallowa River, Grande Ronde River, Oregon';

// Set the page description
$page_description = 'Wallowa River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('SD18');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Vale District<br />
Oregon Parks & Recreation Department</p>
<br />
<h3>Designated Reach:</h3>
<p>July 23, 1996. The segment of the Wallowa River from the confluence of the Wallowa and Minam Rivers in the hamlet of Minam downstream to the confluence of the Wallowa and the Grande Ronde Rivers.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 10.0 miles; Total &#8212; 10.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wallowa.jpg" alt="Wallowa River" title="Wallowa River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/grande-ronde-wallowa-boating-info-2018.pdf" alt="2018 Grande Ronde &amp; Wallowa Rivers Boating Information" title="2018 Grande Ronde &amp; Wallowa Rivers Boating Information" target="_blank">2018 Grande Ronde &amp; Wallowa Rivers Boating Information</a> &#8212; Bureau of Land Management Information</p>
<p><a href="https://www.fs.usda.gov/detail/umatilla/recreation/wateractivities/?cid=fsbdev3_062366" alt="Wallowa River (U.S. Forest Service)" target="_blank">Wallowa River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/wallowa-grande-ronde-plan-ea.pdf" title="Wallowa &amp; Grande Ronde Rivers Management Plan" target="_blank">Wallowa &amp; Grande Ronde Rivers Management Plan (Bureau of Land Management &amp; U.S. Forest Service)</a></p>
<p><a href="../documents/grande-ronde-wallowa-trip.pdf" target="_blank">Boating the Grande Ronde &amp; Wallowa Rivers (Idaho Statesman)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wallowa River</h2>

<p>The Wallowa River begins at the confluence of its east and west forks and flows generally northwest through the Wallowa Valley in northeastern Oregon. From the confluence of the Minam and Wallowa Rivers at Minam, Oregon, to its confluence with the Grande Ronde River, the Wallowa is the gateway to the wild and scenic Grande Ronde River. Approximately 10 miles in length, the river is classified as recreational. It offers incredible fishing, hunting, wildlife viewing and floatboating, as well as a state park for camping.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Grande Ronde River is a nationally renowned sport fishery, one of the top three in the region. The mainstem and its major tributaries provide spawning and rearing habitat for wild and hatchery stock of spring Chinook, fall Chinook, summer steelhead and rainbow trout. Fishing is excellent even late in the season after the water levels have receded.</p>

<p><strong><em>Recreational</em></strong></p>

<p>There are many recreational opportunities on the Grande Ronde and Wallowa Rivers. Those judged to be exceptional in quality include anadromous and resident fishing; floating (rafting, canoeing and kayaking for overnight use); camping; and big game viewing and hunting. Visitors are able to enjoy an unusually long float season for a free-flowing river, from ice breakup in the spring to freeze up in the fall. Trips offer a rare multiple day float for those with beginner and intermediate skills.</p>

<p>The primary launch site for the Wallowa and Grande Ronde rivers, as well as the Bureau of Land Management river station, are located on state lands at Minam on the Wallowa River. Additional access points include Mud Creek, Troy and Boggan's Oasis. Primitive campsites along the river are on a first-come first-served basis. Many portions of the river are roadless and primitive with limited access by vehicles.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Grande Ronde River and Wallowa River corridors feature an unusual diversity of landforms and vegetation that progress from largely forested vistas to forested stringers&#8212;patches of residual pre-fire forest, separated by native bunchgrass slopes.  River users see a largely untouched viewshed in the upper river reach, while the lower portion flows through open, grass covered hills with forested pockets and tributary canyons.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The area hosts an exceptional diversity of species, in part because the river corridor provides critical wintering habitat for bighorn sheep, elk, mule deer and whitetail deer. Others contributing to the impressive viewing opportunities include black bears, cougars and mountain goats. The river corridor also serves as a sensitive wintering area for bald eagles.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>