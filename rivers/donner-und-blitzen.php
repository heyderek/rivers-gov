<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Donner und Blitzen River, Oregon';

// Set the page keywords
$page_keywords = 'Malheur National Wildlife Refuge, Burns District, Bureau of Land Management, Donner und Blitzen River, Oregon';

// Set the page description
$page_description = 'Donner und Blitzen River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('74');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Burns District Office</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988, and October 30, 2000. October 28, 1988: From its headwaters to the confluence with the South Fork Blitzen and Little Blitzen, including the tributaries: Little Blitzen River, South Fork Blitzen River, Big Indian Creek, Little Indian Creek and Fish Creek. October 30, 2000: Mud Creek from its source to its confluence with the Donner und Blitzen River; Ankle Creek from its headwaters to its confluence with the Donner und Blitzen River; and the South Fork of Ankle Creek from its source to its confluence with Ankle Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>October 28, 1988: Wild &#8212; 72.7 miles; Total &#8212; 72.7 miles. October 30, 2000: Wild &#8212; 14.8 miles; Total &#8212; 14.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/donner-und-blitzen.jpg" alt="Donner und Blitzen River" title="Donner und Blitzen River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.outdoorproject.com/adventures/oregon/hikes/donner-und-blitzen-river-trail-hike" alt="Donner and Blitzen River Trail (Bureau of Land Management)" target="_blank">Donner und Blitzen River Trail (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/donner-und-blitzen-plan-ea.pdf" title="Donner und Blitzen River Management Plan" target="_blank">Donner und Blitzen River Management Plan</a></p>
<p><a href="http://www.wildhorse.php" alt="Wildhorse & Kiger Creeks" target="_blank">Wildhorse & Kiger Creeks</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Donner und Blitzen River</h2>

<p>From its headwaters to the Malheur National Wildlife Refuge boundary, the Donner und Blitzen River is located  in the eastern Oregon high desert.  Although much of its course is through marsh, it offers scenic glaciated canyons and unique ecosystems. There is an exceptional wild trout fishery. Named by soldiers of German origin, the Donner und Blitzen River translates as "thunder and lightning" for a thunderstorm the soldiers experienced as they crossed the river.</p>

<p>The Donner und Blitzen River system also includes several spring-fed tributaries&#8212;South Fork Blitzen River, Little Blitzen River, Big Indian Creek, Little Indian Creek, Fish Creek, Mud Creek, and Ankle Creek&#8212;totaling 87.5 miles (140.8 km) of designated river.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural (Historic Sites)</em></strong></p>

<p>Historic sites and resources on Steens Mountain, including the river corridor, reflect the turn-of-the-century period of settlement, homesteading and livestock raising.</p>

<p>Located on the Little Blitzen River is the Riddle Brothers Ranch National Historic District, part of the historic context of pioneer settlement and the development of the livestock industry in the American West. Migrating from western Oregon, three bachelor brothers settled there in the early 1900s and built their ranch by gaining control of water in the area. They sold the ranch in the 1950s, and in 1986 the BLM purchased the property to manage the ranch for its historic value.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Blitzen River supports a wild, native redband trout population and the Malheur mottled sculpin, both listed as sensitive species. It is recognized by anglers as one of Oregon's finest wild trout streams. Fish species in the Blitzen River above Page Springs Dam are redband trout, mountain whitefish, longnose dace and mottled sculpin. The redband trout is the species most commonly found in the system, indicating the presence of good stream habitat, water quality and quantity.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Surface rock is predominantly Steens Basalt&#8212;thin, multiple flows of basalt approximately 15 million years old and several thousand feet in thickness. Thin patches of rhyolite ash-flow tuffs occur sporadically.</p>

<p>The area provides textbook examples of a U-shaped glaciated canyon. Steens Mountain is the northern-most uplifted fault block in the state of Oregon, with well-defined escarpments and graben valleys. It is unique in the region because its upper 2,000 feet were shaped during the ice age as glaciers formed in the creek and streambeds, and the intense weight of the snowcap caused depressions on the surface of the mountains. Over 2,500 was carved and gouged down to a layer of very hard basalt. A second glacial advance in the upper sections of the gorges and scarp created smaller hanging valleys in the tallest areas of the existing gorges.</p>

<p><strong><em>Pre-Historic</em></strong></p>

<p>The river corridor was used by Native Americans, including the Northern Paiute and their predecessors. The area was used for hunting, fishing and gathering plants for food. Prehistoric sites are known to be present in portions of the river system, and conditions for prehistoric site potential exist throughout the corridor.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Steens Mountain is a destination for two- to four-day backpacking or horsepack experiences. Exceptionally high-quality recreation also includes fishing, and one section is designated catch-and-release in one section to protect the population of native fish. Other popular activities are hunting and the viewing and photography of wildlife and scenery.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The river and its tributaries pass through several vegetation zones which are the result of temperature, elevation and precipitation. The lower area features sagebrush and bunchgrass, and the upper subalpine zone offers glaciated canyons and deep basalt formations whose viewsheds are largely untouched and in their natural condition.</p>

<p><strong><em>Vegetation</em></strong></p>

<p>The Blitzen and its tributaries host a great variety of plant communities and a large number of sensitive species. Riparian zones are dominated by willows, western birch, mountain alder, black cottonwood and quaking aspen, as well as other species. Sedge and grass-dominated meadows, bog areas, seeps wetland communities, subalpine and alpine communities are found in the system. The uplands include areas dominated by big sagebrush, western juniper, mountain mahogany, quaking aspen and mountain snowberry, and understory species include Idaho fescue, bluebunch wheatgrass and needlegrasses.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Blitzen River drainage is highly valued for its abundant wildlife. The river area and adjacent uplands are used by an estimated 250 wildlife species. Mule deer winter in the lower four miles of the Blitzen River and the lower four miles of Fish Creek and summer in upper areas. Rocky Mountain elk and pronghorn antelope are also present.</p>

<p>Raptors, such as the American kestrels, and great horned owls nest along canyon rims of the Blitzen River and its tributaries, and the cliffs are also home to nesting turkey vultures and ravens. Sage grouse stay in the upper, flatter terrain, and chukars and valley quail are found along the river.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>