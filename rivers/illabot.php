<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Illabot Creek, Washington';

// Set the page keywords
$page_keywords = 'Illabot Creek, Washington, Mt. Baker-Snoqualmie National Forest';

// Set the page description
$page_description = 'Illabot Creek, Washington';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('211');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Baker-Snoqualmie National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 19, 2014. From the headwaters of Illabot Creek to approximately two miles upstream from its confluence with the Skagit River and just south of the Rockport-Cascade Road. The actual terminus is depicted on the map titled ‘Illabot Creek Proposed WSR–Northern Terminus’, dated September 15, 2009. (We are trying to get a better description, one that can be found on a map. In the meantime, here is a map <a href="http://www.americanrivers.org/assets/pdfs/wild-and-scenic-rivers/illabot-map.pdf?c8031c" target="_blank" title="Illabot Creek Map")</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.3 miles; Scenic &#8212; 10.0 miles; Total &#8212; 14.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/illabot-creek.jpg" alt="Illabot Creek" title="Illabot Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/westfield_pwsr_sub.html" alt="Westfield River (National Park Service)" target="_blank">Westfield River (National Park Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Phil Kincare, U.S. Forest Service</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Illabot Creek</h2>
<p>Illabot Creek provides exceptional spawning and rearing habitat for summer and fall Chinook, coho, chum and pink salmon; native steelhead; and one of the largest populations of bull trout in the Skagit River watershed. Puget Sound Chinook, steelhead and bull trout are listed under the Endangered Species Act. Illabot Creek also supports the highest density of chum and pink salmon in the Skagit River watershed and provides habitat for wintering bald eagles. Eagles using the Illabot roost are a part of one of the largest concentration of wintering bald eagles in the continental United States.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>