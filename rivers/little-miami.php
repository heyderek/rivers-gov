<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Little Miami River, Ohio';

// Set the page keywords
$page_keywords = 'Little Miami River, Ohio';

// Set the page description
$page_description = 'Little Miami River, Ohio';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('SD2','SD6');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Ohio Department of Natural Resources, Division of Watercraft</p>
<br />
<h3>Designated Reach:</h3>
<p>August 20, 1973, and January 11, 1981. From State Highway 72 at Clifton to the Ohio River, including the lower two miles of Caesars Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>August 20, 1973: Scenic &#8212; 18.0 miles; Recreational &#8212; 48.0 miles; Total &#8212; 66.0 miles. January 11, 1981: Recreational &#8212; 28.0 miles; Total &#8212; 28.0 miles. Aggregate Totals: Scenic &#8212; 18.0 miles; Recreational &#8212; 76.0; Total &#8212; 94.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/little-miami.jpg" alt="Little Miami River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://watercraft.ohiodnr.gov/scenic-rivers/list-of-ohios-scenic-rivers/little-miami" alt="Little Miami River (Ohio Department of Natural Resources)" target="_blank">Little Miami River (Ohio Department of Natural Resources)</a></p>
<p><a href="http://www4.wittenberg.edu/academics/hfs/tmartin/LittleMiami/LittleMiamiRiver.htm" alt="Canoeing the Little Miami" target="_blank">Canoeing the Little Miami</a></p>
<p><a href="../documents/plans/little-miami-plan.pdf" target="_blank">Little Miami River Management Plan (14.5 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Sue Jennings</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Little Miami River</h2>
<p>The Little Miami originates near Clifton Gorge State Nature Preserve and National Natural Landmark. Flowing through a deep gorge, wooded bluffs, and rolling farmlands, it is located between Dayton and Cincinnati, placing it within an hour's drive of over three million people.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>