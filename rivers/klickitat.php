<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Klickitat River, Washington';

// Set the page keywords
$page_keywords = 'Columbia River, Columbia George National Scenic Area, Klickitat River, Washington';

// Set the page description
$page_description = 'Klickitat River, Washington';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('60');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Columbia Gorge National Scenic Area</p>
<br />
<h3>Designated Reach:</h3>
<p>November 17, 1986. From the confluence with Wheeler Creek, near the town of Pitt, to the confluence with the Columbia River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 10.8 miles; Total &#8212; 10.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/klickitat.jpg" alt="Klickitat River" title="Klickitat River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/crgnsa/specialplaces/?cid=stelprdb5182064" alt="Klickitat River (U.S. Forest Service)" target="_blank">Klickitat River (U.S. Forest Service)</a></p>
<p><a href="http://www.klickitat-trail.org" target="_blank" alt="Klickitat Trail" target="_blank">Klickitat Trail</a></p>
<p><a href="../documents/plans/klickitat-plan.pdf" title="Klickitat River Management Plan" target="_blank">Klickitat River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Jacquie Moreau</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Klickitat River</h2>
<p>The Klickitat River, located in south-central Washington, flows generally south from its origin on Mt. Adams in the high country of the Yakama Indian Reservation to its confluence with the Columbia River in the Columbia River Gorge. The designated segment is the lowermost 10.8 miles of the river. At the upper end of this segment, the river flows through a broad canyon. As it drops toward the Columbia at a steady gradient of 26 feet per mile, the canyon tightens and small rapids spike the channel.</p>
<p>At about river mile 2.5, the Klickitat drops into a tight, rock-walled gorge. The water cascades and crashes through the rocky channel where the tribes and bands of the Yakama Nation have used dip-net fishing continuously for generations to catch salmon and steelhead. Of the mid-Columbia tributaries, the Klickitat is one of the favored fishing sites, due to both the number of fish and the narrow canyon with its high water volume.</p>
<p>In addition to the river's outstanding hydrology, the geology of the gorge between river mile 1.1 and 2.5, and the dip-net fishing sites, the river is also the most significant anadromous fishery on the Washington side of the Columbia in the stretch from Bonneville Dam to the Snake River. It supports steelhead trout, Chinook salmon and coho salmon, with six distinct runs.</p>
<p>The lower Klickitat offers a variety of recreation opportunities, including boating, fishing, hiking, camping and sightseeing. Boat fishing is popular when the salmon and steelhead are running. There is an undeveloped boat put-in/take-out on Klickitat County Park land just below the Pitt bridge and river access at several places along Highway 142, including a Washington Department of Fish and Wildlife fee camp site Turkey Hole. The take-out is before the fish screw trap at about river mile 5, just above the Klickitat canyon gorge. The newly operating Fishway and Research Facility at the top of Lyle Falls requires boaters to take out at this point. The first drop, Lyle Falls, is a class 5+ and combined with the narrow canyon beyond is deadly to almost all who miss the take out. The falls also marks the beginning of the tribal in-lieu fishing sites and no boating is allowed through this area.</p>
<p>Permits are only required for commercial outfitters; existing commercial outfitters include beginning kayak schools, fishing guides and early season limited rafting.</p>
<p>The Spokane, Portland and Seattle Railroad built a railway linking Lyle and Goldendale in 1903. This branch line was abandoned in 1992 and is now the Klickitat Rails-to-Trail. The trail parallels the river's east bank from the Columbia River to Fisher Hill Bridge, where it crosses to the west bank and continues to the town of Pitt. It crosses Highway 142 and continues along the west bank leaving the wild and scenic river portion traversing many more miles upriver.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>