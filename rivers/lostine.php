<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Lostine River, Oregon';

// Set the page keywords
$page_keywords = 'Wallowa-Whitman National Forest, Eagle Cap Wilderness, Lostine River, Oregon';

// Set the page description
$page_description = 'Lostine River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('82');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters in the Eagle Cap Wilderness to the Wallowa-Whitman National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 5.0 miles; Recreational &#8212; 11.0 miles; Total &#8212; 16.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/lostine.jpg" alt="Lostine River" title="Lostine River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="../documents/plans/lostine-plan.pdf" title="Lostine River Management Plan" target="_blank">Lostine River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Lostine River</h2>
<p>The Lostine River is located in northeast Oregon on the Wallowa-Whitman National Forest. Originating from Minam Lake in the Eagle Cap Wilderness, the river flows through a glaciated, U-shaped valley and is surrounded by mountain meadows and high mountain peaks. The upper five miles of the designated segment, within the wilderness, are classified as wild, and the lower 11 miles are classified as recreational. The river's outstandingly remarkable values include recreation, scenery, fisheries, wildlife and vegetation/botany.</p>
<p>Visitors can access the river in a variety of ways. The Two Pan Trailhead and provides the best hike along the upper section in the Eagle Cap Wilderness. Forest Service roads provide access to most segments below the wilderness boundary. Camping along the river is available at seven developed campgrounds and several dispersed campsites. Most of the day-use trailheads and campgrounds in the corridor are fee sites with on-site payment facilities.</p>
<p>The river corridor supports a diversity of wildlife habitats and species, including Rocky Mountain elk, deer, black bear, wolf, mountain lion, beaver, otter, mink and other small mammals. Rocky Mountain bighorn sheep, indigenous to the Eagle Cap Wilderness, have been reintroduced in the Hurricane Creek-Lostine River drainage. Peregrine falcons, bald eagles and a large variety of other birds inhabit the area. The river supports spring and fall Snake River Chinook salmon (listed as threatened under ESA), steelhead and bull trout.</p>
<p>The unique area is home to numerous proposed, endangered, threatened and sensitive species of plants. These include 11 species of moonwart and the Northern twayblade. The rarity of finding so many moonwarts in one locality provides the opportunity for scientific research and a delight for botanists.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>