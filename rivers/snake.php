<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Snake River, Idaho, Oregon';

// Set the page keywords
$page_keywords = 'Snake River, Idaho, Hells Canyon, Hells Canyon National Recreation Area, Oregon';

// Set the page description
$page_description = 'Snake River, Idaho and Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('12');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<style type="text/css">
.canyon {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12pt;
	font-style: italic;
	font-weight: bold;
	color: #2361AA;
	background-color: #EDE9DE;
	border: thin solid #039;
	text-align: justify;
	padding: 10pt;
	float: right;
	height: auto;
	width: 30%;
	margin-right: 0pt;
	margin-bottom: 10pt;
	margin-left: 10pt;
	margin-top: 0pt;
}
</style>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 1, 1975. The segment from Hells Canyon Dam downstream to an eastward extension of the north boundary of section 1, T5N, R47E, Willamette meridian.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 32.5 miles; Scenic &#8212; 34.4 miles; Total &#8212; 66.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/snake.jpg" alt="Snake River" title="Snake River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5238987/" alt="Hells Canyon National Recreation Area (U.S. Forest Service)" target="_blank">Hells Canyon National Recreation Area (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="http://www.fs.usda.gov/detail/scnf/passes-permits/recreation/?cid=fsbdev3_029568/" alt="Snake River Lottery" target="_blank">Snake River Lottery</a></p>
<p><a href="http://www.blm.gov/id/st/en/fo/cottonwood/recreation_sites_/heller_bar.html" alt="Heller Bar Boat Launch (Bureau of Land Management)" target="_blank">Heller Bar Boat Launch (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/snake-recreation-plan.pdf" title="Snake River Recreation Plan" target="_blank">Snake River Recreation Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">

<div class="canyon">
<div style="font-size:14pt; font-weight:bold; color:#00592C; text-align:center">Is Hells Canyon The Deepest?</div>

<p>It's often claimed that Hells Canyon is the deepest canyon in the United States. But is it? It all depends on how you define a canyon and where you measure it. The highest point above the Snake River is He Devil Mountain in Idaho, a bit less than 7,900 feet above the water and 6 miles away. The other side of the Snake River is Bear Mountain in Oregon, which is only around 5,400 feet.</p>

<p>By comparison, Spanish Mountain rises 8,240 feet above the Kings River in California and is only 4 miles away. The highest point on the other side of the river, coincidentally, is also around 5,400 feet. And again, depending on where you measure and how you define a canyon, several sections of the Middle and South Forks of the Kings River have an average rise of 6,500 to 7,700 feet.</p>

<p>So why is there a dispute over whether Kings Canyon or Hells Canyon is deeper? Kings Canyon is a much more open landscape; Hells Canyon 'feels' like a canyon. But as someone pointed out, hey, the National Park Service calls it Kings Canyon National Park for a reason.</p>

<p>By the way, the Grand Canyon&#8212;which no one would dispute as a canyon&#8212;"only" reaches a depth of 6,093 feet from the river to the North Rim.</p>
</div>

<h2>Snake River</h2>
        
<p>The Snake River likely got its name from the first European explorers who misinterpreted the sign made by the Shoshone people who identified themselves in sign language by moving the hand in a swimming motion which appeared to these explorers to be a "snake." It actually signified that they lived near the river with many fish. In the 1950's, the name "Hells Canyon" was borrowed from Hells Canyon Creek, which enters the river near what is now Hells Canyon Dam.</p>

<p>The Hells Canyon area was once home to Shoshone and Nez Perce tribes. According to the Nez Perce tribe, Coyote dug the Snake River Canyon in a day to protect the people on the west side of the river from the Seven Devils, a band of evil spirits living in the mountain range to the east. In the late nineteenth century, the military drove the Native Americans out and settlers began ranching and mining in the canyon. Today, boaters can explore archaeological sites and old homesteads, all part of the canyon's rich, colorful history.</p>

<p>Hells Canyon is one of the most imposing river gorges in the West. Until a million years ago, the Owyhee Mountains acted as a dam between the Snake River and its current confluence with the Columbia River, creating a vast lake in what is now southwestern Idaho. When the mountains were finally breached, the Snake roared northward, cutting a giant chasm through the volcanic rock. The resulting canyon, roughly ten miles across, is not as dramatic as the Grand Canyon. However, when the surrounding peaks are visible from the river, the sense of depth is tremendous. The adjacent ridges average 5,500' above the river. He Devil Mountain, tallest of the Seven Devils (9,393') towers almost 8,000' above the river, creating the deepest gorge in the United States.</p>

<p>The river is as big as the landscape. Below Hells Canyon Dam, the Snake usually carries more water than the Colorado River through the Grand Canyon. Below the confluence with the Salmon River, flows average 35,000 cfs and often peak over 100,000 when the Salmon is high. Further downstream, the Clearwater and other rivers dump their flows into the Snake River, creating the Columbia River's largest tributary. (The total drainage area is approximately the size of Oregon.)</p>

<p>The outstandingly remarkable values of the Wild and Scenic Snake River are scenery, recreation, geology, wildlife, fisheries, cultural resources, vegetation/botany and ecology.</p>

<p>River recreational use is limited for all user groups (private float and power boat; commercial float and power boats) for each segment of river and within primary and secondary use seasons. Permits are required yearlong for use on the river by float or power boats and are available through www.rec.gov.com.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>