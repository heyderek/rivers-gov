<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Deschutes River, Oregon';

// Set the page keywords
$page_keywords = 'Deschutes National Forest, Prineville District, Deschutes River, Odgen Falls, Lake Billy Chinook, Oregon';

// Set the page description
$page_description = 'Deschutes River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('73');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Prineville District<br />
U.S. Forest Service, Deschutes National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Wikiup Dam to the Bend Urban Growth boundary at the southwest corner of Section 13, T18S, R11E. From Odin Falls to the upper end of Lake Billy Chinook. From the Pelton Reregulating Dam to the confluence with the Columbia River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 31.0 miles; Recreational &#8212; 143.4 miles; Total &#8212; 174.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/deschutes.jpg" alt="Deschutes River" title="Deschutes River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/visit/lower-deschutes-wild-scenic-river" alt="Deschutes River (Bureau of Land Management)" target="_blank">Deschutes River (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/lower-deschutes-rod.pdf" target="_blank">Deschutes (Lower) River Management Plan Record of Decision, Oregon</a></p>
<p><a href="../documents/plans/lower-deschutes-plan-supplement-allocation-system.pdf" target="_blank">Deschutes (Lower) River Supplemental Allocation System Plan, Oregon</a></p>
<p><a href="../documents/plans/middle-deschutes-lower-crooked-plan.pdf" target="_blank">Deschutes (Middle) River Management Plan, Oregon</a></p>
<p><a href="../documents/plans/upper-deschutes-plan.pdf" alt="(Upper) Deschutes River Management Plan (10.2 MB PDF)" target="_blank">(Upper) Deschutes River Management Plan (10.2 MB PDF)</a></p>
<p><a href="../documents/plans/upper-deschutes-eis.pdf" alt="(Upper) Deschutes River Management Plan Environmental Impact Statement (11.3 MB PDF)" target="_blank">(Upper) Deschutes River Management Plan Environmental Impact Statement (11.3 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Gary Marsh</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Deschutes River</h2>

<p>The Deschutes River is located in central Oregon. It provides much of the drainage on the eastern side of the Cascade Range on its way to its confluence with the Columbia River. The Deschutes was an important resource for  thousands of years for Native Americans and in the 19th century for pioneers on the Oregon Trail.</p>

<p>The Deschutes features ruggedly beautiful scenery, outstanding whitewater boating and a renowned sport fishery for steelhead, brown trout and native rainbow trout. The upper Deschutes River is highly accessible, offers an exceptional brown trout fishery, and is close to Bend, Oregon, adjacent to the Newberry National Volcanic Monument. The middle Deschutes River has excellent hiking opportunities with spectacular geologic formations and waterfalls. The lower Deschutes River offers the greatest opportunities for whitewater rafting and is one of Oregon's premier steelhead and trout fisheries.</p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Middle Deschutes Outstandingly Remarkable Values</p>

<p><strong><em>Botany/Ecology</em></strong></p>

<p>The middle Deschutes River segments are in an ecological condition unusual for similar areas within the region and contain a significant portion of Estes' wormwood.</p>

<p><strong><em>Cultural</em></strong></p>

<p>Cultural resources on the middle Deschutes River include prehistoric and historic sites found along the corridor and traditional uses associated with the area. Evidence that rare and/or special activities took place in the river canyon areas is represented by lithic scatters or flaking stations, shell middens, rock shelters, rock features and rock art. These sites have the potential to contribute to the understanding and interpretation of the prehistory of the Deschutes River and the region and are considered to eligible for inclusion in the National Register of Historic Places.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>Surveys have identified fishing as the number one recreation activity in the upper sections. Stories and pictures of huge catches are found in historical records of the early 1900's.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Fifty million years of geologic history are dramatically displayed on the canyon walls of the middle Deschutes River and lower Crooked Rivers. Volcanic eruptions which occurred over thousands of years created a large basin dramatized by colorful layers of basalt, ash and sedimentary formations. The most significant contributor to the outstandingly remarkable geologic resource are the unique intra-canyon basalt formations created by recurring volcanic and hydrologic activities.</p>

<p><strong><em>Hydrology</em></strong></p>

<p>Water from springs and stability of flows through the steep basalt canyons has created a stream habitat and riparian zone that is extremely stable and diverse, unique in a dry semi-arid climate environment. Features, such as Odin, Big and Steelhead Falls; springs and seeps; white water rapids; water sculpted rock; and the river canyons, are very prominent and represent excellent examples of hydrologic activity within central Oregon.</p>

<p><strong><em>Recreational</em></strong></p>

<p>These river corridors offer a diversity of year-round, semi-primitive recreation opportunities, such as fishing, hiking, backpacking, camping, wildlife and nature observation, expert kayaking and rafting, picnicking, swimming, hunting and photography. Interpretive opportunities are exceptional and attract visitors from outside the geographical area.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The exceptional scenic quality along the middle Deschutes River is due to the rugged natural character of the canyons, outstanding scenic vistas, limited visual intrusions and scenic diversity resulting from a variety of geologic formations, vegetation communities and dynamic river characteristics. These canyons truly represent the spectacular natural beauty created by various forces of nature.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The river corridor supports critical mule deer winter range habitat and nesting/hunting habitat for bald eagles, golden eagles, ospreys and other raptors. Bald eagles are known to winter along the Deschutes River downriver from Lower Bridge and also within the lower Crooked River segment. Outstanding habitat areas include high vertical cliffs, wide talus slopes, numerous caves, pristine riparian zones, and extensive grass/sage covered slopes and plateaus.</p>

<p>&nbsp;</p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Lower Deschutes Outstandingly Remarkable Values</p>

<p><strong><em>Botany</em></strong></p>

<p>The variety of plant communities in the Deschutes River Canyon fall into four broad categories; the high desert uplands host big sagebrush, juniper-big sagebrush and bunchgrass types, and the riparian vegetation along the river is dominated by alders.</p>

<p><strong><em>Cultural - Pre-History</em></strong></p>

<p>Humans have occupied the Deschutes Canyon area for at least 10,000 years. One hundred thirty-five prehistoric sites have been recorded in the lower Deschutes River Canyon, and it is believed that many others will yet be found. Most common are habitation sites. One of these, at Macks Canyon Campground, was excavated by University of Oregon archaeologists in the late 1960s and is now listed on the National Register of Historic Places. Sherars Falls, a point of difficult passage for anadromous fishes, is an important traditional fishing station for Native Americans.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The lower Deschutes provides extensive spawning and rearing habitat for both resident fish, such as rainbow trout, and anadromous steelhead and chinook salmon. There is also a regionally unique run of wild sockeye salmon that is sustained by the incidental passage of kokonee smolts through the turbines at the Pelton/Round Butte hydroelectric complex. Runs of anadromous fish sustain an important subsistence fishery for Native Americans.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Deschutes River flows through the Deschutes-Umatilla Plateau, the main part of which slopes northward from 4,000-foot levels in the mountains of Central Oregon to a 400-foot elevation along the Columbia River. The rocks are mostly Columbia River basalt, nearly 2,000 feet thick. The lava flows that make up the plateau occurred over millions of years and formed in distinct layers of various thickness.</p>

<p><strong><em>Historic</em></strong></p>

<p>Exploration and fur trapping by Euro-Americans began in the Deschutes Canyon in the early 19th century. Other historic activities that have been documented include use of the Oregon Trail, road and railroad construction and settlement. In the Deschutes Canyon, 38 historic sites have been documented, most of them associated with early railroad construction.</p>

<p><strong><em> Recreation</em></strong></p>

<p>The lower Deschutes River is central Oregon's playground, an ideal location for outdoor sports, from whitewater paddling and swimming to hiking and biking. The river provides a stable, high-volume flow, available for recreation all year long, and it has been internationally known for its excellent fishery for many years. One hundred river miles offer segments favored for relaxed, overnight camping and fishing floats, one-day whitewater adventures, and guided or non-guided fishing trips. Notable activities include following the Lower Deschutes River Back Country Byway along the river to campgrounds at Beavertail and Macks Canyon or the accessible fishing ramp at Blue Hole.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The lower Deschutes River Canyon contains a diversity of landforms, vegetation and color. The river, having carved a canyon nearly 2,000 feet deep in many locations out of rugged Columbia River basalt flows, provides a dramatic and diverse landscape. The clear water of the river framed by the green riparian vegetative fringe creates a stark contrast to the often barren and broken reddish and brown cliffs and hillsides of the canyon. The river provides a boater with a moving platform for viewing the ever-changing scene. While transportation corridors exist (roads and railroads) and occupational and rural development have occurred in several areas, they are overshadowed by the magnitude and beauty of the river and canyon character.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Deschutes River Canyon provides habitat for approximately 300 different species of wildlife. Most of these utilize riparian habitats adjacent to the river. This provides outstanding opportunities for viewing many species of wildlife including songbirds, waterfowl, mink, heron, mule deer and many reptiles, amphibians and other small and large mammals. Notable species are the bald eagle, peregrine falcon, osprey, Dalles sideboard snail and shortface lanx.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>