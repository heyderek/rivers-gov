<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Kobuk River, Alaska';

// Set the page keywords
$page_keywords = 'Gates of the Arctic National Park, Kobuk River, Endicott Mountains, Walker Lake, Alaska';

// Set the page description
$page_description = 'Kobuk River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('31');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Gates of the Arctic National Park and Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment within Gates of the Arctic National Park.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 110.0 miles; Total &#8212; 110.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/kobuk.jpg" alt="Kobuk River" title="Kobuk" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/gaar/kobukriver.htm" alt="Kobuk River (National Park Service)" target="_blank">Kobuk River (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Sara Dykstra</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Kobuk River</h2>
<p>From its headwaters in the Endicott Mountains and Walker Lake, the river courses south and west through a wide valley and passes through two scenic canyons. In the upper canyon, you'll encounter some Class 4 rapids that need to be portaged (about 1/3-mile portage), and there is one section of Class 2-3 rapids in the lower canyon.</p>
<p>Kobuk means "big river" in the language of the Inuit. Native peoples have hunted, fished and lived along the Kobuk for at least 12,500 years, and the river has long been an important transportation route for inland peoples.</p>
<p>In 1898 the river was the scene of the Kobuk River Stampede, a brief gold rush involving about 2,000 prospectors. Rumors of gold brought miners from along the West Coast by ship to reach the Kobuk and its tributaries. On arrival at the mouth of the Kobuk native people informed the miners it was a scam, and only about 800 miners ventured up the river. The result was that little or no gold was found and only on a few tributaries.</p>
<p>The river supports an amazing, abundant fishery. The put-in for most river trips, Walker Lake, teems with grayling and trout. Anglers can count on catching 14-18" grayling throughout the river, and there is a sheefish (a whitefish) run in August with 36 to 40-inch fish.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>