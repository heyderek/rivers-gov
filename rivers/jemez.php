<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Jemez River (East Fork), New Mexico';

// Set the page keywords
$page_keywords = 'Santa Fe National Forest, Jemez River, New Mexico';

// Set the page description
$page_description = 'Jemez River (East Fork), New Mexico';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('109');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Santa Fe National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>June 6, 1990. From the Santa Fe National Forest boundary to its confluence with the Rio San Antonio.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.0 miles; Scenic &#8212; 5.0 miles; Recreational &#8212; 2.0 miles; Total &#8212; 11.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/jemez.jpg" alt="East Fork Jemez River" title="East Fork Jemez River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/jemez-plan.pdf" alt="Jemez River Management Plan (799 KB PDF)" target="_blank">Jemez River Management Plan (799 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Jemez River (East Fork)</h2>
<p>The East Fork originates in the Valles Caldera as a small meandering stream in a vast crater. On its way to its confluence with the Rio San Antonio, the river passes through the heart of the Jemez Mountains' most popular recreation area.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>