<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sturgeon River, Michigan';

// Set the page keywords
$page_keywords = 'Hiawatha National Forest, Sturgeon River, Michigan';

// Set the page description
$page_description = 'Sturgeon River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('128');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Hiawatha National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From the north line of Section 26, T43N, R19W, to Lake Michigan.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 21.7 miles; Recreational &#8212; 22.2 miles; Total &#8212; 43.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/sturgeon1.jpg" alt="Sturgeon River, Hiawatha National Forest" title="Sturgeon River, Hiawatha National Forest" width="265px" height="204px" title="Sturgeon River, Hiawatha National Forest" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="" target="_blank"></a></p>-->

<div id="photo-credit">
<p>Photo Credit: Mark Beezhold</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sturgeon River (Hiawatha National Forest)</h2>
<p>The Sturgeon Wild and Scenic River, in Michigan's Upper Peninsula, has outstanding wildlife, heritage, ecological and hydrologic resources. It is one of the few areas on the Upper Peninsula with a southern-floodplain microclimate. Early summer and late fall are the best times to canoe the Sturgeon River. Two rapids challenge canoeists; elusive brown trout, steelhead and salmon challenge anglers. The river enters Lake Michigan at the historic town of Nahma.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>