<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Amargosa River, California';

// Set the page keywords
$page_keywords = 'Amargosa River, Death Valley, Mojave Desert, California';

// Set the page description
$page_description = 'Amargosa River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");

//ID for the rivers
$river_id = array('196');
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php include_once ("../iframe.php"); ?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Barstow Field Office</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the northern boundary of Section 7, Township 21 North, Range 7 East to 100 feet upstream of the Tecopa Hot Springs Road crossing. From 100 feet downstream of the Tecopa Hot Springs Road crossing to 100 feet upstream of the Old Spanish Trail Highway crossing near Tecopa. From the northern boundary of Section 16, Township 20 North, Range 7 East to 100 feet upstream of the Dumont Dunes Access Road crossing in Section 32, Township 19 North, Range 7 East. From 100 feet downstream of the Dumont Dunes Access Road for the next 1.4 miles.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 7.9 miles; Scenic &#8212; 12.1 miles; Recreational &#8212; 6.3 miles; Total &#8212; 26.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/amargosa.jpg" alt="Amaragosa River" title="Amaragosa River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/ca/st/en/fo/barstow/amargosa.html" target="_blank" alt="Amargosa River Natural Area (Bureau of Land Management)" target="_blank">Amargosa River Natural Area (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Amargosa River</h2>

<p>Often called the "Crown Jewel of the Mojave Desert," the Amargosa is the only free-flowing river in the Death Valley region of the Mojave, providing a rare and lush riparian area in the desert.</p>

<p>The narrow Amargosa Canyon is known for its dense greenery and the shallow Amargosa River, complete with "hanging gardens" and a small waterfall. The river flows year-round dropping south from Nevada and finally flowing into Death Valley National Park.</p>

<p>The precious water from this desert river has allowed people to live here, on and off, for the past 8,000 years. Nature offers a variety of scenic landscapes here. Important natural systems include the river, marshes, mud hills, riparian area and salt-encrusted mud flats.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>Archeological sites along the river indicate a continuing occupation by indigenous peoples for over 10,000 years. These sites are located on both sides of the river, in each segment. There is a high concentration of sites within the 1/4-mile boundary of the Amargosa River, because it is the largest water source in the region.</p>

<p><strong><em>Geologic</em></strong></p>

<p>These segments of the Amargosa River have been carved into a colorful array of spires, mesas, cliffs and canyons over the years by water flow of varying velocities. The ancient Tecopa Lake bed in the central segment contains fascinating landforms and extensive fossils, including many not recorded frequently in the region.</p>

<p><strong><em>Historic</em></strong></p>

<p>The Old Spanish Trail crosses the river in the central and lower segments and was one of the few pioneer trails used for both east and west travel. Famed explorers, such as Kit Carson and Colonel John C. Fremont, described several sites along these segments. The Tonopah and Tidewater (TNT) Railroad, which traverses all three segments, provided an historic support function for the remote mining communities located in the Death Valley Region in the early part of the 20th century.</p>

<p><strong><em>Recreational</em></strong></p>

<p>As one of the few surface water, riparian vegetation and high canyon density locales in the region, the three protected segments of the Amargosa offer visitors unusual river and canyon-based opportunities. Among these are hiking, exploration, bird watching, photography and equestrian use, all in rugged and physically challenging terrain. The lower segment provides a unique desert experience, because it includes multiple water crossings and scenic views.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Designated segments of the Amargosa River flow past unusual desert wetlands and hot spring creeks; ancient lakebeds; mesas; mudflats; an abandoned railroad and human ruins of all kinds; colorful rock formations and precipitous cliffs; expansive meadows; and even waterfalls. The lush riparian and wetland plant communities present along these segments contrast dramatically with the surrounding stark, desert landscape.</p>

<p><strong><em>Wilderness</em></strong></p>

<p>The Tecopa to Sperry Siding segment encompasses a portion of the Kingston Range Wilderness, where little human modification of the landscape is evident. This segment provides an opportunity to experience solitude in the natural condition of the Mojave Desert.</p>

<p><strong><em>Wildlife &amp; Plants</em></strong></p>

<p>The Amargosa River flows in a part of the Mojave Desert declared an "Area of Critical Environmental Concern" by the BLM to protect plants and animals listed under the Endangered Species Act. The Amargosa vole, least Bell's vireo and southwestern willow flycatcher are listed as state and federally endangered, and the state of California lists the yellow-billed cuckoo, Swainson's hawk and Amargosa niterwort as threatened. Two desert fish species&#8212;the Amargosa pupfish and the Amargosa speckled dace&#8212;also occur in these segments and are designated as sensitive species by the BLM.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>