<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Allegheny River, Pennsylvania';

// Set the page keywords
$page_keywords = 'Allegheny National Forest, Allegheny River, Pennsylvania';

// Set the page description
$page_description = 'Allegheny River, Pennsylvania';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('133');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php 
// includes the TEMPLATE HEADER CODING -- #content-page		
include ('../includes/header.php'); 
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Allegheny National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 20, 1992. From Kinzua Dam downstream to the U.S. Route 62 Bridge. From Buckaloons Recreation Area at Irvine downstream to the southern end of Alcorn Island at Oil City. From the sewage treatment plant at Franklin to the refinery at Emlenton.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 86.6 miles; Total &#8212; 86.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/allegheny.jpg" alt="Allegheny River" title="Allegheny River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/attmain/allegheny/specialplaces/" alt="Allegheny National Forest (U.S. Forest Service)" target="_blank">Allegheny National Forest (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Allegheny River</h2>

<p>Named "La Belle Riviere" by French explorers, these 87 miles of the Allegheny flows through areas of narrow forest valleys, wilderness islands and broad, rural landscapes rich with the early history and culture of the region. Pastoral farmlands, small towns and the narrow winding valleys provide a diversity of views for those travelling the river. Good public access and few hazards make this an ideal river for novice and family canoeing. Fishing for muskie, walleye, rainbow trout and smallmouth bass is popular.</p>

<p>The Allegheny River Basin occupies 11,747 square miles in the states of New York and Pennsylvania. The Allegheny River is over 315 miles long and contributes 60 percent of the Ohio River flow at Pittsburgh, Pennsylvania. It is the location of the most populous freshwater mussel habitat in the world, and one tributary, French Creek, is one of the most biologically diverse watersheds in Pennsylvania.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>