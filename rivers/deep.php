<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Deep Creek, Idaho';

// Set the page keywords
$page_keywords = 'Deep Creek, Idaho';

// Set the page description
$page_description = 'Deep Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('185');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>
			
<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Deep Creek from its confluence with the Owyhee River to the upstream boundary of the Owyhee River Wilderness in Section 30, Township 12 South, Range 2 West, Boise Meridian.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 13.1 miles; Total &#8212; 13.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/deep-creek.jpg" alt="Deep Creek" title="Deep Creek" width="262px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14974/2" alt="Deep Creek (Bureau of Land Management)" target="_blank">Deep Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Judi Zuckert, BLM</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Deep Creek</h2>

<p>Deep Creek is defined by the extremely meandering, vertical-walled canyon surrounding the creek as it flows to its confluence with the Owyhee River. The creek is suitable for kayaks and canoes in the early spring when flows reach into the hundreds of cubic feet per second from snowmelt, and the area provides outstanding hiking and backpacking opportunities.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Deep Creek supports sensitive redband trout populations; however, warmer summer water temperatures are insufficient to support productive fisheries. Seasonal migration of smallmouth bass into the Owyhee River system over the past several decades suggests that conditions may favor the development of a cool water fishery. However, the arrival of smallmouth bass would likely initiate competition for food and space between bass and trout..</p>

<p><strong><em>Geologic</em></strong></p>

<p>Deep Creek is part of the Owyhee, Bruneau and Jarbidge river systems, which provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States, products of Miocene Era (23 to 5 million years ago) volcanic formations. Where overlying basalt is present, rhyolite (rock which began as lava flow) formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved monolithic cliffs and numerous sculptured pinnacles known as "hoodoos." Though not unique to southwest Idaho, the presence of these geologic formations in such great abundance and expansiveness makes Deep Creek, the nearby Owyhee River and other Owyhee tributaries geologically impressive.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Deep Creek is boatable by kayak or open canoe early in the float season. Although this Class II stream offers no difficult rapids, it demands a high level of skill to negotiate its narrow width and braided channels while avoiding thick, overhanging vegetation and frequent strikes against gravel bars, streamside cliffs and fences. Outstanding day-hiking opportunities are available from canyon rims to the stream in late spring and again in the fall when the summer temperatures have subsided. Deep Creek provides exceptional opportunities for solitude and for viewing wildlife and taking photographs.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Landscape forms throughout the Owyhee Canyonlands vary from broad, open sagebrush steppes to narrow canyons, some exceeding 800-feet in depth. The canyons are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs; yellow and subdued green sagebrush-bunchgrass communities; dark green juniper; and reddish rhyolite or coarse-textured, blackish basalt rubble fields. In some areas, colorful rhyolite spires and rock pinnacles ("hoodoos") line the canyons.</p>

>p>Landscapes found along Deep Creek and throughout the designated Owyhee, Bruneau and Jarbidge segments&#8212;combining line, form, color and texture found amidst close associations of landforms, water and vegetation&#8212;are among the best representations of basalt and rhyolite canyon/riparian associations in the region.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Big game found in the area include elk, mule deer and pronghorn. Deep Creek, in combination with the Owyhee River, Battle Creek, Duncan Creek and Wickahoney Creek, supports the majority of the bighorn sheep population in the Owyhee Canyonlands.</p>

<p>Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents, rabbits, shrews, bats, weasels and skunks. The waters along the entire Owyhee River system and its tributaries are considered outstanding habitat for river otters.</p>

<p>Deep Creek offers Preliminary Priority Habitat for the greater sage-grouse. Other sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads.</p>

<p>The high, well-fractured and eroded canyon cliffs are outstanding habitat for cliff-nesting raptors, a small number of which occasionally winter along the canyon walls. Cliffs also support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>

<p>Other wildlife includes several species of reptiles (snakes and lizards) and amphibian (frogs, toads and salamanders).</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>