<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'New River (South Fork), North Carolina';

// Set the page keywords
$page_keywords = 'New River, Stone Mountain State Park, North Carolina';

// Set the page description
$page_description = 'New River (South Fork), North Carolina';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('SD4');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>New River State Park</p>
<br />
<h3>Designated Reach:</h3>
<p>April 13, 1976. The South Fork from its confluence with Dog Creek downstream 22 miles to the confluence with the North Fork. The main stem from the confluence of the North and South Forks with Dog Creek downstream approximately 4.5 miles to the Virginia state line.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 26.5 miles; Total &#8212; 26.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/new.jpg" alt="New River" title="New River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.ncparks.gov/Visit/parks/neri/main.php" alt="New River State Park" target="_blank">New River State Park</a></p>

<div id="photo-credit">
<p>Photo Credit: American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>New River (South Fork)</h2>
<p>Rugged hillsides, pastoral meadows and bucolic farmlands surround the oldest river in North America. The river's waters are slow and placid; its banks are fertile and covered with wildflowers. Fishing is good. Narrow and winding mountain roads dotted with small farms, churches and country stores give an old-fashioned charm. New River State Park provides access and spots for camping, canoeing, picnicking and fishing.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>