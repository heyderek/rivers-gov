<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Rio Chama River, New Mexico';

// Set the page keywords
$page_keywords = 'Rio Chama, Bureau of Land Management, Taos Resource Area, Carson National Forest, Santa Fe National Forest, New Mexico';

// Set the page description
$page_description = 'Rio Chama River, New Mexico';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('108');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Taos Field Office<br />
U.S. Forest Service, Carson National Forest<br />
U.S. Forest Service, Santa Fe National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 7, 1988. From El Vado Ranch launch site (immediately south of El Vado Dam) downstream 24.6 miles.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 21.6 miles; Scenic &#8212; 3.0 miles; Total &#8212; 24.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rio-chama.jpg" alt="Rio Chama" title="Rio Chama" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/programs/recreation/permits-and-passes/lotteries-and-permit-systems/new-mexico/rio-chama-river" alt="Rio Chama (Bureau of Land Management)" target="_blank">Rio Chama (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/rio-chama-plan.pdf" alt="Rio Chama Management Plan (4.9 MB PDF)" target="_blank">Rio Chama Management Plan (4.9 MB PDF)</a></p>
<p><a href="http://www.chamavalley.com/history.html" alt="Rio Chama History" target="_blank">Rio Chama History</a></p>

<div id="photo-credit">
<p>Photo Credit: Martha Moran</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Rio Chama</h2>
<p>The Rio Chama is a major tributary of the Rio Grande in northern New Mexico. It flows through a multi-colored sandstone canyon which is, at times, 1,500 feet deep and through a wilderness and wilderness study area. Towering cliffs, heavily wooded side canyons and historical sites offer an outstanding wild river backdrop for the hiker, fisherman, or boater.</p>

<p>Car camping is popular on the lower eight miles, and boaters enjoy can two- or three-day trips on Class II rapids on the entire 31-mile segment (advance permits required), or half day trips on the lower segment (no advance permits required).</p>

<p>Co-managed by the Bureau of Land Management and the U.S. Forest Service, the Rio Chama offers something for everyone &#8212; paddling, trout fishing, hiking, exploring dinosaur tracks and simply relaxing in the shade. Applications for boating permits in the upper canyon can be obtained by contacting the BLM at the address/telephone above. Applications for the lottery must be received by February 1 of each year.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>Northern New Mexico, and the Rio Chama Valley in particular, are known as some of the richest regions for archeology studies in the U.S. For more than three centuries&#8212;about A.D. 1250 to 1577 &#8212;this spectacular locale was home for over 1500 Pueblo Indians who built villages, dwelled, farmed and hunted game here.</p>

<p><strong><em>Ecologic</em></strong></p>

<p>Trout often flourish in the river, and onshore residents include mule deer, black bears, elk, coyotes and mountain lions. Varying canyon elevations also provide a wide range of vegetation, from low-lying pinion-juniper woodland to ponderosa pine and fir. Between 70 and 80 different bird varieties can be found in the Chama River Canyon. Raptors, hawks and owls perch along the canyon walls and surrounding trees.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Rio Chama carved a 900-foot-deep canyon of colorful siltstone, gypsum and sandstone layers. Floating down the river provides an ever changing view of sedimentary layers traveling back in time millions of years. There are even the imprints of dinosaur tracks preserved in one side canyon.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Water releases from the upstream El Vado Lake Dam are often timed to allow weekend river floats from below El Vado Reservoir to Big Eddy. To protect the river environment and to maintain an opportunity for a high quality experience, use is limited through a lottery system. Several hiking trails also provide access from the uplands down to the river. The lower eight miles of the river includes two campgrounds and a road paralleling the river that provides for easy access.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Rio Chama creates a canyon of colorful siltstone and sandstone surrounded by gently rolling sagebrush-covered plains. Piñon woodlands cover the hills, and forests of ponderosa pine and Douglas fir cover the north facing slopes.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>