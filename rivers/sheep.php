<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sheep Creek, Idaho';

// Set the page keywords
$page_keywords = 'Sheep Creek, Idaho';

// Set the page description
$page_description = 'Sheep Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('193');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Sheep Creek from its confluence with the Bruneau River to the upstream boundary of the Bruneau-Jarbidge Rivers Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 25.6 miles; Total &#8212; 25.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->

<img src="images/sheep-creek.jpg" alt="Sheep Creek" title="Sheep Creek" width="153px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/sheep-creek-wild-and-scenic-river" alt="Sheep Creek (Bureau of Land Management)" target="_blank">Sheep Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sheep Creek</h2>

<p>Sheep Creek is a 63-mile-long tributary of the Bruneau River that originates in northern Nevada and flows north into Owyhee County, Idaho, and the Owyhee Desert. The section designated as wild flows over 25 miles through an extremely narrow, winding canyon with sheer vertical walls and into the Bruneau River, 13 miles downstream from (north of) Indian Hot Springs. Access to Sheep Creek is limited because of its remoteness.</p>

<p><strong><em>Cultural</em></strong></p>

<p>Native Americans have utilized the canyonlands for shelter, weaponry, fish and game, and water for thousands of years. Petroglyphs, pictographs, rock alignments, shrines and vision quest sites of the Shoshone and Paiute peoples are located throughout the Owyhee Canyonlands. Tribal members still frequent the canyonlands to hunt, fish, pray and conduct ceremonies.</p>

<p>Cowboys also left their marks on the sheltered canyon walls. A few homesteaders chose the canyons as a place that was well-suited for collecting water, hunting game and perhaps using nearby thermal pools.</p>

<p><strong><em>Ecologic</em></strong></p>

<p>Steep canyon walls, boulder fields and rock crevices provide unique habitats in an area more commonly dominated by rolling hills and wide plateaus. The Bruneau River phlox, a white flowered and matted plant that clings to ledges, rock crevices and cliffs occurs in vertical or overhanging rhyolitic canyon walls; the entire known extent of Bruneau River phlox in Idaho occurs within this small area.</p>

<p><strong><em>Fisheries and Aquatic Species</em></strong></p>

<p>The Bruneau and Jarbidge river systems, including Sheep Creek, support both redband and bull trout. Bull trout critical habitat, consisting of a Rocky Mountain juniper-dominated riparian zone, is unique to the area. Although bull trout spawn in upstream portions of the Jarbidge River in Nevada, the Jarbidge River in Idaho and Sheep Creek are limited to over-wintering and migratory habitat, which is maintained by bank-full flows that move the river bed materials downstream and the silts and sands to the upper channel banks between bank-full and floodplain levels. The bull trout and redband trout populations also rely on low flows that maintain hiding pools that hold water throughout dryer seasons.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Sheep Creek is part of the Bruneau, Jarbidge and Owyhee river systems, which provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States. Though not unique to southwest Idaho, their great abundance and expansiveness makes the designated river segments geologically significant. Their geology has been shaped by a combination of volcanic activity, glacial melt and regional drainage patterns. The area was the site of eruptive centers whose explosions led to gradual collapse of volcanic cones and the creation of basins. Massive rhyolite flows and basaltic eruptions followed, and the glacial flows have carved out the canyons that expose this activity. If overlying basalt is present, the rhyolite formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved immense monolithic cliffs and numerous sculptured pinnacles known as "hoodoos."</p>

<p><strong><em>Recreational</em></strong></p>

<p>Sheep Creek relies on rainfall to provide flows to its Class IV-V rapids, and it is therefore rarely boatable in any craft. When it is running, Sheep Creek offers a challenging run with a handful of Class IV and V rapids, such as Gun-shy, Blind Date and Grants Slam, before continuing down to the Bruneau River.</p>

<p>Sheep Creek and other Jarbidge-Bruneau canyons provide exceptional opportunities for solitude and for passive, primitive and/or physically challenging activities, including hiking, wildlife viewing and photography.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Sheep Creek Canyon, similar to others in the Owyhee Canyonlands, is dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Intertwined with the vertical features are some very steep diagonal lines that frame triangular forms associated with talus slopes. The slopes have a mosaic of medium-textured, yellow and subdued green sagebrush-bunchgrass communities and/or dark green juniper, as well as either medium-textured, reddish rhyolite rubble fields or coarse-textured, blackish basalt rubble fields. The basalt/rhyolite associations found here and elsewhere along the designated Owyhee, Bruneau and Jarbidge segments are among the best representations of this landscape in the region.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee Canyonlands provide both upland and canyon riparian habitats for a number of wildlife species common to southwest Idaho. Big game species commonly found in the area include California bighorn sheep, elk, mule deer and pronghorn.</p>

<p>Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents (mice, kangaroo rats, voles, squirrels, and chipmunks), rabbits, shrews, bats, weasels and skunks. River otters are supported by year-long flows and a good prey base (fisheries).</p>

<p>Birds include songbirds, waterfowl, shorebirds and raptors.</p>

<p>This area includes Preliminary Priority Habitat for greater sage-grouse. Idaho BLM sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>