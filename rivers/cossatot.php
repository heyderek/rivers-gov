<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Cossatot River, Arkansas';

// Set the page keywords
$page_keywords = 'Ouachita National Forest, Cossatot River, Brushy Creek, Ouachitna Mountains, Arkansas';

// Set the page description
$page_description = 'Cossatot River, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('136','SD15');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Arkansas State Parks Department<br />
U.S. Army Corps of Engineers, Little Rock District<br />
U.S. Forest Service, Ouachita National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992, and January 14, 1994. The segment of the Cossatot River from the confluence with Mine Creek in Polk County, near the town of Shady, Arkansas, downstream to Duchett's Ford, 4.6-miles below the State Highway 4 Bridge. Also, the segment of Brushy Creek from approximately 4.4 miles upstream of the Ouachita National Forest boundary just north of State Highway 246 to its confluence with the Cossatot River in the Cossatot River State Park-Natural Area in Polk County.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 26.6 miles; Recreational &#8212; 4.2 miles; Total &#8212; 30.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/cossatot.jpg" alt="Cossatot River" title="Cossatot River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.arkansasstateparks.com/cossatotriver/" alt="Cossatot River (Arkansas State Parks)" target="_blank">Cossatot River (Arkansas State Parks)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Cossatot River</h2>
<p>The National Park Service describes it as "probably the most challenging" white-water float in the state. The U.S. Army Corps of Engineers is a little more emphatic, saying it "is the most difficult whitewater stream in the state of Arkansas." Early Indians simply called it Cossatot&#8212;their word for "skull crusher." Today, the Cossatot River is still crushing things, but they're mostly canoes, ice chests and the egos of over-confident paddlers.</p>
<p>The stream heads up in rugged Ouachita mountain country just southeast of Mena. It flows in a southerly direction for about 26 miles before its current ceases at Gillham Lake. Along the way the Cossatot travels through the Ouachita National Forest, along-side a wilderness area, and over and around upended layers of jagged bedrock. This last characteristic is what gives the stream its Class IV/Class V rating among river-runners. The Cossatot, however, is not just for floaters. In fact, much of its whitewater is not recommended for casual canoeists. The stream offers something for nearly everyone interested in Arkansas's outdoors.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>