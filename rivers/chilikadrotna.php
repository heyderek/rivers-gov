<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Chilikadrotna River, Alaska';

// Set the page keywords
$page_keywords = 'Lake Clark National Park, Chilikadrotna River, Alaska';

// Set the page description
$page_description = 'Chilikadrotna River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('29');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Lake Clark National Park and Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The portion of the river within Lake Clark National Park and Preserve.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 11.0 miles; Total &#8212; 11.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/chattooga.jpg" alt="" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/lacl/" alt="Lake Clark National Park and Preserve (National Park Service)" target="_blank">Lake Clark National Park and Preserve (National Park Service)</a></p>
<p><a href="http://www.nps.gov/lacl/planyourvisit/rafting.htm" alt="Rafting the Chilikadrotna River (National Park Service)" target="_blank">Rafting the Chilikadrotna River (National Park Service)</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Doug Whittaker</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Chilikadrotna River</h2>
<p>The "Chili" River originates at scenic Twin Lake and flows through Lake Clark National Park and Preserve. The river features long stretches of swift water, outstanding fishing, and magnificent mountain vistas.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>