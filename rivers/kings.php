<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Kings River, California';

// Set the page keywords
$page_keywords = 'Kings Canyon National Park, Sequoia National Forest, Kings River, California';

// Set the page description
$page_description = 'Kings River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('63');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Sequoia/Kings Canyon National Parks<br />
U.S. Forest Service, Sequoia National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 3, 1987. From the confluence of the Middle Fork and the South Fork to the point at elevation 1,595 feet above mean sea level. The Middle Fork from its headwaters at Lake Helen to its confluence with the main stem. The South Fork from its headwaters at Lake 11599 to its confluence with the main stem.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 65.5 miles; Recreational &#8212; 15.5 miles; Total &#8212; 81.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/kings.jpg" alt="Kings River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.fs.usda.gov/recarea/sequoia/recreation/recarea/?recid=79958" alt="Kings River &#8211; U.S. Forest Service" target="_blank">Kings River &#8211; U.S. Forest Service</a></p>
<p><a href="../documents/plans/kings-ea.pdf" title="Kings River Environmental Assessment" target="_blank">Kings River Management Plan Environmental Assessment</a></p>
<p><a href="../documents/plans/kings-plan.pdf" title="Kings River Management Plan" target="_blank">Kings River Management Plan</a></p>
<p><a href="../documents/plans/kings-rod.pdf" title="Kings River Management Plan Record of Decision" target="_blank">Kings River Management Plan Record of Decision</a></p>

<div id="photo-credit">
<p>Photo Credit: Michael Carl</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Kings River</h2>
<p>The Kings Wild and Scenic River includes the entire Middle and South Forks and six miles of the Kings River. It flows through Kings Canyon National Park and the Sequoia and Sierra National Forests. Beginning in glacial lakes above timberline, the river flows by deep, steep-sided canyons, over falls and cataracts, eventually becoming an outstanding whitewater rafting river in its lower reaches in Sequoia National Forest. Geology, scenery, recreation, fish, wildlife and history are all significant aspects.</p>
<p>The river traverses through the second deepest canyon in North America and offers tremendous scenery and recreation experiences. Kings Canyon Scenic Byway will take you to the eastern section of the river where you will find wilderness trailheads, Grizzly Falls picnic area and dispersed camping opportunities along the river's edge. Cave tours are available at Boyden Cavern, there is camping at Convict Flat Campground and some of the best fly fishing in California.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>