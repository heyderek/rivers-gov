<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Westfield River, Massachusetts';

// Set the page keywords
$page_keywords = 'Westfield River, Pioneer Valley Planning Commission, Massachusetts';

// Set the page description
$page_description = 'Westfield River, Massachusetts';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('SD14','SD20');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Northeast Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>November 2, 1993. The designated segments include: the West Branch from a railway bridge 2000 feet downstream of the Becket Town Center to the Huntington/Chester town line; the Middle Branch from the Peru/Worthington town line downstream to the confluence with Kinne Brook in Chester; Gendale Brook from Clark Wright Road bridge to its confluence with the Middle Branch; the East Branch from the Windsor/Cummington town line to a point 0.8 miles upstream of the confluence with Holly Brook in Chesterfield.</p>
<p>September 28, 2004. This designation includes many small tributaries, including:
<ol>
<li>Headwater tributaries of the East Branch including:<li>
<ol>
<li>Drowned Land Brook from its headwaters in Windsor to the confluence with the East Branch in Savoy;</li>
<li>Center Brook from its headwaters below a pond near Savoy Center to its confluence with the East Branch; and<li>
<li>Windsor Jambs Brook from the junction of Phelps Brook and Clear Brook to its confluence with the East Branch in Windsor.</li>
</ol>
<li>The Upper East Branch from the confluence with Drowned Land Brook in Savoy to the Windsor/Cummington Town Line.</li>
<li>The Lower East Branch from Sykes Brook in Huntington to the confluence with the West Branch.</li>
<li>Lower Middle Branch from the Goss Hill Road Bridge downstream to the confluence with the East Branch.</li>
<li>Headwater Tributaries of the West Branch, including:</li>
<ol>
<li>Shaker Mill Brook from Brooker Hill Road in Becket to its confluence with the West Branch;</li>
<li>Depot Brook from its headwaters near Beach Road in Washington to the confluence with Shaker Mill Brook in Becket;</li>
<li>Savery Brook from the headwaters off Pittsfield Road in Washington to the confluence with Shaker Mill Brook;</li>
<li>Watson Brook from the headwaters off Stanley Road in Washington to the confluence with Shaker Mill Brook; and</li>
<li>Center Pond Brook from Center Pond to its confluence with the West Branch.</li>
</ol>
<li>Lower West Branch from Chester/Huntington town line downstream to the confluence with the Main Stem.</li>
<li>Main Stem from the confluence with the East Branch and Middle Branch in Huntington Center downstream until the Huntington/Russell town line.</li>
</ol>
</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 2.6 miles; Scenic &#8212; 42.9 miles; Recreational &#8212; 32.6 miles; Total &#8212; 78.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/westfield.jpg" alt="Westfield River" title="Westfield River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/westfield_pwsr_sub.html" alt="Westfield River (National Park Service)" target="_blank">Westfield River (National Park Service)</a></p>
<p><a href="http://www.westfieldriverwildscenic.org" alt="Westfield River Wild &amp; Scenic Advisory Committee" target="_blank">Westfield River Wild &amp; Scenic Advisory Committee</a></p>
<p><a href="../documents/studies/westfield-river-evaluation-ea.pdf" alt="Westfield River 2(a)(ii) Study &amp; Environmental Assessment" target="_blank">Westfield River 2(a)(ii) Study &amp; Environmental Assessment</a></p>
<p><a href="../documents/plans/westfield-findings.pdf" alt="Westfield River WSR Findings (477 KB PDF)" target="_blank">Westfield River WSR Findings (477 KB PDF)</a></p>
<p><a href="../documents/plans/westfield-summary.pdf" alt="Westfield River WSR Findings Summary Chart (228 KB PDF)" target="_blank">Westfield River WSR Findings Summary Chart (228 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Westfield River</h2>
<p>From its origins in the Berkshire Hills in western Massachusetts, the Westfield River links together historic villages, prime farmland, pristine wilderness areas, and waterfalls and gorges of remarkable quality. The river features native trout fishing, rugged mountain scenery and a historical mill town settlement. The Westfield River provides over 50 miles of the Northeast's finest whitewater canoeing and kayaking. The river corridor also contains one of the largest roadless wilderness areas remaining in the state and is home to several endangered species.</p>
<p>The Westfield is a state and locally managed river. Through the locally initiated designation, community members, municipal officials, conservation organizations, and federal and state agencies have come together to manage and protect this national treasure. Members of the Westfield River Wild &amp; Scenic Advisory Committee lend their eyes, ears and voices for the river. Serving as liaisons to their respective communities and organizations, committee members raise awareness about the Westfield River and its resources and advocate for their preservation and protection.</p>
<p>The Westfield River designation stretches over 78 miles along the river's main stem and its East, Middle and West Branches. The corridor width is 200 feet wide from mean high water, corresponding to the width of the Massachusetts River Protection Act.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>