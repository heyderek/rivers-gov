<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Yellow Dog River, Michigan';

// Set the page keywords
$page_keywords = 'Ottawa National Forest, Yellow Dog River, McCormick Wilderness, Michigan';

// Set the page description
$page_description = 'Yellow Dog River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('132');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ottawa National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From its origin at the outlet of Bulldog Lake Dam to the boundary of the Ottawa National Forest.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.0; Total &#8212; 4.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/yellow-dog.jpg" alt="Yellow Dog Falls" title="Yellow Dog Falls" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/ottawa/recreation/natureviewing/recarea/?recid=12362&actid=64" alt="Yellow Dog Falls (U.S. Forest Service)" target="_blank">Yellow Dog Falls (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/ottawa-nf-plan.pdf" alt="Yellow Dog River Management Plan (1.4 MB PDF)" target="_blank">Yellow Dog River Management Plan (1.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: David Hickey</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Yellow Dog River</h2>
<p>The Yellow Dog River drops sharply through numerous outcrops and cascades within the McCormick Wilderness. Vegetation consists of large eastern white pine, eastern hemlock, sugar maple, northern red oak and other old-growth northern hardwood species. Outstandingly remarkable values include scenery, geology and wildlife.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>