<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Salmon River, Alaska';

// Set the page keywords
$page_keywords = 'Salmon River, Kobuk Valley National Park, Alaska';

// Set the page description
$page_description = 'Salmon River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('35');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Kobuk Valley National Park</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment within the Kobuk Valley National Park.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 70.0 miles; Total &#8212; 70.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/salmon-alaska.jpg" alt="Salmon River, Alaska" title="Salmon River, Alaska" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/kova/" alt="Kobuk Valley National Park (National Park Service)" target="_blank">Kobuk Valley National Park (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Eileen Devinney / NPS</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Salmon River (Alaska)</h2>
<p>Located within Kobuk Valley National Park, the Salmon River is small but exceptionally beautiful, with deep, blue-green pools and many rock outcroppings. Vegetation ranges from alpine tundra to treeless bogs.</p>
<p>The Salmon River flows 70 miles from the limestone cirques of the highest peak in Kobuk Valley National Park, Mt. Angayukaqsraq, to the Kobuk River. The Salmon River supports an excellent grayling fishery and large runs of chum and pink salmon; the river owes its name to the large salmon runs. Over its course, the Salmon River flows through the ecotone where arctic tundra transitions into boreal forest lowlands. In the mountainous headwaters, the river rushes and tumbles through a narrow steep-sided valley, often with bare rock walls on either side. In the middle reach, the river slows and forms numerous inter-connected deep pools as it flows through rolling hills. Water of exceptional clarity, coupled with blue-green gravel found in the area, give the pools exceptional color. In the lowlands, the water slows, flowing through low-growing spruce forests and treeless bogs until joining the Kobuk River.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>