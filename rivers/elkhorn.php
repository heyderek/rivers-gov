<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Elkhorn Creek, Oregon';

// Set the page keywords
$page_keywords = 'Elkhorn Creek, Oregon';

// Set the page description
$page_description = 'Elkhorn Creek River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('159');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Salem District<br />
U.S. Forest Service, Willamette National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>September 30, 1996. This wild and scenic river consists of a 5.8 mile wild river area, extending from a point along the Willamette National Forest to its confluence with Buck Creek. A smaller segment of 0.6 miles, designated as a scenic river area, extends from the confluence of Buck Creek to that point where the segment leaves the Bureau of Land Management boundary in Township 9.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 5.8 miles; Scenic &#8212; 0.6 miles; Total &#8212; 6.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/elkhorn-creek.jpg" alt="Elkhorn Creek" title="Elkhorn Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/willamette/recreation/recarea/?recid=4212" alt="Opal Creek Scenic Recreation Area (U.S. Forest Service)" target="_blank">Opal Creek Scenic Recreation Area (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Zachary Collier, Northwest Rafting Company</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Elkhorn Creek</h2>

<p>Elkhorn Creek is a unique, pristine, low-elevation setting, with limited access, providing a landscape with few signs of human disturbance or activity. It flows through the heavily forested and rugged foothills of the west side of the Cascade Range and within the Opal Creek Scenic Recreation Area, which is known for its majestic old-growth forests. The 'wild' section flows from a point along the Willamette National Forest to 0.6 mile downstream of its confluence with Buck Creek.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries &amp; Wildlife</em></strong></p>

<p>Healthy populations of native cutthroat and rainbow trout reside in Elkhorn Creek.</p>

<p>Elkhorn Creek is within the expected range of the Oregon slender salamander, the only amphibian endemic to Oregon. It is listed by the state as a Species of Concern. It is most common in mature Douglas fir forests and apparently dependent on mature and old-growth stands.  The salamander is found under rocks, wood, wood chips at the base of stumps and under the bark and moss of logs. They are also found in decomposing logs.</p>

<p>A number of bat species of concern are suspected to occur in the corridor. These species are associated with caves and mines, bridges, buildings, cliff habitat, or decadent live trees and large snags with sloughing bark.</p>

<p>A number of migratory birds which are associated with late successional forest are expected to breed in the area.</p>

<p><strong><em>Scenic</em></strong></p>

<p>This picturesque stream flows through prominent bedrock landforms, views of occasional rock outcroppings and talus slopes interspersed with old-growth and second growth forests. Conifers dominate the forest, along with western red cedar and red alder. Throughout much of the designated reach, little evidence of human intrusion into the river corridor is present until the lower 0.6-mile segment.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>