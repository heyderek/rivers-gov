<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Palm Canyon Creek, California';

// Set the page keywords
$page_keywords = 'Palm Canyon Creek, California, San Bernardino National Forest';

// Set the page description
$page_description = 'Palm Canyon Creek, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('202');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, San Bernardino National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the southern boundary of Section 6, Township 7 South, Range 5 East to the San Bernardino National Forest boundary in Section 1, Township 6 South, Range 4 East.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 8.1 miles; Total &#8212; 8.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/palm-canyon-creek.jpg" alt="Palm Canyon Creek" title="Palm Canyon Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/sbnf/recreation/natureviewing/recarea/?recid=74094&actid=62" alt="Palm Canyon Creek (U.S. Forest Service)" target="_blank">Palm Canyon Creek (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Geoffrey McGinnis</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Palm Canyon Creek</h2>
<p>Palm Canyon Creek’s outstanding remarkable values include spectacular scenery of deep, rugged, rocky canyons and thick riparian vegetation. The waters of Palm Canyon Creek within the federally designated wild and scenic river area feed and sustain the nation's largest fan palm oasis, found two miles downstream on the Agua Caliente Indian Reservation. It is free of impoundments, is only accessible by trail and is in a primitive watershed with unpolluted waters. The varied and unspoiled terrain also provides a home for the endangered Peninsular bighorn sheep, the southwestern willow flycatcher and many sensitive songbirds.</p>
<p>The creek is located in the heart of historic Cahuilla territory and has been a resource for Native Americas for over two millennia. Palm Canyon holds special significance to the Agua Caliente Band of Cahuilla Indians, the original inhabitants of the area, who continue to care for and protect Palm Canyon. The waters of Palm Canyon Creek have sustained these Native American people agriculturally, economically, culturally and spiritually during the entirety of this long period, as they still do today.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>