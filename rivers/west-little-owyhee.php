<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'West Little Owyhee River, Oregon';

// Set the page keywords
$page_keywords = 'West Little Owyhee River, Oregon';

// Set the page description
$page_description = 'West Little Owyhee River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('106');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Vale District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters to its confluence with the Owyhee River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 57.6 miles; Total &#8212; 57.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/west-little-owyhee.jpg" alt="West Little Owyhee River" title="West Little Owyhee River" width="265px" height="204px" />-->
 <p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/umatilla/specialplaces/?cid=stelprdb5275389" alt="Wild &amp; Scenic Rivers &#8211; Umatilla National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Umatilla National Forest</a></p>
<p><a href="../documents/plans/owyhee-main-nf-west-little-plan-ea.pdf" title="Owyhee River Management Plan &amp; Environmental Assessment" target="_blank">Owyhee River Management Plan &amp; Environmental Assessment</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>West Little Owyhee River</h2>

<p>The West Little Owyhee River is a tributary of the Owyhee River. The source of the river is at an elevation of 6,508 feet (1,984 m) near the Nevada-Oregon border by the community of McDermitt, Nevada. Approximately 57 miles (91.7 km) in length, the river flows east by Deer Flat and into Louse Canyon. Near a prominent feature known as Twin Buttes, it turns sharply north as it cuts through the Owyhee Desert, making its way to the Owyhee River. The river is recognized for its remarkable scenery, wildlife, and opportunities for solitude and primitive recreation.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Recreational</em></strong></p>

<p>This river segment offers a wide variety of recreational opportunities. Off-trail backpacking and day hikes with the challenging opportunity to travel by swimming and hiking is a major attraction of this area. Excellent wildlife viewing and hunting are available due to the richness of the area's fauna.  Dispersed camping is allowed, although the watershed has no developed parks or campsites.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The designated river corridor is characterized by the stark contrast between the inner canyon and the typically flat sagebrush plateau through which it runs. The river itself offers many cool, clear secluded pools that are confined by sheer rock walls whose colorful, abstract beauty is the product of eons of erosion and weathering. The rugged canyon walls have kept humans and livestock from impacting the vegetation, enhancing the stark beauty of the canyon.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee River Canyon, including the West Little Owyhee, and adjacent sagebrush desert lands on the rims provide diverse habitat for over 200 species of wildlife. Birds are especially abundant, both in number of species and number of individuals. Much of this pristine canyon is used as nesting habitat by a wide variety of raptors, including several hawk species and golden eagles. Chukar partridges frequent the rimrocks and talus slopes, and prairie falcons have a strong presence.  Swainson's, ferruginous and red-tailed hawks, as well as kestrels and northern harriers, are common.</p>

<p>Upland game birds also include California quails and Hungarian partridges. Mourning doves visit during breeding season. Sage grouse are common on adjacent benchlands amidst their mixed sagebrush habitat. Winter visitors include Canada geese, mallards, redheads, teals, scaups, mergansers and grebes.</p>

<p>California bighorn sheep are present, particularly in the vicinity of Toppin Creek Canyon.  Pronghorn antelope, mule deer and white-tailed jackrabbits top the notable list of resident small game and rodents.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>