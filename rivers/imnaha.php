<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Imnaha River, Oregon';

// Set the page keywords
$page_keywords = 'Hells Canyon, Wallowa-Whitman National Forest, Eagle Cap Wilderness, Imnaha River, Oregon';

// Set the page description
$page_description = 'Imnaha River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('78');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. The main stem from the confluence of the North and South Forks of the Imnaha River to its mouth. The South Fork from its headwaters to the confluence with the main stem.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 15.0 miles; Scenic &#8212; 4.0 miles; Recreational &#8212; 58.0 miles; Total &#8212; 77.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/imnaha.jpg" alt="Imnaha River" title="Imnaha River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="../documents/plans/imnaha-plan.pdf" alt="Imnaha River Management Plan" title="Imnaha River Management Plan" target="_blank">Imnaha River Management Plan</a>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Imnaha River</h2>
<p>The Imnaha River is located in northeast Oregon on the Wallowa-Whitman National Forest. Originating from the headwaters of the South Fork of the Imnaha River near Cusick Mountain in the Eagle Cap Wilderness, this 77-mile-long river flows through a mixture of Forest Service and private lands, eventually emptying into the Snake River. Its outstandingly remarkable values include recreation, scenery, fisheries, wildlife, historic/prehistoric, vegetation/botanical and traditional value/lifestyles adaptation.</p>
<p>The river corridor supports unique plant communities and ecosystem diversity. Starting at 8,000 feet and descending to 950 feet, the river corridor contains most of the ecosystems found on the National Forest. Federally listed plants of interest include Wallowa primrose, fraternal paintbrush, Oregon oleander, MacFarlane's four o'clock, Geyer's onion and Hazel's leptodactylon.</p>
<p>The Imnaha River Canyon exhibits the economic and social history of the region and the American West. The Nez Perce and later Euro-Americans adapted to its climatic and geographic conditions, and the traditional Western farm and ranch lifestyle is still active today.</p>
<p>Visitors can access the river in a variety of ways. The upper section in the Eagle Cap Wilderness is best accessed by trails from the Indian Crossing Trailhead. Forest Service and county roads provide access to most segments below the wilderness boundary. Camping along the river on National Forest Service lands is available at the Indian Crossing, Blackhorse, and Ollokot Campgrounds (fee sites).</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>