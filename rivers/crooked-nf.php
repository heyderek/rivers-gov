<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Crooked River (North Fork), Oregon';

// Set the page keywords
$page_keywords = 'Ochoco National Forest, Prineville District, Bureau of Land Management, Crooked River, Oregon';

// Set the page description
$page_description = 'Crooked River (North Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('87');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Prineville District<br />
U.S. Forest Service, Ochoco National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its source at Williams Prairie to one mile from its confluence with the Crooked River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 12.2 miles; Scenic &#8212; 8.2 miles; Recreational &#8212; 13.3 miles; Total &#8212; 33.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/crooked-nf.jpg" alt="North Fork Crooked River" title="North Fork Crooked River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/north-fork-crooked-plan.pdf" alt="Crooked River (North Fork) Management Plan (10.4 MB PDF)" target="_blank">Crooked River (North Fork) Management Plan (10.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Crooked River (North Fork)</h2>

<p>From its source at Williams Prairie, the North Fork of the Crooked River in central Oregon flows through meadows, prairies and canyons before meeting up with the main stem Crooked River. Flowing for 33.7 miles (54.2 km), the North Fork of the Crooked River abounds with opportunities for enjoying remote outdoor experiences.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic</em></strong></p>

<p>Old-growth Ponderosa pine is the headline act in the river corridor. Other players among the wide assortment of species include upland sagebrush, juniper and mountain mahogany.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The canyon sections of the river provide relatively pristine opportunities for fishing, hiking, hunting and other semi-primitive experiences. The remoteness, solitude, natural beauty and a wide variety of flora and fauna contribute to the recreation values.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Outstanding scenic views include wet meadows, rocky cliffs and old-growth Ponderosa pine forests. Downstream from the river's confluence with Deep Creek, the landscape elements include steep-sided volcanic canyons interspersed with old-growth Ponderosa pine forests and riparian meadows.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Mule deer, elk, coyotes and various birds of prey use the river corridor for feeding, nesting, shelter, or travel, and bald eagles use the river corridor during the winter. Wild rainbow and redband trout occur throughout the river.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>