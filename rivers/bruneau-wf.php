<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Bruneau River (West Fork), Idaho';

// Set the page keywords
$page_keywords = 'Bruneau River, Idaho';

// Set the page description
$page_description = 'Bruneau River (West Fork), Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('183');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The West Fork of the Bruneau River from its confluence with the Jarbidge River to the downstream boundary of the Bruneau Canyon Grazing Allotment in the Southeast/Northeast quadrants of Section 5, Township 13 South, Range 7 East, Boise Meridian.</p>
<br />
<h3>Classification/Mileage: </h3>
<p>Wild &#8212; 0.4 miles; Total &#8212; 0.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/bruneau-wf.jpg" alt="West Fork Bruneau River" title="West Fork Bruneau River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14983/2" alt="West Fork Bruneau River (Bureau of Land Management)" target="_blank">West Fork Bruneau River (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Kevin Colburn</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Bruneau River (West Fork)</h2>

<p>The West Fork of the Bruneau River joins with the Jarbidge River to form the Bruneau River about 24 miles north of the Nevada border, just upstream of Indian Hot Springs. The canyon opens up at this portion of the river through the designated 0.3 miles and then becomes narrower as the Bruneau River flows north. The Jarbidge River becomes the Bruneau River where it is joined by the West Fork Bruneau.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>Native Americans have utilized the canyonlands for shelter, weaponry, fish and game, and water for thousands of years. Petroglyphs, pictographs, rock alignments, shrines and vision quest sites of the Shoshone and Paiute peoples are located throughout the Owyhee Canyonlands, and tribal members still frequent the canyonland areas to hunt, fish, pray and conduct ceremonies.</p>

<p>Tradition in the area claims that Bruneau was either named by its French translation of 'brown water' or after a French explorer by the name of Jean-Baptiste Bruneau.</p>

<p><strong><em>Ecologic</em></strong></p>

<p>The Bruneau River aphlox, a white flowered and matted plant that clings to ledges, rock crevices and cliffs, occurs in vertical or overhanging rhyolitic canyon walls, and the entire known extent of Bruneau River phlox in Idaho occurs within approximately 35 miles on the Bruneau, West Fork of the Bruneau and Jarbidge Rivers.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>The Bruneau River, and the Jarbidge River upstream, support both redband trout and threatened bull trout, the latter being the only fish in the Owyhee Canyonlands that is federally listed under the Endangered Species Act. Jarbidge River bull trout are important because they occupy a unique and unusual semi-arid desert ecological setting, and their loss would result in a substantial modification of the species' range.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Bruneau, Jarbidge and Owyhee river systems provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States. Though not unique to southwest Idaho, the presence of these geologic formations in such great abundance and aerial extent makes the designated river segments geologically unique from a national perspective.</p>

<p>The geology of the canyons has been shaped by a combination of volcanic activity, glacial melt and regional drainage patterns. The Bruneau-Jarbidge and Owyhee areas were the sites of the massive Owyhee-Humboldt and Bruneau-Jarbidge eruptive centers, whose explosions, massive rhyolite flows and related glacial flows carved out the canyons that expose this activity. Where overlying basalt is present, the rhyolite formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved immense monolithic cliffs and numerous sculptured pinnacles known as "hoodoos."</p>

<p>There is one small-scale jasper mining claim in the vicinity of Indian Hot Springs in the Bruneau River Canyon, just downstream of the West Branch flows into the Bruneau.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Bruneau and Jarbidge Rivers have a national reputation among paddlers for their outstandingly remarkable float boating and associated experiences along the designated river sections, notably multi-day river trip options that feature remote, challenging whitewater (Class III, IV and V). The West Fork should only be run with kayaks and only by expert kayakers; it is extremely challenging due to its small size, numerous Class IV and V rapids and potential for strenuous portages.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The West Fork Bruneau Canyon is dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Steep diagonal lines associated with talus slopes feature yellow and subdued green sagebrush-bunchgrass, dark green juniper, reddish rhyolite and coarse, blackish basalt rubble fields.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee Canyonlands provide riparian habitats for a number of wildlife species common to southwest Idaho. Big game species commonly found in the area include California bighorn sheep, elk, mule deer and pronghorn.</p>

<p>Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents (mice, kangaroo rats, voles, squirrels and chipmunks), rabbits, shrews, bats, weasels and skunks. River otters are supported by year-long flows and a good prey base (fisheries). Birds include songbirds, waterfowl, shorebirds and raptors.</p>

<p>This area includes Preliminary Priority Habitat for greater sage-grouse. Idaho BLM sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs support spotted and Townsend's big-eared bats.</p>	
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>