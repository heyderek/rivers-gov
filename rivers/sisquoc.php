<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sisquoc River, California';

// Set the page keywords
$page_keywords = 'Los Padres National Forest, Sisquoc River, San Rafael Wilderness, California';

// Set the page description
$page_description = 'Sisquoc River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('143');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Los Padres National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>June 19, 1992. From its origin downstream to the Los Padres National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 33.0 miles; Total &#8212; 33.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/sisquoc.jpg" alt="Sisquoc River" title="Sisquoc River" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/sisquoc-plan.pdf?recid=6891" title="Sisquoc River Management Plan" target="_blank" alt="St. Joe River (U.S. Forest Service)">Sisquoc River Management Plan</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sisquoc River</h2>
<p>The Sisquoc Wild and Scenic River is a 33-mile segment of the main river extending from its headwaters below the Buckhorn Road on the east to the forest boundary on the west. All but 1.5 miles of the west end lie within the San Rafael Wilderness. Elevations within the watershed range from 1,200 feet at the downstream terminus of the designated Wild River segment to 6,590 feet atop San Rafael Mountain. The climate is typified by warm, dry summers and cool, moist winters. Precipitation ranges from an annual rainfall of 13 to 38 inches with approximately 95% falling between November and April.</p>
<p>The Sisquoc River is free flowing without past or present diversions. The entire length of the river is natural, undisturbed and part of a larger area known as the San Rafael Wilderness. The river includes diverse landforms and vegetation ranging from wide floodplains in the lower segments to narrow rocky areas in the upper segments.</p>
<p>In the upper portion of the river, there are rocky areas that include falls and deep pools in the river supporting steelhead trout. The extensive riparian corridor along the Sisquoc River remains relatively natural and surrounded by a large wilderness area. This contiguous, intact and large protected ecosystem is rare in southern California. The river exists deep in a true wilderness. In the center sections of the river, one is far from outside help and communication. A person is able to experience the pioneer spirit as you are truly on your own.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>