<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'White Salmon River, Washington';

// Set the page keywords
$page_keywords = 'White Salmon River, Gifford Pinchot National Forest, Columbia Gorge National Scenic Area, Columbia River, Washington';

// Set the page description
$page_description = 'White Salmon River, Washington';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('167','61');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Columbia River Gorge National Scenic Area</p>
<br />
<h3>Designated Reach:</h3>
<p>November 17, 1986. From its confluence with Gilmer Creek, near the town of B Z Corner, to its confluence with Buck Creek.</p>
<p>August 2, 2005. White Salmon River from its headwaters to the boundary of the Gifford Pinchot National Forest. Cascade Creek from its headwaters to its confluence with the White Salmon River. This designation is not contiguous with the 1986 designation farther downstream.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 6.7; Scenic &#8212; 21.0 miles; Total &#8212; 27.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/white-salmon.jpg" alt="" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/crgnsa/specialplaces/?cid=stelprdb5182064" alt="White Salmon River (U.S. Forest Service)" target="_blank">White Salmon River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/white-salmon-plan.pdf" title="White Salmon River Management Plan" target="_blank">White Salmon River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Sue Baker</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>White Salmon River</h2>
<p>The White Salmon River is located in south-central Washington and its watershed is approximately 400-square miles. Churning rapids and unique beauty draw visitors to the clear, blue White Salmon River. Glacial waters combine with cold, clear springs, supporting a lush, green ribbon of plant life through the dry, pine-oak woodlands. Continuous rapids, waterfalls, and abrupt drops challenge boaters of advanced skill.</p>
<p><strong>Lower River:</strong> Numerous seeps and springs found along the canyon walls provide a consistently cold and sustained flow of water year around. The river drops 50 feet per mile. The five outstandingly remarkable values are whitewater boating (one of the few rivers in the region that has Class III rapids located in a natural setting runnable nearly year-round), the White Salmon River Gorge (the longest vertical wall gorge in the region noteworthy due to its natural character, bedrock geology, caves and numerous falls and springs), hydrology (sustained flows and waterfalls), resident fish (one of the three best resident rainbow trout fisheries in the region) and a Native American Indian longhouse site and cemetery.</p>
<p>Only 10 commercial outfitters are issued special use permits on the White Salmon. They offer whitewater rafting and kayaking trips. Trip documentation cards are filled out at the site by commercial and private trips to track use patterns; this provides documentation so improvements based on user needs can be planned. The put-in at BZ Corners was improved fall 2006 and provides parking, accessible restrooms with changing facilities, sinks and flush toilets (one vault toilet is open during winter months with no running water) and a host site. It is a day-use site (no overnight parking or camping). There are currently no fees charged to park or put in. A rail system is used to slide boats down to the water (cat-a-rafts find it easiest to use PVC pipe between the rail and their boat). Metal stairs at the river's edge are raised during high water.  Stone steps are located just down river; use them when metal stairs are up during high water. There are no fees at this is a day-use area.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>