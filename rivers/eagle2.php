<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Eagle Creek, Oregon';

// Set the page keywords
$page_keywords = 'Wallowa-Whitman National Forest, Eagle Creek, Eagle Lake, Wallowa Mountains, Oregon';

// Set the page description
$page_description = 'Eagle Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('75');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters below Eagle Lake to the Wallowa-Whitman National Forest boundary at Skull Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.5 miles; Scenic &#8212; 6.0 miles; Recreational &#8212; 18.4 miles; Total &#8212; 28.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/eagle-creek-2" alt="Eagle Creek (Wallowa-Whitman National Forest)" title="Eagle Creek (Wallowa-Whitman National Forest)" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="../documents/plans/eagle-creek-ww-plan.pdf" title="Eagle Creek Management Plan" target="_blank">Eagle Creek Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div><!--END #rivers-box -->

<div id="lower-content">
<h2>Eagle Creek (Wallowa-Whitman National Forest)</h2>
<p>The diversity of landforms, water, color and vegetation, which are present throughout the designated portion of Eagle Creek, are recognized as the most attractive attributes of the river corridor. The headwaters originate high in the glacial cirque in the Eagle Cap Wilderness. From its beginning at the outlet of Eagle Lake, the creek follows a steep gradient over small waterfalls and whitewater rapids as it descends from the mountains. Vegetation in the classic U-shaped glacial valley alternates between high mountain meadows and stands of subalpine fir and whitebark pine. Expansive views of the surrounding Wallowa mountains are afforded, and scoured rock outcrops create a highly diverse and dynamic landscape that vies attention with the crystal clear creek.</p>
<p>The valley floor becomes relatively flat and wide at Main Eagle Trailhead and, for the next five miles, Eagle Creek temporarily slows in its rapid descent from the high mountains. Clear, blue-green pools alternate with rapids as the creek winds its way through lush green, boulder-strewn meadows and park-like forests. Vegetation and canyon walls generally limit views to the immediate foreground, except for the breathtaking views of the mountains seen from the northern end of the road.</p>
<p>Eagle Creek leaves a landscape dominated by glacial features below its confluence with West Eagle Creek. For approximately the next 10 miles, the valley closes in and canyon walls become abruptly steep, towering 500-1000' above the valley floor in some places. Eagle Creek resumes its fast-moving, boulder-dominated descent through the narrow canyon, bordered by lush riparian vegetation and picturesque meadows. Dramatic rock formations extending from rim to canyon floor punctuate otherwise forested hillsides. The road paralleling Eagle Creek offers unrestricted views of the creek in the immediate foreground and surrounding hillsides.</p>
<p>As it enters the lower elevation basalt-dominated plateaus surrounding the Wallowa Mountains, the lower seven miles of the designated portion of Eagle Creek take on a character more typical of eastern Oregon rivers. Mixed conifer forests are replaced on drier slopes by grassy openings and park-like stands of ponderosa pine. Unusual rock formations provide visual contrast. By this time, Eagle Creek has become substantial in size from the contributions of several major tributaries, and alternates between rapids, short waterfalls, smooth swift stretches and deep blue pools.</p>
<p>Visitors can access the river in a variety of ways. The lower section of the river is accessible by Forest Service roads, where camping opportunities include; Eagle Forks, Tamarack, Two Color and Boulder Parks Campgrounds. There is also a riverside trail which originates near the Eagle Forks Campground, or midway up the lower river at the Martin Bridge Trailhead. Hiking access to the upper section in the wilderness is best from the Main Eagle Trailhead.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>