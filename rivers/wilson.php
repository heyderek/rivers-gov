<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wilson Creek, North Carolina';

// Set the page keywords
$page_keywords = 'Pisgah National Forest, Wilson Creek, North Carolina';

// Set the page description
$page_description = 'Wilson Creek, North Carolina';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('161');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, National Forests in North Carolina</p>
<br />
<h3>Designated Reach:</h3>
<p>August 18, 2000. From the headwaters below Calloway Peak to the confluence with Johns River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.6 miles; Scenic &#8212; 2.9 miles; Recreational &#8212; 15.8 miles; Total &#8212; 23.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wilson-creek.jpg" alt="Wilson Creek" title="Wilson Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/wilson-creek-plan-ea.pdf" alt="Wilson Creek Management Plan (1.7 MB PDF)" target="_blank">Wilson Creek Management Plan (1.7 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Dan Royall</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wilson Creek</h2>
<p>A two-year partnership, exemplifying bipartisan support between county governments, local communities and the U.S. Forest Service, culminated in designation of Wilson Creek on August 18, 2000. Jack Horan, Outdoors Editor for the Charlotte Observer, aptly describes Wilson Creek's special resources. "Kayakers and canoeists plunge through Boatbuster and Thunderhole Rapids. Anglers probe the greenish pools for brook, brown and rainbow trout. Sunbathers stretch out on massive boulders after a dip in the chilly waters. Wilson Creek long has been an easy-to-reach mountain oasis in the Pisgah National Forest. The free-flowing creek rises on the slopes of Grandfather Mountain in Avery County and, 20 miles later, makes a frantic dash through a 200-foot deep gorge before joining the Johns River in Caldwell County."</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>