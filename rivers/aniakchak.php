<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Aniakchak River, Alaska';

// Set the page keywords
$page_keywords = 'Aniakchak National Monument, Aniakchak River, Alaska';

// Set the page description
$page_description = 'Aniakchak River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('27');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Aniakchak National Monument and Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. That portion of the river&#8212;including its major tributaries, Hidden Creek, Mystery Creek, Albert Johnson Creek and the North Fork Aniakchak River&#8212;within the Aniakchak National Monument and Preserve.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 63.0 miles; Total &#8212; 63.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/aniakchak.jpg" alt="Aniakchak River" title="Aniakchak River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ania/" target="_blank" alt="Aniakchak National Monument" target="_blank">Aniakchak National Monument</a></p>

<div id="photo-credit">
<p>Photo Credit: Thomas O'Keefe</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Aniakchak River</h2>
<p>Lying entirely within Aniakchak National Monument and Preserve, you can float the river from inside the caldera of a volcano to the ocean past spectacular wildlife and geology. The river moves swiftly through a narrow gorge, and large rocks demand precise maneuvering. Only the most experienced rafters contemplate this float trip that takes three to four days to complete.</p>

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>