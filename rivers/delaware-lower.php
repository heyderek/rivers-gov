<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Delaware River (Lower), New Jersey, Pennsylvania';

// Set the page keywords
$page_keywords = 'Delaware River, New Jersey, Pennsylvania';

// Set the page description
$page_description = 'Delaware River (Lower), New Jersey, Pennsylvania';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('165');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Philadelphia Office</p>
<br />
<h3>Designated Reach:</h3>
<p>November 1, 2000. Several segments of the Delaware River and its tributaries: 1) from river mile 193.8 to the northern border of the city of Easton, Pennsylvania; 2) from just south of the Gilbert Generating Station to just north of the Point Pleasant Pumping Station; 3) from just south of the Point Pleasant Pumping Station to a point 1,000 feet north of the Route 202 Bridge; 4) from 1,750 feet south of the Route 202 Bridge to the southern boundary of the town of New Hope, Pennsylvania, to the town of Washington Crossing, Pennsylvania; 5) all of Tinicum Creek; 6) Tohickon Creek from the Lake Nockamixon Dam to the Delaware River; and 7) Paunacussing Creek in Solebury Township.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 25.4 miles; Recreational &#8212; 41.9 miles; Total &#8212; 67.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/delaware-lower-paunacussing-creek.jpg" alt="Paunacussing Creek" title="Paunacussing Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/lode/" alt="Lower Delaware Wild &amp; Scenic River (National Park Service)" target="_blank">Lower Delaware River (National Park Service)</a></p>
<p><a href="https://www.lowerdelawarewildandscenic.org/" alt="Lower Delaware Wild &amp; Scenic River" target="_blank">Lower Delaware River Information</a></p>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/lowerdelaware_pwsr_sub.html" alt="Lower Delaware River (National Park Service)" target="_blank">Lower Delaware River (National Park Service)</a></p>
<p><a href="https://www.delawarerivergreenwaypartnership.org" alt="Delaware River Greenway Partnership" target="_blank">Delaware River Greenway Partnership</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Delaware River (Lower)</h2>
<p>The lower Delaware River possesses a great diversity of significant resources. A high density of population and recreational opportunities combine with a wealth of natural, cultural and historic features of national significance. The river valley contains habitats that do not occur elsewhere in the region. For example, there are sheer cliffs that rise 400 feet above the river. Southern facing cliffs are desert-like and home to prickly pear cactus.  North-facing cliffs exhibit flora and fauna usually found only in arctic-alpine climates. The river itself provides habitat for American shad, striped bass, and river herring. The river is an important component of the Atlantic Flyway, one of four major waterfowl routes in North America. From an historic viewpoint, the river is one of the most significant corridors in the nation. The corridor contains buildings used during Washington's famous crossing, historic navigation canals, Native American and colonial era archaeological sites and mills. Just as important is the magnificent scenery in the river corridor.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>