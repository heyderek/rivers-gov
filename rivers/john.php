<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'John River, Alaska';

// Set the page keywords
$page_keywords = 'Gates of the Arctic National Park, John River, Alaska';

// Set the page description
$page_description = 'John River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('30');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Gates of the Arctic National Park and Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment within Gates of the Arctic National Park.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 52.0 miles; Total &#8212; 52.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/john.jpg" alt="John River" title="John River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/gaar/johnriver.htm/" alt="John River (National Park Service)" target="_blank">John River (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Peter Gorman</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>John River</h2>
<p>This river flows south from Anaktuvuk Pass in Alaska's Brooks Range. The valley is an important migration route for the Arctic Caribou Herd.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>