<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Dickshooter Creek, Idaho';

// Set the page keywords
$page_keywords = 'Dickshooter Creek, Idaho';

// Set the page description
$page_description = 'Dickshooter Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('186');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Dickshooter Creek from its confluence with Deep Creek to a point on the stream 1/4 mile due west of the east boundary of Section 16, Township 12 South, Range 2 West, Boise Meridian.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 9.3 miles; Total &#8212; 9.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/dickshooter-creek.jpg" alt="Dickshooter Creek" title="Dickshooter Creek" width="262px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14975/2" alt="Dickshooter Creek (Bureau of Land Management)" target="_blank">Dickshooter Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Judi Zuckert, BLM</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Dickshooter Creek</h2>

<p>Dickshooter Creek begins west of Dickshooter, Idaho, and flows south, cutting a narrow, deep gorge through the rolling plateau landscape until it joins Deep Creek. The waters of Dickshooter cease to flow by late-spring to early summer, leaving behind only isolated pools in the gravel streambed. This canyon provides outstanding hiking and backpacking opportunities.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>Deep Creek supports sensitive redband trout populations and several reptile and amphibian species (frogs, toads and salamanders).</p>

<p><strong><em>Geologic</em></strong></p>

<p>Dickshooter Creek is part of the Owyhee, Bruneau and Jarbidge river systems, which provide the largest concentration of rhyolite/basalt canyons in the western United States, products of Miocene Era (23 to 5 million years ago) volcanic formations. Where overlying basalt is present, rhyolite (rock which began as lava flow) formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved monolithic cliffs and numerous sculptured pinnacles known as "hoodoos."</p>

<p><strong><em>Recreational</em></strong></p>

<p>Outstanding day-hiking opportunities are available, especially during low-water periods. Due to its meandering character, diversity of landforms and topographic screening, Dickshooter Creek also provides exceptional opportunities for solitude and for primitive and physically challenging wildlife viewing and photography.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Big game found in the area include elk, mule deer, and pronghorn and bighorn sheep. Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents, rabbits, shrews, bats, weasels and skunks. Birds in the area include songbirds, waterfowl, shorebirds and raptors.</p>

<p>Dickshooter Creek offers Preliminary Priority Habitat for the greater sage-grouse. Other sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>