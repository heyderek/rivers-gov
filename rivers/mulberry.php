<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Mulberry River, Arkansas';

// Set the page keywords
$page_keywords = 'Ozark National Forest, St. Francis National Forest, Mulberry River, Boston Mountains, Arkansas';

// Set the page description
$page_description = 'Mulberry River, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('139');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ozark-St. Francis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992. From its origin to the Ozark National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 19.4 miles; Recreational &#8212; 36.6 miles; Total &#8212; 56.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/mulberry.jpg" alt="Mulberry River" title="Mulberry River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/mulberry-plan.pdf" alt="Mulberry River Management Plan (2.2 MB PDF)" target="_blank">Mulberry River Management Plan (2.2 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Robert Duggan/USFS</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Mulberry River</h2>
<p>The Mulberry River is located in Newton, Johnson and Franklin Counties, Arkansas. The river has been recognized by Arkansas Game and Fish Commission as one of the premier smallmouth and spotted bass fisheries in Arkansas. The Mulberry is also popular for canoeing, camping and swimming, offering Class I to II rapids.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>