<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Joseph Creek, Oregon';

// Set the page keywords
$page_keywords = 'Wallowa-Whitman National Forest, Joseph Creek, Oregon';

// Set the page description
$page_description = 'Joseph Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('80');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Joseph Creek Ranch, one mile downstream from Cougar Creek, to the Wallowa-Whitman National Forest boundary.</p>
<br />
<h3>Classification/Mileage: </h3>
<p>Wild &#8212; 8.6 miles; Total &#8212; 8.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/joseph-creek.jpg" alt="Joseph Creek" title="Joseph Creek" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="../documents/plans/joseph-creek-plan.pdf" title="Joseph Creek Management Plan" target="_blank">Joseph Creek Management Plan</a></p>

<div id="photo-credit">
<!--<p>Photo Credit:</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Joseph Creek</h2>
<p>Located in northeast Oregon, Joseph Creek's outstandingly remarkable values include scenery, recreation, geology, fish, wildlife and history. The spectacular natural setting, ruggedness, inaccessibility and steep topography of Joseph Creek and the surrounding environs of Joseph Canyon create a lasting impression on those who view it. The river corridor provides a spectacular example of the steep, rimrock-exposed canyons found in northeast Oregon.</p>
<p>Access to Joseph Canyon and Joseph Creek is limited due to remoteness, steep and rugged terrain and climatic conditions. Hiking, horsepacking, bird watching, wildlife viewing, fishing and big game hunting can be enjoyed in a solitary manner.</p>
<p>The canyon contains textbook-perfect examples of northeast Oregon geology, typified by Columbia River basalt canyons exposed by downcutting of rivers. The 2,000-foot-deep canyon is virtually unmodified and its spectacular details, such as steep sideslopes, basalt layers and dikes, can be easily viewed from the canyon rim.</p>
<p>Joseph Creek is an important wild steelhead and wild rainbow trout fishery. Wildlife includes bighorn sheep, deer, elk, black bear, river otter and cougar.</p>
<p>The canyon and the surrounding area play a vital role in Nez Perce Tribal history. Most important is the proximity of the river corridor to the winter gathering place for Chief Joseph and his band at the mouth of Joseph Creek.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>