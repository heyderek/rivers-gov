<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Bruneau River, Idaho';

// Set the page keywords
$page_keywords = 'Bruneau River, Idaho';

// Set the page description
$page_description = 'Bruneau River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('182');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The Bruneau River from the downstream boundary of the Bruneau-Jarbidge Wilderness to its upstream confluence with the West Fork of the Bruneau River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 38.7 miles; Recreational &#8212; 0.6 miles; Total &#8212; 39.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/bruneau.jpg" alt="Bruneau River" title="Bruneau River" width="262px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14972/2" alt="Bruneau River (Bureau of Land Management)" target="_blank">Bruneau River (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/sites/blm.gov/files/documents/files/Media-Center_Public-Room_Idaho_Bruneau-Jarbidge-Owyhee_BoaterGuide.pdf" alt="Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide" target="_blank">Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Bruneau River</h2>

<p>The Bruneau is formed from the Jarbidge River, which flows north from the mountains of northern Nevada through the beautiful basalt and rhyolite canyons of the Owyhee Uplands. There, it joins the West Fork of the Bruneau, then the East and West Forks join to form the mainstem. Forty miles of the Bruneau River are designated wild, and a 0.6-mile stretch at the Indian Hot Springs access point is designated recreational. The combination of sparkling water, steep multi-colored cliffs and an interesting association of plants and animals make this desert canyon one of superior natural beauty and recreational appeal. The Bruneau River also provides challenging whitewater as it flows through a deep, wild and remote desert canyon.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>Native Americans have utilized the canyonlands for shelter, weaponry, fish and game, and water for thousands of years. Petroglyphs, pictographs, rock alignments, shrines and vision quest sites of the Shoshone and Paiute peoples are located throughout the Owyhee Canyonlands. Tribal members still frequent the canyonland areas to hunt, fish, pray and conduct ceremonies.</p>

<p>Tradition in the Bruneau area claims that Bruneau was either derived from its French translation of 'brown water' or after a French explorer by the name of Jean-Baptiste Bruneau. Cowboys also left their marks on the sheltered canyon walls. A few homesteaders chose the canyons as a place that was well-suited for collecting water, hunting game and perhaps using nearby thermal pools.</p>

<p><strong><em>Ecologic</em></strong></p>

<p>The Bruneau River aphlox, a white flowered and matted plant that clings to ledges, rock crevices and cliffs, occurs in vertical or overhanging rhyolitic canyon walls, and the entire known extent of Bruneau River phlox in Idaho occurs within approximately 35 miles on the Bruneau, West Fork of the Bruneau and Jarbidge Rivers.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>The Bruneau and Jarbidge River upstream support both the redband trout and bull trout. The threatened bull trout is the only fish in the Owyhee Canyonlands that is federally listed under the Endangered Species Act. Jarbidge River bull trout are important because they occupy a unique and unusual semi-arid desert ecological setting, and their loss would result in a substantial modification of the species' range.</p>

<p>The lower (northern) approximate two miles of the Bruneau River near Hot Creek is habitat for the endangered Bruneau hot springsnail, threatened by the reduction and/or elimination of its geothermal habitat resulting from primarily agriculture-related groundwater withdrawal.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Bruneau, Jarbidge and Owyhee river systems provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States. Though not unique to southwest Idaho, their great abundance and aerial extent makes the designated river segments geologically unique from a national perspective.</p>

<p>The geology of the canyons has been shaped by a combination of volcanic activity, glacial melt and regional drainage patterns. The Bruneau-Jarbidge and Owyhee areas were the sites of two massive Owyhee-Humboldt and Bruneau-Jarbidge eruptive centers, whose explosions led to the creation of basins. Massive rhyolite flows and basaltic eruptions followed, and glacial flows carved out the canyons. Weathering and erosion have carved immense monolithic cliffs and numerous sculptured pinnacles known as "hoodoos."</p>

<p>There is one small-scale jasper mining claim in the vicinity of Indian Hot Springs in the Bruneau River Canyon, just downstream of where the Jarbidge flows into the Bruneau.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Bruneau and Jarbidge Rivers have a national reputation among paddlers for their outstandingly remarkable float boating and associated experiences along the designated river sections, notably multi-day river trip options that feature remote, challenging whitewater (Class III, IV and V). Only very experienced boaters should float the 69 miles of the Bruneau and Jarbidge rivers due to challenging rapids, log jams and other hazards.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Landscape forms on the Bruneau and throughout the Owyhee Canyonlands vary from broad, open sagebrush steppes to narrow canyons, some exceeding 800 feet in depth. The canyons are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Steep diagonal lines frame triangular forms associated with talus slopes which feature yellow and subdued green sagebrush-bunchgrasses, dark green juniper, medium-textured, reddish rhyolite rubble fields and coarse-textured, blackish basalt rubble fields. Large boulders and whitewater rapids are interspersed between the calm reaches.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee Canyonlands provide both upland and canyon riparian habitats for a number of wildlife species common to Southwest Idaho. It's common to find California bighorn sheep, elk, mule deer and pronghorn in this area. Large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents (mice, kangaroo rats, voles, squirrels, chipmunks), rabbits, shrews, bats, weasels and skunks. Birds include songbirds, waterfowl, shorebirds and raptors.</p>

<p>This area includes Preliminary Priority Habitat for the greater sage-grouse, bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>