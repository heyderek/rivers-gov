<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Fuller Mill Creek, California';

// Set the page keywords
$page_keywords = 'Fuller Mill Creek, California, San Bernardino National Forest';

// Set the page description
$page_description = 'Fuller Mill Creek, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('201');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, San Bernardino National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the source of Fuller Mill Creek in the San Jacinto Wilderness to its confluence with the North Fork San Jacinto River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 2.6 miles; Recreational &#8212; 0.9 miles; Total &#8212; 3.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/fuller-mill.jpg" alt="Fuller Mill Creek" title="Fuller Mill Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/sbnf/recreation/natureviewing/recarea/?recid=74097&actid=62" alt="Fuller Mill Creek (U.S. Forest Service)" target="_blank">Fuller Mill Creek (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Fuller Mill Creek</h2>
<p>The source of Fuller Mill Creek is on San Jacinto Peak in the Mount San Jacinto State Game Preserve and Wilderness Area. The Pacific Crest National Scenic Area Trail crosses its upper reaches. Above its confluence with the North Fork San Jacinto River is a picnic area and fishing access location. Hike upstream from here to see lovely waterfalls. While there is no camping in the river corridor, there are four campgrounds nearby.</p>
<p>The river supports habitat for many at-risk species, including mountain yellow-legged frogs, California spotted owls, the rubber boa and the San Bernardino flying squirrel.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>