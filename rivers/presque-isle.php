<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Presque Isle River, Michigan';

// Set the page keywords
$page_keywords = 'Ottawa National Forest, Presque Isle River, Michigan';

// Set the page description
$page_description = 'Presque Isle River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('127');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ottawa National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. The main stem from the confluence of the East and West Branches to Minnewawa Falls. The East Branch within the Ottawa National Forest. The South Branch within the Ottawa National Forest. The West Branch within the Ottawa National Forest.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 24.0 miles; Recreational &#8212; 48.0 miles; Total &#8212; 72.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/presque-isle.jpg" alt="Presque Isle River" title="Presque Isle River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.michigandnr.com/publications/pdfs/wildlife/viewingguide/up/05Presque/" alt="Michigan Department of Natural Resources" target="_blank">Michigan Department of Natural Resources</a></p>
<p><a href="../documents/plans/ottawa-nf-plan.pdf" alt="Presque Isle River Management Plan (1.4 MB PDF)" target="_blank">Presque Isle River Management Plan (1.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Steve Brimm, Brimmages.com</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Presque Isle River</h2>
<p>Portions of this river are considered by some as the most challenging whitewater in Michigan, if not in the Midwest. This river is listed as one of the ten North American rivers that "define the outer edge of contemporary whitewater paddling." Its outstandingly remarkable values include scenery, recreation, geology, fish and wildlife.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>