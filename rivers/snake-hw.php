<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Snake River Headwaters, Wyoming';

// Set the page keywords
$page_keywords = 'Snake River, Wyoming, Bridger-Teton National Forest';

// Set the page description
$page_description = 'Snake River Headwaters, Wyoming';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('206');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Bridger-Teton National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p><ol>
<li>Bailey Creek from the divide with the Little Greys River north to its confluence with the Snake River.</li>
<li>Blackrock Creek from its source to the Bridger-Teton National Forest boundary.</li>
<li>The Buffalo Fork of the Snake River consisting of:  the North Fork, Soda Fork, and the South Fork, upstream from Turpin Meadows; and the Buffalo Fork from Turpin Meadows to its confluence with the Snake River.</li>
<li>Crystal Creek from its source to its confluence with the Gros Ventre River.</li>
<li>Granite Creek from its source to the end of Granite Creek Road and from Granite Hot Springs to the point one mile upstream from its confluence with the Hoback River.</li>
<li>The Gros Ventre River from its source to the upstream boundary of Grand Teton National Park, excluding the section along Lower Slide Lake, and the segment flowing across the southern boundary of Grand Teton National Park to the Highlands Drive Loop Bridge.</li>
<li>The Hoback River from the point 10 miles upstream from its confluence with the Snake River to its confluence with the Snake River.</li>
<li>The Lewis River from Shoshone Lake to Lewis Lake and from the outlet of Lewis Lake to its confluence with the Snake River.</li>
<li>Pacific Creek from its source to its confluence with the Snake River.</li>
<li>Shoal Creek from its source to the point eight miles downstream from its source.</li>
<li>The Snake River from its source to Jackson Lake, from one mile downstream of Jackson Lake Dam to one mile downstream of the Teton Park Road Bridge at Moose, Wyoming, and from the mouth of the Hoback River to the point one mile upstream from the Highway 89 Bridge at Alpine Junction.</li>
<li>Willow Creek from the point 16.2 miles upstream from its confluence with the Hoback River to its confluence with the Hoback River.</li>
<li>Wolf Creek from its source to its confluence with the Snake River.</li>
</ol></p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 236.9 miles; Scenic &#8212; 141.5 miles; Recreational &#8212; 33.8 miles; Total &#8212; 412.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/snake-headwaters.jpg" alt="Snake River Headwaters" title="Snake River Headwaters" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/btnf/specialplaces/?cid=stelprdb5281115" alt="Snake River Headwaters (U.S. Forest Service)" target="_blank">Snake River Headwaters (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/snake-headwaters-plan-ea-nps-fws.pdf" title="Snake River Headwaters Management Plan - National Park Service" target="_blank">Snake River Headwaters Management Plan &#8211; National Park Service / U.S. Fish &amp; Wildlife Service</a></p>
<p><a href="../documents/plans/snake-headwaters-plan-usfs.pdf" title="Snake River Headwaters Management Plan - U.S. Forest Service" target="_blank">Snake River Headwaters Management Plan &#8211; U.S. Forest</a></p>

<div id="photo-credit">
<p>Photo Credit: Thomas O'Keefe</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Snake River Headwaters</h2>
<p>The Snake River Headwaters encompasses parts of Yellowstone and Grand Teton National Parks, the John D. Rockefeller Memorial Parkway and the Bridger-Teton National Forest. The river lies at the heart of the Greater Yellowstone Area (GYA), often referred to as one of the last intact functioning temperate ecosystems on earth (U.S. House of Representatives 1985; Keiter and Boyce 1991; Schullery 1997). Thus, the rivers included in the Snake River Headwaters Legacy Act of 2009 (PL 111-11) are among the most pristine in the nation. They have many outstandingly remarkable values and offer myriad recreational opportunities.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>