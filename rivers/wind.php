<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wind River, Alaska';

// Set the page keywords
$page_keywords = 'Wind River, Arctic National Wildlife Refuge, Alaska, Philip Smith Mountains, Chandalar River';

// Set the page description
$page_description = 'Wind River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('43');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Arctic National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment from its source, including all headwaters and one unnamed tributary in T13S, within the boundaries of the Arctic National Wildlife Refuge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 140.0 miles; Total &#8212; 140.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->

<img src="images/wind.jpg" alt="Wind River" title="Wind River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/wilson-creek-plan.pdf" alt="Wilson Creek Management Plan (1.7 MB PDF)" target="_blank">Wilson Creek Management Plan (1.7 MB PDF)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Sharon Seim</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wind River</h2>
<p>Beginning in the Philip Smith Mountains, this river offers a wide variety of vegetation, scenery and wildlife characteristic of a boreal forest on the south slope of the refuge. A small river with little human use, its remoteness provides for excellent populations of wildlife including moose, caribou, Dall sheep and grizzly bear.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>