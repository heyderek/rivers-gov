<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Little Beaver Creek, Ohio';

// Set the page keywords
$page_keywords = 'Little Beaver Creek, Ohio';

// Set the page description
$page_description = 'Little Beaver Creek, Ohio';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('SD3');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Ohio Department of Natural Resources, Division of Watercraft</p>
<br />
<h3>Designated Reach:</h3>
<p>October 23, 1975. The main stem from the confluence of the West Fork with the Middle Fork near Williamsport to the mouth. The North Fork from its confluence with Brush Run to its confluence with the main stem at Fredericktown. The Middle Fork from the vicinity of the County Road 901 (Elkston Road) bridge crossing to its confluence with the West Fork near Williamsport. The West Fork from the vicinity of the County Road 914 (Y-Camp Road) bridge crossing to its confluence with the Middle Fork near Williamsport.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 33.0 miles; Total &#8212; 33.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/little-beaver-creek.jpg" alt="Little Beaver Creek" title="Little Beaver Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://watercraft.ohiodnr.gov/scenic-rivers/list-of-ohios-scenic-rivers/little-beaver" alt="Little Beaver Creek (Ohio Department of Natural Resources)" target="_blank">Little Beaver Creek (Ohio Department of Natural Resources)</a></p>

<div id="photo-credit">
<p>Photo Credit: Ohio Scenic Rivers Program</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div> <!--END #rivers-box -->

<div id="lower-content">
<h2>Little Beaver Creek</h2>
<p>Little Beaver Creek represents some of the most rugged and wildest land in Ohio. Forested to a width of over a mile in places, this stream is home of the endangered hellbender (large aquatic salamander) and retains its original aquatic community. From ridges topping over 200 feet, there are breathtaking views of the valley below.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>