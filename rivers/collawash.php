<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Collawash River, Oregon';

// Set the page keywords
$page_keywords = 'Collawash River, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Collawash River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('178');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the headwaters of the East Fork of the Collawash River to Buckeye Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 11 miles; Recreational &#8212; 6.8 miles; Total &#8212; 17.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/collawash.jpg" alt="Collawash River" title="Collawash River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/scnf/passes-permits/recreation/?cid=fsbdev3_029568/" alt="Selway River Lottery" target="_blank">Selway River Lottery</a></p>
<p><a href="../documents/plans/clearwater-plan.pdf" alt="Clearwater River Management Plan (914 KB PDF)" target="_blank">Clearwater River Management Plan (914 KB PDF)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Collawash River</h2>
<p>The Collawash River is a major tributary to the Clackamas River on the west slopes of the Cascade Range in northwest Oregon. The scenic segment flows through a narrow, steeply sloped, well-dissected canyon that contains several cliffs and talus slopes. The river flows over and around many rocks, through pools and over one waterfall approximately 20-feet high. The river provides excellent spawning habitat for anadromous fish throughout the segment. The steep cascade area located in the lower portion of the segment acts as a partial barrier for most anadromous fish, though some have been known to migrate beyond it. Rainbow trout is the major resident species of the river. Spring Chinook, winter and summer steelhead and winter run coho are found within the lower end of the segment. The winter run coho is a rare native stock of salmon once found throughout the Columbia River drainage. This stock has been known to migrate above the falls.</p>
<p>In the recreational segment of the river there are a number of unstable earth flows. There is one area which could be considered a "textbook" example of a very active earth flow and could be easily interpreted.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>