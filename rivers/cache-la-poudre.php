<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Cache la Poudre River, Colorado';

// Set the page keywords
$page_keywords = 'Arapaho National Forest, Roosevelt National Forest, Cache la Poudre River, Colorado';

// Set the page description
$page_description = 'Cache la Poudre River, Colorado';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('57');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Arapaho and Roosevelt National Forests<br />
National Park Service, Rocky Mountain National Park</p>
<br />
<h3>Designated Reach:</h3>
<p>October 30, 1986. From Poudre Lake downstream to where the river intersects the easterly north-south line of the west 1/2 SW 1/4 of section 1, T8N, R71W of the sixth principal meridian. The South Fork from its source to section 1, T7N, R73W of the sixth principal meridian; from its intersection with the easterly section line of section 30, T8N, R72W of the sixth principal meridian to the confluence with the main stem.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 30.0 miles; Recreational &#8212; 46.0 miles; Total &#8212; 76.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/cache-la-poudre.jpg" alt="Cache la Poudre River" title="Cache la Poudre River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/cache-la-poudre-plan.pdf" target="_blank" alt="Cache la Poudre River Management Plan (1.5 MB PDF)" target="_blank">Cache la Poudre River Management Plan (1.5 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Martha Moran</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Cache la Poudre River</h2>
<p>The Cache la Poudre River is located east of the Continental Divide, in the northern Front Range of Colorado. It is affectionately known as the "Poudre" by local residents and long-time visitors to the area. The main and south forks of the Poudre originate in Rocky Mountain National Park, then flow north and east through the Roosevelt National Forest. The river eventually passes through the City of Fort Collins, then joins the south Platte River east of Greeley.</p>
<p>Historical accounts trace the name for the Cache la Poudre River to early French trappers who were caught in a snowstorm and buried their gunpowder in a cache near the mouth of the river. Most settlement of the area started in the 1870's and 1880's with mining and railroad building in northern Colorado. Trees were cut for railroad ties by "tie hackers" and then floated down the Poudre River. The attempted railroad up the Poudre Canyon was never completed, but many of the grades became the foundation for Highway 14. Early mining exploration had only marginal success as well, leaving ghost towns such as old Poudre City to tell the story.</p>
<p>The Poudre River has attracted rafters and kayakers since the 1950's. Rapids on the river are classified from Class I to VI under the International Whitewater Rating System and vary greatly according to water levels. The rafting season generally occurs from May through August.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>