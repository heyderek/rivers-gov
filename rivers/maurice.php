<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Maurice River, New Jersey';

// Set the page keywords
$page_keywords = 'Maurice River, Menantico Creek, Muskee Creek, Manumuskin River, Pinelands National Reserve, Pinelands, Delaware Estuary, New Jersey';

// Set the page description
$page_description = 'Maurice River, New Jersey';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('146');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Philadelphia Office</p>
<br />
<h3>Designated Reach:</h3>
<p>December 1, 1993. The segment from the U.S. Geological Survey Station at Shellpile to the south side of the Millville sewage treatment plant. The tributaries designated include: Menantico Creek from its confluence with the Maurice River to the base of the Impoundment at Menantico Lake; the Manumuskin River from its confluence with the Maurice to its headwaters near Route 557; and Muskee Creek from its confluence with the Maurice River to the Pennsylvania Reading Seashore Line Railroad Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 28.9 miles; Recreational &#8212; 6.5 miles; Total &#8212; 35.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/maurice.jpg" alt="Maurice River" title="Maurice River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/maurice_pwsr_sub.html" alt="Maurice River (National Park Service)" target="_blank">Maurice River (National Park Service)</a></p>
<p><a href="http://www.cumauriceriver.org" alt="Maurice River (Citizens United)" target="_blank">Maurice River (Citizens United)</a></p>

<div id="photo-credit">
<p>Photo Credit: Paul Kenney</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Maurice River</h2>
<p>In 1993, the Maurice River and several tributaries&#8212;including Menantico and Muskee Creeks and the Manumuskin River&#8212;were added to the National Wild and Scenic River System.</p>
<p>The Maurice (pronounced "morris") River corridor is an unusually pristine Atlantic Coastal river with national and internationally important resources. As part of the Atlantic Flyway, its clean waters and related habitats are vitally important to the migration of shorebirds, songbirds, waterfowl, raptors, rails and fish. Other important resources include a rare and endangered joint vetch, shortnose sturgeon, striped bass, and a pre-historic settlement site. Historically, the Maurice is home to a rich fishing, boating and oystering heritage. The Maurice River flows through what was once an oyster harvesting town; you can still see buildings and activities related to this industry. The river supports New Jersey's largest stand of wild rice and 53 percent of the animal species that New Jersey has recognized as endangered, excluding marine mammals.</p>
<p>The Maurice River is a critical link between the Pinelands National Reserve and the Delaware Estuary&#8212;both nationally and internationally important. The Maurice River corridor serves as the western boundary of the Pinelands and includes the cities of Vineland and Millville, and the Townships of Maurice River, Commercial and Buena Vista.</p>
<p>The Maurice National Scenic and Recreational River is located near the urban centers of Philadelphia, Pennsylvania; Trenton and Camden, New Jersey; and Wilmington, Delaware.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>