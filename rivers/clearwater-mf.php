<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Clearwater River (Middle Fork), Idaho';

// Set the page keywords
$page_keywords = 'Clearwater National Forest, Selway River, Lochsa River, Clearwater River, Idaho';

// Set the page description
$page_description = 'Clearwater River (Middle Fork), Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('1');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Bitterroot National Forest<br />
U.S. Forest Service, Clearwater National Forest<br />
U.S. Forest Service, Nez Perce National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968. The Middle Fork from the town of Kooskia upstream to the town of Lowell. The Lochsa River from its confluence with the Selway River at Lowell (forming the Middle Fork) upstream to the Powell Ranger Station. The Selway River from Lowell upstream to its origin.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 54.0 miles; Recreational &#8212; 131.0 miles; Total &#8212; 185.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/clearwater.jpg" alt="Middle Fork Clearwater River" title="Middle Fork Clearwater River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/wps/portal/fsinternet/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gDfxMDT8MwRydLA1cj72BTJw8jAwgAykeaxcN4jhYG_h4eYX5hPgYwefy6w0H24dcPNgEHcDTQ9_PIz03VL8iNMMgycVQEAHcGOlk!/dl3/d3/L2dJQSEvUUt3QS9ZQnZ3LzZfME80MEkxVkFCOTBFMktTNUJIMjAwMDAwMDA!/?ss=110117&navtype=BROWSEBYSUBJECT&cid=FSE_003741&navid=110000000000000&pnavid=null&position=BROWSEBYSUBJECT&recid=16478&ttype=recarea&name=Selway%2520River%2520Corridor&pname=Nez%2520Perce%2520National%2520Forest%2520-%2520Selway%2520River%2520Corridor" alt="Selway River (U.S. Forest Service)" target="_blank">Selway River (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/scnf/recreation/wateractivities/?cid=stelprdb5300640" alt="Selway River Lottery" target="_blank">Selway River Lottery</a></p>
<p><a href="http://www.fs.usda.gov/detail/nezperceclearwater/passes-permits/recreation/?cid=stelprd3839102" title="Outfitters &amp; Guides Permits" target="_blank">Outfitters &amp; Guides Permits</a></p>
<p><a href="../documents/plans/clearwater-plan.pdf" alt="Clearwater River Management Plan (914 KB PDF)" target="_blank">Clearwater River Management Plan (914 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Thomas O'Keefe</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Clearwater River (Middle Fork)</h2>
<p>The Middle Fork Clearwater includes the Lochsa and Selway Rivers, premier whitewater rivers. Part of the exploration route of Lewis and Clark follows the Lochsa River. Most of the Selway lies in Idaho's Selway-Bitterroot Wilderness. These rivers offer clear, clean water, beautiful scenery with great plant diversity and abundant wildlife.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>