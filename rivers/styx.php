<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'River Styx, Oregon';

// Set the page keywords
$page_keywords = 'Cave Creek, River Styx, Oregon, Oregon Caves National Monument';

// Set the page description
$page_description = 'River Styx, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('208');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Oregon Caves National Monument</p>
<br />
<h3>Designated Reach:</h3>
<p>December 19, 2014. The subterranean segment of Cave Creek flowing within Oregon Caves National Monument.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 0.4 miles; Total &#8212; 0.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/styx.jpg" alt="River Styx" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/orca/" alt="Oregon Caves National Monument" target="_blank">Oregon Caves National Monument (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: National Park Service</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>River Styx</h2>
<p>The River Styx is actually the underground segment of Cave Creek as it flows through Oregon Caves National Monument. This designation is unique within the National Wild and Scenic Rivers System as it is the first&#8212;and only&#8212;underground river in the National System.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>