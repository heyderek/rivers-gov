<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Saline Bayou, Louisiana';

// Set the page keywords
$page_keywords = 'Saline Bayou, Kisatchie National Forest, Louisiana';

// Set the page description
$page_description = 'Saline Bayou, Louisiana';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('58');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Kisatchie National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 30, 1986. From Saline Lake upstream to the Kisatchie National Forest.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 19.0 miles; Total &#8212; 19.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/saline-bayou.jpg" alt="Saline Bayou" title="Saline Bayou" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/kisatchie/recreation/natureviewing/recarea/?recid=34851&actid=62" alt="Saline Bayou Water Trail (U.S. Forest Service)" target="_blank">Saline Bayou Water Trail (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Bill Hyams</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Saline Bayou</h2>
<p>The Saline Bayou National Scenic River Corridor is located west of Louisiana Highway 1233. The northern boundary is just north of Louisiana Highway 126 and its southern boundary is Saline Lake. The river supports a variety of recreational uses that include canoeing, boating, fishing, hunting, camping, hiking and photography. There are various woods roads and highway entry points to the bayou where a visitor can launch a canoe, camp, fish, or take a hike within this beautiful southern ecosystem. It is a peaceful setting, where bald cypress and other hardwoods growing along the banks are often reflected in the bayou's quiet water. A visitor should watch for wood ducks, pileated woodpeckers, otters and many other wildlife species. This meandering bayou supports a wide and diverse variety of wildlife and plant species and offers a rare opportunity to experience a range of ecosystems ranging from upland pine forest to cypress tupelo swamps. Saline Bayou is treasured for its beautiful scenery and many scenic photos have been taken along this beautiful river.</p>
<p>There are five launch sites, Cloud Crossing Campground, a picnic pavilion (Cloud Crossing) and the Saline Bayou hiking trail associated with the river.  Canoeing and boating challenges and natural hazards, such as flooding, depend on the water conditions and time of year. Check with the Winn Ranger District for river condition updates prior to visits.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>