<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Battle Creek, Idaho';

// Set the page keywords
$page_keywords = 'Battle Creek, Idaho';

// Set the page description
$page_description = 'Battle Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('180');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Battle Creek from its confluence with the Owyhee River to the upstream boundary of the Owyhee River Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 23.4 miles; Total &#8212; 23.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/battle-creek.jpg" alt="Battle Creek" title="Battle Creek" width="262px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14970/2" alt="Battle Creek (Bureau of Land Management)" target="_blank">Battle Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Battle Creek</h2>

<p>Battle Creek is a 67-mile-long tributary of the Owyhee River. It flows in a generally southern direction through the Owyhee Desert to meet the Owyhee River west of Riddle, Idaho. The 23 miles designated as wild thread through a 1/8-mile-wide, 200-foot-deep canyon shouldered by nearly continuous vertical walls of rhyolite (rock created from lava flow). For the next 20 miles, the canyon widens to no more than 3/8-mile across and deepens to 500 feet. Tucked between the cliffs and stream channel is a lush riparian area of willow, chokecherry, dogwood, alder, rose, currant, sedges and grasses.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Ecologic</em></strong></p>

<p>Battle Creek is home to the Owyhee River forget-me-not. Found nowhere else but this area, this species occupies north-facing vertical rhyolitic cliffs, sheltered crevices and shady grottos. The pale blue flowers of this species contrast sharply with the backdrop of dark volcanic rock. May and June are the best time to view this species in full flower. The Owyhee River forget-me-not can be found with other cliff-dwelling species, such as mountain snowberry, red alumroot, prickly phlox and desert gooseberry.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Outstandingly remarkable fisheries values are defined as the ability of a given stream segment to support populations of endangered, threatened, or BLM sensitive fish species. Redband trout meet this definition and are found in Battle Creek, as well as other designated tributaries of the Owyhee River.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Battle Creek is part of the Owyhee, Bruneau and Jarbidge river systems. They provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States, products of Miocene Era (23 to 5 million years ago) volcanic formations. Where overlying basalt is present, rhyolite formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved monolithic cliffs and numerous sculptured pinnacles known as "hoodoos."</p>

<p><strong><em>Recreational</em></strong></p>

<p>Class II-rated Battle Creek is not floatable due to its limited access. However, its meandering character, diversity of landforms and topographic interest provide exceptional opportunities for solitude and for primitive hiking, wildlife viewing and photography for those floating the Owyhee River.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Landscape forms throughout the Owyhee Canyonlands vary from broad, open sagebrush steppes to narrow canyons dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Intertwined with the vertical features are some very steep diagonal lines that frame triangular forms associated with talus slopes. The slopes look like a mosaic of yellow and subdued green from the sagebrush, bunchgrasses, dark green juniper, and reddish and blackish rubble fields found there.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Big game found in the area include elk, mule deer, pronghorn and bighorn sheep. Collectively, Battle Creek, Deep Creek, Owyhee River, Duncan Creek and Wickahoney Creek support the majority of the bighorn sheep population in the Owyhee Canyonlands.</p>

<p>Birds in the area include songbirds, waterfowl and shorebirds. The high, well-fractured and eroded canyon cliffs are considered outstanding habitat for cliff nesting raptors, a small number of which occasionally winter along the canyon walls. Other wildlife include snake and lizard species and a few amphibians-frogs, toads and salamanders.</p>

<p>Battle Creek and other areas in the Owyhee region are considered Preliminary Priority Habitat for greater sage grouse. Idaho BLM sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>