<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Pecos River, New Mexico';

// Set the page keywords
$page_keywords = 'Pecos River, New Mexico, Pecos Bill, Santa Fe National Forest';

// Set the page description
$page_description = 'Pecos River, New Mexico';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('110');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Santa Fe National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>June 6, 1990. From its headwaters to the townsite of Terrerro.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 13.5 miles; Recreational &#8212; 7.0 miles; Total &#8212; 20.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/pecos.jpg" alt="Pecos River" title="Pecos River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/pecos-plan.pdf" alt="Pecos River Management Plan (906 KB PDF)" target="_blank">Pecos River Management Plan (906 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Pecos River</h2>
<p>The Pecos River, famous in the folklore of the frontier, flows out of the Pecos Wilderness, through rugged granite canyons and waterfalls, and passes small, high-mountain meadows. The wild and scenic river corridor has been a popular recreation setting for many decades. Its popularity continues to increase because it provides a cool, forested environment and waterway within the arid southwest; it also offers beautiful scenery and high-quality fishing. Some of the most popular recreation activities are hiking, camping, backpacking, hunting and fishing. The wild segment is entirely in the Pecos Wilderness and provides opportunities for remote, primitive activities. The recreational segment is located along State Highway 63 and offers a range of day and overnight activities from Terrero to Cowles.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>