<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Smith River, California';

// Set the page keywords
$page_keywords = 'Smith River, Jedediah Smith Redwoods State Park, Smith River National Recreation Area, California';

// Set the page description
$page_description = 'Smith River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('111','112','113','114','115','SD10');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>California Resources Agency<br />
U.S. Forest Service, Six Rivers National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>January 19, 1981 and November 16, 1990. The segment from the confluence of the Middle Fork Smith River and the North South Fork Smith River to its mouth at the Pacific Ocean, including Rowdy Creek, Mill Creek, West Branch Mill Creek, East Fork Mill Creek, Bummer Lake Creek, Dominie Creek, Savoy Creek and Little Mill Creek. The Middle Fork from its the headwaters to its confluence with the North Fork Smith River, including Myrtle Creek, Shelly Creek, Kelly Creek, Packsaddle Creek, the East Fork of Patrick Creek, the West Fork Patrick Creek, Little Jones Creek, Griffin Creek, Knopki Creek, Monkey Creek, Patrick Creek, and Hardscrabble Creek. The Siskiyou Fork from its headwaters to its confluence with the Middle Fork, including the South Siskyou Fork of the Smith River. The South Fork from its headwaters to its confluence with the main stem, including Williams Creek, Eightmile Creek, Harrington Creek, Prescott Fork, Quartz Creek, Jones Creek, Hurdygurdy Creek, Gordon Creek, Coon Creek, Craigs Creek, Goose Creek, the East Fork of Goose Creek, Buck Creek, Muzzleloader Creek, Canthook Creek, Rock Creek, and Blackhawk Creek. The North Fork from the California-Oregon border to its confluence with the Middle Fork of the Smith River, including Diamond Creek, Bear Creek, Still Creek, the North Fork of Diamond Creek, High Plateau Creek, Stony Creek, and Peridotite Creek.</p>
<br />
<h3>Classification/Mileage: </h3>
<p>Wild &#8212; 78.0 miles; Scenic &#8212; 31.0 miles; Recreational &#8212; 216.4 miles; Total &#8212; 325.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/smith-ca.jpg" alt="Smith River, Jedediah Smith Redwoods State Park" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">

<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/smith-plan.pdf" alt="Smith River Management Plan (MB PDF)" target="_blank">Smith River Management Plan (MB PDF)</a><br />
<a href="http://smithriveralliance.org" title="Smith River Alliance" target="_blank">Smith River Alliance</a><br />
<a href="http://www.youtube.com/watch?v=_7hpQH_91cE" alt="Smith River Video" target="_blank">Smith River Video by Randy Johnson &#8211; Excellent Introduction to the Smith River</a><br />
<a href="../documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Smith River Eligibility Report" target="_blank">Smith River Eligibility Report</a><br />
<a href="../documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Smith River Environmental Impact Statement" target="_blank">Smith River Environmental Impact Statement</a></p>

<div id="photo-credit">
<p>Photo Credit: Jon Parmentier</p>

</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Smith River</h2>

<p>The Smith River was added to the National Wild and Scenic Rivers System in 1981 at the request of the Governor of California. In 1990, the portions of the Smith River on the Six Rivers National Forest were redesignated through the public law that created the Smith River National Recreation Area. More than 300 miles of the Smith River drainage are designated wild and scenic, making it one of the most complete river systems in the National System. The emerald-green Smith River flows freely and naturally, without a single dam for its entire length. It is the only major system in California to do so.</p>

<p>The Smith River National Recreation Area (NRA) is located in the northwest corner of California and is managed by the Six Rivers National Forest. The NRA was created to protect the area's special scenic value, natural diversity, cultural and historical attributes, wilderness, wildlife, fisheries and the Smith River watershed's clean waters. The Forest Service has been designated as the steward of the NRA to provide recreational opportunities and to manage this diverse area for all of its valuable resources. Below the National Forest, the Smith River and designated tributaries flow through Jedediah Smith Redwoods State Park and Redwoods National Park and on to the Pacific Ocean.</p>

<p>The Smith River, its three forks and countless creeks drain a beautiful, rugged terrain. The NRA encompasses more than 450-square-miles of densely forested mountains, pristine botanical areas, remote wilderness landscapes, high-mountain lakes and steep, rocky canyons. The river is an important stream for fish, with towering trees along its banks provide shaded conditions necessary for cold-water species.</p>

<p>Winter rains provide the whitewater conditions so avidly sought by kayakers. The Smith River abounds with Class IV and V rapids on all three forks and has many miles of steep creeking waters. Its miles of whitewater require a fairly high degree of technical skills by the boater. Once the three forks join to form the mainstem, the land levels out, and the last 16 miles to the ocean present less demanding conditions (Class I-II in medium flows).</p>

<p>Just past the confluence of the Middle Fork and South Fork, the river leaves the NRA and flows through the Redwood National and State Parks, with stunning views of the giant redwoods and great summer floating in Class I and II waters. The Smith River offers surprises for even the most seasoned boater&#8212;some rapids may change little for many years, others are different every year and each season offers something new.</p>

<p>Other recreational opportunities include fish watching, steelhead and salmon fishing, swimming or snorkeling in deep natural pools of clear and emerald water, tubing and rafting, hiking along backcountry and wilderness trails, and mountain biking. Sightseeing is also popular; wildlife and beautiful flowers are abundant&#8212;Pacific dogwoods, azaleas, rhododendrons, ladyslipper orchids and lilies. In the fall, stunning fall colors leap out in patches amongst the dark greens of the conifers.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>