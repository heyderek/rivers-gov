<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Housatonic River, Connecticut';

// Set the page keywords
$page_keywords = 'Housatonic River, Connecticut, National Park Service';

// Set the page description
$page_description = 'Housatonic River, Connecticut';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('305');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<!--<?php
// includes ESRI
include ('../iframe.php');
?>-->

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Study Agency:</h3>
<p>National Park Service</p>
<br />
<h3>Study Segments:</h3>
<p>November 11, 2016 (Section 2(a)(ii) Application by Governor Malloy). From the Massachusetts/Connecticut border downstream to Boardman Bridge in New Milford, Connecticut.</p>
<br />
<h3>Mileage:</h3>
<p>41.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/roaring-sf.jpg" alt="South Fork Roaring River" title="South Fork Roaring River" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/studies/housatonic-2aii-application.pdf" title="Governor Malloy's 2(a)(ii) Application" target="_blank" alt="Governor Malloy's 2(a)(ii) Application">Governor Malloy's 2(a)(ii) Application</a></p>
<p><a href="../documents/studies/housatonic-federal-register-notice-2aii-application.pdf" title="Federal Register Notice of the 2(a)(ii) Study" target="_blank" alt="Federal Register Notice of the 2(a)(ii) Study">Federal Register Notice of the 2(a)(ii) Study</a></p>
<p><a href="../documents/studies/housatonic-study.pdf" title="Original Study of the Housatonic River (1979)" alt="Original Study of the Housatonic River (1979)" target="_blank">Original Study of the Housatonic River (1979)</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Martha Moran</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Housatonic River</h2>
<p>On November 11, 2016, Governor Malloy of Connecticut applied for designation of the Housatonic River under Section 2(a)(ii) of the Wild and Scenic Rivers Act. He requested that the river be managed and administered through the local Housatonic River Commission. The National Park Service is reviewing the proposed nomination to determine whether the river meets the requirements for eligibility and suitability.  A draft of the NPS's findings in the form of an evaluation and environmental assessment will be published for public and agency comment.</p>
<p>The Housatonic River basin lies principally in western Connecticut and southwestern Massachusetts, with small sections extending into southeastern New York. This area is well known for its charming rural character, historical heritage and natural beauty, which is remarkable considering its proximity to the northeastern megalopolis. Based on the preliminary evidence of free-flowing river conditions and the presence of multiple natural, cultural and recreational resources with the potential to meet the "Outstandingly Remarkable Values" threshold as defined by the Wild and Scenic Rivers Act, the Housatonic River is being considered for inclusion in the National Wild and Scenic Rivers System as a state-administered river under Section 2(a)(ii) of the Wild and Scenic Rivers Act.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>