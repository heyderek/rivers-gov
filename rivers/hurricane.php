<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Hurricane Creek, Arkansas';

// Set the page keywords
$page_keywords = 'Ozark National Forest, St. Francis National Forest, Hurricane Creek, Hurricane Creek Wilderness, Arkansas';

// Set the page description
$page_description = 'Hurricane Creek, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('137');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ozark-St. Francis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992. From its origin to its confluence with Big Piney Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 2.4 miles; Scenic &#8212; 13.1 miles; Total &#8212; 15.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/hurricane-creek.jpg" alt="Hurricane Creek" title="Hurricane Creek" width="265px" height="204px" />-->
<p>We could use a photo. <br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/hurricane-creek-plan.pdf" alt="Hurricane Creek Management Plan" target="_blank">Hurricane Creek Management Plan</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: National Park Service</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Hurricane Creek</h2>
<p>A tributary to Big Piney Creek, this stream flows through the Hurricane Creek Wilderness. Its outstanding scenery is characterized by sharp ridges and cliffs, unusual rock formations and clear reflecting pools. The stream is clear, exhibiting interesting ripple-pool patterns, while meandering through richly diverse vegetation with dominant overstory of beech in some reaches. Bristle fern, which is listed as threatened by the Arkansas Natural Heritage Commission, is located within the river corridor inside Hurricane Creek Wilderness.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>