<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Tuolumne River, California';

// Set the page keywords
$page_keywords = 'Tuolumne River, Yosemite National Park, Stanislaus National Forest, California';

// Set the page description
$page_description = 'Tuolumne River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('53');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Mother Lode Field Office<br />
National Park Service, Yosemite National Park<br />
U.S. Forest Service, Stanislaus National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>September 28, 1984. The main stem from its source to the Don Pedro Reservoir.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 47.0 miles; Scenic &#8212; 23.0 miles; Recreational &#8212; 13.0 miles; Total &#8212; 83.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/tuolumne.jpg" alt="Tuolumne River" title="Tuolumne River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/yose/" alt="Yosemite National Park (National Park Service)" target="_blank">Yosemite National Park (National Park Service)</a></p>
<p><a href="http://www.fs.usda.gov/recarea/stanislaus/recarea/?recid=14975" target="_blank" alt="Tuolumne River (U.S. Forest Service)">Tuolumne River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/tuolumne-plan.pdf" title="Tuolumne River Management Plan" target="_blank">Tuolumne River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Michael Carl</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Tuolumne River</h2>

<p>The Tuolumne River, designated in 1984, originates high in the Sierra Nevada on the eastern side of Yosemite National Park and flows westward for 62 miles before it continues into Stanislaus National Forest. The river has two principal sources: 1) the Dana Fork, which drains the west-facing slopes of Mount Dana; and 2) the Lyell Fork, which begins at the base of the glacier on Mount Lyell. The two forks converge at the eastern end of Tuolumne Meadows, one of the largest subalpine meadows in the Sierra Nevada. The Tuolumne River meanders through Tuolumne Meadows and then cascades through the Grand Canyon of the Tuolumne before it enters the eastern end of Hetch Hetchy Reservoir (still within the park, but not part of the National Wild and Scenic Rivers System). Below O'Shaughnessy Dam, the river again is included in the National Wild and Scenic Rivers System as it continues through a low-elevation meadow and rocky gorge.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural &amp; Historic</em></strong></p>

<p>The rich archeological landscape along the Tuolumne River reflects thousands of years of travel, settlement and trade. Parsons Memorial Lodge, a national historic landmark sited near the Tuolumne River, commemorates the significance of this free-flowing segment of the river in inspiring conservation activism and protection of the natural world on a national scale.</p>

<p><strong><em>Fisheries &amp; Wildlife Habitat</em></strong></p>

<p>In Tuolumne Meadows, Dana Meadows and along the Lyell Fork, the Tuolumne River sustains one of the most extensive Sierra Nevada complexes of subalpine meadows and riparian habitats with relatively high biological integrity. Poopenaut Valley contains a type of low-elevation riparian and wetland habitat that is rare in the Sierra Nevada.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Rock types of the upper Tuolumne River are chiefly granites; three major intrusive periods in the development of the Sierra Nevada have contributed different granitic varieties. Metamorphic remnants occur at higher elevations, such as the slate of Mt. Lyell and the limestone of Mt. Dana. Visitors can witness volcanic rocks at Tuolumne Meadows and glacial deposits at Lumsden Campground. Below Early Intake, granites have weathered, and at the South Fork confluence, they give way to Calaveras Formation metasedimentary rock. Schists and slates with limestone bands characterize the rocks of the lower Tuolumne River, and gold occurs in this metamorphic belt, as well.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The unique recreational feature of the Lower Tuolumne is whitewater boating. The section from Lumsden Bridge to Wards Ferry provides one of the finest boating experiences in the nation. It combines a series of demanding rapids spaced at close intervals with the power and waves of larger rivers and requires no portages during the 18-mile run.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Lyell Canyon offers remarkable and varied views of lush meadows, a meandering river, a U-shaped glacially carved canyon and surrounding peaks. The Grand Canyon of the Tuolumne offers views of a deep, rugged canyon with vast escarpments of granite, hanging valleys and long cascades of falling water.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>