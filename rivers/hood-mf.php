<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Hood River (Middle Fork), Oregon';

// Set the page keywords
$page_keywords = 'Hood River, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Hood River (Middle Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('173');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the confluence of Clear and Coe Branches to the north section line of Section 11, Township 1 South, Range 9 East.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 3.7 miles; Total &#8212; 3.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/hood-mf.jpg" alt="Middle Fork Hood River" title="Middle Fork Hood River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/pgdata/content/ak/en/prog/nlcs/gulkana_nwr.html" alt="Gulkana River (Bureau of Land Management)" target="_blank">Gulkana River (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Hood River (Middle Fork)</h2>
<p>The Middle Fork Hood River has its origins in several glaciers on the north slope of Mt. Hood in Oregon's Cascade Range. The Clear Branch, Coe Branch and Eliot Branch join to form the Middle Fork Hood River near the Parkdale Lava Beds. The river flows in a northerly direction, joins the West and the East Fork and eventually flows into the Columbia River near the town of Hood River, Oregon. The 3.7-mile segment of the Middle Fork Hood River&#8212;from the confluence of Clear and Coe Branches to the north section line of section 11, township 1 south, range 9 east&#8212;is administered as a scenic river. Outstandingly remarkable values are geology/hydrology, ecology/botany and recreation.</p>
<p>The river is bounded on the east side by the Parkdale Lava Beds, an excellent example of an A'a (pronounced "ah ah") type of lava flow which is typified by rough, jagged and cindery surfaces. Large deposits of stream and lake sediments at the upper end of the lava flow indicate that the river was once dammed by the lava flow. High-quality flows of this nature are rare for the region and can be considered a "textbook" example which can be easily studied and interpreted.</p>
<p>The lava flow provides an excellent example of successional stages taking place in the reestablishment of vegetative cover on the lava flow. The southern, or upper, end of the flow already has trees and other vegetation becoming reestablished. The northern, or lower, end of the flow is still virtually barren. The diversity throughout the lava flow provides a unique display of natural processes in action in one location.</p>
<p>While recreation use in the area is currently limited to dispersed day or overnight use primarily by local users, the area has a high potential for interpreting volcanic processes that could attract users from around the region. The lava flow is easily accessible and is an excellent example of some of the volcanic forces that helped form the Cascade Mountain Range.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>