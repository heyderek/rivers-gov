<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Obed River, Tennessee';

// Set the page keywords
$page_keywords = 'Obed River, National Park Service, Tennessee';

// Set the page description
$page_description = 'Obed River, Tennessee';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('15');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Obed Wild and Scenic River</p>
<br />
<h3>Designated Reach:</h3>
<p>October 12, 1976. The segment from the western edge of the Catoosa Wildlife Management Area to the confluence with the Emory River. Clear Creek from the Morgan County line to the confluence with the Obed River. Daddys Creek from the Morgan County line to the confluence with the Obed River. The Emory River from the confluence with the Obed River to Nemo Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 43.3 miles; Recreational &#8212; 2.0 mile; Total &#8212; 45.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/obed-clear-creek.jpg" alt="Clear Creek" title="Clear Creek" width="265" height="204" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/obed/" alt="Obed Wild and Scenic River (National Park Service)" target="_blank">Obed Wild and Scenic River (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Obed River</h2>
<p>The Obed River and its two main tributaries, Clear Creek and Daddys Creek, cut into the Cumberland Plateau of East Tennessee, providing some of the most rugged scenery in the Southeast. Wild whitewater running through 500-foot deep gorges offers boaters the chance to drift below rock climbers challenging their skills on the sandstone bluffs.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>