<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Richland Creek, Arkansas';

// Set the page keywords
$page_keywords = 'Ozark National Forest, St. Francis National Forest, Richland Creek, Richland Creek Falls, Richland Creek Wilderness, Arkansas';

// Set the page description
$page_description = 'Richland Creek, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('141');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ozark-St. Francis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992. From its origin to the northern boundary of section 32, T14N, R18W.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 5.3 miles; Scenic &#8212; 11.2 miles; Total &#8212; 16.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/richland-creek.jpg" alt="Richland Creek" title="Richland Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/richland-creek-plan.pdf" alt="Richland Creek Management Plan (2.4 MB PDF)" target="_blank">Richland Creek Management Plan (2.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Robert Duggan</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Richland Creek</h2>
<p>Richland Creek is located in Newton and Searcy Counties, Arkansas. Richland Creek Falls, Twin Falls, an upland swamp, fossiliferous limestones and smallmouth bass fishing are some of the features of this Ozark Mountain's stream as it flows through the Richland Creek Wilderness.</p>
<p>Canoeing, kayaking, horseback riding, camping, picnicking, swimming and fishing are the primary forms of recreation. The Richland Recreation Area is heavily used during the summer months for swimming and fishing. The exposed bedrock of the river displays geological formations which are very important to understanding the stratigraphic synthesis of North Arkansas. Exposed fossiliferous limestones and shales represent some of the youngest Mississippian-age rocks in North America.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>