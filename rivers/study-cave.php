<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Cave Creek, Oregon';

// Set the page keywords
$page_keywords = 'Cave Creek, Oregon, National Park Service';

// Set the page description
$page_description = 'Cave Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('304');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<!--<?php
// includes ESRI
include ('../iframe.php');
?>-->

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Study Agency:</h3>
<p>National Park Service</p>
<br />
<h3>Study Segments:</h3>
<p>December 19, 2014 (Public Law 113-291). Cave Creek from the River Styx to the boundary of the Rogue River-Siskiyou National  Forest. Lake Creek from its headwaters at Bigelow Lakes to the confluence with Cave Creek. No Name Creek from its headwaters to the confluence with Cave Creek. Panther Creek from its headwaters to the confluence with Lake Creek. Upper Cave Creek from its headwaters to the confluence with the River Styx.</p>
<br />
<h3>Mileage:</h3>
<p>8.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/cave-creek.jpg" alt="Lake Creek" title="Lake Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.nps.gov/orca/" title="Oregon Caves National Monument" alt="Oregon Caves National Monument" target="_blank">Oregon Caves National Monument</a></p>
<p><a href="https://www.rivers.gov/rivers/styx.php" title="River Styx" target="_blank" alt="River Styx">River Styx National Wild &amp; Scenic River</a></p>

<div id="photo-credit">
<p>Photo Credit: Lake Creek, National Park Service</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Cave Creek</h2>
<p>Cave, Lake, No Name and Panther Creeks are part of the geologic marvel of the Oregon Caves system. Sometims above ground, other times below, this system of streams is a fragile, fascinating world.</p>
<p>The upper Cave Creek watershed is located within the Oregon Caves National Monument and Preserve. The pristine waters are part of the headwater tributaries of the Illinois River, one of the last major undammed rivers in the Pacific Northwest. The complex, dynamic cave and riparian ecosystems are dependent on the continued existence and integrity of these waters.</p>
<p>The study streams are picturesque with opportunities to view the waterfalls, boulders, glacial cirques, dense vegetation, and hanging lakes. Glacial features include ice-carved lakes (tarns), subalpine ponds, glacially displaced boulders, windblown loess deposits, hanging valleys, faceted boulders, and residual rock piles known as moraines. Oregon Caves National Monument and Preserve is a premier site for concentrated geodiversity and is a great place to view the complex array of geology within a short amount of time.</p>
<p>The Port Orford cedar, the ecologically dominant riparian species, provides dense shading that contributes to cold temperatures and outstanding water quality. The scenic Port Orford cedar has distinctive lacy, graceful, and low-hanging foliage. Visitors get a sense of immersion and solitude when walking through sections of dense Port Orford cedar.</p>
<p>A draft wild and scenic river study report for these creeks will be available for public review in spring 2018.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>