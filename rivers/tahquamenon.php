<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Tahquamenon River (East Branch), Michigan';

// Set the page keywords 
$page_keywords = 'Hiawatha National Forest, Tahquamenon River, Michigan';

// Set the page description
$page_description = 'Tahquamenon River (East Branch), Michigan';

// Set the region for Sidebar Images 
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('130');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Hiawatha National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From its origin to the Hiawatha National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 3.2 miles; Recreational &#8212; 10.0 miles; Total &#8212; 13.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->

<!--<img src="images/tahquamenon.jpg" alt="Tahquamenon River" title="Tahquamenon River" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/suasco_pwsr_sub.html" alt="Sudbury, Assabet &amp; Concord Rivers (National Park Service)" target="_blank">Sudbury, Assabet &amp; Concord Rivers (National Park Service)</a></p>-->

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Tahquamenon River (East Branch)</h2>
<p>The East Branch is a high-quality brook trout stream that rewards persistent anglers who overcome the dense alder along much of its length. Fall is a special time to visit the river when the vibrant colors of Michigan hardwoods are reflected in the dark, tannin-stained water. Outstandingly remarkable values include excellent fishing and unique hydrological resources.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>