<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Chattooga River, Georgia, N. Carolina, S. Carolina';

// Set the page keywords
$page_keywords = 'Chattahoochee National Forest, Chattooga River, Georgia, North Carolina, South Carolina';

// Set the page description
$page_description = 'Chattooga River, Georgia, N. Carolina, S. Carolina';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('10');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Chattahoochee National Forest<br />
U.S. Forest Service, Nantahala National Forest<br />
U.S. Forest Service, Sumter National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>May 10, 1974. The segment from 0.8 miles below Cashiers Lake in North Carolina to the Tugaloo Reservoir. The West Fork from its confluence with the main stem upstream 7.3 miles.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 41.6 miles; Scenic &#8212; 2.5 miles; Recreational &#8212; 14.6 miles; Total &#8212; 58.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/chattooga.jpg" alt="Chattooga River" title="Chattooga River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/scnfs/recreation/wateractivities/recarea/?recid=47159&actid=79" target="_blank" alt="Chattooga River (U.S. Forest Service)" target="_blank">Chattooga River (U.S. Forest Service)</a></p>
<p><a href="http://www.chattooga-river.net" target="_blank" alt="Chattooga River Network" target="_blank">Chattooga River Network</a></p>

<div id="photo-credit">
<p>Photo Credit: Doug Whittaker</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Chattooga River</h2>
<p>Flowing through three states and the Ellicott Rock Wilderness, the Chattooga is recognized as one of the Southeast's premier whitewater rivers. It begins in mountainous North Carolina as small rivulets, nourished by springs and abundant rainfall. High on the slopes of the Appalachian Mountains is the start of a 50-mile journey that ends at Lake Tugaloo between South Carolina and Georgia, dropping almost 1/2-mile in elevation.</p>
<p>The river is one of the few remaining free-flowing streams in the Southeast. The Chattooga offers outstanding scenery, ranging from thundering falls and twisting rock-choked channels to narrow, cliff-enclosed deep pools. The setting is primitive; dense forests and undeveloped shorelines characterize the primitive nature of the area. No motorized vehicles are permitted within a corridor about 1/4-mile wide on either side of the river. Visitors must rely on their own skills and strength rather than on motorized equipment. Man-made facilities are minimal, consisting primarily of hiking trails.</p>
<p>The river's outstandingly remarkable values include recreation, biology, scenery, geology and history.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>