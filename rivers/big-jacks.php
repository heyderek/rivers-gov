<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Big Jacks Creek, Idaho';

// Set the page keywords
$page_keywords = 'Big Jacks Creek, Idaho';

// Set the page description
$page_description = 'Big Jacks Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('181');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Big Jacks Creek from the downstream border of the Big Jacks Creek Wilderness in Section 8, Township 8 South, Range 4 East, to the point at which it enters the Northwest 1/4 of Section 26, Township 10 South, Range 2 East, Boise Meridian.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 35.0 miles; Total &#8212; 35.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/big-jacks-creek.jpg" alt="Big Jacks Creek" title="Big Jacks Creek" width="262px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14971/2" alt="Big Jacks Creek (Bureau of Land Management)" target="_blank">Big Jacks Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Judi Zuckert, BLM</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Big Jacks Creek</h2>

<p>Big Jacks Creek is located in Owyhee County, Idaho. Beginning north of Riddle, it flows north and east through the Big Jacks Creek Wilderness, ultimately flowing into the Bruneau River. Enveloped by sheer and terraced canyon walls, this perennial stream is surrounded by riparian vegetation. Redband trout are found in the creek, and bighorn sheep inhabit the canyon. The area is remote, accessible only by a few trails and dirt roads.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Ecologic</em></strong></p>

<p>The area is home to another rare plant, the Owyhee River forget-me-not. Found nowhere else but here, this species occupies north-facing vertical rhyolitic cliffs, sheltered crevices and shady grottos. The pale blue flowers of this species contrast sharply with the backdrop of dark volcanic rock. May and June are the best time to view this species in full flower.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Big Jacks Creek is outstandingly remarkable from a fisheries population and habitat standpoint. When considered in combination with Little Jacks, Cottonwood and Duncan Creeks, the streams are among the 17% of desert streams in the Northern Basin and Range identified as aquatic-habitat strongholds for redband trout, a BLM sensitive species and a state of Idaho species of special concern.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Big Jacks Creek, and the wilderness through which it flows, is notable for its solitude and limitless character. The easiest way to get into the area is via a series of dirt roads that wind around the southern part of the wilderness. Most of the hiking trails wander up and down the canyons because the volcanic countryside is uneven, deeply carved by erosive forces.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The landscape is a mix of streams, plateaus and rugged canyons. Canyons in this region are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Intertwined with the vertical features are steep diagonal lines that frame triangular forms associated with talus slopes. Slopes feature a mosaic of medium-textured, yellow and subdued green sagebrush-bunchgrass communities and/or dark green juniper, as well as either medium-textured, reddish rhyolite rubble fields or coarse-textured, blackish basalt rubble fields.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The wilderness in the Big Jacks Creek Basin consists of rugged canyons, streams and plateaus that provide habitat for redband trout, mountain quail, sage hens, mountain lions, mule deer, pronghorns, bobcats, coyotes and bighorn sheep.</p>

<p>Big Jacks Creek is included in the area of Preliminary Priority Habitat for greater sage-grouse. Idaho BLM sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>