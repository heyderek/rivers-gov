<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Merced River, California';

// Set the page keywords 
$page_keywords = 'Merced River, Yosemite National Park, Sierra National Forest, California';

// Set the page description
$page_description = 'Merced River, California';

// Set the region for Sidebar Images 
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('62');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Mother Lode Field Office<br />
National Park Service, Yosemite National Park<br />
U.S. Forest Service, Sierra National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 2, 1987 and October 23, 1992. From its source (including Red Peak Fork, Merced Peak Fork, Triple Peak Fork, and Lyle Fork) in Yosemite National Park to a the normal maximum operating pool (water surface level) of Lake McClure (elevation 867 feet mean sea level). The South Fork from its source in Yosemite National Park to the confluence with the main stem.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 71.0 miles; Scenic &#8212; 16.0 miles; Recreational &#8212; 35.5 miles; Total &#8212; 122.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/merced.jpg" alt="Merced River" title="Merced River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/yose/" alt="Yosemite National Park (National Park Service)" target="_blank">Yosemite National Park (National Park Service)</a><br />
<a href="../documents/plans/merced-plan-blm.pdf" title="Merced River Management Plan (Bureau of Land Management)" target="_blank">Merced River Management Plan (Bureau of Land Management)</a> <a href="../documents/plans/merced-plan.pdf" title="Merced River Management Plan (U.S. Forest Service)" target="_blank">Merced River Management Plan (U.S. Forest Service)</a><br />
<a href="../documents/plans/merced-eis.pdf" title="Merced River Management Plan EIS (U.S. Forest Service)" target="_blank">Merced River Management Plan EIS (U.S. Forest Service)</a><br />
<a href="../documents/plans/merced-rod.pdf" title="Merced River Management Plan ROD (U.S. Forest Service)" target="_blank">Merced River Management Plan ROD (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Merced River</h2>

<p>From its source on the south side of Mount Lyell at 13,114 feet, through a glacially carved canyon within Yosemite National Park, the river flows downstream to Lake McClure Reservoir. Seventy-one of the total length of 79 miles are protected with wild and scenic river designation. The Merced, including the South Fork, flows through exceptional scenery&#8212;glaciated peaks, lakes and alpine and subalpine meadows&#8212;in alternating pools and cascades. Wildflower displays are spectacular. The South Fork possesses one of the few remaining pristine Sierra Nevada fisheries with self-sustaining populations of rainbow, eastern brook and brown trout. Archeology and wildlife are also noteworthy.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural &amp; Historic</em></strong></p>

<p>Historical artifacts create an area of historic and cultural significance along the river. The continuum of human use along the Merced River and South Fork of the Merced River encompasses millennia of diverse peoples, cultures and uses. American Indian and late 19th-century American cultures flourished along these rivers, because they provided reliable, year-round water. Evidence that reflects trade, travel and settlement patterns abounds in an intricate and interconnected landscape of archeological sites representing this cultural history.</p>

<p>The Wawona historic resources includes one of the few covered bridges in the region. The National Historic Landmark Wawona Hotel Complex includes one of the largest existing Victorian hotel complexes in a national park and one of the few remaining that remains largely representative of its original configuration.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Merced River is the product of geologic and hydrologic processes that continue to shape the landscape. Glaciation and river erosion, coupled with the influence of bedrock fractures, carved pathways that the Merced River continues to follow and create the river's variable gradients and dramatic changes in water speed and volume. The river flows through glacially carved canyons, over sheer cliffs and stair-step river morphology, through an alluvial landscape in Yosemite Valley and past a classic boulder bar in El Portal.</p>

<p><strong><em>Recreational</em></strong></p>

<p>From the high country of Yosemite National Park, the Merced River pursues its headlong rush through glacially carved canyons, rugged mountains and foothills to the San Joaquin Valley. The river flows in a series of rapids to Lake McClure at approximately 900 feet elevation. Outstanding whitewater rafting, camping and hiking opportunities contribute to the Merced's outstandingly remarkable recreation, made special against uniquely dramatic, picturesque backdrops or wilderness, high granite cliffs, and towering waterfalls.</p>

<p><strong><em>Vegetation</em></strong></p>

<p>Riparian and meadow ecosystems within the river corridor include alpine and subalpine meadows along the river stretches. Dependent on these habitats are a variety of native, endemic and/or rare plant and animal species, such as the threatened limestone salamander. The Merced River sustains numerous small meadows and riparian habitat with high biological integrity.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Designated in 1986 as the Limestone Salamander Area of Critical Environmental Concern (ACEC), BLM-administered lands encompass 1,600 acres of confirmed and potential limestone salamander habitat, including adjacent BLM lands along the Merced River and its tributaries in western Mariposa County. A state-listed threatened species and former Federal Category 2 candidate, the limestone salamander's home is restricted to 15 population sites along a 20-mile stretch of the Merced River between the headwaters of Lake McClure, near the community of Bagby, and the mouth of Sweetwater Creek, near Briceburg. This species occurs nowhere else in the world.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>