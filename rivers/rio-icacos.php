<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Rio Icacos, Puerto Rico';

// Set the page keywords
$page_keywords = 'Caribbean National Forest, Icacos, El Yunqe National Forest, Puerto Rico';

// Set the page description
$page_description = 'Rio Icacos, Puerto Rico';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'tropical';

//ID for the rivers
$river_id = array('166C');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Caribbean National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 19, 2002. From its headwaters to the boundary of the Caribbean National Forest.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 2.3 miles; Total &#8212; 2.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rio-icacos.jpg" alt="Rio Icacos" title="Rio Icacos" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detailfull/elyunque/home/?cid=fsbdev3_042977&width=full#wild_scenic" alt="Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)" target="_blank">Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/el-yunque-plan.pdf" alt="Rio Icacos Management Plan (2.5 MB PDF)" target="_blank">Rio Icacos Management Plan (2.5 MB PDF)</a></p>
<p><a href="../documents/plans/el-yunque-plan-ea.pdf" alt="Rio Icacos Management Plan Environmental Assessment (2.0 MB PDF)" target="_blank">Rio Icacos Management Plan Environmental Assessment (2.0 MB PDF)</a></p>
<p><a href="../documents/plans/el-yunque-plan-decision-notice.pdf" alt="Rio Icacos Management Plan (454 KB PDF)" target="_blank">Rio Icacos Management Plan (454 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Rio Icacos</h2>
<p>The Rio Icacos has some of the most varied terrain of any of the forest rivers. The stream gradient is less steep near its headwaters elevation at 2,158 feet (658 meters) than farther downstream in contrast to all rivers on the forest. The stream exhibits a unique sandy bed due to its upper flatter sections.  Contrasting sharply is the downstream segment, which more closely resembles other rivers in the forest with huge jumbled boulders, rapids and steep gradients.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>