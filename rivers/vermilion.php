<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Vermilion River (Middle Fork), Illinois';

// Set the page keywords
$page_keywords = 'Vermilion River, Illinois';

// Set the page description
$page_description = 'Vermilion River (Middle Fork), Illinois';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('SD13');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Illinois Department of Natural Resources</p>
<br />
<h3>Designated Reach:</h3>
<p>May 11, 1989. From river mile 46.9 near Collison downstream to river mile 29.8 at the Conrail Railroad crossing north of U.S. Highway 150.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 17.1 miles; Total &#8212; 17.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/vermilion.jpg" alt="Vermilion River" title="Vermilion River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.dnr.illinois.gov/Parks/About/Pages/MiddleFork.aspx" alt="Vermilion Scenic River (Illinois Department of Natural Resources)" target="_blank">Vermilion Scenic River (Illinois Department of Natural Resources)</a></p>
<p><a href="http://www.middleforkriver.org" alt="Middle Fork Citizens' Organization" target="_blank">Middle Fork Citizens' Organization</a></p>
<p><a href="http://prairierivers.org/articles/2010/05/the-middle-fork-of-the-vermilion-river-illinois-state-scenic-river/" alt="Vermilion River (Prairie Rivers Network)" target="_blank">Vermilion River (Prairie Rivers Network)</a></p>

<div id="photo-credit">
<p>Photo Credit: Thomas O'Keefe</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Vermilion River (Middle Fork)</h2>
<p>The river meanders deeply through Illinois' Grand Prairie glacial deposits, exposing scenic, steep, valley slopes with high bluffs of geological note. The valley's unique flora and fauna are highlighted in several adjacent natural areas and nature preserves, both forested and prairie, along with the river's recreational and historical characteristics. The majority of the designated river segment is publicly owned with a wide variety of access opportunities.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>