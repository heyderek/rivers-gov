<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Willamette River (N. Fork of the Mid. Fork), Oregon';

// Set the page keywords
$page_keywords = 'Willamette Wild and Scenic River, Willamette National Forest, Oregon';

// Set the page description
$page_description = 'Willamette River (N. Fork of the Mid. Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('90');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Willamette National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Waldo Lake to the Willamette National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 8.8 miles; Scenic &#8212; 6.5 miles; Recreational &#8212; 27.0 miles; Total &#8212; 42.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/willamette-mf.jpg" alt="North Fork Middle Fork Willamette River" title="North Fork Middle Fork Willamette River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/or/resources/recreation/site_info.php?siteid=19" alt="Kiger Gorge Overlook (Bureau of Land Management)" target="_blank">Kiger Gorge Overlook (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Willamette River (North Fork of the Middle Fork)</h2>
<p>The North Fork of the Middle Fork of the Willamette River was designated as a wild and scenic river in 1988. Outstandingly remarkable values for the river include recreation, vegetation, scenic, water quality, fish, wildlife, geologic/hydrologic and historic.</p>
<p>The river corridor offers a diversity of recreation settings ranging from developed campgrounds to wilderness. Aufderheide Drive, designated as one of the original 50 of the National Forest Scenic Byways, and the southernmost portion of the West Cascades National Scenic Byway parallel the river for some 30 miles, offering access and views of the river to byway travelers and bicyclists.</p>
<p>The North Fork is one of the few rivers in western Oregon managed for wild trout&#8212;fly fishing only&#8212;by the Oregon Department of Fish and Wildlife. The vegetation along the river is diverse, because of its elevation range from 1100 to 5400 feet and diversity of landforms from marshy bogs to dry rock gardens.</p>
<p>Water quality is one of the most outstanding attributes of the North Fork, as its source, Waldo Lake, is regarded by some experts as one of the purest in the world. Roosevelt elk herds use the river corridor through the year, as do blacktail deer, black bear and cougar. The river cuts through two distinct geologic periods&#8212;the High Cascade, from 6,000 to 1 million years ago, and the Western Cascade, from 15 million to 25 million years ago.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>