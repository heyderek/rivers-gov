<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Crooked River, Oregon';

// Set the page keywords
$page_keywords = 'Ochoco National Forest, Chetco National Forest, Prineville District, Bureau of Land Management, Crooked River, Oregon';

// Set the page description
$page_description = 'Crooked River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('72');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Prineville District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the National Grassland boundary to Dry Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 17.8 miles; Total &#8212; 17.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/crooked.jpg" alt="Crooked River" title="Crooked River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<!--<p><a href="http://www.blm.gov/or/resources/recreation/site_info.php?siteid=147" alt="Crooked River (Bureau of Land Management)" target="_blank">Crooked River (Bureau of Land Management)</a></p>-->
<p><a href="../documents/plans/middle-deschutes-lower-crooked-plan.pdf" title="Lower Crooked River Management Plan" target="_blank">Lower Crooked River Management Plan</a></p>
<p><a href="../documents/plans/crooked-chimney-rock-plan.pdf" title="Chimney Rock Segment Crooked River Management Plan" target="_blank">Chimney Rock Segment Crooked River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Northwest Rafting Company (www.nwrafting.com)</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Crooked River</h2>

<p>The Crooked River is noted for its ruggedly beautiful scenery, outstanding whitewater boating and a renowned sport fishery for steelhead, brown trout and native rainbow trout. Located in central Oregon, it offers excellent hiking opportunities with spectacular geologic formations and waterfalls.  A portion of the designated segment provides expert class IV-V kayaking/rafting during spring runoff.  The section of river from the Ochoco National Forest to Opal Springs flows through scenic vertical basalt canyons. The Chimney Rock segment is becoming increasingly popular for the accessibility of outdoor activities.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic &amp; Ecologic</em></strong></p>

<p>In addition to supporting a wide variety of botanical resources, the Crooked River possesses a unique stand of mature white alder/red-osier dogwood in an area that is in near-pristine condition and is also suspected to contain of Estes' wormwood.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Fifty million years of geologic history are dramatically displayed on the canyon walls of the Crooked River. Volcanic eruptions which occurred over thousands of years created a large basin dramatized by colorful layers of basalt, ash and sedimentary formations. The most significant contributor to the outstandingly remarkable geologic resource are the unique intra-canyon basalt formations created by recurring volcanic and hydrologic activities.</p>

<p><strong><em>Hydrologic</em></strong></p>

<p>Water from springs and stability of flows through the steep basalt canyon section of the Crooked River has created a stream habitat and riparian zone that is extremely stable and diverse, unique in a dry semi-arid climate environment. Features, such as Odin, Big and Steelhead Falls; springs and seeps; white water rapids; water sculpted rock; and the river canyons, are very prominent and represent excellent examples of hydrologic activity within central Oregon.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Crooked offers a diversity of year-round recreation opportunities, such as fishing, hiking, backpacking, camping, wildlife and nature observation, whitewater boating, picnicking, swimming, hunting and photography. The Chimney Rock segment is popular for camping, fishing, hiking, bicycling and for viewing eagles, ospreys and other wildlife. The 2.6-mile (round trip) hike to Chimney Rock rewards visitors with expansive views of the Crooked River Canyon and Cascades. The lower section offers a semi-primitive experience due to its remoteness, and a portion of the river is noted for high quality class IV-V whitewater paddling.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The exceptional scenic quality along the Crooked River is due to the rugged natural character of the canyons, outstanding scenic vistas, limited visual intrusions and scenic diversity resulting from a variety of geologic formations, vegetation communities and dynamic river characteristics. State Scenic Highway 27, a designated National Back Country Byway, offers views of western juniper decorating the steep hillsides, spectacular geologic formations and eroded lava flows throughout the narrow, winding canyon corridor.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The river supports critical mule deer winter range habitat and nesting/hunting habitat for bald eagles, golden eagles, ospreys and other raptors. Bald eagles are known to winter within the Crooked River segment and along the Deschutes River downriver from the Lower Bridge. Outstanding habitat areas include high vertical cliffs, wide talus slopes, numerous caves, pristine riparian zones and extensive grass/sage covered slopes and plateaus.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>