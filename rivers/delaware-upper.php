<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Delaware River (Upper), New York, Pennsylvania';

// Set the page keywords
$page_keywords = 'National Park Service, Delaware River, New York, Pennsylvania';

// Set the page description
$page_description = 'Delaware River (Upper), New York, Pennsylvania';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('19');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Upper Delaware Scenic and Recreational River</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978. The segment from the confluence of the East and West Branches below Hancock, New York, to the existing railroad bridge immediately downstream of Cherry Island in the vicinity of Sparrow Bush, New York.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 23.1 miles; Recreational &#8212; 50.3 miles; Total &#8212; 73.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/upper-delaware.jpg" alt="Stone Arch Bridge, Ten Mile River" title="Stone Arch Bridge, Ten Mile River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/upde/" alt="Upper Delaware Scenic and Recreational River (National Park Service)" target="_blank">Upper Delaware Scenic and Recreational River (National Park Service)</a></p>
<p><a href="../documents/plans/upper-delaware-plan.pdf" target="_blank">Upper Delaware River Management Plan (20.5MB PDF)</a></p>
<p><a href="http://www.trailkeeper.org" target="_blank">Sullivan County Trailkeeper</a></p>

<div id="photo-credit">
<p>Photo Credit: David Soete</p>

</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Delaware River (Upper)</h2>
<p>This 73-mile stretch of river flows between Hancock and Sparrow Bush, New York, along the Pennsylvania border. The Roebling Bridge, believed to be the oldest existing wire cable suspension bridge, spans the river. The Zane Grey House and museum are located here.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>