<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'White Clay Creek, Delaware, Pennsylvania';

// Set the page keywords
$page_keywords = 'White Clay Creek, Delaware, Pennsylvania';

// Set the page description
$page_description = 'White Clay Creek, Delaware, Pennsylvania';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('163A','163B');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Philadelphia Office<br />White Clay Creek Watershed Association</p>
<br />
<h3>Designated Reach:</h3>
<p>October 24, 2000. Numerous river segments (including tributaries of White Clay Creek and all second order tributaries of the designated segments) in the states of Delaware and Pennsylvania. The main stem, excluding Lamborn Run but including all second order tributaries outside the boundaries of the White Clay Creek Preserve and White Clay Creek State Park: 1) from the confluence of the East and Middle Branches in London Britain Township, Pennsylvania, to the northern boundary of the city of Newark, Delaware; 2) from the Paper Mill Road to the Old Route 4 Bridge; 3) from the southern boundary of United Water Delaware Corporation's property to the confluence with the Christina River. The East Branch, including Trout Run, from its headwaters within West Marlborough Township to 500 feet north of the Borough of Avondale's wastewater treatment facility. The East Branch, including Walnut Run and Broad Run, from the southern boundary of the Borough of Avondale to the New Garden Township boundary. The East Branch from the northern boundary of London Britain Township to the confluence of the Middle and East Branches. The Middle Branch from its headwaters within Londonderry Township to the downstream boundary of the White Clay Creek Preserve, excluding 500 feet on either side of the Borough of West Grove's wastewater treatment facility. The West Branch from its headwaters within Penn Township to the confluence with the Middle Branch. The Middle Run from its headwaters to its confluence. All of Pike and Mill Creeks.</p>
<p>December 19, 2014. Lengthened the East Branch designation from the southern boundary of the Borough of Avondale to the  boundary of the White Clay Creek Preserve, including Walnut Run, Broad Run and Eqypt Run. Added the Lamborn Run to the main stem designation (see the October 24, 2000, designation of the main stem of White Clay Creek above).</p>
<p><a href="../documents/white-clay-creek-map.pdf" target="_blank">See A Map Of White Clay Creek Designation (PDF)</a><br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 31.4 miles; Recreational &#8212; 167.6 miles; Total &#8212; 199.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/white-clay-creek.jpg" alt="White Clay Creek" title="White Clay Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/whiteclaycreek_pwsr_sub.html" alt="White Clay Creek (National Park Service)" target="_blank">White Clay Creek (National Park Service)</a></p>
<p><a href="../documents/plans/white-clay-creek-plan.pdf" alt="White Clay Creek Management Plan" target="_blank">White Clay Creek Management Plan</a></p>
<p><a href="http://www.destateparks.com/park/white-clay-creek/" alt="White Clay Creek State Park (Delaware State Parks)" target="_blank">White Clay Creek State Park (Delaware State Parks)</a></p>
<p><a href="http://www.dcnr.state.pa.us/stateparks/findapark/whiteclaycreek/" alt="White Clay Creek Preserve (Pennsylvania State Parks)" target="_blank">White Clay Creek Preserve (Pennsylvania State Parks)</a></p>
<p><a href="http://www.whiteclay.org" alt="White Clay Creek Watershed Management Committee" target="_blank">White Clay Creek Watershed Management Committee</a></p>
<p><a href="http://mercury.ccil.org/~wcwa/" alt="White Clay Creek Watershed Association" target="_blank">White Clay Creek Watershed Association</a></p>

<div id="photo-credit">
<p>Photo Credit: Thomas O'Keefe</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>White Clay Creek</h2>
<p>In 1991, citizens of the White Clay Creek area requested that the creek and its tributaries be considered for inclusion in the National Wild and Scenic Rivers System. The White Clay Creek study represents the first time an entire watershed was studied for designation. In 2000, the White Clay Creek and several tributaries&#8212;including parts of the East Branch and all of West Branch, Pike and Mill Creeks, and Middle Run&#8212;were added to the National Wild and Scenic Rivers System. Building on that success, another nine miles of the watershed were designated in 2014.</p>
<p>The White Clay Creek Scenic &amp; Recreational River flows through southwestern Chester County, Pennsylvania, and northwestern New Castle County, Delaware. The urban center of Newark, Delaware, is located in the southern end of the watershed.</p>
<p>The White Clay Creek watershed is an exceptional resource in the bi-state area, renowned for its scenery, opportunities for birdwatching and trout fishing, and for its historic features, such as lime kilns and 19th century mills. Other important resources include the federally listed endangered bog turtle, the most extensive mature Piedmont forests remaining in the state of Delaware, and the Cockeysville Marble Formation, an exceptional aquifer.</p>
<p>The watershed is an important source of drinking water for residents in both Pennsylvania and Delaware. Part of the Christina River Basin, White Clay Creek is a critical link to the Delaware Estuary, which is nationally and internationally important. Municipalities, counties, states and federal agencies, together with private organizations and landowners, participate in maintaining the high quality of the White Clay Creek watershed through a committee representing all watershed interests. A key principle of the administrative framework is that existing institutions and authorities will play the primary roles in the long-term protection of the watershed. Watershed residents are active stewards of the river area and must be especially vigilant due to environmental impacts from the proximity of the Philadelphia and Wilmington-Newark metropolitan areas. These impacts to the watershed include pollution, fewer migrating birds, and receding forests.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>