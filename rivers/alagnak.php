<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Alagnak River, Alaska';

// Set the page keywords
$page_keywords = 'Katmai National Park, National Park Service, Alagnak River, Kukaklek Lake, Nonvianuk River, Alaska';

// Set the page description
$page_description = 'Alagnak, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('25');

// Includes the meta data that is common to all pages
include( "../includes/metascript.php" );
?>

<script>
var riverID = <?php echo json_encode( $river_id ); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include( '../includes/header.php' );
?>

<?php
// includes the content page top
include( '../includes/content-head.php' );
?>


<?php
// includes the top of the rivers page and zoomify button
include( "../includes/rivers-top.php" );
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Katmai National Park and Preserve<br/>
Post Office Box 7<br/>
King Salmon, Alaska 99613</p>
<br/>
<h3>Designated Reach:</h3>
<p>December 2, 1980. From Kukaklek Lake to the west boundary of T13S, R43W and the entire Nonvianuk River.</p>
<br/>
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 67.0 miles; Total &#8212; 67.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<p>We could use a photo.<br/>Do you have one you'd like to let us use?<br/>If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br/>We would be happy to credit you as the photographer.</p>
<!--<img src="images/alagnak.jpg" alt="Alagnak River" title="Alagnak River" width="265px" height="204px" />-->
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/alag/index.htm" alt="Alagnak Wild River (National Park Service)" target="_blank">Alagnak Wild River (National Park Service)</a></p>
<p><a href="http://www.nps.gov/katm/" alt="Katmai National Park and Preserve" target="_blank">Katmai National Park and Preserve</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Alagnak River</h2>
<p>The Alagnak River, originating in Katmai National Preserve's Kukaklek Lake, has abundant wildlife, including brown bear, moose, beaver, river otter, bald eagle and osprey. Visitors enjoy the fishing along this clear, braided river, as well as the striking changes in landscape, large undeveloped lakes, boreal forest, wet sedge tundra, shrubby islands and Class I-III rapids.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include( '../includes/content-foot.php' );
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include( '../includes/footer.php' );
?>