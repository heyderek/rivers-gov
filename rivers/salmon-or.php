<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Salmon River, Oregon';

// Set the page keywords
$page_keywords = 'Salmon River, Mt. Hood National Forest, Oregon';

// Set the page description
$page_description = 'Salmon River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('99');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Salem District<br />
U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters to its confluence with the Sandy River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 15.0 miles; Scenic &#8212; 4.8 miles; Recreational &#8212; 13.7 miles; Total &#8212; 33.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/salmon-or.jpg" alt="Salmon River, Oregon" title="Salmon River, Oregon" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<!--<p><a href="http://www.blm.gov/or/districts/salem/plans/sandy_river_corridor.php" alt="Salmon &amp; Sandy Rivers (Bureau of Land Management)" target="_blank">Salmon &amp; Sandy Rivers (Bureau of Land Management)</a></p>-->
<p><a href="../documents/plans/salmon-oregon-plan.pdf" alt="Salmon River Management Plan" target="_blank">Salmon River Management Plan (PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Salmon River (Oregon)</h2>

<p>Only an hour's drive from Portland, Oregon, the clear water of the Salmon River cascades over numerous waterfalls in the Salmon-Huckleberry Wilderness before reaching its lower forested canyons. From its headwaters to the confluence with the Sandy River approximately 33.5 miles (53.9 km) downstream, the river's proximity makes it easy for a great number of people to enjoy its diverse recreational opportunities. The Salmon River incorporates portions of two major physiographic zones&#8212;the Cascade Mountain Range and the Columbia Basin. As a result, the river corridor contains great natural diversity, from alpine environments and narrow basalt canyons to wide floodplains with their associated wetlands.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic &amp; Ecologic</em></strong></p>

<p>The Red Top/Salmon River Meadows Complex is an area of great ecological diversity and productivity. This complex includes a wide variety of rare and unique plant communities. One example is the population of a sensitive species, a native podgrass (<em>Scheuchzerias palustris</em>) that is rare in Oregon and whose population along the Salmon is the largest in the state.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The lower Salmon River provides very important and productive anadromous fish spawning and rearing habitat. In addition to summer steelhead, the river also contains winter steelhead, coho salmon, spring Chinook salmon, native cutthroat trout and native and hatchery rainbow trout.</p>

<p><strong><em>Hydrologic</em></strong></p>

<p>The presence of six waterfalls in a three-mile segment is rare in the state. These waterfalls range in height from 15 to 75 feet.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The river and its corridor offer a wide variety of recreational opportunities, including hiking, Nordic and alpine skiing, camping and developed recreation sites. Sport fishing is also exceptional on this river, and its summer steelhead fishery draws anglers from within an outside the state of Oregon. The unique Cascade Streamwatch Interpretive Area at the Bureau of Land Management's Wildwood Recreation Site is located on the lower section.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The upper river corridor includes impressive close-up views of Mt. Hood from the upper river area near Timberline Lodge, as well as the scenic diversity of Red Top Meadows and Salmon River Meadows areas. Farther downstream, the river flows through a narrow river canyon with basalt cliffs and through a series of six waterfalls in a short three-mile section of the river.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The entire river provides important habitat, optimal both in summer and winter, for big game species from both the east and west sides of the Cascades, and the upper meadow complexes support  a wide diversity of wildlife habitat. Two important species found within the area include Roosevelt elk and greater Sandhill cranes. The small population of greater Sandhill cranes is the northernmost breeding population of this species in Oregon.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>