<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Rio Grande, Texas';

// Set the page keywords
$page_keywords = 'Big Bend National Park, National Park Service, Rio Grande, Texas';

// Set the page description
$page_description = 'Rio Grande, Texas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('17');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Big Bend National Park</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978. The segment on the United States side of the river from river mile 842.3 above Mariscal Canyon downstream to river mile 651.1 at the Terrell-Val Verde County line.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 95.2 miles; Scenic &#8212; 96.0 miles; Total &#8212; 191.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rio-grande-texas.jpg" alt="Rio Grande, Texas" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/rigr/" alt="Rio Grande Wild and Scenic River (National Park Service)" target="_blank">Rio Grande Wild and Scenic River (National Park Service)</a></p>
<p><a href="http://www.nps.gov/bibe/" alt="Big Bend National Park (National Park Service)" target="_blank">Big Bend National Park (National Park Service)</a></p>
<p><a href="../documents/plans/rio-grande-texas-plan.pdf" title="Rio Grande River Management Plan &amp; EIS" target="_blank">Rio Grande River Management Plan &amp; EIS</a></p>

<div id="photo-credit">
<p>Photo Credit: Greg Anderson</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Rio Grande (Texas)</h2>
<p>This 191-mile stretch of the United States side of the Rio Grande along the Mexican border begins in Big Bend National Park. The river cuts through isolated, rugged canyons and the Chihuahuan Desert as it flows through some of the most critical wildlife habitat in the country.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>