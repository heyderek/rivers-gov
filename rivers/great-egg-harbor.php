<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Great Egg Harbor River, New Jersey';

// Set the page keywords
$page_keywords = 'Great Egg Harbor River, New Jersey';

// Set the page description
$page_description = 'Great Egg Harbor River, New Jersey';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('145');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Philadelphia Office</p>
<br />
<h3>Designated Reach:</h3>
<p>October 27, 1992. From the mouth of Patcong Creek to the Mill Street Bridge. From Lake Lenape to the Atlantic City Expressway. From the Williamstown-New Freedom Road to the Pennsylvania Railroad right-of-way.</p>
<p>The following tributaries from their confluence with the Great Egg Harbor River: Squankum Branch to Malaga Road; Big Bridge Branch to its headwaters; Penny Pot Stream Branch to 14th Street; Deep Run to Pancoast Mill Road; Mare Run to Weymouth Avenue; Babcock Creek to its headwaters; Gravelly Run to the Pennsylvania Railroad right-of-way; Miry Run to Asbury Road; South River to Main Avenue; Stephen Creek to New Jersey Route 50; Gibson Creek to First Avenue; English Creek to Zion Road; Lakes Creek to the dam; Middle River to the levee; Patcong Creek to the Garden State Parkway; Tuckahoe River to the Route 49 Bridge; Cedar Swamp Creek from its confluence with the Tuckahoe River to its headwaters.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 30.6 miles; Recreational &#8212; 98.4 miles; Total &#8212; 129.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/great-egg-harbor.jpg" alt="Great Egg Harbor River" title="Great Egg Harbor River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/greateggharbor_pwsr_sub.html" alt="Great Egg Harbor River (National Park Service)" target="_blank">Great Egg Harbor River (National Park Service)</a></p>
<p><a href="http://www.nps.gov/greg/" alt="Great Egg Harbor River (National Park Service)" target="_blank">Great Egg Harbor River (National Park Service)</a></p>
<p><a href="http://www.gehwa.org" alt="Great Egg Harbor River Watershed Association" target="_blank">Great Egg Harbor River Watershed Association</a></p>

<div id="photo-credit">
<p>Photo Credit: National Park Service</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Great Egg Harbor River</h2>
<p>In 1992, 129 miles of the Great Egg Harbor River and its tributaries were designated into the National Wild and Scenic Rivers System. The Great Egg, as it is known locally, drains 304 square miles of pristine wetlands in the heart of New Jersey's Pinelands Reserve (the famous "Pine Barrens") on its way to the Atlantic Ocean. The Great Egg flows through 12 New Jersey municipalities and is located near the urban centers of Philadelphia, Pennsylvania; Trenton and Camden, New Jersey; and Wilmington, Delaware. The river's proximity to millions of people, togther with it being the largest canoeing river in the Pine Barrens, makes the Great Egg an important recreation destination.</p>
<p>Dissolved iron and tannin, a product of fallen leaves and cedar roots, produce the river's tea-colored "cedar water" along much of its length. Freshwater and tidal wetlands, amid undisturbed forests, serve as resting, feeding and breeding areas for waterfowl throughout the year. Each year striped bass and alewife herring return from the ocean to spawn in the gravel-bottomed tributaries of the Great Egg Harbor River. The river also provides habitat for several threatened and endangered species including the bald eagle, the peregrine falcon, and the Pine Barrens tree frog.</p>
<p>The watershed has been occupied since pre-historic times, lived upon traditionally by the Lenape Indians before European occupation in the early 1700s. The lands contained all the necessary materials for shipbuilding, and in the Revolutionary War its "bog iron" made cannon balls, while its hidden coves sheltered privateers. Blast furnaces, sawmills, glass factories, and brick and tile works followed until the Industrial Revolution drew its people away. Today, the development of the area's prime agricultural land has contributed greatly to the cultural diversity of the area.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>