<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Allagash River, Maine';

// Set the page keywords
$page_keywords = 'Allagash River, Allagash Wilderness Waterway, Chamberlain Lake, Telos Lake Dam, Maine';

// Set the page description
$page_description = 'Allagash River, Maine';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('SD1');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<!--ESRI map-->
<?php include_once( "../iframe.php" ); ?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Maine Bureau of Parks and Recreation, Department of Conservation</p>
<br />
<h3>Designated Reach:</h3>
<p>July 19, 1970. The main stem from Telos Lake Dam northerly to the confluence with West Twin Brook; the main stem from the juncture with the west boundary of T.S, R. 14 easterly to the inlet of the Allagash at Chamberlain Lake. The designation includes all associated lakes, rivers and streams.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 92.5 miles; Total &#8212; 92.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/allagash.jpg" alt="Allagash River" title="Allagash River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.maine.gov/cgi-bin/online/doc/parksearch/search_name.pl?state_park=2&historic_site=&public_reserved_land=&shared_use_trails=&option=search" alt="Allagash Wilderness Waterway State Park" target="_blank">Allagash Wilderness Waterway State Park</a></p>

<div id="photo-credit">
<p>Photo Credit: Tom Gainer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Allagash River</h2>
<p>From the top of Mt. Katahdin, the Allagash Wilderness Waterway stretches northward as far as the eye can see. The river winds through swampy conifer forests and between ridges of hardwoods. This resource includes Allagash Lake, the Allagash River and several interconnecting lakes.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>