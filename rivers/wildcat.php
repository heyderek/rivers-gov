<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wildcat Brook, New Hampshire';

// Set the page keywords
$page_keywords = 'Wildcat River, Wildcat Brook, Saco River, Jackson Falls, Ellis River, White Mountain National Forest, New Hampshire';

// Set the page description
$page_description = 'Wildcat Brook, New Hampshire';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('67');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Town of Jackson, New Hampshire<br />
U.S. Forest Service, White Mountain National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. Wildcat River from the headwaters in Carter Notch to the confluence with the Ellis River (9.05 miles). Little Wildcat Brook from the headwaters on Wildcat Mountain to the confluence with Wildcat Brook (2.83 miles). Bog Brook from the headwaters near Perkins Notch to the confluence with Wildcat River (1.58 miles). Great Brook from the Route 16B bridge at Whitney's Pond to the confluence with Wildcat Brook (1.05 miles).</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 13.7 miles; Recreational &#8212; 0.8 mile; Total &#8212; 14.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wildcat.jpg" alt="Wildcat River" title="Wildcat River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/wildcat-plan.pdf" alt="Wildcat River Management Plan (3.3 MB PDF)" target="_blank">Wildcat River Management Plan (3.3 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wildcat River</h2>
<p>A small mountain stream that rises from the scenic White Mountain National Forest, the brook tumbles over a series of ledges into the center of the classically picturesque New England town of Jackson. The area has many hiking and cross country skiing opportunities. The outstanding scenic beauty, high-quality water, and recreational value of the river area and Jackson Falls provided a centerpiece for the historic resort town, as Jackson evolved from its agricultural origins a century ago to the rural, tourism-based resort community that it is today.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>