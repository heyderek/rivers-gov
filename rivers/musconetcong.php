<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Musconetcong River, New Jersey';

// Set the page keywords
$page_keywords = 'Musconetcong River, New Jersey';

// Set the page description
$page_description = 'Musconetcong River, New Jersey';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('169');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Philadelphia Office</p>
<br />
<h3>Designated Reach:</h3>
<p>December 22, 2006. The 3.5-mile segment from Saxton Falls to the Route 46 Bridge and the 20.7-mile from the King's Highway Bridge to the railroad tunnels at Musconetcong Gorge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 3.5 miles; Recreational &#8212; 20.7 miles; Total &#8212; 24.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/musconetcong.jpg" alt="Musconetcong River" title="Musconetcong River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/musconetcong_pwsr_sub.html" alt="Musconetcong River (National Park Service)" target="_blank">Musconetcong River (National Park Service)</a></p>
<p><a href="http://www.musconetcong.org" alt="Musconetcong Watershed Association" target="_blank">Musconetcong Watershed Association</a></p>
<p><a href="https://www.heritageconservancy.org/images/news/publications/pdf/musconetcong-study.pdf" alt="Musconetcong Wild &amp; Scenic River Study" target="_blank">Musconetcong Wild &amp; Scenic River Study</a></p>
<p><a href="../documents/plans/musconetcong-plan.pdf" alt="Musconetcong River Management Plan (1.2 MB PDF)" target="_blank">Musconetcong River Management Plan (1.2 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Musconetcong River</h2>

<p>The Musconetcong River drains a 157.6-square-mile watershed area in northern New Jersey, and, as a major tributary to the Delaware River, is part of the larger (12,755-square-miles) Delaware River watershed. The 42.5-mile river in a general southwest direction to the Delaware River and ultimately to the Delaware Bay.</p>

<p>The Musconetcong River, nestled in the heart of the distinctive New Jersey Highlands region, features a remarkably diverse array of natural and cultural resources. The limestone geologic features present in the river corridor are unique in the region, and the steep slopes and forested ridges in the upper segments of the river corridor stand in stark contrast with the historic hamlets, pastures and rolling agricultural lands at the middle and lower end of the river valley.</p>

<p>Because the Musconetcong River corridor is located in a more remote part of New Jersey, much of the corridor's historical and archeological resources remain intact. Dozens of culturally significant historic and archeological resources&#8212;many of which are registered in the State and National Registers of Historic places&#8212;can be found along the 24.2 miles of the river's federal designation. Waterloo Village, Stanhope, Asbury and Finesville are places that bring visitors back to earlier times and underscore the importance of the Musconetcong River as the sustaining resource that established them.</p>

<p>The Musconetcong River also offers exemplary natural resources, often referred to as the best trout fishery in New Jersey. Brown trout can be found in the designated river's seven main tributaries, and anglers in the region have access to the river from hundreds of acres of publicly owned lands along the river's banks. Paddlers enjoy the river's rapid flows, and hikers trek the miles of hilly trails that flank the river, affording stunning views of the river corridor.</p>

<p>The Musconetcong is a "Partnership River" whereby a number of municipalities coordinate on its management with the National Park Service.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>