<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Big Piney Creek, Arkansas';

// Set the page keywords
$page_keywords = 'Ozark National Forest, St. Francis National Forest, Big Piney Creek, Arkansas';

// Set the page description
$page_description = 'Big Piney Creek, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('134');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ozark-St. Francis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992. From its origin to the Ozark National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 45.2 miles; Total &#8212; 45.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/big-piney-creek.jpg" alt="Big Piney Creek" title="Big Piney Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/big-piney-creek-plan.pdf" target="_blank" alt="Big Piney Creek Management Plan (1.9 MB PDF)" target="_blank">Big Piney Creek Management Plan (1.9 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Robert Duggan</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Big Piney Creek</h2>
<p>Big Piney Creek is located on the Ozark National Forest in northwest Arkansas. The river begins near the community of Fallsville and flows east and south for 57 miles to its junction with the Arkansas River. The river's outstandingly remarkable values include scenery, recreation, fish, botany and geology.</p>
<p>The distinctive scenery of Big Piney Creek is characterized by its sandstone bluffs, waterfalls, still pools and stands of oak, hickory and pine. Principal recreation activities in the corridor include canoeing, camping, swimming and fishing. The river is very popular for canoeing, with Class I to III rapids. It is considered by the Arkansas Game and Fish Commission to be an outstanding sportfishing river, with smallmouth bass and spotted bass the most sought-after species.</p>
<p>Big Piney supports plant species considered by the Arkansas Heritage Commission to be sensitive. One species, Alabama snow-wreath, is currently under study for listing as a federal threatened and endangered species and is already listed as threatened by the state of Arkansas. The corridor also includes the Waldo Mountain-Wainscott Bottoms Special Interest Area, which contains diverse plant communities and plant species.</p>
<p>The Mississippian and lowermost Pennsylvanian rock exposed near the town of Limestone are very important to understanding the history of the southern margin of the Ozark platform. The youngest Mississippian-age rocks in North America, as well as the southernmost exposure of Morrowan-type rocks, are located in this area. These exposures create a structural window important in developing an understanding of the upper Mississippian and the basal Morrowan sequence. Many of these features are exposed along Big Piney and are of unique scenic and geologic value.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>