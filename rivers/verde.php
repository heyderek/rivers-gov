<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Verde River, Arizona';

// Set the page keywords
$page_keywords = 'Verde River, Prescott National Forest, Arizona';

// Set the page description
$page_description = 'Verde River, Arizona';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('51');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Coconino National Forest<br />
U.S. Forest Service, Prescott National Forest<br />
U.S. Forest Service, Tonto National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>August 28, 1984. Scenic River Area: The northern boundary of the Scenic River Area is the section line between Section 26 and 27, T13N, R5E, Gila-Salt River meridian. The southern boundary is the boundary of the Mazatzal Wilderness which is within Section 11, T11N, R6E. Wild River Area: The northern boundary of the Wild River Area is the boundary of the Mazatzal Wilderness which is within Section 11, T11N, R6E. The southern boundary is at the confluence of Red Creek with the Verde River within Section 34, T9 l/2N, R6E.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 35.7 kilometers/22.2 miles; Scenic &#8212; 29.5 kilometers/18.3 miles; Total &#8212; 65.2 kilometers/40.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/verde.jpg" alt="Verde River" title="Verde River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/coconino/recarea/?recid=70863" alt="Coconino National Forest" target="_blank">Coconino National Forest</a></p>
<p><a href="http://www.redrockcountry.org/recreation/verde-river-guide.pdf" alt="Verde River Boaters Guide" target="_blank">Verde River Boaters Guide</a></p>
<p><a href="../documents/plans/verde-plan.pdf" alt="Verde River Management Plan (1.3 MB PDF)" target="_blank">Verde River Management Plan (1.3 MB PDF)</a></p>
<p><a href="../documents/plans/verde-ea.pdf" alt="Verde River Management Plan Environmental Assessment (2.3 MB PDF)" target="_blank">Verde River Management Plan Environmental Assessment (2.3 MB PDF)</a></p>
<p><a href="../documents/plans/verde-fonsi.pdf" alt="Verde River Management Plan FONSI (295 KB PDF)" target="_blank">Verde River Management Plan FONSI (295 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Verde River</h2>
<p>The Verde River is a unique and important resource in Arizona. In a southwestern state where perennial flowing water is rare, the riparian oasis of the Verde River stands in stark contrast to the arid uplands through which it meanders. Indeed, Verde is the Spanish term for the color "green." Major river access points are at White Bridge where State Highway 260 crossed the river east of Camp Verde, at Beasley Flats via Forest Roads 574 and 529, and near Childs Power Plant via Forest Roads 708 and 502. The Verde River provides diverse recreational opportunities including boating, hunting, fishing, birding, hiking, picnicking and photography.</p>
<p style="text-align:center"><strong>OUTSTANDINGLY REMARKABLE VALUES</strong></p>
<p>The outstandingly remarkable values of the Verde River are its scenic, fish and wildlife, and historic and cultural values.</p>
<p>The river's scenic qualities of landform, vegetation and water are distinctive. Landform varies from steep, rocky canyons from the river to plateaus dropping to wide floodplains, with the river a central feature. Vegetation varies according to terrain, from broad mesquite bosques and cottonwood gallery forests to narrow bands of riparian willows, in contrast to the surrounding dry grassland and desert vegetation. There is dramatic seasonal variation and water flow changes from shallow, still pools and slow water to high flow, seasonal rapids and waterfalls.</p>
<p>The river's high-quality habitat supports the more than 50 threatened, endangered, sensitive, or special status fish and wildlife species. The fish community with the Verde River is represented by six native species and numerous introduced non-native fish species. Native fish species include razorback sucker, Colorado pikeminnow, roundtail chub, Sonora sucker, desert sucker and longfin dace. The razorback sucker and Colorado pikeminnow are listed as endangered species; the river also provides critical habitat for razorback sucker, spikedace and loach minor.</p>
<p>The Verde provides habitat for a diverse array of wildlife species and contains some the most important riparian and associated upland habitat found in Arizona and the Southwest. This habitat supports over 60 percent of the vertebrate species that inhabit the Coconino, Prescott and Tonto National Forests. There are bald eagle nest territories, migratory and possibly occupied habitat for southwestern and potential habitat for several listed species.</p>
<p>The river corridor contains archaeological evidence of occupation and agricultural use and modification of the Verde River floodplain, terraces and hill slopes by people related to the Hohokam and Southern Sinagua cultural traditions over a period of at least 600 years. It may contain sites of human use and occupation from as long as 8,000 to 10,000 years. It has sites representing the Anglo, Hispanic and Basque stockmen who raised or drove cattle and sheep through the area. The earliest hydroelectric power plant in the state of Arizona (now decommissioned) is located in the Verde corridor as is the remains of one of Arizona's first tourist developments, the Verde Hot Springs Resort.</p>
<p style="text-align:center"><strong>REGULATIONS</strong></p>
<p>Virtually the entire Wild River Area is within the Mazatzal Wilderness. There are several restrictions: (1) Length of stay is no longer than 14 days; (2) Group size is limited to 15 persons, and (3) No more than 15 head of livestock are allowed per group.</p>
<p>No permit is required for private parties to run this river; however, with added freedom comes added responsibility. While the river may appear calm at river access points, the large numbers of wrecked canoes that have been removed from the river testify to the fact that it has its share of hazards. Please plan ahead, be prepared and practice Leave-No-Trace ethics to leave the Verde just as you found it for those who come after.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>