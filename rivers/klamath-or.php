<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Klamath River, Oregon';

// Set the page keywords
$page_keywords = 'Klamath Wild and Scenic River, Oregon';

// Set the page description
$page_description = 'Klamath River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('SD17');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Lakeview District<br />
Oregon Department of Parks &amp; Recreation</p>
<br />
<h3>Designated Reach:</h3>
<p>September 22, 1994. From the J.C. Boyle Powerhouse to the California-Oregon border. The Klamath River is in Klamath County 25 miles to the southwest of Klamath Falls in south-central Oregon.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 11.0 miles; Total &#8212; 11.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/klamath-or.jpg" alt="Klamath River, Oregon" title="Klamath River, Oregon" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<!--<p><a href="http://www.blm.gov/or/resources/recreation/site_info.php?siteid=87" alt="Klamath River (Bureau of Land Management)" target="_blank">Klamath River (Bureau of Land Management)</a></p>-->
<p><a href="../documents/plans/upper-klamath-draft-plan-eis.pdf" title="Klamath River Draft Management Plan &amp; EIS" target="_blank">Klamath River Draft Management Plan &amp; EIS</a></p>
<p><a href="../documents/studies/klamath-study.pdf" title="Klamath River 2(a)(ii) Study Report" target="_blank">Klamath River 2(a)(ii) Study Report</a></p>
<p><a href="http://www.pacificorp.com/about/or/oregon.html" alt="Klamath River (PacifiCorp)" target="_blank">Klamath River (PacifiCorp)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Klamath River (Oregon)</h2>

<p>The Klamath River is one of only three rivers that bisect the Cascade Mountain Range. Beginning in Oregon's high desert interior, it cuts through the Cascades and the Klamath Mountains before entering the Pacific Ocean in northern California. This creates a wide diversity of habitats supporting an abundance of fish and wildlife. Due to an abundance of food and a mild climate, the Klamath River Basin was and is an important location for at least three Native American tribes. The river was also an attractive location for early European settlement, providing a travel corridor through the mountains.</p>

<p>The Klamath is recognized as an outstanding whitewater boating river with many class III- IV+ rapids, warm water and a long boating season.  Steep, boulder strewn rapids in the Hell's Corner gorge testify to the Native American name for the river, "Klamet," meaning "swiftness."  The abundance of resources, such as a unique rainbow trout subspecies, spectacular scenery, Native American cultural sites and gold rush era historical sites, combine to provide the setting for a spectacular and profound river experience. Other popular activities include camping, fishing and scenic four-wheel drive touring.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Klamath River supports a genetically unique population of rainbow trout able to survive the naturally high temperatures and acidity of the river. The Klamath one of only three rivers in the region and six in the state managed as a wild rainbow trout fishery, significant in a state known for its fishing. In addition, access to prime fishing spots is plentiful. Endangered species, such as the Lost River and shortnose suckers, are dependent on the Klamath River, as is the sensitive but non-listed Klamath largescale sucker.</p>

<p><strong><em>Historic</em></strong></p>

<p>Historic ranches along the river offer a glimpse into the challenges facing early settlers. One of few access points to the river is actually part of an historic stagecoach route serving the Klamath Basin and northern California. The livery station serving the stagecoach is still there. Just below the California border, an abandoned log flume speaks to one of the early uses of the river by Europeans.</p>

<p><strong><em>Native American Traditional Use</em></strong></p>

<p>There is considerable evidence that the Klamath River Canyon has been in use by Native Americans for at least 7,000 years, both as a place to make a living and as a location for vision quests, curing ceremonies and spiritual preparation. The Shasta Nation and the Klamath Tribes consider the canyon to be sacred and of immeasurable spiritual significance. Burial sites add to the spiritual significance for both groups and allow their spiritual leaders to prepare for religious and medicinal ceremonies.</p>

<p><strong><em>Pre-Historic</em></strong></p>

<p>There are at least forty prehistoric sites in the canyon, including camps and burial grounds, offering important opportunities to learn about the past. Three, possibly four, Native American tribes' use of the river has earned it the distinction of being eligible for the National Register of Historic Places as an Archaeological District.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Class II-IV+ rapids and warm water provide a long whitewater boating season. The first five miles are relatively easy with class II and III rapids for warm up. In Hell's Corner Gorge that follows, the pool-drop rapids are frequent and long, and boater expertise is a must for paddling at higher flows.</p>

<p>Opportunities to fish, hunt and simply view the abundant wildlife are available year-round. There are many camping sites and easy hikes to view the unique resources of the river.  Scenic four-wheel drive touring is also popular.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Unique combinations of landform, water and vegetation create a continually changing landscape as it changes from desert to mountainous. Steep canyon walls covered by a variety of pine, sage and juniper and decorated with basalt outcrops create an exceptionally peaceful experience.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The high-quality habitat in the canyon is exceptionally diverse. The riverine habitat is important to a variety of birds and mammals; it provides a natural migration corridor for a variety of raptors; the extensive rimrock is suited to raptor nesting; large conifers provide nesting and roosting locations for bald eagles and osprey; caves provide nursing and roosting spots for several bat species; and the extensive oak forest and grasslands are important to large numbers of wintering non-game birds.</p>

<p>The diversity of habitats provides for exceptional populations of birds of prey, game and other birds, ringtail cats, river otters and many other animals. Numerous threatened or endangered species are dependent on the Klamath River for their continued survival. Included in this list are bald and golden eagles, prairie and peregrine falcons, and Townsend's big-eared bats.  Other notable residents include ospreys, Lewis's and acorn woodpeckers and western pond turtles.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>