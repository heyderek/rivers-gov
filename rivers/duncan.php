<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Duncan Creek, Idaho';

// Set the page keywords
$page_keywords = 'Duncan Creek, Idaho';

// Set the page description
$page_description = 'Duncan Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('187');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Duncan Creek from its confluence with Big Jacks Creek upstream to the east boundary of Section 18, Township 10 South, Range 4 East, Boise Meridian.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 0.9 miles; Total &#8212; 0.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/duncan-creek.jpg" alt="Duncan Creek" title="Duncan Creek" width="262px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14976/2" alt="Duncan Creek (Bureau of Land Management)" target="_blank">Duncan Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Judi Zuckert, BLM</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Duncan Creek</h2>

<p>For nearly one mile before it flows into Big Jacks Creek, Duncan Creek is designated as wild. It is characterized by it dense riparian vegetation and tight meanders. Redband trout are found in the creek, and mule deer are common in the area. Access to the area is limited to hiking trails.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Duncan Creek, when considered in combination with Big Jacks, Little Jacks and Cottonwood Creeks, are among the 17% of desert streams in the Northern Basin and Range identified as aquatic-habitat strongholds for redband trout, a BLM sensitive species and a state of Idaho species of special concern.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Duncan Creek, and the wilderness through which it flows, are notable for their solitude and limitless character. The primary hiking/horseback riding trail along Duncan Creek (in Zeno Canyon) requires keen wayfinding and preparation skills for traveling over and through rough, rocky terrain.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The landscape is a mix of streams, rugged canyons and plateaus. Coarse-textured, red, brown, or blackish eroded cliffs seven-hundred-feet high dominate canyons.  The canyon walls are often glazed with yellow to light green micro-flora. Intertwined with the vertical features are steep diagonal lines that frame talus slopes, mosaics of medium-textured, yellow and subdued green sagebrush-bunchgrass, dark green juniper, and rubble fields of medium-textured, reddish rhyolite and/or coarse-textured, blackish basalt.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The wilderness in Duncan Creek, part of Big Jacks Creek Basin, consists of rugged canyons, streams and plateaus that provide habitat for mountain quail, sage hens, mountain lions, mule deer, pronghorns, bobcats, coyotes and bighorn sheep.</p>

<p>This drainage is included in Southwestern Idaho's Preliminary Priority Habitat area for greater sage-grouse. Idaho BLM sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats, Idaho BLM sensitive species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>