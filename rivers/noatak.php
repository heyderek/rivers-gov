<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Noatak River, Alaska';

// Set the page keywords
$page_keywords = 'Noatak River, Gates of the Arctic National Park, Noatak National Preserve, Kobuk Valley National Park, Kelly River, Alaska';

// Set the page description
$page_description = 'Noatak River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('33');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Gates of the Arctic National Park<br />
National Park Service, Noatak National Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The river from its source in Gates of the Arctic National Park to the Kelly River in the Noatak National Preserve.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 330.0 miles; Total &#8212; 330.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/noatak.jpg" alt="Noatak River"height="204px" title="Noatak River" width="265px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/gaar/noatakriver.htm" alt="Noatak River (National Park Service)" target="_blank">Noatak River (National Park Service)</a></p>
<p><a href="http://www.nps.gov/gaar/" alt="Gates of the Arctic National Park and Preserve (National Park Service)" target="_blank">Gates of the Arctic National Park and Preserve (National Park Service)</a></p>
<p><a href="http://www.nps.gov/noat/" alt="Noatak National Preserve (National Park Service)" target="_blank">Noatak National Preserve (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Thomas Martin</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Noatak River</h2>
<p>Situated in Gates of the Arctic National Park and Preserve and Noatak National Preserve, the Noatak drains the largest mountain-ringed river basin in America that is still virtually unaffected by human activities.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>