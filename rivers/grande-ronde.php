<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Grande Ronde River, Oregon';

// Set the page keywords
$page_keywords = 'Wallowa-Whitman National Forest, Vale District, Bureau of Land Management, Grande Ronde River, Oregon';

// Set the page description
$page_description = 'Grande Ronde River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('77');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>
<title>2018 Grande Ronde &amp;amp; Wallowa Rivers Boating Information</title>


<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Baker Field Office<br />
U.S. Forest Service, Umatilla National Forest<br />
U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its confluence with the Wallowa River to the Oregon-Washington border.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 26.4 miles; Recreational &#8212; 17.4 miles; Total &#8212; 43.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/grande-ronde.jpg" alt="Grande Ronde River" title="Grande Ronde River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/grande-ronde-wallowa-boating-info-2018.pdf" alt="2018 Grande Ronde &amp; Wallowa Rivers Boating Information" title="2018 Grande Ronde &amp; Wallowa Rivers Boating Information" target="_blank">2018 Grande Ronde &amp; Wallowa Rivers Boating Information</a> &#8212; Bureau of Land Management Information</p>
<p><a href="https://www.fs.usda.gov/detail/umatilla/recreation/wateractivities/?cid=fsbdev3_062366" alt="Grande Ronde River (U.S. Forest Service)" target="_blank">Grande Ronde River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/wallowa-grande-ronde-plan-ea.pdf" title="Wallowa &amp; Grande Ronde Rivers Management Plan" target="_blank">Wallowa &amp; Grande Ronde Rivers Management Plan (Bureau of Land Management &amp; U.S. Forest Service)</a></p>
<p><a href="../documents/grande-ronde-wallowa-trip.pdf" target="_blank">Boating the Grande Ronde &amp; Wallowa Rivers (Idaho Statesman)</a></p>

<div id="photo-credit">
<p>Photo Credit: Kevin Hoskins</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Grande Ronde River</h2>

<p>The Grande Ronde River is located in northeast Oregon and flows through lands that are privately owned and others administered by the Bureau of Land Management and U.S. Forest Service. At 43.8 miles (70.5 km) in length, the federally protected section begins at the confluence with the Wallowa River near Rondowa, and ends near the Oregon-Washington border.</p>

<p>The entire river corridor is a complex ecosystem rich in unique natural features, history, spectacular scenery and a variety of plant and animal life. The 'upper river' consists of steep basalt canyons and ascending ridges within dense evergreen forest, portions of which are only accessible by boat. The meandering curves of the 'middle river' parallel a seldom-traveled county road as the canyon begins to widen and forests yield to open ridges and steep range lands. The 'lower river' section in Washington is characterized by sparsely-vegetated, rugged terrain and contains the history of ancient peoples and pioneer homesteads amongst a few active ranches.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Grande Ronde River is a nationally renowned sport fishery, one of the top three in the region. The mainstem and its major tributaries provide spawning and rearing habitat for wild and hatchery stock of spring Chinook, fall Chinook, summer steelhead and rainbow trout. Fishing is excellent even late in the season after the water levels have receded.</p>

<p><strong><em>Recreational</em></strong></p>

<p>There are many recreational opportunities on the Grande Ronde. Those judged to be exceptional in quality include anadromous and resident fishing, floating (rafting, canoeing and kayaking for overnight use), and big game viewing and hunting. Visitors are able to enjoy an unusually long float season for a free-flowing river, from ice breakup in the spring to freeze up in the fall. Trips offer a rare multiple day float for those with beginner and intermediate skills.</p>

<p>The primary launch site for the Wallowa and Grande Ronde corridors, as well as the Bureau of Land Management river station, are located on state lands at Minam on the Wallowa River. Additional access points include Mud Creek, Troy and Boggan's Oasis. Primitive campsites along the river are on a first-come, first-served basis. Many portions of the river are roadless and primitive with limited access by vehicles.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Grande Ronde River corridor contains a diversity of landforms and vegetation that progress from largely forested vistas to forested stringers&#8212;patches of residual pre- fire forest, separated by native bunchgrass slopes. River users see a largely untouched viewshed in the upper river reach, while the lower portion flows through open, grass covered hills with forested pockets and tributary canyons.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The area hosts an exceptional diversity of species, in part because the river corridor provides critical wintering habitat for bighorn sheep, elk, mule deer and whitetail deer. Others contributing to the impressive viewing opportunities include black bears, cougars and mountain goats. The river corridor also serves as a sensitive wintering area for bald eagles.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>