<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sandy River, Oregon';

// Set the page keywords
$page_keywords = 'Sandy River, Mt. Hood National Forest, Oregon';

// Set the page description
$page_description = 'Sandy River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('100');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Salem District<br />
U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the headwaters to the Mt. Hood National Forest boundary. From the east boundary of Section 25 and 36, T1S, R4E downstream to the west line of the east 1/2 of northeast 1/4 Section 6, T1S, R4E.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.5 miles; Scenic &#8212; 3.8 miles; Recreational &#8212; 16.6 miles; Total &#8212; 24.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/sandy.jpg" alt="Sandy River, Oregon" title="Sandy River, Oregon" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/sandy-blm-plan.pdf" alt="Salmon &amp; Sandy River Management Plan" target="_blank">Sandy River Management Plan (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/sandy-usfs-plan.pdf" alt="Sandy River Management Plan (3.2 MB PDF)" target="_blank">Sandy River Management Plan (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Thomas O'Keefe</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sandy River</h2>

<p>The Sandy River originates in the high glaciers of Mt. Hood, the most prominent peak in Oregon's Cascade Mountains. Riverside trails offer spectacular scenery, easily observed geologic features, unique plant communities and a variety of recreational opportunities. Just outside Portland, the lower reaches of the Sandy River flows through a deep, winding, forested gorge known for its anadromous fish runs, botanical diversity, recreational boating and beautiful parks.</p>

<p>The U.S. Forest Service administers the initial 12.4-mile segment, and the Bureau of Land Management manages the lower 12.5-mile section between Dodge Park and Dabney Park.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic &amp; Ecologic</em></strong></p>

<p>Western Oregon's climate features mild, wet winters and narrow daily fluctuations in temperature.  In addition, the river incorporates portions of two major physiographic zones, the Willamette Valley and Western Cascades regions. This setting supports an unusual diversity of vegetation (plant species and communities), including the last remaining stand of low-elevation old-growth forest representative of pre-European transitional vegetation in the Willamette Valley region.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>Fisheries within the lower Sandy River are noted for their population diversity, high quality of spawning and rearing habitat, and excellence for sport. The Sandy River contains populations of at least eight runs of anadromous fish species (includes wild and hatchery stock), as well as up to ten resident species. A major contributor is the well-protected riparian vegetation in the sub-basin; generally good stream shading in the upper and middle portions of the drainage keeps water temperatures relatively cool.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The shape and character of the upper Sandy River Valley are products of the glaciation and volcanic activity of Mt. Hood. As glaciers widened and deepened the Sandy River Valley, they exposed volcanic rocks of many different ages in the valley walls. Geologic history includes incised oxbows from the Pliocene river channel, and the valley has been partially filled in by mudflows and pyroclastic flows from two recent significant eruptive periods at Mt. Hood.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The upper Sandy River provides hiking, fishing, backpacking and camping. The Ramona Falls Trail is the most popular trail in the river corridor, providing a relatively easy hike to see this well-known and very scenic waterfall. The upper Sandy River provides a unique kayaking opportunity for experienced kayakers. Nordic skiing is popular during winter months. The lower Sandy River provides an outstanding sport fishery and exceptional recreation opportunities for nature study, day-use activities and non-motorized boating or floating. Its proximity to Portland and its near-pristine condition combine to make it a unique recreational resource within the region.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The upper reaches of the Sandy River and its tributaries flow through rolling mountainous terrain, characterized by narrow chutes and boulder-choked channels. The middle portion flows through a wider river valley with a moderate gradient, descends through narrow and incised bedrock in the Sandy River Gorge and opens into a section characterized by gravel bars and shallow riffles, whose sand and sediment provided the basis for its name. The lower Sandy flows through a steep canyon landscape and varied and diverse vegetation.</p>

<p><strong><em>Water Quality</em></strong></p>

<p>Annual precipitation in the Sandy River watershed ranges from around 110 inches in the upper elevations to about 70 inches near the mouth. Mt. Hood sustains a snowpack year-round in its higher elevations, which provides water storage over the winter and supplemental flows in the summer. The Sandy River has one of the highest percentages of glacial melt of all major Oregon rivers. "Glacial flour," formed by the grinding of rock under the tremendous weight of the glaciers, gives the Sandy a pale green opacity or milky-gray color, which is most apparent in middle to late summer during the peak of glacial melt.</p>

<p>In addition, the water quality of the Sandy River exceeds most state water quality standards set for the watershed.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Sandy River Gorge offers one of the greatest levels of diversity in both wildlife species and habitat of any river in the region. Habitats bordering the river and major tributaries provide critically important travel corridors for wildlife movement along the river and to and from the Larch Mountain area to the east, especially for important big game species such as Roosevelt elk. The area is especially valuable because it is relatively isolated and undisturbed, despite being located only thirty miles from one-third of Oregon's citizens.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>