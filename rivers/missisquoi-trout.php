<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Missisquoi &amp; Trout Rivers, Vermont';

// Set the page keywords
$page_keywords = 'Missisquoi River, Trout River, Vermont, National Park Service';

// Set the page description
$page_description = 'Missisquoi &amp; Trout Rivers, Vermont';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('212');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service</p>
<br />
<h3>Designated Reach:</h3>
<p>December 19, 2014. The Missisquoi River from the Lowell/Westfield town line to the Canadian border in North Troy, excluding the property and project boundary of the Troy and North Troy hydroelectric facilities. The Missisquoi River from the Canadian border in Richford to the upstream
project boundary of the Enosburg Falls hydroelectric facility in Sampsonville. The Trout River from the confluence of the Jay and Wade Brooks in Montgomery
to its confluence with the Missisquoi River in East Berkshire.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 46.1 miles; Total &#8212; 46.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/missisquoi.jpg" alt="Missisquoi River" title="Missisquoi River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.vtwsr.org" alt="Upper Missisquoi &amp; Trout Rivers Study Site" target="_blank">Upper Missisquoi &amp; Trout Rivers Study Site</a></p>
<p><a href="../documents/studies/missisquoi-trout-study-ea.pdf" title="Missisquoi &amp; Trout Rivers Study Report" target="_blank">Missisquoi &amp; Trout Rivers Study Report &amp; Environmental Assessment</a></p>
<p><a href="../documents/plans/missisquoi-trout-plan.pdf" alt="Missisquoi &amp; Trout Rivers Management Plan" target="_blank">Missisquoi &amp; Trout Rivers Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Jim MacCartney</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Missisquoi &amp; Trout Rivers</h2>
<p>The Missisquoi and Trout Rivers are renowned for their numerous deep, picturesque bedrock swimming holes. Some, like the Three Holes swimming area on the Trout River in Montgomery, have been featured in publications such as Yankee magazine. Clear waters add to exception scenery. Big Falls is the largest natural,
undammed falls in Vermont; it is also a State Park. This geologic feature consists of three separate channels with a total vertical drop of about 40 feet (25 feet being the largest single drop). There is a 225-foot-long gorge downstream of the falls with 60-foot-high walls. The gorge ends in a large pool about 100 feet across with beaches that make for good swimming and an excellent place for a picnic or fall foliage viewing. Geologic values are also an attraction, with blue schists and serpentinites to be found. The rivers support several rare, threatened and endangered species of plants and animals, such as the spiny softshell turtle.</p>
<p>Being New England, it's almost a given that historic and culturally significant resources can be found throughout the area. The rivers are spanned by the greatest concentration of covered bridges of any area in the country. In Montgomery alone, there are six covered bridges still in use today. A seventh, the Hectorville Bridge from Gibou Road, is currently in off-site storage awaiting repair. All were built by the same men, the Jewett brothers, in the 1800s and are listed on the National Register of Historic Places. These historic bridges, along with one in Troy and another in Enosburgh, are popular destinations for sightseers and add to the unique local character of the region.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>