<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Smith River (North Fork), Oregon';

// Set the page keywords
$page_keywords = 'Smith River, Siskiyou National Forest, Oregon';

// Set the page description
$page_description = 'Smith River (North Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('92');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Rogue River-Siskiyou National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters to the Oregon-California state line.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 8.5 miles; Scenic &#8212; 4.5 miles; Total &#8212; 13.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/smith-or.jpg" alt="Smith River" title="Smith River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/ipnf/recreation/recarea/?recid=6891" alt="St. Joe River (U.S. Forest Service)" target="_blank">St. Joe River (U.S. Forest Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Nate Wilson</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Smith River (North Fork)</h2>
<p>There are 13 miles of the North Fork Smith River in Oregon. Designated as wild and scenic in 1988, the two wild sections extend from the headwaters to Horse Creek and from Baldface Creek to the Oregon/California state line. The portion between Horse Creek and Baldface Creek is classified as scenic.</p>
<p>The North Fork Smith River is a relatively low-gradient river, dropping approximately 1,800 feet from an elevation of 2,900 feet in its headwaters in the Kalmiopsis Wilderness to 1,100 feet at the Oregon/California border. The North Fork flows south from the flank of Chetco Peak in the Kalmiopsis Wilderness to its confluence with the Smith River in California, creating one of the best protected river systems in the National System.</p>
<p>The river’s outstandingly remarkable values are its fisheries, water quality and scenic quality.</p>
<p><b>Fisheries:</b> The North Fork Smith River's fishery, typical of Pacific coastal systems, is dominated by trout and salmon. Winter steelhead and sea-run cutthroat trout are the predominant anadromous species on the North Fork. Some populations of coho, Chinook (both fall and spring run) and summer steelhead are found in the lower reaches of the river, primarily in California. The North Fork provides seven miles of near-pristine steelhead spawning and rearing habitat and is a significant source of the high-quality water on which the anadromous fishery of the Smith River depends.</p>
<p><b>Water Quality:</b>The North Fork Smith River is known for its outstanding water quality and for its ability to clear quickly following storms. Low turbidity and lack of pollutants contribute to the river's excellent habitat and high fisheries value.</p>
<p><b>Scenic Quality:</b> The scenic quality in the river corridor is a result of a combination of the color, geology, water and vegetation features. Factors contributing to site-specific scenic diversity include large rocks, deep pools, exposed outcrops of Peridotite (or Serpentinite), a variety of soil types and colors, a variety of plant life (including old growth Douglas-fir), anadromous fisheries, wildlife and emerald-colored water.</p>
<p><b>Recreation Opportunities:</b> The primary recreational attractions in the watershed are the Kalmiopsis Wilderness, Sourdough Camp and several trails. Sourdough Camp offers excellent dispersed car camping and is located south of the Kalmiopsis Wilderness at the confluence of Bald Face Creek and the North Fork Smith River. Sourdough Camp is undeveloped, but there are existing traditionally used sites which should be used to concentrate camping impacts.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>