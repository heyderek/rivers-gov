<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Horsepasture River, North Carolina';

// Set the page keywords
$page_keywords = 'Nantahala National Forest, Horsepasture River, North Carolina';

// Set the page description
$page_description = 'Horsepasture River, North Carolina';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('56');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Nantahala National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 26, 1986. From Bohaynee Road (N.C. 281) downstream to Lake Jocassee.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 3.6 miles; Recreational &#8212; 0.6 miles; Total &#8212; 4.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/horsepasture.jpg" alt="Horsepasture River" title="Horsepasture River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/horsepasture-plan.pdf" alt="Horsepasture River Management Plan (1.6 MB PDF)" target="_blank">Horsepasture River Management Plan (1.6 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Horsepasture River</h2>
<p>At 4.2 miles, the designated segment of the Horsepasture is one of the shortest rivers in the National System in the continental United States. Cascading out of North Carolina's rugged forests, it is an exceptional example of an escarpment river with five major waterfalls within two miles and numerous cascades, rapids, boulders and rock outcroppings. Rainbow Falls is the largest of the five falls along the river's course, with a vertical drop of approximately 125 feet. The falling water generates a great deal of spray and a rainbow can be seen when the sun is at the proper angle. The view downstream from the top of the falls is panoramic. A hiking trail that can be accessed from national forest land provides excellent viewing opportunities. Please respect private property and only use this access point.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>