<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Indian River, Michigan';

// Set the page keywords
$page_keywords = 'Hiawatha National Forest, Indian River, Michigan';

// Set the page description
$page_description = 'Indian River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('122');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Hiawatha National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From Hovey Lake to Indian Lake.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 12.0 miles; Recreational &#8212; 39.0 miles; Total &#8212; 51.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/indian.jpg" alt="Indian River" title="Indian River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Indian River</h2>
<p>The Indian River located on the Hiawatha National Forest in Michigan's Upper Peninsula provides beautiful and varied northwoods scenery. The river flows through a variety of settings ranging from a series of connected lakes in the upper sections, to a meandering channel in an incised valley through the middle section, to a braided channel and an area of extensive wetland before flowing into Indian Lake.</p>
<p>Trout fishing is good, and there is spawning habitat for lake sturgeon and habitat for eagles and ospreys. The river offers excellent canoeing and kayaking for all skill levels throughout the year due to its steady flow. There are numerous dispersed campsites along the river, as well as Widewater Campground.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>