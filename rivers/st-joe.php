<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'St. Joe River, Idaho';

// Set the page keywords
$page_keywords = 'St. Joe River, Panhandle National Forest, Idaho';

// Set the page description
$page_description = 'St. Joe River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('23');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Idaho Panhandle National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978. The segment above the confluence of the North Fork of the St. Joe River to St. Joe Lake.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 26.6 miles; Recreational &#8212; 39.7 miles; Total &#8212; 66.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/st-joe.jpg" alt="St. Joe River" height="204px" title="St. Joe River" width="265px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/ipnf/recreation/recarea/?recid=6891" alt="St. Joe River (U.S. Forest Service)" target="_blank">St. Joe River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/st-joe-plan.pdf" title="St. Joe River Management Plan" target="_blank">St. Joe River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>St. Joe River</h2>

<p>This northern Idaho river features crystal clear water and offers outstanding scenery, excellent catch and release fishing, and plenty of wildlife. There are numerous small, rustic campgrounds along the shores of the roaded portion of the river (39.7 miles) and a trail along the entire wild portion (26.6 miles)of the river to its headwaters at St. Joe Lake. The river was originally named the "St. Joseph" by Father Pierre-Jean Desmet, a Catholic priest who established a mission nearby.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>