<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Carp River, Michigan';

// Set the page keywords
$page_keywords = 'Hiawatha National Forest, Carp River, Michigan';

// Set the page description
$page_description = 'Carp River, Michigan';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('121');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Hiawatha National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From the west section line of section 30, T43N, R5W to Lake Huron.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 12.4 miles; Scenic &#8212; 9.3 miles; Recreational &#8212; 6.1 miles; Total &#8212; 27.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/carp.jpg" alt="Carp River" title="Carp River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/cache-la-poudre-plan.pdf" target="_blank" alt="Cache la Poudre River Management Plan (1.5 MB PDF)" target="_blank">Cache la Poudre River Management Plan (1.5 MB PDF)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Patty VerWiebe, U.S. Forest Service</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Carp River</h2>
<p>The Carp River, in Michigan's Upper Peninsula, flows through predominantly forested lands with little development along its way. Spring's high water provides for canoeing and offers steelhead fishing and dipping for smelt near the river's mouth. Summer is the time for brook or brown trout, and fall brings salmon fishing. The Carp is known for its outstanding recreation, wildlife, geologic, ecological, fisheries and heritage resource values. The river flows through the Mackinac Wilderness Area.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>