<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Skagit River, Washington';

// Set the page keywords
$page_keywords = 'Skagit River, Mt. Baker-Snoqualmie National Forest, Cascade River, Sauk River, Suiattle River, Glacier Peak Wilderness, Washington';

// Set the page description
$page_description = 'Skagit River, Washington';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('18');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Baker-Snoqualmie National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978. The segment from the pipeline crossing at Sedro-Wooley upstream to and including the mouth of Bacon Creek. The Cascade River from its mouth to the junction of its North and South Forks; the South Fork to the boundary of the Glacier Peak Wilderness Area. The Suiattle River from its mouth to the boundary of the Glacier Peak Wilderness Area at Milk Creek. The Sauk River from its mouth to its junction with Elliott Creek. The North Fork of the Sauk River from its junction with the South Fork of the Sauk to the boundary of the Glacier Peak Wilderness Area. The North Fork of the Cascade River from its confluence with the South Fork to the boundary of North Cascades National Park.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 100.0 miles; Recreational &#8212; 58.5 miles; Total &#8212; 158.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/skagit.jpg" alt="Skagit River" title="Skagit River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/mbs/landmanagement/resourcemanagement/?cid=stelprdb5228912" alt="Skagit River (U.S. Forest Service)" target="_blank">Skagit River (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Skagit River</h2>
<p>The Skagit River, in Northwest Washington, is the largest and most biologically important river draining to Puget Sound. The Skagit Wild and Scenic River System includes a portion of the Skagit River from Bacon Creek downstream to just east of the town of Sedro Woolley, which is classified as recreational. Three tributaries&#8212;the Sauk, Suiattle and Cascade Rivers&#8212;are classified as scenic. The system is 158.5 miles and includes over 38,000 acres. Approximately 50% of the river system is in private ownership, primarily in the Skagit and lower Sauk.</p>
<p>Whitewater rafting is popular on the Sauk and Suiattle Rivers. Commercial guides require permits from the U.S. Forest Service. Private boaters do not require permits. Winter eagle viewing on the upper Skagit draws thousands of visitors from November to February. Fishing for steelhead, coho and Chinook is popular on the Skagit and lower Sauk rivers. The outstandingly remarkable values are fish, wildlife and scenic quality. The upper Suiattle, upper Cascade and upper Sauk are on the Mt. Baker-Snoqualmie National Forest.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>