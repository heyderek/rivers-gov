<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Rapid River, Idaho';

// Set the page keywords
$page_keywords = 'Rapid River, Hells Canyon, Hells Canyon National Recreation Area, Idaho';

// Set the page description
$page_description = 'Rapid River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('11');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Nez Perce National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 31, 1975. The segment from the headwaters of the main stem to the national forest boundary. The segment of the West Fork from the wilderness boundary downstream to the confluence with the main stem.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 26.8 miles; Total &#8212; 26.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rapid.jpg" alt="Rapid River" title="Rapid River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.or.blm.gov/salem/html/rec/quartz.htm" alt="Recreation on Quartzville Creek (Bureau of Land Management)" target="_blank">Recreation on Quartzville Creek (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<p>Jonathan Berman</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Rapid River</h2>

<p>The water quality of Rapid River is exceptional; the river contains three listed fish species, chinook salmon, steelhead and bull trout, and associated critical habitat. The river's scenery is also outstanding; the steep gradient and narrow canyon focus the viewer's perspective on the fast-moving water and diverse riparian vegetation.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>