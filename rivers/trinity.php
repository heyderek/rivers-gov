<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Trinity River, California';

// Set the page keywords
$page_keywords = 'Trinity River, Hoopa Valley Indian Reservation, Klamath River, Salmon-Trinity Primitive Area, California';

// Set the page description
$page_description = 'Trinity River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('SD11');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Redding Field Office<br />
California Resources Agency<br />
Hoopa Valley Indian Reservation<br />
U.S. Forest Service, Shasta-Trinity National Forest<br />
U.S. Forest Service, Six Rivers National Forest<br />
Yurok Tribe</p>
<br />
<h3>Designated Reach:</h3>
<p>January 19, 1981. From the confluence with the Klamath River to 100 yards below Lewiston Dam. The North Fork from the Trinity River confluence to the southern boundary of the Salmon-Trinity Primitive Area. The South Fork from the Trinity River confluence to the California State Highway 36 Bridge crossing. The New River from the Trinity River confluence to the Salmon-Trinity Primitive Area.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 44.0 miles; Scenic &#8212; 39.0 miles; Recreational &#8212; 120.0 miles; Total &#8212; 203.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/trinity.jpg" alt="Trinity River" title="Trinity River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/srnf/recreation/wateractivities/recarea/?recid=11519&actid=79" alt="Trinity River (U.S. Forest Service)" target="_blank">Trinity River (U.S. Forest Service)</a><br />
<a href="http://www.fs.usda.gov/recarea/srnf/recreation/wateractivities/recarea/?recid=11584&actid=79" alt="Klamath River (U.S. Forest Service)" target="_blank">Klamath River (U.S. Forest Service)</a><br />
<p><a href="../documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Trinity River Eligibility Report" target="_blank">Trinity River Eligibility Report</a><br />
<a href="../documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Trinity River Environmental Impact Statement" target="_blank">Trinity River Environmental Impact Statement</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Trinity River</h2>

<p>This major tributary of California's Klamath River begins in the rugged Trinity Alps and makes its way through wilderness before meeting up with the mighty Klamath at Weitchpec. The Trinity is noted for its salmon and steelhead fishery resources, as well as its attraction to rafters, kayakers and canoeists. The North and South Forks of the Trinity and the New River are included in the designation. The Trinity River offers a wide variety of opportunities for fun, family and fishing.</p>

<p>The natural beauty has been one of the most popular sights for visitors to the north coast. Scenic Highway 299 makes easy access to many points of interest. Rafts, canoes and kayaks frequent the rapids in the springtime, and tubers enjoy summertime flows.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Trinity is legendary for its salmon and steelhead fishing by drift boat or walk-in riverside spots, as well as for trophy brown trout. Chinook salmon are the most sought-after gamefish in the Trinity River system. Spring-run salmon begin to enter the river in May and provide trophy fishing through November. Although brown trout are not native, they were heavily stocked until the late 1970s. Today, a self-sustaining population remains in the upper river, providing fly and bait fishing.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>