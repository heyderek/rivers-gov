<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Roaring River (South Fork), Oregon';

// Set the page keywords
$page_keywords = 'Roaring River, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Roaring River (South Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('174');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From its headwaters to its confluence with Roaring River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.6 miles; Total &#8212; 4.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/roaring-sf.jpg" alt="South Fork Roaring River" title="South Fork Roaring River" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detailfull/elyunque/home/?cid=fsbdev3_042977&width=full#wild_scenic" alt="Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)" target="_blank">Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)</a></p>-->

<div id="photo-credit">
<!--<p>Photo Credit: Martha Moran</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Roaring River (South Fork)</h2>
<p>The South Fork Roaring River originates in the Rock Lakes Basin in Roaring River Wilderness. The area is in the Cascade Mountain Range in northwest Oregon. The entire 4.6-mile segment of the South Fork Roaring River from its headwaters to its confluence with Roaring River is administered as a wild river. Its outstandingly remarkable value is wildlife.</p>
<p>The river flows through a narrow, deeply incised canyon which has large rock outcroppings and cliffs along portions of the canyon. Old-growth trees are predominant along the river, and the river itself flows over numerous cascades and through several pools. The corridor provides prime quality habitat for northern spotted owl. Owls are known to nest there. With almost no development in the area within and around the corridor, habitat quality is considered excellent.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>