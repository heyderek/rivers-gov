<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Little Missouri River, Arkansas';

// Set the page keywords
$page_keywords = 'Ouachita National Forest, Little Missouri River, Arkansas';

// Set the page description
$page_description = 'Little Missouri River, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('138');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ouachita National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992. From its origin to the west section line of section 22, T4S, R27W; from the southern property line between national forest lands and private lands to the north line of the NW 1/4 of the SW 1/4 of section 5, T5S, R27W.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.4 miles; Scenic &#8212; 11.3 miles; Total &#8212; 15.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/little-missouri.jpg" alt="Little Missouri River" title="Little Missouri River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://dnr.state.oh.us/tabid/1862/" alt="Little Miami River (Ohio Department of Natural Resources)" target="_blank">Little Miami River (Ohio Department of Natural Resources)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Mike Welch</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Little Missouri River</h2>
<p>The stream's first section from its source to the Albert Pike Campground is not one for floating. This upper stretch has its merits; however, a chief attraction is the Little Missouri Falls area which has been developed for day-use activities (i.e., no camping) by the Ouachita National Forest. While there are no developed facilities between the falls and Albert Pike, the river corridor offers possibilities for all kinds of outdoor pursuits&#8212;swimming in deep pools, hiking along the streambank and wildlife photography, just to name a few.</p>
<p>The run from Albert Pike Campground to Arkansas 84 is one of the best in the state. It begins on the National Forest near the junction of Forest Roads 73 and 106 and continues for about 8.5 rough-and-tumble miles. This stretch of the river heads downhill at a good clip&#8212;25 feet per mile. The rapids are exciting (up to Class IV in high water), with many featuring standing waves at their bases. Along the way floaters will pass the mouth of Greasy Creek, near which Albert Pike&#8212;the famed pioneer lawyer, general and poet&#8212;once lived in a well-appointed cabin. More noticeable will be Winding Stair Rapid, a series of drops that may well put water into one's boat. The rapid, which is approximately three miles below the put-in, can, and should, be scouted from the left (east) bank; heavy flows can put it in the Class IV level. The remainder of the float features numerous rapids in the Class I/Class III categories, including a diagonally running ledge about a quarter mile below Winding Stair that can be tricky.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>