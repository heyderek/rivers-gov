<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Crescent Creek, Oregon';

// Set the page keywords
$page_keywords = 'Deschutes National Forest, Crescent Creek, Oregon';

// Set the page description
$page_description = 'Crescent Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('71');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Deschutes National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From SW 1/4 of Section 11, T24S, R6E to the west section line of Section 13, T24S, R7E.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 10.0 miles; Total &#8212; 10.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/crescent-creek.jpg" alt="Crescent Creek" title="Crescent Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/id/st/en/prog/blm_special_areas/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Crescent Creek</h2>
<p>The segment of Crescent Creek below the Crescent Creek Dam flows through a narrow canyon with old-growth pine in the lower portion; the scenery is its outstandingly remarkable resource.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>