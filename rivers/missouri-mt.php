<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Missouri River, Montana';

// Set the page keywords
$page_keywords = 'Missouri River, Montana, Lewis and Clark';

// Set the page description
$page_description = 'Missouri River, Montana';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('14');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Lewistown Field Office</p>
<br />
<h3>Designated Reach:</h3>
<p>October 12, 1976. The segment from Fort Benton downstream to Robinson Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 64.0 miles; Scenic &#8212; 26.0 miles; Recreational &#8212; 59.0 miles; Total &#8212; 149.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/missouri-upper.jpg" alt="Missouri River" title="Missouri River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/visit/upper-missouri-national-wild-and-scenic-river" alt="Missouri River (Bureau of Land Management)" target="_blank">Missouri River (Bureau of Land Management)</a><br />
<a href="http://www.nps.gov/mnrr/" alt="Missouri National Recreational River (National Park Service)" target="_blank">Missouri National Recreational River (National Park Service)</a><br />
<a href="../documents/plans/upper-missouri-plan.pdf" title="Upper Missouri River Management Plan" target="_blank">Upper Missouri River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Rich Deline</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Missouri River (Montana)</h2>

<p>The Missouri is the longest river in the United States, flowing more than 2,500 miles from its source on the eastern slope of the Rockies near Three Forks, Montana, to its confluence with the Mississippi River at St. Louis, Missouri. Congress designated 149 miles of the Upper Missouri as a component of the National Wild and Scenic River System in 1976, calling it an irreplaceable legacy of the historic American west. The Upper Missouri National Wild and Scenic River (UMNWSR) section starts at Fort Benton, Montana, and runs 149 miles downstream ending at the James Kipp Recreation Area.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fish</em></strong></p>

<p>Forty-nine species of fish (ranging from 1/2-oz. minnows to 140 lb. paddlefish) reside in the river. Fishermen are most likely to catch goldeye, drum, sauger, walleye, northern pike, channel catfish, carp and small mouth buffalo. Of the six remaining paddlefish populations in the United States, the Upper Missouri's appears to be the largest in average size. Generally only taken by snagging in the spring during upstream spawning runs, they are excellent table fare. Occasionally floaters may see these lunkers roll on the surface. Other unusual species in the river are the endangered pallid sturgeon and shovel nose sturgeon.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The valley of the Upper Missouri is a living museum, the product of many events over time. The land was originally laid down in horizontal layers, the sediments and shorelines of a great inland sea that once covered most of the Great Plains. These layers have since been folded, faulted, uplifted, modified by volcanic activity and sculpted by glaciers. Erosion then added to the variety seen along the river today, a landform known as the Breaks.</p>

<p><strong><em>Historic</em></strong></p>

<p>As a route of western expansion, the Missouri River had few equals. Lewis and Clark spent three weeks, from May 24 through June 13, 1805, exploring the segment that is now the Upper Missouri National Wild &amp; Scenic River. Today, this portion is considered to be the premier component of the Lewis & Clark National Historic Trail. </p>

<p><strong><em>Recreational</em></strong></p>

<p>The public lands of the Upper Missouri River Breaks National Monument, both under federal and state management, make a significant contribution to the local lifestyle and the regional economy. Within the monument you can float the river, fish, hike, hunt, camp, drive for pleasure, find a little solitude, enjoy a sense of exploration, or simply marvel at the variety of resources around you. Vast portions of the monument are serviced only by graveled and unimproved roads. Much of the monument is not accessible by any road; visitors are invited to explore on foot.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Most of the 60 species of mammals, 233 species of birds, and 20 species of amphibians and reptiles that inhabit the Upper Missouri River valley are dependent in one way or another upon the riparian zone. Among the more common species are white-tailed deer and pheasant. While at one time they only visited the area during the late fall and winter, bald eagles are again nesting in cottonwood snags.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>