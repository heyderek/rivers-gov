<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Andreafsky River, Alaska';

// Set the page keywords
$page_keywords = 'Yukon Delta National Wildlife Refuge, Andreafsky River, Alaska, bristle-thighed curlew, Alaska National Interest Lands Conservation Act, ANILCA, Nulato Hills';

// Set the page description
$page_description = 'Andreafsky River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('38');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Fish &amp; Wildlife Service, Yukon Delta National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. From its source, including all headwaters, and the East Fork, within the boundary of the Yukon Delta National Wildlife Refuge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 262.0 miles; Total &#8212; 262.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/american-nf.jpg" alt="" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/ca/st/en/fo/folsom/nfamerican.html" target="_blank" alt="North Fork of the American River (Bureau of Land Management)" target="_blank">North Fork of the American River (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<!--<p>Photo Credit: Bureau of Land Management</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Andreafsky River</h2>
<p>The Andreafsky River, including the East Fork, was designated a national wild river by the Alaska National Interest Lands Conservation Act (ANILCA) in 1980. The river received the designation due to its natural and free-flowing condition, water quality, wildlife, geology and primitive setting. This designation covers approximately 265 river miles, of which approximately 198 miles are within designated wilderness, 54 miles cross private lands and 13 miles cross non-wilderness refuge lands. Along these latter stretches, a wild river corridor has been established.</p>
<p>The Andreafsky National Wild River lies within the Andreafsky Wilderness in the northern portion of the Yukon Delta National Wildlife Refuge. The Andreafsky and its East Fork flow southwest and drain into the Yukon River. The village of St. Marys lies on the west bank 1-1/4 miles below where the main stem and East Fork join.</p>
<p>Included within the wilderness are the southern extension of the Nulato Hills, where the bristle-thighed curlew&#8212;a unique species of large upland nesting shorebird&#8212;has its primary nesting grounds. The highlands around both forks of the Andreafsky are one of two known nesting areas for the bird. Both forks are scenic; the East Fork, however, has more trees and is closer to the mountains. Vegetation consists primarily of black and white spruce, balsam poplar, and extensive bogs. Willow shrubs, mosses and lichens, and flowers grow on the tundra of the surrounding hills. Wildlife along the rivers includes bears, foxes, beavers, eagles (both bald and golden), falcons, hawks, owls and geese. Fishing is excellent for salmon, grayling and dolly varden. Both rivers have large populations of brown bears. During late June and July, the bears can be seen feeding on fish along riverbanks, and in August, they move to upland areas to feed on berries. Visitors should inform themselves about bear behavior, how to conduct oneself in bear country, and take appropriate precautions.</p>
<p>Neither river is free of ice until June 1 or later. Water during June is high and turbid. Water during July is low, creating several exposed gravel bars. By mid-August, the rivers are usually high again due to summer rains. September weather is cool, but the rivers are usually floatable throughout the month. The current in these clearwater rivers is 3 mph, except for the lower 15 miles where it dramatically slows. Both rivers offer Class I water on the International Whitewater Scale for their entire length.</p>
<p>Both rivers are used by local residents for subsistence hunting, fishing and trapping. During early June, the lower portions are used for waterfowl hunting, and moose hunting occurs during September.</p>
<p>Access to the Andreafsky and East Fork Rivers is difficult at best. From St. Marys, you must arrange to either: 1) have your raft and gear hauled upriver by a local person using a powerboat; or 2) charter a small airplane to take you to a convenient dropoff point. During most of the season, a powerboat can go upstream about 60 miles to the vicinity of Allen Creek. Only one lake that is large enough for floatplane landings occurs along these rivers. A small airplane can land on gravel bars along the river or on some ridgetops. In addition to rafting and sportfishing, day hikes or extended backpacking trips can be conducted in the hills that parallel both forks.</p>
<p>Preserving its wild character for future generations is made easier due to the difficulty in accessing the Andreafsky and East Fork Rivers.</p><br />

<table align="center" border="0" cellpadding="0" cellspacing="5" width="100%">
  <tr>
    <td align="left" colspan="2" valign="top">Charter services are quite limited and guide services do not exist in the area.  A visitor will need to research extensively in order to set up a float trip. The following addresses may assist potential visitors:</td>
  </tr>
  <tr>
    <td align="left" valign="top" width="50%">City of St. Marys<br />Post Office Box 163<br />St. Marys, Alaska  99658<br />(907) 438-2247</td>
    <td align="left" valign="top" width="50%">Hagland Aviation<br />Post Office Box 195<br />St. Marys, Alaska  99658<br />(907) 438-2247 or 1-800-478-2246</td>
  </tr>
</table>

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>