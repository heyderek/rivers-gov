<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Big and Little Darby Creeks, Ohio';

// Set the page keywords
$page_keywords = 'Big Darby Creek, Little Darby Creek, Ohio';

// Set the page description
$page_description = 'Big and Little Darby Creeks, Ohio';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('SD16');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Midwest Regional Office<br />Ohio Department of Natural Resources, Division of Natural Areas and Preserves</p>
<br />
<h3>Designated Reach:</h3>
<p>March 10, 1994. Upper Darby Creek from the Champaign-Union County line to the Conrail railroad trestle (0.9 miles upstream of U.S. 40). Lower Darby Creek from the confluence with Little Darby Creek near Georgesville to the Scioto River. Little Darby Creek from the Lafayette-Plain City Road Bridge to 0.8 miles upstream from the confluence with Big Darby Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 85.9 miles; Total &#8212; 85.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/big-darby-creek.jpg" alt="Big Darby Creek" title="Big Darby Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://watercraft.ohiodnr.gov/scenic-rivers/list-of-ohios-scenic-rivers/darby-big-little" target="_blank" alt="Big &amp; Little Darby Creeks (Ohio Department of Natural Resources)" target="_blank">Big &amp; Little Darby Creeks (Ohio Department of Natural Resources)</a></p>

<div id="photo-credit">
<p>Photo Credit: Anthony Sasson</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Big &amp; Little Darby Creeks</h2>
<p>Big and Little Darby Creeks are located in the corn belt region of Ohio within an easy drive of Columbus. They are among the top streams in biological quality in Ohio and the Midwest. There are over 100 recorded fish and 44 mussel species, including 37 species of fish and mussels that are endangered or otherwise rare. Several other fish species found in the streams are declining in numbers throughout Ohio.</p>
<p>Water quality is excellent, with the creeks being classified as "Exceptional Warmwater Habitat" by the state of Ohio under the Clean Water Act and as "Outstanding State  Waters," the highest level of protection under the state's anti-degradation policy.</p>
<p>Big and Little Darby Creeks have been designated a "Last Great Place" by The Nature Conservancy.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>