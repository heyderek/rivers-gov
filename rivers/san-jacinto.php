<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'San Jacinto River (North Fork), California';

// Set the page keywords
$page_keywords = 'North Fork San Jacinto River, California, San Bernardino National Forest';

// Set the page description
$page_description = 'San Jacinto River (North Fork), California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('200');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, San Bernardino National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the source of the North Fork San Jacinto River at Deer Springs in Mt. San Jacinto State Park to the northern boundary of Section 17, Township 5 South, Range 2 East.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 7.2 miles; Scenic &#8212; 2.3 miles; Recreational &#8212; 0.7 miles; Total &#8212; 10.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/san-jacinto.jpg" alt="San Jacinto River" title="San Jacinto River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/ipnf/recreation/recarea/?recid=6891" alt="St. Joe River (U.S. Forest Service)" target="_blank">St. Joe River (U.S. Forest Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Doug Steakley, American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>San Jacinto River (North Fork)</h2>
<p>The source of the North Fork San Jacinto River is on San Jacinto Peak in the Mount San Jacinto State Game Preserve and Wilderness Area. The Pacific Crest National Scenic Trail crosses the upper river. Above its confluence with Fuller Mill Creek there is a picnic area, fishing access and a trail to lovely waterfalls. While there is no camping in the river corridor, there are four campgrounds nearby.</p>
<p>The river supports habitat for many at-risk species, including mountain yellow-legged frogs, California spotted owls, the rubber boa and the San Bernardino flying squirrel.</p>

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>