<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Owyhee River (South Fork), Idaho';

// Set the page keywords
$page_keywords = 'South Fork of the Owyhee River, Idaho';

// Set the page description
$page_description = 'Owyhee River (South Fork), Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('194');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The South Fork of the Owyhee River upstream from its confluence with the Owyhee River to the upstream boundary of the Owyhee River Wilderness at the Idaho Nevada State border.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 30.2 miles; Recreational &#8212; 1.2 miles; Total &#8212; 31.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/owyhee-sf-id.jpg" alt="South Fork Owyhee River" title="South Fork Owyhee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/sites/blm.gov/files/documents/files/Media-Center_Public-Room_Idaho_Bruneau-Jarbidge-Owyhee_BoaterGuide.pdf" alt="Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide" target="_blank">Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14982/2" alt="South Fork Owyhee River (Bureau of Land Management)" target="_blank">South Fork Owyhee River (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Robin Fehlau</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Owyhee River (South Fork)</h2>

<p>The South Fork of the Owyhee is a tributary of the Owyhee and is mainly designated wild as it enters Idaho from Nevada; a short portion of the river, where the river crosses private land, is managed as a recreational river. This river is known as an outstanding wilderness river experience because of the canyon's scenic qualities, solitude opportunities, wildlife viewing and length of trip available.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>Many Owyhee Canyonlands stream corridors were the major locations of permanent water, fuel and varied animal and vegetable materials for early Native Americans and, as such, could have supported campsites. According to representatives for the Shoshone Paiute Tribes, the canyonlands and rivers have provided essential resources, shelter and life for the Shoshone and Paiute people for countless generations.</p>

<p>Traditionally, the tribes subsisted on seasonally available game, such as antelope, deer, elk, bison (residents of southern Idaho until the early 19th century) and bighorn sheep; roots and bulbs, such as bitterroot and arrowleaf balsamroot; and small game such as rabbits, ground squirrels, sage hens and insects. They have cultivated about 150 plant species, and some authors estimate that more than 50 percent of the tribes' food came from root crops&#8212;camas bulb, yampa root, tobacco root and bitterroot boiled or steamed in pits and stored in caches for later use.</p>

<p>The Owyhee, Bruneau and Jarbidge rivers were also the migration route and spawning areas for anadromous fish. An element of tribal culture was lost when dams blocked the salmon from migrating into the upper reaches of these rivers, but fishing stories and sites still remain an important part of its tradition and history.</p>

<p>Petroglyphs, pictographs, rock alignments, shrines and vision quest sites are located throughout the canyonlands. Tribal members still frequent the canyonland areas to hunt, fish, pray and conduct ceremonies.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Redband trout are found in the South Fork of the Owyhee River and the Owyhee River and its other designated tributaries&#8212;the North Fork of the Owyhee River, Battle Creek, Deep Creek, Dickshooter Creek and Red Canyon. However, warmer summer water temperatures, in part the result of upstream impoundments in the Duck Valley Indian Reservation, do not support productive redband fisheries. The seasonal migration of smallmouth bass into the Owyhee River system over the past several decades, and favorable habitat conditions and water quality parameters for bass, has created competition for food and space between smallmouth bass and trout in many Owyhee tributaries.</p>

<p>The success of fisheries in these systems depends on appropriate flows during key life stages; resident fish species are adapted to the flashy, desert flows. Summer and early fall low flows and high flows that may occur only every few years are integral to the maintenance of pool depths and channel diversity.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Owyhee River and its tributaries are located in the "Owyhee Volcanic Field," an area of volcanic activity from the Miocene Era (25 to 3 million years ago). Where overlying basalt is present, rhyolite (rock created by lava flow) formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved immense monolithic cliffs and numerous sculptured pinnacles known as "hoodoos." The most dramatic area of hoodoo formations within the entire Owyhee River system are located in the South Fork canyons.</p>

<p>The Owyhee, Bruneau and Jarbidge river systems provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States. The presence of these geologic formations in such great abundance and expansiveness makes the designated river segments nationally unique.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Owyhee River and its major tributaries are generally rated as Class II whitewater, although several Class III or IV rapids exist on the South Fork. Depending on the water level, some portaging may be necessary at Class III rapids. Rafts under 15' are ideal for this section.</p>

<p>Floating in many areas is enhanced by outstanding day-hiking opportunities. It is possible to hike from canyon rims to the stream in many locations, especially during low-water periods. Due to their meandering character, diversity of landforms and topographic screening, the canyons provide exceptional opportunities for solitude and for passive, primitive and/or physically challenging activities, including hiking, wildlife viewing, photography, floating, angling and camping.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Owyhee River canyons are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Talus slopes feature mosaics of medium-textured, yellow and subdued green sagebrush-bunchgrass and/or dark green juniper, as well as either reddish rhyolite or coarse-textured, blackish basalt rubble fields.</p>

<p>Spring rains bring rich green riparian vegetation along brownish high water. Summertime low flows are tinted with green and brown channel colors and expose whitish, medium-textured stream-bottom gravel and boulders.</p>

<p>The basalt and rhyolite canyon/riparian associations found along the designated Owyhee, Bruneau and Jarbidge segments are among the best representations of this landscape in the region. They provide exceptional combinations of line, form, color and texture found amidst this close association of landforms, water and vegetation.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee provides upland and canyon riparian habitat for California bighorn sheep, elk, mule deer and pronghorn. The Owyhee River, in combination with Battle Creek, Deep Creek, Duncan Creek and Wickahoney Creek, supports the majority of the bighorn sheep population in the Owyhee Canyonlands.</p>

<p><Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents, rabbits, shrews, bats, weasels, and skunks. The waters along the entire Owyhee River system and its tributaries are considered outstanding habitat for river otters./p>

<p>A variety of birds occur in the area, including songbirds, waterfowl, shorebirds and raptors. The high, well-fractured and eroded canyon cliffs are considered outstanding habitat for cliff nesting raptors, a small number of which occasionally winter along the canyon walls.  Other wildlife includes several reptile (snake and lizard) and a few amphibian (frogs, toads and salamanders) species.</p>

<p>The Owyhee River area is considered Preliminary Priority Habitat for greater sage-grouse. Idaho BLM sensitive species include bald eagle yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>