<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Black River, Michigan';

// Set the page keywords
$page_keywords = 'Ottawa National Forest, Black River, Michigan';

// Set the page description
$page_description = 'Black River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('120');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ottawa National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From the Ottawa National Forest Boundary to Lake Superior.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 14.0 miles; Total &#8212; 14.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/black.jpg" alt="Black River, Michigan" title="Black River, Michigan" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.michigandnr.com/publications/pdfs/wildlife/viewingguide/up/03Black/" alt="Michigan Department of Natural Resources" target="_blank">Michigan Department of Natural Resources</a></p>
<p><a href="../documents/plans/ottawa-nf-plan.pdf" alt="Black River Management Plan (1.4 MB PDF)" target="_blank">Black River Management Plan (1.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Black River</h2>
<p>The Black River provides visitors with outstanding scenery, unique geographical features, superb fisheries, cultural history and abundant recreation opportunities. The many waterfalls, rapids and gorge-like landscapes along this river, combined with a mix of large hemlock and eastern white pine, has long been recognized as a distinctive resource. Outstandingly remarkable values include scenery, recreation, geology, fish, wildlife and heritage.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>