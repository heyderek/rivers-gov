<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Black Butte River, California';

// Set the page keywords
$page_keywords = 'Black Butte River, Mendicino National Forest, California';

// Set the page description
$page_description = 'Black Butte River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('168');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mendocino National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 17, 2006. The segment from the Mendocino County line to its confluence with the Middle Eel River and Cold Creek from the Mendocino County line to its confluence with the Black Butte River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 17.5 miles; Scenic &#8212; 3.5 miles; Total &#8212; 21.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/black-butte.jpg" alt="Black Butte River" title="Black Butte River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">

<div id="photo-credit">
<p>Photo Credit: Greg Hodson, Mendocino National Forest</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Black Butte River</h2>
<p>The rugged Black Butte River flows through the Grindstone and Covelo Ranger Districts of the Mendocino National Forest. An additional wild river segment is included in the designation, and encompasses 1.5 miles of Cold Creek from the Mendocino County line on the east to its confluence with the Black Butte River to the west. The Black Butte river terminates at its confluence with the Eel River, also a National Wild and Scenic River, in the vicinity of the Eel River Work Center and the Eel River Campground.</p>
<p>Steelhead are fairly numerous in addition to a limited number of Chinook salmon. However, the entire designated river corridor is closed to fishing in order to protect the juvenile steelhead trout. Terrain within the river corridor is rugged, with limited access. Cultural resources are an important component of this wild and scenic river, and numerous cultural resource values associated with the Yuki Indians have been identified within the area.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>