<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Red Canyon River, Idaho';

// Set the page keywords
$page_keywords = 'Red Canyon, Idaho';

// Set the page description
$page_description = 'Red Canyon River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('192');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Red Canyon from its confluence with the Owyhee River to the upstream boundary of the Owyhee River Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.6 miles; Total &#8212; 4.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/red-canyon.jpg" alt="Red Canyon" title="Red Canyon" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14980/2" alt="Red Canyon (Bureau of Land Management)" target="_blank">Red Canyon (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Casey Steenhoven</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Red Canyon</h2>

<p>Red Canyon flows south, and the stream cuts a narrow, deep gorge through the rolling plateau landscape on its way to the Owyhee River. Like the other canyons of the Owyhee River system, Red Canyon contains basalt and rhyolite walls. The canyon provides outstanding hiking and backpacking opportunities.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Red Canyon supports the sensitive redband trout populations; however, warmer summer water temperatures are insufficient to support productive redband fisheries. Seasonal migration of smallmouth bass into the Owyhee River system over the past several decades suggests that conditions may favor the development of a cool water fishery. However, the arrival of smallmouth bass would like initiate competition for food and space between bass and trout.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Red Canyon is part of the Owyhee, Bruneau and Jarbidge river systems, home of the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States, products of Miocene Era (23 to 5 million years ago) volcanic formations. Where overlying basalt is present, rhyolite (rock which began as lava flow) formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved monolithic cliffs and numerous sculptured pinnacles known as "hoodoos." The presence of these geologic formations in such great abundance and expansiveness make Red Canyon, the nearby Owyhee River and other Owyhee tributaries geologically unique.</p>

<p><strong><em>Recreational</em></strong></p>

<p>High-quality day-hiking opportunities are available, especially during low-water periods. The remoteness provides exceptional opportunities for solitude, wildlife viewing and photography.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Landscape forms throughout the Owyhee Canyonlands vary from broad, open sagebrush steppes to narrow canyons, some exceeding 800 feet in depth. The canyons are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Intertwined with the vertical features are some very steep diagonal lines that frame triangular forms associated with talus slopes. The slopes have a mosaic of medium-textured, yellow and subdued green sagebrush-bunchgrass communities and/or dark green juniper, as well as either medium-textured, reddish rhyolite rubble fields or coarse-textured, blackish basalt rubble fields.</p>

<p>Spring rains support medium-textured, rich, green riparian vegetation along streams that run brownish in high flows amidst both large boulder-strewn rapids and calm reaches. During summer months, sparkling pools and slow-moving water tinted with green and brown channel colors reflect blue sky and a blend of forms, colors and lines from surrounding cliffs and steep slopes.</p>

<p>The landscapes found in Red Canyon and throughout the designated Owyhee, Bruneau and Jarbidge segments&#8212;combining line, form, color and texture found amidst close associations of landforms, water and vegetation&#8212;are among the best representations of basalt and rhyolite canyon/riparian associations in the region.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Big game commonly found in the area include elk, mule deer and pronghorn, and the area's steep cliffs and alcoves along the canyons provide key critical lambing and escape habitat for bighorn sheep. Red Canyon, in combination with the Owyhee River, Battle Creek, Deep Creek, Duncan Creek and Wickahoney Creek, support the majority of the bighorn sheep population in the Owyhee Canyonlands.</p>

<p>Red Canyon offers Preliminary Priority Habitat for the greater sage-grouse. Other sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>

<p>Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents, rabbits, shrews, weasels and skunks. The waters along the entire Owyhee River system and its tributaries are considered outstanding habitat for river otters.</p>

<p>Birds in the area also include songbirds, waterfowl, shorebirds and raptors. The high, well-fractured and eroded canyon cliffs are considered outstanding habitat for cliff nesting raptors, a small number of which occasionally winter along the canyon walls of the upper Owyhee River system and its major tributaries.</p>

<p>Other wildlife includes several species of reptiles (snakes and lizards) and amphibian (frogs, toads and salamanders).</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>