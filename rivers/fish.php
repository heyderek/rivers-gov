<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Fish Creek, Oregon';

// Set the page keywords
$page_keywords = 'Fish Creek, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Fish Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('179');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From its headwaters to its confluence with the Clackamas River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 13.5 miles; Total &#8212; 13.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/fish.jpg" alt="Fish Creek" title="Fish Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detailfull/plumas/about-forest/?cid=stelprdb5097323&width=full" alt="Feather River (U.S. Forest Service)" target="_blank">Feather River (U.S. Forest Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Fish Creek</h2>
<p>Fish Creek is located on the western slope of the Cascade Range in northwest Oregon. It flows through a narrow canyon near the headwaters and into a broader canyon as it nears the confluence with the Clackamas River. The four-mile section ending at the Clackamas is suited for whitewater paddling by experienced boaters.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>Fish Creek is very important as potential anadromous fish habitat for spring Chinook, winter and summer steelhead, and winter run coho salmon. The creek once provided excellent habitat, and while past management practices and flooding have eroded its quality, an ambitious restoration effort is taking place.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>