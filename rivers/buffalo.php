<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Buffalo River, Arkansas';

// Set the page keywords
$page_keywords = 'Ozark National Forest, St. Francis National Forest, Buffalo River, Arkansas';

// Set the page description
$page_description = 'Buffalo River, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('135');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ozark-St. Francis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992. From its origin to the Ozark National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 9.4 miles; Scenic &#8212; 6.4 miles; Total &#8212; 15.8 miles.</p>
</div>

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div><!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/buffalo.jpg" alt="Buffalo River" height="204px" title="Buffalo River" width="265px" />
</div><!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/buffalo-plan.pdf" target="_blank" alt="Buffalo River Management Plan (2.3 MB PDF)" target="_blank">Buffalo River Management Plan (2.3 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: John Moore</p>
</div><!--END #photo-credit -->

</div><!--END #photo-details -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div><!--END #rivers-box -->

<div id="lower-content">
<h2>Buffalo River</h2>

<p>The Buffalo Wild and Scenic River is located in northwestern Arkansas. This segment of the river flows from its headwaters through the Upper Buffalo Wilderness to the boundary of the Ozark National Forest. From the National Forest boundary, 135 miles of the river, to its confluence with White River, was designated as the Buffalo National River in 1972 and is managed as a unit of the National Park Service.

<p>The upper river (the portion designated as a component of the National Wild and Scenic Rivers System) offers outstanding scenery. This approximate 16-mile segment is characterized by highly varied and strongly dissected terrain with uneven, sharp ridges and/or cliffs with significant vertical relief, large unusual rock outcrops or formations and slopes greater than 35%. The stream is clear and exhibits rapids and still pools with reflecting qualities.</p>

<p>Recreational opportunities include camping, hiking and fishing, with most of this use occurring in the Upper Buffalo Wilderness. Experienced whitewater boaters float the wild segment immediately following rain storms, with river difficulty rated Class III-IV and an average gradient drop of 38 feet per mile.</p>

<p>A variety of animal life is found within the corridor. Mammals, such as eastern pipistrelle bats, gray squirrels, raccoons, white-tailed deer, wood rats and black bear, occur. Neotropical migrant birds, such as the ovenbird, wood thrush and red eyed vireo, are well represented, along with species like the Louisiana waterthrush and parula warblers, which favor riparian habitat. In the corridor, where mixed herbaceous plants and shrubs are found, species such as the yellow-breasted chat, yellowthroat and summer tanager may be found.</p>

<p>The Buffalo River is managed for smallmouth bass with special length and creel limits. The upper reaches of the river differ in character from downstream reaches where the river widens and supports larger fishable pools.</p>
</div><!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>