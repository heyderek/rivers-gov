<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Feather River (Middle Fork), California';

// Set the page keywords
$page_keywords = 'Plumas National Forest, Feather River, Feather Falls, California';

// Set the page description
$page_description = 'Feather River (Middle Fork), California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('3');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Plumas National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968. The entire Middle Fork downstream from the confluence of its tributary streams one kilometer south of Beckwourth, California.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 32.9 miles; Scenic &#8212; 9.7 miles; Recreational &#8212; 35.0 miles; Total &#8212; 77.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/feather.jpg" alt="" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detailfull/plumas/about-forest/?cid=stelprdb5097323&width=full" alt="Feather River (U.S. Forest Service)" target="_blank">Feather River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/feather-plan.pdf" title="Feather River Management Plan" target="_blank">Feather River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Alice Bodnar</p>
</div>

<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Feather River</h2>
<p>The Middle Fork of the Feather River was one of the first nationally designated wild and scenic rivers. The river runs from it headwaters near Beckwourth, California, to Lake Oroville. The river gradient varies from gentle at the upper end to very steep in the deep canyons of the lower reaches. The adjacent lands range from the most primitive imaginable to manicured golf courses and residential area. Access is good by oiled roads in the upper portion and is nonexistent mile after mile in the wild river zones.</p>
<p>In the wild sections, huge boulders, cliffs and waterfalls are a part of the natural beauty of the area, but can make navigating the river and hiking difficult.</p>
<p>There are scenic opportunities throughout the river corridor. In the summer the sun sparkles off the river, and in the fall, the beautiful colors along the steep mountain slopes  make autumn spectacular. Bald eagles, mule deer and beaver are just a few of the varieties of wildlife you can expect to see.</p>
<p>Fishing, kayaking and swimming are abundant in the recreational portions of the river.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>