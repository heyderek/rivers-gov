<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Quartzville Creek, Oregon';

// Set the page keywords
$page_keywords = 'Quartzville Creek, Willamette National Forest, Oregon';

// Set the page description
$page_description = 'Quartzville Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('97');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Salem District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the Willamette National Forest boundary to the slack water of Green Peter Reservoir.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 12.0 miles; Total &#8212; 12.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/quartzville-creek.jpg" alt="Quartzville Creek" title="Quartzville Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/quartzville-creek-brochure.pdf" alt="Recreation on Quartzville Creek (Bureau of Land Management)" title="Recreation on Quartzville Creek (Bureau of Land Management)" target="_blank">Quartzville Creek (Bureau of Land Management) Recreational Corridor Brochure</a><br />
<a href="../documents/plans/quartzville-creek-plan.pdf" title="Quartzville Creek Management Plan" target="_blank">Quartzville Creek Management Plan</a></p>

<div id="photo-credit">
<p>Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Quartzville Creek</h2>

<p>Quartzville Creek begins in the Cascade Mountain Range of Oregon. The 12-mile (19.3 km) section that was designated in 1988 flows into Green Peter Reservoir near the town of Sweethome. Classified as a recreational section of river, Quartzville Creek is named for the gemstone that has been mined in the area and a ghost town that was once the center of two brief gold mining efforts (1863-1892). The Quartzville Back Country Byway parallels the winding river, offering views of old-growth forests and wildlife, and provides easy access to developed and undeveloped picnic and camping sites.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Recreational</em></strong></p>

<p>The corridor offers a variety of recreational opportunities, such as fishing, camping and boating for skilled whitewater paddlers. Quartzville Creek offers a 'bucket list' experience for Class IV whitewater boaters (primarily kayaking) when flows are high. Recreational mining attracts visitors from within and outside of the region who can experience a part of Oregon's mining history as they pan for gold. Visitors can also camp and fish for small stream trout, as well.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Visitors to Quartzville Creek enjoy seeing a unique, colorful combination of old-growth stands, cascading whitewater, water clarity, rocky outcrops and diverse vegetation of conifers and hardwoods. Much of the older forest along the byway is managed by the BLM and U.S. Forest Service to provide habitat for plant and animal species that rely on old-growth forest characteristics.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>