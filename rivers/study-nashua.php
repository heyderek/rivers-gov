<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Nashua River, Massachusetts';

// Set the page keywords
$page_keywords = 'Nashua River, Massachusetts, National Park Service';

// Set the page description
$page_description = 'Nashua River, Massachusetts';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('302');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<!--<?php
// includes ESRI
include ('../iframe.php');
?>-->

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<center><img src="images/nashua.jpg" alt="Nashua River, Massachusetts" width="509" height="310" title="Nashua River, Massachusetts"/></center>
<br />
<h3>Study Agency:</h3>
<p>National Park Service</p>
<br />
<h3>Study Segments:</h3>
<p>December 19, 2014 (Public Law 113-291). The mainstem from the confluence with the North and South Nashua Rivers in Lancaster, Massachusetts, north to the Massachusetts-New Hampshire State line, excluding the segment from the Route 119 Bridge in Groton, Massachusetts, downstream to the confluence with the Nissitissit River in Pepperell, Massachusetts. The Squannacook River from the headwaters at Ash Swamp downstream to the confluence with the Nashua River in the towns of Shirley and Ayer, Massachusetts. The Nissitissit River from the Massachusetts-New Hampshire State line downstream to the confluence with the Nashua River in Pepperell, Massachusetts.</p>
<br />
<h3>Mileage:</h3>
<p>32.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/nashua2.jpg" alt="Nashua River" title="Nashua River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../../documents/studies/nashua-study.pdf" title="Nashua River Reconnaissance Survey" target="_blank" alt="Nashua River Reconnaissance Survey (National Park Service)">Nashua River Reconnaissance Survey (National Park Service)</a></p>
<p><a href="https://www.wildandscenicnashuarivers.org" title="Nashua River Watershed Association" target="_blank" alt="Nashua River Watershed Association">Nashua Wild &amp; Scenic River Study</a></p>

<div id="photo-credit">
<p>Photo Credit: Joyce Kennedy Raymes</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Nashua River</h2>
<p>The Nashua River watershed is located in north-central Massachusetts and southeastern New Hampshire, encompassing 32 communities. It flows north and joins the Merrimack River in Nashua, New Hampshire. The watershed covers 538 square miles in total and supplies drinking water to over two million people. It is highly forested with abundant water resources and open spaces and has thousands of acres of land protected. According to the Nashua River Watershed Association, the lands are comprised of approximately 62&#37; forested land, 13&#37; residential lands and 12&#37; agricultural lands. The watershed area within Massachusetts is still rather rural, but is under constant threat of development due to its proximity to the greater Boston area. Some towns within the region have seen significant growth, resulting in loss of open space.</p>
<p>Besides the Squannacook and Nissitissit Rivers, the other major tributaries to the Nashua include the Quinapoxet River, the Stillwater River and the North and South Nashua Rivers. The river segment that runs through the town of Bolton is part of the Bolton Flats Wildlife Management Area managed by the Massachusetts Division of Fisheries and Wildlife. The river also runs through the Devens Regional Enterprise Zone, site of the former Fort Devens comprised of portions of the communities of Ayer, Harvard and Shirley.</p>
<p>The Nashua, Squannacook and Nissitissit Rivers are good candidates for wild and scenic river designation based on the preliminary evidence of free-flowing river conditions and the presence of multiple natural, cultural and recreational resources with the potential to meet the "Outstandingly Remarkable Values" threshold as defined by the Wild and Scenic Rivers Act. There is demonstrated local and regional interest and support for a study and existing river/watershed protection elements that would support the NPS framework for a Partnership Wild and Scenic River designation. In addition, local stakeholders have indicated an initial level of interest in developing the river management plan that would be developed as a part of the study process, which is required as a part of the designation.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>