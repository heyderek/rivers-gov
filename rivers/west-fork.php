<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'West Fork River (Sipsey Fork), Alabama';

// Set the page keywords
$page_keywords = 'Sipsey Fork, West Fork, Black Warrior River, Alabama';

// Set the page description
$page_description = 'Black Warrior River (Sipsey Fork), Alabama';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('66');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<script>
var river = {
}
</script>

<?php include_once ("../iframe.php"); ?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, William B. Bankhead National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the confluence of Sandy Creek upstream to the confluence of Thompson Creek and Hubbard Creek. Hubbard Creek from its confluence with Thompson Creek upstream to Forest Road 210. Thompson Creek from its confluence with Hubbard Creek upstream to its origin. Tedford Creek from its confluence with Thompson Creek upstream to section 17, T8S, R9E. Mattox Creek from its confluence with Thompson Creek upstream to section 36, T7S, R9W. Borden Creek from its confluence with the Sipsey Fork upstream to its confluence with Montgomery Creek. Montgomery Creek from its confluence with Borden Creek upstream to the SW 1/4 of section 36, T7S, R8W. Flannigan Creek from its confluence with Borden Creek upstream to section 4, T8S, R8W. Braziel Creek from its confluence with Borden Creek upstream to section 12, T8S, R9W. Hogood Creek from its confluence with Braziel Creek upstream to section 7, T8S, R8W.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 36.4 miles; Scenic &#8212; 25.0 miles; Total &#8212; 61.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->

<img src="images/sipsey-fork.jpg" alt="Sipsey Fork of the Black Warrior River" title="Sipsey Fork of the Black Warrior River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<!--<p><a href="http://www.fs.usda.gov/detail/umatilla/specialplaces/?cid=stelprdb5275389" alt="Wild &amp; Scenic Rivers &#8211; Umatilla National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Umatilla National Forest</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Dr. Sarah Praskievicz</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sipsey Fork of the Black Warrior River</h2>
<p>The Sipsey Fork of the Black warrior River (aka, Sipsey Fork of the West Fork) is situated in northwestern Alabama. The river exhibits the blending of striking landforms, diverse plant life and outstanding scenery. Steep canyon walls and sandstone bluffs, ranging in height from 30 to 100 feet, cascading waterfalls and water seepages combine with a variety of plant life native to the Coastal Plain and the Appalachian Mountains to make the river one of the most scenic areas in the region.</p>
<p>Waterflow on the Sipsey Fork of the West Fork River is highly dependent on rainfall occurrences. During summer drought seasons, the river will be characterized by a low stream flow of only a few inches deep mixed with occasional deep pools. During the fall, winter and spring months, river levels can rise from low flow to flood stage in a matter of hours due to rapid run-off from steep topography, slow permeable soils and highly intense rainstorms. After rainfall subsides, the river levels tend to recede rapidly.</p>
<p>The official designation in the Wild &amp; Rivers Act is the "Sipsey Fork of the West Fork." However, the name "West Fork" does not appear on any official U.S. Geological Survey maps.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>