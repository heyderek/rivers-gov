<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Metolius River, Oregon';

// Set the page keywords
$page_keywords = 'Metolius River, Deschutes National Forest, Lake Billy Chinook, Warm Springs Indian Reservation, Oregon';

// Set the page description
$page_description = 'Metolius River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('85');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Deschutes National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the Deschutes National Forest boundary to Lake Billy Chinook.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 17.1 miles; Recreational &#8212; 11.5 miles; Total &#8212; 28.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/metolius.jpg" alt="Metolius River" title="Metolius River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/metolius-plan.pdf" alt="Metolius River Management Plan (23.1 MB PDF)" target="_blank">Metolius River Management Plan (23.1 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Metolius River</h2>
<p>Nestled in a forested valley on the east side of the Cascade Mountain Range in central Oregon, the Metolius is one of the largest spring-fed rivers in the United States. Its clear, cold and constant waters support blue ribbon flyfishing and whitewater boating. The lower river, adjacent to the Warm Springs Indian Reservation, is managed as a primitive area with no motorized access.</p>
<p>The Metolius River gushes full-grown from the spectacular springs at its headwaters. Nationally renowned for its amazingly clear and cold water and fabulous fly fishing, the river basin also provides outstandingly remarkable geology, scenery, botanical, cultural and historic values. As it flows through memorable stands of old growth ponderosa pine it is as cherished by the thousands who visit it annually from across the nation as it is by the Native Americans who share in the traditions and administration of this rare resource.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>