<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Selawik River, Alaska';

// Set the page keywords
$page_keywords = 'Selawik River, Selawik National Wildlife Refuge, Kugarak River, Alaska';

// Set the page description
$page_description = 'Selawik River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('41');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Fish &amp; Wildlife Service, Selawik National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment from a fork of the headwaters in T12N, R10E, Kateel River meridian, to the confluence of the Kugarak River within the Selawik National Wildlife Refuge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 160.0 miles; Total &#8212; 160.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/selawik.jpg" alt="Selawik River" title="Selawick River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fws.gov/refuge/Selawik/about/wild_river.html" alt="Selawik Wild River (U.S. Fish &amp; Wildlife Service)" target="_blank">Selawik Wild River (U.S. Fish &amp; Wildlife Service)</a>
<br />
<a href="http://www.fws.gov/uploadedFiles/Region_7/NWRS/Zone_2/Selawik/PDF/floating_selawik.pdf" alt="Floating the Selawik River (U.S. Fish &amp; Wildlife Service)" target="_blank">Floating the Selawik River (U.S. Fish &amp; Wildlife Service)</a></p>
</p>

<div id="photo-credit">
<p>Photo Credit: USFWS</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Selawik River</h2>
<p>Lying entirely within the Selawik National Wildlife Refuge, this river is known for its fishing and variety of wildlife. The Selawik Valley is an important waterfowl nesting area and seasonally supports much of the Western Arctic Caribou Herd, which numbers over 500,000.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>