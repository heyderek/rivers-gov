<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Big Sur River, California';

// Set the page keywords
$page_keywords = 'Los Padres National Forest, Big Sur River, Ventana Wilderness, California';

// Set the page description
$page_description = 'Big Sur River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('144');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Los Padres National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>June 19, 1992. From the confluence of the South and North Forks downsteam to the boundary of the Ventana Wilderness. The South Fork and the North Fork from their headwaters to their confluence.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 19.5 miles; Total &#8212; 19.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->

<img src="images/big-sur.jpg" alt="Big Sur River" title="Big Sur River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">

<h3>RELATED LINKS</h3>
<p><a href="http://www.ventanawild.org/planning-trip/sykes-camp-hot-springs.html" target="_blank" alt="Ventana Wilderness Alliance Trip Planning" title="Ventana Wilderness Alliance Trip Planning" target="_blank">Ventana Wilderness Alliance Trip Planning</a></p>
<p><a href="../documents/plans/big-sur-plan.pdf" title="Big Sur River Management Plan" target="_blank">Big Sur River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Michael Carl</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Big Sur River</h2>
<p>The natural sulfur hot spring, the redwood riparian environment and the year-long water flows provide outstanding opportunities for primitive camping, swimming, fishing, picnicking and nature study. There is abundant, rapid-flowing water, with pools, springs and occasional waterfalls; a diverse tree canopy, including redwoods; and a combination of scenic features uncommon to central southern California.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>