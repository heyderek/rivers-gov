<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'AuSable River, Michigan';

// Set the page keywords
$page_keywords = 'Huron-Manistee National Forest, Au Sable River, Michigan';

// Set the page description
$page_description = 'AuSable River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('52');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Huron-Manistee National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 4, 1984. The main stem from the Mio Pond project boundary downstream to the Alcona Pond project boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 23.0 miles; Total &#8212; 23.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/ausable.jpg" alt="AuSable River" title="AuSable River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/hmnf/recarea/?recid=18540" target="_blank" alt="Huron-Manistee National Forest (U.S. Forest Service)" target="_blank">Huron-Manistee National Forest (U.S. Forest Service)</a></p>
<p><a href="http://www.michigandnr.com/publications/pdfs/wildlife/viewingguide/nlp/68AuSable/" target="_blank" alt="Michigan Department of Natural Resources" target="_blank">Michigan Department of Natural Resources</a></p>
<p><a href="../documents/plans/ausable-plan.pdf" title="Management Direction for the AuSable National Scenic River" target="_blank">Management Direction for the AuSable National Scenic River</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Au Sable River</h2>
<p>Located in the northern lower peninsula of Michigan, the Au Sable is known for its high water quality, scenery, recreational opportunities, coldwater fishery, and historic and cultural significance. It may just be the finest brown trout flyfishing east of the Rockies. If that were not enough reason to visit the river, the Au Sable is also one of the best canoeing rivers in the Midwest.</p>
<p>Camping is permitted only in designated sites and fees are required at some of the campgrounds. A number of canoe liveries rent watercraft. Fishing guides are also available in the area.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>