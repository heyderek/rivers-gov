<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Farmington River, Connecticut';

// Set the page keywords
$page_keywords = 'Farmington River, Connecticut, Atlantic salmon, Connecticut';

// Set the page description
$page_description = 'Farmington River, Connecticut';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('156');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Northeast Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>August 26, 1994. The segment of the West Branch and mainstem extending from immediately below the Goodwin Dam and Hydroelectric Project in Hartland to the downstream end of the New Hartford/Canton town line.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 14.0 miles; Total &#8212; 14.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/farmington.jpg" alt="Farmington River" title="Farmington River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/farmington_pwsr_sub.html" alt="Farmington River (National Park Service)" target="_blank">Farmington River (National Park Service)</a></p>
<p><a href="http://www.farmingtonriver.org" alt="Farmington River Coordinating Committee" target="_blank">Farmington River Coordinating Committee</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Farmington River</h2>
<p>In August 1994, Congress added 14 miles of the Farmington River's West Branch to the National Wild and Scenic Rivers System. This exciting milestone in the river's history recognizes the Farmington's beauty and character, and ensures that it will be enjoyed by generations to come.</p>
<p>Every year, thousands of people canoe, kayak and fish the waters of the Farmington, as well as visit the state parks, forests and historic mills that dot the river's edge. The river is an important habitat for wildlife, such as otters and bald eagles, and the Farmington River Valley is currently the only place in Connecticut with nesting bald eagles. In addition, the Atlantic salmon may return to the river after an absence of decades. Recreational value, rare wildlife, outstanding fisheries and a rich history are some of the outstanding features of the Farmington. It is managed through a partnership among local, state, and federal interests.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>