<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Malheur River (North Fork), Oregon';

// Set the page keywords
$page_keywords = 'Malheur National Forest, Malheur River, Oregon';

// Set the page description
$page_description = 'Malheur River (North Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('89');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Malheur National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters to the Malheur National Forest boundary.</p>
<br />
<h3>Classification/Mileage: </h3>
<p>Scenic &#8212; 25.5 miles; Total &#8212; 25.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/malhuer-nf.jpg" alt="North Fork Malhuer River" title="North Fork Malhuer River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/malheur/specialplaces/?cid=fsbdev3_033794" alt="Malheur River (U.S. Forest Service)" target="_blank">Malheur River (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Malheur River (North Fork)</h2>
<p>The river corridor is generally characterized by a broad valley carved by glacial activity in the upstream northern half and by a rugged and steep canyon ranging from about 250- to 750-feet deep in the south. The canyon geology is evident in the various rock outcrops, talus slopes and cliffs that contribute significantly to the scenic diversity of the landscape.</p>
<p>Evidence of human presence is moderate in the northern half and very limited in the southern half. Wildlife habitat is extremely diverse and of exceptionally high quality because of these undisturbed conditions. The river corridor also provides important connectivity between the Blue Mountains and Great Basin physiographic provinces. Camping, fishing, hiking and hunting are popular. The river is an important producer of native fish, and it has a significant recreational trout fishery.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>