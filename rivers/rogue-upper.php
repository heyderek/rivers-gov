<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = '(Upper) Rogue River, Oregon';

// Set the page keywords
$page_keywords = 'Rogue River, Rogue River National Forest, Crater Lake National Park, Oregon';

// Set the page description
$page_description = '(Upper) Rogue River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('104');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Rogue River National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the Crater Lake National Park boundary downstream to the Rogue River National Forest boundary at Prospect.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 6.1 miles; Scenic &#8212; 34.2 miles; Total &#8212; 40.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rogue-upper.jpg" alt="Upper Rogue River" title="Upper Rogue River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/rogue-siskiyou/recreation/?cid=stelprdb5305663" alt="Wild Rogue Wilderness (U.S. Forest Service)" target="_blank">Wild Rogue Wilderness (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/upper-rogue-plan-ea.pdf" title="Rogue (Upper) River Management Plan" target="_blank">Rogue (Upper) River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>(Upper) Rogue River</h2>
<p>From the Crater Lake National Park boundary downstream to Prospect, this section of the Rogue is paralleled by a major highway and tourist route from the park. Its diverse landscape includes pumice flats, deep gorges and chutes and unique ecological systems.</p>
<p>The Upper Rogue is fed by snowmelt and springs originating on the peaks of the Cascade Range, including the slopes of Mt. Mazama&#8212;the volcanic caldera that contains Crater Lake. Along the uppermost portion of the Upper Rogue, the river is narrow and shallow but very swift. Numerous moss-draped snags of lodgepole pines and other trees that have fallen across the river slow its pace only a little. The river has, for a two-mile stretch, carved more than 200' down into the Mazama pumice (which was deposited about 8,000 years ago during the explosion that created Crater Lake). This sheer, white-walled canyon, with the whitewater river glinting in the sunlight far below, is a favorite sight for travelers along Highway 230.</p>
<p>Downstream, the Upper Rogue passes through the narrow, turbulent Rogue Gorge of black lava and soon plunges entirely underground (into a lava tube) for about 250 feet at Natural Bridge. Interpretive trails at both of these places provide good views of the river, with signs that explain the natural history. Fed by tributaries, such as Union Creek and Flat Creek, the river subsequently flows through rugged Takelma Gorge, well out of sight of any road. The Upper Rogue features several waterfalls, although the more spectacular falls are located on tributaries National Creek and Muir Creek.</p>
<p>The entire length of the Upper Rogue Wild and Scenic River is closely followed by the Upper Rogue River Trail, designated a National Recreation Trail. Because of the various road crossings, much of this hiker-only trail (open to equestrians for short sections near Hamaker Campground) is easily traveled for short segments at a time.</p>
<p>In addition to its clear, clean water and scenic beauty, the Upper Rogue is notable for its history. Parallel to Highway 62 between Farewell Bend and Natural Bridge, it passes through part of the Union Creek Historic District, listed on the National Register of Historic Places for the rustic 1920s-30s buildings at Union Creek Resort and the nearby former ranger station. At Union Creek Wayside, a traveler's comfort station along Highway 62, an interpretive kiosk tells the history of Union Creek.</p>
<p>The Upper Rogue is considered extremely hazardous for whitewater boating due to its gorges, chutes and many obstructions, ranging from "sweeper" logs across the river to the underground section at Natural Bridge. For the entire wild section of the Upper Rogue and some portions of the scenic sections, we recommend no boating due to ever-changing instream hazards. Certain portions of the main scenic section of the Upper Rogue can be floated, with hard-shell kayaks being the most appropriate and preferred craft on this challenging section with always changing instream hazards. This is a river for experienced-to-expert boaters.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>