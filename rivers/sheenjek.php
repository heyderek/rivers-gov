<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sheenjek River, Alaska';

// Set the page keywords
$page_keywords = 'Sheenjek River, Porcupine River, Yukon River, Arctic National Wildlife Refuge, Alaska, Romanzof Mountains, Brooks Range';

// Set the page description
$page_description = 'Sheenjek River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('42');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Fish &amp; Wildlife Service, Arctic National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment within the Arctic National Wildlife Refuge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 160.0 miles; Total &#8212; 160.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/sheenjek.jpg" alt="Sheenjek River" title="Sheenjek River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.fws.gov/refuge/arctic/wildriversman.html" alt="Sheenjek River (U.S. Fish &amp; Wildlife Service)" target="_blank">Sheenjek River (U.S. Fish &amp; Wildlife Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Sharon Seim</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sheenjek River</h2>
<p>Originating from extensive glaciers in the Romanzof Mountains, this river travels south 200 miles to join the Porcupine River near its junction with the mighty Yukon. The river flows through a wide variety of arctic habitats and scenery. Portions of the Porcupine Caribou Herd occasionally winter in the Sheenjek Valley.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>