<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Hood River (East Fork), Oregon';

// Set the page keywords
$page_keywords = 'Hood River, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Hood River (East Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('177');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From Oregon State Highway 35 to the Mt. Hood National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 13.5 miles; Total &#8212; 13.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/hood-ef.jpg" alt="East Fork Hood River" title="East Fork Hood River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">

<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/pgdata/content/ak/en/prog/nlcs/gulkana_nwr.html" alt="Gulkana River (Bureau of Land Management)" target="_blank">Gulkana River (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div><!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Hood River (East Fork)</h2>
<p>The East Fork Hood River flows out of the Newton-Clark Glacier on the south face of Mt. Hood in the Cascade Range of Oregon. After flowing for about 2.5 miles toward the southeast, the river makes a sweeping turn to the north. Oregon State Highway 35 hugs the river after this turn. The 13.5 mile segment of the river from Highway 35 (just east of Sahalie Falls) to the Mt. Hood National Forest boundary is administered as a recreational river.</p>
<p>The river flows through a relatively broad valley bottom made up of glacial outwash before flowing into a narrower steep-sided canyon containing a number of cliffs. Due to the nature of the outwash, there are numerous springs and small tributaries that flow into the river.</p>
<p>Fishing is one of the primary recreational activities along the river, especially where access to the river is easier. Steelhead and coho salmon are the primary anadromous species present. Rainbow trout are stocked in the river to help meet the heavy fishing pressure. In winter, the upper portion of the river segment receives heavy Nordic ski use since the glacial outwash provides excellent terrain for ski trail development.</p>
<p>The area within the corridor, especially in the upper portion, provides very important habitat of high quality which meets the needs of big and small game. The area provides critical elk calving/deer fawning habitat and is part of a major migratory route for big game. In the upper corridor, the river and its immediate environment provides important riparian habitat in quantities greater than that usually found along other rivers throughout the region.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>