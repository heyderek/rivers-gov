<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Cottonwood Creek, California';

// Set the page keywords
$page_keywords = 'Cottonwood Creek, California, Inyo National Forest';

// Set the page description
$page_description = 'Cottonwood Creek, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('198');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Inyo National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From its headwaters at the spring in Section 27, Township 4 South, Range 34 East to the northern boundary of Section 5, Township 4 South, Range 34 East.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 17.4 miles; Recreational &#8212; 4.1 miles; Total &#8212; 21.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->

<img src="images/cottonwood-creek-ca.jpg" alt="Cottonwood Creek, California" title="Cottonwood Creek, California" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.arkansasstateparks.com/cossatotriver/" alt="Cossatot River (Arkansas State Parks)" target="_blank">Cossatot River (Arkansas State Parks)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Cottonwood Creek (California)</h2>

<p>Cottonwood Creek originates in ancient bristlecone forests and is the longest perennial stream east of the White Mountains. The creek flows eastward from the 14,000-foot crest of the White Mountains and steeply descends through groves of aspen, eventually flowing into a sagebrush desert. Numerous springs feed the creek as it meanders through large meadows in the upper reaches. Stands of aspen and bristlecone pine can be found in the higher elevations, while lower elevations are marked with stands of pinyon and juniper trees.</p>

<p>Cottonwood Creek is home to the Paiute cutthroat trout, one of the rarest trout in North America.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Recreational</em></strong></p>

<p>The creek is very remote, but offers numerous opportunities for primitive recreation, including hiking, backpacking, equestrian use, angling, hunting and bird watching. Anglers visiting the lower portion of the creek can find a quality brown trout fishery. There are no formal trails accessing the area; however, one can find user trails accessing portions of the creek.</p>

<p><strong><em>Riparian</em></strong></p>

<p>The Cottonwood Creek Wildlife Area includes approximately 6,300 acres of steep oak-grassland (upper unit) and steep hilly grassland (lower unit).</p>

<p><strong><em>Scenic</em></strong></p>

<p>The lush riparian plant community along the bottom of Cottonwood Creek contrasts dramatically with the surrounding stark and primitive White Mountain Wilderness study area located to the north and south.</p>

<p><strong><em>Wildlife &amp; Plants</em></strong></p>

<p>The stream is flanked by a rich forest of aspen, willow and cottonwoods and is home to an 'Unusual Plant Assemblage' in the California Desert Conservation Area Plan. Wildlife supported by this plant community include a number of special status and/or sensitive bird species, such as yellow warbler, yellow-breasted chat, prairie falcon, sharp-shinned and Cooper's hawk, and the basin is potentially suitable habitat for the southwestern willow flycatcher, a federally endangered species. This segment of Cottonwood Creek supports over 70 species of birds. The lower segment of Cottonwood Creek is also an important habitat for the spotted bat, which is a federal and California state special concern species. Paiute cutthroat trout, a federally threatened species, inhabit the north fork of Cottonwood Creek in the Inyo National Forest.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>