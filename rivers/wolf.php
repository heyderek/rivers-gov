<?php

// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wolf River, Wisconsin';

// Set the page keywords
$page_keywords = 'Wolf River, Menominee Indian Tribe, Wisconsin';

// Set the page description
$page_description = 'Wolf River, Wisconsin';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('8');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Midwest Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968. From the Langlade-Menominee County line downstream to Keshena Falls.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 24.0 miles; Total &#8212; 24.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wolf.jpg" alt="Wolf River" title="Wolf River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/wolf-plan.pdf" alt="Wolf River Management Plan" target="_blank">Wolf River Management Plan (1.2MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Susan Hollingsworth Elliott</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wolf River</h2>
<p>Noted as one of the most scenic and rugged rivers in the Midwest, the Wolf flows through the Menominee Reservation. The river is not developed for public use.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>