<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Whitefish River, Michigan';

// Set the page keywords
$page_keywords = 'Hiawatha National Forest, Whitefish River, Michigan';

// Set the page description
$page_description = 'Whitefish River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('131');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Hiawatha National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. The main stem from its confluence with the East and West Branches to Lake Michigan. The East Branch from the crossing of County Road 003 to its confluence with the West Branch. The West Branch from County Road 444 to its confluence with the East Branch.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 31.5 miles; Recreational &#8212; 2.1 miles; Total &#8212; 33.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/whitefish.jpg" alt="Whitefish River" title="Whitefish River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/westfield_pwsr_sub.html" alt="Westfield River (National Park Service)" target="_blank">Westfield River (National Park Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Whitefish River</h2>
<p>The Whitefish River is cold, swift and deep in the early spring, challenging canoeists and kayakers and providing good steelhead fishing. During the summer, much of the river becomes too shallow for canoeing, but offers fishing for brook trout in the upper reaches and a variety of warm-water species downstream from the confluence of the East and West Branches. The river is located on the Hiawatha National Forest in Michigan's Upper Peninsula.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>