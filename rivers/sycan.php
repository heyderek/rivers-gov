<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sycan River, Oregon';

// Set the page keywords
$page_keywords = 'Sycan River, Fremont National Forest, Winema National Forest, Oregon';

// Set the page description
$page_description = 'Sycan River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('103');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>
			
<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Fremont National Forest<br />
U.S. Forest Service, Winema National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the northeast 1/4 of Section 5, T34S, R17E to Coyote Bucket at the Fremont National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 50.4 miles; Recreational &#8212; 8.6 miles; Total &#8212; 59.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/sycan.jpg" alt="Sycan River" title="Sycan River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/sycan-plan.pdf" alt="Sycan River Management Plan" target="_blank">Sycan River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sycan River</h2>
<p>The unique features of this southern Oregon stream are its distinctive scenery that varies from a steep canyon to broad meadows. The corridor contains a diversity of landform, rock form and vegetation. The vegetation is primarily coniferous with scattered old-growth ponderosa pine and lodgepole pine flats intermingled with water-related riparian vegetation, such as willows and other deciduous shrubs. Expanses of sage and bitterbrush in the dryer areas lend diversity. Of particular significance is Sycan Marsh which includes several rare plant communities and provides wetland habitat for Sandhill cranes, bald eagles and waterfowl.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>