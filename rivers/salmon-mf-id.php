<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Salmon River (Middle Fork), Idaho';

// Set the page keywords
$page_keywords = 'Salmon River, River of No Return, Frank Church, Frank Church Wilderness, Salmon National Forest, Challis National Forest, Idaho';

// Set the page description
$page_description = 'Salmon River (Middle Fork), Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('7');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Salmon-Challis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968. From its origin to its confluence with the Main Salmon River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 103.0 miles; Scenic &#8212; 1.0 mile; Total &#8212; 104.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/salmon-mf.jpg" alt="Middle Fork of the Salmon River" title="Middle Fork of the Salmon River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/scnf/recarea/?recid=77793" alt="Middle Fork, Salmon River (U.S. Forest Service)" target="_blank">Middle Fork, Salmon River (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/attmain/scnf/specialplaces" alt="Salmon River (U.S. Forest Service)" target="_blank">Salmon River (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/scnf/specialplaces/?cid=stelprdb5360033" alt="Frank Church River of No Return Wilderness (U.S. Forest Service)" target="_blank">Frank Church River of No Return Wilderness (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/scnf/passes-permits/recreation/?cid=fsbdev3_029568/" alt="Middle Fork Salmon River Lottery" target="_blank">Middle Fork Salmon River Lottery</a></p>
<p><a href="../documents/plans/salmon-middle-salmon-plan.pdf" title="Salmon &amp; Middle Fork Salmon River &amp; Wilderness Management Plan" target="_blank">Salmon &amp; Middle Fork Salmon River &amp; Wilderness Management Plan</a></p>
<p>

<div id="photo-credit">
<p>Photo Credit: Audra Serrian</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Salmon River (Middle Fork) (Idaho)</h2>
<p>One of the original eight rivers in the nation designated as Wild and Scenic on October 2, 1968, the Middle Fork of the Salmon River originates 20 miles northwest of Stanley, Idaho, with the merging of Bear Valley and Marsh Creeks. The entire river, to its confluence with the Salmon River, is designated and is classified as wild with the exception of a one-mile segment near the Dagger Falls-Boundary Creek Road, which is classified as scenic. All except this short scenic segment is also within the Frank Church River of No Return Wilderness.</p>
<p>The Middle Fork is one of the last free-flowing tributaries of the Salmon River system. Because of its remote location, man's presence in the area was somewhat limited, leaving it in the condition we see today. Only a few trails, landing strips, private ranches and U.S. Forest Service stations are evidence of man's intrusion.</p>
<p>While man's impact on the area has been relatively light, it has been diverse. The Native Americans who occupied the Middle Fork drainage were known as The Sheepeaters. They gained their name from the bighorn sheep that were prevalent in the area and constituted much of their diet. White trappers, miners and settlers began coming into the area in the 1850's. No road access was ever built, and all supplies came in by horseback. Floating the river began in the 20's with a few adventurous souls who wanted to see beyond the rock wall canyon at Big Creek where the trail ended.</p>
<p>Wildlife along the Middle Fork river is abundant due to the designation and isolation of the Frank Church River of No Return Wilderness. The river moves through a variety of climates and land types, from alpine forest to high mountain desert to sheer rock-walled canyon, creating a wide variety of habitats. Deer, elk, bighorn sheep, mountain goat, bear and cougar are just a few of the animals to make the area their home. The Middle Fork drainage was one of the sites for the wolf reintroduction program. The fishery is one of the best catch and release fly fisheries in the nation.</p>
<p>The Frank Church River of No Return Wilderness occupies part of an extensive geological formation known as the Idaho Batholith. This formation, mainly granite, has been severely eroded, exposing underlying rock formations laid down during the Precambrian, Permian, Triassic and Cretaceous Periods.</p>
<p>The Middle Fork is internationally recognized for its white water/wilderness float trip. Known for its rugged scenic beauty, quiet isolation, crystal clear water and the challenge of its whitewater, it is floated by more than 10,000 people each summer. Class III - IV+ rapids offer boating excitement for both families and hard-core adventure types. Hiking from the river campsites offers a taste of the wilderness experience, and you may be lucky enough to catch a glimpse of the past and present inhabitants.</p>
<p>The floating activity on the Middle Fork is managed and regulated to preserve the pristine character of the area and experience. Permits to float the river are required all year. The highest use is from May 28 through September 3. This is known as the control season, and permits for this time are allocated through a computerized lottery. Permits for the pre- and/or post-season launch dates that occur outside the lottery control season are also available for reservation beginning on October 1. Campsites are assigned to boaters all year.</p>
<p>Fire pans, portable toilets, ash containers, shovels, buckets and strainers are required equipment to float the river during any season. Leave No Trace, pack-in, pack-out is the camping ethic mandatory to all camping in the corridor and throughout the entire wilderness. During high use periods, campsites are assigned to boaters at the launch sites.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>