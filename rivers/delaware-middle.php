<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Delaware River (Middle), New Jersey, Pennsylvania';

// Set the page keywords
$page_keywords = 'Delaware River, Delaware Water Gap National Recreation Area, New Jersey, Pennsylvania';

// Set the page description
$page_description = 'Delaware River (Middle), New Jersey, Pennsylvania';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('20');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Delaware Water Gap National Recreation Area</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978. The segment from the point where the river crosses the northern boundary of the Delaware Water Gap National Recreation Area to the point where the river crosses the southern boundary.</p>
<br />
<h3>Classification/Mileage: </h3>
<p>Scenic &#8212; 35.0 miles; Recreational &#8212; 5.0 miles; Total &#8212; 40.0 miles.</p>
</div>

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/middle-delaware.jpg" alt="Middle Delaware River" title="Middle Delaware River" width="265px" height="204px" title="Middle Delaware River" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/dewa/" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Delaware Water Gap National Recreation Area (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Jim Davis</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Delaware River (Middle)</h2>
<p>This segment of the Delaware flows through the Delaware Water Gap National Recreation Area and cuts an "S" curve through Kittatinny Ridge. This beautiful landscape provides great recreational opportunities in addition to sightseeing and geological study value.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>