<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Red River, Kentucky';

// Set the page keywords
$page_keywords = 'Red River, Daniel Boone National Forest, Red River Gorge Geological Area, Clifty Wilderness, Kentucky';

// Set the page description
$page_description = 'Red River, Kentucky';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('154');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Daniel Boone National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1993. From the Highway 746 Bridge to the confluence with the School House Branch.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 9.1 miles; Recreational &#8212; 10.3 miles; Total &#8212; 19.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/red-ky.jpg" alt="Red River" title="Red River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/dbnf/recreation/wateractivities/recarea/?recid=39548&actid=79" title="Red River" target="new">Red River (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/dbnf/specialplaces/?cid=stelprdb5345319" alt="Red River Gorge Geological Area (U.S. Forest Service)" target="_blank">Red River Gorge Geological Area (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Red River</h2>
<p>The Red River flows through the Red River Gorge geological area and bisects the Clifty Wilderness. Sandstone cliffs, rock shelters, natural stone arches and boulders provide excellent views of unique geological features nestled among the mountain laurel, rhododendron and hemlocks. The river offers scenic views as well as protection for numerous prehistoric and historic sites featuring unique ethnobotanic remains and other features that led to the designation of this area as a Natural Historic Landmark. The surrounding clifflines are home to the white-haired goldenrod, a plant only found in the Red River Gorge, as well as numerous other species of wildlife. More than 70 species of fish and 16 mussels thrive in the free flowing waters of the Red River. The scenic beauty and geological formations throughout the river corridor attract thousands of canoeists, kayakers, hikers, rock climbers and backcountry enthusiasts each year.</p>
<p>The U.S. Forest Service maintains one access point at Copperas Creek Canoe Launch on Highway 715. The river is too low for canoeing or kayaking most of the year. However, Class I, II and III rapids may be experienced after late fall and spring floods or heavy rains.</p>
<p>A camping permit is required for dispersed camping in the Red River Gorge Geological Area. Permits can be purchased at local vendors or Gladie Learning Center.</p>
<p>The Red River corridor is black bear habitat. Proper food storage is required. The forest website and area bulletin boards provide additional information regarding food storage requirements and other rules.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>