<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Eel River, California';

// Set the page keywords
$page_keywords = 'Hoopa Valley Indian Reservation, Eel River, Yolla Bolly Wilderness, Van Duzen River, Old Gilman Ranch, California';

// Set the page description
$page_description = 'Eel River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('SD8');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Arcata Field Office<br />
California Resources Agency<br />
Round Valley Indian Reservation<br />
U.S. Forest Service, Six Rivers National Forest<br />
U.S. Forest Service, Mendocino National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>January 19, 1981. From the mouth of the river to 100 yards below Van Ardsdale Dam. The Middle Fork from its confluence with the main stem to the southern boundary of the Yolla Bolly Wilderness Area. The South Fork from its confluence with the main stem to the Section Four Creek confluence. The North Fork from its confluence with the main stem to Old Gilman Ranch.  The Van Duzen River from the confluence with the Eel River to Dinsmure Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 97.0 miles; Scenic &#8212; 28.0 miles; Recreational &#8212; 273.0; Total &#8212; 398.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/eel.jpg" alt="Eel River" title="Eel River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Eel River Eligibility Report" target="_blank">Eel River Eligibility Report</a><br />
<a href="../documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Eel River Environmental Impact Statement" target="_blank">Eel River Environmental Impact Statement</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Eel River</h2>

<p>The Eel River represents California's third largest watershed. The mainstem flows more than two hundred air miles and travels over 800 river miles from the headwaters above Lake Pillsbury in Lake County to the ocean. The Eel River has received both state (1972) and federal (1981) wild and scenic river designation, which protects the river from dams and ensure that environmental concerns rank equally with development and industry.</p>

<p>The three forks of the Eel illustrate several river types, originating in high mountain pine forests; flowing through steep canyons and coastal redwood forests; and emptying into the Pacific in a gently sloping valley with virgin redwood stands. The North Fork flows 35 miles, completely in Trinity County. The Middle Fork, the Eel's largest tributary, travels a total of 70 miles before joining the mainstem Eel. The South Fork begins in Mendocino County and travels through ancient redwood forests to join the mainstem.</p>

<p>A fourth tributary, the Van Duzen River enters the mainstem of the Eel once it has reached the coastal plain.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The primary fish of interest for the Eel include steelhead, chinook, coho and sea-run cutthroat trout. In normal years, chinook begin arriving in August and remain until rains allow them upstream. The run continues through December, with the peak in late October.</p>

<p>The Eel River water, fish and ecosystem are have faced development challenges, and sections of the river are closed to fishing to protect the juvenile steelhead.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Dos Rios, located at the confluence of the Middle Fork of the Eel River and the mainstem, is the put-in for a popular four-day trip through the Eel River Canyon to Alderpoint. A number of trails access the river, and the highest public use is by summer swimmers downstream near the Eel River Work Center and Eel River Campground.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>