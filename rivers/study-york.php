<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'York River, Maine';

// Set the page keywords
$page_keywords = 'York River, Maine, National Park Service';

// Set the page description
$page_description = 'York River, Maine';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('301');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<!--<?php
// includes ESRI
include ('../iframe.php');
?>-->

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<center><img src="images/york.jpg" width="509" height="381" alt="York River" title="York River" /></center>
<br />
<h3>Study Agency:</h3>
<p>National Park Service</p>
<br />
<h3>Study Segments:</h3>
<p>December 19, 2014 (Public Law 113-291). From the headwaters of the York River at York Pond to the mouth of the river at York Harbor and any associated tributaries.</p>
<br />
<h3>Mileage:</h3>
<p>11.3 miles plus tributaries.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/york2.jpg" alt="York River" title="York River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../../documents/studies/york-study.pdf" title="York River Reconnaissance Survey" target="_blank" alt="York River Reconnaissance Survey (National Park Service)">York River Reconnaissance Survey (National Park Service)</a></p>
<p><a href="http://www.yorkrivermaine.org" title="York River Study Web Site" target="_blank" alt="York River Study Web Site">York River Study Web Site</a></p>

<div id="photo-credit">
<p>Photo Credit: Top - Joyce Kennedy Raymes; Bottom - Karen Young</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>York River</h2>
<p>The York River watershed is located in southern Maine within the towns of Kittery, Eliot, South Berwick and York. The majority (72%) of the watershed area is located within the town of York. The watershed covers 32 square miles and includes the York River mainstem and numerous wetlands, ponds and tributaries, as well as drinking water reservoirs and an extensive salt marsh estuary. There are a total of 109 miles of streams and rivers. The major York River tributaries are Cider Hill Creek, Cutts Ridge Brook, Rogers Brook and Smelt Brook. This area is primarily comprised of large, unfragmented forested areas and agricultural lands, along with rural areas and some suburban residential development. The many important habitat areas support rare and endangered plant and animal species.</p>

<p>The York River is a good candidate for a wild and scenic river designation based on the preliminary evidence of free-flowing river conditions and the presence of multiple natural, cultural and recreational resources with the potential to meet the "Outstandingly Remarkable Value" threshold as defined by the Wild and Scenic Rivers Act. There is demonstrated local and regional interest and support for a study and existing river/watershed protection elements that would support the NPS framework for a Partnership Wild and Scenic River designation. In addition, local stakeholders have indicated an initial level of interest in developing the river management plan that would be developed as a part of the study process, which is required as a part of the designation.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>