<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wickahoney Creek, Idaho';

// Set the page keywords
$page_keywords = 'Wickahoney Creek, Idaho';

// Set the page description
$page_description = 'Wickahoney Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('195');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Wickahoney Creek from its confluence with Big Jacks Creek to the upstream boundary of the Big Jacks Creek Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 1.5 miles; Total &#8212; 1.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wickahoney-creek.jpg" alt="Wickahoney Creek" title="Wickahoney Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14984/2" alt="Wickahoney Creek (Bureau of Land Management)" target="_blank">Wickahoney Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Evan Worthington</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wickahoney Creek</h2>

<p>Wickahoney Creek is a tributary of Big Jacks Creek, and it is designated wild for 1.5 miles upstream of their confluence. It is characterized by dense riparian vegetation and tight meanders. Redband trout may be found in the creek, and mule deer are common in the area. Access to the area is difficult.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Wickahoney Creek is outstandingly remarkable from a fisheries population and habitat standpoint. When considered in combination with Little Jack's, Cottonwood and Duncan Creeks, the streams are among the 17% of desert streams in the Northern Basin and Range identified as aquatic-habitat strongholds for redband trout, a BLM sensitive species and a state of Idaho species of special concern.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Canyons in this region are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Intertwined with the vertical features are steep diagonal lines that frame triangular forms associated with talus slopes. Slopes feature a mosaic of medium-textured, yellow and subdued green sagebrush-bunchgrass communities and/or dark green juniper, as well as either medium-textured, reddish rhyolite rubble fields or coarse-textured, blackish basalt rubble fields.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wickahoney Creek is included in the area of Preliminary Priority Habitat for greater sage-grouse. Idaho BLM sensitive species also include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>