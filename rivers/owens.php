<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Owens River Headwaters, California';

// Set the page keywords
$page_keywords = 'Owens River, California, Inyo National Forest';

// Set the page description
$page_description = 'Owens River Headwaters, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('197');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Inyo National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Deadman Creek fromfrom the two-forked source east of San Joaquin Peak to 100 feet upstream of Big Springs. The upper Owens River from 100 feet upstream of Big Springs to the private property boundary in Section 19, Township 2 South, Range 28 East. Glass Creek from its two-forked source to its confluence with Deadman Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 6.3 miles; Scenic &#8212; 6.6 miles; Recreational &#8212; 6.2 miles; Total &#8212; 19.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/owens.jpg" alt="Owens River" title="Owens River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/inyo/recreation/recarea/?recid=21881" alt="Owens River Hedwaters Wilderness (U.S. Forest Service)" target="_blank">Owens River Hedwaters Wilderness (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Michael Carl</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Owens River Headwaters</h2>
<p>The Owens River and its tributaries, Glass and Deadman Creeks are headwaters for the famed Owens River System, a renowned fishery which calls anglers and other recreationists to the rugged landscapes of the Eastern Sierra. With its headwaters beginning at the 11,600-foot summit of San Joaquin Mountain, the eastward draining waters contain over 100 seeps and springs that sustain some of the most abundant riparian habitat in the Eastern Sierra.</p>
<p>The Owens River Headwaters are an area of forested mountains and alpine meadows on the east side of the crest of the Sierra Nevada Mountains in Mono County, California. This area contains exceptionally diverse landforms and habitat including the expansive subalpine Glass Creek Meadow, the largest sub alpine meadow east of the crest, which provides habitat for the Yosemite Toad, a candidate endangered species, and is also home to the highest diversity of butterflies in the Eastern Sierra. These waterways flow through the region's largest old growth red fir forest, which provides for highly diverse ecological conditions that support many other plants and animals in the area.</p>
<p>The relatively low elevation ridge along the Sierra Crest allows moisture from Pacific storms to funnel over this area. The abundant moisture has created an island of wet meadows among the forested ridges in the dry eastern side of the Sierra Nevada. It is these clear and cold springs which sustain this celebrated river system.</p>
<p>Diversity can also be found in the many activities and pursuits that one can find in the area. Popular activities include hiking, camping, angling, birding, and many other forms of nature study. Nordic skiing, snow shoeing, and snow play are also popular winter time activities. The relative ease of access make this river ideal for both day and overnight trips for the travelers who are exploring the Inyo National Forest along Highway 395.</p>
<p>To get to the Glass Creek Trailhead, drive south on Highway 395 from Lee Vining approximately 15.7 miles, turn right (west) on Obsidian Dome/Glass Flow Road. From the south, drive approximately 50.5 miles north on Highway 395 from Bishop, turn left on Obsidian Dome Road. Drive west and south on Obsidian Dome/Glass Flow Road (dirt) 2.7 miles to a 3-way road junction, veer right and proceed 0.1 mile to the parking area and trailhead on the north side of Glass Creek. Hike two miles west and climb 700 feet in elevation on the trail along Glass Creek, past Glass Creek Falls to Glass Creek Meadow.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>