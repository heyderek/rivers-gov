<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Whychus Creek, Oregon';

// Set the page keywords
$page_keywords = 'Whychus Creek, Squaw Creek, Deschutes National Forest, Three Sisters Wilderness, Oregon';

// Set the page description
$page_description = 'Whychus Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('102');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Deschutes National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its source to the gauging station 800 feet upstream from the intake of McAllister Ditch.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 6.6 miles; Scenic &#8212; 8.8 miles; Total &#8212; 15.4 miles.</p>
<br />
<h3><strong><em>Please note that this creek is still listed in the Wild &amp; Scenic Rivers Act as "Squaw Creek," the name by which it was originally known and designated.</em></h3>
<p><p>On December 8, 2005, the U.S. Board on Geographic Names approved the name change of Squaw Creek and other public place names that used the term “squaw.” The name Whychus, meaning “the place we cross the water,” was chosen for the creek based on its strong historical and legal usage. Making this change officially in the Wild and Scenic Rivers Act requires a technical correction, which is underway.</strong></p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/whychus-creek.jpg" alt="Whychus Creek" title="Whychus Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/whychus-creek-plan.pdf" alt="Whychus Creek Management Plan (1.4 MB PDF)" target="_blank">Whychus Creek Management Plan (1.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Whychus Creek</h2>
<p>The name Whychus, meaning "the place we cross the water," was chosen for the creek based on its strong historical and legal usage.</p>
<p>The three forks of Whychus Creek begin in the Three Sisters Wilderness on the east slope of the Cascade Range in Central Oregon. The river’s outstandingly remarkable values include geology, hydrology, fish, scenery, prehistory and traditional cultural use.</p>
<p>Glacial and volcanic events have created complex and diverse landscapes, including steep and narrow canyons, deep bedrock canyons, numerous waterfalls and a variety of channel shapes and gradients. The fish populations are regionally significant due to the presence of a native strain of Interior Columbia Basin redband trout with little genetic influence from hatchery fish and the imminent reintroduction of federally listed “threatened” steelhead.</p>
<p>The wild, unmodified scenery of the corridor is unique in the region, with distinctive visual elements provided by the change in elevation and varied geologic features. The headwaters of Whychus Creek, the glaciers on the Three Sisters Mountains, are a symbol of Central Oregon and widely photographed.</p>
<p>Whychus Creek has a long history of use by Native Americans and provided a travel corridor to and from obsidian sources in the high Cascades. Traditional use by Native Americans is well-documented.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>