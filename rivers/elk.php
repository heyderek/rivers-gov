<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Elk River, Oregon';

// Set the page keywords
$page_keywords = 'Siskiyou National Forest, Elk River, Oregon';

// Set the page description
$page_description = 'Elk River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('76');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Rogue River-Siskiyou National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988, and March 30, 2009. The main stem from the confluence of the North and South Forks of the Elk River to Anvil Creek. The North Fork from its source in Section 21, Township 33 South, Range 12 West, to its confluence with the South Fork. The South Fork Elk from its source in the southeast quarter of Section 32, Township 33 South, Range 12 West, to its confluence with the North Fork.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>October 28, 1988: Wild &#8212; 2.0 miles; Recreational &#8212; 17.0 miles; Total &#8212; 19.0 miles. March 30, 2009: Wild &#8212; 7.7 miles; Scenic &#8212; 1.5 miles; Total &#8212; 9.2 miles. Total &#8212; 28.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/elk.jpg" alt="Elk River" title="Elk River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/rogue-siskiyou/recreation/scenicdrivinginfo/recarea/?recid=74291&actid=105" alt="Elk River (U.S. Forest Service)" target="_blank">Elk River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/elk-plan.pdf" title="Elk River Management Plan" target="_blank">Elk River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Matt Lindsay, American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Elk River</h2>
<p>The Elk River is in Curry County, approximately three miles north of Port Orford, along the beautiful southern Oregon Coast. Seventeen miles of the mainstem and a two-mile segment of the North Fork Elk were designated in 1988; the remainder of the North Fork and the South Fork Elk were added in 2009.</p>
<p>The outstandingly remarkable values of the Elk River are its fisheries and water quality. The river contains very important populations of native Chinook salmon, sea-run cutthroat trout, winter steelhead and some coho salmon. In addition to its outstanding species diversity, the Elk River provides excellent spawning and rearing habitat and is also highly valued for its productive commercial and recreational fisheries. The river and watershed are used for scientific study.</p>
<p>The striking blue-green water color and crystalline water quality are exceptional. The river clears more far more quickly that other streams in region attracting fishermen and other recreationists. Its low turbidity, cool temperatures and lack of pollutants contribute to the excellent habitat and high fisheries value.</p>
<p>The combination of water, color, exposed rock surfaces, dynamic flow and relatively undisturbed environment creates an interesting and beautiful landscape throughout the year. The scenic quality of the river corridor draws on these features to create a significant value within the federally protected corridor. The lower section of the river flows through a steep canyon with exposed rock surfaces, forming an inner-gorge environment. Upstream, the gorge widens slightly, but the corridor remains very steep.</p>
<p>Few northwest coastal rivers exhibit the quantity of old-growth found in the Elk River basin. Wildlife species utilizing this habitat include marbled murrelet, northern spotted owl and bald eagle.</p>
<p>There are three developed campgrounds within the river corridor; two sites are located along the river, Butler Bar and Sunshine Bar, and the third campground is at Laird Lake. The river itself provides many pools and swimming areas, and there are several dispersed sites along the river corridor. The Elk River attracts fishermen from the entire nation. However, the vast majority of sport fishing occurs outside the designated segment, with only a short segment of the designated river open for salmon and steelhead fishing.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>