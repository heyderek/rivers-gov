<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Alatna, Alaska';

// Set the page keywords
$page_keywords = 'Gates of the Arctic National Park, National Park Service, Alatna River, Alaska';

// Set the page description
$page_description = 'Alatna, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('26');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>


<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Gates of the Arctic National Park and Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The main stem within Gates of the Arctic National Park.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 83.0 miles; Total &#8212; 83.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/alatna.jpg" alt="Alatna River" title="Alatna River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/gaar/" alt="Gates of the Arctic National Park" target="_blank">Gates of the Arctic National Park</a></p>

<div id="photo-credit">
<p>Photo Credit: Brett Cole, American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Alatna River</h2>
<p>The stream lies partially within the Gates of the Arctic National Park and Preserve in the Central Brooks Range of Alaska. Wildlife, scenery and interesting geologic features abound in the river corridor.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>