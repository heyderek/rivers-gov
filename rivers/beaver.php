<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Beaver Creek, Alaska';

// Set the page keywords
$page_keywords = 'Yukon Flats National Wildlife Refuge, Steese Mountain, White Mountain, Beaver Creek, Alaska';

// Set the page description
$page_description = 'Beaver Creek, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('45');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Fairbanks District Office<br />
U.S. Fish &amp; Wildlife Service, Yukon Flats National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment of the main stem from the vicinity of the confluence of the Bear and Champion Creek downstream to its exit from the NE corner of T12N, R6E, Fairbanks meridian within the White Mountains National Recreation Area and the Yukon Flats National Wildlife Refuge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 127.0 miles; Total &#8212; 127.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/beaver-creek.jpg" alt="Beaver Creek" title="Beaver Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/ak/st/en/prog/nlcs/beavercrk_nwsr.html" target="_blank" alt="Beaver Creek (Bureau of Land Management)">Beaver Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Bob Wick, BLM</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Beaver Creek</h2>

<p>Beaver Creek has its headwaters in the White Mountains, approximately 50 miles north of Fairbanks, Alaska. The river flows west past the jagged limestone ridges of the White Mountains before flowing to the north and east, where it enters the Yukon Flats and joins the Yukon River. The first 127 miles of Beaver Creek, most of it within the White Mountains National Recreation Area, were designated by the Alaska National Interest Lands Conservation Act in 1980. The last 16 miles of river that are designated 'wild' lie within the Yukon Flats National Wildlife Refuge.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fish &amp; Wildlife</em></strong></p>

<p>Beaver Creek's fishery consists primarily of Arctic grayling. Northern pike, sheefish and whitefish are also present in the lower reaches of the river. The jagged peaks of the White Mountains' high ridges are home to Dall sheep and endangered peregrine falcons. Wildlife in the valley include moose, black and grizzly bears, wolves and caribou. Common furbearers include lynxes, beavers, martens, wolverines, muskrats and foxes. Eagles, peregrine falcons and owls hunt the river corridor. Migratory waterfowl, such as merganser, shovelhead, goldeneye and harlequin ducks, spend the summers along Beaver Creek. The Steese and White Mountains are important calving, summering and wintering areas for caribou, although numbers have declined&#8212;the Steese-Fortymile herd may have numbered as many as a million animals during the early 1900's.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Beaver Creek flows out of the Tanana-Yukon Uplands, a structurally complex region of igneous, metamorphic and sedimentary rocks. Around the White Mountains, the river flows through a tectonically active fault zone. The first major fault occurs at the Big Bend, where the river crosses a band of Tolovana Limestone, the backbone of the White Mountains. The creek then runs parallel to a series of active thrust faults and touches a zone of Lower Paleozoic mafic and ultramafic rocks (including serpentine) along the river's northern margin before continuing into the sedimentary bedrock of the Yukon Flats.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Beaver Creek has long been a popular destination for river adventurers. The river's clear water, modest rapids and unparalleled scenery make for a relaxing trip. Floating Beaver Creek can take from seven days to three weeks to complete. For shorter trips, arrangements can be made with an air taxi for a gravel bar pick-up near Victoria Creek. Others continue for several more weeks onto the Yukon River and take out at the bridge on the Dalton Highway. This 360-mile trip has been called the longest road-to-road float in North America.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The river flows through the heart of the White Mountains, whose massive, white limestone formations up to several thousand feet thick offer stunning scenery and peaceful solitude. Wind, rain and freezing temperatures have weathered away the surrounding rock to expose the jagged cliffs and peaks. In contrast, the valley bottoms usually consist of permafrost (permanently frozen soil) about a foot beneath the surface. This results in forests of short, stunted black spruce, deep sedge tussocks and thick stands of willows. Creekside gravel soils support tall white spruce trees and dense brush.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>