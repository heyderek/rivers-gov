<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wenaha River, Oregon';

// Set the page keywords
$page_keywords = 'Wenaha River, Umatilla National Forest, Grande Ronde River, Oregon';

// Set the page description
$page_description = 'Wenaha River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('105');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Umatilla National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the confluence of the North and South Forks to its confluence with the Grande Ronde River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 18.7 miles; Scenic &#8212; 2.7 miles; Recreational &#8212; 0.2 miles; Total &#8212; 21.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wenaha.jpg" alt="Wenaha River" title="Wenaha River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/umatilla/specialplaces/?cid=stelprdb5275389" alt="Wild &amp; Scenic Rivers &#8211; Umatilla National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Umatilla National Forest</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wenaha River</h2>
<p>The Wenaha River is located in northeast Oregon on the Umatilla National Forest, starting within the Wenaha-Tucannon Wilderness. The Wenaha drainage is located on the eastern slope of the northern Blue Mountains of Oregon. The Wenaha flows between rugged basalt outcrops rising 1,600 feet to the plateau above. The river is noted for its remoteness and largely primitive setting. Exceptional opportunities exist for big game hunting, fishing camping and hiking. Wild runs of Chinook, steelhead and bull trout are found here; it is one of the best rainbow trout streams in northeastern Oregon.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>