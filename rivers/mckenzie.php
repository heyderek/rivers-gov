<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'McKenzie River, Oregon';

// Set the page keywords
$page_keywords = 'McKenzie River, Willamette National Forest, Oregon';

// Set the page description
$page_description = 'McKenzie River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('84');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Willamette National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Clear Creek to Scott Creek, not including Carmen and Trail Bridge Reservoir Dams.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 12.7 miles; Total &#8212; 12.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/mckenzie.jpg" alt="McKenzie River" title="McKenzie River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/willamette/recreation/recarea/?recid=4402" alt="McKenzie River (U.S. Forest Service)" target="_blank">McKenzie River (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>McKenzie River</h2>
<p>The McKenzie Wild and Scenic River was designated in 1988 for its outstandingly remarkable values of fish, scenic quality, recreation, hydrology/geology and water quality. The "Blue McKenzie" is well-known for its exceptional water quality, resulting from low turbidity, absence of organic material and the high water quality of its tributaries. The excellent quality of its fish habitat supports a variety of fish species, including three native wild trout&#8212;rainbow, bull and cutthroat trout&#8212;as well as wild spring Chinook. Recent vulcanism, including three distinctive lava flows, has shaped the river into pools, dramatic waterfalls and cascading whitewater. These features provide for exceptional whitewater boating, hiking and fishing.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>