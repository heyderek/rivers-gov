<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Yellowstone River (Clarks Fork), Wyoming';

// Set the page keywords
$page_keywords = 'Shoshone National Forest, Clarks Fork, Yellowstone River, Yellowstone National Park, Wyoming';

// Set the page description
$page_description = 'Yellowstone River (Clarks Fork), Wyoming';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('116');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Shoshone National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 28, 1990. From Crandall Creek Bridge downstream to the north boundary of Section 13, T56N, R104W at Clarks Fork Canyon.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 20.5; Total &#8212; 20.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/yellowstone.jpg" alt="Clarks Fork of the Yellowstone River" title="Clarks Fork of the Yellowstone River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/yell/" alt="Yellowstone National Park (National Park Service)" target="_blank">Yellowstone National Park (National Park Service)</a></p>
<p><a href="../documents/plans/clarks-fork-plan.pdf" alt="Yellowstone River (Clarks Fork) Management Plan (247 KB PDF)" target="_blank">Yellowstone River (Clarks Fork) Management Plan (247 KB PDF)</a></p>
<p><a href="../documents/plans/clarks-fork-plan-ea.pdf" alt="Yellowstone River (Clarks Fork) Management Plan Environmental Assessment (7.3 MB PDF)" target="_blank">Yellowstone River (Clarks Fork) Management Plan Environmental Assessment (7.3 MB PDF)</a></p>
<p><a href="../documents/plans/clarks-fork-plan-fonsi.pdf" alt="Yellowstone River (Clarks Fork) Management Plan FONSI (250 KB PDF)" target="_blank">Yellowstone River (Clarks Fork) Management Plan FONSI (250 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Yellowstone River (Clarks Fork)</h2>
<p>Nationally, this river segment's scenery is important due to its near proximity to the Chief Joseph State Scenic Byway and the Beartooth All American Highway. (All American Highways are the National Scenic Byway Program's elite designation; approximately six National Byways have qualified for this title.) Generally, these scenic highways follow the Clarks Fork of the Yellowstone River with high mountain peaks in the background and very little development or other human activities nearby. National and international visitors travel this scenic/recreation corridor on their way to Yellowstone National Park.</p>
<p>The Clarks Fork possesses regionally and nationally important wildlife habitat for a diverse array of species including large carnivores and ungulates: grizzly bears, gray wolves, moose, elk, deer and other smaller species. The area offers world class fishing and hunting opportunities. Dramatic waterfalls exist within the inner gorges; few visitors trek the harsh and rugged terrain to access the river's gorges.</p>
<p>Most access is by wildlife, volunteer and other lightly defined paths; otherwise access to the river shores is by overland orienteering. Much of the river bottom and/or gorges are inaccessible to traditional hikers. This river segment offers a high-quality wilderness experience without the formal "wilderness" designation. Neither special access permit regulations nor fees are in effect. Review the Shoshone National Forest travel management maps and brochures prior to planning any motorized vehicle access.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>