<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Owyhee River, Idaho';

// Set the page keywords
$page_keywords = 'Owyhee River, Idaho';

// Set the page description
$page_description = 'Owyhee River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('191');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The Owyhee River from the Idaho-Oregon State border to the upstream boundary of the Owyhee River Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 67.3 miles; Total &#8212; 67.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/owyhee-id.jpg" alt="Owyhee River" title="Owyhee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14979/2" alt="Owyhee River (Bureau of Land Management)" target="_blank">Owyhee River (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/sites/blm.gov/files/documents/files/Media-Center_Public-Room_Idaho_Bruneau-Jarbidge-Owyhee_BoaterGuide.pdf" alt="Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide" target="_blank">Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Owyhee River</h2>

<p>The Owyhee River is a 346-mile-long tributary of the Snake River that begins in northern Nevada and flows through southwestern Idaho, before finishing its route in Oregon. The downstream section of the river below the confluence with the South Fork of the Owyhee River is known as the "Grand Canyon of the Owyhee," referring to the steep, rhyolite walls ranging in height from 250 feet to over 1,000 feet near the Oregon border. Floating the Owyhee is popular in the spring during higher water flows, and low water float trips are possible in smaller craft.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Ecologic</em></strong></p>

<p>The Owyhee River System is home to a rare plant, the Owyhee River forget-me-not. Found nowhere else but this river system, this species occupies north-facing vertical rhyolitic cliffs, sheltered crevices and shady grottos. The pale blue flowers of this species contrast sharply with the backdrop of dark volcanic rock. May and June are the best time to view this species in full flower.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Redband trout are found in the Owyhee River and its tributaries, including the North and South Forks of the Owyhee River, Battle Creek, Deep Creek, Dickshooter Creek and Red Canyon. Unfortunately, the seasonal migration of smallmouth bass into the Owyhee River system over the past several decades, where current habitat conditions and water quality parameters are favorable to the bass, has created competition for food and space between the bass and trout in many Owyhee tributaries.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Owyhee River and its tributaries are located in the "Owyhee Volcanic Field," an area of volcanic activity from the Miocene Era (25 to 3 million years ago). Where overlying basalt is present, rhyolite (rock created by lava flow) formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved towering cliffs and numerous sculptured pinnacles known as "hoodoos." This is the most dramatic area of hoodoo formations in the entire Owyhee River system, and the Owyhee, together with the Bruneau and Jarbidge river systems, provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States.</p>

<p>The oxbow canyon of "The Tules" on the East Fork of the Owyhee several miles north of Garat Crossing is a rare geologic formation.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Owyhee River and its major tributaries are generally rated as Class II whitewater, although several Class IV through Class VI sections exist in the Lambert and Garat Gorges. The upper section, referred to as the East Fork of the Owyhee River, is recommended for only kayaks and whitewater canoes, as there are two difficult portages located seven and nine miles above the confluence with the South Fork.</p>

<p>Float boating is in many areas enhanced by outstanding day-hiking opportunities. It is possible to hike from canyon rims to the stream in many locations, especially during low-water periods. Due to their meandering character, diversity of landforms and topographic screening, the canyons provide exceptional opportunities for solitude and for passive, primitive and/or physically challenging activities, including wildlife viewing, photography, angling and camping.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Owyhee River canyons are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Intertwined with the vertical features are some very steep diagonal lines that frame triangular forms associated with talus slopes. The slopes have a mosaic of medium-textured, yellow and subdued green sagebrush- bunchgrass communities and/or dark green juniper, as well as either medium-textured, reddish rhyolite rubble fields or coarse-textured, blackish basalt rubble fields.</p>

<p>Spring rains result in medium-textured, rich green riparian vegetation that follows the meandering lines of fast-moving streams that run brownish in high flows. Large boulders and whitewater rapids are interspersed to varying degrees between the calm reaches. During summer months, sparkling pools and slow-moving water tinted with green and brown channel colors reflect blue sky and a blend of forms, colors and lines from surrounding cliffs and steep slopes. Receding waters also expose whitish, medium-textured stream-bottom gravel and boulders.</p>

<p>The basalt and rhyolite canyon/riparian associations found along the designated Owyhee, Bruneau and Jarbidge segments are among the best representations of this landscape in the region. They provide exceptional combinations of line, form, color and texture found amidst this close association of landforms, water and vegetation.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee Canyonlands system as a whole is a wildlife habitat area of national, if not international, importance. It provides upland and canyon riparian habitats for California bighorn sheep, elk, mule deer and pronghorn. The Owyhee River, Battle Creek, Deep Creek, Duncan Creek and Wickahoney Creek support the majority of the bighorn sheep population in the Owyhee Canyonlands.</p>

<p>Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents, rabbits, shrews, bats, weasels and skunks. The waters along the entire Owyhee River system and its tributaries are considered outstanding habitat for river otters.</p>

<p>Bird in the area include songbirds, waterfowl, shorebirds and raptors. The high, well-fractured and eroded canyon cliffs are considered outstanding habitat for cliff nesting raptors.</p>

<p>Other wildlife includes several snakes, lizards and a few amphibians (frogs, toads and salamanders).</p>

<p>The Owyhee River area is considered Preliminary Priority Habitat for greater sage-grouse. Idaho BLM sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>