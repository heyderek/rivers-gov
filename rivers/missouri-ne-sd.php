<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Missouri River, Nebraska, South Dakota';

// Set the page keywords
$page_keywords = 'Missouri National Recreation River, National Park Service, Missouri River, Nebraska, South Dakota';

// Set the page description
$page_description = 'Missouri River, Nebraska, South Dakota';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('118','22');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php include_once ("../iframe.php"); ?>
<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>


<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Missouri National Recreational River</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978, and May 24, 1991. From Gavins Point Dam, South Dakota, downstream to Ponca State Park, Nebraska. From Fort Randall Dam to Lewis and Clark Lake.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>November 10, 1978: Recreational &#8212; 59.0 miles; Total &#8212; 59.0 miles. May 24, 1991: Recreational &#8212; 39.0 miles; Total &#8212; 39.0 miles. Aggregate Totals: Recreational &#8212; 98.0 miles; Total &#8212; 98.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/missouri.jpg" alt="Missouri River" title="Missouri River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/mnrr/" alt="Missouri National Recreational River (National Park Service)" target="_blank">Missouri National Recreational River (National Park Service)</a></p>
<p><a href="http://www.blm.gov/mt/st/en/fo/umrbnm.html" alt="Missouri River (Bureau of Land Management)" target="_blank">Missouri River (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Lisa Yager</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Missouri River (Nebraska and South Dakota)</h2>
<p>This designation consists of two separate segments&#8212;from the Fort Randall Dam downstream to the backwaters of Lewis and Clark Lake and from Gavins Point Dam downstream to Ponca State Park. These are among the last free-flowing segments of the once "Mighty Mo" and still exhibit the river's dynamic character in its islands, bars, chutes and snags.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>