<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Rio de la Mina River, Puerto Rico';

// Set the page keywords
$page_keywords = 'Caribbean National Forest, Riode la Mina, El Yunqe National Forest, Puerto Rico';

// Set the page description
$page_description = 'Rio de la Mina River, Puerto Rico';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'tropical';

//ID for the rivers
$river_id = array('166B');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Caribbean National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 19, 2002. From its headwaters to its confluence with the Rio Mameyes.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 1.2 miles; Recreational &#8212; 0.9 miles; Total &#8212; 2.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rio-de-la-mina.jpg" alt="Rio de la Mina" title="Rio de la Mina" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detailfull/elyunque/home/?cid=fsbdev3_042977&width=full#wild_scenic" alt="Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)" target="_blank">Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/el-yunque-plan.pdf" alt="Rio de la Mina Management Plan (2.5 MB PDF)" target="_blank">Rio de la Mina Management Plan (2.5 MB PDF)</a></p>
<p><a href="../documents/plans/el-yunque-ea.pdf" alt="Rio de la Mina Management Plan Environmental Assessment (2.0 MB PDF)" target="_blank">Rio de la Mina Management Plan Environmental Assessment (2.0 MB PDF)</a></p>
<p><a href="../documents/plans/el-yunque-decision-notice.pdf" alt="Rio de la Mina Management Plan (454 KB PDF)" target="_blank">Rio de la Mina Management Plan (454 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Raul Garcia</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Rio de la Mina</h2>
<p>The Rio de la Mina, from its headwaters elevation at 2,408 feet (734 meters) to the confluence with the Rio Mameyes at 820 feet (250 meters), is strewn with boulders of various sizes, forming many rapids, waterfalls and frequent pools. La Mina Falls is located at the midway point of the stream at elevation 1,640 (500 meters) and is located where La Mina Trail and Big Tree Trail connect. The Rio de la Mina is part of the Mameyes Watershed; the Rio de la Mina sub-watershed covers an area of 2.7 square miles which equals 6.1% of the forest. The river provides habitat for shrimp, and Puerto Rican parrots can be glimpsed at times feeding on palm fruit along the river.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>