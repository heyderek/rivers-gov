<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Rio Grande, New Mexico';

// Set the page keywords
$page_keywords = 'Rio Grande, Red River, Bureau of Land Management, Taos Resource Area, Carson National Forest, New Mexico';

// Set the page description
$page_description = 'Rio Grande River, New Mexico';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('155','4');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Taos Field Office<br />
U.S. Forest Service, Carson National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968, and May 4, 1994. The segment extending from the Colorado state line downstream approximately 68 miles to the west section line of Section 15, T23N, R10E. The lower four miles of the Red River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>October 2, 1968: Wild &#8212; 54.9 miles; Recreational &#8212; 0.8 miles; Total &#8212; 55.7 miles. May 4, 1994: Scenic &#8212; 12.5 miles; Total &#8212; 12.5 miles. Aggregate Totals: Wild  &#8212; 54.9 miles; Scenic &#8212; 12.5 miles; Recreational &#8212; 0.8 miles; Total &#8212; 68.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rio-grande-nm.jpg" alt="Rio Grande, New Mexico" title="Rio Grande, New Mexico" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/nm/st/en/prog/NLCS/RGDN_NM/rgdn_recreation_opportunities.html" alt="Rio Grande (Bureau of Land Management)" target="_blank">Rio Grande (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/rio-grande-plan.pdf" alt="Rio Grande Management Plan (2.9 MB PDF)" target="_blank">Rio Grande Management Plan (2.9 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Rio Grande (New Mexico)</h2>
<p>The Rio Grande flows out of the snowcapped Rocky Mountains in Colorado and journeys 1,900 miles to the Gulf of Mexico. It passes through the 800-foot chasms of the Rio Grande Gorge, a wild and remote area of northern New Mexico.</p>

<p>The Rio Grande and Red River designation was among the original eight rivers designated by Congress as wild and scenic in 1968. In 1994, the designation was extended by legislation to include an additional 12.5 miles of the Rio Grande. The designated area includes 56 miles of the Rio Grande from the Colorado/New Mexico state line to just beyond BLM's County Line Recreation Site and the lower 4 miles of the Red River.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural &amp; Historic</em></strong></p>

<p>This area has attracted human activity since prehistoric times. Evidence of ancient use is found throughout the area in the form of petroglyphs, prehistoric dwelling sites and other types of archaeological sites.</p>

<p><strong><em>Fish &amp; Wildlife</em></strong></p>

<p>The river gorge is home to numerous species of wildlife, including big horn sheep, river otters and the Río Grande cutthroat trout.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Río Grande del Norte National Monument is comprised of rugged, wide-open plains at an average elevation of 7,000 feet, dotted by volcanic cones and cut by steep canyons with rivers tucked away in their depths. The Río Grande carves an 800-foot-deep gorge through layers of volcanic basalt flows and ash. Among the volcanic cones in the Monument, Ute Mountain is the highest, reaching to 10,093 feet.</p>

<p>Flowing out of the Sangre de Cristo Mountains of New Mexico, the Red River was the head of a smaller Río Grande of ancient times. Scientists believe that some 400,000 years ago, a closed drainage basin with no outflow existed in the San Luis Valley to the north in Colorado. With changes causing the region to become wetter, the lake within this basin overflowed to the south and drained into the Red River. The volume of water continued to increase as it cut through the earth, forming the modern Río Grande Gorge we know today. As a result, the Red River became a tributary and the headwaters of the Río Grande.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Two developed recreation areas are located along the river&#8212;Wild Rivers on the north and Orilla Verde in the south. In addition to these scenic recreation areas, visitors can enjoy a spectacular vista of the gorge from the High Bridge Overlook where Highway 64 crosses the river.</p>

<p>Recreation opportunities include biking, camping, fishing, hiking/backpacking, picnicking, wildlife viewing, horseback riding and whitewater boating.</p>

<p>The Upper Gorge includes the Class II Ute Mountain Run (24 miles), Class V Upper Box (6.5 miles), and Class II-IV Taos Box (15 miles), which takes 6 to 8 hours, including stops for lunch and sightseeing. The rapids in the Box offer the best whitewater opportunities in New Mexico, through one of the most scenic sections of the Río Grande Gorge. Paralleled by state roads, the Lower Gorge receives the majority of the recreational use, beginning at the Taos Junction Bridge in the Orilla Verde area and extending south for 18 miles. The Racecourse, a five-mile stretch of whitewater located along NM 68, is less demanding than the Box but still offers an exciting ride. Trips in the Lower Gorge vary in time from two to six hours, depending on river flow and ones' choice of put in and take out.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Río Grande Wild and Scenic River, located within the Río Grande del Norte National Monument, includes 74 miles of the river as it passes through the 800-foot deep Río Grande Gorge. Flowing out of the snowcapped Rocky Mountains in Colorado, the river journeys 1,900 miles to the Gulf of Mexico. Here the river flows in a rugged and scenic part of northern New Mexico.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>