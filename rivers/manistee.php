<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Manistee River, Michigan';

// Set the page keywords
$page_keywords = 'Huron-Manistee National Forest, Manistee River, Michigan';

// Set the page description
$page_description = 'Manistee River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('123');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Huron-Manistee National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From the Michigan Department of Natural Resources boat ramp below Tippy Dam to the Michigan State Highway 55 Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 26.0 miles; Total &#8212; 26.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/manistee.jpg" alt="Manistee River" title="Manistee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recmain/hmnf/recreation" alt="Huron-Manistee National Forest" target="_blank">Huron-Manistee National Forest</a></p>
<p><a href="http://www.michigandnr.com/publications/pdfs/wildlife/viewingguide/nlp/41Manistee/" alt="Michigan Department of Natural Resources" target="_blank">Michigan Department of Natural Resources</a></p>
<p><a href="../documents/plans/bear-creek-manistee-plan.pdf" alt="Manistee River Management Plan (8.7 MB PDF)" target="_blank">Manistee River Management Plan (8.7 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Pamela Behling</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Manistee River</h2>
<p>The Manistee Wild and Scenic River is well known for beautiful scenery, excellent fishing and a variety of recreational activities. In the spring and fall, high numbers of anglers are attracted to the superb salmon and steelhead runs. During the summer, walleye and pike fishing become the primary recreational activity. The river supports a variety of other recreational uses including wildlife viewing, hiking, canoeing and hunting.</p>
<p>Private businesses and government agencies have developed a variety of facilities and services to meet the expanding recreation demands of the public.  Commercial guided fishing is one of the most popular activities on the Manistee River. The amount of recreational use fluctuates from year to year, mostly based on the fishing runs and local economic factors. There are eight developed river access sites within the wild and scenic river corridor.  The Forest Service maintains sites at High Bridge, Bear Creek, Rainbow Bend and Blacksmith Bayou. The state of Michigan operates a river access site at Tippy Dam.  Private recreation sites include Big Manistee Riverview Campground and Coho Bend Campground. The U.S. Forest Service developed recreation sites along the Manistee River require a vehicle parking pass under the Recreation Enhancement Act.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>