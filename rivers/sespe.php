<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sespe Creek, California';

// Set the page keywords
$page_keywords = 'Los Padres National Forest, condors, Sespe Creek, California';

// Set the page description
$page_description = 'Sespe Creek, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('142');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Los Padres National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>June 19, 1992. The main stem from its confluence with Rock Creek and Howard Creek downstream to where it leaves Section 26, Township 5 North, Range 20 West.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 27.5 miles; Scenic &#8212; 4.0 miles; Total &#8212; 31.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/sespe-creek.jpg" alt="Sespe Creek" title="Sespe Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/sespe-creek-plan.pdf" alt="Sespe Creek Management Plan" target="_blank">Sespe Creek Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sespe Creek</h2>
<p>Sespe Creek has exemplary visual features, including contrast created by large rock outcroppings and seasonal colors, and water that attracts regional and national attention. Below Chorro Grande Canyon, Sespe Creek offers excellent dispersed recreation opportunities, such as swimming and wading, picnicking, backpacking, hiking, horseback riding, bicycling, rock climbing, hunting, fishing, photography, driving for pleasure and viewing scenery on the adjacent scenic byway.</p>
<p>Sespe Creek provides important spawning habitat for the endangered southern steelhead trout, which migrates from the Pacific Ocean, up the Santa Clara River and into Sespe Creek to spawn. The creek also supports one of the largest populations of endangered arroyo toads within a one-hundred miles radius. Its rich and relatively intact riparian habitat, which is rare in Southern California, supports the endangered southwestern willow flycatcher and least Bell's vireo and many songbirds.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>