<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Little Jacks Creek, Idaho';

// Set the page keywords
$page_keywords = 'Little Jacks Creek, Idaho';

// Set the page description
$page_description = 'Little Jacks Creek, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('189');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. Little Jacks Creek from the downstream boundary of the Little Jacks Creek Wilderness upstream to the mouth of OX Prong Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 12.4 miles; Total &#8212; 12.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/little-jacks-creek.jpg" alt="Little Jacks Creek" title="Little Jacks Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14978/2" alt="Little Jacks Creek (Bureau of Land Management)" target="_blank">Little Jacks Creek (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Judi Zuckert, BLM</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<p>Little Jacks Creek is located in the Little Jacks Wilderness and flows through a multi-tiered, 1,000-foot-deep basalt canyon system. The narrow, winding stream provides habitat for redband trout, and the area offers excellent opportunities for viewing wildlife, especially bighorn sheep.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Recreational</em></strong></p>

<p>Due to its meandering character, diversity of landforms and topographic screening, Little Dicks Creek offers exceptional opportunities for solitude and for challenging wilderness hiking, wildlife viewing and photography.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>Little Jacks, Big Jacks, Cottonwood and Duncan Creeks are among the 17% of desert streams in the Northern Basin and Range identified as aquatic-habitat strongholds for redband trout, a BLM sensitive species and a state of Idaho species of special concern. Little Jacks Creek's good water quality, a well-shaded riparian vegetative canopy, and long-term protection from livestock grazing have produced the highest densities of redband trout of any surveyed stream in southwest Idaho.</p>

<p>The success of fisheries in these systems depends on appropriate flows during key life stages. These streams exhibit typical flashy, desert streamflows to which the resident fish species are adapted. In summer and early fall, low flows are sufficient to maintain standing pools for fish survival. The high flows that may occur only every few years are integral to the maintenance of channels that support the pool depths and channel diversity, also necessary for fish survival.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Canyons along Little Jacks Creek are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Intertwined with the vertical features are some very steep diagonal lines that frame triangular forms associated with talus slopes. Slopes have a mosaic of medium-textured, yellow and subdued green sagebrush-bunchgrass and/or dark green juniper, and medium-textured, reddish rhyolite rubble fields and/or coarse-textured, blackish basalt rubble fields.</p>

<p>Although basalt and rhyolite canyon/riparian associations are seen in many areas of southwest Idaho, the associations found on Little Jacks Creek and other designated Owyhee, Bruneau and Jarbidge segments are among the best representations of this landscape in the region.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Little Jacks Creek and surrounding wilderness provide habitat for mountain quail, sage hens, mountain lions, mule deer, pronghorn antelope, bobcats, coyotes and bighorn sheep. Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents (mice, kangaroo rats, voles, squirrels and chipmunks), rabbits, shrews, bats, weasels and skunks.</p>

<p>Songbirds, waterfowl, shorebirds and raptors are present in the area, as are several snake and lizard species and a few amphibians (frogs, toads and salamanders).</p>

<p>Little Jacks Creek is included in the area of Preliminary Priority Habitat for greater sage-grouse. Additional Idaho BLM sensitive species include the bald eagle, yellow-billed cuckoo, prairie falcon ferruginous hawk, several neotropical migratory bird species, several bat species, Columbia spotted frog and western trout. Cliffs support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>