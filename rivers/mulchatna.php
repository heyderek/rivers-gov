<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Mulchatna River, Alaska';

// Set the page keywords
$page_keywords = 'Mulchatna River, Lake Clark National Park, Turquoise Lake, Chigmit Mountains, Alaska';

// Set the page description
$page_description = 'Mulchatna River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('32');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Lake Clark National Park and Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment within Lake Clark National Park and Preserve.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 24.0 miles; Total &#8212; 24.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/mulchatna.jpg" alt="Mulchatna River" title="Mulchatna River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/lacl/" alt="Lake Clark National Park and Preserve (National Park Service)" target="_blank">Lake Clark National Park and Preserve (National Park Service)</a></p>
<p><a href="http://www.nps.gov/lacl/planyourvisit/rafting.htm" alt="Rafting the Chilikadrotna River (National Park Service)" target="_blank">Rafting the Chilikadrotna River (National Park Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Lamont Glass</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Mulchatna River</h2>
<p>The Mulchatna River, originating in Lake Clark National Park and Preserve, flows through astonishingly scenic tundra. Its headwaters are Turquoise Lake, and it is flanked by the glacier-clad Chigmit Mountains to the east. The Mulchatna Caribou Herd frequents the area.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>