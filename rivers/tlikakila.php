<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Tlikakila River, Alaska';

// Set the page keywords
$page_keywords = 'Tlikakila River, Lake Clark National Park, Alaska';

// Set the page description
$page_description = 'Tlikakila River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('37');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Lake Clark National Park and Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment within Lake Clark National Park.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 51.0 miles; Total &#8212; 51.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/tlikakila.jpg" alt="Tlikakila River" title="Tlikakila River" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/lacl/" alt="Lake Clark National Park and Preserve (National Park Service" target="_blank">Lake Clark National Park and Preserve (National Park Service)</a></p>
<p><a href="http://www.nps.gov/lacl/planyourvisit/rafting.htm" alt="Rafting the Chilikadrotna River (National Park Service)" target="_blank">Rafting the Chilikadrotna River (National Park Service)</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Tlikakila River</h2>
<p>Originating at the foot of glaciers in a mountain pass, the Tlikakila is a braided glacial river located entirely within Lake Clark National Park. It flows through 10,000-foot high rock-and-snow-capped mountains and perpendicular cliffs to empty into Lake Clark.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>