<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Piru Creek, California';

// Set the page keywords
$page_keywords = 'Piru Creek, California, Los Padres National Forest, Angeles National Forest';

// Set the page description
$page_description = 'Piru Creek, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('199');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Angeles National Forest<br />
U.S. Forest Service, Los Padres National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From 0.5 miles downstream of Pyramid Dam at the first bridge crossing to the boundary between Los Angeles and Ventura Counties.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.3 miles; Recreational &#8212; 3.0 miles; Total &#8212; 7.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/piru-creek.jpg" alt="Piru Creek" title="Piru Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recmain/hmnf/recreation/" alt="Huron-Manistee National Forest" target="_blank">Huron-Manistee National Forest</a></p>
<p><a href="../documents/plans/pine-plan.pdf" alt="Pine River Management Plan (7.3 MB PDF)" target="_blank">Pine River Management Plan (7.3 MB PDF)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Piru Creek</h2>
<p>Piru Creek is one of two streams on the Angeles National Forest being managed for wild trout by the California Department of Fish and Game. Riparian areas provide habitat for several riparian-dependent species, including the federally listed southwestern willow flycatcher, least Bell's vireo and California condor. The demand for low-elevation recreation along riparian areas can be observed at the Frenchman's Flat day use area, which is also where Piru Creek starts.</p>
<p>The lower segment of Piru Creek starts one-half mile below Pyramid Lake Dam and continues downstream into the Sespe Wilderness. Along this 7.3-mile stretch of the river, geological values were determined to be outstandingly remarkable, including scenic tilted layers of sedimentary rocks as well as faults and rock formations with features crucial to the understanding of geological formation of the West Coast of North America.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>