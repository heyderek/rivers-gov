<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Charley River, Alaska';

// Set the page keywords
$page_keywords = 'Yukon-Charley National Preserve, National Park Service, Charley River, Yukon River, Alaska';

// Set the page description
$page_description = 'Charley River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('28');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Yukon-Charley Rivers National Preserve</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The entire river, including its major tributaries&#8212;Copper Creek, Bonanza Creek, Hosford Creek, Derwent Creek, Flat-Orthmer Creek, Crescent Creek, and Moraine Creek&#8212;within the Yukon-Charley Rivers National Preserve.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 208.0 miles; Total &#8212; 208.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/cache-la-poudre.jpg" alt="" width="265px" height="204px" />-->
<p>We could use a photo. <br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/yuch/" target="_blank" alt="Yukon-Charley Rivers National Preserve (National Park Service)" target="_blank">Yukon-Charley Rivers National Preserve (National Park Service)</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Martha Moran</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Charley River</h2>
<p>The exceptionally clear Charley River is completely contained within Yukon-Charley Rivers National Preserve. The river drops 32 feet/mile in its upper reaches and offers whitewater challenges during high water; during low water flows, it is mild and can be enjoyed by many. Bighorn sheep, caribou, peregrine falcon, moose and bears may be seen along its banks.  This is one of the few rivers designated for its entire length.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>