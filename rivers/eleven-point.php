<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Eleven Point River, Missouri';

// Set the page keywords
$page_keywords = 'Mark Twain National Forest, Eleven Point River, Missouri';

// Set the page description
$page_description = 'Eleven Point River, Missouri';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('2');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mark Twain National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968. The segment extending downstream from Thomasville to State Highway 142.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 44.4 miles; Total &#8212; 44.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/eleven-point.jpg" alt="Eleven Point River" title="Eleven Point River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/mtnf/recreation/recarea/?recid=21676" alt="Eleven Point River (U.S. Forest Service)" target="_blank">Eleven Point River (U.S. Forest Service)</a></p>
<p><a href="http://www.memphisparent.com/Memphis-Parent/Living-in-the-Moment/June-2012/Lets-Go-Canoeing-the-Scenic-Eleven-Point-River-in-Alton-Missouri/" target="_blank">Canoeing the Eleven Point River (Memphis Parent Magazine)</a></p>
<p><a href="../documents/plans/eleven-point-plan.pdf" title="Eleven Point Management Plan" target="_blank">Eleven Point Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Bob Charity</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Eleven Point River</h2>
<p>The Eleven Point River has been recognized and loved for its rich history and outstanding scenic beauty since early settlement days. A 44-mile portion of the Eleven Point between Thomasville, Missouri, and the Highway 142 Bridge became one of the eight initial components of the National Wild and Scenic River System in 1968.</p>
<p>About half of the lands within the Eleven Point Scenic River area are private lands, while the remainder of the area is National Forest System land. The private lands along the river are managed by scenic easements to assure protection of their scenic values and natural resources. Public entry of these lands is prohibited. River users should study the river map and be alert for private property or scenic easement signs located along the river course to avoid trespass.</p>
<p>The Eleven Point River meanders through the picturesque Ozark hills of southern Missouri. Its course is cut in the shadows of steep bluffs, through sloping forested valleys and low-lying riparian ecosystems. Barely more than a small stream at its upper reaches near Thomasville, Missouri, it gains considerable width and depth as it proceeds southeastward. Springs pouring from dolomite bluffs or rushing up from a vast network of underground flow systems provide a continuous source of water and beauty.</p>
<p>Alternating stretches of rapids and deep clear pools wind around moss-covered boulders and shading bottomland hardwood trees. River birch is abundant along the shore, and aged sycamores lean out across the river from their shoreline moorings. In some places, the canopy of green closes overhead.</p>
<p>To the experienced canoeist, the Eleven Point is a relatively easy river (Class I and Class II on the International Scale) requiring intermediate experience. Snags, trees and root wads still remain the most dangerous of all obstacles and on occasion may require scouting from shore. Although canoes are the time-tested means of travel through fast water, kayaks are more and more common, and flat bottom John boats are used on the river, primarily for fishing trips. You may encounter boats with motors. Motor boats are restricted to a 25 hp limit. Particularly during the late summer, you may also encounter some float tubes, but the cold water and longer distances between river accesses limits this use.</p>
<p>Smallmouth bass, rock bass, walleye and trout are eagerly sought by anglers on the Eleven Point. All fishing is subject to state of Missouri regulations. Non-residents may purchase a three-day or annual license. A trout permit is also required if you are in possession of trout. Gigging is popular, but not permitted within the Wild Trout Management Area. To prevent illegal fishing practices and possible citations, review and study the latest fish and game regulations published by the Missouri Department of Conservation.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>