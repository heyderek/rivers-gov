<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'John Day River (South Fork), Oregon';

// Set the page keywords
$page_keywords = 'Malheur National Forest, Bureau of Land Management, John Day River, Oregon';

// Set the page description
$page_description = 'John Day River (South Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('101');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Prineville District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the Malheur National Forest boundary to the confluence with Smoky Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 47.0 miles; Total &#8212; 47.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/john-day-sf-2.jpg" alt="South Fork John Day River" title="South Fork John Day River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<!--<p><a href="http://www.blm.gov/or/resources/recreation/site_info.php?siteid=137" alt="John Day River (Bureau of Land Management)" target="_blank">John Day River (Bureau of Land Management)</a></p>-->
<p><a href="../documents/plans/south-fork-john-day-rod.pdf" title="South Fork John Day River Management Plan Record of Decision" target="_blank">South Fork John Day River Management Plan Record of Decision</a></p>

<div id="photo-credit">
<p>Photo Credit: Zachary Collier, Northwest Rafting Company</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>John Day River (South Fork)</h2>

<p>The South Fork of the John Day River is located in central Oregon, flowing from the Ochoco and Aldrich Mountains to meet the main stem of the John Day River at the town of Dayville. The river is nationally known for smallmouth bass and steelhead and is an excellent destination for many outdoor activities.  Aside from the road that parallels the river and handful of small ranches, the area  provides a near-natural setting.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic</em></strong></p>

<p>The South Fork of the John Day River corridor contains a number of relatively pristine plant communities and two significant species, the Washington monkeyflower and threatened south John Day milk vetch. The diversity of plant communities provides important wildlife habitat, interpretive opportunities and aesthetic values to the area. Vegetation has been impacted by humans via fire control, road construction, unmanaged livestock grazing. A coordinated multi-agency program is in place to restore the quality of the vegetative communities.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The John Day River hosts one of the few remaining wild fish runs in the Pacific Northwest. The summer steelhead and spring Chinook salmon returning for spawning contribute to the largest entirely wild run in the mid and upper Columbia River Basin; spawning and rearing takes place below Izee Falls. Resident redband trout populations are augmented with hatchery stock, and whitefish are also present and popular.</p>

<p><strong><em>Geologic &amp; Paleontologic</em></strong></p>

<p>A complicated geologic history in the area has resulted in a diverse assemblage of rocks that include masses of oceanic crust, marine sediments, a wide variety of volcanic rocks, ancient river and lake sediments, and recent landslide deposits. North of Izee Falls, the Mascall Formation features marine invertebrates, fossiliferous outcrops and fissure dikes. South of the falls, features include ammonites, bivalves and rhyconellid brachiopods.</p>

<p><strong><em>Recreational</em></strong></p>

<p>This river corridor provides a wide variety of recreation opportunities, including camping, boating, hunting and fishing. The river is nationally known for smallmouth bass and steelhead, and popular for floating and swimming, hunting, hiking and camping. Visitors travel here to see the rugged geologic formations of the canyon and internationally significant fossil beds.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The views here are colorful, striking and unique. Basalt outcrops, Ponderosa pine and Douglas and white fir intermix with juniper, sagebrush and native bunchgrasses to create a distinct pattern on the rugged canyon slopes. The river is petite but active, and it flows over 55-foot Izee Falls halfway to the mainstem.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The South Fork is a key wildlife area due to the diversity and condition of the habitats in its corridor. Mature (never been cut) Ponderosa pine and fir forests have provided a stable environment for its diverse, balanced population. Grass and sagebrush hillsides provide forage for big game species and nesting for many migratory and resident bird species.  Bald eagles visit in winter; golden eagles, redtail hawks and prairie falcons nest in the canyon. Lewis' and other woodpeckers, owls and quail nest and feed here. Mule deer and elk winter here, as well. Resident predators include mountain lions and bobcats.  Minks, beavers, river otters, coyotes and rattlesnakes are common, as well.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>