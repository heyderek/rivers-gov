<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Clackamas River (South Fork), Oregon';

// Set the page keywords
$page_keywords = 'Clackamas River, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Clackamas River (South Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('171');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From its confluence with the East Fork of the South Fork Clackamas to its confluence with the Clackamas River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 4.2 miles; Total &#8212; 4.2 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/clackamas-sf.jpg" alt="South Fork Clackamas River" title="South Fork Clackamas River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/mthood/home/?cid=STELPRDB5200831" alt="Clackamas River (U.S. Forest Service)" target="_blank">Clackamas River (U.S. Forest Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Clackamas River (South Fork)</h2>

<p>The South Fork of the Clackamas River is a tributary of the Clackamas River on the western slope of the Cascade Range in northwest Oregon. The 4.2-mile segment flows from its confluence with the East Fork of the South Fork of the Clackamas River and merges with Memaloose Creek to join the mainstem Clackamas River less than an hour's drive from Portland.  As it flows through a narrow canyon with large rock outcrops and cliffs, a 100-foot waterfall in the lower part of the segment and old-growth trees along the river add to the visual diversity.  Seasonally, deer and elk are in the area, and spotted owls and bald eagles also call the watershed home. Most of the watershed, filled with Pacific silver fir, Douglas fir and western hemlock, is under the care of the U.S. Forest Service.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>There are small resident rainbow and cutthroat trout populations above the falls. Below the falls, spring chinook, winter and summer steelhead, fall run coho and a late winter run coho are found. The winter coho are a rare native stock of salmon originally found throughout the Columbia River drainage but now limited to the Clackamas River and its tributaries.</p>

<p><strong><em>Recreational</em></strong></p>

<p>This is one of the most remote large streams in the state. Trail uses include hiking, mountain biking, backpacking and access to catch-and-release fishing.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>