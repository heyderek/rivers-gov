<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Illinois River, Oregon';

// Set the page keywords
$page_keywords = 'Illinois Wild and Scenic River, Oregon';

// Set the page description
$page_description = 'Illinois River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('54');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Rogue River-Siskiyou National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 19, 1984. From the boundary of the Siskiyou National Forest downstream to its confluence with the Rogue River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 28.7 miles; Scenic &#8212; 17.9 miles; Recreational &#8212; 3.8 miles; Total &#8212; 50.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/illinois.jpg" alt="Illinois River" title="Illinois River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/rogue-siskiyou/recreation/recarea/?recid=74294" alt="Illinois River, U.S. Foirest Service" title="Illinois River, U.S. Foirest Service" target="_blank">Illinois River, U.S. Forest Service</a></p>
<p><a href="../documents/plans/illinois-plan.pdf" alt="Illinois River Management Plan (10.8 MB PDF)" target="_blank">Illinois River Management Plan (10.8 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Illinois River</h2>
<p>Located in southwest Oregon, the Illinois Wild and Scenic River flows from the Rogue River-Siskiyou National Forest boundary to its confluence with the Wild and Scenic Rogue River, a distance of 50.4 miles. The river's outstandingly remarkable values are scenery, recreation, fish, botany and water quality.</p>
<p>The river offers marvelous recreation opportunities, including whitewater that provides plenty of excitement for small rafts and kayaks. The wild section flows through a steep canyon for 29 miles between Briggs and Nancy Creeks, featuring 150 rapids, of which 11 are Class IV and one, is Class V. This section is reputed to be the most remote, inaccessible river segment in the lower 48 states. Permits for floating the Wild Illinois are necessary year-round, but are not restricted and there is no fee; primary boating season is March through mid-May.</p>
<p>Still, clear, blue-green pools offer contrast to the whitewater segments of the river and opportunities for catching anadromous fish, including large steelhead.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>