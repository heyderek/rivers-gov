<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Pere Marquette River, Michigan';

// Set the page keywords
$page_keywords = 'Pere Marquette River, Huron-Manistee National Forest, Michigan';

// Set the page description
$page_description = 'Pere Marquette River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('16');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Huron-Manistee National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978. The segment downstream from the junction of the Middle and Little South Branches to its junction with U.S. Highway 31.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 66.0 miles; Total &#8212; 66.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/pere-marquette.jpg" alt="Pere Marquette River" title="Pere Marquette River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recmain/hmnf/recreation/" alt="Huron-Manistee National Forest" target="_blank">Huron-Manistee National Forest</a></p>
<p><a href="../documents/plans/pere-marquette-plan.pdf" alt="Pere Marquette River Management Plan (586 KB PDF)" target="_blank">Pere Marquette River Management Plan (586 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Frank Willetts, Pere Marquette River Lodge</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Pere Marquette River</h2>
<p>In 1978, 66 miles of the Pere Marquette were designated as a Wild and Scenic River. In that same year, the state designated the entire Pere Marquette and its major tributaries a Michigan Natural River. The Pere Marquette River is the longest unregulated (no dams or impoundments) river system in Michigan's Lower Peninsula. The river is a nationally known, high-quality stream, which supports large populations of resident trout, steelhead and salmon. From M-37 to Gleason's Landing, the State has designated the river as quality fishing water requiring catch and release with flies only.</p>
<p>The Pere Marquette River, within the national forest boundary, has been divided into three non-motorized watercraft segments: The Forks to Bowman Bridge; Bowman Bridge to Upper Branch Bridge; and Upper Branch Bridge to Walhalla Bridge. These segments allow for easier administration of the river and the watercraft permit system.</p>
<p>Watercraft permits are required from the Friday of Memorial Day weekend through the Monday of Labor Day weekend for launching and retrieving watercraft from Forest Service access sites. Watercraft permit numbers vary by river segment and the day of the week, with permits divided between one permitted livery and the Forest Service. During this time period, watercraft hours are from 9:00 a.m. to 6:00 p.m. Watercraft permits are available on line at receation.gov.</p>
<p>Camping within the river corridor on National Forest System lands is allowed only at designated sites. Sites range in experience from campgrounds to canoe-in only camps. Fees for camping and using access sites are charged under the Recreation Fee Enhancement Act. Forest Service access sites are plowed in the winter providing year round access.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>