<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Bear Creek, Michigan';

// Set the page keywords
$page_keywords = 'Huron-Manistee National Forest, Bear Creek, Michigan';

// Set the page description
$page_description = 'Bear Creek, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('119');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Huron-Manistee National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From Coates Highway to the confluence with the Manistee River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 6.5 miles; Total &#8212; 6.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/bear.jpg" alt="Bear Creek" title="Bear Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/hmnf/recreation/wateractivities/recarea/?recid=18644&actid=79" target="_blank" alt="Bear Creek, U.S. Forest Service" target="_blank">Bear Creek, U.S. Forest Service</a></p>
<p><a href="../documents/plans/bear-creek-manistee-plan.pdf" target="_blank" alt="Bear Creek Management Plan" target="_blank">Bear Creek Management Plan (8.7 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">

<h2>Bear Creek</h2>
<p>Bear Creek is located in the lower peninsula of Michigan near Manistee and meanders through lush swamps, rolling hills and farm land. The Bear Creek corridor is relatively undeveloped, providing recreational opportunities for those seeking solitude in an otherwise developed region. The primary recreational activity is fishing, with healthy populations of resident trout and excellent salmon and steelhead runs in the spring and fall. Although recreation use is low, canoeing is gaining popularity, especially for those with more advanced canoeing skills. Due to the size of the stream, canoeists must occasionally stop and push their boats through low water areas. Thus, the degree of difficulty attracts users seeking a sense of adventure. Since there is limited access to the river, hunting is the only other major recreational activity in the river corridor.</p>
<p>The primary access to the river is at Coates Highway on the northernmost boundary of the National Wild and Scenic River and Bear Creek River Access Site on River Road to the south. Only a minor amount of dispersed camping occurs in the area. The Bear Creek River Access Site requires a vehicle parking pass under the Recreation Enhancement Act.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>