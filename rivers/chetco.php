<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Chetco River, Oregon';

// Set the page keywords
$page_keywords = 'Siskiyou National Forest, Chetco River, Kalmiopsis Wilderness, Loeb State Park, Oregon';

// Set the page description
$page_description = 'Chetco River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('69');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Rogue River-Siskiyou National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters to the Rogue River-Siskiyou National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 27.5 miles; Scenic &#8212; 8.0 miles; Recreational &#8212; 11.0 miles; Total &#8212; 44.5 miles.</p>
</div>

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/chetco.jpg" alt="Chetco River" width="265px" height="204px" title="Fishing the Chetco - Agatha Conrad" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p></p>-->

<div id="photo-credit">
<p>Photo Credit: Agatha Conrad</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Chetco River</h2>
<p>The 44.5-mile designated segment of the Chetco is located within Curry County in southwest Oregon on the Chetco Ranger District of the Rogue River-Siskiyou National Forest. The Chetco heads in steep, deeply dissected, sparsely vegetated, mountainous terrain within the Kalmiopsis Wilderness. Over its 55.5-mile length, the Chetco drops from 3,700 feet to sea-level as it empties into the Pacific Ocean between the towns of Brookings and Harbor, about five miles north of the California border. In the upper section, the river floor is fairly narrow and boulder-strewn with numerous falls and rapids. As the river leaves the wilderness, its character gradually changes. The terrain becomes less dissected, the river gradient gradually becomes less steep, the river bottom widens and the surrounding hills become more densely forested.</p>
<p>Water quality, the fishery and recreation are the Chetco's outstandingly remarkable values (ORVs).</p>
<p><strong><em>Water Quality.</em></strong> The Chetco's water quality was found to be an ORV based on its striking color and clarity, its ability to clear quickly following storm events, its contribution to both recreation and fisheries, and its contribution of exceptionally pure and clean water for the domestic water supplies of both Brookings and Harbor.</p>
<p><strong><em>Fisheries.</em></strong> The Chetco River fishery, typical of Pacific coastal systems, is dominated by salmon and trout. There are important populations of anadromous winter steelhead, fall Chinook salmon and sea-run cutthroat trout. The Chetco provides excellent spawning and rearing habitat and has some of the highest salmonid smolt returns of any coastal stream in Oregon.</p>
<p><strong><em>Recreation.</em></strong> The Chetco River and its adjacent corridor offer a wide diversity of recreational opportunities. In winter, salmon and steelhead fishing and whitewater kayaking are the primary recreational uses.  In summer, fishing, four-wheel driving, swimming, boating, camping, sightseeing and picnicking are the major attractions. The primary fishing season for steelhead and salmon is between November and March.</p>
<p>Primary transportation routes within the river corridor are the North Bank Chetco River Road (#1376), the South Bank River Road (#1205) and associated spur roads. Recreationists can access the recreational and scenic river segments and launch boats in several locations, including Miller, Nook, Redwood Riverbars; upper and lower South Fork Camps; the Low-water Bridge Site; Forest Road #1917-067; and the dispersed campsite at Steel Bridge. Various trails access the river, both maintained and unmaintained.</p>
<p>Mandatory permits are required to float the Chetco for all users year round. Free, self-issuing permits are available at a boat registration station along the North Bank Chetco River Road (Forest Service Road 1376 a short distance past the Forest Boundary). Whitewater boating on the Chetco is restricted by its limited access, high use during the fishing season and availability of other good boating opportunities in the area. Rapids are Class III and below during the summer months under average flows and generally higher during winter months. Summer floating during low water conditions has become an increasingly popular river activity on all river segments. Motorized boat use is not allowed on any river segment. Motorized vehicle crossings of the river are prohibited.</p>
<p>Day use of river bars and boat launch sites is free. Fees are required for overnight use at Miller, Nook and Redwood Riverbars, Upper and Lower South Fork Camps and the Little Redwood Campground. A camping fee is also required at Loeb State Park located below the Rogue River-Siskiyou National Forest boundary.</p>	
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>