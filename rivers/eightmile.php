<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Eightmile River, Connecticut';

// Set the page keywords
$page_keywords = 'Eightmile River, Connecticut';

// Set the page description
$page_description = 'Eightmile River, Connecticut';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('170');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Northeast Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>May 8, 2008. Segments of the main stem and specified tributaries of the Eightmile River including: (1) The entire 10.8-mile segment of the main stem, starting at its confluence with Lake Hayward Brook to its confluence with the Connecticut River at the mouth of Hamburg Cove. (2) The 8.0-mile segment of the East Branch of the Eightmile River starting at Witch Meadow Road to its confluence with the main stem of the Eightmile River. (3) The 3.9-mile segment of Harris Brook starting with the confluence of an unnamed stream lying 0.74 miles due east of the intersection of Hartford Road (State Route 85) and Round Hill Road to its confluence with the East Branch of the Eightmile River. (4) The 1.9-mile segment of Beaver Brook starting at its confluence with Cedar Pond Brook to its confluence with the main stem of the Eightmile River. (5) The 0.7-mile segment of Falls Brook from its confluence with Tisdale Brook to its confluence with the main stem of the Eightmile River at Hamburg Cove.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 25.3 miles; Total &#8212; 25.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/eightmile.jpg" alt="Eightmile River" title="Eightmile River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/eightmile_pwsr_sub.html" alt="Eightmile River (National Park Service)" target="_blank">Eightmile River (National Park Service)</a></p>
<p><a href="http://www.eightmileriver.org" alt="Eightmile River Wild and Scenic Coordinating Committee" target="_blank">Eightmile River Wild and Scenic Coordinating Committee</a></p>

<div id="photo-credit">
<p>Photo Credit: Matt Lindsay, American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Eightmile River</h2>
<p>With over 150 miles of pristine rivers and streams and 62 square miles of relatively undeveloped rural land, the Eightmile River Watershed is an exceptional natural and cultural resource. The watershed contains large areas of unfragmented habitat, an array of rare and diverse wildlife, scenic vistas, high water quality, unimpeded stream flow, and significant cultural features. Most notable is that the overall Eightmile River Watershed ecosystem is healthy and intact throughout virtually all of its range. The landscape of the watershed is characterized as one of low rolling hills and ridges separated by numerous small, narrow drainage corridors and hollows, and in places broader valleys and basins.</p>
<p>The Eightmile River Wild and Scenic Coordinating Committee and the National Park Service are charged with implementing the Eightmile River Watershed Management Plan. The locally led Coordinating Committee is comprised of members appointed by local municipalities and land trusts as well as members from The Nature Conservancy, the Connecticut Department of Environmental Protection and the National Park Service.</p>
<p>The Eightmile River Watershed Management Plan was created as a part of the Eightmile River Wild and Scenic Study to establish recommended tools and strategies for ensuring the watershed ecosystem is protected and enhanced for generations to come. The Eightmile River Watershed Management Plan is a non-regulatory document, reflecting a partnership where local, state and federal interests all voluntarily agree to participate in its implementation and the realization of its purpose and goals.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>