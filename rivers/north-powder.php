<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'North Powder River, Oregon';

// Set the page keywords
$page_keywords = 'Powder River, Wallowa-Whitman National Forest, Oregon';

// Set the page description
$page_description = 'North Powder River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('94');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters in the Elkhorn Mountains to the Wallowa-Whitman National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 6.4 miles; Total &#8212; 6.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/north-powder.jpg" alt="North Powder River" title="North Powder River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="../documents/plans/north-powder-ea-assessment.pdf" title="Environmental Assessment for Management of the North Powder River" target="_blank">Environmental Assessment for Management of the North Powder River</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>North Powder River</h2>
<p>The North Powder River is located in northeast Oregon on the Wallowa-Whitman National Forest. From its headwaters at nearly 7,900 feet in the Elkhorn Mountains, this small alpine stream is fed by waters from Meadow, Little Summit and Summit Lakes. The 6.4-mile section of the river is classified as scenic and ends at the National Forest boundary. Its outstandingly remarkable values include recreation and scenery.</p>
<p>The river's name is derived from the Chinook words "polallie illahe." This translates as "sandy" or "powdery ground," which describes the soils along the stream. The drainage was utilized by the Umatilla, Nez Perce, Shoshone-Bannock and Northern Paiute Indians to gather, hunt and fish.</p>
<p>The primitive road now found paralleling the stream is a portion of a route to early-day mining camps. This road was originally constructed in 1864 by the Dealy Wagon Road Company as one of the earliest routes across the Elkhorns to Bourne and Sumpter. As this was a difficult route, it was not heavily used and easier alternate routes were constructed elsewhere. Historic references can be found to its early use, including references by Native American travelers.</p>
<p>Visitors can access the river with four-wheel drive vehicles, mountain bikes, on horseback or on foot on Forest Service roads and hiking trails. Popular activities include camping along the river at dispersed campsites, hunting, exploring the historic wagon route and sightseeing.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>