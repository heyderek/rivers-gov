<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Eagle Creek, Oregon';

// Set the page keywords
$page_keywords = 'Eagle Creek, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Eagle Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('172');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From its headwaters to the Mt. Hood National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 8.3 miles; Total &#8212; 8.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/eagle-creek-1.jpg" alt="Eagle Creek (Mt. Hood National Forest)" title="Eagle Creek (Mt. Hood National Forest)" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/id/st/en/prog/blm_special_areas/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Eagle Creek (Mt. Hood National Forest)</h2>
<p>Eagle Creek is a tributary of the Clackamas River in the western slopes of the Cascade Range in northwest Oregon. Eagle Creek flows to the west and joins the Clackamas River north of the town of Estacada, Oregon. Outstandingly remarkable values for the river segment are fisheries and wildlife.</p>
<p>The creek begins in a cirque-shaped headwater area flowing then through a steeply sloped "V" shaped valley that is heavily forested with older aged Douglas-fir and hemlock. The creek contains some riffles, with numerous downed logs and pools. The presence of large, old trees and the lack of human alterations along the segment add to the visual quality of the river.</p>
<p>The area is in a pristine condition with high habitat diversity and high water quality, making for excellent fish habitat. Stream productivity is also high. There are very few places within the region where this combination of qualities exist.</p>
<p>The forest along the creek consists primarily of an undisturbed, older aged, Douglas-fir and hemlock multi-storied stand that provides prime quality habitat for the northern spotted owl, which are known to nest in the area. There is also high-quality riparian habitat along the creek edge that meets many needs of big and small game species in the area. The lower end of the corridor provides key winter range for big game species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>