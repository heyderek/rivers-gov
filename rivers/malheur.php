<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Malheur River, Oregon';

// Set the page keywords
$page_keywords = 'Malheur National Forest, Malheur River, Oregon';

// Set the page description
$page_description = 'Malheur River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('83');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Malheur National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Bosenberg Creek to the Malheur National Forest boundary.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 6.7 miles; Scenic &#8212; 7.0 miles; Total &#8212; 13.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/malhuer.jpg" alt="Malheur River" title="Malhuer River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/malheur/specialplaces/?cid=fsbdev3_033794" alt="Malheur River (U.S. Forest Service)" target="_blank">Malheur River (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Malheur River</h2>
<p>The Malheur River is located in eastern Oregon with outstanding scenery, geology, wildlife habitat and history. The river corridor is generally characterized by a rugged and steep canyon ranging from 300- to 1,000-feet deep. The canyon geology is evident in the various rock outcrops, talus slopes and cliffs, contributing to the scenic diversity of the landscape. Scenic vistas from the canyon rims and views up and down canyon from the river are spectacular.</p>
<p>Wildlife habitat in the canyon is relatively undisturbed with a diversity of high-quality habitat components, including old-growth tree stands. The river corridor also provides important connectivity between the Blue Mountains and Great Basin physiographic provinces. The historic value of the river is its early day logging history evidenced by an old splash dam, logging camps and high-stumps. The river provides a variety of recreational opportunities for visitors willing to travel to its remote location, including trout fishing, hiking, horseback riding and camping.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>