<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Paint River, Michigan';

// Set the page keywords
$page_keywords = 'Ottawa National Forest, Paint River, Michigan';

// Set the page description
$page_description = 'Paint River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('125');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ottawa National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. The main stem from the confluence of the North and South Branches to the Ottawa National Forest boundary. The North Branch from its origin to its confluence with the South Branch. The South Branch from its origin to its confluence with the North Branch.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 52.0 miles; Total &#8212; 52.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/paint.jpg" alt="Paint River" title="Paint River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/ottawa-nf-plan.pdf" alt="Paint River Management Plan (1.4 MB PDF)" target="_blank">Paint River Management Plan (1.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Paint River</h2>
<p>The main branch of the Paint River is an excellent year-long canoe route. The brook and rainbow trout fishing is outstanding. The historical use of this river for log drives provides many interpretive opportunities. The river's outstanding remarkable values include recreation, fish and wildlife.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>