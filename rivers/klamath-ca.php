<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Klamath River, California';

// Set the page keywords
$page_keywords = 'Redwood National Park, Klamath River, Marble Mountain Wilderness, California';

// Set the page description
$page_description = 'Klamath River, California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('SD9');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Redding Field Office<br />
California Resources Agency<br />
Hoopa Valley Indian Reservation<br />
Karuk Tribe of California<br />
U.S. Forest Service, Klamath National Forest<br />
U.S. Forest Service, Six Rivers National Forest<br />
Yurok Tribe</p>
<br />
<h3>Designated Reach:</h3>
<p>January 19, 1981. From the mouth to 3,600 feet below Iron Gate Dam. The Salmon River from its confluence with the Klamath to the confluence of the North and South Forks of the Salmon River. The North Fork of the Salmon River from the Salmon River confluence to the southern boundary of the Marble Mountain Wilderness Area. The South Fork of the Salmon River from the Salmon River confluence to the Cecilville Bridge. The Scott River from its confluence with the Klamath to its confluence with Schackleford Creek. All of Wooley Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 11.7 miles; Scenic &#8212; 23.5 miles; Recreational &#8212; 250.8; Total &#8212; 286.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/klamath-ca.jpg" alt="Klamath River, California" title="Klamath River, California" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/srnf/recreation/wateractivities/recarea/?recid=11584&actid=79" alt="Klamath River (U.S. Forest Service)" target="_blank">Klamath River (U.S. Forest Service)</a><br />
<a href="http://www.fs.usda.gov/recarea/srnf/recreation/recarea/?recid=59663" alt="Salmon River (U.S. Forest Service)" target="_blank">Salmon River (U.S. Forest Service)</a><br />
<a href="http://www.fs.usda.gov/recarea/srnf/recreation/wateractivities/recarea/?recid=11519&actid=79" alt="Trinity River (U.S. Forest Service)" target="_blank">Trinity River (U.S. Forest Service)</a><br />
<a href="../documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Klamath River Eligibility Report" target="_blank">Klamath River Eligibility Report</a><br />
<a href="../documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Klamath River Environmental Impact Statement" target="_blank">Klamath River Environmental Impact Statement</a></p>

<div id="photo-credit">
<p>Photo Credit: Dave Payne</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Klamath River (California)</h2>

<p>The designated California segment of the Klamath River begins 3,600 feet below Iron Gate Dam and ends at the Pacific Ocean; the designation includes its principal tributaries-the Scott River, Salmon River and Wooley Creek.</p>

<p>Klamath tributaries flow from Mount Shasta, Marble Mountain and the Siskiyou and Trinity Alps Wilderness Areas. The U.S. Forest Service and Bureau of Land Management manage the upper 127 miles of the Klamath, and the state of California, with support from the National Park Service and Native American tribes, manage the remainder. Tributaries are administered by the U.S. Forest Service, except one short segment of the Scott. The Klamath River is well-known for its beautiful scenery, and paddlers are attracted to its Class II to Class V rapids. The river is also an important wildlife habitat corridor, known for its raptors.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The river supports several anadromous species during most of their in-river life stages, including chinook salmon (spring and fall runs), coho salmon, steelhead trout (summer and winter runs), coastal cutthroat trout, green and white sturgeon, and Pacific lamprey. The evolutionarily significant unit of coho salmon, the Southern Oregon/Northern California Coast coho, is federally listed as a threatened species under the Endangered Species Act, and the Klamath River is designated critical habitat. The anadromous fishery supports the river's sport fishing guide and resort industry, Native American subsistence and ceremonial culture and the ocean commercial and sport fishing industry. Visitors also enjoy viewing salmonids, especially during migration seasons.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>