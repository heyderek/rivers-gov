<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'North Sylamore Creek, Arkansas';

// Set the page keywords
$page_keywords = 'Ozark National Forest, St. Francis National Forest, North Sylamore Creek, Blanchard Caverns, Blanchard Springs Recreation Complex, Arkansas';

// Set the page description
$page_description = 'North Sylamore Creek, Arkansas';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('140');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ozark-St. Francis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>April 22, 1992. From the Clifty Canyon Botanical Area boundary to the confluence with the White River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 14.5 miles; Total &#8212; 14.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/north-sylamore-creek.jpg" alt="North Sylamore Creek" title="North Sylamore Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/north-sylamore-creek-plan.pdf" alt="North Sylamore Creek Management Plan (1.5 MB PDF)" target="_blank">North Sylamore Creek Management Plan (1.5 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Robert Duggan/USFS</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>North Sylamore Creek</h2>
<p>North Sylamore Creek is located in Stone County in north-central Arkansas. It flows through the Blanchard Springs Recreation Complex, which provides camping, swimming and hiking, and includes the famous Blanchard Caverns. North Sylamore Creek is a very productive smallmouth bass fishery and supports a high diversity of fish species. Endangered species of bats utilize the stream corridor for foraging.</p>
<p>Several plant species listed as sensitive by the Arkansas Natural Heritage Commission are located along the stream corridor, with the largest concentrations being within Clifty Canyon Special Interest Area, which lies just north of the river corridor.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>