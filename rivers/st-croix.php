<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'St. Croix River, Minnesota, Wisconsin';

// Set the page keywords
$page_keywords = 'St. Croix National Scenic River, National Park Service, St. Croix River, Namekagon River, Minnesota, Wisconsin';

// Set the page description
$page_description = 'St. Croix River, Minnesota, Wisconsin';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('6','9','SD5');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, St. Croix National Scenic Riverway<br />
Minnesota Department of Natural Resources<br />
Wisconsin Department of Natural Resources</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968: The segment between the dam near Taylor Falls, Minnesota, and the dam near Gordon, Wisconsin. The Namekagon River from Lake Namekagon downstream to its confluence with the St. Croix River. October 25, 1972: The segment from the dam near Taylors Falls, Minnesota, downstream 27 miles. June 17, 1976: The segment from the confluence with the Mississippi River upstream 25 miles.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>October 2, 1968: Scenic &#8212; 181.0 miles; Recreational &#8212; 19.0; Total &#8212; 200.0 miles. October 25, 1972: Scenic &#8212; 12.0 miles; Recreational &#8212; 15.0; Total &#8212; 27.0 miles. June 17, 1976: Recreational &#8212; 25.0; Total &#8212; 25.0 miles. Aggregate Totals: Scenic &#8212; 193.0 miles; Recreational &#8212; 59.0 miles; Total &#8212; 252.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/st-croix.jpg" alt="St. Croix River" title="St. Croix River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/sacn/" alt="St. Croix National Scenic Riverway (National Park Service)" target="_blank">St. Croix National Scenic Riverway (National Park Service)</a></p>
<p><a href="http://www.dnr.state.mn.us/waters/watermgmt_section/wild_scenic/wsrivers/rivers.html" alt="Minnesota's Wild &amp; Scenic Rivers (Minnesota Dept. of Natural Resources)" target="_blank">Minnesota's Wild &amp; Scenic Rivers (Minnesota Dept. of Natural Resources)</a></p>
<p><a href="http://dnr.wi.gov/wnrmag/html/stories/2008/jun08/rivers.htm" alt="St. Croix River (Wisconsin Natural Resources Magazine)" target="_blank">St. Croix River (Wisconsin Natural Resources Magazine)</a></p>
<p><a href="../documents/plans/lower-st-croix-plan.pdf" target="_blank">Lower St. Croix River Management Plan (5.2MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>St. Croix River</h2>
<p>In 1968, 200 miles of the St. Croix National Scenic Riverway, which includes its major tributary the Namekagon, was established as one of the original eight rivers under the Wild and Scenic Rivers Act. In 1972, an additional 27 miles of the Lower St. Croix River was the first riverway segment added to the National Wild and Scenic Rivers System by Congress since its inception in 1968. This segment flows along the border of Minnesota and Wisconsin, from Taylor's Falls Dam downstream for 27 miles. This legislation also directed the Secretary of the Interior to add the next 25 miles down to the confluence with the Mississippi River as a state-administered river following application by the Governors of Minnesota and Wisconsin (under Section 2(a)(ii) of the Wild and Scenic Rivers Act). This approval was given on June 17, 1976.</p>
<p>The St. Croix and Namekagon Rivers offer clean water gliding or rushing past a lush green landscape, with glimpses of a human presence. Choose to canoe and camp amid the northwoods, or boat and fish surrounded by wooded bluffs and historic towns. This river corridor provides bountiful scenic views and a haven for wildlife near a major metropolitan area.</p>
<p>The St. Croix River offers outdoor enthusiasts a chance to enjoy a wilderness-like experience and a variety of outdoor recreation opportunities within easy reach of a major metropolitan area. On the upper portion of the St. Croix and Namekagon Rivers, Class I-II rapids challenge the canoeist. The Lower St. Croix is popular for recreational enthusiasts, who enjoy canoeing, boating, fishing, rock climbing and hiking along its scenic shores. At the very lowest end, where the river widens as Lake St. Croix, power and sail boating are popular. Anglers, campers, picnickers, swimmers and birdwatchers enjoy its variety of scenes throughout.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>