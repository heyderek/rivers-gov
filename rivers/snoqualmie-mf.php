<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Snoqualmie River (Middle Fork), Washington';

// Set the page keywords
$page_keywords = 'Snoqualmie River, Washington, Mt. Baker-Snoqualmie National Forest';

// Set the page description
$page_description = 'Snoqualmie River (Middle Fork), Washington';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('209');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Baker-Snoqualmie National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 19, 2014. The 27.4-mile segment from the headwaters of the Middle Fork Snoqualmie River near La Bohn Gap in NE 1/4, Section 20, Township 24 North, Range 13 East, to the northern boundary of Section 11, Township 23 North, Range 9 East.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 6.4 miles; Scenic &#8212; 21.0 miles; Total &#8212; 27.4 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/snoqualmie-mf.jpg" alt="Middle Fork of the Snoqualmie River" title="Middle Fork of the Snoqualmie River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/mbs/recreation/recarea/?recid=35190&actid=43" alt="Middle Fork of the Snoqualmie River (U.S. Forest Service)" target="_blank">Middle Fork of the Snoqualmie River (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: Philip Kincare</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Snoqualmie River (Middle Fork)</h2>
<p>Less than an hour’s drive from downtown Seattle, the Middle Fork Snoqualmie River is one of King County’s last truly wild places, with old-growth forests, alpine lakes and peaks, whitewater rivers, strong native trout runs and healthy populations of bear, elk and cougar. Readily accessible to a population of over three million people and treasured by hikers, mountain bikers, fishermen and whitewater enthusiasts, the Middle Fork Snoqualmie area is one of the most significant outdoor recreation destinations near Seattle. It is also an important source of pure water for the entire Snoqualmie River system. (Source: American Rivers)</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>