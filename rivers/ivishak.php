<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Ivishak River, Alaska';

// Set the page keywords
$page_keywords = 'Arctic Wildlife National Refuge, Ivishak River, Alaska, Sagavanirktok River, Philip Smith Mountains, Prudhoe Bay';

// Set the page description
$page_description = 'Ivishak River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('39');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Fish &amp; Wildlife Service, Arctic Wildlife National Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment from its source, including all headwaters and an unnamed tributary from Porcupine Lake, within the boundary of the Arctic National Wildlife Range.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 80.0 miles; Total &#8212; 80.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/ivishak.jpg" alt="Ivishak River" title="Ivishak River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://alaska.fws.gov/nwr/arctic/rivers.htm" alt="Ivishak River (U.S. Fish &amp; Wildlife Service)" target="_blank">Ivishak River (U.S. Fish &amp; Wildlife Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Sharon Seim</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Ivishak River</h2>
<p>The 95-mile long Ivishak River flows north, through the Philip Smith Mountains and the northern foothills of the Arctic National Wildlife Refuge, to join the Sagavanirktok River on the coastal plain south of Prudhoe Bay. Fed by glaciers in the headwaters area, the Ivishak develops an increasingly wide, braided floodplain typical of northern Alaska rivers. It winds past year-round flowing springs, ice fields and glaciers in hanging valleys. Birdlife most likely exceeds 100 species. The river is designated wild where it flows through the Arctic National Wildlife Refuge.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>