<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Powder River, Oregon';

// Set the page keywords
$page_keywords = 'Powder River, Oregon';

// Set the page description
$page_description = 'Powder River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('96');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Vale District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Thief Valley Dam to the Highway 203 Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 11.7 miles; Total &#8212; 11.7 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/powder.jpg" alt="Powder River" title="Powder River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/powder-plan-ea.pdf" title="Powder River Management Plan" target="_blank" alt="Powder River Management Plan &amp; Environmental Assessment (7.3 MB PDF)">Pine River Management Plan (7.3 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Powder River</h2>

<p>The Powder River is located in northeastern Oregon and is a tributary of the Snake River. It flows almost entirely within Baker County, but downstream of the city of North Powder, it becomes part of the border between Baker County and Union County. This 11.7-mile (18.8 km) stretch of the Powder River is classified as 'wild' and flows through a rugged canyon with geologic formations of spectacular beauty. The fishing is great, as is the hunting, although access is limited. Float boating does occur, but only in the early spring.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural &amp; Pre-Historic</em></strong></p>

<p>Long before the arrival of pioneers and settlers, the Cayuse, Umatilla and Nez Perce Indians utilized the hunting and fishing grounds along the length of the Powder River.  In this area are archaeological sites which contain important information about the use of local lowland areas during the prehistoric middle archaic period.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Powder River supports a number of species of fish, including rainbow trout (stocked and native), catfish, crappie, dace, redside shiner, brown bullhead and various species of suckers. The Powder is known regionally for its truly outstanding rainbow fisheries. It provides habitat for native spawning rainbows that can grow to record size.</p>

<p>The riparian zone and the diversity of life in the river, including plentiful level of insects, crustaceans and baitfish, is truly outstanding and the reason for the health of the trout population.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Power River corridor provides a wide variety of recreational opportunities, the primary activities being fishing; upland game and big game hunting; and geological, zoological and scenic sightseeing. Kayaking takes place on the Powder River only during the Spring runoff period and should be pursued only by highly skilled floaters.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Powder River corridor contains a diversity of landforms and vegetation that capture viewers' attention. It runs through a steep-walled canyon that is in some locations 500 feet deep, offering a truly remote and primitive feeling. The canyon is semi-desert, and the river provides a riparian contrast. The hillsides host bunchgrasses and sage, with a few riverside Ponderosa pines for diversity.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife species found within the Powder River corridor are mule deer, badgers, yellow-bellied marmots, river otters, chukars, golden eagles, prairie falcons, red-tailed hawks, American kestrals, western kingbirds and rattlesnakes. The river segment includes a portion of a crucial deer wintering range that is occupied by hundreds of mule deer. The steep cliffs provide nesting habitat for a high concentration of raptors, such as golden eagles, prairie falcons and red-tailed hawks. In addition, bald eagles are found on this river during the winter.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>