<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Roaring River, Oregon';

// Set the page keywords 
$page_keywords = 'Roaring River, Mt. Hood National Forest, Oregon';

// Set the page description
$page_description = 'Roaring River, Oregon';

// Set the region for Sidebar Images 
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
 ?>
 
<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>
			
									<div id="details-text">
									<h3>Managing Agency:</h3>
									<p>U.S. Forest Service, Mt. Hood National Forest</p>
									
									<br />
									<h3>Designated Reach:</h3>
									<p>October 28, 1988.  From its headwaters to the confluence with the Clackamas River.</p>
                                    
									<br />
									<h3>Classification/Mileage: </h3>
									<p>Wild &#8212; 13.5 miles; Recreational &#8212; 0.2 miles; Total &#8212; 13.7 miles.</p>

									</div>
								<div class="clear"></div>	<!-- Allows for content above to be flexible -->					
							</div><!--END #details-box -->
						
								<div id="photo-frame">
								<!-- Image height and width are also defined in style.css -->
                                							
									<!--<img src="images/rio-chama.jpg" alt="" width="265px" height="204px" />-->
                                    <p>We could use a photo. <br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />
                                    We would be happy to credit you as the photographer.</p>
								
								</div><!--END #photo-frame -->
								
								<div id="photo-details">
								
								<!--<h3>RELATED LINKS</h3>
								<p><a href="http://www.fs.usda.gov/detailfull/elyunque/home/?cid=fsbdev3_042977&width=full#wild_scenic" alt="Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)" target="_blank">Wild &amp; Scenic Rivers of Puerto Rico (U.S. Forest Service)</a></p>-->

								<div id="photo-credit">
								
                                	<!--<p>Photo Credit: Martha Moran</p>-->
								
								</div><!--END #photo-credit -->
								
								</div><!--END #photo-details -->
							<div class="clear"></div>	<!-- Allows for content above to be flexible -->
					</div> <!--END #rivers-box -->
					
				
					<div id="lower-content">
					
						<h2>Roaring River</h2>
						<p>The Roaring River's primitive character and remoteness are responsible for its outstandingly remarkable water quality, botany, fish, wildlife, recreation and scenic values.  The topography of the river's upper drainage is dominated by a relatively broad glacial valley with forested slopes and talus sweeping up to sharp ridges interspersed with small basins, many of which contain lakes.  The lower four miles of the river corridor is a narrow gorge with steep basalt cliffs and talus.  Elevations in the Roaring river drainage range from 996 feet at its confluence with the Clackamas River to over 4,500 feet along the upper ridges and basins which form the headwaters.  The total area of the Roaring River drainage is about 44 square miles.<br /><br />

The drainage is characterized by a variety of stream complexes including large log jams in the upper river, large and small landslides, and small waterfalls alternating with large pools.  Below its confluence with the South Fork Roaring River, the river is constricted between basalt cliffs and forms two large falls.  The lowest mile of river is relatively wide (30 to 40 feet) and straight.  Most of the drainage is covered with snowpack for five to seven months of the year.  At the high elevations, the snowpack melts off in late June and July, and the area generally remains snow-free until early November.<br /><br />

The corridor includes a diversity of plant communities that form a mosaic of riparian and upland species.  Rocky outcrops, talus habitat and the numerous braided streams and side channels support a unique combination of plant communities.  Most of the river corridor is flanked by old-growth stands.  The side slopes and ridges are comprised of a mosaic of different successional stages of coniferous forest, hardwood and shrub communities, rocky outcrops and talus habitats, and meadow communities.<br /><br />

The Roaring River supports populations of native cutthroat trout, late-run coho salmon and late-run winter steelhead.  All three of these populations are considered to be native to the river.  The cutthroat trout population is present in the mainstem and its major tributaries above the first falls at river mile 3.5 and is becoming increasingly scarce.  The remote and relatively pristine conditions of the Roaring River drainage are ideal for this population.  Excellent trout habitat exists along about 11 miles of the river.  Populations of late-run coho salmon and late-run winter steelhead are present only within the lower 3.5 miles below the falls.  The particular stock of late-run coho salmon, also common to other parts of the Clackamas River, is considered to be the last self-sustaining run of native coho salmon in the entire Columbia River Basin.<br /><br />

The Roaring River's mosaic of habitat resulting from previous wildfires, riparian areas, rocky outcrops, and talus slopes offers excellent wildlife habitat diversity. Riparian hardwood and shrub vegetation, various successional stages of conifer forest, old-growth Douglas-fir forest, shrub-dominated non-forested side slopes and upper elevation meadows typify the rich variety of wildlife habitat within the drainage.<br /><br />

The river's primitive environment and remoteness and its native cutthroat trout, coho salmon and steelhead provide a unique sport fishery and an undeveloped setting in which to hike and explore.  The unmodified scenery of the river corridor and surrounding ridges is unique in the region.  The sweeping vistas into the Roaring River's wild, unmodified valley are the most important characteristic of its scenic resources.
</p>
				
					</div> <!--END #lower-content -->
					
<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>