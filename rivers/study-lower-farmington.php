<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Farmington River, Connecticut';

// Set the page keywords
$page_keywords = 'Farmington River, Connecticut, National Park Service';

// Set the page description
$page_description = 'Farmington River, Connecticut';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('306');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<!--<?php
// includes ESRI
include ('../iframe.php');
?>-->

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<center><img src="images/lower-farmington-2.jpg" alt="Lower Farmington River" width="509" height="382" title="Lower Farmington River"/></center>
<h3>Study Agency:</h3>
<p>National Park Service</p>
<br />
<h3>Study Segments:</h3>
<p>March 3, 2009 (Public Law 111-11). The segment of the Farmington River downstream from the segment designated as a recreational river to its confluence with the Connecticut River and the segment of the Salmon Brook including its mainstream and east and west branches.</p>
<br />
<h3>Mileage:</h3>
<p>70.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/lower-farmington-1.jpg" alt="Lower Farmington River" title="Lower Farmington River" width="265" height="204" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.lowerfarmingtonriver.org" title="Lower Farmington River Study Site" target="_blank" alt="Lower Farmington River Study Site">Lower Farmington River Study Site</a></p>
<p><a href="../documents/studies/lower-farmington-study-ea.pdf" title="Lower Farmington River &amp; Salmon Brook Study" target="_blank" alt="Lower Farmington River &amp; Salmon Brook Study">Lower Farmington River &amp; Salmon Brook Study</a></p>
<p><a href="../documents/plans/lower-farmington-plan.pdf" title="Lower Farmington River Management Plan" alt="Lower Farmington River Management Plan" target="_blank">Lower Farmington River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Top - Tom Cameron; Bottom - Farmington River Watershed Association</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Farmington River</h2>
<p>The Farmington is the largest tributary to the Connecticut River, and Salmon Brook is the largest tributary to the Farmington. Due to its high-quality resources, Salmon Brook is arguably the most important tributary. Salmon Brook joins the Farmington River in the town of East Granby. The confluence of the Farmington and Connecticut Rivers is in the town of Windsor, which is 55 miles north of the Long Island Sound, 2.8 miles north of the city of Hartford, and 13.1 miles south of the Massachusetts state line. The Farmington River Valley is situated about halfway between New York and Boston. The towns of the wild and scenic study area include Avon, Bloomfield, Burlington, Canton, East Granby, Farmington, Granby, Hartland, Simsbury and Windsor.</p>
<p>Overall, the Lower Farmington River and Salmon Brook corridors are a remarkable combination of varied geology; healthy forested watershed; excellent fishing and paddling; well-kept walking and biking trails; diverse communities of plants, wildlife, fish and aquatic invertebrates; rich agricultural soils; archaeological sites; historic towns and landmarks; and striking scenic views. The watercourses are exceptional natural and cultural resources.</p>
<p>The Wild and Scenic River Study of the Lower Farmington River and Salmon Brook concludes that the lower Farmington River and Salmon Brook are eligible for designation into the National Wild and Scenic Rivers System based on their free-flowing condition and the presence of one or more "Outstandingly Remarkable Values (ORVs)," as defined by the Wild and Scenic Rivers Act. The ORVs are geology, water quality, biological diversity, cultural landscape and recreation.</p>
<p>The river segment that includes the Rainbow Dam and reservoir is found ineligible for designation.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>