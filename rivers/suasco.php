<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sudbury, Assabet &amp; Concord Rivers, Massachusetts';

// Set the page keywords
$page_keywords = 'Concord River, Sudbury River, Assabet River, Minute Man National Historical Park, Great Meadows National Wildlife Refuge, Old North Bridge, Shot Heard Round the World, Hawthorne, Emerson, Thoreau, Massachusetts';

// Set the page description
$page_description = 'Sudbury, Assabet &amp; Concord Rivers, Massachusetts';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('160');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Northeast Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>April 9, 1999. The 14.9-mile segment of the Sudbury River beginning at the Danforth Street Bridge in the town of Framingham, downstream to the Route 2 Bridge in Concord. The 1.7-mile segment of the Sudbury River from the Route 2 Bridge downstream to its confluence with the Assabet River at Egg Rock. The 4.4-mile segment of the Assabet River beginning 1,000 feet downstream from the Damon Mill Dam in the town of Concord, to its confluence with the Sudbury River at Egg Rock in Concord. The eight-mile segment of the Concord River from Egg Rock at the confluence of the Sudbury and Assabet Rivers downstream to the Route 3 Bridge in the town of Billerica.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 14.9 miles; Recreational &#8212; 14.1 miles; Total &#8212; 29.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/suasco.jpg" alt="Sudbury, Assabet and Concord Rivers" title="Sudbury, Assabet and Concord Rivers" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/suasco_pwsr_sub.html" alt="Sudbury, Assabet &amp; Concord Rivers (National Park Service)" target="_blank">Sudbury, Assabet &amp; Concord Rivers (National Park Service)</a></p>
<p><a href="http://www.sudbury-assabet-concord.org" alt="Sudbury, Assabet &amp; Concord Wild &amp; Scenic River Stewardship Council" target="_blank">Sudbury, Assabet &amp; Concord Wild &amp; Scenic River Stewardship Council</a></p>
<p><a href="http://www.nps.gov/mima/" alt="Minute Man National Historical Park (National Park Service)" target="_blank">Minute Man National Historical Park (National Park Service)</a></p>
<p><a href="../documents/plans/suasco-river-conservation-plan.pdf" alt="Sudbury, Assabet &amp; Concord Rivers Conservation Plan (1.3 MB PDF)" target="_blank">Sudbury, Assabet &amp; Concord Rivers Conservation Plan (1.3 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Glenn Rigby, American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sudbury, Assabet, Concord Rivers</h2>
<p>Located 25 miles west of Boston, these rivers are recognized for their outstanding ecology, history, scenery, recreation values, and place in American literature. Portions of the segments lie within Great Meadows National Wildlife Refuge, and among the many historic sites along the rivers is North Bridge at Minute Man National Historical Park in Concord, site of the revolutionary "shot heard 'round the world." The rivers are managed by the National Park Service in cooperation with the SuAsCo River Stewardship Council, which includes members from local towns, state government, and advocacy groups.</p>
<p><b>From President Clinton's April 9, 1999 signing statement (slightly edited):</p>
<p>"The addition of these rivers to the National System recognizes their outstanding ecology, history, scenery, recreation values, and place in American literature. Located about 25 miles west of Boston, the rivers are remarkably undeveloped and provide recreational opportunities in a natural setting to several million people living in the greater Boston metropolitan area. Ten of the river miles lie within the boundary of the Great Meadows National Wildlife Refuge, established to protect the outstanding waterfowl habitat associated with extensive riparian wetlands. Historic sites of national importance, including many in the Minute Man National Historical Park, are located near the rivers in the Town of Concord. Among these is Old North Bridge, site of the revolutionary "Shot Heard 'Round the World."  The rivers are featured prominently in the works of nineteenth century authors Hawthorne, Emerson, and Thoreau and have been the subject of ornithological studies since early days of field observation techniques."</p>
<p>"Important to the designation of these rivers and their long-term protection is the strong local support and commitment for preservation as expressed by the communities along the river segments. Each of the eight towns along the river segments held Town Meetings regarding the designation of these river segments. Votes at these meetings in support of designation and endorsement of the Sudbury, Assabet and Concord River Conservation Plan were unanimous among the eight towns. The Conservation Plan relies on local and private initiatives to protect the river segments through local zoning and land use controls. The SuAsCo River Stewardship Council will have the primary responsibility for implementation of the conservation plan."</p>
<p>"A more lovely stream than this, for a mile above its junction with the Concord,<br />
has never flowed on earth."<br />
<i>&#8211; Nathaniel Hawthorne, Referring to the Assabet</i></p>
<p>"By the rude bridge that arched the flood,<br />
Their flag to April's breeze unfurled,<br />
Here once the embattled farmers stood,<br />
And fired the shot heard round the world."<br />
<i>&#8211; Ralph Waldo Emerson, "Concord Hymn,"<br />
About the Old North Bridge Over the Concord River</i></p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>