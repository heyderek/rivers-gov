<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'North Umpqua River, Oregon';

// Set the page keywords
$page_keywords = 'North Umpqua River, steelhead, flyfishing, Steamboat Inn, Zane Grey, Umpqua National Forest, Oregon';

// Set the page description
$page_description = 'North Umpqua River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('95');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>
<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Roseburg District<br />
U.S. Forest Service, Umpqua National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Soda Springs Powerhouse to the confluence with Rock Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 33.8 miles; Total &#8212; 33.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/north-umpqua.jpg" alt="North Umpqua River" title="North Umpqua River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/visit/north-umpqua-wild-scenic-river" alt="North Umpqua River (Bureau of Land Management)" target="_blank">North Umpqua River (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/north-umpqua-plan.pdf" title="North Umpqua River Management Plan" target="_blank">North Umpqua River Management Plan</a></p>
<p><a href="http://www.blm.gov/or/districts/roseburg/recreation/wild_and_scenic_river/" alt="North Umpqua River Users Guide" target="_blank">North Umpqua River Users Guide</a></p>
<p><a href="http://www.blm.gov/or/districts/roseburg/recreation/umpquatrails/" alt="North Umpqua River Trails" target="_blank">North Umpqua River Trails</a></p>
<p><a href="http://www.fs.usda.gov/recarea/umpqua/recreation/wateractivities/recarea/?recid=63394&actid=79" title="Boating the North Umpqua River" target="_blank">Boating the North Umpqua River (U.S. Forest Service</a></p>
<p><a href="http://www.fs.usda.gov/recarea/umpqua/recreation/fishing/recarea/?recid=75133&actid=43" title="Fishing the North Umpqua River" target="_blank">Fishing the North Umpqua River (U.S. Forest Service</a></p>

<div id="photo-credit">
<p>Photo Credit: Ron Murphy</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>North Umpqua River</h2>

<p>The North Umpqua, a tributary of the Umpqua River that drains a scenic and rugged area of the Cascade Range south of Eugene, is one of Oregon's most beautiful rivers. Flyfishing, whitewater boating, camping and scenic driving are premier recreation activities. The clear water, large Douglas-fir stands and geologic formations add to the spectacular scenery. The river is known for a variety of resident and anadromous fish species, including summer and winter steelhead, fall and spring chinook salmon, coho salmon and sea-run cutthroat trout.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>Radiocarbon dating places prehistoric occupation of the North Umpqua River as early as 6,300 years ago, and the presence of the time-sensitive artifacts indicates the occupation may go as far back as 8,000 years. This long period of occupation resulted in the formation of a number of sites within the river corridor that have unusual characteristics when compared with other sites in the region.</p>

<p>Visitors were initially drawn to the Steamboat area within the North Umpqua River corridor because of the excellent fishing. The first known fish camp constructed on the river in the 1920s was located in this vicinity. Also found in this area is Mott Bridge, an Oregon Historical Civil Engineering Landmark. Constructed by the Civilian Conservation Corps in 1935-36, it may be the only surviving example of three such structures from that time in the Pacific Northwest.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The combination of large summer run steelhead, fly-fishing only restrictions and majestic scenery has drawn anglers from all over the world. The river serves as needed habitat for a variety of resident and anadromous fish species, including summer and winter steelhead, fall and spring chinook, coho sea-run and cutthroat trout, and is distinguished from other rivers by the large and consistent numbers of native (non-hatchery) fish in the run. The North Umpqua summer steelhead fishery is considered to be one of the most outstanding on the West Coast.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Renowned for outstanding salmon and steelhead fishing and exhilarating whitewater, the North Umpqua provides river running opportunities for paddlers of all skill levels, from placid waters to roaring rapids. The best months for enjoying whitewater are May, June and early July. Almost 34 miles of the North Umpqua have been designated as a wild and scenic river and this section has been set aside for fly-only fishing. The 79-mile long North Umpqua Trail parallels the river and offers visitors challenging hiking and mountain biking experiences.</p>

<p>The area is readily accessible to a broad segment of the population and provides a variety of river-related recreation, such as boating activities, fly angling, developed and dispersed camping, day use picnicking and swimming, horseback riding, hiking, sightseeing, bicycling, photography, nature study and scenic driving. Visitors from over the world travel through the North Umpqua River corridor to enjoy these major destination attractions.</p>

<p><strong><em>Scenic</em></strong></p>

<p>This truly distinctive canyon landscape is generally characterized by a combination of jade green rushing water, vertical rock cliffs and spires within a mosaic of mountain meadows and hemlock forests. Adding to the natural scenic quality of the North Umpqua River corridor are the locations of numerous prominent geologic features of columnar basalt, large basalt rock boulders and spires which are currently managed as the Rocks Geologic Area. Few river systems in the region expose as much of the volcanic and geologic history of the formation of the Cascades in one straight, east-west direction.</p>

<p>Highway 138 through the corridor provides access to Diamond Lake Recreation Area and Crater Lake National Park. This highway has received both national and regional recognition for its exceptional scenic quality and accessibility to a myriad of recreational and interpretive opportunities.</p>

<p><strong><em>Water Quality</em></strong></p>

<p>The North Umpqua River sustains a dependable water flow of both high quality and quantity. Components that combine to produce the high water quality are low turbidity (except during peak flow periods), low levels of contaminants and pollutants, cool water temperatures and stable minimum instream flows. The water quality and quantity of the North Umpqua River is the foundation for the other outstandingly remarkable values, providing a steady flow for both recreational uses and the maintenance of fish and aquatic life.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>