<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Clackamas River, Oregon';

// Set the page keywords
$page_keywords = 'Mt. Hood National Forest, Clackamas River, Oregon';

// Set the page description
$page_description = 'Clackamas River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('70');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Big Springs to Big Cliff.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 20.0 miles; Recreational &#8212; 27.0 miles; Total &#8212; 47.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/clackamas.jpg" alt="Clackamas River" title="Clackamas River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.fs.usda.gov/attmain/mthood/specialplaces" alt="Clackamas River (U.S. Forest Service)" target="_blank">Clackamas River (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/clackamas-plan-ea.pdf" alt="Clackamas River Management Plan Environmental Assessment (7.3 MB PDF)" target="_blank">Clackamas River Management Plan Environmental Assessment (7.3 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Clackamas River</h2>

<p>The Clackamas River is located to the west of the Cascade Range and to the south of the Columbia River Gorge in northern Oregon. Flowing northwest from its sources high in the Cascade Mountains, the designated portion of the river, which is 47 miles (75.6 km) in length, runs from Big Spring (headwaters area) to Big Cliff, just south of the town of Estacada. This most picturesque region is entirely within the Mt. Hood National Forest and encompasses forested lands, wetlands, riparian areas and rock cliffs.</p>

<p>Streams and rivers in the Clackamas drainage have been subject to hydroelectric development, with power generation facilities located on the Oak Grove Fork and on the mainstem in the designated corridor. The hydro-power facilities in the designated corridor actually discharge water diverted from the Oak Grove Fork farther upstream back into the Clackamas. Several dams are situated below the designated section between Big Cliff and Oregon City.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic &amp; Ecologic</em></strong></p>

<p>Before 1800, coniferous forests covered most of the watershed, and its streams supported big populations of salmon, steelhead and other fish. The Big Bottom area represents forest vegetation that was once common along Cascades-region rivers, including old-growth Douglas-fir forests that are still present along the banks.</p>

<p><strong><em>Cultural &amp; Historic</em></strong></p>

<p>There is evidence that the Clackamas River had significant human use prior to European exploration and settlement, as well as a variety of evidence of historic use. The Clackamas has played an important role in local and regional development.</p>

<p>Native Americans hunted, fished and gathered food and materials in the Clackamas River drainage as early as 10,000 years ago. By 2,000 to 3,000 years ago, they had established permanent settlements along the river's lower floodplain.</p>

<p>In the 1800s, Congress granted parcels of land to the Oregon and California (O & C) Railroad for sale to settlers. In 1917, when the railroad company failed to meet the grant's terms, Congress reclaimed these lands, and they returned to federal ownership as public lands.</p>

<p>The river's steep southern bank &#8212; rising to more than 1,400 feet &#8212; shows evidence of logging, authorized in 1937 when Congress directed that the O & C lands be managed for permanent forest production under the principles of sustained yield management.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Clackamas River is home to the last significant run of wild late-winter coho salmon in the Columbia Basin, which generally spawn on the main stem of the Clackamas above the North Fork Reservoir. The watershed also has one of only two remaining runs of spring chinook in the Willamette basin and supports a significant population of winter steelhead, cutthroat trout and native lamprey.</p>

<p><strong><em>Recreational</em></strong></p>

<p>This section of the Clackamas River, convenient to Portland, offers breathtaking mountain views, replete with native wildflowers and wildlife, as well as direct access for fishing, boating and hiking. The area is most recognizable by the green Pratt truss bridge at Memaloose Road, now closed to vehicular traffic, which sits atop the river and connects walkers to the day-use area on the river's south side.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Clackamas River provides habitat for the bald eagle and the threatened northern spotted owl, and it is potential habitat for the peregrine falcon.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>