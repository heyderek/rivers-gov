<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Fossil Creek, Arizona';

// Set the page keywords
$page_keywords = 'Fossil Creek, Arizona';

// Set the page description
$page_description = 'Fossil Creek, Arizona';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';

//ID for the rivers
$river_id = array('205');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Coconino National Forest<br />U.S. Forest Service, Tonto National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From the confluence of Sand Rock and Calf Pen Canyons to its confluence with the Verde River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 9.3 miles; Recreational &#8212; 7.5 miles; Total &#8212; 16.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/fossil-creek.jpg" alt="Fossil Creek" title="Fossil Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/coconino/recarea/?recid=75356" alt="Fossil Creek" target="_blank">Fossil Creek Recreation Area (U.S. Forest Service)</a></p><p><a href="../documents/plans/fossil-creek-resource-assessment.pdf" alt="Fossil Creek Resource Assessment (1.5 MB PDF)" target="_blank">Fossil Creek Resource Assessment (1.5 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Andrew Fahlund, American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Fossil Creek</h2>

<p>Fossil Creek has been described as the most diverse riparian area in Arizona. Over 30 species of trees and shrubs and over a hundred species of birds have been observed in this unique habitat. The stream seems to appear out of nowhere, originating from a collection of artesian springs that produce a constant, year-round flow of about 43 cubic feet per second. In a state where water is scarce, the magnitude and constant water flow of this perennial stream is rare.</p>
<p>For most of the last century, the stream was used for hydropower production through a series of diversion flumes that carried the majority of the flow from the springs to two power plants. Constructing this hydropower system in the early 1900's was a great engineering feat.</p>

<p>In 2005, the hydropower project was decommissioned and flow was restored to the creek. In coordination with the decommissioning, a fish barrier was built, non-native fish were removed, and native fish were restored to Fossil Creek. It now is one of only a few streams in Arizona with only native fish and with ten federally listed or sensitive fish species either present or provided suitable habitat.</p>
<p>The water of Fossil Creek is also rare also because, as it comes out of the ground, it is supersaturated with calcium carbonate and forms travertine as it flows. In all of North America, there are only three travertine systems larger (with respect to water discharge and mineral deposition potential) than Fossil Creek, all of which are partially or wholly contained within National Parks. The travertine creates terraces, steep waterfalls and large pools and encases whatever happens to fall into the streambed, forming the fossils for which the area is named.</p>

<p>Fossil Creek provides outstanding opportunities for a variety of recreational activities and it attracts numerous visitors especially to the deep, clear pools in which to wade and swim. The constant water flow is welcome relief from the intense heat of the desert southwest. Calcium carbonate dissolved in the water gives it a beautiful blue-green color, which adds to the enjoyment of the area.</p>
<p>Because of the year-round constant flow of water, Fossil Creek has been used for millennia and it has exceptional cultural value to various Apache and Yavapai groups.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>
