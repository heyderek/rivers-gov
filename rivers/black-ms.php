<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Black Creek, Mississippi';

// Set the page keywords
$page_keywords = 'DeSoto National Forest, Black Creek, Mississippi';

// Set the page description
$page_description = 'Black Creek, Mississippi';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('59');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, DeSoto National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 30, 1986. The segment from Fairley Bridge Landing upstream to Moody's Landing.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 21.0 miles; Total &#8212; 21.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/black-creek.jpg" alt="Black Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://prdp2fs.ess.usda.gov/wps/portal/fsinternet/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gDfxMDT8MwRydLA1cj72BTJw8jAwgAykeaxcN4jhYG_h4eYX5hPgYwefy6w0H24dcPNgEHcDTQ9_PIz03VL8iNMMgycVQEAHcGOlk!/dl3/d3/L2dJQSEvUUt3QS9ZQnZ3LzZfME80MEkxVkFCOTBFMktTNUJIMjAwMDAwMDA!/?ss=110807&navtype=BROWSEBYSUBJECT&cid=FSE_003741&navid=110000000000000&pnavid=null&position=BROWSEBYSUBJECT&recid=28841&ttype=recarea&name=Black%20Creek&pname=National%20Forests%20In%20Mississippi%20-%20Black%20Creek" alt="Black Creek (U.S. Forest Service)" target="_blank">Black Creek (U.S. Forest Service)</a></p>

<div id="photo-credit">
<p>Photo Credit: DeSoto National Forest</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Black Creek</h2>
<p>This scenic southern river features deep, black water, colorful vertical bluffs and contrasting white sand bars as it follows a meandering course through Mississippi's coastal plain. Plants flourish and wildlife abounds. Trees and flowering shrubs overhang the banks. Wood ducks and otters are often seen by those visiting the stream. The river and area provide a variety of opportunities for backpacking and fishing and six launch points provide for canoeing. Primitive camping is allowed all along the stream within the National Forest, and a number of trails can be accessed near the river.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>