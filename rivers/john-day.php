<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'John Day River, Oregon';

// Set the page keywords
$page_keywords = 'John Day River, Oregon';

// Set the page description
$page_description = 'John Day River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('79');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Prineville District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Service Creek to Tumwater Falls.</p>
<br />
<h3>Classification/Mileage: </h3>
<p>Recreational &#8212; 147.5 miles; Total &#8212; 147.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/john-day.jpg" alt="John Day River" title="John Day River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/or/permit/" alt="John Day River (Bureau of Land Management)" target="_blank">John Day River (Bureau of Land Management)</a><br />
<a href="../documents/plans/john-day-plan-ea.pdf" title="John Day River Management Plan &amp; Environmental Assessment" target="_blank">John Day River Management Plan &amp; Environmental Assessment</a></p>

<div id="photo-credit">
<p>Photo Credit: American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>John Day River</h2>

<p>The John Day River is the longest undammed river in Oregon. Located in eastern Oregon, the section from Service Creek to Tumwater Falls flows through a number of colorful canyons broad valleys, and breathtaking terrain. This segment offers exceptional anadromous steelhead and warm-water bass fishing; calm water boating punctuated with a few rapids; and locations of archeological, historical and paleontological interest.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic &amp; Ecologic</em></strong></p>

<p>The riparian zone offers lush, green vegetation important to wildlife and hydrologic processes. The adjacent dry, steep, rocky hillsides are dominated by vast acreage of bluebunch wheatgrass. Several plant species have evolved to thrive in the clays of varying colors and textures, resulting from volcanic activity, and 16 special status species have been seen or suspected in the corridor.</p>

<p><strong><em>Cultural, Pre-Historic &amp; Archeologic</em></strong></p>

<p>Evidence supports the presence of humans as far back as 8,000 years and nearly one hundred prehistoric sites include pithouse villages, rock shelters, pictograph sites, petroglyphs, rock feature sites and tool manufacturing sites. The area has been used traditionally by the Tanino Group of Sahaptian speakers and the Northern Paiute.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The John Day hosts one of the few remaining wild fish runs in the Pacific Northwest; the summer steelhead and spring Chinook salmon returning for spawning contribute to the largest entirely wild run in the mid and upper Columbia River Basin. In addition to anadromous fisheries, this river section contains prime habitat for smallmouth bass. Also present are rainbow trout, Pacific lamprey, bridgelip sucker and speckled and longnose dace.</p>

<p><strong><em>Geologic &amp; Paleontologic</em></strong></p>

<p>A complicated geologic history has resulted in a diverse assemblage of rocks exposed at the earth's surface. These rocks include masses of oceanic crust, marine sediments, intrusive bodies, a wide variety of volcanic materials, ancient river and lake deposits, and recent river and landslide deposits. The Clarno, John Day and Mascall Formations are famous for plant and invertebrate fossils of international significance.</p>

<p><strong><em>Historic</em></strong></p>

<p>The John Day River and its corridor played an important role during pioneer migration and settlement of the west, with a few sites significant enough to make them eligible for the National Register of Historic Places. The Oregon Trail, a significant westerly route for homesteaders and adventurers, crossed the John Day River at McDonald Ford. Thousands of emigrants crossed here between the 1840s and 1860s.  Twenty-six documented historic sites represent settlement associated with livestock grazing and transportation-related features.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Recreational activities most commonly pursued on and along the river are hunting, fishing and whitewater boating. Fall and winter are the seasons for hunting waterfowl, upland birds and deer. The area is noted for excellent bass and steelhead fishing. Slow water floating with a few challenging rapids are appealing for a wide spectrum of visitors. Camping, picnicking, sightseeing, photography, swimming and wildlife viewing are also popular, and snowmobiling and skiing are options during the winter months.</p>

<p><strong><em>Scenic</em></strong></p>

<p>This section flows through undeveloped land, and areas of high plateaus bisected by the river and its tributaries.  The surroundings include farm valleys, majestic basalt cliffs that reach over 1,000 feet and steeply sloped hills covered with grass and sagebrush. The unique contrast between the riparian and high desert upland vegetation provides a unique and spectacular backdrop for the unusually varied viewable geology and abundant wildlife.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The variety of fish and wildlife species in the John Day Basin may be more diverse than any other river system in the state due to the diversity of habitats. Notable species include bald and golden eagles, peregrine falcons, red-tailed hawks and ospreys. Herons, Canada geese and a variety of ducks live here, and several species of warbler, vireos and swallows migrate into the John Day Basin to nest. Mule deer are present year-round. Rocky Mountain elk populations have been restored, and California bighorn sheep are becoming re-established. Other common animals are minks, coyotes, bobcats, beavers, western fence lizards, Pacific treefrogs and rattlesnakes.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>