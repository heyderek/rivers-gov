<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Pine River, Michigan';

// Set the page keywords
$page_keywords = 'Huron-Manistee National Forest, Pine River, Michigan';

// Set the page description
$page_description = 'Pine River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('126');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Huron-Manistee National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. The segment from Lincoln Bridge to the east 1/16th line of Section 16, T21N, R13W.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 26.0 miles; Total &#8212; 26.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/pine.jpg" alt="Pine River, Michigan" title="Pine River, Michigan" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recmain/hmnf/recreation/" alt="Huron-Manistee National Forest" target="_blank">Huron-Manistee National Forest</a></p>
<p><a href="../documents/plans/pine-plan.pdf" alt="Pine River Management Plan (7.3 MB PDF)" target="_blank">Pine River Management Plan (7.3 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Pine River</h2>
<p>The Pine River is located in the Lower Peninsula of Michigan near the town of Cadillac and offers small-craft boating, high-quality fishing and outstanding scenery. The river is well-known for boating (primarily canoeing) and trout fishing. However, several other recreational activities are popular, including picnicking, hunting, wildlife viewing and swimming. Trout fishing is a popular activity during the early morning and evening throughout the summer. The majority of anglers fish from shore, but access by boat is also common. Numerous user-developed foot trails to popular fishing areas are located along the shoreline.</p>
<p>The U.S. Forest Service has established a watercraft permit system that restricts watercraft use to 616 watercraft/day on weekdays and 240 watercraft on weekends from May 15 through September 10th. Watercraft permits are available at local U.S. Forest Service offices and private liveries. The developed recreation sites along the Pine River require a vehicle parking pass under the Recreation Enhancement Act.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>