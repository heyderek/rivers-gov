<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Niobrara River, Nebraska';

// Set the page keywords
$page_keywords = 'Niobrara National Scenic River, National Park Service, Niobrara River, Fort Niobrara National Wildlife Refuge, Nebraska';

// Set the page description
$page_description = 'Niobrara River, Nebraska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('117');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Niobrara National Scenic River<br / >
Fort Niobrara National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>May 24, 1991. From Borman Bridge to State Highway 137. From the western boundary of Knox County to its confluence with the Missouri River. Verdigre Creek from its confluence with the Niobrara to the north boundary of the town of Verdigre.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 76.0 miles; Recreational &#8212; 28.0 miles; Total &#8212; 104.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/niobrara.jpg" alt="Niobrara River" title="Niobrara River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/niob/" alt="Niobrara National Scenic River (National Park Service)" target="_blank">Niobrara National Scenic River (National Park Service)</a></p>
<p><a href="../documents/plans/niobrara-plan.pdf" title="Niobrara River Management Plan" target="_blank">Niobrara River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Stuart Schneider</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Niobrara River</h2>
<p>Perhaps the epitome of a prairie river, the Niobrara is known as a biological crossroads. Although passing primarily through private land, it also flows through the Fort Niobrara National Wildlife Refuge and the largest single holding of The Nature Conservancy where bison have been reintroduced. The upper portion provides excellent canoeing.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>