<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Salmon River, Idaho';

// Set the page keywords
$page_keywords = 'River of No Return, Salmon River, Sawtooth Mountains, Frank Church Wilderness, Frank Church, Gospel-Hump Wilderness, Idaho';

// Set the page description
$page_description = 'Salmon River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('24');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Salmon-Challis National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>July 23, 1980. The segment of the main stem from the mouth of the North Fork of the Salmon River downstream to Long Tom Bar.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 79.0 miles; Recreational &#8212; 46.0 miles; Total &#8212; 125.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/salmon-id.jpg" alt="Salmon River, Idaho" title="Salmon River, Idaho" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/attmain/nezperce/specialplaces" alt="Salmon River (U.S. Forest Service)" target="_blank">Salmon River (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/attmain/scnf/specialplaces" alt="Salmon River (U.S. Forest Service)" target="_blank">Salmon River (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/scnf/specialplaces/?cid=stelprdb5360033" alt="Frank Church River of No Return Wilderness (U.S. Forest Service)" target="_blank">Frank Church River of No Return Wilderness (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/scnf/passes-permits/recreation/?cid=fsbdev3_029568/" alt="Salmon River Lottery" target="_blank">Salmon River Lottery</a></p>
<p><a href="../documents/plans/salmon-middle-salmon-plan.pdf" title="Salmon &amp; Middle Fork Salmon River &amp; Wilderness Management Plan" target="_blank">Salmon &amp; Middle Fork Salmon River &amp; Wilderness Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Paul Whitwell</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Salmon River (Idaho)</h2>

<p>Known as "The River of No Return," the Salmon River originates in the Sawtooth and Lemhi Valleys of central and eastern Idaho; snows from the Sawtooth and Salmon River Mountains in the south and the Clearwater and Bitterroot Mountains in the north feed this river. The upper section passes through the Frank Church River of No Return Wilderness, while the lower section forms the southern boundary of the Gospel-Hump Wilderness. In recognition of the river's many outstanding values, including scenery, recreation, geology, fish, wildlife, water quality, botany, prehistory, history and cultural traditional use, Congress designated 46 miles of the river, from North Fork to Corn Creek, as a recreational river and 79 miles, from Corn Creek to Long Tom Bar, as a wild river.</p>

<p>The Salmon flows through a vast wilderness in one of the deepest gorges on the continent. Its granite-walled canyon is one-fifth of a mile deeper than the Grand Canyon, and, for approximately 180 miles, the Salmon Canyon is more than one mile deep.</p>

<p>From North Fork to Corn Creek, the spectacular canyon of the Salmon River has exposed some of the oldest known rocks in the state of Idaho. In the vicinity of Shoup, these rocks, called gneiss, have been dated as 1.5 billion years old. From Corn Creek to Long Tom Bar, the majority of the rocks exposed in the canyon walls are part of the Idaho Batholith. These rocks are approximately 65 million years old. The canyon itself was formed 35 to 45 million years ago.</p>

<p>This rugged canyon provides habitat for an abundant and varied wildlife resource. Big game species commonly observed along the river include bighorn sheep, elk, mule deer, white-tailed deer, mountain goats, black bear, cougar and moose. Small mammal populations also are well represented by species such as bobcat, coyote, red fox, porcupine, badger, beaver, mink, marten, river otter, muskrat, weasel, marmots and skunks. Waterfowl, shorebirds and songbirds are particularly abundant during seasonal migrations. Chukar, partridge, blue grouse, ruffed grouse and spruce grouse are also common residents.</p>

<p>The main stem of the Salmon River provides habitat for a variety of fish species. These include cutthroat trout, bull trout, rainbow trout, mountain white fish, sockeye salmon, Chinook salmon (spring/summer/fall run), steelhead, smallmouth bass, squawfish, sucker and sturgeon. The river offers high-quality sportfishing for resident populations of cutthroat and rainbow trout, steelhead and whitefish.</p>

<p>Evidence suggests that man first inhabited the Salmon River country 8,000 years ago. White man came to the Salmon River in the very early 1800's following Lewis and Clark's 1805 expedition. There are several Native American and pioneer historical sites to visit along the river corridor.</p>

<p>For more than 150 years after the first white men came to this valley, only one-way trips down the Salmon River were possible due to its whitewater. In recent years, with the advent of power boats, skilled operators have been able to travel up river. Even today, however, this trip demands the best in skill, experience and equipment.</p>

<p>Since 1976, controlled permits have been required to float the river during the June 20 - September 7 period. Approximately 7000 people go down the corridor via float boat each summer through Class I to class IV whitewater. The Salmon's abundant sandy beaches provide campsites to float groups throughout the summer and fall. Floaters are not the only recreationists to enjoy the canyon; hikers, horse packers and jet boaters all share in the Salmon River experience. The historical use of jetboats was recognized by Congress as an integral part of the transportation system on the Salmon River; therefore, provisions were made to continue powerboat use. Permits are required for both float boats and jetboats on the wild section during the control period of June 20 - September 7. Applications for permits are taken in December through January through the National Recreation Reservation System at rec.gov. All floaters are required to carry fire pans, portable toilets, ash containers, shovel, bucket, and food strainer. Leave No Trace and minimum-impact camping techniques are the required method of dealing with your overnight stay in the corridor. Jet boat permits are issued on a phone-in, first-come/first-served basis.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>