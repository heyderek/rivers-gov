<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sturgeon River, Michigan';

// Set the page keywords
$page_keywords = 'Ottawa National Forest, Sturgeon River, Michigan';

// Set the page description
$page_description = 'Sturgeon River, Michigan';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';

//ID for the rivers
$river_id = array('129');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Ottawa National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 3, 1992. From its entry into the Ottawa National Forest to the northern boundary of the Ottawa National Forest.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 20.0 miles; Scenic &#8212; 8.0 miles; Total &#8212; 28.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/sturgeon-ottawa.jpg" alt="Sturgeon River, Ottawa National Forest" title="Sturgeon River, Ottawa National Forest" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/ottawa/recarea/?recid=12322" alt="Sturgeon River Gorge Wilderness (U.S. Forest Service)" target="_blank">Sturgeon River Gorge Wilderness (U.S. Forest Service)</a></p>
<p><a href="../documents/plans/ottawa-nf-plan.pdf" alt="Sturgeon River Management Plan (1.4 MB PDF)" target="_blank">Sturgeon River Management Plan (1.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Steve Brimm, Brimmages.com</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sturgeon River (Ottawa National Forest)</h2>
<p>This river flows through a narrow, dissected, gorge-like valley with an average depth of 200-300 feet and through the Sturgeon River Gorge Wilderness. Outstandingly remarkable values include scenery, recreation, geology, fish and wildlife.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom+
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>