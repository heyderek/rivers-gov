<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Flathead River, Montana';

// Set the page keywords
$page_keywords = 'Flathead National Forest, Glacier National Park, Flathead River, Hungry Horse Reservoir, Bob Marshall Wilderness, Great Bear Wilderness, Montana';

// Set the page description
$page_description = 'Flathead River, Montana';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('13');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Glacier National Park<br />
U.S. Forest Service, Flathead National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 12, 1976. The North Fork from the Canadian border downstream to its confluence with the Middle Fork. The Middle Fork from its headwaters to its confluence with the South Fork. The South Fork from its origin to the Hungry Horse Reservoir.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 97.9 miles; Scenic &#8212; 40.7 miles; Recreational &#8212; 80.4 miles; Total &#8212; 219.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/flathead.jpg" alt="Flathead River" title="Flathead River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/glac/" alt="Glacier National Park" target="_blank">Glacier National Park</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Flathead River</h2>
<p>The designated river includes the North Fork, Middle Fork and South Fork above Hungry Horse Reservoir and features recreation, scenery, historic sites, unique fisheries and wildlife such as grizzly bears and wolves. The rugged area includes the landscapes of Glacier National Park and the Bob Marshall and Great Bear Wilderness areas.</p>
<p>No permits are required for river use; however, there are regulations for human waste, campfires, stay limits, motorized watercraft, vehicle use, proper bear attractant storage and other site-specific rules on national forest lands. Please contact the Hungry Horse or Spotted Bear Ranger Districts for more information. Additionally, the lower Middle Fork and entire North Fork are boundaries to Glacier National Park, and those segments of river are managed cooperatively by both agencies. Specific rules and regulations for activities in Glacier National Park, including camping, campfires, bear attractant storage and other approved activities apply on Park Service lands and, in some cases, are different than the National Forest regulations. Please contact Glacier National Park for more information.</p>
<p>There are several commercial outfitters that offer float trips. Details on these commercial rafting companies can be found at the Flathead Convention and Visitor Bureau or at any of the Flathead National Forest offices.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>