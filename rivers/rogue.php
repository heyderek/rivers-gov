<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Rogue River, Oregon';

// Set the page keywords
$page_keywords = 'Rogue River, Siskiyou National Forest, Applegate River, Oregon';

// Set the page description
$page_description = 'Rogue River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('5');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Medford District<br />U.S. Forest Service, Rogue River-Siskiyou National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 2, 1968. The segment of the river extending from the mouth of the Applegate River downstream to the Lobster Creek Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 33.6 miles; Scenic &#8212; 7.5 miles; Recreational &#8212; 43.4 miles; Total &#8212; 84.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/rogue.jpg" alt="Rogue River" title="Rogue River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/rogue-siskiyou/recreation/?cid=stelprdb5305663" alt="Wild Rogue Wilderness (U.S. Forest Service)" target="_blank">Wild Rogue Wilderness (U.S. Forest Service)</a></p>
<p><a href="https://www.blm.gov/programs/recreation/permits-and-passes/lotteries-and-permit-systems/oregon-washington/rogue-river" alt="Rogue River (Bureau of Land Management)" target="_blank">Rogue River (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/rogue-plan.pdf" alt="Rogue River Management Plan (PDF)" target="_blank">Rogue River Management Plan(1.5MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Rogue River</h2>

<p>From its source in the high Cascade Mountains in southwestern Oregon near Crater Lake National Park, the Rogue, one of the longest rivers in Oregon, tumbles and flows more than 200 miles, entering the Pacific Ocean at Gold Beach. One of the eight rivers established with passage of the Wild and Scenic Rivers Act in 1968, the designated segment of the Rogue extends from the mouth of the Applegate River (about six miles downstream from Grants Pass) to the Lobster Creek Bridge (about eleven miles upstream from its mouth), a total distance of 84 miles. The first 47 miles are administered by the Medford District BLM, and the remaining 37 miles are administered by the Siskiyou National Forest.</p>

<p>The Rogue River is known nationally for its salmon and steelhead fishing and high-quality whitewater boating opportunities.</p>

<p>The lure of gold in the 1850's attracted a numbers of miners, hunters, stocker raisers and subsistence farmers. Conflicts between white settlers and Native Americans culminated in the 1855-56 Rogue River "Indian War," after which Native Americans relocated to reservations. For settlers, life in the Rogue Canyon was difficult and isolated. While gold mining operations were extensive, overall production was low. The remnants of mining, such as pipe, flumes, trestles and stamp mills can still be found.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Rogue River is home to a variety of species, including chinook salmon, coho salmon, steelhead, cutthroat trout and green sturgeon. The adult fish use the clean river substrate to deposit their eggs. The eggs then remain in the gravel for several months. After hatching, some species such as coho, steelhead and green sturgeon, rear within the river from one to three years, while others such as chinook begin the journey to the ocean and will eventually make their way back to their natal stream to spawn.</p>

<p>It is not uncommon to see black bears, deer, otters, great blue herons and ospreys looking for a meal of salmon.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Rogue River area sees over a half million visitors each year. Recreation opportunities include driving for pleasure, boating, fishing, guided motorized tour boat trips, guided whitewater fishing and float trips, camping, hiking, swimming, picnicking, wildlife viewing, sun bathing and gold panning.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Surrounding the Rogue River, the rugged and complex canyon landscape of the Wild Rogue Wilderness was partly designed to provide watershed protection for the wild section of the Rogue River. The established boundary assured preservation of a rugged and complex canyon landscape. The area is characterized by steep terrain of near vertical cliffs, razor-sharp ridges and cascading mountain creeks. Look for diverse flora and fauna among the near-vertical cliffs, razor-sharp ridges and cascading waters of numerous mountain creeks of the Rogue River watershed.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>