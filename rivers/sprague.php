<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Sprague River (North Fork), Oregon';

// Set the page keywords
$page_keywords = 'Sprague River, Fremont National Forest, Oregon';

// Set the page description
$page_description = 'Sprague River (North Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('93');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>
			
<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Fremont National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From the head of River Spring in the southwest 1/4 Section 15, T35S, R16E to the northwest 1/4 of southwest Section 11, T35S, R15E.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 15.0 miles; Total &#8212; 15.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/sprague.jpg" alt="Sprague River" title="Sprague River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/recarea/ipnf/recreation/recarea/?recid=6891" alt="St. Joe River (U.S. Forest Service)" target="_blank">St. Joe River (U.S. Forest Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Sprague River (North Fork)</h2>
<p>The North Fork of the Sprague flows out of the Gearhart Mountain Wilderness through broad, high-elevation meadows, before dropping into a steep basalt canyon. Its outstandingly remarkable values are scenery and geology. The river canyon offers views of a variety of vegetative patterns within a steep, narrow basaltic canyon. Vegetation color ranges from the generally green riparian areas, to the grey green of the sagebrush covered slopes to the darker green of the forested areas, with considerable seasonal variation. The river canyon below Sandhill Crossing Campground is a steep V-shaped canyon with talus slopes and high-volume springs.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>