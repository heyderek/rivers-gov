<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'White River, Oregon';

// Set the page keywords
$page_keywords = 'White River, Oregon, Deschutes River, Mt. Hood National Forest';

// Set the page description
$page_description = 'White River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('107');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Prineville District<br />U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From Mt. Hood National Forest to the confluence with the Deschutes River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 24.3 miles; Recreational &#8212; 22.5 miles; Total &#8212; 46.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/white.jpg" alt="White River" title="White River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/white-plan.pdf" title="White River Management Plan" target="_blank" />White River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>White River</h2>

<p>The White River lies east of the Cascade Range and south of the Columbia River Gorge. Originating on Mt. Hood, the river flows for approximately 53 miles to its confluence with the Deschutes River just above Sherar's Bridge. All but the 0.6-mile-long section at White River Falls is designated wild and scenic. The U.S. Forest Service manages the upper segments, and the Bureau of Land Management managers the lower segments.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic</em></strong></p>

<p>The river flows through a wide variety of life zones. The corridor's broad diversity of plant species ranges from subalpine to desert steppe, supporting many sensitive and unique plant species found only in this area. The uppermost part of the river is rocks and ice. Bogs and their associated species are present in the poorly drained portion of the floodplain, and downstream there are mixed conifer.  The climate is hot and dry with as little as 10 inches of annual precipitation at the river mouth, where vegetation consists mostly of shrubs, forbs and grasses.</p>

<p>Many regionally important sensitive and unique plants and plant communities are present along the river, including bog communities with stiff club moss, in the upper drainage; dark-soiled bogs with "genus communities" of grape ferns in the Iron Creek-Buck Creek areas; the notable plant communities of the south-facing, rocky openings along the river near the National Forest boundary; and an endemic plant with a very small range, Tygh Vatley milkvetch.</p>

<p><strong><em>Cultural</em></strong></p>

<p>The White River corridor lies within ceded lands of the Confederated Tribes of the Warm Springs Reservation. Special treaty rights exist concerning use of the land for traditional practices or activities.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The section above White River Falls is home to a race of redband rainbow trout that is genetically distinct from other redband rainbow trout. The introduction of chinook in this section is also regionally significant.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The White River area provides evidence of recent volcanic activity, ghost forests, an active fumarole field, an active mountain glacial carved valley on Mt. Hood's flank, steep canyons with impressive waterfalls and the Graveyard Butte area.</p>

<p>The uppermost area hosts an active fumarole field named "Devil's Kitchen" at the White River Glacier and immediately below a mixture of andesite, dacite flows with pyroclastic and mudflow deposits, known as the "Old Maid" flows. These flows occurred about 260 years ago and buried a forest on the slopes of Mt. Hood. Recent downcutting by the White River and its tributaries has exposed portions of this "Ghost Forest" along with several of the Old Maid flows in a sequence of terraces along the valley edge upriver from the Highway 35 crossing.</p>

<p>The U-shaped valley which follows contains remnant glacial moraines, glacial erratics, a kettled lake and another ghost forest. An adjacent series of sand flats contain a partially pebble armored surface. Armored sand flats are particularly fragile and susceptible to disturbance from wheeled vehicles. A steep V-shaped canyon follows a series of steps formed by benches and rock walls, alternating layers of fluvial sediments, ash flows and lava flows. The lower sections contain the oldest rocks in the White River; the 14.5-million-year-old Frenchman Springs Member of the Wanapum Basalt lies over the 15-million-year-old Grande Ronde Basalts.</p>

<p><strong><em>Historic</em></strong></p>

<p>The Bartow Road and Keeps Mill lie along the river corridor, interesting places to visit and important for their historical value. The Barlow Road is an important alternate route along the nationally significant Oregon Trail, accompanying the river for approximately four miles until it crosses at White River Station. Also present are the Timberline Trail and historic structures like a waterwheel.</p>

<p><strong><em>Hydrologic</em></strong></p>

<p>The river's unique hydrology includes its color in late summer and early fall and its isolation from other rivers. White River Falls isolates the watershed aquatically, providing an environment in which indigenous aquatic species, such as the White River race of redband rainbow trout, have evolved. As the leading edge of the White River Glacier melts in late summer, it releases large amounts of silt and sand which settle out and cover much of the channel bottom. This sand and silt give the river a milky appearance and explains the basis for its name!</p>

<p><strong><em>Recreational</em></strong></p>

<p>The White River offers outstanding opportunities for a wide variety of recreational activities. These include Nordic skiing, photography, camping, rugged hiking and nature and wildlife observation. The river canyon's outstanding solitude and hiking opportunities attract visitors within and outside the region.</p>

<p><strong><em>Scenic</em></strong></p>

<p>White River scenery is regionally important and widely appreciated in all seasons. Outstanding views include those within the corridor from the White River; the campgrounds and dispersed sites; the river corridor from Timberline Lodge, its lower parking area and Timberline Trail; views of Mt. Hood and White River Valley from White River East Sno-park; the view into the canyon from above Keeps Mill; views from Bonney Butte; and views into the rugged canyon between the National Forest boundary and Tygh Valley.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The White River area supports a diversity of endangered, threatened and sensitive species, including northern spotted owls, harlequin ducks and peregrine falcons. Elk and various raptors are also present. Wild turkey populations are the highest in northern Oregon. Silver gray squirrels are abundant. Spring and summer are the best times to see Lewis woodpeckers. This is a popular hunting area during the fall, and feeding stations provide good deer and elk viewing during winter.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>