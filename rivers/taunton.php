<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Taunton River, Massachusetts';

// Set the page keywords
$page_keywords = 'Taunton River, Massachusetts';

// Set the page description
$page_description = 'Taunton River, Massachusetts';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('207');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Northeast Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The main stem of the Taunton River from its headwaters at the confluence of the Town and Matfield Rivers in the Town of Bridgewater downstream 40 miles to its confluence with the Quequechan River at the Route 195 Bridge in the city of Fall River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Scenic &#8212; 26.0 miles; Recreational &#8212; 14.0 miles; Total &#8212; 40.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/taunton.jpg" alt="Taunton River" title="Taunton River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/taunton_pwsr_sub.html" alt="Taunton River (National Park Service)" target="_blank">Taunton River (National Park Service)</a></p>
<p><a href="http://www.tauntonriver.org" alt="Taunton River Stewardship Council" target="_blank">Taunton River Stewardship Council</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Taunton River</h2>
<p>The Taunton River is the longest undammed coastal river in New England. It supports 154 species of birds and 45 species of fish, including the bald eagle and the Atlantic sturgeon. The corridor is home to seven rare reptiles and amphibians, river otters, mink, gray foxes, and deer. One of the earliest and largest settlement areas in the Northeast for early Native People is found in the watershed.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>