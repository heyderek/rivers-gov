<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wildhorse &amp; Kiger Creeks, Oregon';

// Set the page keywords
$page_keywords = 'Wildhorse Creek, Wild Horse Creek, Kiger Creek, Oregon';

// Set the page description
$page_description = 'Wildhorse &amp; Kiger Creeks, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('164');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Burns District Office</p>
<br />
<h3>Designated Reach:</h3>
<p>October 30, 2000. Kiger Creek, from its headwaters at the top of Kiger Gorge to the boundary of the Steens Mountain Wilderness Area. Wildhorse Creek, from its headwaters to the private property line at the mouth of Wildhorse Canyon, into section 34, township 34 south, range 33 east. Little Wildhorse Creek from its headwaters to its confluence with Wildhorse Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Kiger Creek, Wild &#8212; 4.3 miles; Wildhorse Creek, Wild &#8212; 7.0 miles; Little Wildhorse Creek, Wild &#8212; 2.6 miles.  Total &#8212; 13.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/wildhorse.jpg" alt="Wildhorse Creek" title="Wildhorse Creek" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/programs/recreation/recreation-activities/oregon-washington/steens-mountain" alt="Kiger Gorge Overlook (Bureau of Land Management)" target="_blank">Sttens Mountain Recreation (Bureau of Land Management)</a></p>

<div id="photo-credit">
<!--<p>Photo Credit: Unknown</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->
</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wildhorse & Kiger Creeks</h2>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Wildhorse Creek</p>

<p>From its headwaters in Wildhorse Canyon, Wildhorse Creek descends south from Wildhorse Lake, just below Wildhorse Overlook, one of the  tallest points on Steens Mountain in the high desert of southeastern Oregon. Little Wildhorse Creek begins at Little Wildhorse Creek Lake. The two streams combine for over four additional miles through the Steens Mountain Wilderness Area. This scenic wonder ultimately ventures through private land and into the Alvord Valley.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic</em></strong></p>

<p>The headwaters of Little Wildhorse Creek are designated as a Research Natural Area (RNA)/ Area of Critical Environmental Concern (ACEC) because of a mid- to high-elevation lake and associated ecosystem. This RNA/ACEC was designated to protect the area for scientific study opportunities. Interesting and unique plant communities are located above the Wildhorse Creek and Little Wildhorse Creek confluence.</p>

<p><strong><em>Recreational</em></strong></p>

<p>This river system offers many opportunities for remote hiking and primitive camping, as well as solitude, especially in the upper pristine areas. A portion of the Oregon High Desert National Recreation Trail traverses the Wildhorse and Little Wildhorse Canyons.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Wildhorse and Little Wildhorse Creek corridors offer outstanding opportunities to view glacier-formed canyons and other significant geologic features, including two glacial lakes. The canyons drop off the east face of Steens Mountain, resulting in spectacular views of geology and extensive landscape vistas. Both creeks exhibit good examples of past glacial activity, erosional processes and tilting action of a massive, fault-block mountain.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildhorse and Little Wildhorse Creeks provide habitat for California bighorn sheep and a variety of wildlife from headwaters to canyon mouths. Species such as pika can be found at upper elevations. Little Wildhorse Creek contains excellent riparian habitat at higher elevations. Wildhorse and Little Wildhorse Lakes provide vernal high elevation pools, talus and cliffs, willows and adjacent upland vegetation, which together provide a diversity of wildlife habitats.</p>

<p>&nbsp;</p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Kiger Creek</p>

<p>From its headwaters just below Kiger Overlook and north through private land toward the Malheur National Wildlife Refuge, Kiger Creek offers unrivaled scenic beauty. Throughout its winding path in Kiger Gorge, a glacial canyon, the 4.3-mile slice of this wild river runs within the newly designated Steens Mountain Wilderness. The natural setting and steep topography of Kiger Gorge create a spectacular setting. Recreation opportunities in the area include hiking, primitive camping, fishing and sightseeing. Kiger Creek also serves as habitat for redband trout. Other wildlife includes mule deer, Rocky Mountain elk, bighorn sheep and pronghorn.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Botanic</em></strong></p>

<p>The west rim of Kiger Creek contains unique botanical sites. Wet meadows, interspersed with willow forests, are habitat for Special Status plant species, including pinnate grapefern, Cusick's draba and foetid sedge. Drier, rocky areas adjacent to meadows contain Steens Mountain penstemon, weak-stemmed stonecrop and sky pilot. Glacial lakes located above the meadows host vegetation that grows in other canyon sections later in the season. Aspen, western juniper, cottonwood, beaver ponds and large spring areas contribute to habitat diversity.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>Kiger Creek provides habitat for wild, native redband trout. Malheur mottled sculpin, mountain whitefish and longnose dace have been observed in Kiger Creek downstream of the designated segment. Redband trout and Malheur mottled sculpin have been recognized by the BLM as Special Status Species.</p>

<p><strong><em>Recreational</em></strong></p>

<p>This remote river corridor offers outstanding opportunities for both primitive recreation and solitude. The area lies in rugged country with difficult and limited access. A high level of backcountry skill is required of visitors to this area.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Kiger Creek flows through one of the most prominent canyons in the Steens Mountain area and offers a spectacular display of past glacial activity. The long corridor can be seen from miles away and is identifiable by its own unique geologic feature&#8212;Kiger Notch. The U-shaped gorge is a classic example of a glaciated canyon. On a clear day, scenic vistas extend to the horizon.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Elevation, varying gradients and canyon slopes provide highly diverse habitats associated with the riparian area. Pikas may be present in talus slopes near the canyon head. Bighorn sheep use the eastern slope of the gorge; elk and mule deer use this area extensively during summer months.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>