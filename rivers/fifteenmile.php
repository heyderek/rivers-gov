<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Fifteenmile Creek, Oregon';

// Set the page keywords
$page_keywords = 'Fifteenmile Creek, Oregon, Mt. Hood National Forest';

// Set the page description
$page_description = 'Fifteenmile Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('176');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Hood National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. From its source at Senecal Spring to the southern edge of the northwest quarter of the northwest quarter of Section 20, Township 2 South, Range 12 East.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 10.5 miles; Scenic &#8212; 0.6 miles; Total &#8212; 11.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<!--<img src="images/fifteenmile.jpg" alt="Fifteenmile Creek" title="Fifteenmile Creek" width="265px" height="204px" />-->
<p>We could use a photo.<br />Do you have one you'd like to let us use?<br />If so, please <a href="../info/contact.cfm" alt="Contact Us" target="_blank">contact us</a> today.<br />We would be happy to credit you as the photographer.</p>
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detailfull/plumas/about-forest/?cid=stelprdb5097323&width=full" alt="Feather River (U.S. Forest Service)" target="_blank">Feather River (U.S. Forest Service)</a></p>-->

<div id="photo-credit">
<!--<p>Photo Credit: Matt Lindsay, American Rivers</p>-->
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Fifteenmile Creek</h2>
<p>Fifteenmile Creek originates at Senecal Spring on Lookout Mountain in Oregon's Cascade Range. At 5,900 feet in elevation, Lookout Mountain is the highest point in the Badger Creek Wilderness. Fifteenmile Creek flows toward the northeast eventually joining the Deschutes River near its confluence with the Columbia River east of The Dalles, Oregon.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>