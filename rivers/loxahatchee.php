<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Loxahatchee River, Florida';

// Set the page keywords
$page_keywords = 'Loxahatchee River, Florida';

// Set the page description
$page_description = 'Loxahatchee River, Florida';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southeast';

//ID for the rivers
$river_id = array('SD12');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Jonathan Dickinson State Park</p>
<br />
<h3>Designated Reach:</h3>
<p>May 17, 1985. From Riverbend Park downstream to Jonathan Dickinson State Park.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 1.3 miles; Scenic &#8212; 5.8 miles; Recreational &#8212; 0.5 miles; Total &#8212; 7.6 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/loxahatchee.jpg" alt="Loxahatchee River" title="Loxahatchee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.floridastateparks.org/jonathandickinson/" alt="Jonathan Dickinson State Park" target="_blank">Jonathan Dickinson State Park</a></p>
<p><a href="http://www.loxahatcheeriver.org/friends_loxahatchee.php" alt="Friends of the Loxahatchee River" target="_blank">Friends of the Loxahatchee River</a></p>
<p><a href="http://www.loxahatcheeriver.org/environmental_center.php" alt="Loxahatchee River Center" target="_blank">Loxahatchee River Center</a></p>
<p><a href="../documents/plans/loxahatchee-plan.pdf" alt="Loxahatchee River Management Plan (5.4 MB PDF)" target="_blank">Loxahatchee River Management Plan (5.4 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: American Rivers</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Loxahatchee River</h2>
<p>This scenic southern river flows through an interesting vegetative landscape which supports a wide range of aquatic and terrestrial fish and wildlife species. The river also provides for an abundance of bird species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>