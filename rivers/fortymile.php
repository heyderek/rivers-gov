<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Fortymile River, Alaska';

// Set the page keywords
$page_keywords = 'Fortymile Wild and Scenic River, Alaska';

// Set the page description
$page_description = 'Fortymile River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('48');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Fairbanks District Office</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The main stem within the state of Alaska, including O'Brien Creek, the South Fork, Napoleon Creek, Franklin Creek, Uhler Creek, the Walker Fork downstream from the confluence of Liberty Creek, Wade Creek, the Mosquito Fork downstream from the vicinity of Kechumstuk, the West Fork Dennison Fork downstream from the confluence of Logging Cabin Creek, the Dennison Fork downstream from the confluence of the West Fork Dennison Fork, Logging Cabin Creek, the North Fork, Hutchison Creek, Champion Creek, the Middle Fork downstream from the confluence of Joseph Creek, and Joseph Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 179.0 miles; Scenic &#8212; 203.0 miles; Recreational &#8212; 10.0 miles; Total &#8212; 392.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/fortymile.jpg" alt="Fortymile River" title="Fortymile River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/ak/st/en/prog/nlcs/fortymile_nwsr.html" alt="Fortymile River (Bureau of Land Management)" target="_blank">Fortymile River (Bureau of Land Management)</a><br />
<a href="../documents/plans/fortymile-plan.pdf" title="Fortymile River Management Plan" target="_blank">Fortymile River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Fortymile River</h2>

<p>The Fortymile River is a clear water stream whose six main forks and their tributaries flow out of the Yukon-Tanana Uplands east of the Mertie Mountains and north of the Tanana State Forest. It is a major tributary of the Yukon River.</p>

<p>The site of Alaska's first major gold rush in 1886, the history of the area is written in the cabins and mine workings along the stream. Gold prospectors gave the Fortymile River its name. It joins the Yukon River about 40 miles below Fort Reliance, an old Canadian trading post.</p>

<p>Road-accessible boat launch sites and bush strips in the upper reaches allow trips varying from one day to two weeks. The main Fortymile offers a great way to see the differing landscapes of the interior of Alaska in a two-day float, from incised canyons to the wide-open Yukon Valley.</p>

<p>Threading through this rugged landscape, the twisting, picturesque Taylor Highway leads motorists into the heart of the Fortymile and over American Summit to the historic town of Eagle on the Yukon River.</p>

<p>Alpine tundra, tussocks and boreal forest are among the many different types of plant communities found along the Fortymile River. Various hillsides and valley floors are home to white spruce, birch, willow and aspen. Blueberry and cranberry bushes provide vivid fall colors, as well as tasty berries enjoyed by wildlife and people alike.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural &amp; Historic</em></strong></p>

<p>The 1886 discovery of gold on Franklin's Bar on the Fortymile River touched off interior Alaska's first gold rush. The mining boom ushered in a wave of settlement that forever changed the place, notably for the native Athabascan Indians who occupied this region. Miners eventually extracted more than a half-million ounces of gold from the Fortymile, according to the Alaska Division of Geological and Geophysical Surveys.  During the height of mining activity, the town of Steele Creek served as a community center and transportation hub with its trading post, post office and restaurant. Several cabins and a two-story roadhouse remain.</p>

<p>The military figured prominently in the history of the Fortymile region, as Army troops were sent to the Eagle area in 1899 to address reports of starvation and lawlessness among the miners. The trail built by these soldiers, from Valdez to Eagle, as well as construction of Fort Egbert in Eagle, did much to improve communication with the rest of the world. In 1900, the military further improved communication through the construction of the Washington-Alaska Military Cable and Telegraph System (WAMCATS). Messages that once took up to a year to reach Washington, D.C., could now be sent in a matter of days. Remains of this system, eventually replaced by the wireless telegraph station in Eagle, may still be found throughout the Fortymile drainage.</p>

<p><strong><em>Fish &amp; Wildlife</em></strong></p>

<p>The Fortymile River is home to Arctic grayling, round whitefish and burbot. The river corridor provides habitat for caribou, moose, Dall sheep, grizzly and black bear, furbearers, small game, raptors, waterfowl and numerous species of small mammals and birds. One endangered species, the peregrine falcon, nests in the area.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Float trips on the Fortymile River offer scenic beauty, solitude and glimpses of gold-mining dredges, turn-of-the-century trapper cabins and abandoned townsites.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>