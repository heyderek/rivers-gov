<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Jarbidge River, Idaho';

// Set the page keywords
$page_keywords = 'Jarbidge River, Idaho';

// Set the page description
$page_description = 'Jarbidge River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('188');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The Jarbidge River from its confluence with the West Fork of the Bruneau River to the upstream boundary of the Bruneau-Jarbidge Rivers Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 28.8 miles; Total &#8212; 28.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/jarbidge.jpg" alt="Jarbidge River" title="Jarbidge River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/nlcs_web/sites/id/st/en/prog/NLCS/Idaho_WSRs.html" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/visit/search-details/14977/2" alt="Jarbidge River (Bureau of Land Management)" target="_blank">Jarbidge River (Bureau of Land Management)</a></p>
<p><a href="https://www.blm.gov/sites/blm.gov/files/documents/files/Media-Center_Public-Room_Idaho_Bruneau-Jarbidge-Owyhee_BoaterGuide.pdf" alt="Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide" target="_blank">Owyhee, Bruneau &amp; Jarbidge Rivers Boating Guide</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Jarbidge River</h2>

<p>The Jarbidge River originates as two main forks in the Jarbidge Mountains of northeastern Nevada. It flows through basalt and rhyolite canyons on the high plateau of the Owyhee Desert before joining the West Fork of the Bruneau River to form the Bruneau River about 24 miles north of the Nevada border, just upstream of Indian Hot Springs. Nearly 29 miles are designated wild. Challenging whitewater flows through a canyon with steep walls and statuesque rock formations. Golden eagles are often seen, and chukar are abundant.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>Native Americans have utilized the canyonlands for shelter, weaponry, fish and game, and water for thousands of years. Petroglyphs, pictographs, rock alignments, shrines and vision quest sites of the Shoshone and Paiute peoples are located throughout the Owyhee Canyonlands. Tribal members still frequent the canyonland areas to hunt, fish, pray and conduct ceremonies.</p>

<p>A Native American legend about a dangerous creature that lived in the Jarbidge Canyon supports the idea that ancient peoples avoided living there long-term. The creature was called Tsa-hau-bitts or Jahabich, loosely translating to "evil spirit." Through many English interpretations of these names, the area eventually became known as Jarbidge.</p>

<p><strong><em>Ecologic</em></strong></p>

<p>The Bruneau River phlox, a white-flowered and matted plant that clings to ledges, rock crevices and cliffs, occurs in vertical or overhanging rhyolitic canyon walls along the inner Jarbidge River Canyons. The entire known extent of Bruneau River phlox in Idaho occurs within approximately 35 miles on the Bruneau, West Fork of the Bruneau and Jarbidge Rivers.</p>

<p><strong><em>Fisheries &amp; Aquatic Species</em></strong></p>

<p>The Jarbidge River supports both redband trout and the southernmost population of bull trout in North America and contains one of six bull trout populations identified for recovery. The threatened bull trout is the only fish in the Owyhee Canyonlands that is federally listed under the Endangered Species Act. Jarbidge River bull trout are important because they occupy a unique and unusual semi-arid desert ecological setting, and their loss would result in a substantial modification of the species' range.</p>

<p>Although bull trout spawn in upstream portions of the Jarbidge River in Nevada, the designated segment contains bull trout over-wintering and migratory habitat. Bank-full flows move the riverbed materials downstream, and the silts and sands to upper channel banks, making for good bull trout habitat. The bull trout and redband trout populations also thrive on low flows that maintain hiding pools that hold water throughout dryer seasons.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Jarbidge, Bruneau and Owyhee river systems provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States. Though not unique to southwest Idaho, the presence of these geologic formations in such great abundance and aerial extent makes the designated river segments geologically unique from a national perspective.</p>

<p>The geology of the canyons has been shaped by a combination of volcanic activity, glacial melt and regional drainage patterns. The Bruneau-Jarbidge and Owyhee areas were the sites of the massive Owyhee-Humboldt and Bruneau-Jarbidge volcanic centers, whose explosions, massive rhyolite flows, basaltic eruptions and related glacial flows carved out their spectacular canyons. Where overlying basalt is present, the rhyolite formations are nestled in the rubble slopes below vertical walls of basalt. Weathering and erosion have carved immense monolithic cliffs and numerous sculptured pinnacles known as "hoodoos."</p>

<p>There is one small-scale jasper-mining claim near Indian Hot Springs in the Bruneau River Canyon, just downstream of where the Jarbidge flows into the Bruneau.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The Jarbidge River is a narrow, technical river known nationally for its challenging whitewater through a remote desert canyon. Boats over 14' are not recommended for there are two or more long, rocky and challenging portages. Additional hazards on the Jarbidge are ever-changing logjams and blind corners. Given the four-wheel drive, high-clearance vehicle required to travel to the Jarbidge River takeout, most paddlers continue on to the Bruneau.</p>

<p><strong><em>Scenic</em></strong></p>

<p>Narrow canyons along the Jarbidge River can be as deep as 800 feet. They are dominated by a mixture of high, vertical lines and forms of coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora. Steep diagonal lines of talus slopes, with their yellow and subdued green sagebrush-bunchgrass communities, dark green juniper, medium-textured, reddish rhyolite rubble fields and coarse-textured, blackish basalt rubble fields, add visual variety. The combinations of line, form, color and texture found amidst this close association of landforms, water and vegetation create exceptional landscapes.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee Canyonlands provide riparian habitat for a number of wildlife species common to Southwest Idaho. Big game commonly found in the area includes California bighorn sheep, elk, mule deer and pronghorn.</p>

<p>Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents (mice, kangaroo rats, voles, squirrels, and chipmunks), rabbits, shrews, bats, weasels and skunks. River otters are supported by year-long flows and a good prey base (fisheries). Birds include songbirds, waterfowl, shorebirds and raptors.</p>

<p>This area includes Preliminary Priority Habitat for greater sage-grouse and a number of Idaho BLM sensitive species, including bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>