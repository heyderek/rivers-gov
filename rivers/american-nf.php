<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'American River (North Fork), California';

// Set the page keywords
$page_keywords = 'Tahoe National Forest, American River, California';

// Set the page description
$page_description = 'American River (North Fork), California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('21');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php include_once ("../iframe.php"); ?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Mother Lode Field Office<br />
U.S. Forest Service, Tahoe National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>November 10, 1978. From a point 0.3 miles above Heath Springs downstream to a point 1,000 feet upstream of the Colfax-Iowa Hill Bridge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 38.3 miles; Total &#8212; 38.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/american-nf.jpg" alt="North Fork American River" title="North Fork American River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/attmain/tahoe/specialplaces/" target="_blank" alt="Tahoe National Forest (U.S. Forest Service)" target="_blank">Tahoe National Forest (U.S. Forest Service)</a><br />
<a href="../documents/plans/north-fork-american-plan-fonsi.pdf" title="American (North Fork) Management Plan &amp; FONSI, California" target="_blank">American (North Fork) Management Plan &amp; FONSI, California</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>American River (North Fork)</h2>

<p>The North Fork of the American River originates in eastern Placer County in the Tahoe National Forest. It flows west and then southwest, passing the town of Colfax and on through Clementine/North Fort Reservoir; it meets the Middle Fork of the American four miles below the North Fork Reservoir Dam near the town of Auburn and flows past the site of the abandoned Auburn Dam. The Tahoe National Forest manages the upper 26 miles of this wild reach and the Bureau of Land Management manages the lower third.</p>

<p>While the awe-inspiring river canyon is best known for its thrilling whitewater, challenging hiking trails, excellent fishing, abundant wildlife and dramatic scenery contribute to its popularity and significance in the National Wild and Scenic Rivers System.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Cultural</em></strong></p>

<p>The North Fork, along with neighboring Middle and South Forks, were central to the California Gold Rush that began in January 1848 and attracted a swarm of gold seekers to the region. By the summer of 1849, there were over a 100,000 prospectors working on the river and other nearby rivers in the Sierra Nevada Mountains. The river originates from the gold-rich Sierra Nevada Mountains and rich veins are all throughout the neighboring mountains, feeding the many creeks that flow into the American River. While the richest placer deposits on the American River were exhausted long ago, amateur and professional panners still visit today. The upper watershed canyons are dotted with more than 1,500 historical gold rush era and Native American cultural sites, many of which are eligible for inclusion on the National Register of Historic Places.</p>

<p><strong><em>Fish &amp; Wildlife</em></strong></p>

<p>The upper American River watershed supports a wide variety of wildlife. Typical bird and mammal species include mountain quail, mourning doves, Steller's jays, western bluebirds, warblers, squirrels, skunks, chipmunks, coyotes, mule deer, black bears and mountain lions. Endangered species in the watershed include the American bald eagle, California red-legged frog and valley elderberry longhorn beetle. With few exceptions, the high mountain lakes above 6,000 feet were historically fishless, dominated instead by amphibians, insects and small aquatic invertebrates. It has been only within the last few decades that fish have appeared the higher elevation lakes. The upper watershed streams provide high-quality habitat for native fish, including trophy-sized rainbow trout. Anadromous fish species exist in the lower American River, but do not have access past Nimbus Dam.</p>

<p><strong><em>Recreational</em></strong></p>

<p>The watershed a popular recreation designation. Gorge scrambling is the most popular activity, followed by hiking, fishing and boating. Hikers and fishing enthusiasts can choose from a number of trails to access the river canyon, most of them dropping steeply from the canyon rim down to the water.</p>

<p>The rivers offer a wide variety of whitewater recreation opportunities, including Class IV and V reaches. While visitation peaks in the summer, primarily driven by hikers/swimmers, late spring into mid-summer is typically the height of the boating season. Generation Gap (12 miles) includes a three-mile long hike to the put in and is run by only the most experienced boaters. Paddling the next reach, known as Giant Gap (14 miles), requires a two-mile hike down the Euchre Bar Trail. While overnight camping permits are not required, visitors must obtain a fire permit to build a campfire.
</p>

<p><strong><em>Scenic</em></strong></p>

<p>The North Fork of the American River has the classic hydrologic characteristics of an "A" channel river, with its scoured rocks, high waterfalls and deep plunge pools throughout. Walls tower 2,000 feet to 4,000 feet above the river, creating a majestic backdrop for cascading waterfalls, brightly colored wildflowers and the bright, clear water of the river itself.</p>

<p><strong><em>Vegetation</em></strong></p>

<p>The watershed supports a diversity of habitat and vegetation types. The higher elevations consist of mixed conifers and montane hardwoods, while the lower elevations include grasslands, oak woodland, chaparral and mixed conifers. Historically, fire has played an important role in maintaining a diverse landscape; however, accumulated fuels resulting from land management practices, along with residential development in forested area, have increased the risk of catastrophic fire in many parts of the region.</p>

<p><strong><em>Water Quality</em></strong></p>

<p>Water quality in the North Fork of the American River is considered to be very good. Streams in the upper watershed are typically clear, cold streams that are naturally highly oxygenated, low in dissolved ions and nutrients, and exhibit low instream plant or algal growth. Images of the North Fork often highlight its aquamarine water.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>