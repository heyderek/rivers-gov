<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'John Day River (North Fork), Oregon';

// Set the page keywords
$page_keywords = 'Wallowa-Whitman National Forest, Umatilla National Forest, John Day River, Oregon';

// Set the page description
$page_description = 'John Day River (North Fork), Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('88');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Umatilla National Forest<br />
U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters in the North Fork of the John Day Wilderness Area to its confluence with Camas Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 27.8 miles; Scenic &#8212; 10.5 miles; Recreational &#8212; 15.8 miles; Total &#8212; 54.1 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/john-day-nf.jpg" alt="North Fork John Day River" title="North Fork John Day River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.fs.usda.gov/detail/umatilla/recreation/wateractivities/?cid=fsbdev3_062366#nfjd" title="North Fork Rafting (U.S. Forest Service)" target="_blank">North Fork Rafting (U.S. Forest Service)</a></p>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="http://www.fs.usda.gov/detail/umatilla/specialplaces/?cid=stelprdb5275389" alt="Wild &amp; Scenic Rivers &#8211; Umatilla National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Umatilla National Forest</a></p>
<p><a href="../documents/plans/north-fork-john-day-plan.pdf" title="North Fork John Day River Management Plan" target="_blank">North Fork John Day River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>John Day River (North Fork)</h2>
<p>From its headwaters in the North Fork John Day Wilderness to Camas Creek, the North Fork of the John Day River is one of the most important rivers in northeast Oregon for the production of anadromous fish. It supports the largest remaining wild run of Chinook salmon and steelhead trout in the Columbia River basin. Wildlife found along the river's corridor includes Rocky mountain elk, mule deer and black bears, along with bald and golden eagles, ospreys and goshawks.</p>
<p>The river's diverse landscape and geologic formations create high-quality natural scenery. Man-made developments have a primitive or historic appearance, including early day mining remains. There is a great deal of history from the gold mining era tied to this area. Recreation opportunities range from hiking and horseback riding to rafting/kayaking and gold panning.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>