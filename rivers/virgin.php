<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Virgin River, Utah';

// Set the page keywords
$page_keywords = 'Virgin River, Utah, Zion National Park';

// Set the page description
$page_description = 'Virgin River, Utah';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('204');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<script>
$(document).ready(function () {
var content = $('.accordion-content');
var heading = $('.accordion-heading');
content.hide();
heading.click(function () {
$(this).next(content).slideToggle(300)
$(this).toggleClass('active-accordion');
})
});
</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, St. George Field Office<br />
National Park Service, Zion National Park</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The Virgin River and tributaries, including:</p>

<h2 class="accordion-heading">Virgin River Tributaries</h2>

<div class="accordion-content">
<ol>
<li>Taylor Creek from the junction of the North, Middle and South Forks of Taylor Creek west to the Zion National Park boundary.</li>
<li>The North Fork of Taylor Creek from the head of the North Fork to its junction with Taylor Creek.</li>
<li>The <a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Middle Fork of Taylor Creek</a> from the head of the Middle Fork on Bureau of Land Management land to its junction with Taylor Creek.</li>
<li>The South Fork of Taylor Creek from the head of the South Fork to its junction with Taylor Creek.</li>
<li>Timber Creek and its tributaries from the head of Timber Creek and the tributaries of Timber Creek to its junction with LaVerkin Creek.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">LaVerkin Creek</a> beginning in Section 21, Township 38 South, Range 11 West, on Bureau of Land Management land southwest through Zion National Park to the south end of Section 7, Township 40 South, Range 12 West.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Willis Creek</a> from Bureau of Land Management land in Section 27, Township 38 South, Range 11 West, to its junction with LaVerkin Creek in Zion National Park.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Beartrap Canyon</a> beginning on Bureau of Land Management land in Section 3, Township 39 South, Range 11 West, to its junction with LaVerkin Creek and the segment from the headwaters north of Long Point to its junction with LaVerkin Creek.</li>
<li>Hop Valley Creek beginning at the southern boundary of Section 20, Township 39 South, Range 11 West, to its junction with LaVerkin Creek.</li>
<li>Current Creek from the head of Current Creek to its junction with LaVerkin Creek.</li>
<li>Cane Creek from the head of Cane Creek to its junction with LaVerkin Creek.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Smith Creek</a> from the head of Smith Creek to its junction with LaVerkin Creek.</li>
<li>North Creek (Left and Right Forks): The Left Fork from its junction with Wildcat Canyon to its junction with the Right Fork; the head of the Right Fork to its junction with the Left Fork; and North Creek from the junction of the Left and Right Forks southwest to the Zion National Park boundary.</li>
<li>Wildcat Canyon (Blue Creek) from the Zion National Park boundary to its junction with the Right Fork of North Creek.</li>
<li>Little Creek beginning at the head of Little Creek to its junction with the Left Fork of North Creek.</li>
<li>Russell Gulch from the head of Russell Gulch to its junction with the Left Fork of North Creek.</li>
<li>Grapevine Wash from the Lower Kolob Plateau to its junction with the Left Fork of North Creek.</li>
<li>Pine Spring Wash from the head of Pine Spring Wash to its junction with the Left Fork of North Creek.</li>
<li>Wolf Springs Wash from the head of Wolf Springs Wash to its junction with Pine Spring Wash.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Kolob Creek</a> beginning in Section 30, Township 39 South, Range 10 West, through Bureau of Land Management land and Zion National Park land to its junction with the North Fork of the Virgin River.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Oak Creek</a> beginning in Section 19, Township 39 South, Range 10 West, to its junction with Kolob Creek.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Goose Creek</a> from the head of Goose Creek to its junction with the North Fork of the Virgin River.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Deep Creek</a> beginning on Bureau of Land Management land at the northern boundary of Section 23, Township 39 South, Range 10 West, to its junction with the North Fork of the Virgin River.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">The North Fork of the Virgin River</a> beginning on Bureau of Land Management land at the eastern border of Section 35, Township 39 South, Range 10 West, to the Zion National Park boundary.</li>
<li>Imlay Canyon from the head of Imlay Creek to its junction with the North Fork of the Virgin River.</li>
<li>Orderville Canyon from the eastern boundary of Zion National Park to its junction with the North Fork of the Virgin River.</li>
<li>Mystery Canyon from the head of Mystery Canyon to its junction with the North Fork of the Virgin River.</li>
<li>Echo Canyon from the eastern boundary of Zion National Park to its junction with the North Fork of the Virgin River.</li>
<li>Behunin Canyon from the head of Behunin Canyon to its junction with the North Fork of the Virgin River.</li>
<li>Heaps Canyon from the head of Heaps Canyon to its junction with the North Fork of the Virgin River.</li>
<li>Birch Creek from the head of Birch Creek to its junction with the North Fork of the Virgin River.</li>
<li>Oak Creek from the head of Oak Creek to its junction with the North Fork of the Virgin River.</li>
<li>Clear Creek from the eastern boundary of Zion National Park to its junction with Pine Creek.</li>
<li>Pine Creek from the head of Pine Creek to its junction with the North Fork of the Virgin River.</li>
<li>The East Fork of the Virgin River from the eastern boundary of Zion National Park through Parunuweap Canyon to the western boundary of Zion National Park.</li>
<li><a href="../documents/virgin-river-orvs.pdf" title="BLM Virgin River Streams' Outstandlingly Remarkable Values" target="_blank">Shunes Creek</a> from the dry waterfall on land administered by the Bureau of Land Management through Zion National Park to the western boundary of Zion National Park.</li>
</ol>
</div>

<br />

<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 145.4 miles; Scenic &#8212; 11.3 miles; Recreational &#8212; 12.6 miles; Total &#8212; 169.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/virgin.jpg" alt="Virgin River" title="Virgin River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/ut/st/en/prog/blm_special_areas/wild_and_scenic_rivers.html" alt="Special Places in Utah (Bureau of Land Management)" target="_blank">Special Places in Utah (Bureau of Land Management)</a></p>
<p><a href="http://www.nps.gov/zion/planyourvisit/virgin-river-narrows.htm" alt="Virgin River (National Park Service)" target="_blank">Virgin River (National Park Service)</a></p>
<p><a href="https://www.rivers.gov/documents/plans/virgin-plan.pdf">Virgin River Comprehensive Management Plan &amp; Environmental Assessment (National Park Service &amp; Bureau of Land Management</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Virgin River</h2>
<p>Through the Omnibus Public Lands Management Act of 2009 (P.L. 111-11), Congress designated approximately 165.5 miles of the Virgin River and its tributaries across federal land within Zion National Park (28 segments) and adjacent Bureau of Land Management Wilderness (11 segments), as part of the National Wild and Scenic Rivers System.</p>
<p>Over the course of 13 million years, the Virgin River has carved through the red sandstones of Zion National Park to create some of the most unforgettable scenery in the National Park System. In fact, this very act of natural erosion is responsible for "The Narrows," which is one of the premiere hiking adventures in the United States, possibly the world. In addition, there are several easy trails along the river.</p>
<p>Despite the obvious evidence of the erosive force of the river, the river itself winds peacefully through the canyon. Natural river processes proceed unimpeded, allowing for seasonal flooding and meander migration, vegetative recruitment and plant succession.</p>
<p>The corridor includes populations of desert bighorn sheep, Mexican spotted owl and the endemic Zion snail and exemplary riparian corridors and rare plant communities. Cottonwoods and willows along the banks provide shade of hikers and hiding spots for mule deer and numerous bird species. Other wildlife, such as ringtail cats, bobcats, foxes, rock squirrels and cottontail rabbits rest in the rocky hiding places carved in the sandstone. As the heat of the day yields to the cool of the desert night, look for the many animals drawn to the river to emerge to get on with their lives.</p>
<p>The Virgin River system contains some of the best examples in the region of prehistoric American Indian sites that provide a tangible connection between culturally associated tribes and their ancestors.</p>
<p>The Bureau of Land Management has identified the outstandingly remarkable values on its lands that make the Virgin River and its tributaries so special and unique. If you don't see a description of the ORVs for a particular segment, they either have not yet been defined, or the agency hasn't yet sent them to us.</p>

<h2 class="accordion-heading">Beartrap Canyon</h2>

<div class="accordion-content">
<p>Beartrap Canyon is largely rugged, steeply sloped and contains the headwaters area for many tributaries that flow through the Kolob Terrace. The terrain consists of a sandstone finger of a mesa and the upper reach of the Beartrap Canyon. At a top elevation of 7,500 feet, both the mesa top and canyon bottom sustains Utah juniper and ponderosa and pinyon pine trees. This area is also a designated wilderness of 40 acres directly adjacent to a larger wilderness area in Zion National Park.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Virgin River and its tributaries are uniquely situated along the western margin of the Colorado Plateau, where the recent history of tectonic activity and erosional downcutting has resulted in a labyrinth of deep sandstone canyons, volcanic phenomena and widespread exposures of brilliantly colored sedimentary deposits.</p>

<p>Beartrap Canyon features high cliffs of Navajo sandstone, known to be the world's highest sandstone cliffs and slot canyons, which are deep and exceptionally narrow vertical walled canyons. Springs that discharge from the Navajo sandstone aquifer in these canyons support a large number of rare and endemic species.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Despite its small size, because of its proximity to adjacent wilderness and other relatively undisturbed lands, a wide variety of wildlife lives here. Hawks, falcons and eagles soar above the canyons, while ring-tailed cats, mountain lions and black bears hunt in the uplands and along the canyon bottoms. The dissected remote canyons also offer suitable nesting habitat for the Mexican spotted owl, a threatened species.</p>

<p><a href="https://www.blm.gov/visit/search-details/16316/2" title="BLM Beartrap Canyon" target="_blank">Bureau of Land Management &#8211; Beartrap Canyon</a></p>

<p><a href="https://www.nps.gov/zion/planyourvisit/laverkin-creek-trail.htm" title="NPS Beartrap Canyon Hiking Trail" target="_blank">National Park Service &#8211; Beartrap Canyon Hiking Trail</a></p>
</div>

<h2 class="accordion-heading">Deep Creek</h2>

<div class="accordion-content">
<p>Deep Creek is named for the deeply incised stream channel that it has carved through the Deep Creek Wilderness of northeastern Washington County, Utah. The BLM's St. George Field Office manages a majority of the five-mile segment. Deep Creek travels less than a half mile through Zion National Park to a confluence with North Creek, one of the major tributaries to the Virgin River system.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Virgin River and its tributaries provide a unique and intact habitat for four native species, including the Virgin spinedace, flannelmouth sucker, desert sucker and speckled dace. The Virgin spinedace is nationally significant and only exists in the Virgin River system. Both the Virgin River spinedace and the flannelmouth sucker are managed under conservation agreements. The Virgin River and several of its tributaries support regionally significant levels of natural and sustainable reproduction for all four native fish species. The North and East Forks of the Virgin River provide the most productive habitat for these fish in the Virgin River basin. The geologic setting, high flows, large sediment loads, unique water quality and frequent disturbance are effective deterrents to exotic species. Other factors contributing to the productivity of native fish are connectivity to tributary systems and habitat diversity for spawning, rearing and supporting adult fish. Additionally, the Zion stonefly (<em>Isogenoides zionensis</em>), an important component of the food web, is found along the Virgin River and its tributaries. The type specimen for this species was identified in Zion in 1949.</p>

<p><img src="../images/deep-creek-ut.jpg" style="padding-right: 10px; padding-bottom: 5px" align="left" alt="Deep Creek" width="341" height="400" title="Deep Creek"/><strong><em>Geologic</em></strong></p>

<p>The geologic formations through which Deep Creek flows are spectacular. Flowing unimpeded over the millennia, this narrow perennial stream has down cut through 2,000 feet of Jurassic age Navajo sandstone, sculpting narrow canyons, chutes and waterfalls and exposing brilliantly colored sedimentary deposits. Lush riparian vegetation is sustained along the stream channel by surface flows and discharge from the aquifer created by the Navajo sandstone.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Exceptional recreational opportunities exist along the Virgin River and its tributaries, providing visitors from around the world with a chance to develop personal and lasting connections with the river within some of the most unique water-carved desert canyons in the region.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Virgin River and its tributaries create diverse opportunities for views of the river's unparalleled scenery, which can be both dramatic and subtle. The river creates a landscape of cross-bedded sandstone cliffs, towering thousands of feet above the canyon floor.</p>

<p>River and tributary canyons offer a pleasing contrast in soil, rock, vegetation and water.  Views are greatly enhanced by the still or cascading water dominating the landscape. Light changes in the canyon depending on the time of day and the season. Rocks can appear fiery red, golden, bright white, grey, or black. Even the absence of water in some "phantom channels" creates drama and visual interest.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The extensive cover, availability of water and a contiguous landscape of wildlands creates habitat for a wide variety of animals. Mule deer, elk, mountain lions and bobcats are the larger animals that make a home here. Just a few of the smaller mammals include badgers, marmots and ringtail cats. Numerous bird species include golden eagles, screech owls, chukar partridges and wild turkeys. The remote canyons of the wilderness provide suitable nesting habitat for the Mexican spotted owl, a threatened species.</p>

<p><a href="https://www.blm.gov/visit/search-details/16317/2" title="Deep Creek" target="_blank">Bureau of Land Management &#8211; Deep Creek</a></p>
</div>

<h2 class="accordion-heading">Goose Creek</h2>

<div class="accordion-content">
<p>Goose Creek flows through wilderness at the start of the steep and deep Goose Creek Canyon to its junction with the North Fork of the Virgin River. The landscape is typical of high-elevation desert&#8212;dense tree stands on the slopes and thick riparian vegetation thriving in the canyon's steep-walled shade. A mix of ponderosa and pinyon pine, and Utah juniper grow on top of the sandstone cliffs.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>This creek features towering, water-carved cliffs, lengthy sections of narrow slot canyons and occasional patches of bushes and wildflowers. Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert, river-carved canyons forming the world's tallest sandstone cliffs, narrow slot canyons, hanging waterfalls, springs and seeps, and accelerated erosion processes. This dynamic geologic system creates a diverse landscape of channels, canyons and springs that support a variety of ecological communities, including hanging gardens, desert fish and other aquatic species. The geology of Virgin River and its tributaries offer world-class opportunities for canyoneering, rock climbing, hiking and wilderness experiences.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Birds, such as hawks and golden eagles, ride the air currents above the canyon walls, while a variety of mammals use both the uplands and the canyon bottoms. Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls and the endemic Zion snail.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16318/2" title="Goose Creek Wilderness" target="_blank">Bureau of Land Management &#8211; Goose Creek Wilderness</a></p>
</div>

<h2 class="accordion-heading">Kolob Creek</h2>

<div class="accordion-content">
<p>Kolob Creek begins atop the Kolob Plateau at 9,200 feet on the slopes of Kanarra Mountain. It flows across open meadowland for several miles through the Kolob Reservoir and basalt plateau before forming a canyon, and meeting up with the North Fork of the Virgin River. Kolob Creek canyon combines with Deep Creek and the Virgin River canyons to form Zion Canyon. Kolob Creek has the longest stretch of narrows of the three.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>Like other tributaries of the Virgin River, Kolob Creek is located along the western margin of the Colorado Plateau where the recent history of tectonic activity and erosional downcutting has resulted in a labyrinth of deep canyons, volcanic phenomena and widespread exposures of brilliantly colored sedimentary deposits. Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert, river-carved canyons forming the world's tallest sandstone cliffs, narrow slot canyons, hanging waterfalls, springs and seeps, and accelerated erosion processes. This dynamic geologic system creates a diverse landscape of channels, canyons and springs that support a variety of ecological communities, including hanging gardens, desert fish and other aquatic species. The geology of Virgin River and its tributaries offer world-class opportunities for canyoneering, rock climbing, hiking and wilderness experiences.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls, and the endemic Zion snail.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16319/2" title="Kolob Creek" target="_blank">Bureau of Land Management &#8211; Kolob Creek</a></p>

<p><a href="https://www.nps.gov/zion/planyourvisit/kolob-canyons.htm" title="Kolob Canyons" target="_blank">National Park Service &#8211; Kolob Canyons</a></p>
</div>

<h2 class="accordion-heading">LaVerkin Creek</h2>

<div class="accordion-content">
<p><img src="images/laverkin-creek.jpg" align="left" alt="LaVerkin Creek" width="329" height="248" title="LaVerkin Creek" style="padding-right: 10px; padding-bottom: 2px"/>LaVerkin Creek begins in Section 21, Township 38 South, Range 11 West, on Bureau of Land Management land and flows southwest through Zion National Park to the south end of Section 7, Township 40 South, Range 12 West.  It is typified by its steep canyon walls. Similar to Goose Creek and Bear Trap Canyon, the habitat value of LaVerkin Creek is greatly enhanced by its proximity to Zion National Park and the thousands of acres of remote, private wild lands surrounding it. Dense vegetation of pines, juniper and scrub oak; canyon-wall-created shade; access to water; and other factors create habitat suitable for a large of plants and animals.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Virgin River and its tributaries are uniquely situated along the western margin of the Colorado Plateau where the recent history of tectonic activity and erosional downcutting has resulted in a labyrinth of deep sandstone canyons, volcanic phenomena and widespread exposures of brilliantly colored sedimentary deposits. Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert, river-carved canyons forming the world's tallest sandstone cliffs, narrow slot canyons, hanging waterfalls, springs and seeps, and accelerated erosion processes. This dynamic geologic system creates a diverse landscape of channels, canyons and springs that support a variety of ecological communities, including hanging gardens, desert fish and other aquatic species. The geology of Virgin River and its tributaries offer world class opportunities for canyoneering, rock climbing, hiking, and wilderness experiences.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Exceptional recreational opportunities exist along the Virgin River and its tributaries, providing visitors from around the world with a chance to develop personal and lasting connections with the river within some of the most unique, water-carved desert canyons in the region. The dramatic setting, dominated by scenic grandeur, contribute to a spectrum of river-related experiences, from the self-reliant adventure of canyoneering or hiking and backpacking through narrow river and creek channels to enjoying photography and other artistic pursuits, viewing scenery, or camping along the river.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls, and the endemic Zion snail.</p>

<p>The federally threatened Mexican spotted owl breeds in all of the designated river corridors at the highest density in the state and region. Breeding occurs in the cool microclimates provided by the narrow canyons along the designated stream courses. As primary nesting habitat, the river corridors provide the core of the designated critical habitat identified in the recovery plan for this species. Occupied habitat with successful breeding occurs in each of the designated river segments and tributaries, including LaVerkin Creek.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16320/2" title="LaVerkin Creek" target="_blank">Bureau of Land Management &#8211; LaVerkin Creek</a></p>
</div>

<h2 class="accordion-heading">Oak Creek</h2>

<div class="accordion-content">
<p>Oak Creek begins in Section 19, Township 39 South, Range 10 West, and flows to its junction with Kolob Creek. Oak Creek is a classic wet slot canyon, typifying the erosion and downcutting of the sandstone in the area.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Virgin River and its tributaries are uniquely situated along the western margin of the Colorado Plateau, where the recent history of tectonic activity has resulted in a labyrinth of deep sandstone canyons, volcanic phenomena and widespread exposures of brilliantly colored sedimentary deposits. Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert and river-carved canyons. The geology of the Virgin River and its tributaries offers world-class opportunities for canyoneering, rock climbing, hiking and wilderness experiences.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls and the endemic Zion snail.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16323/2" title="Oak Creek" target="_blank">Bureau of Land Management &#8211; Oak Creek</a></p>
</div>

<h2 class="accordion-heading">Shunes Creek</h2>

<div class="accordion-content">
<p>Shunes Creek travels from the dry waterfall on land administered by the Bureau of Land Management through Zion National Park to the western boundary of Zion National Park, where it joins the East Fork of the Virgin River.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Ecologic Processes</em></strong></p>

<p>Ecological processes supporting vegetation is an outstandingly remarkable value in some of the Virgin River designated segments due to the presence of exemplary riparian corridors and rare plant communities. The cottonwood gallery forests along Shunes Creek provide rare examples of relatively intact, properly functioning riparian corridors. Natural river processes proceed unimpeded, allowing for seasonal flooding and meander migration, vegetative recruitment and plant succession. Riparian vegetation is abundant and diverse. Thick grasses and sedges along the banks form stable undercuts for fish habitat, and woody species provide habitat for numerous species of wildlife.</p>

<p>Steep-walled canyons, carved over time by the rivers, create cool, moist microclimates that support hanging gardens that are rare and exemplary in the region. These gardens, occurring on seeps vertical sandstone walls, support a complex biotic community including several plant and animal species found only in the Virgin River system.</p>

<p><strong><em>Fish</em></strong></p>

<p>The Virgin River and its tributaries provide a unique and intact habitat for four native species, including the Virgin spinedace, flannelmouth sucker, desert sucker and speckled dace. The Virgin spinedace is nationally significant, for it exists only in the Virgin River system. The geologic setting, high flows, large sediment loads, unique water quality and frequent disturbance are effective deterrents to exotic species. Other factors contributing to the productivity of native fish are connectivity to tributary systems and habitat diversity for spawning, rearing and supporting adult fish. Additionally, the Zion stonefly (<em>Isogenoides zionensis</em>, an important component of the food web, is found along the Virgin River and its tributaries.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls and the endemic Zion snail.</p>

<p>Desert bighorn sheep are listed as a sensitive species across the multi-state region. On Shunes Creek, the convergence of river-carved cliffs near-stream vegetation for forage and proximity of year-round water provides one of the few known locations for bighorn sheep lambing in the region. Lambing grounds are concentrated along this river segment and are exceptionally productive. The productivity of these lambing grounds are critical for the long-term reproductive success of the species, since Virgin River sheep disperse throughout the area and are the source for bighorn populations in much of the region. Research opportunities due to this population's success are regionally significant.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16324/2" title="Shunes Creek" target="_blank">Bureau of Land Management &#8211; Shunes Creek</a></p>
</div>

<h2 class="accordion-heading">Smith Creek</h2>

<div class="accordion-content">
<p>Smith Creek flows west to its junction with LaVerkin Creek.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Virgin River and its tributaries are uniquely situated along the western margin of the Colorado Plateau, where the recent history of tectonic activity and erosional downcutting has resulted in a labyrinth of deep sandstone canyons, volcanic phenomena and widespread exposures of brilliantly colored sedimentary deposits. Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert, river-carved canyons forming the world's tallest sandstone cliffs, narrow slot canyons, hanging waterfalls, springs and seeps, and accelerated erosion processes. This dynamic geologic system creates a diverse landscape of channels, canyons and springs that support a variety of ecological communities that, in turn, support hanging gardens, desert fish and other aquatic species. The geology of the Virgin River and its tributaries offer world-class opportunities for canyoneering, rock climbing, hiking and wilderness experiences.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of,desert bighorn sheep, Mexican spotted owlsand the endemic Zion snail.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16325/2" title="Smith Creek" target="_blank">Bureau of Land Management &#8211; Smith Creek</a></p>
</div>

<h2 class="accordion-heading">(Middle Fork) Taylor Creek</h2>

<div class="accordion-content">
<p>The Middle Fork of Taylor Creek is a portion of an isolated public land parcel managed by the BLM. The very upper reaches of the Middle Fork of Taylor Creek lie with wilderness. The stream channel is narrow and steep-sided, with dense stands of scrub oak, pinyon pine and Utah juniper on the mesa top.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Virgin River system is uniquely situated along the western margin of the Colorado Plateau, where the recent history of tectonic activity and erosional downcutting has resulted in a labyrinth of deep sandstone canyons, volcanic phenomena and widespread exposures of brilliantly colored sedimentary deposits. Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert, river-carved canyons forming the world's tallest sandstone cliffs, narrow slot canyons, hanging waterfalls, springs and seeps, and accelerated erosion processes.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The Virgin River and its tributaries, such as the Middle Fork of Taylor Creek, create diverse opportunities for views of the river's unparalleled scenery, which can be both dramatic and subtle. The river creates a landscape of cross-bedded sandstone cliffs, towering thousands of feet above the canyon floor.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls and the endemic Zion snail. Wilderness provides habitat for a diverse range of wildlife&#8212;mule deer, wild turkey, raptors, jackrabbits, squirrels, coyotes, bobcats, mountain lions, numerous reptiles and small mammals.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16321/2" title="Taylor Creek" target="_blank">Bureau of Land Management &#8211; Taylor Creek</a></p>
</div>

<h2 class="accordion-heading">(North Fork) Virgin River</h2>

<div class="accordion-content">
<p>The Virgin River and its tributaries create diverse opportunities for views of the river's unparalleled scenery, which can be both dramatic and subtle. The river creates a landscape of cross-bedded sandstone cliffs, towering thousands of feet above the canyon floor.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Ecologic Processes</em></strong></p>

<p>The ecological processes supporting vegetation is an outstandingly remarkable value due to the presence of exemplary riparian corridors and rare plant communities. Natural river processes proceed unimpeded, allowing for seasonal flooding and meander migration, vegetative recruitment and plant succession. Steep-walled canyons, carved over time by the rivers, create cool, moist microclimates that support hanging gardens that are rare and exemplary in the region. These gardens, occurring on seeps vertical sandstone walls, support a complex biotic community, including several plant and animal species found only in the Virgin River system. The gardens are home to seven species of plants that grow nowhere else in the world. The moist microclimate provided by the river adds to the diversity of plant species in these gardens, which in some cases includes up to 26 species.</p>

<p><strong><em>Fisheries</em></strong></p>

<p>The Virgin River and its tributaries provide a unique and intact habitat for four native species, including the Virgin spinedace, flannelmouth sucker, desert sucker and speckled dace. The Virgin spinedace is nationally significant and only exists in the Virgin River system. The North and East Forks of the Virgin River provide the most productive habitat for these fish in the Virgin River basin. The geologic setting and flow regime provide high flows, large sediment loads, unique water quality and frequent disturbance which are effective deterrents to exotic species. Additionally, the Zion stonefly (<em>Isogenoides zionensis</em>), an important component of the food web, is found along the Virgin River and its tributaries.</p>

<p><strong><em>Geologic</em></strong></p>

<p>The geologic tapestry of contrasting colors and textures&#8212;red, white and pink cliffs; slivers of blue sky; and lush green ribbons of riparian vegetation and hanging gardens&#8212;encompass the sculpted and undulating canyons. Seasonal waterfalls flow over slickrock form hanging canyons over 100 feet above the canyon floor.</p>

<img src="../images/virgin-nf.jpg" style="padding-right: 10px; padding-bottom: 5px" align="left" alt="North Fork of the Virgin River" width="400" height="535" title="North Fork of the Virgin River"/>

<p><br />Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert, river-carved canyons forming the world's tallest sandstone cliffs, narrow slot canyons, hanging waterfalls, springs and seeps, and accelerated erosion processes. This dynamic geologic system creates a diverse landscape of channels, canyons and springs that support a variety of ecological communities, in turn supporting hanging gardens, desert fish and other aquatic species.</p>

<p><br /><br /></p>

<p><strong><em>Recreational</em></strong></p>

<p>Exceptional recreational opportunities exist along the Virgin River and its tributaries, providing visitors from around the world with a chance to develop personal and lasting connections with the river within some of the most unique water-carved desert canyons in the region. The main recreation activity involves trekkers accessing Zion National Park. In the extreme canyon narrows portion, the river itself often becomes the hiking trail. The outstanding scenery and wilderness-like setting make the trek along the river unique and exceptionally satisfying. Day-use activities include hiking into portions of the canyon, nature photography, wildlife viewing and occasional hunting.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The scarcity of man-made structures; variety of natural shapes, textures and colors; and the gradual transition from a relatively open valley stream setting to a deeply entrenched, prominent slot canyon make the North Fork of the Virgin River exceptionally scenic and photogenic.  Light changes in the canyon depending on the time of day and the season. Rocks can appear fiery red, golden, bright white, grey, or black. Even the absence of water in some "phantom channels" creates drama and visual interest.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls and the endemic Zion snail. This segment includes possible neotropical migratory bird habitat. It is also Mexican spotted owl designated critical habitat.</p>

<p><a href="https://www.blm.gov/visit-us/search-details/16322/2" title="North Fork of the Virgin River" target="_blank">Bureau of Land Management &#8211; North Fork of the Virgin River</a></p>
</div>

<h2 class="accordion-heading">Willis Creek</h2>

<div class="accordion-content">
<p>Willis Creek travels from Bureau of Land Management land in Section 27, Township 38 South, Range 11 West, to its junction with LaVerkin Creek in Zion National Park. It offers an opportunity to add another unique, dramatic slot canyon hike for visitors to LaVerkin Creek.</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32" align="center">Outstandingly Remarkable Values</p>

<p><strong><em>Geologic</em></strong></p>

<p>The Virgin River and its tributaries are uniquely situated along the western margin of the Colorado Plateau, where the recent history of tectonic activity and erosional downcutting has resulted in a labyrinth of deep sandstone canyons, volcanic phenomena and widespread exposures of brilliantly colored sedimentary deposits. Unique geologic features include Navajo sandstone exposures, a remnant of the world's largest sand dune desert, river-carved canyons forming the world's tallest sandstone cliffs, narrow slot canyons, hanging waterfalls, springs and seeps, and accelerated erosion processes. This dynamic geologic system creates a diverse landscape of channels, canyons and springs that support a variety of ecological communities, in turn supporting hanging gardens, desert fish and other aquatic species. The geology of Virgin River and its tributaries offer world-class opportunities for canyoneering, rock climbing, hiking and wilderness experiences.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>Wildlife is an outstandingly remarkable value in the Virgin River and its tributaries due to the habitat for, and populations of, desert bighorn sheep, Mexican spotted owls and the endemic Zion snail.</p>

<p><a href="https://www.blm.gov/visit/search-details/16326/2" title="Willis Creek" target="_blank">Bureau of Land Management &#8211; Willis Creek</a></p>
</div>

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>