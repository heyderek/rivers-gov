<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Pratt River, Washington';

// Set the page keywords
$page_keywords = 'Pratt River, Washington, Mt. Baker-Snoqualmie National Forest';

// Set the page description
$page_description = 'Pratt River, Washington';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('210');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Mt. Baker-Snoqualmie National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>December 19, 2014. The entire 9.5-mile Pratt River, from its headwaters to its confluence with the Middle Fork of the Snoqualmie River.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 9.5 miles; Total &#8212; 9.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/pratt.jpg" alt="Pratt River" title="Pratt River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<!--<h3>RELATED LINKS</h3>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/westfield_pwsr_sub.html" alt="Westfield River (National Park Service)" target="_blank">Westfield River (National Park Service)</a></p>-->

<div id="photo-credit">
<p>Photo Credit: Monty VanderBilt</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Pratt River</h2>
<p>Like the Snoqualmie River it empties into, the Pratt River is an exceptionally scenic waterway originating in the Alpine Lakes Wilderness. A refurbished trail provides access to this beautiful mountain stream, especially for the nearby Seattle Metropolitan Area. The river flows through old-growth forest and supports native trout runs, black bear, elk and cougar.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>