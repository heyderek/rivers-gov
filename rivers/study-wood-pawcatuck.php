<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Wood &amp; Pawcatuck Rivers, Rhode Island';

// Set the page keywords
$page_keywords = 'Wood River, Pawcatuck Rivers Rhode Island, National Park Service';

// Set the page description
$page_description = 'Wood &amp; Pawcatuck Rivers, Rhode Island';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('303');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<!--<?php
// includes ESRI
include ('../iframe.php');
?>-->

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<center><img src="images/wood-pawcatuck.jpg" alt="Wood-Pawcatuck Rivers" width="509" height="253" title="Wood-Pawcatuck Rivers"/></center>
<br />
<h3>Study Agency:</h3>
<p>National Park Service</p>
<br />
<h3>Study Segments:</h3>
<p>December 19, 2014 (Public Law 113-291). The Wood River, including all the tributaries in the upper Wood River (headwaters to Skunk Hill Road in Richmond and Hopkinton, Rhode Island). The Pawcatuck River from Worden Pond to Pawcatuck. The Beaver River from its headwaters in Exeter, Rhode Island, to the confluence with the Pawcatuck River. The Chipuxet River from Hundred Acre Pond to the outlet into Worden Pond. The Queen River from its headwaters to the reservoir of Usquepaugh Dam in South Kingstown, Rhode Island, including all tributaries. The Queen (Usquepaugh) River from the Usquepaugh Dam to the confluence with the Pawcatuck River.</p>
<br />
<h3>Mileage:</h3>
<p>86.0 miles plus tributaries.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/wood-pawcatuck2.jpg" alt="Wood-Pawcatuck Rivers" title="Wood-Pawcatuck Rivers" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../../documents/studies/wood-pawcatuck-study.pdf" title="Wood-Pawcatuck RiverS Reconnaissance Survey" target="_blank" alt="Wood-Pacatuck Rivers Reconnaissance Survey (National Park Service)">Wood-Pawcatuck Rivers Reconnaissance Survey (National Park Service)</a></p>
<p><a href="http://wpwildrivers.org/" title="Wood-Pawcatuck Rivers Study Web Site" target="_blank" alt="Wood-Pawcatuck Rivers Study Web Site">Wood-Pawcatuck Rivers Study Web Site</a></p>

<div id="photo-credit">
<p>Photo Credit: Wood-Pawcatuck Watershed Association</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Wood &amp; Pawcatuck Rivers</h2>
<p>The Wood and Pawcatuck River systems lie in southeastern Connecticut and southwestern Rhode Island. The source of the Pawcatuck River is in the town of South Kingston, Rhode Island, and its terminus is in the town of Westerly, Rhode Island, and Stonington, Connecticut, where it drains into Little Narragansett Bay (Long Island Sound). The coastal town of Westerly is a popular tourism destination, with its scenic views of Narragansett Bay; it has long been a destination for those seeking a beach community vacation. The watershed area is approximately 300 square miles, encompassing many high-quality tributaries within seven major drainage areas, including the Queen, Wood, Chickasheen, Chipuxet, Shunock, Green Falls and Pawcatuck Rivers. It is one of the few remaining relatively pristine natural areas along the northeast corridor between New York and Boston.</p>
<p>The Pawcatuck River is 38 miles long and the Wood River, its major tributary, is 27 miles long. The Pawcatuck River and its associated tributaries run through a rural wooded landscape amongst a series of towns that grew up on the banks of the watercourses, historically as mill villages. Vestiges of the textile and fabric dyeing industry can still be found on the banks of the rivers. The watershed is the most rural, least developed in Rhode Island, with approximately 87&#37; of the land undeveloped and approximately 75&#37; forested. The estuary of the Pawcatuck River winds its way through the more highly developed communities of Pawcatuck, Connecticut, and Westerly, Rhode Island. Development pressure is high in this region as is typical in the states along the Atlantic coastline.</p>
<p>The Wood-Pawcatuck Watershed is a good candidate for a wild and scenic river designation based on the preliminary evidence of free-flowing river conditions and the presence of multiple natural, cultural and recreational resources with the potential to meet the "Outstandingly Remarkable Value" threshold as defined by the Wild and Scenic Rivers Act. There is demonstrated local and regional interest and support for a study and existing river/watershed protection elements that would support the NPS framework for a Partnership Wild and Scenic River designation. In addition, local stakeholders have indicated an initial level of interest in developing the river management plan that would be developed as a part of the study process, which is required as a part of the designation.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>