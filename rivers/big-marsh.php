<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Big Marsh Creek, Oregon';

// Set the page keywords
$page_keywords = 'Deschutes National Forest, Big Marsh Creek, Oregon';

// Set the page description
$page_description = 'Big Marsh Creek, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('68');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Deschutes National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From NE 1/4 Section 15, T26S, R6E to the confluence with Crescent Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 15.0 miles; Total &#8212; 15.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/big-marsh.jpg" alt="Big Marsh Creek" title="Big Marsh Creek" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/big-marsh-creek-little-deschutes-plan.pdf" alt="Big Marsh Creek Management Plan (110 KB PDF)" target="_blank">Big Marsh Creek Management Plan (110 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Big Marsh Creek</h2>
<p>Big Marsh Creek meanders through a rare high-mountain marsh to its confluence with Crescent Creek. The upper portion of the river is in the Oregon Cascades National Recreation Area where there are great opportunities for viewing scenery and observing wildlife. The river has outstandingly remarkable scenic, vegetation, geologic and wildlife values.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>