<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Minam River, Oregon';

// Set the page keywords
$page_keywords = 'Minam River, Wallowa-Whitman National Forest, Eagle Cap Wilderness, Oregon';

// Set the page description
$page_description = 'Minam River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';

//ID for the rivers
$river_id = array('86');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Wallowa-Whitman National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its headwaters at the south end of Minam Lake to the Eagle Cap Wilderness boundary, one-half mile downstream from Cougar Creek.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 41.9 miles; Total &#8212; 41.9 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/minam.jpg" alt="Minam River" title="Minam River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.fs.usda.gov/detail/wallowa-whitman/specialplaces/?cid=stelprdb5227105" alt="Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest" target="_blank">Wild &amp; Scenic Rivers &#8211; Wallowa-Whitman National Forest</a></p>
<p><a href="../documents/plans/minam-plan.pdf" title="Minam River Management Plan" target="_blank">Minam River Management Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Minam River</h2>
<p>The Minam Wild and Scenic River is located entirely within the Eagle Cap Wilderness in northeast Oregon on the Wallowa-Whitman National Forest. Originating out of Blue and Minam Lakes, this 77-mile-long river is classified as "wild." Its outstandingly remarkable values include recreation, scenery, fisheries, wildlife and geology.</p>
<p>Visitors can only access the river by hiking or horseback riding the wilderness trails, or flying into Reds Horse Ranch. The ranch is a key feature along the river and was acquired by the Forest Service in the mid-1990s. Once a locally famous dude ranch and lodge, it is now managed as a historic site and a wilderness backcountry facility. Although no longer available as an overnight facility, it is open to the public for viewing and visitation by volunteer staff in the summer months. The primitive airstrip at Reds Horse Ranch is also open for public landings and for limited commercial use (permit required).</p>
<p>The variety in landscapes along the river is dramatic and memorable, from the steep and glaciated upper drainage to the heavily forested U-shaped valley in the middle portion to the deeply dissected basalt drainages of the lower canyon.</p>
<p>Since the entire drainage lies within the Eagle Cap Wilderness, it is an area where natural processes dominate. The diversity of unaltered habitat includes a wide range of solitude-dependent animals, such as wolverine, fisher, Rocky Mountain bighorn sheep, wolf, mule deer, Rocky Mountain elk, black bear, river otter, bald eagle and cougar. The area is considered a premiere reservoir for big-game species.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>