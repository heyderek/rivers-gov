<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Little Deschutes River, Oregon';

// Set the page keywords
$page_keywords = 'Deschutes National Forest, Little Deschutes River, Oregon';

// Set the page description
$page_description = 'Little Deschutes River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('81');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>U.S. Forest Service, Deschutes National Forest</p>
<br />
<h3>Designated Reach:</h3>
<p>October 28, 1988. From its source in the northwest 1/4 of Section 15, T26S, R6E, to the north section line of Section 12, T26S, R7E.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 12.0 miles; Total &#8212; 12.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/little-deschutes.jpg" alt="Little Deschutes River" title="Little Deschutes River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="../documents/plans/big-marsh-creek-little-deschutes-plan.pdf" alt="Little Deschutes River Management Plan (110 KB PDF)" target="_blank">Little Deschutes River Management Plan (110 KB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Little Deschutes River</h2>
<p>The Little Deschutes River offers outstandingly remarkable geology, scenery and vegetation values. It flows through a classic glacial canyon with moraines and an outwash plain. The interaction of present-day erosional processes with the pumice and ash deposited during Mt. Mazama's eruption 6,800 years ago provides an opportunity for geomorphic study. The river is also noted for its scenic variety and vegetative character.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>