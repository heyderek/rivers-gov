<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'American River (Lower), California';

// Set the page keywords
$page_keywords = 'American River, California';

// Set the page description
$page_description = 'American River (Lower), California';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

//ID for the rivers
$river_id = array('SD7');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?>

<?php
// includes ESRI
include ('../iframe.php');
?>

<?php include_once ("../iframe.php"); ?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>California Resources Agency</p>
<br />
<h3>Designated Reach:</h3>
<p>January 19, 1981. From the confluence with the Sacramento River to the Nimbus Dam.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Recreational &#8212; 23.0 miles; Total &#8212; 23.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/american-lower.jpg" alt="Lower American River" title="Lower American River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">

<h3>RELATED LINKS</h3>
<p><a href="../documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="American River Eligibility Report" target="_blank">American River Eligibility Report</a><br />
<a href="../documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="American River Environmental Impact Statement" target="_blank">American River Environmental Impact Statement</a></p>

<div id="photo-credit">
<p>Photo Credit: Tim Palmer</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">

<h2>American River (Lower)</h2>
<p>This short stretch of river, flowing through the city of Sacramento, is the most heavily used recreation river in California. It provides an urban greenway for trail and boating activities and is also known for its runs of steelhead trout and salmon.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>