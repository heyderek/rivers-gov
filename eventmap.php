<?php

// Set the page title  -- GENERAL TEMPLATE 2

$page_title = '50th Anniversary Event Map';

// Set the page keywords

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description

$page_description = 'The National Wild and Scenic Rivers System.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'general';

// Includes the meta data that is common to all pages

include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php

// includes the TEMPLATE HEADER CODING -- #content-page

include ("includes/header.php")

?>

<?php

// includes the content page top

include ("includes/content-head.php")

?>

<div id="intro-box">

<h2>Add Your Anniversary Event!</h2>

<p>How is your river organization planning to celebrate the 50th anniversary of the Wild and Scenic Rivers Act? Log your event information on our national rivers event map for better publicity and national recognition for your efforts &#8212; and of course, see what others are doing!.</p>

<p align="center" style="font-size: 15pt">50th Anniversary Event Map &amp; Entry Form</p>

</div>

<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->

<center>
<a href="https://umontana.maps.arcgis.com/apps/MapJournal/index.html?appid=f85c819b95f14ae1a44400e46ee9578c" title="50th Anniversary Event Map" target="_blank"><img src="images/eventmap.jpg" alt="50th Anniversary Event Map" width="565px" height="121px" title="50th Anniversary Event Map" /></a>
</center>

<div id="lower-content">

<div id="lc-left">

<h2>Don't Know How You'll Celebrate?</h2>

<p>Visit our event planning toolkit for event ideas, logo and marketing resources and more.</p>

</div>

<!--END #lc-left -->

<div id="block-quote">

<h4><a href="https://www.rivers.gov/toolkit.php">Event Planning Toolkit</a></h4>

</div>

<!--END #block-quote -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div><!--END #lower-content -->

<?php

// includes the content page bottom

include ("includes/content-foot.php")

?>

<?php

// includes the TEMPLATE FOOTER CODING -- </html>

include ("includes/footer.php")

?>
				