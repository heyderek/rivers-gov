<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Washington';
// Set the page keywords
$page_keywords = 'Washington';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Washington.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'WA';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Washington has approximately 70,439 miles of river, of which 197 miles are designated as wild &amp; scenic&#8212;less than 3/10ths of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>


<ul>
<li><a href="rivers/illabot.php" title="Illabot Creek">Illabot Creek</a></li>
<li><a href="rivers/klickitat.php" title="Klickitat River">Klickitat River</a></li>
<li><a href="rivers/pratt.php" title="Pratt River">Pratt River</a></li>
<li><a href="rivers/skagit.php" title="Skagit River">Skagit River</a></li>
<li><a href="rivers/snoqualmie-mf.php" title="Snoqualmie (Middle Fork) River">Snoqualmie (Middle Fork) River</a></li>
<li><a href="rivers/white-salmon.php" title="White Salmon River">White Salmon River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>