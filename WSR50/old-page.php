<?php

// Set the page title  -- GENERAL TEMPLATE 2

$page_title = 'Wild &amp; Scenic Rivers Act 50th Anniversary';

// Set the page keywords

$page_keywords = ', 50 years, anniversary, rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description

$page_description = 'The 50th anniversary of the National Wild and Scenic Rivers System.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'general';

// Includes the meta data that is common to all pages

include ("../includes/metascript.php");

?>

    <!-- BEGIN page specific CSS and Scripts -->

    <!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("../includes/header.php")
?>

<?php
// includes the content page top
include ("../includes/content-head.php")
?>

    <div id="open-content">

        <div style="margin: 0px 0px 0px 0px"><img src="../images/feather.jpg" align="center" alt="Feather River" height="391" vspace="10" title="Feather River, One of the Original Eight Designated Rivers" width="535" /><div style="font-size:12px; font-style:italic; color:#1B4C8C; margin: 0px 0px 0px 0px">The Feather River, One of the Original Eight Designations; Photo by Tim Palmer</div></div>

        <p>&nbsp;</p>

        <p><img src="../images/wsr-50th-logo.jpg" alt="50th Anniversary Logo" height="150" style="margin: 0px 0px 0px 15px; float: right" width="110" title="50th Anniversary Logo" /><font style="font-size:24px; color:#1B4C8C; font-style:italic">1968.</font> A year of profound change. The Tet Offensive and Vietnam protests. The assassinations of Robert Kennedy and Dr. Martin Luther King, Jr. A standoff with North Korea over the capture of the USS Pueblo. Racial protests&#8212;even at the Mexico Olympics. Escalation of the Cold War; the Soviet Union invades Czechoslovakia. Riots at the Democratic Convention in Chicago. The transition of “Flower Power” to a more somber time in the country.</p>

        <p>But it was also a year of new hope and growth. A manned spacecraft first orbits the moon. The Paris Peace Talks. The beginning of the end to Vietnam.</p>

        <p>And it was a year of profound progress in environmental protection. “Sustainability” enters the lexicon with the publication of <em>The Population Bomb</em> and <em>The Whole Earth Catalog</em>. “Earthrise” from Apollo 8 defines a new perspective. Six new national park units. Over 800,000 acres of wilderness protected. The National Trails System Act.</p>

        <p>And on October 2, 1968, the Wild &amp; Scenic Rivers Act.</p>

        <p>There are approximately 3.6 million miles of streams in the United States; 1.1 million are at least five miles in length. Only 12,709 miles are protected by the Wild &amp; Scenic Rivers Act&#8212;only 0.35% of the rivers found here.</p>

        <p>But what a wonderful 12,709 miles! Allagash. Salmon. Snake. Missouri. Concord. Fortymile. Trinity.</p>

        <p>As the Act nears a half century of protecting some of our greatest rivers, we hope you’ll join us in celebrating its accomplishments&#8212;and in working for its future. While there is much we have to do, there is much we have done, and to the thousands of people across the country that have worked tirelessly to save their local river, it’s time to take a moment to celebrate, to congratulate each other, to look forward. To add to the 12,709 miles.</p>

        <p>Check back as we introduce new pages, install new maps, and create a Facebook presence to follow celebratory events. In the meantime, check out our new Flickr account. And if you need a copy of the 50th anniversary logo for your event, help yourself&#8212;and let us know about your event.</p>

        <p>&nbsp;</p>

        <hr style="float: center" width="50%">

        <p>&nbsp;</p>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="48%">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><a href="files/50th-wsr-logo.zip"><img src="../images/wsr-50th-logo-download.jpg" width="74" height="100" alt="Download 50th Anniversary Logo" title="Download 50th Anniversary Logo" /></a></td>
                            <td width="10"></td>
                            <td>If you need a copy of the 50th anniversary logo, click on the image to the left for a zipped file of different options. Guidelines for use are included in the file.</td>
                        </tr>
                    </table>
                </td>
                <td width="4%">&nbsp;</td>
                <td width="48%">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><a href="https://www.flickr.com/photos/wild_rivers/" target="_blank"><img src="../images/flickr.jpg" width="74" height="100" alt="Visit Our Flickr Site" title="Visit Our Flickr Site" /></a></td>
                            <td width="10"></td>
                            <td>Our new Flickr site has some stunning images of the National Wild &amp; Scenic Rivers System. Click on the image to the left.</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


    </div>
    <!-- End #open-content -->

    <div class="clear"></div>
    <!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("../includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("../includes/footer.php")
?>