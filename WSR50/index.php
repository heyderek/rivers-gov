﻿<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Wild &amp; Scenic Rivers Act 50th Anniversary';

// Set the page keywords
$page_keywords = '50 years, anniversary, rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = '50th Anniversary of the National Wild and Scenic Rivers System';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include( "../includes/metascript.php" );

//Turn off sidebar for this page.
$sidebar_bool = 0;
$body_class   = "full-width";
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include( "../includes/header.php" )
?>

<div id="inner-wrapper">

<div id="content">

<div id="content-wrapper">

<div id="content-page">

<div id="nwsrs_intro_hero" class="nwsrs-section">

<div class="nwsrs-row">

<div class="nwsrs-columns text-center collapse">

<div class="nwsrs-hero-message">
<h1><span><em>&mdash; Celebrating 50 Years &mdash;</em></span><br/>Wild &amp; Scenic Rivers System</h1>
<img src="../images/nwsrs-50-hero.png" alt="Celebrating 50 years of National Wild and Scenic Rivers System"/>
</div>

</div>

</div>

</div>

<div class="nwsrs-main-content nwsrs-section">

	<div class="nwsrs-row">

		<div class="nwsrs-50 nwsrs-columns nwsrs-vert-padded-column">

			<div class="nwsrs-hero-message">
			<!--<h2>Welcome to the 50th Anniversary</h2>-->
			<p><strong>1968.</strong> A year of profound change. The Tet Offensive and Vietnam protests. The assassinations of Robert Kennedy and Dr. Martin Luther King, Jr. A standoff with North Korea over the capture of the USS Pueblo. Racial protests&#8212;even at the Mexico Olympics. Escalation of the Cold War; the Soviet Union invades Czechoslovakia. Riots at the Democratic Convention in Chicago. The transition of “Flower Power” to a more somber time in the country.</p>
			<p>But it was also a year of new hope and growth. A manned spacecraft first orbits the moon. The Paris Peace Talks. The beginning of the end to Vietnam.</p>
			<p>And it was a year of profound progress in environmental protection. “Sustainability” enters the lexicon with the publication of <em>The Population Bomb</em> and <em>The Whole Earth Catalog</em>. “Earthrise” from Apollo 8 defines a new perspective of our world. Six new national park units. Over 800,000 acres of wilderness protected. The National Trails System Act.</p>
			</div>

		</div>

		<div class="nwsrs-50 nwsrs-columns collapse">

			<div class="nwsrs-hero-message">
			<img src="../images/nwsrs-50-boy-jumping.jpg" alt="Boy jumping into river"/>
			</div>

		</div>

	</div>

	<div class="nwsrs-row nwsrs-banner-cta">

		<div class="nwsrs-50 nwsrs-columns nwsrs-vert-padded-column">
		<h2><a href="https://www.flickr.com/photos/wild_rivers/" target="_blank">Find us on Flickr</a> and witness the beauty of the rivers. <a href="https://www.flickr.com/photos/wild_rivers/" target="_blank">See it now &rarr;</a></h2>
		</div>

		<div class="nwsrs-50 nwsrs-columns collapse">
		<img src="../images/flickr-screen.png" alt="find us on Flickr" id="flickr_teaser"/>
		</div>

	</div>

	<div class="nwsrs-row">

		<div class="nwsrs-50 nwsrs-columns collapse">

			<div class="nwsrs-hero-message">
			<img src="../images/nwsrs-50-delaware-bridge.jpg" alt="The foggy Delaware river"/>
			</div>

		</div>

		<div class="nwsrs-50 nwsrs-columns nwsrs-vert-padded-column">

			<div class="nwsrs-hero-message">
			<p>And on October 2, 1968, the Wild &amp; Scenic Rivers Act.</p>
			<p>There are approximately 3.6 million miles of streams in the United States; 1.1 million are at least five miles in length. Only 12,734 miles are protected by the Wild &amp; Scenic Rivers Act&#8212;only 0.35% of the rivers found here.</p>
			<p>But what a wonderful 12,734 miles! Allagash. Salmon. Snake. Missouri. Concord. Fortymile. Trinity.</p>
			<p>As the Act nears a half century of protecting some of our greatest rivers, we hope you’ll join us in celebrating its accomplishments&#8212;and in working for its future. While there is much we have to do, there is much we have done, and to the thousands of people across the country that have worked tirelessly to save their local river, it’s time to take a moment to celebrate, to congratulate each other, to look forward. To add to the 12,734 miles.</p>
			</div>

		</div>

	</div>

	<div class="nwsrs-row">

		<div class="nwsrs-columns nwsrs-callout nwsrs-vert-padded-column">
		<p>Check out our <a href="../toolkit.php" title="50th Anniversary Toolkit" style="font-size: 95%">50th Anniversary Toolkit</a> to learn more about upcoming events, event planning and how to connect people to their river, their watershed, their heritage. If you need a copy of the <a href="files/50th-wsr-logo.zip" title="50th Anniversary Logo" style="font-size: 95%">50th Anniversary Logo</a> for your event, help yourself. And be sure to let us know about your event through our <a href="https://umontana.maps.arcgis.com/apps/MapJournal/index.html?appid=f85c819b95f14ae1a44400e46ee9578c" title="50th Anniversary Event Map" target="_blank" style="font-size: 95%">Event Map</a>. Need a photo for event promotion? Our <a href="https://www.flickr.com/photos/wild_rivers/" title="Flickr" target="_blank" style="font-size: 95%">Flickr</a> site is just the spot to start.</p>
		</div>

	</div>

	<div class="nwsrs-row">

		<div class="nwsrs-columns">
		<h2 id="connect_heading" class="text-center">Connect with us</h2>
		<hr>
		</div>

	</div>

	<div class="nwsrs-row nwsrs-social-assets">

		<div class="nwsrs-50 nwsrs-columns nwsrs-vert-padded-column">

			<div class="nwsrs-row">

				<div class="nwsrs-25 nwsrs-columns">
				<a href="files/50th-wsr-logo.zip"><img src="../images/wsr-50th-logo-download.jpg" alt="Wild &amp; Scenic Rivers System 50 Logo" title="Wild &amp; Scenic Rivers System 50 Logo" /></a>
				</div>

				<div class="nwsrs-75 nwsrs-columns">
				<a href="files/50th-wsr-logo.zip" alt="50th Anniversary Branding Kit" title="50th Anniversary Branding Kit">Download 50th Anniversary<br>Brand Package</a>
				<p>If you need a copy of the 50th anniversary logo, click on the image to the left for a zipped file of different options. Guidelines for use are included in the file.<br /> <br /><br /></p>
				</div>

			</div>

			<div class="nwsrs-row">

				<div class="nwsrs-25 nwsrs-columns">
				<a href="http://wsr50.onlinepresskit247.com" alt="Press Kit" target="_blank"><img src="../images/press-kit.jpg" title="Press Kit" /></a>
				</div>

				<div class="nwsrs-75 nwsrs-columns">
				<a href="http://wsr50.onlinepresskit247.com" alt="Press Kit" target="_blank">Press Kit</a>
				<p>Use our online press kit for interview ideas, interview questions, stories ideas, photos and more.</p>
				</div>

			</div>

		</div>

		<div class="nwsrs-50 nwsrs-columns nwsrs-vert-padded-column">

			<div class="nwsrs-row">

				<div class="nwsrs-25 nwsrs-columns ">
				<a href="https://www.flickr.com/photos/wild_rivers/" alt="WSR Flickr Site" title="WSR Flickr Site"><img src="../images/flickr.jpg" alt="NWSR on Flickr"/></a>
				</div>

				<div class="nwsrs-75 nwsrs-columns">
				<a href="https://www.flickr.com/photos/wild_rivers/" title="WSR Flickr Site" target="_blank">Visit us on Flickr</a>
				<p> Our new Flickr site has some stunning images of the National Wild & Scenic Rivers System.</p>
				</div>

			</div>

		</div>

	</div>

</div>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include( "../includes/content-footNEW.php" )
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include( "../includes/footer.php" )
?>