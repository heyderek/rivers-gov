<?php

// Set the page title
$page_title = 'USA National Wild and Scenic Rivers | www.rivers.gov | ';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'The National Wild and Scenic Rivers System.';

$body_class = 'home-page';

// Includes the meta data that is common to all pages
include("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<link type="text/css" href="<?php echo $base_url; ?>style/gallery.css" rel="stylesheet"/>

<style type="text/css">
	#photocredit {
	border: 1px solid #ccc;
	width: 822px;
	height: 105px;
	margin: 35px auto 25px 70px;
	padding: 0px;
	position: static;
	/* Adds rounded corners */
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	}
</style>

<style type="text/css">
	#photocredittext {
	font-style: italic;
	float: left;
	padding: 15px 15px 10px 18px;
	}
</style>

<style type="text/css">
	#fiftyth {
	border: 1px solid #ccc;
	width: 822px;
	height: 105px;
	margin: 35px auto 25px 70px;
	padding: 0px;
	position: static;
	/* Adds rounded corners */
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	}
</style>

<style type="text/css">
	#fiftythtext {
	font-style: normal;
	float: left;
	padding: 15px 15px 10px 18px;
	}
</style>

<script type="text/javascript" src="<?php echo $base_url; ?>scripts/jquery.pikachoose.js"></script>

<script language="javascript">
	$(document).ready(
		function () {
		$("#pikame").PikaChoose({transition: [5]});
	});
</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the #masthead and #navmenu
include("includes/header.php")
?>

<div id="slideshow">

	<div id="photo-edges">
	</div>


	<div class="pikachoose">

		<div id="slideshow-text">
		<h1>DID YOU KNOW...</h1>
		<p>Less than 1/4 of 1% of our rivers are protected under the National Wild &amp; Scenic Rivers System.</p>
		</div>

	<ul id="pikame">
	<li><img alt="Westfield River, Massachusetts" title="Westfield River, Massachusetts" src="images/large/slide-1.jpg"></li>
	<li><img alt="Au Sable River, Michigan" title="Au Sable River, Michigan" src="images/large/slide-2.jpg"></li>
	<li><img alt="Black River, Michigan" title="Black River, Michigan" src="images/large/slide-3.jpg"></li>
	<li><img alt="Crooked River, Oregon" title="Crooked River, Oregon" src="images/large/slide-4.jpg"></li>
	<li><img alt="Delta River, Alaska" title="Delta River, Alaska" src="images/large/slide-5.jpg"></li>
	<li><img alt="John Day River, Oregon" title="John Day River, Oregon" src="images/large/slide-6.jpg"></li>
	<li><img alt="Namekagon River, Wisconsin" title="Namekagon River, Wisconsin" src="images/large/slide-7.jpg"></li>
	<li><img alt="Ontonagon River, Michigan" title="Ontonagon River, Michigan" src="images/large/slide-8.jpg"></li>
	<li><img alt="Rio Grande, New Mexico" title="Rio Grande, New Mexico" src="images/large/slide-9.jpg"></li>
	<li><img alt="Rio Icacos, Puerto Rico" title="Rio Icacos, Puerto Rico" src="images/large/slide-10.jpg"></li>
	<li><img alt="Wekiva River (Rock Springs Run), Florida" title="Wekiva River (Rock Springs Run), Florida" src="images/large/slide-11.jpg"></li>
	<li><img alt="Wolf Creek, Wyoming" title="Wolf Creek, Wyoming" src="images/large/slide-12.jpg"></li>
	</ul>
	</div>

</div>
<!--END #slideshow -->

<script>
	$(document).ready(function () {
		// Fix Anchor links in the slideshow
		// that throw accessibiliy errors.
		// Slideshow auto-builds the HTML
		(function pikaFixAnchorLinks() {
			var anchors = $('a:not([href])');
			$(anchors).each(function (e) {
				var className = $(this)[0].className;
				$(this).attr('href', '#');
				$(this).attr('title', className);
				$(this)[0].text += className;
			});
		})();
	})
</script>

<div id="home-inner-wrapper">

	<div class="row collapse nwsrs-50-homepage-hero">

		<div class="col-50 column push-50">
		<h3>Celebrating 50 years of the Wild &amp; Scenic Rivers Act</h3>
		<p>In 2018, the Wild &amp; Scenic Rivers Act turns 50. The Wild &amp; Scenic Rivers Council, together with other partners like American Rivers and the River Management Society, are thinking about the best way to celebrate this milestone.<br/><br/>To learn about this historic event, please visit our <a href="wsr50/index.php">50th Anniversary</a> page.<br/><br/>And look for Tim Palmer's new book, <em>Wild and Scenic Rivers: An American Legacy.</em></p>
		</div>

	</div>

	<div id="home-content">

		<div class="nwsrs-story-map-container">
		<article class="nwsrs-story-map-content">
		<h3>National Wild and<br/>Scenic Rivers Story Map</h3>
		<p>See a series of interactive maps showing management,<br/> classification and river values.</p>
		<a href="https://nps.maps.arcgis.com/apps/MapJournal/index.html?appid=ba6debd907c7431ea765071e9502d5ac" class="map-button" target="_blank">View the Map</a></article>
		</div>

		<div id="feature-1">
		<!-- Map Button assigned as display:block; DO NOT MOVE -->
		<a href="map.php" class="map-btn home-btn" title="River Map"></a>
		<h1>A National System</h1>
		<p><em>In the past 50 years, we have learned&#8212;all too slowly, I think&#8212;to prize and protect God's precious gifts. Because we have, our own children and grandchildren will come to know and come to love the great forests and the wild rivers that we have protected and left to them . . . An unspoiled river is a very rare thing in this Nation today. Their flow and vitality have been harnessed by dams and too often they have been turned into open sewers by communities and by industries. It makes us all very fearful that all rivers will go this way unless somebody acts now to try to balance our river development.</em><br/>&#8211; President Lyndon Johnson on signing the Wild & Scenic Rivers Act, October 2, 1968.<br/><span style="font-style:italic; color:#81080A">[Note: This is an historical citation, not an endorsement of religion. These are the historic remarks made by a U.S. President.]</span>
		<a href="national-system.php" alt="FIND OUT MORE" class="findmore">+ FIND OUT MORE</a></p>
		</div>
		<!--END #feature-1 -->

		<div class="row clearfix callout">

			<div class="col-100 column">
			<h3 class="pull-title">About the Wild and Scenic Rivers Act</h3>
			<img alt="Mule Deer crossing the stream" class="float-right" src="images/mule-deer.png"/>
			<p>The country changed in the 1960s, including our treatment of the environment, leading to the Wilderness, Clean Air, Clean Water and National Environmental Policy Acts &#8211; and the Wild &amp; Scenic Rivers Act. <a href="wsr-act.php" alt="FIND OUT MORE" class="findmore">+ FIND OUT MORE</a></p>
			</div>

		</div>

<!--<div class="row clearfix callout">-->
<!--<div class="column">-->
<!--<h3 class="pull-title">50th Anniversary of the Act</h3>-->
<!--</div>-->
<!--<div class="col-12 column">-->
<!--<a href="wsr50/index.php"><img src="images/wsr-50th-logo.png" width="52" height="70"-->
<!--alt="50th Anniversary Logo"-->
<!--class="center-align"-->
<!--title="50th Anniversary Logo"/></a>-->
<!--</div>-->
<!--<div class="col-87 column">-->
<!--<p>In 2018, the Wild &amp; Scenic Rivers Act turns 50. The Wild &amp; Scenic Rivers Council, together with other partners like American Rivers and the River Management Society, are thinking about the best way to celebrate this milestone.<br/><br/>To learn about this historic event, please visit our <a href="wsr50/index.php">50th Anniversary</a> page.</p>-->
<!--</div>-->
<!--</div>-->

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include("includes/footer.php")
?>