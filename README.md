# Rivers.gov #

National Wild and Scenic River System project for nation-wide river information.

## Table of Contents: ##
* Setup
* Grid System and Layouts
* Image Alignment

---

## Setup ##

* Project files require a number of assets (.pdfs and more) not managed in this repository.
* Metascript.php holds a global PHP variable to construct relative links that needs to be updated to the domain in which the site will reside.

---

##Grid System and Layouts ##

The latest version of the Rivers.gov website is capable of creating dynamic layouts through the use of a simple grid system. The system, constructed to mimic that of [Zurb's Foundation](http://foundation.zurb.com/) grid, can be quickly configued through the use of rows and columns.

### Constructing a layout ###

To construct a layout, simply create a row for columns to reside with the `.row` selector class.

~~~~
<div class="row"></div>
~~~~

With the row in place, you can now populate the section with columns to contain your content. To declare a column, simply add the `.column` HTML class. 

~~~~

<div class="row">
    <div class="column"></div>
</div>

~~~~

### Multi-Column Layouts

Sometimes a row needs to contain multiple columns. For this, you declare column widths simply add the HTML class with the size as appropriate using the `.col-` syntax.

***Note:** the `.column` class must always be present regardless of column width.* 

**For example:**

~~~~

<div class="row">
    <div class="col-50 column">
        <p>How much fun we have with a two-column layout!</p>
    </div>
    <div class="col-50 column">
        <p>How much fun we have with a two-column layout!</p>
    </div>
</div>

~~~~

Outputs a row with 2 columns of 50% width.

**Three Column Layout:**

~~~~

<div class="row">
    <div class="col-33 column">
        <p>How much fun we have with a three-column layout!</p>
    </div>
    <div class="col-33 column">
        <p>How much fun we have with a three-column layout!</p>
    </div>
    <div class="col-33 column">
        <p>How much fun we have with a three-column layout!</p>
    </div>
</div>

~~~~

Outputs a row with 3 columns of 33% width.

**Mix it up!**

~~~~

<div class="row">
    <div class="col-25 column">
        <p>How much fun we have with a three-column layout!</p>
    </div>
    <div class="col-25 column">
        <p>How much fun we have with a three-column layout!</p>
    </div>
    <div class="col-50 column">
        <p>How much fun we have with a three-column layout!</p>
    </div>
</div>

~~~~

Outputs 2 columns of 25% width and a column of 50% width.

### Column Options ###

As of currently, there are only three column sizes. More can be added as necessary. The sizes are as follows:

* 1 column (100%): `.col-100`
* 7/8 column (87%): `.col-87`
* 3/4 column (75%): `.col-75`
* 2/3 column (66%): `.col-66`
* 1/2 column (50%): `.col-50`
* 1/3 column (33%): `.col-33`
* 1/4 column (25%): `.col-25`
* 1/8 column (12%): `.col-12`

By default, the `.column` class is a 100% column. The `.col-100` class exists for better documentation within your own code.

### Centering a Column ###

To center a column within its container, simply add the `.centered` HTML class to the column.

~~~~

<div class="row">
    <div class="col-25 centered column">
        <p>How much fun we have with a centered column!</p>
    </div>
</div>

~~~~

Output would be a centered 25% column. 

---

## Image Alignment ##

There are several classes available to help align images within their content.
 * `.float-right` aligns to the right side of the container
 * `.float-left` aligns to the left side of the container
 * `.center-align` center aligns the image in the container