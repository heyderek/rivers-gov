<?php

// Set the page title  -- GENERAL TEMPLATE 2
$page_title = '50th Anniversary Toolkit';

// Set the page keywords
$page_keywords = '50th anniversary, event planning, events, rivers, wild and scenic rivers';

// Set the page description
$page_description = 'Event planning and community building resources for the 50th anniversary of the Wild and Scenic Rivers Act in 2018.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include("includes/metascript.php");
$sidebar_bool = false;

?>

<!-- BEGIN page specific CSS and Scripts -->
<!-- JS that controls the accordion -->
<!--<script type="text/javascript">-->
<!--	$(document).ready(function(){-->
<!--	$(".accordion-content").hide();-->
<!--	$("h2.accordion-heading").click(function(){-->
<!--	$(this).toggleClass("active").next().slideToggle("slow");-->
<!--	});-->
<!--	});-->
<!--</script>-->
<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
$body_class = 'fullwidth';
include("includes/header.php")
?>

<?php
// includes the content page top
include("includes/content-head.php")
?>

<br/>
<br/>
<br/>
<br/>
<br/>

<script>
	$(document).ready(function () {
		var content = $('.accordion-content');
		var heading = $('.accordion-heading');
		content.hide();
		heading.click(function () {
			$(this).next(content).slideToggle(300)
			$(this).toggleClass('active-accordion');
		})
	});
</script>

<script>
	$(document).ready(function () {
		var content = $('.accordion50-content');
		var heading = $('.accordion50-heading');
		content.hide();
		heading.click(function () {
			$(this).next(content).slideToggle(300)
			$(this).toggleClass('active-accordion');
		})
	});
</script>

<p><strong><em>NOTE: This toolkit is a living resource, which we will continue to add to and develop. If you have resources you'd like to see posted here, or have questions, contact Lisa Ronald, 50th Anniversary Coordinator, <a href="mailto:wsr50@river-management.org">wsr50@river-management.org</a>.</em></strong></p>

<div class="row">

	<div class="col-66 column">
		<!-- Left hand column text. <p> tags denote new paragraphs. <p> opens the pragraph, </p> closes it. If you want single spacing, use <br /> between lines; that just denotes a line break, not a new paragraph. -->
		<!-- Remove the heading and the <h3> </h3> tags if no heading is needed. The page title is set above under $page_title = 'Toolbox';. -->
		<br/>
        <h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;">Event Planning &amp; Community Building Resources</h3>
		<img src="images/events.jpg" width="547" height="199" border="0" style="margin-bottom: 10px" />
		<br/>

		<!-- Begin first-level accordion.-->

		<div>
			<h2 class="accordion50-heading">Turn Your Event Into a Rivers 50th Event</h2>

			<div class="accordion50-content">
				<ol>
					<li>Assess what events, trainings, and conferences are already happening in your area or that your organization already hosts. Reach out to your local Chamber of Commerce, Cultural Council, Downtown Association, etc., and take a look at community calendars.</li>
					<li>Check out the list of River Organizations in the Reference Materials for potential partners.</li>
					<li>See Logo and Marketing and Messages under Reference Materials to theme your outreach.</li>
					<li>See information below about purchasing 50th anniversary logo items, such as stickers, pins, etc.</li>
				</ol>
                <p>Use this <a href="wsr50/files/building-community-around-rivers-worksheet.docx">event and relationship brainstorming worksheet</a> to help think through steps 1 and 2 above.</p>
			</div>

			<h2 class="accordion50-heading">Host a New Rivers 50th Event</h2>

				<div class="accordion50-content">
				<ol>
					<li>Check out event ideas below.</li>
					<li>Check out the list of River Organizations in the Reference Materials for potential partners. Search and connect with other community partners.</li>
					<li>See Marketing and Messages under Reference Materials.</li>
				</ol>

				<!-- Begin second-level accordion -->
                
                <h2 class="accordion-heading">Art and Music</h2>

					<div class="accordion-content">

						<div class="block">
							<p>Engaging artists and musicians of diverse mediums encourages new perspectives on rivers and river values. Hosting an Artist in Residence at your event, river, organization, or community will provide an opportunity to create new art and music celebrating a specific Wild and Scenic River, wild area, or topic of celebration or concern. An Artist in Residence can provide performances, displays, readings, artist talks, and community panel discussions. Arts events can help bridge gaps of awareness between rivers and modern society. If you would like to host an Artist in Residence, reach out to the artists in your local community who might already be creating art and music inspired by rivers and wildlands. The following links provide examples of events, residency programs, and environmental artists.</p>
                            
                            <ul>
                            <li><a href="https://wildernessactperformanceseries.com/" target="_blank">50th Anniversary of the Wilderness Act Performance Series</a></li>
                            <li><a href="http://landscapemusic.org/" target="_blank">Landscape Music</a> - <a href="wsr50/files/landscape-music-rivers-trails-proposal-for-partners.pdf" target="_blank">Fall 2018 Concert Series proposal for partners</a></li>
                            <li><a href="http://www.bmwf.org/awc/" target="_blank">Artist Wilderness Connection Residency</a> in the Bob Marshall Wilderness</li>
                            <li><a href="https://www.nps.gov/subjects/arts/air.htm" target="_blank">National Parks Artist in Residence Programs</a></li>
                           	<li><a href="https://www.blm.gov/get-involved/artist-in-residence/about-the-program" target="_blank">Bureau of Land Management Artist in Residence Programs</a></li>
                            <li><a href="http://stephenwoodmusic.com/inspiring-stewardship/" target="_blank">Inspiring Stewardship</a> music in nature workshop</li>
                            </ul>
                            
							<p>You may also contact Wilderness Composer and Art Residency Consultant <a href="mailto:stephenwoodmusic@gmail.com">Stephen Wood</a>.
</p>
						</div>

				</div>

				<h2 class="accordion-heading">Boat Tags</h2>

					<div class="accordion-content">

						<div class="block">
							<p>For agencies or offices that issue permitted or non-permitted boat tags, the <a href="wsr50/files/mf-salmon-river-tags-2018-50th-black-3-color.pub">Middle Fork of the Salmon River</a> and the <a href="wsr50/files/selway-river-tags-2018-50th-black-3-color.pub">Selway River</a> have produced commemorative boat tag templates for 2018 that feature the anniversary logo.</p>
						</div>

				</div>

				<h2 class="accordion-heading">Displays and Selfie Picture Frame</h2>

				<div class="accordion-content">

					<div class="block">
						<p><img src="wsr50/files/find-your-way-picture-frame.jpg" width="200" style="float: right; margin-left: 20px; border: 0px;" />These <a href="wsr50/files/50th-anniversary-displays.pdf" target="_blank">displays&#8212;featuring rivers, trails and co-branded anniversary messaging</a>&#8212;were produced by the Bureau of Land Management and manufactured by the San Diego Sign Company. Use these templates to create new displays or partner with your local BLM office to reserve these displays for your event.</p>
						<ul>
							<li><a href="http://www.sdsign.com/EZ-Tube-Display-8ft-Curved-p/ezt8crvg1.htm" target="_blank">8ft EZ Tube Backdrop</a> - download <a href="wsr50/files/50th-rivers-trails-8ft-ez-tube-template.ai">template</a></li>
							<br />
							<li><a href="http://www.sdsign.com/Cinch-Retractable-Banner-Stand-p/cnchg.htm" target="_blank">Cinch Retractable Banner</a> - download <a href="wsr50/files/50th-anniversary-cinch-banner-template.ai">template</a></li>
						</ul>
                        
                        <p>To provide an interactive way for attendees at your event to document their presence on social media, use these selfie picture frames:</p>
                        <ul>
                        	<li><a href="https://static1.squarespace.com/static/579233b2579fb3e1df049c71/t/5b0d7d1488251bef8577440e/1527610644686/Makeyoursplash.pdf" target="_blank">#makeyoursplash selfie frame</a></li>
                            <li><a href="https://static1.squarespace.com/static/579233b2579fb3e1df049c71/t/5b032327aa4a99b7ce0c9abb/1526932263462/FindYourWay.pdf" target="_blank">#findyourway selfie frame</a></li>
                        </ul>
					</div>

				</div>

				<h2 class="accordion-heading">Environmental Education</h2>

				<div class="accordion-content">

					<div class="block">
						<p>Rivers offer a great opportunity for environmental education for both kids and adults! Water quality, fishing, and recreation can all be captured in outdoor, field-based lessons. Check out our <a href="https://docs.google.com/document/d/1S-u9UHMeETa7O77D7JXIywCwYb5SgFtYTKBH0T95AOw/edit?usp=sharing" target="_blank">rivers reading list</a>, as well as these river-focused environmental education ideas, and search for local water advocacy and environmental education groups in your area.</p>
						<ul>
                        	<li><b>Jr. Ranger Wild and Scenic Rivers Flyer</b> - This resources is an activity flyer, in <a href="wsr50/files/jr-ranger-wild-and-scenic-river-flyer.pdf" target="_blank">english</a> and <a href="wsr50/files/jr-ranger-wild-and-scenic-river-flyer-spanish.pdf" target="_blank">spanish</a>, for kids.</li>
							<li><a href="https://water.usgs.gov/edu/msac.html" target="_blank">U.S. Geological Survey water activity center</a> - This site has questionnaires, opinions, student challenges, and even surveys in the Activity Center for many activities to help your students learn about water.</li>
							<li><a href="https://water.usgs.gov/edu/sitemap.html" target="_blank">U.S. Geological Survey's water science</a> - This site offers many aspects of water, along with pictures, data, maps, interactive center and test your water knowledge.</li>
							<li>The U.S. Geological Survey recently released a new resource about the <a href="https://water.usgs.gov/edu/watercycle-kids.html" target="_blank">water cycle</a>, aimed at schools and students. This interactive module, which can also be found in image format, shows the various stages, actors, and components of the cycle in three different versions for various ages and a variety of languages.</li>
							<li>The <a href="https://www3.epa.gov/safewater/kids/index.html" target="_blank">Water Sourcebooks</a> contain 324 activities for grades K-12 divided into four sections: K-2, 3-5, 5-8, and 9-12. Each section is divided into five chapters: Introduction to Water, Drinking Water and Wastewater Treatment, Surface Water Resources, Ground Water Resources, and Wetlands and Coastal Waters.</li>
							<li><a href="https://www.epa.gov/environmental-topics/water-topics" target="_blank">The US EPA Office of Water's site</a> takes a look at how water quality is measured, what toxins are found in drinking water, and wetlands and groundwater. Highlights include a "What you can do?" section and suggested science projects.</li>
                            <li><a href="wsr50/files/freshwater-snorkeling.pdf" target="_blank">The Forest Service River Snorkling Program</a>, offered in select areas, takes school children on river snorkling field trips and is accompanied by two videos and curriculum resources.</li>
						</ul>
					</div>

				</div>

				<h2 class="accordion-heading">Films</h2>

				<div class="accordion-content">

					<div class="block">
						<p>The Wild &amp; Scenic Film Festival has created two <a href="wsr50/files/wild-scenic-film-festival-2018.pdf" target="_blank">one-night turnkey festival package of river-specific films</a> for 2018 at a discounted rate. The <a href="wsr50/files/wild-and-scenic-film-festival-2018-on-tour-rivers-program.pdf" target="_blank">Wild Rivers on-tour program includes 10 films</a> and the <a href="wsr50/files/wild-and-scenic-film-festival-2018-on-tour-our-waters-program.pdf" target="_blank">Our Waters on-tour program includes 11 films</a>.</p>
						<p>Individual <a href="videos.php">short feature films</a> produced for the anniversary are available online.</p>
					</div>

				</div>

				<h2 class="accordion-heading">Leave No Trace</h2>

				<div class="accordion-content">

					<div class="block">
						<p>Leave No Trace (LNT) is a natural fit with the 50th anniversary of the Wild and Scenic Rivers Act.</p>
						<p>LNT outdoor ethics were initially developed in response to increased interest in and use of wild lands in the 1970s, and are even more important today. LNT is a broad national ongoing effort on all public lands, led by the Leave No Trace Center for Outdoor Ethics. LNT programs and services include: <a href="https://lnt.org/learn/online-awareness-course" target="_blank">online LNT awareness training</a>, sale of LNT educational materials, and the ability to <a href="https://lnt.org/learn/request-a-visit" target="_blank">request that an LNT  travelling trainer team visit your 50th event</a> (submit requests at least four months in advance).</p>
					</div>

				</div>

				<h2 class="accordion-heading">Museum/Airport/Visitor Center Exhibit</h2>

				<div class="accordion-content">

					<div class="block">
						<p>Museums, airports, and visitor centers are high-traffic areas that can be used to present educational exhibits about rivers.</p>
						<p>Use this <a href="wsr50/files/museum-exhibit-request-template.docx" target="_blank">exhibit request template</a> to approach area museums, airports, or visitor centers about securing exhibit space in 2018 (do this early, even if you do not have a clear idea yet of what the display will contain).</p>
					</div>

				</div>

				<h2 class="accordion-heading">Presenters &amp; Speakers</h2>

				<div class="accordion-content">

					<div class="block">
						<p>Here are a few ideas for speakers and presenters to bring to your event! Also consider local experts familiar with your states Wild &amp; Scenic Rivers, as well as those who have a lot of experience recreating on them, such as river guides.</p>
						<ul>
							<li><a href="wsr50/files/tim-palmer.pdf" target="_blank">Tim Palmer</a>, river author, photographer, speaker</li>
							<li><a href="http://www.wildriverlife.com/" target="_blank">Adam and Susan Elliott of @wildriverlife</a>, adventure athletes paddling 50 Wild &amp; Scenic Rivers in 2017 and 2018</li>
							<li><a href="http://www.oregonwildandscenic.com" target="_blank">Zachary Collier</a>, outfitter and adventure athlete paddling all 58 of Oregon's Wild &amp; Scenic Rivers in 2017 and 2018</li>
						</ul>
					</div>

				</div>

				<h2 class="accordion-heading">River Cleanup</h2>

				<div class="accordion-content">

					<div class="block">
						<p>American Rivers makes it easy for you to host a <a href="https://www.americanrivers.org/make-an-impact/national-river-cleanup/" target="_blank">river cleanup</a> or participate in one already scheduled.</p>
					</div>

				</div>
                
                <h2 class="accordion-heading">Trivia</h2>

				<div class="accordion50-content">

					<div class="block">
						<p><a href="waterfacts.php">River and water trivia</a> make a great interactive activity at events (ex. bar trivia or Jeopardy) or can be turned into an autoplay PowerPoint shown before an event starts.</p>
					</div>

				</div>

			</div>

			<!-- End second-level accordion here. -->

			<h2 class="accordion50-heading">Share Your Event and Learn About Upcoming Rivers 50th Event</h2>

			<div class="accordion50-content">

				<div class="block">
					<p>Local and regional community events commemorating the WSR50 are posted on a <a href="https://umontana.maps.arcgis.com/apps/MapJournal/index.html?appid=f85c819b95f14ae1a44400e46ee9578c" target="_blank">public rivers event map</a>. <a href="https://umontana.maps.arcgis.com/apps/GeoForm/index.html?appid=a076f47363d84ec4b861ec3ca8f4e673" target="_blank">Add a pushpin on the map for your event</a> using a public form.</p>
				</div>

			</div>
            
            <h2 class="accordion50-heading">Engage Volunteers</h2>

			<div class="accordion50-content">

				<div class="block">
					<p>Volunteers help add capacity to anniversary event planning and stewardship/advocacy activities. Use this <a href="wsr50/files/50th-anniversary-volunteer-resources-package.pdf">volunteer resources package</a> to better engage volunteers around the anniversary. Recognize your volunteers with the <a href="wsr50/files/find-your-way-volunteer-certificate.pdf">50 Hours for 50 Years Certificate</a>.</p>
				</div>

			</div>

            <h2 class="accordion50-heading">Share Your River Story</h2>

			<div class="accordion50-content">

				<div class="block">

				<p>Share your unique river story! American Rivers and partners are collecting <a href="http://www.5000miles.org" target="_blank">5,000 personal river stories</a> to highlight the many ways rivers touch our lives.</p>
                <p>Share your original river video! Rafting Magazine is collecting and showing <a href="https://www.raftingmag.com/wsra50-film-series/" target="_blank">amateur river videos from paddlers like you</a>. Winning videos will be shown at Feather Fest and the River Management Symposium.</p>
                <p>The Digital Outdoorsman is providing an opportunity to <a href="https://www.revenueriver.co/share-your-river-gear-story-for-50th-anniversary-wild-scenic-rivers?utm_campaign=Digital%20Outdoorsman-Marketing&utm_source=hs_email&utm_medium=email&utm_content=2&_hsenc=p2ANqtz-_aberfCknbyz8ImRuc6EgJGaajHS74jiu3A-3nVcZT9AnP25hve56hp--ctPeFsplMbOpG4LEkpK4oIqQJkF7Bn-brZw&_hsmi=2" target="_blank">share stories related to river gear</a>.</p>
                <p>ESRI Storymaps--easy-to-use technologies to create maps, images, and videos to illustrate a topic--are also a great way to tell the story of your river online. These <a href="https://drive.google.com/file/d/0B2azh5BtUlPCR1dPYVVTbFFQYWc/view" target="_blank">storymap creation instructions</a> give detailed guidance on the preparatory and technological steps involved in creating a <a href="https://nps.maps.arcgis.com/apps/MapSeries/index.html?appid=12265dd81e5b4c44a870b937c8d8cfa8" target="_blank">compelling, interactive storymap</a>.</p>
				</div>

			</div>

			<h2 class="accordion50-heading">Purchase Rivers 50th Products</h2>

			<div class="accordion50-content">

				<div class="block">
                	<p>The follow <b>50th anniversary logo-themed products</b> are available from select vendors. Use this <a href="wsr50/files/50th-products-information-sheet.pdf" target="_blank">product display sign</a> along with these products.</p>
                    
                    <b>Pins/Patches/Medallions/Stickers/Magnets</b>
					<p>Pins, patches, medallions (for paddles or walking sticks), stickers (white or black background), and magnets are available in individual or wholesale quantities from Eagle River Designs: <a href="wsr50/files/erd-wsr50-product-ordering-info.docx" target="_blank">price sheet and order form</a>, <a href="wsr50/files/erd-wsr50-products.pdf" target="_blank">pictures of products</a>, and <a href="wsr50/files/erd-wsr50-product-barcodes.pdf" target="_blank">product barcodes</a>. Eagle River Designs will donate a percentage of proceeds to support the anniversary.</p>
                    
                    <b>Hats</b>
                    <p>Two trucker hats, buckets, and microfleece beanie from Top to the World. These are available for <a href="https://towcaps.com/crowntrails/ranger-1-river-management-system-snap-back-two-tone-5-rang1-rvms-adj-2tn5" target="_blank">individual sales online (other styles coming soon)</a> or wholesale. For wholesale purchases, a minimum order quantity 14 hats applies (can be mixed with a minimum of 6 per style). Top of the World will donate a percentage of proceeds to support the anniversary. Complete the <a href="wsr50/files/tow-order-form.pdf">order form</a> and contact Whitney LaRuffa at <a href="mailto:whitney@corwntrailsheadwear.com">whitney@corwntrailsheadwear.com</a> or 503-545-2613. Ranger $11.00 each, $24.95 (Minimum Advertised Pricing); Primitive Tonal $11.00 each, $24.95 (MAP); Hybrid Bucket, $11.00 each, $27.95 (MAP); Hailstorm Knit, $10.00 each, $22.95 (MAP).</p>
                    
                    <b>Apparel Etc.</b>
                    <p>T-shirts, hoodies, baby clothes, shopping and beach bags, water bottles, coffee mugs, flasks, laptop and phone cases, flip flops and Christmas ornaments and more can all be ordered individually (not intended for wholesale purchases or resale) from the <a href="http://www.cafepress.com/rivers50" target="_blank">Rivers 50th anniversary CafePress store</a>. CafePress will donate a percentage of proceeds to support the anniversary.</p>
					
                    <b>Cell Phone Dry Bags</b>
                    <p><a href="wsr50/files/loksak-lanyard.pdf" target="_blank">Waterproof cell phone bags with lanyards</a> are available from Loksak (not intended for resale). Pricing is as follows: 100 at $4.45 each; 500 at $3.50 each; 1000 at $3.33 each. Organizations interested in purchasing quantities less than 100 can combine orders to meet the minimum purchase requirement. Organizations partnering on an order can use multiple charge cards (all card numbers needed at time order is placed) and shares can be drop shipped to different locations. Contact Ryan Zvibleman at <a href="mailto:ryan@loksak.com">ryan@loksak.com</a>, D 239-449-8170  O 239-331-5550 Ext. 305. Production and shipment time is 2-3 weeks.</p>
                    
                    <b>Whistles</b>
                    <p><a href="wsr50/files/fox40whistles-rivers50.pdf" target="_blank">Safety whistles</a> are available from Fox 40. Pricing for the Fox 40 Classic Safety whistle, product code 9902-0704, is as follows: 144 at $3.60 each; 288 at $3.45 each; 360 at $3.30 each. Organizations interested in purchasing quantities less than 144 can combine orders to meet the minimum purchase requirement. Organizations partnering on an order can use multiple charge cards (all card numbers needed at time order is placed) and shares can be drop shipped to different locations. Contact Lori Elwood at <a href="mailto:lori@fox40world.com">lori@fox40world.com</a>, 888-663-6940. Production and shipment time is 2-3 weeks.</p>
                    
                    <b>Temporary Tattoos</b>
                    <p>Great for kids activities or family programs, temporary tattoos are available from Rocky Mountain Promotional Products and Apparel. Pricing is as follows: Item number 1520CT; 1.5" W x 2" H; 200 at $0.75 each; 500 at $0.40 each; 1000 at $0.25 each; 3000 at $0.11 each; 5000 at $0.08 each; 10000 at $0.07 each. Organizations interested in partnering can take advantage of price breaks for larger quantities. Multiple charge cards can be used (all card numbers needed at time order is placed) and shares of a larger order can be drop shipped to different locations, however this may add costs for shipping and handling. Rocky Mountain Promotional Products and Apparel donates a percentage of proceeds to support the anniversary. <a href="wsr50/files/tattoo-order-form.pdf" target="_blank">Complete the tattoo order form</a> and contact Tyler Riehl at <a href="mailto:info@rockymountainpromo.com">info@rockymountainpromo.com</a>, 970-245-9814. Production and shipment time is 2-3 weeks.</p>
                    
                    <b>Stainless Steel Pint Cups, Water Bottles and Insulated Tumblers</b>
                    <p>Green your event with <a href="wsr50/files/klean-kanteen-wild-and-scenic-rivers-50th-logo-16oz-pint.pdf" target="_blank">50th anniversary pints</a> available from Klean Kanteen. Prices starting at $5 each.  Ask about <a href="wsr50/files/klean-kanteen-your-logo-and-wild-and-scenic-rivers-50th-logo-16oz-pint.pdf" target="_blank">additional colors and adding your logo</a> to the backside of the pint. Minimum order 90 units. Bottles and Insulated Tumblers also available. Klean Kanteen will donate 50 cents per unit ordered up to $5,000 to American Rivers in support their 5,000 Miles of Wild Campaign. Contact Edgar Nava at <a href="mailto:co-brand@kleankanteen.com">co-brand@kleankanteen.com</a>, 800-767-3173 ext 311. Production and shipment time is 2-4 weeks.</p>
                    
                    <b>Cozies</b>
                    <p><a href="wsr50/files/rocky-mountain-promotionals-beverage-cozie.pdf" target="_blank">Two different beverage cozies</a> are available from Rocky Mountain Promotional Products and Apparel. Pricing is as follows:</p>
                    <div style="margin-left: 25px;">
                    	<b>Cool-Apsible Beverage Insulator: 2-color screen print</b>
                    	<p>Item number CRVJH-DMCCC #1003. 3" W x 3" H; Screen printed in two colors. 100 at $3.03 each; 250 at $1.54 each; 500 at $1.43 each; 1000 at $1.38 each.</p>
                        <b>Cool-Apsible Beverage Insulator: 4-color sublimation</b>
						<p>Item number GUYIB-JJOEO #1003-ACP. 3.25" W x 3.25" H; Sublimation printed in four colors. 100 at $2.75 each; 250 at $1.54 each; 500 at $1.43 each; 1000 at $1.38 each.
                    </div>
                    <p>Organizations interested in purchasing quantities less than 100 can combine orders to meet the minimum purchase requirement. Organizations partnering on an order can use multiple charge cards (all card numbers needed at time order is placed) and shares can be drop shipped to different locations, however this may increase the cost. Rocky Mountain Promotional Products and Apparel will donate a percentage of proceeds to support the anniversary. <a href="wsr50/files/cozie-order-form.pdf" target="_blank">Complete the cozie order form</a> and contact Tyler Riehl at <a href="mailto:info@rockymountainpromo.com">info@rockymountainpromo.com</a>, 970-245-9814. Production and shipment time is 2-3 weeks.</p>
				</div>

			</div>

			<h2 class="accordion50-heading">Attend a River Conference or Training</h2>

			<div class="accordion50-content">

				<div class="block">
					<p>In addition to varied local community events, there are local, regional and national conference, trainings, and tradeshows leading up to the 50th Anniversary. There are also important annual holidays when many community events may occur.</p>
					<ul>
						<li>Wild &amp; Scenic Film Festival - Nevada City and Grass Valley, California, January (mid) 2018</li>
                        <li><a href="http://pnts.org/new/our-work/hike-the-hill/" target="_blank">Hike the Hill</a> (Rivers and Trails Forum held Tuesday, February 13; National Forest Foundation - Rivers &amp; Trails 50th evening reception held Thursday, February 15)  - Washington, D.C., February 11-15, 2018</li>
						<li><a href="https://www.2018norc.org/" target="_blank">Society of Outdoor Recreation Professionals Conference</a> - Burlington, VT, April 23-26, 2018</li>
						<li><a href="https://www.rivernetwork.org/events-learning/river-rally/about/program/" target="_blank">River Rally</a> (River Network) - Lake Tahoe, April 29-May 2, 2018</li>
						<li>Outdoor Retailer Summer Market - Denver, CO, July 23-26, 2018 (presentations likely to be included in agenda)</li>
						<li><a href="http://www.river-management.org/2018-symposium-call-for-papers" target="_blank">River Management Symposium</a> (River Management Society), Vancouver, WA - October 22-25, 2018</li>
					</ul>
				</div>

			</div>

		<p>&nbsp;</p>
		<table width="75%" bgcolor="#EDE9DE" cellspacing="0" style="border-radius: 6px" align="center">
			<tbody>
				<tr>
				<td style="padding-top: 10px; padding-bottom: 0px; padding-left: 10px; padding-right: 10px"><center><h3 style="font-size: 16pt; color: #2C7843; font-style: italic; padding-bottom: 5px;" align="center">For The Press</h3></center><p align="left">Use our online <a href="http://wsr50.onlinepresskit247.com/" target="_blank">MEDIA KIT</a> for interview ideas, interview questions, stories ideas, photos and more.<br /><center><img src="images/newspaper.png" width="200" height="200" alt="Newspaper" align="center"/></center></p></td>
				</tr>
			</tbody>
		</table>

		</div>

	</div>

	<div class="col-33 column">
		<!-- Right hand column text. -->
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
        
        <div><img src="images/webinar7-19.jpg" width="265" height="325" border="0" /></div>
        <p>Missed any of our webinars? Scroll down for the recordings.</p>
		<p>&nbsp;</p>
        
        <!--CONSTANT CONTACT FORM-->
        <!-- Begin Constant Contact Inline Form Code -->
        <div class="ctct-inline-form" data-form-id="9d0a8e01-dc10-4beb-9ea5-d6996e4c3036"></div>
        <!-- End Constant Contact Inline Form Code -->
        
		<p>&nbsp;</p>

        <h3 style="font-size:16pt; color:#2C7843; padding-bottom:5px;">Reference Materials</h3>

		<!--Start of right column accordion.-->

		<h2 class="accordion-heading">General Information</h2>

		<div class="accordion-content">

			<div class="block">
				<ul>
                <li><a href="national-system.php">What is a Wild and Scenic River?</a></li>
                <li><a href="http://nps.maps.arcgis.com/apps/MapJournal/index.html?appid=ba6debd907c7431ea765071e9502d5ac" target="_blank">Storymap</a></li>
                <li><a href="documents/rivers-table.pdf" target="_blank">Rivers List</a></li>
                <li><a href="documents/mileage-agency.zip">Agency-by-agency Mileage Chart</a></li>
                <li><a href="documents/mileage-state.zip">State-by-state Mileage Chart</a></li>
                <li>Infographics: Why Wild &amp; Scenic Rivers are Important (more river values coming soon!)
                    <ul>
                        <li><a href="wsr50/files/general-wild-and-scenic-rivers-infographic.pdf" target="_blank">Wild and Scenic Rivers: The National System</a> (<a href="wsr50/files/general-wild-and-scenic-rivers-infographic-spanish.pdf" target="_blank">en español</a>)</li>
                        <li><a href="wsr50/files/water-quality-infographic.pdf" target="_blank">Water Quality</a> (<a href="wsr50/files/water-quality-infographic-spanish.pdf" target="_blank">en español</a>)</li>
                        <li><a href="wsr50/files/fish-wildlife-ecology-botany-infographic.pdf" target="_blank">Fish, Wildlife, Ecology and Botany</a> (<a href="wsr50/files/fish-wildlife-ecology-botany-infographic-spanish.pdf" target="_blank">en español</a>)</li>
                        <li><a href="wsr50/files/recreation-economics-infographic.pdf" target="_blank">Recreation and Economics</a> (<a href="wsr50/files/recreation-economics-infographic-spanish.pdf" target="_blank">en español</a>)</li>
                        <li><a href="wsr50/files/geology-hydrology-infographic.pdf" target="_blank">Geology and Hydrology</a> (<a href="wsr50/files/geology-hydrology-infographic-spanish.pdf" target="_blank">en español</a>)</li>
                        <li><a href="wsr50/files/history-culture-paleotology-infographic.pdf" target="_blank">History, Culture and Paleontology</a> (<a href="wsr50/files/history-culture-paleotology-infographic-spanish.pdf" target="_blank">en español</a>)</li>
                        <li><a href="wsr50/files/health-safety-infographic.pdf" target="_blank">Health and Safety</a> (<a href="wsr50/files/health-safety-infographic-spanish.pdf" target="_blank">en español</a>)</li>
                    </ul>
                </li>
                </ul>
			</div>

		</div>

		<h2 class="accordion-heading">Find Your Wild and Scenic River</h2>

		<div class="accordion-content">

			<div class="block">
				<ul>
					<li><a href="map.php">Rivers.gov</a> is a definitive resource for the system, its history, management and designated river attributes.</li>
                    <li><a href="https://nps.maps.arcgis.com/apps/MapSeries/index.html?appid=12265dd81e5b4c44a870b937c8d8cfa8" target="_blank">National River Storymap</a> is an online ESRI storymap showcasing western, central and eastern rivers.</li>
					<li><a href="http://www.nationalriversproject.com" target="_blank">NationalRiversProject.com</a> is a searchable resource to find floatable Wild &amp; Scenic Rivers, water trails, and whitewater and shows managing agency, permits required, camping and other amenities, nearby rivers and their difficulty.</li>
					<li><a href="https://www.americanrivers.org/threats-solutions/protecting-rivers/map-wild-scenic-rivers/" target="_blank">American Rivers map</a></li>
					<li><a href="https://www.americanwhitewater.org/content/Document/fetch/documentid/277/.raw">American Whitewater's Google Maps KMZ file</a> of Wild and Scenic Rivers</li>
				</ul>
			</div>

		</div>

		<h2 class="accordion-heading">Marketing and Messaging</h2>

		<div class="accordion-content">

			<div class="block">
            	<ul>
					<li>Wild &amp; Scenic Rivers 50th anniversary messages
						<ul>
							<li>Tagline: It's your river. Make your splash!</li>
							<li>Hashtags: #makeyoursplash/#EncuentraTuSendero (spanish) (#wildandscenic and #wildandscenicriver are also organicly-emerging tags)</li>
							<li>Buzz Words: Experience, Discover, Protect</li>
							<li>Website: <a href="http://www.wildandscenicrivers50.us" target="_blank">www.wildandscenicrivers50.us</a></li>
                            <li>Video: <a href="https://www.doi.gov/video/makeyoursplash" target="_blank">#findyourway river video</a> (released October 2, 2017)</li>
						</ul>
					</li>
					<li>National Trails Act 50th anniversary messages
						<ul>
							<li>Tagline: Find Your Trail.</li>
							<li>Hashtags: #FindYourTrail, #trails50, #trailsunite</li>
							<li>Buzz Words: Discover, Connect, Explore</li>
							<li>Website: <a href="http://www.trails50.org" target="_blank">www.trails50.org</a></li>
						</ul>
					</li>
                    <li>Joint Rivers/Trails 50th anniversary messages
                    	<ul>
                        	<li>Tagline: Find Your Way.</li>
                            <li>Hashtag: #findyourway</li>
                            <li><a href="https://www.nps.gov/orgs/1207/06-01-2017-find-your-way-launch.htm" target="_blank">Press release</a> (National Trails Day 2017) and <a href="https://www.doi.gov/video/findyourway" target="_blank">video commercial</a></li>
                        </ul>
                    </li>
                    <li>See the "Photos" section below for ways to make these messages visual</li>
					<li>Use the following advice and templates (created and used during the Wilderness Act anniversary in 2014) to get good press for your local events and be sure to use #rivers50 and #makeyoursplash/#EncuentraTuSendero (spanish) when posting online: <a href="http://www.wilderness.net/toolboxes/documents/50th/50th%20Media%20-%20Pitching%20Tips%20and%20Principles.pdf" target="_blank">Pitching Tips and Principles</a>, <a href="http://www.wilderness.net/toolboxes/documents/50th/50th%20Media%20-%20Interview%20Tips.pdf" target="_blank">Interview Tips</a>, <a href="http://www.wilderness.net/toolboxes/documents/50th/50th%20Media%20-%20Anatomy%20of%20an%20Advisory.doc">Advisory Example/Template</a>, <a href="http://www.wilderness.net/toolboxes/documents/50th/50th%20Media%20-%20How%20to%20Write%20and%20Distribute%20a%20Press%20Release.pdf" target="_blank">How to Write and Distribute a Press Release</a>, <a href="http://www.wilderness.net/toolboxes/documents/50th/50th%20Media%20-%20Anatomy%20of%20a%20Press%20Release.doc">Press Release Example/Template</a>.</li>
					<li>Select annual social media hook dates
						<ul>
							<li>International Day of Action for Rivers: March 14, 2018</li>
							<li>International Day of Forests: March 21, 2018</li>
							<li>World Water Day: March 22, 2018</li>
                            <li>World Fish Migration Day: April 21, 2018</li>
							<li>Earth Day: April 22, 2018</li>
							<li>Park Rx Day: April 23, 2018</li>
                            <li>Water and Trail Month: Entire month of June</li>
							<li>National Trails Day: June 2, 2018</li>
							<li>National Fishing and Boating Week: June 2-10, 2018</li>
                            <li>Latino Conservation Week: July 14-22, 2018</li>
							<li>National Public Lands Day: September 29, 2018</li>
							<li>Wild &amp; Scenic Rivers Act 50th Anniversary: October 2, 2018</li>
						</ul>
					</li>
				</ul>
			</div>

		</div>
        
		<!--<h2 class="accordion-heading">Music List</h2>

		<div class="accordion-content">

			<div class="block">
			<p>Although not comprehensive, this growing <a href="https://docs.google.com/spreadsheets/d/1VBnTcVbX851qFUS0VWTVmkcFdbqJ6r-cWLdtLueqkfA" target="_blank">list of river-related songs</a> includes popular and contemporary artists. Also see the article, <a href="https://tpwmagazine.com/archive/2015/dec/ed_1_riversongs/index.phtml" target="_blank">Rivers of Song: Musical stories flow through Texas waterways</a> and the accompanying <a href="http://spoti.fi/1Xz1RIR" target="_blank">Spotify playlist of Texas river songs</a>.</p>
			</div>

		</div>-->
        
        
        <h2 class="accordion-heading">Photos and Videos</h2>

		<div class="accordion-content">

			<div class="block">
				<p>To make your messages and information about the anniversary visual, consider using any of the following photo resources. Have a Wild and Scenic Rivers photos site to suggest? Let us know!</p>
                <ul>
					<li><a href="http://wsr50.onlinepresskit247.com/image-gallery.html" target="_blank">Select Wild and Scenic Rivers photos</a> in the 50th anniversary media kit by multiple photographers</li>
                    <li><a href="https://www.flickr.com/photos/wild_rivers/albums/" target="_blank">Wild and Scenic Rivers by State</a> on Flickr with most photos by river historian Tim Palmer</li>
                    <li><a href="https://www.flickr.com/photos/forestservicenw/albums/72157683485449304" target="_blank">Forest Service Wild and Scenic Rivers in the Pacific Northwest</a> on Flickr with photos by the agency</li>
                    <li><a href="https://www.flickr.com/photos/mypubliclands/sets/72157666746773391/" target="_blank">Bureau of Land Management Wild and Scenic Rivers</a> on Flickr with photos by the agency</li>
				</ul>
                <p>Although various feature films have been created for the anniversary, many other local, regional or topical films, such as those listed below are available online.</p>
                <ul>
                	<li><a href="https://www.youtube.com/watch?v=XIVZDRndwHM" target="_blank">How boaters and anglers can protect spawning salmon on the White Salmon Wild &amp; Scenic River</a></li>
                    <li><a href="https://vimeo.com/214726130" target="_blank">Little Miami Wild &amp; Scenic River</a></li>
                    <li><a href="https://youtu.be/fmK7vVMgoog" target="_blank">Tuolomne Wild &amp; Scenic River</a></li>
                    <li><a href="https://youtu.be/xhuF7L0a_gs" target="_blank">Middle Fork of the Salmon River</a> (home of the Shoshone-Bannok Tuka-Deka)</li>
                    <li><a href="https://youtu.be/EhmyCS-r7U8" target="_blank">Accessible Adventures: Wild and Scenic Rivers in the Pacific Northwest</a></li>
                    <li><a href="https://www.youtube.com/watch?v=yhU-USw1_U4" target="_blank">The World Beneath the Rims</a> (Grand Canyon)</li>
                    <li><a href="https://www.youtube.com/watch?v=fbb0VfmOWrs" target="_blank">The Legacy of Wyoming's Snake Headwaters</a></li>
                    <li><a href="https://www.youtube.com/watch?v=qPmCo0riO5g" target="_blank">Celebration of Northwest Rivers</a></li>
                    <li><a href="https://www.youtube.com/watch?v=JD2i5YI-40k" target="_blank">Tale of Two Rivers</a>, Forest Service Region 6</li>
                    <li><a href="https://www.youtube.com/watch?v=hf7q2En6pfw" target="_blank">Quilt for Two River project</a></li>
                </ul>
			</div>

		</div>
        
        <h2 class="accordion-heading">Logo</h2>

		<div class="accordion-content">

			<div class="block">
				<p>A special <a href="wsr50/files/50th-wsr-logo.zip">50th anniversary logo</a> (<a href="wsr50/files/50th-wsr-logo-spanish.zip">Spanish version</a>) has been designed for use during 2017 and 2018. This zip file containing logo files in different formats and the logo use guide.</p>
			</div>

		</div>

		<h2 class="accordion-heading">Quotes</h2>

		<div class="accordion-content">

			<div class="block">
			<p>Although not comprehensive, this growing <a href="quotations.php">river quotations</a> list includes quotations from a variety of authors, conservationists, legislators etc.</p>
			</div>

		</div>

		<h2 class="accordion-heading">Webinars</h2>

		<div class="accordion-content">

			<div class="block">
            <p><b>50th Event Planning and Community Building Series</b></p>
            <p>Stay tuned for registration information for upcoming webinars! This series is designed to help you learn from and get inspired by the work of your peers when it comes to event planning, community building, and partnership development for the Wild and Scenic Rivers Act 50th anniversary. This series will feature tips and lessons learned from non-profit organizations and agencies alike.</p>
        	<ul>
            	<li>Webinar #1, February 6 (recording): <a href="https://www.rivernetwork.org/resource/celebrating-wild-scenic-rivers-50th-spark-community-engagement/" target="_blank">Integration into existing events and collaboration with the arts</a></li>
        		<li>Webinar #2, April 5 (recording, enter your name and email for access): <a href="https://register.gotowebinar.com/register/5787803209525718275">Plug-and-play Anniversary Opportunities</a></li>
        		<li>Webinar #3, June 4 (recording, enter your name and email for access): <a href="https://register.gotowebinar.com/register/1177177120463414787" target="_blank">Engaging Your Diverse Community</a></li>
            </ul>
            <p>Other Webinars</p>
			<ul>
				<li>Forest Service Office of Sustainability and Climate Change
					<ul>
						<li><a href="http://www.forestrywebinars.net/webinars/drought-and-water-challenges" target="_blank">Responding to Drought and Water Challenges-National Kickoff Meeting</a> (archived)</li>
						<li><a href="http://www.forestrywebinars.net/webinars/fisheries-responding-to-drought-and-water-challenges" target="_blank">Fisheries-Responding to Drought and Water Challenges</a> (archived)</li>
						<li><a href="http://climatewebinars.net/webinars/drought-recreation-wilderness" target="_blank">Effects of Drought on Recreation and Wilderness</a> (3.22.17, archived)</li>
					</ul>
				</li>
				<li><a href="https://www.rivernetwork.org/events-learning/events-webinars/" target="_blank">River Network Webinars</a></li>
				<li><a href="http://www.river-management.org/wild-and-scenic" target="_blank">River Management Society webinars, symposium presentation proceedings</a></li>
			</ul>

			</div>

		</div>
        
         <h2 class="accordion-heading">Legislation</h2>

		<div class="accordion-content">

			<div class="block">
            
       		While various proposals for additional Wild and Scenic Rivers designation are being considered by Congress, state resolutions provide an important way to engage local and state government. Please send us the link to your state's rivers resolution.
            
            <ul>
            	<li><a href="https://legislature.idaho.gov/wp-content/uploads/sessioninfo/2018/legislation/SCR132.pdf" target="_blank">Idaho State Wild and Scenic River Resolution</a></li>
                <li><a href="https://nmlegis.gov/Legislation/Legislation?Chamber=S&LegType=M&LegNo=118&year=18" target="_blank">New Mexico State Wild and Scenic River Resolution</a></li>
            </ul>
                
			</div>

		</div>
        
        <h2 class="accordion-heading">Learn More...</h2>

		<div class="accordion-content">

			<div class="block">
            
            	<ul>
                <li>Agency Documents
                    <ul>
                        <li><a href="council.php">Interagency Coordinating Council</a></li>
                    </ul>
                </li>
                <li>Historical Information and Archives
                    <ul>
                        <li><a href="documents/craighead-article-1957.pdf" target="_blank">Wild River</a> - A letter published by Dr. John Craighead in Montana Wildlife Magazine in June 1957</li>
                        <li><a href="documents/wsr-primer.pdf" target="_blank">An Introduction to Wild &amp; Scenic Rivers</a> - A concise primer on Wild &amp; Scenic Rivers and what designation means to you.</li>
                        <li><a href="documents/q-a.pdf" target="_blank">A Compendium of Questions &amp; Answers Relating to Wild &amp; Scenic Rivers</a> - Everything you wanted to know about Wild &amp; Scenic Rivers in a Q&amp;A format. These Q&amp;As can also be accessed through a searchable data base.</li>
                        <li><a href="publications.php">Other publications from the Interagency Wild &amp; Scenic Rivers Coordinating Council</a></li>
                    </ul>
                </li>
                <li>Legislation
                    <ul>
                        <li><a href="documents/act/90-542.pdf" target="_blank">Wild and Scenic Rivers Act</a></li>
                        <li><a href="wsr-act.php">About the WSR Act</a></li>
                        <li><a href="documents/wsr-act.pdf" target="_blank">Wild &amp; Scenic Rivers Act Abridged</a> (the full act minus the rivers listed in Sections 3(a) and 5(a))</li>
                    </ul>
                </li>
                <li><a href="https://docs.google.com/spreadsheets/d/1qUPFXJ8-MZrNg7pJ-DwPTBifk9aUWv6b_-8bWB3PCm8/edit?usp=sharing" target="_blank">River Organizations List</a></li>
                <li><a href="https://docs.google.com/document/d/1S-u9UHMeETa7O77D7JXIywCwYb5SgFtYTKBH0T95AOw/edit?usp=sharing" target="_blank">Rivers Reading List</a>
                	<ul>
                    	<li><a href="http://ijw.org/subscription-type/" target="_blank">International Journal of Wilderness December 2017 Rivers Special Issue</a></li>
                        <li><a href="wsr50/files/river-management-socity-journal-2017-winter.pdf" target="_blank">River Management Society 2017 Winter Journal</a></li>
                    </ul>
                </li>
             	</ul>
                
			</div>

		</div>

	</div>

	</div>

<!--CONSTANT CONTACT FORM universal code, goes before </body>-->

<!-- Begin Constant Contact Active Forms -->
<script> var _ctct_m = "b6145ada6e3df051fdfeeac419decbb4"; </script>
<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
<!-- End Constant Contact Active Forms -->

<?php
// includes the content page bottom
include("includes/content-footNEW.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include("includes/footer.php")
?>