<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Portageville Railroad Bridge Replacement, Genesee River, New York';

// Set the page keywords
$page_keywords = 'Portageville, Railroad, Bridge, Replacement';

// Set the page description
$page_description = 'The replacement of the Portageville Railroad Bridge on the Genesee River.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<link rel="stylesheet" type="text/css" href="../transportation-slideshow.css"/>

<!-- BEGIN page specific CSS and Scripts -->

<script>
	var slideIndex = 1;
	showSlides(slideIndex);

	// Next/previous controls
	function plusSlides(n) {
		showSlides(slideIndex += n);
	}

	// Thumbnail image controls
	function currentSlide(n) {
		showSlides(slideIndex = n);
	}

	function showSlides(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("dot");
		if (n > slides.length) {slideIndex = 1}
		if (n < 1) {slideIndex = slides.length}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" active", "");
		}
		slides[slideIndex-1].style.display = "block";
		dots[slideIndex-1].className += " active";
	}
</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="trans-box" style="min-height: 0;">

	<div id="trans-details-box">

		<div>
		<!-- Insert title and text for tan summary box between the tags below. -->
		<h3>Project Overview</h3>
		<p>Project Proponent: New York Department of Transportation, Norfolk Southern Railroad WSR Administering Office: National Park Service; Northeast Region.</p>
		</div>

	</div>

	<div class="clear"></div>
	<!-- Allows for content above to be flexible -->

</div>
<!--END #trans-summary-box -->

<div id="lower-content">
<!-- Insert title and text for main case discussion between the tags below. -->
<h2 style="line-height:150%;">Portageville Railroad Bridge Replacement</h2>
<p>Located within Letchworth State Park, the Portageville Railroad Bridge Replacement addresses the deficiencies in the current industry standard freight rail loads while reducing maintenance cost and efforts on the bridge. The bridge replacement is necessary for the continuation of safe, reliable, and efficient rail operations in the region. The new bridge utilizes an arch structure that spans the entire river with supports at the top of the Letchworth Gorge walls and no supports in the river channel. Under the 1989 Genesee River Protection Act, the Genesee River has been afforded the protections of a Study River under Section 7 of the National Wild and Scenic Rivers Act. Note: The Genesee River is not a designated WSR but has protection under Section 7 as a permanent study river.</p>

<p>NPS Concurrence Letter<br />
EIS, Wild and Scenic Rivers<br />
Project Photos.</p>
</div>
<!--END #lower-content -->

	<!-- Slideshow container -->
	<div class="slideshow-container">
	<!-- Full-width images with number and caption text -->

		<div class="mySlides fade">
			<div class="numbertext">1 / 7</div>
		<img src="HistoricBridgeSign_NYDOT.jpg" style="width:100%">
			<div class="text">State Historic Marker - Portageville Railroad Bridge</div>
		</div>

		<div class="mySlides fade">
			<div class="numbertext">2 / 7</div>
		<img src="OldBridge1_NYDOT.jpg" style="width:100%">
		    <div class="text">Old Portageville Railroad Bridge over the Genesee River</div>
 	 	</div>

 		<div class="mySlides fade">
			<div class="numbertext">3 / 7</div>
		<img src="OldBridge2_NYDOT.jpg" style="width:100%">
			<div class="text">Old Portageville Railroad Bridge over the Genesee River</div>
		</div>

		<div class="mySlides fade">
			<div class="numbertext">4 / 7</div>
		<img src="OldBridge3_NYDOT.jpg" style="width:100%">
			<div class="text">Old Portageville Railroad Bridge</div>
		</div>

		<div class="mySlides fade">
			<div class="numbertext">5 / 7</div>
		<img src="ConceptualNewBridge1_NYDOT.jpg" style="width:100%">
			<div class="text">Conceptual view of the new arch railroad bridge over the Genesee River</div>
		</div>

		<div class="mySlides fade">
			<div class="numbertext">6 / 7</div>
		<img src="ConceptualNewBridge2_NYDOT.jpg" style="width:100%">
			<div class="text">Conceptual view of the new arch railroad bridge over the Genesee River</div>
		</div>

		<div class="mySlides fade">
			<div class="numbertext">7 / 7</div>
		<img src="ConceptualNewBridge3_NYDOT.jpg" style="width:100%">
			<div class="text">New Portageville Railroad Bridge</div>
		</div>

		<!-- Next and previous buttons -->
		<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
		<a class="next" onclick="plusSlides(1)">&#10095;</a>
	</div>
	<br />

	<!-- The dots/circles -->
	<div style="text-align:center">
	<span class="dot" onclick="currentSlide(1)"></span>
	<span class="dot" onclick="currentSlide(2)"></span>
	<span class="dot" onclick="currentSlide(3)"></span>
	<span class="dot" onclick="currentSlide(4)"></span>
	<span class="dot" onclick="currentSlide(5)"></span>
	<span class="dot" onclick="currentSlide(6)"></span>
	<span class="dot" onclick="currentSlide(7)"></span>
	</div>

	<div class="clear"></div>
	<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>