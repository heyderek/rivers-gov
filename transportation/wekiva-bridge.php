<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Bridge Replacement, Wekiva River, Florida';

// Set the page keywords
$page_keywords = 'Wekiva, Bridge, Replacement';

// Set the page description
$page_description = 'A bridge replacement over the Wekiva River, Florida.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include("../includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include("../includes/header.php")
?>

<?php
// includes the content page top
include("../includes/content-head.php");
?>

<div id="trans-box" style="min-height: 0;">

	<div id="trans-details-box" style="min-height: 0;">

		<div>

		<!-- Insert title and text for tan summary box between the tags below. -->
		<h3>Project Overview'</h3>
		<p><strong><em>Project Proponent:</em></strong> Florida Department of Transportation<br />
		<strong><em>WSR Administering Office:</em></strong> National Park Service; Southeast Region<br />
		<strong><em>Consulting Firms:</em></strong> Figg Bridge Engineers, Inc.<br />
		<strong><em>ORVs:</em></strong> Fish, Wildlife, Historic, Recreational, Scenic<br />
		<strong><em>Project Location:</em></strong> WSR Segment</p>

		</div>

		<div class="clear">
		</div>
		<!-- Allows for content above to be flexible -->

	</div>

</div>

<div id="lower-content">

<!-- Insert title and text for main case discussion between the tags below. -->
<h2 style="line-height:150%;">Wekiva Parkway Bridge Replacement</h2>
<p>The Wekiva River Parkway Bridge replacement is part of the larger Wekiva Parkway Project headed by the Florida Department of Transportation and the Central Florida Expressway Authority (CFX). The Wekiva Parkway (State Route 429) will connect to State Route 417, completing a beltway around central Florida while helping to protect the natural resources surrounding the Wekiva Wild and Scenic River. The current 561' long Wekiva River SR46 Bridge will be replaced with a 1,750' bridge that spans the river and forested wetlands adjacent to the river, preserving the natural riverine environment and enhancing wildlife connectivity.</p>
  <dl>
		<dd>Section 7 Determination</dd>
		<dd>NPS Concurrence Letter</dd>
		<dd>Environmental Assessment</dd>
		<dd>Bridge Design</dd>
	</dl>
	<img src="images/wekiva-old-bridge.jpg" alt="Old Wekiva Bridge" width="550" height="300" title="Old Wekiva Bridge"/><br />
	<img src="images/wekiva-pier-design.jpg" alt="New Wekiva River Pier Design" width="550" height="300" title="New Wekiva River Pier Design"/><br />
	<img src="images/wekiva-new-bridge.jpg" alt="New Bridge Over Wekiva River" width="550" height="300" title="New Bridge Over Wekiva River"/>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include("../includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include("../includes/footer.php")
?>