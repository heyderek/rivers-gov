<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'Wild &amp; Scenic River Studies';

// Set the page keywords
$page_keywords = 'wild, scenic, study rivers, rivers, water, conservation, Congress';

// Set the page description
$page_description = 'Wild &amp; Scenic River Studies.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- JS that controls the accordion -->

<script type="text/javascript">
$(document).ready(function(){
$(".toggle_container").hide();
$("h2.trigger").click(function(){
$(this).toggleClass("active").next().slideToggle("slow");
});
});
</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">

<h2>Wild &amp; Scenic River Studies</h2>

<p>There are two study provisions in the Act &#8212; Section 5(a), through which Congress directs the study of select rivers, and Section 5(d)(1), which directs federal agencies to identify potential additions to the National Wild and Scenic Rivers System (National System) through federal agency plans. A brief explanation is provided in the following respective sections.</p>
</div>

<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->

<center><img src="images/yellow-headed-blackbirds.jpg" alt="Yellow-Headed Blackbirds" title="Yellow-Headed Blackbirds - Dave McCurry" width="565px" height="121px" /></center>

<div id="accords">

<h2>Current Active Studies</h2>

<p>Currently, there are five rivers or river systems under "authorized" study&#8212;four under Section 5(a) of the Wild &amp; Scenic Rivers Act and one under Section 2(a)(ii). This does not include those that might be under assessment as part of normal agency land-planning processes.</p>

<h2 class="trigger">Rivers Currently Under Study</h2>

<div class="toggle_container">

<div class="block">

<!--<h2>PARAGRAPH HEADING</h2>-->

<ul style="list-style: disc; margin-left: -.3in">
<li style="line-height: 25px"><a href="rivers/study-wood-pawcatuck.php" title="Beaver, Chipuxet, Queen, Wood and Pawcatuck Rivers, Rhode Island and Connecticut">Beaver, Chipuxet, Queen, Wood and Pawcatuck Rivers, Rhode Island and Connecticut</a> (Public Law 113-291, December 19, 2014) &#8211; Under study by the National Park Service.</li><br />
<li style="line-height: 25px"><a href="rivers/study-cave.php" title="Cave, Lake, No Name and Panther Creeks">Cave, Lake, No Name and Panther Creeks, Oregon</a> (Public Law 113-291, December 19, 2014) &#8211; Under study by the National Park Service.</li><br />
<li style="line-height: 25px"><a href="rivers/study-lower-farmington.php" title="Lower Farmington Study" alt="Lower Farmington Study">Farmington River and Salmon Brook, Connecticut</a> (Public Law 111-11, March 3, 2009) &#8211; Under study by the National Park Service.</li><br />
<li style="line-height: 25px"><a href="rivers/study-housatonic.php" title="Housatonic River, Connecticut">Housatonic River, Connecticut</a> (Governor Malloy Request for Section 2(a)(ii) Designation, November 16, 2016) &#8211; Under study by the National Park Service.</li><br />
<li style="line-height: 25px"><a href="rivers/study-nashua.php" title="Nashua River, Massachusetts">Nashua River, Massachusetts</a> (Public Law 113-291, December 19, 2014) &#8211; Under study by the National Park Service.</li><br />
<li style="line-height: 25px"><a href="rivers/study-york.php" title="York River, Maine">York River, Maine</a>. (Public Law 113-291, December 19, 2014) &#8211; Under study by the National Park Service.</li>
</ul>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<hr style="margin-top:80px;"/>

<br />

<h2>Section 2(a)(ii) Studies</h2>

<p>Under Section 2(a)(ii) of the Act, a governor (or governors for a river in multiple states) of a state can request that a river be designated, provided certain conditions are met (refer to the <a href="documents/2aii.pdf" target="_blank">Council White Paper on Section 2(a)(ii)</a> for specifics). The NPS then conducts a study to determine of certain conditions are met. Here are some of the studis conducted under Section 2(a)(ii). Again, if you don't see a study listed, we do not have a copy.</p>

<h2 class="trigger">Section 2(a)(ii) Studies Available for Download</h2>

<div class="toggle_container">

<div class="block">
<!--<h2>PARAGRAPH HEADING</h2>-->
<ul style="list-style:disc; margin-left: -.3in;">
<li><a href="documents/studies/allagash-study.pdf" title="Allagash River Study Report, Maine" target="_blank">Allagash River Study Report, Maine</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="American River Eligibility Report, California" target="_blank">American River Eligibility Report, California</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="American River Environmental Impact Statement, California" target="_blank">American River Environmental Impact Statement, California</a></li>
<li><a href="documents/studies/big-little-darby-creeks-study-ea.pdf" title="Big &amp; Little Darby Creeks Study Report &amp; Environmental Assessment, Ohio" target="_blank">Big &amp; Little Darby Creeks Study Report &amp; Environmental Assessment, Ohio</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Eel River Eligibility Report, California" target="_blank">Eel River Eligibility Report, California</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Eel River Environmental Impact Statement, California" target="_blank">Eel River Environmental Impact Statement, California</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Klamath River Eligibility Report, California" target="_blank">Klamath River Eligibility Report, California</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Klamath River Environmental Impact Statement, California" target="_blank">Klamath River Environmental Impact Statement, California</a></li>
<li><a href="documents/studies/klamath-study.pdf" title="Klamath River Study Report, Oregon" target="_blank">Klamath River Study Report, Oregon</a></li>
<li><a href="documents/studies/lumber-study.pdf" title="Lumber River Study Report, North Carolina" target="_blank">Lumber River Study Report, North Carolina</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Smith River Eligibility Report, California" target="_blank">Smith River Eligibility Report, California</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Smith River Environmental Impact Statement, California" target="_blank">Smith River Environmental Impact Statement, California</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-study.pdf" title="Trinity River Eligibility Report, California" target="_blank">Trinity River Eligibility Report, California</a></li>
<li><a href="documents/studies/american-eel-klamath-smith-trinity-eis.pdf" title="Trinity River Environmental Impact Statement, California" target="_blank">Trinity River Environmental Impact Statement, California</a></li>
<li><a href="documents/studies/wallowa-study.pdf" title="Wallowa River Study Report, Oregon" target="_blank">Wallowa River Study Report, Oregon</a></li>
<li><a href="documents/studies/westfield-river-evaluation-ea.pdf" title="Westfield River Study Report &amp; Environmental Assessment (Initial Study 1993), Massachusetts" target="_blank">Westfield River Study Report &amp; Environmental Assessment (Initial Study 1993), Massachusetts</a>
<li><a href="documents/studies/westfield-draft-2aii-addition-study.pdf" title="Westfield River Draft Study Report (Expansion 2002), Massachusetts" target="_blank">Westfield River Draft Study Report (Expansion 2002), Massachusetts</a></li>
</ul>
</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<hr style="margin-top:80px;"/>

<br />

<h2>Section 5(d)(1), Agency-Identified Studies</h2>

<p>In recent years, hundreds of rivers have been identified for study through Section 5(d)(1) of the Act. This provision directs federal agencies to identify potential addition to the National System through their respective resource and management plans. Its application has resulted in numerous individual river designations, statewide legislation (e.g., Omnibus Oregon Wild and Scenic Rivers Act, P.L. 100-557; Michigan Scenic Rivers Act, P.L. 102-249) and multi-state legislation (e.g., Omnibus Public Land Management Act of 2009, P.L. 111-11). Here are examples of agency-identified studies and transmittal documents (if available).</p>

<h2 class="trigger">Section 5(d)(1) Studies Available for Download</h2>

<div class="toggle_container">

<div class="block">

<!--<h2>PARAGRAPH HEADING</h2>-->
<ul style="list-style:disc; margin-left: -.3in">
<li><a href="documents/studies/arizona-blm-study-leis.pdf" title="Arizona Bureau of Land Management Statewide Study LEIS" target="_blank">Arizona Bureau of Land Management Statewide Study LEIS (8.6 MB PDF)</a></li>
<li><a href="documents/studies/arizona-blm-study-leis-rivers.pdf" title="Arizona Bureau of Land Management Statewide Study River Assessments" target="_blank">Arizona Bureau of Land Management Statewide Study River Assessments (10.5 MB PDF)</a></li>
<li><a href="documents/studies/blue-kp-creek-study.pdf" title="Blue River &amp; KP Creek  (Arizona)" target="_blank">Blue River &amp; KP Creek  (Arizona) (11.9 MB PDF)</a></li>
<li><a href="documents/studies/flathead-deis.pdf" title="Flathead River Draft Proposed Addition &amp; Environmental Impact Statement, Montana" target="_blank">Flathead River Draft Proposed Addition &amp; Environmental Impact Statement, Montana</a></li>
</ul>
<br />
<p style="font-size: 13pt"><strong>Utah Statewide Suitability Study:</strong></p>
<ul style="max-width:450px; list-style:disc; margin-left: -.3in">
<li><a href="documents/studies/utah-study/rod.pdf" target="_blank">Record of Decision (19.9 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/eis-cover.pdf" target="_blank">EIS Cover (697 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/summary-toc.pdf" target="_blank">Summary &amp; Table of Contents (138 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/chapter1-purpose-need.pdf" target="_blank">Chapter 1 - Purpose &amp; Need (734 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/chapter2-alternatives.pdf" target="_blank">Chapter 2 - Alternatives (2.8 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/chapter3-consequences.pdf" target="_blank">Chapter 3 - Environmental Consequences (1.4 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/chapter4-consultation.pdf" target="_blank">Chapter 4 - Consultation &amp; Coordination (109 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/chapter5-references.pdf" target="_blank">Chapter 5 - Referances (183 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/chapter6-comments.pdf" target="_blank">Chapter 6 - Comments &amp; Responses (22.4 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/addendix-a-toc.pdf" target="_blank">Appendix A - Table of Contents (799 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-a-ashley-suitability.pdf" target="_blank">Appendix A - Ashley NF Suitability Evaluation Report (36.7 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-a-dixie-fishlake-suitability.pdf" target="_blank">Appendix A - Dixie &amp; Fishlake NFs Suitability Evaluation Report (13.7 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-a-manti-suitability.pdf" target="_blank">Appendix A - Manti La Sal NF Suitability Evaluation Report (10.8 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-a-uinta-wasatch-suitability.pdf" target="_blank">Appendix A - Uinta &amp; Wasatch NF Suitability Evaluation Report (52.5 MB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-b-blm-nps-rivers.pdf" target="_blank">Appendix B - BLM &amp; NPS Rivers (686 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-c-statutory-requirements.pdf" target="_blank">Appendix C - Statutory Requirements (146 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-d-effects-managing.pdf" target="_blank">Appendix D - Effects of Managing Rivers (155 KB PDF)</a></li>
<li><a href="documents/studies/utah-study/appendix-e-water-rights-maps.pdf" target="_blank">Appendix E - Water Rights Maps (16.7 MB PDF)</a></li>
</ul>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<hr style="margin-top:80px;"/>

<br />

<h2>Congressionally Authorized Study Reports</h2>

<p>We have collected a few of the study reports prepared at the direction of Congress (see next section, "Section 5(a), Congressionally Authorized Studies," for the complete list of congressionally authorized studies). If you do not see a report here, we do not have it, and you will have to contact the study agency at the local level for a copy.</p>

<h2 class="trigger">Congressionally Authorized Study Reports Available for Download</h2>

<div class="toggle_container">

<div class="block">

<!--<h2>PARAGRAPH HEADING</h2>-->

<ul style="list-style:disc; margin-left: -.3in">
<li><a href="documents/studies/allegheny-study-deis.pdf" title="Allegheny River Study Report &amp; Draft Environmental Impact Statement, Pennsylvania" target="_blank">Allegheny River Study Report &amp; Draft Environmental Impact Statement, Pennsylvania</a></li>
<li><a href="documents/studies/suasco-draft-study.pdf" title="Assabet, Concord &amp; Sudbury Rivers Draft Study Report, Massachusetts" target="_blank">Assabet, Concord &amp; Sudbury Rivers Draft Study Report, Massachusetts</a></li>
<li><a href="documents/studies/ausable-study-eis.pdf" title="AuSable River Study Report &amp; Environmental Impact Statement, Michigan" target="_blank">AuSable River Study Report &amp; Environmental Impact Statement, Michigan</a></li>
<li><a href="documents/studies/black-creek-study-deis.pdf" title="Black Creek Draft Study Report &amp; Draft Environmental Impact Statement, Mississippi" target="_blank">Black Creek Draft Study Report &amp; Draft Environmental Impact Statement, Mississippi</a></li>
<li><a href="documents/studies/bluestone-study.pdf" title="Bluestone River Study Report, West Virginia" target="_blank">Bluestone River Study Report, West Virginia</a></li>
<li><a href="documents/studies/bruneau-study.pdf" title="Bruneau River Study Report, Idaho" target="_blank">Bruneau River Study Report, Idaho</a></li>
<li><a href="documents/studies/buffalo-study.pdf" title="Buffalo River Study Report, Tennessee" target="_blank">Buffalo River Study Report, Tennessee</a></li>
<li><a href="documents/studies/cache-study-eis.pdf" title="Cache la Poudre River Study Report &amp; Environmental Impact Statement, Colorado" target="_blank">Cache la Poudre River Study Report &amp; Environmental Impact Statement, Colorado</a></li>
<li><a href="documents/studies/chattooga-study.pdf" title="Chattooga River Study Report, Georgia, North Carolina &amp; South Carolina" target="_blank">Chattooga River Study Report, Georgia, North Carolina &amp; South Carolina</a></li>
<li><a href="documents/studies/clarion-study.pdf" title="Clarion River Study Report, Pennsylvania" target="_blank">Clarion River Study Report, Pennsylvania</a></li>
<li><a href="documents/studies/suasco-draft-study.pdf" title="Concord, Assabet &amp; Sudbury Rivers Draft Study Report, Massachusetts" target="_blank">Concord, Assabet &amp; Sudbury Rivers Draft Study Report, Massachusetts</a></li>
<li><a href="documents/studies/lower-delaware-study.pdf" title="Delaware (Lower) River Study Report, New Jersey, New York &amp; Pennsylvania" target="_blank">Delaware (Lower) River Study Report, New Jersey, New York &amp; Pennsylvania</a></li>
<li><a href="documents/studies/upper-delaware-study.pdf" title="Delaware (Upper) River Study Report, New Jersey, New York &amp; Pennsylvania" target="_blank">Delaware (Upper) River Study Report, New Jersey, New York &amp; Pennsylvania</a></li>
<li><a href="documents/studies/farmington-study.pdf" title="Farmington River Study Report, Connecticut" target="_blank">Farmington River Study Report, Connecticut</a></li>
<li><a href="documents/studies/lower-farmington-study-ea.pdf" title="Farmington (Lower) River Study Report &amp; Environmental Assessment, Connecticut" target="_blank">Farmington (Lower) River Study Report &amp; Environmental Assessment, Connecticut</a></li>
<li><a href="documents/studies/flathead-study.pdf" title="Flathead River Study Report, Montana" target="_blank">Flathead River Study Report, Montana</a></li>
<li><a href="documents/studies/great-egg-harbor-study.pdf" title="Great Egg Harbor River Study Report, New Jersey" target="_blank">Great Egg Harbor River Study Report, New Jersey</a></li>
<li><a href="documents/studies/housatonic-study.pdf" title="Housatonic River Study Report, Connecticut" target="_blank">Housatonic River Study Report, Connecticut</a></li>
<li><a href="documents/studies/illinois-study.pdf" title="Illinois River Study Report, Oregon" target="_blank">Illinois River Study Report, Oregon</a></li>
<li><a href="documents/studies/john-day-study.pdf" title="John Day River Study Report, Oregon" target="_blank">John Day River Study Report, Oregon</a></li>
<li><a href="documents/studies/kern-nf-study.pdf" title="Kern (North Fork) River Study Report, California" target="_blank">Kern (North Fork) River Study Report, California</a></li>
<li><a href="documents/studies/kern-nf-study-eis.pdf" title="Kern (North Fork) River Study Environmental Impact Statement, California" target="_blank">Kern (North Fork) River Study Environmental Impact Statement, California</a></li>
<li><a href="documents/studies/kern-sf-study-deis.pdf" title="Kern (South Fork) River Study &amp; Environmental Impact Statement, California" target="_blank">Kern (South Fork) River Study &amp; Environmental Impact Statement, California</a></li>
<li><a href="documents/studies/kern-nf-sf-rod.pdf" title="Kern (North &amp; South Forks) River Record of Decision, California" target="_blank">Kern (North &amp; South Forks) River Record of Decision, California</a></li>
<li><a href="documents/studies/klamath-upper-draft-study.pdf" title="Klamath River Draft Study Report (Section 5(d)(2) of the Act), Oregon" target="_blank">Klamath River Draft Study Report (Section 5(d)(2) of the Act), Oregon</a></li>
<li><a href="documents/studies/lamprey-study.pdf" title="Lamprey River Study Report, New Hampshire" target="_blank">Lamprey River Study Report, New Hampshire</a></li>
<li><a href="documents/studies/lamprey-resource-assessment.pdf" title="Lamprey River Resource Assesssment, New Hampshire" target="_blank">Lamprey River Resource Assesssment, New Hampshire</a></li>
<li><a href="documents/studies/little-beaver-creek-study.pdf" title="Little Beaver Creek Study Report, Ohio" target="_blank">Little Beaver Creek Study Report, Ohio</a></li>
<li><a href="documents/studies/little-miami-plan.pdf" title="Little Miami River Study Report, Ohio" target="_blank">Little Miami River Study Report, Ohio</a></li>
<li><a href="documents/studies/loxahatchee-study-eis.pdf" title="Loxahatchee River Study Report &amp; Environmental Impact Statement, Florida" target="_blank">Loxahatchee River Study Report &amp; Environmental Impact Statement, Florida</a></li>
<li><a href="documents/studies/malheur-nf-study.pdf" title="Malhuer (North Fork) River Study Report, Oregon" target="_blank">Malhuer (North Fork) River Study Report, Oregon</a></li>
<li><a href="documents/studies/manistee-study-eis.pdf" title="Manistee River Study Report &amp; Environmental Impact Statement, Michigan" target="_blank">Manistee River Study Report &amp; Environmental Impact Statement, Michigan</a></li>
<li><a href="documents/studies/maurice-eligibility-classification-report.pdf" title="Maurice River Eligibility &amp; Classification Report, New Jersey" target="_blank">Maurice River Eligibility &amp; Classification Report, New Jersey</a></li>
<li><a href="documents/studies/maurice-study.pdf" title="Maurice River Study Report, New Jersey" target="_blank">Maurice River Study Report, New Jersey</a></li>
<li><a href="documents/studies/merrimack-draft-study.pdf" title="Merrimack (Upper) River Draft Study Report, New Hampshire" target="_blank">Merrimack (Upper) River Draft Study Report, New Hampshire</a></li>
<li><a href="documents/studies/missisquoi-trout-study-ea.pdf" title="Missisquoi River Study Report &amp; Environmental Assessment, Vermont" target="_blank">Missisquoi River Study Report &amp; Environmental Assessment, Vermont</a></li>
<li><a href="documents/studies/missouri-study-mt.pdf" title="Missouri River Study Report, Montana" target="_blank">Missouri River Study Report, Montana</a></li>
<li><a href="documents/studies/missouri-study-environmental-statement.pdf" title="Missouri River Environmental Statement, Montana" target="_blank">Missouri River Environmental Statement, Montana</a></li>
<li><a href="documents/studies/musconetcong-study.pdf" title="Musconetcong River Study Report, New Jersey" target="_blank">Musconetcong River Study Report, New Jersey</a></li>
<li><a href="documents/studies/new-study.pdf" title="New River Study Report, Virginia &amp; West Virginia" target="_blank">New River Study Report, Virginia &amp; West Virginia</a></li>
<li><a href="documents/studies/new-study-memos.pdf" title="New River Study Transmittal Memorandums, Virginia &amp; West Virginia" target="_blank">New River Study Transmittal Memorandums, Virginia &amp; West Virginia</a></li>
<li><a href="documents/studies/new-sf-study-eis.pdf" title="New River (South Fork) Study Report &amp; Environmental Impact Statement, North Carolina" target="_blank">New River (South Fork) Study Report &amp; Environmental Impact Statement, North Carolina</a></li>
<li><a href="documents/studies/niobrara-study.pdf" title="Niobrara River Study Report, Nebraska" target="_blank">Niobrara River Study Report, Nebraska</a></li>
<li><a href="documents/studies/obed-study.pdf" title="Obed River Study Report, Tennessee" target="_blank">Obed River Study Report, Tennessee</a></li>
<li><a href="documents/studies/owyhee-id-study-eis.pdf" title="Owyhee River Study Report, Idaho" target="_blank">Owyhee River Study Report, Idaho</a></li>
<li><a href="documents/studies/pemigewasset-draft-study.pdf" title="Pemigewasset River Draft Study Report, New Hampshire" target="_blank">Pemigewasset River Draft Study Report, New Hampshire</a></li>
<li><a href="documents/studies/pemigewasset-draft-study-appendices.pdf" title="Pemigewasset River Draft Study Report Appendices, New Hampshire" target="_blank">Pemigewasset River Draft Study Report Appendices, New Hampshire</a></li>
<li><a href="documents/studies/pere-marquette-study.pdf" title="Pere Marquette River Study Report, Michigan" target="_blank">Pere Marquette River Study Report, Michigan</a></li>
<li><a href="documents/studies/red-draft-study-eis.pdf" title="Red River Draft Study Report &amp; Environmental Impact Statement, Kentucky" target="_blank">Red River Draft Study Report &amp; Environmental Impact Statement, Kentucky</a></li>
<li><a href="documents/studies/rio-grande-tx-study.pdf" title="Rio Grande River Study Report, Texas" target="_blank">Rio Grande River Study Report, Texas</a></li>
<li><a href="documents/studies/rio-grande-tx-eis.pdf" title="Rio Grande River Study Environmental Impact Statement, Texas" target="_blank">Rio Grande River Study Environmental Impact Statement, Texas</a></li>
<li><a href="documents/studies/lower-st-croix-study.pdf" title="St. Croix River Study Report, Minnesota &amp; Wisconsin" target="_blank">St. Croix River Study Report, Minnesota &amp; Wisconsin</a></li>
<li><a href="documents/studies/st-marys-study.pdf" title="St. Marys River Study Report, Florida" target="_blank">St. Marys River Study Report, Florida</a></li>
<li><a href="documents/studies/sheenjek-study-leis.pdf" title="Sheenjek River Study Report &amp; Legislative Environmental Impact Statement, Alaska" target="_blank">Sheenjek River Study Report &amp; Legislative Environmental Impact Statement, Alaska</a></li>
<li><a href="documents/studies/skagit-study.pdf" title="Skagit River Study Report, Washington" target="_blank">Skagit River Study Report, Washington</a></li>
<li><a href="documents/studies/snake-study-eis.pdf" title="Snake River Study Report &amp; Environmental Impact Statement, Idaho, Oregon &amp; Washington" target="_blank">Snake River Study Report &amp; Environmental Impact Statement, Idaho, Oregon &amp; Washington</a></li>
<li><a href="documents/studies/suasco-draft-study.pdf" title="Sudbury, Assabet &amp; Concord Rivers Study Report, Massachusetts" target="_blank">Sudbury, Assabet &amp; Concord Rivers Study Report, Massachusetts</a></li>
<li><a href="documents/studies/sweetwater-study.pdf" title="Sweetwater River Study Report, Wyoming" target="_blank">Sweetwater River Study Report, Wyoming</a></li>
<li><a href="documents/studies/taunton-draft-study-ea.pdf" title="Taunton River Draft Study Report &amp; Environmental Assessment, Massachusetts" target="_blank">Taunton River Draft Study Report &amp; Environmental Assessment, Massachusetts</a></li>
<li><a href="documents/studies/missisquoi-trout-study-ea.pdf" title="Trout-Missisquoi River Study Report &amp; Environmental Assessment, Vermont" target="_blank">Trout-Missisquoi River Study Report &amp; Environmental Assessment, Vermont</a></li>
<li><a href="documents/studies/tuolumne-study.pdf" title="Tuolumne River Study Report, California" target="_blank">Tuolumne River Study Report, California</a></li>
<li><a href="documents/studies/verde-study-eis.pdf" title="Verde River Study Report &amp; Environmental Assessment, Arizona" target="_blank">Verde River Study Report &amp; Environmental Assessment, Arizona</a></li>
<li><a href="documents/studies/wekiva-study.pdf" title="Wekiva River Study Report, Florida" target="_blank">Wekiva River Study Report, Florida</a></li>
<li><a href="documents/studies/white-clay-creek-draft-study.pdf" title="White Clay Creek Draft Study Report, Delaware &amp; Pennsylvania" target="_blank">White Clay Creek Draft Study Report, Delaware &amp; Pennsylvania</a></li>
<li><a href="documents/studies/wildcat-brook-draft-study.pdf" title="Wildcat Brook Draft Study Report, New Hampshire" target="_blank">Wildcat Brook Draft Study Report, New Hampshire</a></li>
<li><a href="documents/studies/wolf-bor-study.pdf" title="Wolf River Bureau of Outdoor Rrecreation Study Report, Wisconsin" target="_blank">Wolf River Bureau of Outdoor Rrecreation Study Report, Wisconsin</a></li>
<li><a href="documents/studies/wolf-draft-study.pdf" title="Wolf River Lake Central Regional Task Group Draft Study Report, Wisconsin" target="_blank">Wolf River Lake Central Regional Task Group Draft Study Report, Wisconsin</a></li>
<li><a href="documents/studies/yellowstone-study-environmental-statement.pdf" title="Yellowstone (Clarks Fork) River Study Report &amp; Environmental Statement, Wyoming" target="_blank">Yellowstone (Clarks Fork) River Study Report &amp; Environmental Statement, Wyoming</a></li>
</ul>
</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<hr style="margin-top:80px;"/>

<br />

<h2>Section 5(a), Congressionally Authorized Studies</h2>

<p>Through Section 5(a), Congress authorizes the study of select rivers and directs one of the four federal river-administering agencies to conduct the study, as outlined in Sections 4(a) and 5(c) of the Wild &amp; Scenic Rivers Act. The enabling legislation of 1968, P.L. 90-542, authorized 27 rivers for study as potential components of the National System. Amendments to the law have increased the number of studies authorized by Congress to 144.</p>

<p>These studies have lead to 48 designations by either Congress or the Secretary of the Interior. One study led to the establishment of a National Recreation Area.</p>

<p>The number of rivers included in the National System differs from the number of rivers authorized for study by Congress for the following reasons:

<ul style="max-width:450px; list-style:disc;">
<li>Not all rivers studied are found eligible or suitable for designation&#8212;many study rivers will not be included in the National System.</li>
<li>Some rivers are designated by Congress or the Secretary of the Interior without a pre-authorization or 5(a) study (e.g., Niobrara River).</li>
<li>Some rivers are designated as a result of recommendation in federal agency plans (e.g., 49 rivers designated in Oregon in 1988).</li>
</ul>
</p>

<p>The 144 rivers below have been authorized for study. The agency leading the study is indicated as National Park Service (NPS), Bureau of Outdoor Recreation (BOR), Heritage Conservation and Recreation Service (HCRS), Bureau of Land Management (BLM), or U.S. Forest Service (USFS). Within the Department of the Interior, the study function was transferred from the HCRS (formerly the BOR) to the NPS by Secretarial Order Number 3017, January 25, 1978. All studies indicated as BOR or HCRS were completed by these agencies before the program was transferred to the NPS. The BLM was delegated responsibility for conducting studies on Public Lands on October 11, 1988. The USFS (Department of Agriculture) has always conducted studies on National Forest System Lands and as directed by Congress.</p>

<h2 class="trigger">Section 5(a), Congressionally Authorized Studies</h2>

<div class="toggle_container">

<div class="block">

<!--<h2>PARAGRAPH HEADING</h2>-->

<p>For each study river, the number in parentheses is the approximate number of miles to be studied. If river segments were designated, the total designated mileage appears in the text.</p>
<p>Several of these studies are available in the section above (Section 5(a), Congressionally Authorized Studies).
<p><em><strong>I. Public Law 90-542 (October 2, 1968) &#8212; 27 rivers, studies due October 2, 1978</strong></em></p>
<p style="margin-left:.5in">(1) <strong>Allegheny, Pennsylvania.</strong> (BOR) Letter report to Congress on January 23, 1974. River not qualified. (69.5 miles)</p>
<p style="margin-left:.5in">(2) <strong>Bruneau, Idaho.</strong> (BOR) Report recommending congressional designation transmitted to Congress on May 23, 1977. (121 miles)</p>
<p style="margin-left:.5in">(3) <strong>Buffalo, Tennessee.</strong> (NPS) Report transmitted to Congress on October 2, 1979. Preservation of river by state recommended. (117 miles)</p>
<p style="margin-left:.5in">(4) <strong>Chattooga, North Carolina, South Carolina, and Georgia.</strong> (USFS) Fifty-six point nine miles added to the National System, Public Law 93-279, May 10, 1974. (56.9 miles)</p>
<p style="margin-left:.5in">(5) <strong>Clarion, Pennsylvania.</strong> (BOR) Letter report to Congress on February 22, 1974. River not qualified. (90 miles)</p>
<p style="margin-left:.5in">(6) <strong>Delaware, Pennsylvania and New York.</strong> (BOR) Seventy-five point four miles added to the National System, Public Law 95-625, November 10, 1978. (75.4 miles)</p>
<p style="margin-left:.5in">(7) <strong>Flathead, Montana.</strong> (USFS) Two hundred nineteen miles added to the National System, Public Law 94-486, October 12, 1976. (219 miles)</p>
<p style="margin-left:.5in">(8) <strong>Gasconade, Missouri.</strong> (BOR) Report transmitted to Congress on May 23, 1977. Preservation of river by state recommended. (265 miles)</p>
<p style="margin-left:.5in">(9) <strong>Illinois, Oregon.</strong> (USFS) Fifty point four miles added to the National System, Public Law 98-494, October 19, 1984. (88 miles)</p>
<p style="margin-left:.5in">(10) <strong>Little Beaver, Ohio.</strong> (BOR) Thirty-three miles added to the National System by the Secretary of the Interior on October 23, 1975. Report transmitted to Congress on February 10, 1976. (33 miles)</p>
<p style="margin-left:.5in">(11) <strong>Little Miami, Ohio.</strong> (BOR) Sixty-six miles added to the National System by the Secretary of the Interior on August 20, 1973. Report transmitted to Congress on November 5, 1973. An additional 28-mile segment was added by the Secretary of the Interior on January 28, 1980. (94 miles)</p>
<p style="margin-left:.5in">(12) <strong>Maumee, Ohio and Indiana.</strong> (BOR) Report transmitted to Congress on September 13, 1974. River not qualified. (236 miles)</p>
<p style="margin-left:.5in">(13) <strong>Missouri, Montana.</strong> (BOR) One hundred forty-nine miles added to the National System, Public Law 94-486, October 12, 1976. (180 miles)</p>
<p style="margin-left:.5in">(14) <strong>Moyie, Idaho.</strong> (USFS) Report transmitted to Congress on September 13, 1982. Designation not recommended. (26.1 miles)</p>
<p style="margin-left:.5in">(15) <strong>Obed, Tennessee.</strong> (BOR/NPS) Forty-five miles added to the National System, Public Law 94-486, October 12, 1976. Report transmitted to Congress on April 26, 1985. Submission of final report was in abeyance pending completion of a mineral evaluation. Further designation was not recommended. (100 miles)</p>
<p style="margin-left:.5in">(16) <strong>Penobscot, Maine.</strong> (BOR) Report transmitted to Congress on May 23, 1977. Preservation of river by state recommended. (327 miles)</p>
<p style="margin-left:.5in">(17) <strong>Pere Marquette, Michigan.</strong> (USFS) Sixty-six point four miles added to the National System, Public Law 95-625, November 10, 1978. (153 miles)</p>
<p style="margin-left:.5in">(18) <strong>Pine Creek, Pennsylvania.</strong> (NPS) Report transmitted to Congress on October 2, 1979. Preservation of river by state recommended. (51.7 miles)</p>
<p style="margin-left:.5in">(19) <strong>Priest, Idaho.</strong> (USFS) Report recommending congressional designation transmitted to Congress on October 2, 1979. (67 miles)</p>
<p style="margin-left:.5in">(20) <strong>Rio Grande, Texas.</strong> (BOR) One hundred ninety-one point two miles added to the National System, Public Law 95-625, November 10, 1978. (556 miles)</p>
<p style="margin-left:.5in">(21) <strong>Saint Croix, Minnesota and Wisconsin.</strong> (BOR) Twenty-seven mile federally administered segment added to the National System by Public Law 92-560, October 25, 1972. Twenty-five mile state-administered segment added by the Secretary of the Interior on June 17, 1976. (52 miles)</p>
<p style="margin-left:.5in">(22) <strong>St. Joe, Idaho.</strong> (USFS) Sixty-six point three miles added to the National System, Public Law 95-625, November 10, 1978. (132.1 miles)</p>
<p style="margin-left:.5in">(23) <strong>Salmon, Idaho.</strong> (USFS) One hundred twenty-five miles added to the National System, Public Law 96-312, July 23, 1980. Additional 53 miles subject to provisions of Section 7(a) of Public Law 90-542. (237 miles)</p>
<p style="margin-left:.5in">(24) <strong>Skagit, Washington.</strong> (USFS) One hundred fifty-seven point five miles added to the National System, Public Law 95-625, November 10, 1978. (166.3 miles)</p>
<p style="margin-left:.5in">(25) <strong>Suwannee, Florida and Georgia.</strong> (BOR) Report transmitted to Congress on March 15, 1974. Preservation of river by state recommended. (272 miles)</p>
<p style="margin-left:.5in">(26) <strong>Upper Iowa, Iowa.</strong> (BOR) Report transmitted to Congress on May 11, 1972. Preservation of river by state recommended. (80 miles)</p>
<p style="margin-left:.5in">(27) <strong>Youghigheny, Maryland and Pennsylvania.</strong> (NPS) Report transmitted to Congress on October 2, 1979. Preservation of river by state recommended. (49 miles)</p>
<p><em><strong>II. Public Law 93-621 (January 3, 1975) &#8212; 29 rivers, studies due October 2, 1979, except the Dolores River due October 2, 1976, and the Green and Yampa Rivers due January 1, 1987</strong></em></p>
<p style="margin-left:.5in">(28) <strong>American, California.</strong> (USFS) Thirty-eight point three miles added to the National System, Public Law 95-625, November 10, 1978. (41.1 miles)</p>
<p style="margin-left:.5in">(29) <strong>AuSable, Michigan.</strong> (USFS) Twenty-three miles added to the National System, Public Law 98-444, October 4, 1984. (165 miles)</p>
<p style="margin-left:.5in">(30) <strong>Big Thompson, Colorado.</strong> (NPS) Report transmitted to Congress on October 2, 1979. Designation not recommended. (13.6 miles)</p>
<p style="margin-left:.5in">(31) <strong>Cache la Poudre, Colorado.</strong> (USFS) Seventy-six miles added to the National System, Public Law 99-590, October 30, 1986. (76 miles)</p>
<p style="margin-left:.5in">(32) <strong>Cahaba, Alabama.</strong> (USFS) Report transmitted to Congress on December 14, 1979. River not qualified. (116 miles)</p>
<p style="margin-left:.5in">(33) <strong>Clarks Fork, Wyoming.</strong> (USFS) Twenty point five miles added to the National System, Public Law 101-628, November 28, 1990. (23 miles)</p>
<p style="margin-left:.5in">(34) <strong>Colorado, Colorado and Utah.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Designation not recommended. (75.7 miles)</p>
<p style="margin-left:.5in">(35) <strong>Conejos, Colorado.</strong> (USFS) Report recommending congressional designation transmitted to Congress on September 13, 1982. (48.8 miles)</p>
<p style="margin-left:.5in">(36) <strong>Elk, Colorado.</strong> (USFS) Report recommending congressional designation transmitted to Congress on September 13, 1982. (35 miles)</p>
<p style="margin-left:.5in">(37) <strong>Encampment, Colorado.</strong> (USFS) Report recommending congressional designation transmitted to Congress on October 2, 1979. (19.5 miles)</p>
<p style="margin-left:.5in">(38) <strong>Green, Colorado and Utah.</strong> (NPS) Report transmitted to Congress in combination with the Yampa River on November 14, 1983. The river was determined eligible, but the Secretary did not include a recommendation for designation. (91 miles)</p>
<p style="margin-left:.5in">(39) <strong>Gunnison, Colorado.</strong> (NPS) Report recommending congressional designation transmitted to Congress on October 2, 1979. (29 miles)</p>
<p style="margin-left:.5in">(40) <strong>Illinois, Oklahoma.</strong> (HCRS) Report transmitted to Congress on October 2, 1979. Preservation of river by state recommended. (115 miles)</p>
<p style="margin-left:.5in">(41) <strong>John Day, Oregon.</strong> (NPS) One hundred forty-seven point five miles added to the National System, Public Law 100-557, October 28, 1988. (149 miles)</p>
<p style="margin-left:.5in">(42) <strong>Kettle, Minnesota.</strong> (NPS) Report transmitted to Congress on October 2, 1979. Preservation of river by state recommended. (79 miles)</p>
<p style="margin-left:.5in">(43) <strong>Los Pinos, Colorado.</strong> (USFS) Report recommending congressional designation transmitted to Congress on September 13, 1982. (54 miles)</p>
<p style="margin-left:.5in">(44) <strong>Manistee, Michigan.</strong> (USFS) Twenty-six miles added to the National System, Public Law 102-249, March 3, 1992. (232 miles)</p>
<p style="margin-left:.5in">(45) <strong>Nolichucky, Tennessee and North Carolina.</strong> (NPS) Report transmitted to Congress on April 26, 1985. River not qualified. (110 miles)</p>
<p style="margin-left:.5in">(46) <strong>Owyhee, Oregon.</strong> (NPS) One hundred twenty miles added to the National System, Public Law 98-494, October 19, 1984. (192 miles)</p>
<p style="margin-left:.5in">(47) <strong>Piedra, Colorado.</strong> (USFS) Report recommending congressional designation transmitted to Congress on September 13, 1982. (53 miles)</p>
<p style="margin-left:.5in">(48) <strong>Shepaug, Connecticut.</strong> (NPS) Report transmitted to Congress on October 2, 1979. Preservation of river by state and local action recommended. (28 miles)</p>
<p style="margin-left:.5in">(49) <strong>Sipsey Fork, Alabama.</strong> (USFS) Sixty-one miles added to the National System, Public Law 100-547, October 28, 1988. (71 miles)</p>
<p style="margin-left:.5in">(50) <strong>Snake, Wyoming.</strong> (USFS) Report recommending congressional designation transmitted to Congress on September 13, 1982. (50 miles)</p>
<p style="margin-left:.5in">(51) <strong>Sweetwater, Wyoming.</strong> (NPS) Report transmitted to Congress on November 14, 1979. Designation not recommended. (9.5 miles)</p>
<p style="margin-left:.5in">(52) <strong>Tuolumne, California.</strong> (NPS/USFS) Eighty-three miles added to the National System, Public Law 98-425, September 28, 1984. (92 miles)</p>
<p style="margin-left:.5in">(53) <strong>Upper Mississippi, Minnesota.</strong> (BOR) Report recommending congressional designation transmitted to Congress on August 25, 1977. (466 miles)</p>
<p style="margin-left:.5in">(54) <strong>Wisconsin, Wisconsin.</strong> (NPS/USFS) Report transmitted to Congress on October 2, 1979. Preservation of river by state recommended. (82.4 miles)</p>
<p style="margin-left:.5in">(55) <strong>Yampa, Colorado.</strong> (NPS) Report transmitted to Congress in combination with Green River on November 14, 1983. The river was determined eligible, but the Secretary did not include a recommendation for designation. (47 miles)</p>
<p style="margin-left:.5in">(56) <strong>Dolores, Colorado.</strong> (BOR/USFS) Report recommending Congressional designation transmitted to Congress on May 23, 1977. (105 miles)</p>
<p><em><strong>III. Public Law 94-199 (December 31, 1975) &#8212; 1 river, study due October 1, 1979</strong></em></p>
<p style="margin-left:.5in">(57) <strong>Snake, Washington, Oregon and Idaho.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Designation not recommended. (33 miles)</p>
<p><em><strong>IV. Public Law 94-486 (October 12, 1976) &#8212; 1 river, study due October 1, 1980</strong></em></p>
<p style="margin-left:.5in">(58) <strong>Housatonic, Connecticut.</strong> (NPS) Report transmitted to Congress on October 2, 1979. Preservation of river by state and local action recommended. (51 miles)</p>
<p><em><strong>V. Public Law 95-625 (November 10, 1978) &#8212; 17 rivers, studies due October 1, 1984</strong></em></p>
<p style="margin-left:.5in">(59) <strong>Kern (North Fork), California.</strong> (USFS) One hundred fifty-one miles of the North and South Forks added to the National System, Public Law 100-174, November 24, 1987. (74 miles)</p>
<p style="margin-left:.5in">(60) <strong>Loxahatchee, Florida.</strong> (NPS) Seven point five miles added to the National System by the Secretary of the Interior on May 17, 1985. (25 miles)</p>
<p style="margin-left:.5in">(61) <strong>Ogeechee, Georgia.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Preservation of river by state recommended. (246 miles)</p>
<p style="margin-left:.5in">(62) <strong>Salt, Arizona.</strong> (USFS) Report transmitted to Congress on September 13, 1982. Designation not recommended. (22 miles)</p>
<p style="margin-left:.5in">(63) <strong>Verde, Arizona.</strong> (USFS) Forty point five miles added to the National System, Public Law 98-406, August 28, 1984. (78 miles)</p>
<p style="margin-left:.5in">(64) <strong>San Francisco, Arizona.</strong> (USFS) Report transmitted to Congress on September 13, 1982. Designation not recommended. (29 miles)</p>
<p style="margin-left:.5in">(65) <strong>Fish Creek, East Branch, New York.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Preservation of river by state and local action recommended. (49 miles)</p>
<p style="margin-left:.5in">(66) <strong>Black Creek, Mississippi.</strong> (USFS) Twenty-one miles added to the National System, Public Law 99-590, October 30, 1986. (122.8 miles)</p>
<p style="margin-left:.5in">(67) <strong>Allegheny, Pennsylvania.</strong> (USFS) Eighty-five miles added to the National System, Public Law 102-271, April 20, 1992. (128 miles)</p>
<p style="margin-left:.5in">(68) <strong>Cacapon, West Virginia.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Preservation of river by state and local action recommended. (114 miles)</p>
<p style="margin-left:.5in">(69) <strong>Escatawpa, Alabama and Mississippi.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Preservation of river by state and local action recommended. (72 miles)</p>
<p style="margin-left:.5in">(70) <strong>Myakka, Florida.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Preservation of river by state recommended. (37 miles)</p>
<p style="margin-left:.5in">(71) <strong>Soldier Creek, Alabama.</strong> (NPS) Report transmitted to Congress on April 26, 1985. River not qualified. (.2 miles)</p>
<p style="margin-left:.5in">(72) <strong>Red, Kentucky.</strong> (USFS) Nineteen point four miles added to the National System, Public Law 103-170, December 2, 1993. (19.4 miles)</p>
<p style="margin-left:.5in">(73) <strong>Bluestone, West Virginia.</strong> (NPS) Ten miles added to the National System, Public Law 100-534, October 26, 1988. (40 miles)</p>
<p style="margin-left:.5in">(74) <strong>Gauley, West Virginia.</strong> (NPS) A 25-mile segment established as a National Recreation Area on October 26, 1988. (164 miles)</p>
<p style="margin-left:.5in">(75) <strong>Greenbrier, West Virginia.</strong> (USFS) Report transmitted to Congress on January 7, 1993. Preservation of river by state and local action recommended. (175 miles)</p>
<p><em><strong>VI. Public Law 96-199 (March 5, 1980) &#8212; 1 river, study due October 1, 1984</strong></em></p>
<p style="margin-left:.5in">(76) <strong>Birch, West Virginia.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Preservation of river by state and local action recommended. (20 miles)</p>
<p><em><strong>VII. Public Law 96-487 (December 2, 1980) &#8212; 12 rivers, studies due October 1, 1984, except the Sheenjek and Squirrel Rivers due January 1, 1987.</strong></em> <strong>The following rivers were added for study by the Alaska National Interest Lands Conservation Act (ANILCA.)</strong>
<p style="margin-left:.5in">(77) <strong>Colville, Alaska.</strong> (NPS) Study submitted to Congress on April 12, 1979, as part of 105(c) study mandated by Public Law 94-258. This was prior to passage of ANILCA. (428 miles)</p>
<p style="margin-left:.5in">(78) <strong>Etivluk-Nigu, Alaska.</strong> (NPS) Study submitted to Congress on April 12, 1979, as part of 105(c) study mandated by Public Law 94-258. This was prior to passage of ANILCA. (160 miles)</p>
<p style="margin-left:.5in">(79) <strong>Utukok, Alaska.</strong> (NPS) Study submitted to Congress on April 12, 1979, as part of 105(c) study mandated by Public Law 94-258. This was prior to passage of ANILCA. (250 miles)</p>
<p style="margin-left:.5in">(80) <strong>Kanektok, Alaska.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Designation not recommended. (75 miles)</p>
<p style="margin-left:.5in">(81) <strong>Kisaralik, Alaska.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Designation not recommended. (75 miles)</p>
<p style="margin-left:.5in">(82) <strong>Melozitna, Alaska.</strong> (NPS) Report transmitted to Congress on April 26, 1985. River not qualified. (270 miles)</p>
<p style="margin-left:.5in">(83) <strong>Sheenjek (lower segment), Alaska.</strong> (NPS) Report recommending congressional designation transmitted to Congress on January 19, 2001. (109 miles)</p>
<p style="margin-left:.5in">(84) <strong>Situk, Alaska.</strong> (USFS) Report transmitted to Congress on April 26, 1985. Designation not recommended. (21 miles)</p>
<p style="margin-left:.5in">(85) <strong>Porcupine, Alaska.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Designation not recommended. (75 miles)</p>
<p style="margin-left:.5in">(86) <strong>Yukon (Ramparts section), Alaska.</strong> (NPS) Report transmitted to Congress on April 26, 1985. Designation not recommended. (128 miles)</p>
<p style="margin-left:.5in">(87) <strong>Squirrel, Alaska.</strong> (Initiated by NPS/Completed by BLM) Final report/EIS issued January 26, 1999. Designation not recommended. (72 miles)</p>
<p style="margin-left:.5in">(88) <strong>Koyuk, Alaska.</strong> (NPS) Report transmitted to Congress on April 26, 1985. River not qualified. (159 miles)</p>
<p><em><strong>VIII. Public Law 98-323 (June 6, 1984) &#8212; 1 river, study due October 1, 1990</strong></em></p>
<p style="margin-left:.5in">(89) <strong>Wildcat Creek, New Hampshire.</strong> (NPS) Fourteen point five miles added to the National System, Public Law 100-554, October 28, 1988. (21 miles)</p>
<p><em><strong>IX. Public Law 98-484 (October 17, 1984) &#8212; 1 river, study due October 17, 1987</strong></em></p>
<p style="margin-left:.5in">(90) <strong>Horsepasture, North Carolina.</strong> (USFS) Four point two miles added to the National System, Public Law 99-530, October 27, 1986. (4.2 miles)</p>
<p><em><strong>X. Public Law 98-494 (October 19, 1984) &#8212; 1 river, study due October 1, 1988</strong></em></p>
<p style="margin-left:.5in">(91) <strong>North Umpqua, Oregon.</strong> (USFS) Thirty-three point eight miles added to the National System, Public Law 100-557, October 28, 1988. (33.8 miles)</p>
<p><em><strong>XI. Public Law 99-590 (October 30, 1986) &#8212; 2 rivers, studies due October 30, 1989, for the Great Egg Harbor and October 1, 1990, for the Farmington</strong></em></p>
<p style="margin-left:.5in">(92) <strong>Farmington, West Branch, Connecticut and Massachusetts.</strong> (NPS) Fourteen miles added to the National System, Public Law 103-313, August 26, 1994. Report transmitted to Congress on December 13, 1995. (25 miles)</p>
<p style="margin-left:.5in">(93) <strong>Great Egg Harbor, New Jersey.</strong> (NPS) One hundred twenty-nine miles added to the National System, Public Law 102-536, October 26, 1992. (127 miles)</p>
<p><em><strong>XII. Public Law 99-663 (November 17, 1986) &#8212; 2 rivers, studies due October 1, 1990</strong></em></p>
<p style="margin-left:.5in">(94) <strong>Klickitat, Washington.</strong> (USFS) Draft report issued June 1990. Final report completed, but not transmitted to Congress. (30 miles)</p>
<p style="margin-left:.5in">(95) <strong>White Salmon, Washington.</strong> (USFS) Twenty miles added to the National System, Public Law 109-44, August 2, 2005. The portion designated was added to the study by the USFS and is the headwaters above the segment authorized for study. (13.5 miles)</p>
<p><em><strong>XIII. Public Law 100-33 (May 7, 1987) &#8212; 3 rivers, studies due October 1, 1990</strong></em></p>
<p style="margin-left:.5in">(96) <strong>Maurice, New Jersey.</strong> (NPS) Ten point five miles added to the National System, Public Law 103-162, December 1, 1993. (14 miles)</p>
<p style="margin-left:.5in">(97) <strong>Manumuskin, New Jersey.</strong> (NPS) Fourteen point three miles added to the National System, Public Law 103-162, December 1, 1993. (3.5 miles)</p>
<p style="margin-left:.5in">(98) <strong>Menantico Creek, New Jersey.</strong> (NPS) Seven point nine miles added to the National System, Public Law 103-162, December 1, 1993. (7 miles)</p>
<p><em><strong>XIV. Public Law 100-149 (November 2, 1987) &#8212; 1 river, study due October 1, 1991</strong></em></p>
<p style="margin-left:.5in">(99) <strong>Merced, California.</strong> (BLM) Eight miles added to the National System, Public Law 102-432, October 23, 1992. (8 miles)</p>
<p><em><strong>XV. Public Law 100-557 (October 28, 1988) &#8212; 6 rivers, studies due October 1, 1992</strong></em></p>
<p style="margin-left:.5in">(100) <strong>Blue, Oregon.</strong> (USFS) Study initiated in 1989. River determined ineligible, but report not transmitted to Congress. (9 miles)</p>
<p style="margin-left:.5in">(101) <strong>Chewaucan, Oregon.</strong> (USFS) Study initiated in 1989. River determined ineligible, but report not transmitted to Congress. (23 miles)</p>
<p style="margin-left:.5in">(102) <strong>North Fork Malheur, Oregon.</strong> (BLM) River determined eligible, but report not transmitted to Congress. (15 miles)</p>
<p style="margin-left:.5in">(103) <strong>South Fork McKenzie, Oregon.</strong> (USFS) Study initiated in 1989. River determined eligible, with plans to complete the study at revision of the Willamette National Forest Land and Resource Management Plan. (26 miles)</p>
<p style="margin-left:.5in">(104) <strong>Steamboat Creek, Oregon.</strong> (USFS) Final report completed in 1993. River determined eligible, but report not transmitted to Congress. (24 miles)</p>
<p style="margin-left:.5in">(105) <strong>Wallowa, Oregon.</strong> (USFS) Ten miles added to the National System by the Secretary of the Interior on July 25, 1996. (10 miles)</p>
<p><em><strong>XVI. Public Law 101-356 (August 10, 1990) &#8212; 1 river, study due August 10, 1993</strong></em></p>
<p style="margin-left:.5in">(106) <strong>Merrimack, New Hampshire.</strong> (NPS) Draft report issued October 7, 1999. River was determined eligible, but final report not transmitted to Congress. (22 miles)</p>
<p><em><strong>XVII. Public Law 101-357 (August 10, 1990) &#8212; 1 river, study due August 10, 1993</strong></em></p>
<p style="margin-left:.5in">(107) <strong>Pemigewasset, New Hampshire.</strong> (NPS) Report transmitted to Congress on May 5, 1998. Designation not recommended. (36 miles)</p>
<p><em><strong>XVIII. Public Law 101-364 (August 15, 1990) &#8212; 1 river, study due August 15, 1993</strong></em></p>
<p style="margin-left:.5in">(108) <strong>St. Marys, Florida.</strong> (NPS) Draft report issued on March 16, 1994. River was determined eligible, but final report not transmitted to Congress. (120 miles)</p>
<p><em><strong>XIX. Public Law 101-538 (November 8, 1990) &#8212; 1 river, study due September 30, 1994</strong></em></p>
<p style="margin-left:.5in">(109) <strong>Mills, North Carolina.</strong> (USFS) Final report completed in 1996 but not transmitted to Congress. (33 miles)</p>
<p><em><strong>XX. Public Law 101-628 (November 28, 1990) &#8212; 1 river, study due September 30, 1994</strong></em></p>
<p style="margin-left:.5in">(110) <strong><a href="documents/studies/suasco-river-conservation-plan.pdf" target="_blank">Concord, Assabet and Sudbury, Massachusetts.</a></strong> (NPS) Twenty-nine miles added to the National System, Public Law 106-20, April 9, 1999. (29 miles)</p>
<p><em><strong>XXI. Public Law 102-50 (May 24, 1991) &#8212; 1 river, study due September 30, 1994</strong></em></p>
<p style="margin-left:.5in">(111) <strong>Niobrara, Nebraska.</strong> (NPS) Six miles added to the National System, Public Law 102-50, May 24, 1996. (6 miles)</p>
<p><em><strong>XXII. Public Law 102-214 (December 11, 1991) &#8212; 1 river, study due December 11, 1994</strong></em></p>
<p style="margin-left:.5in">(112) <strong><a href="documents/studies/lamprey-study.pdf" target="_blank">Lamprey, New Hampshire</a>.</strong> (NPS) Eleven point five miles added to the National System, Public Law 104-333, November 12, 1996. Twelve miles added to the National System, Public Law 106-192, May 5, 2000. (10 miles)</p>
<p><em><strong>XXIII. Public Law 102-215 (December 11, 1991) &#8212; 1 river, study due December 11, 1994</strong></em></p>
<p style="margin-left:.5in">(113) <strong>White Clay Creek, Pennsylvania and Delaware.</strong> (NPS) One hundred ninety miles added to the National System, Public Law 106-357, October 24, 2000. (23+ miles)</p>
<p><em><strong>XXIV. Public Law 102-249 (March 3, 1992) &#8212; 11 rivers, studies due September 30, 1995</strong></em></p>
<p style="margin-left:.5in">(114) <strong>Brule, Michigan and Wisconsin.</strong> (USFS) River determined eligible; suitability study not completed. (33 miles)</p>
<p style="margin-left:.5in">(115) <strong>Carp, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (7.6 miles)</p>
<p style="margin-left:.5in">(116) <strong>Little Manistee, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (42 miles)</p>
<p style="margin-left:.5in">(117) <strong>White, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (75.4 miles)</p>
<p style="margin-left:.5in">(118) <strong>Ontonagon, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (32 miles)</p>
<p style="margin-left:.5in">(119) <strong>Paint, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (70 miles)</p>
<p style="margin-left:.5in">(120) <strong>Presque Isle, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (13 miles)</p>
<p style="margin-left:.5in">(121) <strong>Sturgeon (Ottawa National Forest), Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (36 miles)</p>
<p style="margin-left:.5in">(122) <strong>Sturgeon (Hiawatha National Forest), Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (18.1 miles)</p>
<p style="margin-left:.5in">(123) <strong>Tahquamenon, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (103.5 miles)</p>
<p style="margin-left:.5in">(124) <strong>Whitefish, Michigan.</strong> (USFS) River determined eligible; suitability study not completed. (26 miles)</p>
<p><em><strong>XXV. Public Law 102-271 (April 20, 1992) &#8212; 2 rivers, studies due September 30, 1995</strong></em></p>
<p style="margin-left:.5in">(125) <strong>Clarion, Pennsylvania.</strong> (USFS) Fifty-one point seven miles added to the National System, Public Law 104-333, October 19, 1996. (104 miles)</p>
<p style="margin-left:.5in">(126) <strong>Mill Creek, Pennsylvania.</strong> (USFS) River determined eligible, suitability study not completed. (18 miles)</p>
<p><em><strong>XXVI. Public Law 102-301 (June 19, 1992) &#8212; 5 rivers, studies due September 30, 1995</strong></em></p>
<p style="margin-left:.5in">(127) <strong>Piru Creek, California.</strong> (USFS) Seven point three miles of area below Pyramid Lake added to the National System, Public Law 111-11, March 30, 2009. Two areas of river authorized for study&#8212;source to Pyramid Lake and 300 feet below Pyramid Lake to Lake Piru. Study of area above Pyramid Lake completed in revision of Los Padres National Forest Land and Resource Management Plan. (49 miles)</p>
<p style="margin-left:.5in">(128) <strong>Little Sur, California.</strong> (USFS) Study completed in revision of Los Padres National Forest Land and Resource Management Plan. River determined eligible, but report not transmitted to Congress. (23 miles)</p>
<p style="margin-left:.5in">(129) <strong>Matilija Creek, California.</strong> (USFS) Study completed in revision of Los Padres National Forest Land and Resource Management Plan. River determined ineligible, but report not transmitted to Congress. (16 miles)</p>
<p style="margin-left:.5in">(130) <strong>Lopez Creek, California.</strong> (USFS) Study completed in revision of Los Padres National Forest Land and Resource Management Plan. River determined ineligible, but report not transmitted to Congress. (11 miles)</p>
<p style="margin-left:.5in">(131) <strong>Sespe Creek, California.</strong> (USFS) Study completed in revision of Los Padres National Forest Land and Resource Management Plan. River determined eligible, but report not transmitted to Congress. (10.5 miles)</p>
<p><em><strong>XXVII. Public Law 102-432 (October 23, 1992) &#8212; 1 river, study due September 30, 1995</strong></em></p>
<p style="margin-left:.5in">(132) <strong>North Fork Merced, California.</strong> (BLM) Study has been completed through the Folsom Resource Management Plan. River determined ineligible, but report not transmitted to Congress. (15 miles)</p>
<p><em><strong>XXVIII. Public Law 102-460 (October 23, 1992) &#8212; 1 river, study due October 23, 1993</strong></em></p>
<p style="margin-left:.5in">(133) <strong>Delaware, Pennsylvania and New Jersey.</strong> (NPS) Sixty-seven point three miles added to the National System, Public Law 106-418, November 1, 2000. (70 miles)</p>
<p><em><strong>XXIX. Public Law 102-525 (October 26, 1992) &#8212; 1 river, study due October 26, 1993</strong></em></p>
<p style="margin-left:.5in">(134) <strong><a href="documents/studies/new-study.pdf" target="_blank">New, Virginia and West Virginia</a>.</strong> (NPS) Report transmitted to Congress on April 8, 2011. Designation not recommended. (20 miles)<br /><a href="documents/studies/new-study-memos.pdf" target="_blank">Transmittal Memos</a></p>
<p><em><strong>XXX. Public Law 103-242 (May 4, 1994) &#8212; 1 river, study due May 4, 1997</strong></em></p>
<p style="margin-left:.5in">(135) <strong>Rio Grande, New Mexico.</strong> (BLM) Final report issued on January 4, 2000, but not transmitted to Congress. Seven point six miles determined eligible. (8 miles)</p>
<p><em><strong>XXXI. Public Law 104-311 (October 19, 1996) &#8212; 1 river, study due October 19, 1998</strong></em></p>
<p style="margin-left:.5in">(136) <strong>Wekiva, Florida.</strong> (NPS) Forty-one point six miles added to the National System, Public Law 106-299, October 13, 2000. (27 miles)</p>
<p><em><strong>XXXII. Public Law 106-318 (October 19, 2000) &#8212; 1 river, study due October 19, 2003</strong></em></p>
<p style="margin-left:.5in">(137) <strong>Taunton, Massachusetts.</strong> (NPS) Forty point zero miles added to the National System, Public Law 111-11, March 30, 2009. (22 miles)</p>
<p><em><strong>XXXIII. Public Law 107-65 (November 6, 2001) &#8212; 1 river, study due November 6, 2004</strong></em></p>
<p style="margin-left:.5in">(138) <strong>Eight Mile, Connecticut.</strong> (NPS) Twenty-five point three miles added to the National System, Public Law 110-229, May 8, 2008. (15 miles)</p>
<p><em><strong>XXXIV. Public Law 109-370 (November 27, 2006) &#8212; 1 river, study due November 27, 2009</strong></em></p>
<p style="margin-left:.5in">(139) <strong>Lower Farmington and Salmon Brook, Connecticut.</strong> (NPS) Study initiated in 2007. (70 miles)</p>
<p><em><strong>XXXV. Public Law 111-11 (March 3, 2009) &#8212; 1 river, study due March 30, 2012</strong></em></p>
<p style="margin-left:.5in">(140) <strong>Missisquoi and Trout, Vermont.</strong> (NPS) Forty-six point one miles added to the National System, Public Law 113-291, December 19, 2014. (70 miles)</p>
<p><em><strong>XXXVI. Public Law 113-291 (December 19, 2014) &#8212; 4 rivers, studies due 3 years after the date on which funds are made available to conduct the studies</strong></em></p>
<p style="margin-left:.5in">(141) <strong>Cave Creek, Lake Creek, No Name Creek, Panther Creek, and Upper Cave Creek, Oregon.</strong> (NPS) (8.3 miles)</p>
<p style="margin-left:.5in">(142) <strong>Beaver, Chipuxet, Queen, Wood and Pawcatuck Rivers, Rhode Island and Connecticut.</strong> (NPS) (86 miles)</p>
<p style="margin-left:.5in">(143) <strong>Nashua River, Massachusetts.</strong> (NPS) (32.5 miles)</p>
<p style="margin-left:.5in">(144) <strong>York River, Maine.</strong> (NPS) (11.3 miles)</p>
<p>For each study river, the number in parentheses is the approximate number of miles to be studied. If river segments were designated, the total designated mileage appears in the text.</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

</div>

<!--END #accords -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>