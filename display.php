<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Wild &amp; Scenic Rivers System Display';

// Set the page keywords
$page_keywords = 'National Wild &amp; Scenic Rivers System display.';

// Set the page description
$page_description = 'Wild &amp; Scenic Rivers System Display';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<!--<div id="intro-box">
<h2>Please note:</h2>
<p>Most of these publications must be purchased or borrowed from a library.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><div style="margin-top:75px"><img src="images/display.jpg" alt="WSR Display" width="565px" height="356px" /></div></center>

<div id="lower-content">

<div id="lc-left">
<h2>Wild &amp; Scenic Rivers Display</h2>

<p>The Wild &amp; Scenic Rivers Council has created a portable display unit on the National Wild and Scenic Rivers System, which is available for use at public river-related events.</p>

<p>The display is free-standing, 8' (height) x 10' (length). Please contact Dan Haas at <a href="mailto:rivers@fws.gov">rivers@fws.gov</a> or any Council member for reservations and additional detail.</p>

<p>If the display is already scheduled for the period you're looking at, we have a 6' tall banner version available as well.</p>

</div>
<!--END #lc-left -->

<div id="block-quote">
<h4>If we'd known ten years ago today would be ten years from now, would we spend tomorrow's yesterdays and make it last somehow? &#8211; Clint Black, No Time To Kill</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>