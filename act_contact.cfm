<!--- Form data from contact.cfm --->	
<cftry>	
	<cf_recaptcha
				privateKey="#Request.PrivateKey#"
				publicKey="#Request.PublicKey#" 
				action="check"
				SSL="YES">
 <cfif not FORM.Recaptcha>
	<cfoutput><script>
		alert("Incorrect Validation Text");
		window.history.back();
	</script>
	</cfoutput>
	<cfabort>
</cfif>
	  	<cfscript>
			if(Comments_Email NEQ ''){
		  	respond = "Yes"; 
			}
			else{
			respond = "No";
			}
		</cfscript>
		
		
		    <cfscript>
				if(Comments_Email EQ ''){
		  		Comments_Email = "Comments.html@fws.gov"; 
				}
				if (form.Comments EQ ''){
					Comments = "No comments were entered";
				}
			</cfscript>
	 
	<!--- Insert to DB --->
	 <cfquery name="insert_comments" datasource="#ds#">
		insert into comments
								(
								comments,
								comments_email,
								addtolist,
								first_name,
								last_name,
								organization,
								address_1,
								address_2,
								city,
								state,
								zip_code,
								telephone,
								email,
								comment_date,
								org_form
								)
								values
								(
								'#comments#',
								'#comments_email#',
								'#addtolist#',
								'#first_name#',
								'#last_name#',
								'#organization#',
								'#address_1#',
								'#address_2#',
								'#city#',
								'#state#',
								'#zip_code#',
								'#telephone#',
								'#email#',
								#createODBCDate(now())#,
								'#org_form#'
								)
				</cfquery>  
	
	
	
		<!--- Mail  notification, change server="pop.fws.gov" to correct mail server POP, remove if set in CF Administrator --->
	 <!--- server="pop.fws.gov" ---> 
 
 <cfmail to="#notify_email#" from="#from_Email#" subject="Wild and Scenic River Question" type="HTML">  
			<table align="center" width="450" cellpadding="4" class="0" border="0">
				<tr>
					<td colspan="2"><strong>A comment has been added</strong>
					</td>
				</tr>
			
				<tr>
					<td><strong>By:</strong>
					</td>
					<td>	#comments_email#<br />#email#
					</td>
				</tr>
				<tr>
					<td><strong>Date:</strong>
					</td>
					<td>	#Dateformat(now(),'mm/dd/yyyy')#
					</td>
				</tr>
				<tr>
					<td><strong>Comments:</strong>
					</td>
					<td>	#Comments#
					</td>
				</tr>
				<tr>
					<td><strong>Brochure:</strong>
					</td>
					<td>	#AddToList#
					</td>
				</tr>
				<tr>
					<td valign="top"><strong>Contact Information</strong></td>
					<td>#FORM.First_Name# #FORM.Last_Name#<br />
					    <cfif FORM.Organization neq "">#FORM.Organization#<br /></cfif>
						<cfif FORM.Address_1 neq "">#FORM.Address_1#<br /></cfif>
						<cfif FORM.Address_2 neq "">#FORM.Address_2#<br /></cfif>
						#FORM.City#,  #FORM.State#  #FORM.Zip_Code#<br />
					</td>
				</tr>
				<tr>
					<td><strong>Phone</strong></td>
					<td>#FORM.Telephone#</td>
				</tr>
				<tr>
					<td><strong>Email</strong></td>
					<td>#FORM.Email#</td>
				</tr>
			</table>
		</cfmail>
		
		   <cflocation url="../contact-thanks.php">   
		   
 <!-----
   Template: act_contact.cfm
   
   Author: James Hopkins, jim@fivek.com
   
	Purpose: Processes form data from contact.cfm
   
   Use: Error trap for contact info if requesting mailing list, and isert comments into DB
   
   Date Created: 02/21/06
   
----->
<cfcatch><cfdump var="#CFCATCH#"></cfcatch>
</cftry>