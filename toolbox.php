<?php

// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Toolbox';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'National Wild and Scenic Rivers managing agencies.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
$sidebar_bool = false;

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
$body_class = 'fullwidth';
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>
<br />
<br />
<br />
<br />
<br />

<div class="row">
<div class="col-66 column">
<!-- Left hand column text. <p> tags denote new paragraphs. <p> opens the pragraph, </p> closes it. If you want single spacing, use <br /> between lines; that just denotes a line break, not a new paragraph. -->
<!-- Remove the heading and the <h3> </h3> tags if no heading is needed. The page title is set above under $page_title = 'Toolbox';. -->
<h3>Heading</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt tortor ut velit suscipit pellentesque. Integer nisi magna, egestas vulputate tincidunt a, ultrices placerat urna. Donec ullamcorper ut felis ut dapibus. Duis rutrum quam vel velit blandit, eu porta lacus facilisis. Mauris ac mauris lectus.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt tortor ut velit suscipit pellentesque. Integer nisi magna, egestas vulputate tincidunt a, ultrices placerat urna. Donec ullamcorper ut felis ut dapibus. Duis rutrum quam vel velit blandit, eu porta lacus facilisis. Mauris ac mauris lectus.</p>
</div>

<div class="col-33 column">
<!-- Right hand column text. -->
<!-- Remove the heading and the h3 tags if no heading is needed. -->
<h3>Heading</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt tortor ut velit suscipit pellentesque. Integer nisi magna, egestas vulputate tincidunt a, ultrices placerat urna. Donec ullamcorper ut felis ut dapibus. Duis rutrum quam vel velit blandit, eu porta lacus facilisis. Mauris ac mauris lectus.</p>
</div>

</div>

<?php
// includes the content page bottom
include ("includes/content-footNEW.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>