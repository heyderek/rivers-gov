<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'New Jersey';
// Set the page keywords
$page_keywords = 'New Jersey';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - New Jersey.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'NJ';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>New Jersey has approximately 6,450 miles of river, of which 262.9 miles are designated as wild &amp; scenic&#8212;more than 4% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/delaware-lower.php" title="Delaware River (Lower)">Delaware River (Lower)</a></li>
<li><a href="rivers/delaware-middle.php" title="Delaware River (Middle)">Delaware River (Middle)</a></li>
<li><a href="rivers/great-egg-harbor.php" title="Great Egg Harbor River">Great Egg Harbor River</a></li>
<li><a href="rivers/maurice.php" title="Maurice River">Maurice River</a></li>
<li><a href="rivers/musconetcong.php" title="Musconetcong River">Musconetcong River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>