<?php

// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Managing Agencies';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'National Wild and Scenic Rivers managing agencies.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Wild &amp; Scenic Rivers Managing Agencies</h2>
<p>There are four primary federal agencies charged with protecting and managing our wild and scenic rivers and our nation's cultural, recreational and natural resources.</p>
<p>
<ul style="list-style-type:disc; color:#1570b4; font-size:12px;">
<li><a style="font-size:12px; color:#1570b4; font-style:italic;" href="http://www.blm.gov" target="_blank">Bureau of Land Management</a></li>
<li><a style="font-size:12px; color:#1570b4; font-style:italic;" href="http://www.nps.gov" target="_blank">National Park Service</a></li>
<li><a style="font-size:12px; color:#1570b4; font-style:italic;" href="http://www.fws.gov" target="_blank">U.S. Fish and Wildlife Service</a></li>
<li><a style="font-size:12px; color:#1570b4; font-style:italic;" href="http://www.fs.fed.us" target="_blank">U.S. Forest Service</a></li>
</ul>
</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/zigzag.jpg" alt="Zigzag River, Oregon" width="565" height="212" title="Zigzag River, Oregon" /></center><figcaption style="font-size:12px; font-style:italic; color:#1B4C8C; margin: 0px 0px 0px 0px">Zigzag River, Oregon; Photo by Tim Palmer</figcaption>

<div id="lower-content">

<div id="lc-left">
<table width="100%">
<tr>
<td width="10%" colspan="4">
<h2>River Program Websites, By State</h2>
<p>The three largest state river protection programs are in Maine, California and New York (Cordell 1999, <em>Outdoor Recreation in American Life</em>, citing Tim Palmer 1993 <em>Wild and Scenic Rivers of America</em>). However, these, as well as most other states, do not have web sites devoted to their state river programs. (It should also be noted that New York does not have any federally designated state-managed rivers in the National Wild and Scenic Rivers System.)</p></td>
</tr>
<tr>
<td width="10%">&nbsp;</td>
<td width="35%">
<ul style="list-style-type:disc;">
<li style="line-height: 25px">Arkansas</li>
<li style="line-height: 25px">California</li>
<li><a href="http://www.in.gov/dnr/outdoor/5355.htm" title="Indiana Scenic Rivers Program" target="_blank">Indiana</a></li>
<li style="line-height: 25px">Kentucky</li>
<li style="line-height: 25px">Louisiana</li>
<li style="line-height: 25px">Massachusetts</li>
<li><a href="http://www.michigan.gov/dnr/0,4570,7-153-10364_52259_31442-95823--,00.html" title="Michigan Natural Rivers Program" target="_blank">Michigan</a></li>
<li style="line-height: 25px">Minnesota</li>
<li style="line-height: 25px">Mississippi</li>
</ul>
</td>
<td width="20%">&nbsp;</td>
<td width="35%">
<ul style="list-style-type:disc;">
<li style="line-height: 25px">New Hampshire</li>
<li style="line-height: 25px">New York</li>
<li style="line-height: 25px">North Carolina</li>
<li style="line-height: 25px">Ohio</li>
<li style="line-height: 25px">Oklahoma</li>
<li><a href="http://www.oregon.gov/oprd/NATRES/scenicwaterways/Pages/index.aspx" title="Oregon Scenic Rivers Program" target="_blank">Oregon</a></li>
<li><a href="http://www.dcnr.state.pa.us/brc/conservation/rivers/scenicrivers/index.htm" title="Pennsylvania Scenic Rivers Program" target="_blank">Pennsylvania</a></li>
<li><a href="http://www.dnr.sc.gov/water/river/index.html" title="South Carolina Scenic Rivers Program" target="_blank">South Carolina</a></li>
<li><a href="http://www.tennessee.gov/environment/topic/na-sr-scenic-rivers" title="Tennessee Scenic Rivers Program" target="_blank">Tennessee</a></li>
</ul>
</td>
</tr>
</table>
</div>
<!--END #lc-left -->

<div id="block-quote">
<h4>If you know of more state river program sites, please send the address to<br />us at: <a href="mailto:rivers@fws.gov">rivers@fws.gov</a></h4>
</div>
<!--END #block-quote -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div><!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>
