<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Michigan';
// Set the page keywords
$page_keywords = 'Michigan';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Michigan.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'MI';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Michigan has approximately 51,438 miles of river, of which 656.4 miles are designated as wild &amp; scenic&#8212;just a bit more than 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/ausable.php" title="AuSable River">AuSable River</a></li>
<li><a href="rivers/bear.php" title="Bear Creek">Bear Creek</a></li>
<li><a href="rivers/black-mi.php" title="Black River">Black River</a></li>
<li><a href="rivers/carp.php" title="Carp River">Carp River</a></li>
<li><a href="rivers/indian.php" title="Indian River">Indian River</a></li>
<li><a href="rivers/manistee.php" title="Manistee River">Manistee River</a></li>
<li><a href="rivers/ontonagon.php" title="Ontonagon River">Ontonagon River</a></li>
<li><a href="rivers/paint.php" title="Paint River">Paint River</a></li>
<li><a href="rivers/pere-marquette.php" title="Pere Marquette River">Pere Marquette River</a></li>
<li><a href="rivers/pine.php" title="Pine River">Pine River</a></li>
<li><a href="rivers/presque-isle.php" title="Presque Isle River">Presque Isle River</a></li>
<li><a href="rivers/sturgeon1.php" title="Sturgeon River (Hiawatha National Forest)">Sturgeon River (Hiawatha National Forest)</a></li>
<li><a href="rivers/sturgeon2.php" title="Sturgeon River (Ottawa National Forest)">Sturgeon River (Ottawa National Forest)</a></li>
<li><a href="rivers/tahquamenon.php" title="Tahquamenon River (East Branch)">Tahquamenon River (East Branch)</a></li>
<li><a href="rivers/whitefish.php" title="Whitefish River)">Whitefish River</a></li>
<li><a href="rivers/yellow-dog.php" title="Yellow Dog River">Yellow Dog River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>