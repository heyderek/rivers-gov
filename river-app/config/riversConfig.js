
define({
  // When true, the template will query arcgis.com for the webmap item.
  "river": {
  	"featureService": "http://apps.fs.usda.gov/arcx/rest/services/EDW/EDW_WildScenicRiverSegments_01/MapServer/0",
    "featureServiceQueryColumn": "RIVER_ID"
  }
});
