﻿define([
    "dojo/ready",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/_base/Color",
    "dojox/jsonPath",
    "esri/arcgis/utils",
    "esri/urlUtils",
    "esri/geometry/geometryEngine",
    "esri/graphic",
    "esri/geometry/ScreenPoint",
    "dojo/on",
    "dojo/has",
    "dojo/sniff",
    "dojo/aspect",
    "dijit/registry",
    "application/MapUrlParams",
    "esri/dijit/Search",
    "application/SearchSources",
    "esri/tasks/locator",
    "esri/lang",
    "esri/dijit/Legend",
    "esri/tasks/QueryTask",
    "esri/geometry/screenUtils",
    "esri/geometry/Extent",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom",
    "dojo/query",
    "dojo/fx/Toggler",
    "dojo/dom-construct",
    "esri/dijit/LocateButton",
    "esri/dijit/HomeButton",
    "esri/layers/FeatureLayer",
    "esri/dijit/InfoWindow"
], function (
    ready,
    declare, lang,
    array, Color, jsonPath,
    arcgisUtils, urlUtils, geometryEngine, Graphic, ScreenPoint,
    on,
    has, sniff, aspect,
    registry,
    MapUrlParams,
    Search, SearchSources,
    Locator,
    esriLang,
    Legend, QueryTask, ScreenUtils, Extent,
    domClass, domStyle,
    dom,
    query, Toggler,
    domConstruct,
    LocateButton, HomeButton,
    FeatureLayer, InfoWindow
    ) {
    return declare("", null, {
        config: {},
        riversFeatureLayer: null,
        theme: null,
        color: null,
        togglerLegend: null,
        paneltheme: null,
        startup: function (config) {
            // config will contain application and user defined info for the template such as i18n strings, the web map id
            // and application id
            // any url parameters and any application specific configuration information.
            this.config = config;
            // responsive drawer
            var rtl = (query(".esriRTL").length > 0) ? "rtl" : "ltr";

            // document ready
            ready(lang.hitch(this, function () {

                this.theme = this.setColor(this.config.theme);
                this.color = this.setColor(this.config.color);
                this.paneltheme = this.setColor(this.config.paneltheme);
                //supply either the webmap id or, if available, the item info
                var itemInfo = this.config.itemInfo || this.config.webmap;
                this.itemInfo = itemInfo;
                var mapParams = new MapUrlParams({
                    center: this.config.center || null,
                    extent: this.config.extent || null,
                    level: this.config.level || null,
                    marker: this.config.marker || null,
                    mapSpatialReference: itemInfo.itemData.spatialReference,
                    defaultMarkerSymbol: this.config.markerSymbol,
                    defaultMarkerSymbolWidth: this.config.markerSymbolWidth,
                    defaultMarkerSymbolHeight: this.config.markerSymbolHeight,
                    geometryService: this.config.helperServices.geometry.url
                });
                mapParams.processUrlParams().then(lang.hitch(this, function (urlParams) {
                    promise = this._createWebMap(itemInfo, urlParams);
                }), lang.hitch(this, function (error) {
                    console.log(error);
                }));
                this.togglerLegend = new Toggler({
                    node: "legendDiv"
                });
                this.togglerLegend.hide();

                domStyle.set("legend-cont", { "visiblity": "hidden" });
                on(dom.byId("legendDivOpen"), "click", lang.hitch(this, function (e) {
                    this.togglerLegend.show();
                    domStyle.set("legendDiv", { display: "block" });
                    domStyle.set("legendDivClose", { display: "block", "visiblity": "visible" });
                    domStyle.set("legendDivOpen", { display: "none", "visiblity": "hidden" });
                }));
                on(dom.byId("legendDivClose"), "click", lang.hitch(this, function (e) {
                    this.togglerLegend.hide();
                    domStyle.set("legendDiv", { display: "none" });
                    domStyle.set("legendDivClose", { display: "none", "visiblity": "hidden" });
                    domStyle.set("legendDivOpen", { display: "block", "visiblity": "visible" });
                }));
                domStyle.set("legendDiv", { display: "none" });
                domStyle.set("legendDivClose", { display: "none", "style": "hidden" });
                domStyle.set("legendDivOpen", { display: "block", "style": "visible" });

            }));
        },
        _mapLoaded: function () {
            domStyle.set("legend-cont", { "visiblity": "visible" });
            var queryParamValue = dojo.queryToObject(document.location.search)[this.config.queryUrlParam] ||
                dojo.queryToObject(document.location.search)["?" + this.config.queryUrlParam];
            this._highlightRivers(this.itemInfo, queryParamValue, this.map);
            // remove loading class
            domClass.remove(document.body, "app-loading");


            //Add the geocoder if search is enabled
            if (this.config.search) {
                var searchOptions = {
                    map: this.map,
                    useMapExtent: this.config.searchExtent,
                    itemData: this.config.response.itemInfo.itemData
                };
                if (this.config.searchConfig) {
                    searchOptions.applicationConfiguredSources = this.config.searchConfig.sources || [];
                } else if (this.config.searchLayers) {
                    var configuredSearchLayers = (this.config.searchLayers instanceof Array) ? this.config.searchLayers : JSON.parse(this.config.searchLayers);
                    searchOptions.configuredSearchLayers = configuredSearchLayers;
                    searchOptions.geocoders = this.config.locationSearch ? this.config.helperServices.geocode : [];
                }
                var searchSources = new SearchSources(searchOptions);
                var createdOptions = searchSources.createOptions();

                if (this.config.searchConfig && this.config.searchConfig.activeSourceIndex) {
                    createdOptions.activeSourceIndex = this.config.searchConfig.activeSourceIndex;
                }

                var search = new Search(createdOptions, domConstruct.create("div"));

                search.startup();

                if (search && search.domNode) {
                    domConstruct.place(search.domNode, "search");
                }

            }
            //Feature Search or find (if no search widget)
            if ((this.config.find || (this.config.customUrlLayer.id !== null && this.config.customUrlLayer.fields.length > 0 && this.config.customUrlParam !== null))) {
                require(["esri/dijit/Search"], lang.hitch(this, function (Search) {
                    var source = null,
                        value = null,
                        searchLayer = null;

                    var urlObject = urlUtils.urlToObject(document.location.href);
                    urlObject.query = urlObject.query || {};
                    urlObject.query = esriLang.stripTags(urlObject.query);
                    var customUrl = null;
                    for (var prop in urlObject.query) {
                        if (urlObject.query.hasOwnProperty(prop)) {
                            if (prop.toUpperCase() === this.config.customUrlParam.toUpperCase()) {
                                customUrl = prop;
                            }
                        }
                    }

                    //Support find or custom url param 
                    if (this.config.find) {
                        value = decodeURIComponent(this.config.find);
                    } else if (customUrl) {

                        value = urlObject.query[customUrl];
                        searchLayer = this.map.getLayer(this.config.customUrlLayer.id);
                        if (searchLayer) {

                            var searchFields = this.config.customUrlLayer.fields[0].fields;
                            source = {
                                exactMatch: true,
                                outFields: ["*"],
                                featureLayer: searchLayer,
                                displayField: searchFields[0],
                                searchFields: searchFields
                            };
                        }
                    }
                    var urlSearch = new Search({
                        map: this.map
                    });

                    if (source) {
                        urlSearch.set("sources", [source]);
                    }
                    urlSearch.on("load", lang.hitch(this, function () {
                        urlSearch.search(value).then(lang.hitch(this, function () {
                            on.once(this.map.infoWindow, "hide", lang.hitch(this, function () {
                                urlSearch.clear();
                                urlSearch.destroy();
                            }));
                        }));
                    }));
                    urlSearch.startup();

                }));

            }

            //Add the location button if enabled
            if (this.config.locate) {
                var location = new LocateButton({
                    map: this.map
                }, domConstruct.create("div", {
                    id: "locateDiv"
                }, "mapDiv"));
                location.startup();
            } else {
                domClass.add(document.body, "nolocate");
            }
            //Add the home button if configured
            if (this.config.home) {
                var homeButton = new HomeButton({
                    map: this.map
                }, domConstruct.create("div", {
                    id: "homeDiv"
                }, "mapDiv"));
                homeButton.startup();
            }


            //Define legend panel content
            var noLegend = null, noAbout = null;
            var layerInfo = arcgisUtils.getLegendLayers(this.config.response);
            if (layerInfo && layerInfo.length && layerInfo.length > 0) {
                //dom.byId("legend-label").innerHTML = this.config.i18n.tools.legend;
                var legend_div = domConstruct.create("div", {
                    className: "panel_content"
                }, dom.byId("legendDiv"));

                var legend = new Legend({
                    map: this.map,
                    layerInfos: layerInfo
                }, legend_div);
                legend.startup();
            } else {
                domClass.add(dom.byId("legend-cont"), "node-hidden");
                noLegend = true;
            }/*



            //Define about panel content

            var about_content = this.config.about || this.config.itemInfo.item.description;
            if (about_content !== null && about_content !== "") {
                dom.byId("about-label").innerHTML = this.config.i18n.tools.about;
                domConstruct.create("div", {
                    innerHTML: about_content,
                    className: "panel_content"
                }, dom.byId("aboutDiv"));
            } else {
                domClass.add(dom.byId("about-cont"), "node-hidden");
                noAbout= true;
            }
            //hide arrows if legend or about if both options aren't shown
            if(noAbout ||  noLegend){
                query(".ac-container").forEach(function(node){
                    domClass.add(node, "no-label");
                });
            }
            if(noAbout && noLegend){ //hide drawer
                domStyle.set(dom.byId("toggle_button"),"display","none");
                domStyle.set(dom.byId("cp_left"),"display", "none");
                query(".top-bar-title").style("margin-left", "5px");
                registry.byId("border_container").layout();
            }
            this._updateTheme();*/

        },

        repositionPopup: function() {
            
            var divEsriPopupWrapper = query(".esriPopupWrapper")[0];
            var divEsriPopupBottom = query(".esriPopup")[0];
            var styleEsriPopupWrapper = domStyle.getComputedStyle(divEsriPopupWrapper);
            var styleEsriPopupBottom = domStyle.getComputedStyle(divEsriPopupBottom);
            var styleEsriPopupBottom_top = parseInt(styleEsriPopupBottom.top);
            var styleEsriPopupBottom_left = parseInt(styleEsriPopupBottom.left);
            var styleEsriPopupWrapper_height = parseInt(styleEsriPopupWrapper.height) + 20;
            var displacedHeightPixels = styleEsriPopupBottom_top - styleEsriPopupWrapper_height;
            if (displacedHeightPixels < 0) {
                
                var screenPointPixels = new ScreenPoint(styleEsriPopupBottom_left, displacedHeightPixels);
                var displaceToPoint = ScreenUtils.toMapGeometry(this.map.extent, this.map.width, this.map.height, screenPointPixels);
                var ymax = 0;
                var ymin = 0;
                var lowerShift = 0;
                if (this.map.getInfoWindowAnchor().indexOf("upper") >= 0) {
                    ymax = displaceToPoint.y;
                    ymin = this.map.extent.ymin + (displaceToPoint.y - this.map.extent.ymax);
                } else {
                    ymin = displaceToPoint.y;
                    ymax = this.map.extent.ymax + (displaceToPoint.y - this.map.extent.ymin);
                   
                }
                var extent = new Extent(this.map.extent.xmin, ymin, this.map.extent.xmax, ymax, this.map.spatialReference);
                this.map.setExtent(extent);
            }

            var mapComputedStyle = domStyle.getComputedStyle(map.root);
        },
        //create a map based on the input web map id
        _createWebMap: function (itemInfo, params) {

            arcgisUtils.createMap(itemInfo, "mapDiv", {
                mapOptions: params.mapOptions,
                editable: false,
                usePopupManager: true,
                layerMixins: this.config.layerMixins || [],
                bingMapsKey: this.config.bingmapskey
            }).then(lang.hitch(this, function (response) {

                //define the application title
                /*var title = this.config.title || response.itemInfo.item.title;
                dom.byId("title").innerHTML = title;
                document.title = title;*/

                
                this.map = response.map;
                window.map = this.map;
               
                on(this.map.infoWindow, 'selection-change', lang.hitch(this, function (location) {
                    
                    //this.repositionPopup();
                }));
                aspect.before(this.map.infoWindow, "show", lang.hitch(this, function (args0, args1, args2) {

                    var centerx = map.extent.xmin +  Math.abs((map.extent.xmin - map.extent.xmax) / 2);
                    var centery = map.extent.ymin + Math.abs((map.extent.ymin - map.extent.ymax) / 2);
                    if (args0.x > centerx) {
                        this.map.infoWindow.set("anchor", "top-left");
                    } else {
                        this.map.infoWindow.set("anchor", "top-right");
                    }
                    //this.map.infoWindow.setFixedAnchor(InfoWindow.ANCHOR_UPPERLEFT);
                    //this.repositionPopup();
                }));
                aspect.after(this.map.infoWindow, "_setPosition", lang.hitch(this, function (deferred) {
                    
                    this.repositionPopup();
                }));
                //selection - change
                domClass.add(this.map.infoWindow.domNode, "light");
                if (params.markerGraphic) {
                    // Add a marker graphic with an optional info window if
                    // one was specified via the marker url parameter
                    require(["esri/layers/GraphicsLayer"], lang.hitch(this, function (GraphicsLayer) {
                        var markerLayer = new GraphicsLayer();

                        this.map.addLayer(markerLayer);
                        markerLayer.add(params.markerGraphic);

                        if (params.markerGraphic.infoTemplate) {
                            this.map.infoWindow.setFeatures([params.markerGraphic]);
                            this.map.infoWindow.show(params.markerGraphic.geometry);
                        }
                    }));

                }
                this.config.response = response;

                // make sure map is loaded
                if (this.map.loaded) {
                    // do something with the map
                    this._mapLoaded();
                } else {
                    on.once(this.map, "load", lang.hitch(this, function () {
                        // do something with the map
                        this._mapLoaded();
                    }));
                }
            }), lang.hitch(this, function (error) {
                //an error occurred - notify the user. In this example we pull the string from the
                //resource.js file located in the nls folder because we've set the application up
                //for localization. If you don't need to support multiple languages you can hardcode the
                //strings here and comment out the call in index.html to get the localization strings.
                if (this.config && this.config.i18n) {
                    alert(this.config.i18n.map.error + ": " + error.message);
                } else {
                    alert("Unable to create map: " + error.message);
                }
            }));
        },
        setColor: function (value) {
            var colorValue = null;
            var rgb = Color.fromHex(value).toRgb();

            if (has("ie") == 8) {
                colorValue = value;
            } else {
                rgb.push(0.9);
                colorValue = Color.fromArray(rgb);
            }
            return colorValue;

        },


        _updateTheme: function () {
            //Apply the configured theme to the template
            //Add the bg class to any elements that you want to display using the specified background color
            //Apply the fc class to elements that should display using the specified font color
            query(".bg").style("backgroundColor", this.theme.toString());
            query(".bg").style("color", this.color.toString());
            query(".fc").style("color", this.color.toString());
            query(".ac-container label:after").style("color", this.color.toString());


            //Style the popup title bar to use the theme color.
            query(".esriPopup .pointer").style("backgroundColor", this.theme.toString());
            query(".esriPopup .titlePane").style("backgroundColor", this.theme.toString());


            query(".esriPopup .titlePane").style("color", this.color.toString());
            query(".esriPopup. .titleButton").style("color", this.color.toString());



            //Query for the title areas in the drawer and  apply the panel theme.
            query(".ab").style("backgroundColor", this.paneltheme.toString());

            //this._drawer.resize();
            registry.byId("border_container").resize();
        },
        _setLevel: function (options) {
            var level = this.config.level;
            //specify center and zoom if provided as url params 
            if (level) {
                options.zoom = level;
            }
            return options;
        },

        _setCenter: function (options) {
            var center = this.config.center;
            if (center) {
                var points = center.split(",");
                if (points && points.length === 2) {
                    options.center = [parseFloat(points[0]), parseFloat(points[1])];
                }
            }
            return options;
        },

        _setExtent: function (info) {
            var e = this.config.extent;
            //If a custom extent is set as a url parameter handle that before creating the map
            if (e) {
                var extArray = e.split(",");
                var extLength = extArray.length;
                if (extLength === 4) {
                    info.item.extent = [[parseFloat(extArray[0]), parseFloat(extArray[1])], [parseFloat(extArray[2]), parseFloat(extArray[3])]];
                }
            }
            return info;
        },

        _highlightRivers: function (itemInfo, paramItem, map) {
            if (typeof (paramItem) == 'undefined' || paramItem == null) {
                console.log("paramItem is undefined");
            }
            var arrDefinitionQueries = this._configRiverQueries(itemInfo);
            if (arrDefinitionQueries.length < 1) {
                return;
            }
            var layerDefExpressions = [];
            var arrDefinitionQuery = arrDefinitionQueries[0];

            var query = new esri.tasks.Query();
            var params = paramItem.split(",");
            var tmp = "";
            for (var q = 0; q < params.length; q++) {
                if (params[q] == "") {
                    continue;
                }
                if (q > 0) {
                    tmp += " OR ";
                }
                tmp += arrDefinitionQuery.definitionEditorQuery.replace('{0}', params[q]);
            }
            layerDefExpressions[arrDefinitionQuery.id] = tmp;
            tmp = tmp.replace("<>", "=");
            query.where = tmp.replace("N'", "'");
            query.outSpatialReference = map.spatialReference;
            query.returnGeometry = true;
            var queryTask = new QueryTask(arrDefinitionQuery.url);
            //var riversFeatureLayer = new FeatureLayer(arrDefinitionQuery.url, esri.layers.FeatureLayer.MODE_SELECTION);
            //riversFeatureLayer.selectFeatures(query,esri.layers.FeatureLayer.SELECTION_NEW, 
            queryTask.execute(query,
                lang.hitch(this, function (map, arrDefinitionQuery, featureSet, count) {
                    var extent = null;
                    var bLayer = map.getLayer(map.layerIds[0]);
                    var geoms = [];
                    var extents = [];
                    for (var i = 0; i < featureSet.features.length; i++) {
                        var geometry = featureSet.features[i].geometry;
                        var tmpExtent = geometry.getExtent();
                        extents[extents.length] = geometry.getExtent();
                        geoms[geoms.length] = geometry;
                        if (extent == null) {
                            extent = tmpExtent;
                        } else {
                            extent = extent.union(tmpExtent);
                        }
                    }

                    if (extent != null) {
                        var expansionFactor = 1.4;
                        if (typeof (this.config.expansionFactor) == 'number') {
                            expansionFactor = this.config.expansionFactor;
                        }
                        map.setExtent(extent.expand(expansionFactor), true);
                    }
                    if (this.config.queryUrlParam == "state") {
                        var geometry = geometryEngine.union(geoms);
                        var diffGeom = geometryEngine.difference([bLayer.fullExtent], geometry);
                        var symbol = arrDefinitionQuery.layerObject.renderer.getSymbol();
                        symbol.color.a = arrDefinitionQuery.layerObject.opacity;
                        arrDefinitionQuery.layerObject.hide();
                        var holeGeomGraphic = new Graphic(diffGeom[0], symbol);
                        map.graphics.add(holeGeomGraphic);

                        require(["esri/layers/GraphicsLayer"], lang.hitch(this,
                            function (holeGeomGraphic, layerObject, GraphicsLayer) {
                                var markerLayer = new GraphicsLayer();
                                markerLayer.setOpacity(layerObject.opacity);
                                this.map.addLayer(markerLayer);
                                markerLayer.add(holeGeomGraphic);
                            }, holeGeomGraphic, arrDefinitionQuery.layerObject));
                    }

                }, map, arrDefinitionQuery),
                function (err) {
                    console.log(err);
                }
            );
            if (typeof (arrDefinitionQuery.layerObject.setLayerDefinitions) != 'undefined') {
                arrDefinitionQuery.layerObject.setLayerDefinitions(layerDefExpressions);
            } else {
                arrDefinitionQuery.layerObject.setDefinitionExpression(layerDefExpressions[arrDefinitionQuery.id]);
            }


        },

        _configRiverQueries: function (itemInfo) {
            var arrDefinitionQueries = [];
            var opLayers = dojox.jsonPath.query(itemInfo, "itemInfo.itemData.operationalLayers");
            if (opLayers.length > 0) {
                opLayers = opLayers[0];
            }
            for (var i = 0; i < opLayers.length; i++) {
                var arrDevQueries = this._configRiversGetDefinationQuery(opLayers[i], opLayers[i].url, opLayers[i].layerObject);
                if (arrDevQueries && arrDevQueries.length) {
                    arrDefinitionQueries = arrDefinitionQueries.concat(arrDevQueries);
                }
            }
            return arrDefinitionQueries;
        },

        _configRiversGetDefinationQuery: function (layer, url, layerObject) {
            if (layer && layer.layers) {
                var arrDefQueries = [];
                for (var i = 0; i < layer.layers.length; i++) {
                    var arr = this._configRiversGetDefinationQuery(layer.layers[i], url + "/" + layer.layers[i].id, layerObject);
                    if (arr) {
                        arrDefQueries.push(arr[0]);
                    }
                }
                return arrDefQueries;
            }
            var pe = dojox.jsonPath.query(layer, "layer.definitionEditor.parameterizedExpression");
            if (pe.length > 0) {
                return [{ url: url, definitionEditorQuery: pe[0], layerObject: layerObject, id: layer.id }];
            }
        }

    });
});
