<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Nevada';
// Set the page keywords
$page_keywords = 'Nevada';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Nevada.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'NV';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Nevada has approximately 141,796 miles of river, but no designated wild &amp; scenic rivers.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li>Nevada does not have any designated rivers.</li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>