<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Transportation Case Studies';

// Set the page keywords
$page_keywords = 'Blah, Blah';

// Set the page description
$page_description = 'Transportation Case Studies';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="trans-box">

	<div id="trans-details-box">

		<div id="trans-details-text">
		<h3>Title</h3>
		<p>National Park Service, Katmai National Park and Preserve<br/>
		Post Office Box 7<br/>
		King Salmon, Alaska 99613</p>
		</div>

	</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="lower-content">
<h2 style="line-height:150%;">Optional Title</h2>
<p>Main text.</p>
</div>
<!--END #lower-content -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>