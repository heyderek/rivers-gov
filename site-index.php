<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Site Index';

// Set the page keywords
$page_keywords = 'wild and scenic rivers, site index, index';

// Set the page description
$page_description = 'Wild and Scenic Rivers site index.';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="lower-content">

<div id="lc-left" style="width:450px; margin-top:35px;">

<p>This Site Index matches the menu system within this web site. It will help the user navigate the site in the event that the menu system does not work on the users device.</p>

<p>&nbsp;</p>

<p style="font-size: 15pt">NATIONAL SYSTEM</p>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>national-system.php" title="What Are Wild &amp; Scenic Rivers?">What Are Wild &amp; Scenic Rivers?</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>map.php" title="Find Designated Rivers">Find Designated Rivers</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>designation.php" title="How Rivers Are Designated">How Rivers Are Designated</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>wsr-act.php" title="About the Wild and Scenic Rivers Act">About The Wild &amp; Scenic Rivers Act</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>documents/rivers-table.pdf" title="WSR Table (Mileage Classifications)" target="_blank">WSR Table (Mileage Classifications)</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>study.php" title="Study Rivers">Study Rivers</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>stewardship.php" title="Stewardship">Stewardship</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>awards/index.php" title="National Awards">National Awards</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>act.php" title="WSR Legislation and Amendments">WSR Legislation &amp; Amendments</a></li>
</ul>
<p style="font-size: 12pt; font-style: italic">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;External Links:<br/>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="http://www.nps.gov/ncrc/programs/rtca/nri/" title="Nationwide Rivers Inventory (NRI)">Nationwide Rivers Inventory (NRI)</a></li></a>
<li style="margin-left:.2in;text-indent:-.2in"><a href="http://www.presidency.ucsb.edu/ws/index.php?pid=29150#axzz1xE2Od0br" title="Signing Remarks" target="_blank">Signing Remarks</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="https://www.rivers.gov/documents/landowners.pdf" title="Riverfront Owners Brochure" target="_blank">Riverfront Owners Brochure</a></li>
</ul>
</p>

<p>&nbsp;</p>

<p style="font-size: 15pt">MANAGEMENT</p>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>council.php" title="Interagency Council">Interagency Council</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>agencies.php" title="Agencies">Managing Agencies</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>management-plans.php" title="Management Plans">Management Plans</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>mapping-gis.php" title="GIS Mapping">Gis Mapping</a></li>
</ul>
<p style="font-size: 12pt; font-style: italic">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;External Links:<br/>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="https://www.blm.gov" title="Bureau of Land Management" target="_blank">Bureau of Land Management</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="https://www.nps.gov" title="National Park Service" target="_blank">National Park Service</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="https://www.nps.gov/orgs/1912/" title="National Park Service Rivers" target="_blank">National Park Service Rivers</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="https://www.fws.gov" title="US Fish And Wildlife Service" target="_blank">US Fish &amp; Wildlife Service</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="http://www.fs.fed.us" title="US Forest Service" target="_blank">US Forest Service</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="http://www.river-management.org" title="River Management Society" target="_blank">River Management Society</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="https://www.nps.gov/ncrc/programs/rtca/" title="Rivers, Trails &amp; Conservation Assistance Program" target="_blank">Rivers, Trails &amp; Conservation Assistance Program</a></li>
</ul>
</p>

<p>&nbsp;</p>

<p style="font-size: 15pt">RESOURCES</p>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>information.php" title="Q and A Search Engine">Q &amp; A Search Engine</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>bibliography.php" title="Bibliography">Bibliography</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>publications.php" title="Publications">Publications</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>mapping-gis.php" title="Gis Mapping">Maps &amp; GIS</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>publications.php" title="">Logo &amp; Sign Standards</a></li>
<!--<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>display.php" title="Display">Display</a></li>-->
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>quotations.php" title="River Quotes">River Quotes</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>waterfacts.php" title="Water Facts">Water Facts</a></li>
<!--<li style="margin-left:.2in;text-indent:-.2in"><a href="#" title="Transportation Projects">Transportation Projects</a></li>-->
</ul>

<p>&nbsp;</p>

<p style="font-size: 15pt">PUBLICATIONS</p>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>publications.php" title="Technical Papers">Technical Papers</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>publications.php" title="Section 7 Determinations">Section 7 Determinations</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>publications.php" title="Non-Council Papers">Non-Council Papers</a></li>
</ul>

<p>&nbsp;</p>

<p style="font-size: 15pt">CONTACT US</p>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>info/contact.cfm" title="Contact Us">Contact Us</a></li>
</ul>

<p>&nbsp;</p>

<p style="font-size: 15pt">50 YEARS</p>
<ul>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>wsr50/index.php" title="50th Anniversary">About The 50 Years</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>toolkit.php" title="Toolkit">Toolkit</a></li>
<li><a href="https://umontana.maps.arcgis.com/apps/MapJournal/index.html?appid=f85c819b95f14ae1a44400e46ee9578c" title="50th Anniversary Event Map" target="_blank">Event Map</a></li>
<li style="margin-left:.2in;text-indent:-.2in"><a href="<?php echo $base_url; ?>videos.php" title="Anniversary Videos">Anniversary Videos</a></li>
</ul>

</div>
<!--END #lc-left -->

<!--<div id="block-quote">
<h4>Please note that most of these publications must be purchased or borrowed from a library.</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>