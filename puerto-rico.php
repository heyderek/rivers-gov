<?php

// Set the page title  -- GENERAL TEMPLATE 3 

$page_title = 'Puerto Rico';


// Set the page keywords

$page_keywords = 'Puerto Rico';


// Set the page description

$page_description = 'National Wild and Scenic Rivers - Puerto Rico.';


// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'tropical';


// Create a postal code ID for checking against.
$state_code = 'PR';

// Includes the meta data that is common to all pages

include( "includes/metascript.php" );

?>


<!-- BEGIN page specific CSS and Scripts -->


<!-- END page specific CSS and Scripts -->


<?php

// includes the TEMPLATE HEADER CODING -- #content-page

include( "includes/header.php" )

?>



<?php

// includes the content page top

include( "includes/content-head.php" )

?>


<div id="intro-box">

	<p>Puerto Rico has approximately 5,385 miles of river, of which 8.9 miles of three rivers are designated as wild
		&amp; scenic&#8212;less than 2/10ths of 1% of the commonwealth's river miles.</p>


</div><!--END #intro-box -->


<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>


<ul>

	<li><a href="rivers/rio-de-la-mina.php" title="Rio de la Mina">Rio de la Mina</a></li>
	<li><a href="rivers/rio-icacos.php" title="Rio Icacos">Rio Icacos</a></li>
	<li><a href="rivers/rio-mameyes.php" title="Rio Mameyes">Rio Mameyes</a></li>


</ul>


<div class="clear"></div><!-- Allows for content above to be flexible -->

<?php

// includes the content page bottom

include( "includes/content-foot.php" )

?>



<?php

// includes the TEMPLATE FOOTER CODING -- </html> 		

include( "includes/footer.php" )

?>

	