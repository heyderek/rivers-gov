<?php

//Set the base URL for absolute path references 

//$base_url = 'https://www.rivers.gov/';
 $base_url = 'http://gov.rivers.www/';
// $base_url = 'http://rivers.focalpointmarketing.com/';
$sidebar_bool = 1;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

	<title><?php echo $page_title; ?></title>


	<meta name="Description" content="<?php echo $page_description; ?>"/>

	<meta name="Keywords" content="<?php echo $page_keywords; ?>"/>

	<meta name="author" content="Focal Point Marketing Web Design - Brad Sappenfield"/>

	<meta name="copyright"
	      content="This site is copyrighted by U.S. Laws. Violation of these rules are strictly prohibited."/>

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>


	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>style.css"/>


	<link rel="shortcut icon" href="<?php echo $base_url; ?>images/favicon.ico"/>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.js"></script>


	<script type="text/javascript" language="javascript" src="<?php echo $base_url; ?>scripts/hoverintent.js"></script>

<!--	<script type="text/javascript" language="javascript"-->
<!--	        src="--><?php //echo $base_url; ?><!--scripts/jquery.dropdown.js"></script>-->


	<script type="text/javascript">

		<!--

		function stateURL() {

			var url = document.stateredirect.states.value

			document.location.href = url


		}


		// -->

	</script>


	<script type="text/javascript">

		<!--

		function riverURL() {

			var url = document.riverredirect.rivers.value

			document.location.href = url


		}


		// -->

	</script>


<!--	<script type="text/javascript">-->
<!---->
<!--		$(document).ready(function () {-->
<!--			if ($.browser.msie && $.browser.version < 9) $('select.wide')-->
<!---->
<!--				.bind('focus mouseover', function () {-->
<!--					$(this).addClass('expand').removeClass('clicked');-->
<!--				})-->
<!---->
<!--				.bind('click', function () {-->
<!--					$(this).toggleClass('clicked');-->
<!--				})-->
<!---->
<!--				.bind('mouseout', function () {-->
<!--					if (!$(this).hasClass('clicked')) {-->
<!--						$(this).removeClass('expand');-->
<!--					}-->
<!--				})-->
<!---->
<!--				.bind('blur', function () {-->
<!--					$(this).removeClass('expand clicked');-->
<!--				});-->
<!---->
<!---->
<!--		});-->
<!---->
<!--	</script>-->



