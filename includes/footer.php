<div id="footer">

    <div id="footernav">
        <ul>
            <li><a href="http://www.nps.gov/ncrc/programs/rtca/nri/" title="NATIONWIDE RIVERS INVENTORY" target="_blank">Nationwide Rivers Inventory</a></li>
            <!--<li><a href="https://www.rivers.gov/kids/index.html" title="Kid's Site" target="_blank">Kid's Site</a></li> -->
            <li><a href="https://www.rivers.gov/info/contact.cfm" title="Contact Us">Contact Us</a></li>
            <li><a href="<?php echo $base_url; ?>privacy.html" title="Privacy Notice" target="_blank" onClick="privacyPOP()">Privacy Notice</a></li>
            <li><a href="<?php echo $base_url; ?>information.php" title="Q &amp; A Search Engine">Q &amp; A Search Engine</a></li>
            <li class="last"><a href="<?php echo $base_url; ?>site-index.php" title="Site Map">Site Map</a></li>
        </ul>
    </div>

    <div class="nwsrs-social-media-icons-wrap">
        <!-- Pull in Font Awesome icon set -->
        <script src="https://use.fontawesome.com/b29caa6564.js"></script>
        <ul class="nwsrs-social-media-icons">
<!--            <li><a href="#" title="Pinterest" target="_blank" class="social-link"><i class="fa fa-pinterest"></i></a></li>-->
<!--            <li><a href="#" title="Twitter" target="_blank" class="social-link"><i class="fa fa-twitter"></i></a></li>-->
<!--            <li><a href="#" title="Facebook" target="_blank" class="social-link"><i class="fa fa-facebook"></i></a></li>-->
<!--            <li><a href="https://www.flickr.com/photos/wild_rivers/" title="Flickr" target="_blank" class="social-link"><i class="fa fa-flickr"></i></a></li>-->
            <li><a href="https://www.flickr.com/photos/wild_rivers/" target="_blank" class="flickr-button"><img src="https://s.yimg.com/pw/images/goodies/white-flickr.png" alt="Rivers on Flickr" /></a></li>
        </ul>
    </div>


    <?php
    //    Only show the credits on the home page.
    if ($body_class == 'home-page') { ?>

        <div class="row callout no-border clearfix">
            <div class="col-12 column">
                <h3 class="text-left" style="white-space: nowrap">Photo Credits:</h3>
            </div>
            <div class="col-87 column nwsrs-photo-credits text-left">
                <p>Slides, in order: Westfield River (Massachusetts), Au Sable River (Michigan), Black River (Michigan), Crooked River (Oregon), Delta River (Alaska), John Day River (Oregon), Namekagon River (Wisconsin), Ontonagon River (Michigan), Rio Grande (New Mexico), Rio Icacos (Puerto Rico), Wekiva River (Rock Springs Run - Florida), Wolf Creek (Wyoming). All slide photos by Tim Palmer. Mule Deer &#8211; Chuck &amp; Grace Bartlett.</p>
            </div>
        </div>

    <?php } ?>


    <div id="footertabs" class="clearfix">

        <div class="ft-column">
            <h4><a href="<?php echo $base_url; ?>map.php" title="Explore Designated Rivers">Designated Rivers</a></h4>
            <ul>
                <li><a href="<?php echo $base_url; ?>wsr-act.php" title="About WSR Act">About WSR Act</a></li>
                <li><a href="<?php echo $base_url; ?>map.php" title="State Listings">State Listings</a></li>
                <li><a href="<?php echo $base_url; ?>map.php" title="Profile Pages">Profile Pages</a></li>
            </ul>
        </div>
        <div class="ft-column">
            <h4><a href="<?php echo $base_url; ?>national-system.php" title="National System">National System</a></h4>
            <ul>
                <li><a href="<?php echo $base_url; ?>documents/rivers-table.pdf" title="WSR Table" target="_blank">WSR Table</a></li>
                <li><a href="<?php echo $base_url; ?>study.php" title="Study Rivers">Study Rivers</a></li>
                <li><a href="<?php echo $base_url; ?>stewardship.php" title="Stewardship">Stewardship</a></li>
                <li><a href="<?php echo $base_url; ?>publications.php" title="WSR Legislation">WSR Legislation</a></li>
            </ul>
        </div>
        <div class="ft-column">
            <h4><a href="<?php echo $base_url; ?>council.php" title="River Management">River Management</a></h4>
            <ul>
                <li><a href="<?php echo $base_url; ?>council.php" title="Council">Council</a></li>
                <li><a href="<?php echo $base_url; ?>agencies.php" title="Agencies">Agencies</a></li>
                <li><a href="<?php echo $base_url; ?>management-plans.php" title="Management Plans">Management Plans</a></li>
                <li><a href="http://www.river-management.org" title="River Management Society" target="_blank">River Mgt. Society</a></li>
                <li><a href="<?php echo $base_url; ?>mapping-gis.php" title="GIS Mapping">GIS Mapping</a></li>
            </ul>
        </div>
        <div class="ft-column">
            <h4><a href="<?php echo $base_url; ?>information.php" title="Resources">Resources</a></h4>
            <ul>
                <li><a href="<?php echo $base_url; ?>information.php" title="Q &amp; A Search">Q &amp; A Search</a></li>
                <li><a href="<?php echo $base_url; ?>bibliography.php" title="Bibliography">Bibliography</a></li>
                <li><a href="<?php echo $base_url; ?>publications.php" title="Publications">Publications</a></li>
                <li><a href="<?php echo $base_url; ?>mapping-gis.php" title="GIS Mapping">GIS Mapping</a></li>
                <li><a href="<?php echo $base_url; ?>publications.php" title="Logo &amp; Sign Standards">Logo &amp; Sign Standards</a></li>
                <!--<li><a href="<?php echo $base_url; ?>display.php" title="Display">Display</a></li>-->
            </ul>
        </div>

    </div>

</div> <!--END #footer -->

</div> <!--END #inner-wrapper -->

</div> <!--END #outer-wrapper -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99750958-1', 'auto');
  ga('send', 'pageview');

</script>

</body>

</html>