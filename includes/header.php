</head>

<?php
	//Check to see if there's a postal code id. If so, make an ID.
	!empty($state_code) ? $statePostalCode = 'id="' . $state_code . '"' : $statePostalCode = '';
	//Check if there's a body class.
	!empty($body_class) ? $bodyClass = 'class="' . $body_class . '"' : $bodyClass = '';

?>

<body <?php echo $statePostalCode; ?> <?php echo $bodyClass; ?>>
<div id="outer-wrapper">

	<div id="masthead">

		<div id="logo">
			<a href="<?php echo $base_url; ?>index.php" title="Return to Rivers.gov Home Page"><img src="<?php echo $base_url; ?>images/logo.gif" width="115px" height="140px" border="0" alt="National Wild and Scenic Rivers System - www.rivers.gov" title="National Wild and Scenic Rivers System"/></a>
		</div>

		<div id="logo-group">
			<img src="<?php echo $base_url; ?>images/blm-logo.png" width="52px" height="52px" alt="Bureau of Land Management" title="Bureau of Land Management"/>
			<img src="<?php echo $base_url; ?>images/usnps-logo.png" width="52px" height="52px" alt="National Park Service" title="National Park Service"/>
			<img src="<?php echo $base_url; ?>images/usfws-logo.png" width="52px" height="52px" alt="US Fish and Wildlife Service" title="US Fish and Wildlife Service"/>
			<img src="<?php echo $base_url; ?>images/fs-logo.png" width="52px" height="52px" alt="US Forest Service" title="US Forest Service"/>
		</div>

	</div>

	<div id="toolbar">

		<div id="navmenu">
			<ul class="dropdown">
				<!--<li><a href="<?php echo $base_url; ?>index.php" title="Wild and Scenic Rivers Homepage">Home</a></li>-->
				<li><a href="<?php echo $base_url; ?>national-system.php" title="The National System">National System</a>
					<ul class="sub_menu">
						<li><a href="<?php echo $base_url; ?>map.php" title="Explore Designated Rivers">Designated Rivers</a></li>
						<li><a href="<?php echo $base_url; ?>designation.php" title="How Rivers Are Designated">How Rivers Are Designated</a></li>
						<li><a href="<?php echo $base_url; ?>wsr-act.php" title="About the Wild and Scenic Rivers Act">About The Wild &amp; Scenic Rivers Act</a></li>
						<li><a href="<?php echo $base_url; ?>documents/rivers-table.pdf" title="WSR Table (Mileage Classifications)" target="_blank">WSR Table (Mileage Classifications)</a></li>
						<li><a href="<?php echo $base_url; ?>study.php" title="Study Rivers">Study Rivers</a></li>
						<li><a href="<?php echo $base_url; ?>stewardship.php" title="Stewardship">Stewardship</a></li>
						<li><a href="<?php echo $base_url; ?>awards/index.php" title="National Awards">National Awards</a></li>
						<li><a href="<?php echo $base_url; ?>act.php" title="WSR Legislation and Amendments">WSR Legislation &amp; Amendments</a></li>
						<li class="external"><br/>External Links:<br/></li>
						<li><a href="http://www.nps.gov/ncrc/programs/rtca/nri/" title="Nationwide Rivers Inventory (NRI)">Nationwide Rivers Inventory (NRI)</a></li></a>
						<li><a href="http://www.presidency.ucsb.edu/ws/index.php?pid=29150#axzz1xE2Od0br" title="Signing Remarks" target="_blank">Signing Remarks</a></li>
						<li><a href="https://www.rivers.gov/documents/landowners.pdf" title="Riverfront Owners Brochure" target="_blank">Riverfront Owners Brochure</a></li>
					</ul>
					<div class="clear"></div>
				</li>
				<li><a href="<?php echo $base_url; ?>council.php" title="Interagency Council">Management</a>
					<ul class="sub_menu">
						<li><a href="<?php echo $base_url; ?>council.php" title="Management Council">Council</a></li>
						<li><a href="<?php echo $base_url; ?>agencies.php" title="Agencies">Agencies</a></li>
						<li><a href="<?php echo $base_url; ?>management-plans.php" title="Management Plans">Management Plans</a></li>
						<li><a href="<?php echo $base_url; ?>mapping-gis.php" title="GIS Mapping">Gis Mapping</a></li>
						<li class="external"><br/>External Links:<br/></li>
						<li><a href="https://www.blm.gov" title="Bureau of Land Management" target="_blank">Bureau of Land Management</a></li>
						<li><a href="https://www.nps.gov" title="National Park Service" target="_blank">National Park Service</a></li>
                        <li><a href="https://www.nps.gov/orgs/1912/" title="National Park Service Rivers" target="_blank">National Park Service Rivers</a></li>
						<li><a href="https://www.fws.gov" title="US Fish And Wildlife Service" target="_blank">US Fish &amp; Wildlife Service</a></li>
						<li><a href="http://www.fs.fed.us" title="US Forest Service" target="_blank">US Forest Service</a></li>
						<li><a href="http://www.river-management.org" title="River Management Society" target="_blank">River Management Society</a></li>
						<li><a href="https://www.nps.gov/ncrc/programs/rtca/" title="Rivers, Trails &amp; Conservation Assistance Program" target="_blank">Rivers, Trails &amp; Conservation Assistance Program</a></li>
						<br/>
					</ul>
					<div class="clear"></div>
				</li>
				<li><a href="#" title="Resources">Resources</a>
					<ul class="sub_menu">
						<li><a href="<?php echo $base_url; ?>information.php" title="Q and A Search Engine">Q &amp; A Search Engine</a></li>
                        <li><a href="<?php echo $base_url; ?>bibliography.php" title="Bibliography">Bibliography</a></li>
						<li><a href="<?php echo $base_url; ?>publications.php" title="Publications">Publications</a></li>
						<li><a href="<?php echo $base_url; ?>mapping-gis.php" title="Gis Mapping">Maps &amp; GIS</a></li>
						<li><a href="<?php echo $base_url; ?>publications.php" title="">Logo &amp; Sign Standards</a></li>
						<!--<li><a href="<?php echo $base_url; ?>display.php" title="Display">Display</a></li>-->
						<li><a href="<?php echo $base_url; ?>quotations.php" title="River Quotes">River Quotes</a></li>
						<li><a href="<?php echo $base_url; ?>waterfacts.php" title="Water Facts">Water Facts</a></li>

<!--                        TODO: Comment out after approval-->
<!--                        <li><a href="#" title="Transportation Projects">Transportation Projects</a></li>-->

					</ul>
					<div class="clear"></div>
				</li>
				<li><a href="<?php echo $base_url; ?>publications.php" title="Publications">Publications</a>
					<ul class="sub_menu">
						<li><a href="<?php echo $base_url; ?>publications.php" title="Technical Papers">Technical Papers</a></li>
						<li><a href="<?php echo $base_url; ?>publications.php" title="Section 7 Determinations">Section 7 Determinations</a></li>
						<li><a href="<?php echo $base_url; ?>publications.php" title="Non-Council Papers">Non-Council Papers</a></li>
					</ul>
					<div class="clear"></div>
				</li>
				<li><a href="<?php echo $base_url; ?>info/contact.cfm" title="Contact Us">Contact Us</a></li>
				<li><a href="<?php echo $base_url; ?>wsr50/index.php" title="50th Anniversary">50 Years</a>
                    <ul class="sub_menu">
                        <li><a href="<?php echo $base_url; ?>toolkit.php" title="Toolkit">Toolkit</a></li>
						<li><a href="https://umontana.maps.arcgis.com/apps/MapJournal/index.html?appid=f85c819b95f14ae1a44400e46ee9578c" title="50th Anniverssary Event Map" target="_blank">Event Map</a></li>
                        <li><a href="<?php echo $base_url; ?>videos.php" title="Anniversary Videos">Anniversary Videos</a></li>
                    </ul>
                    <div class="clear"></div>
                </li>
                <li><a href="<?php echo $base_url; ?>site-index.php" title="Site Map">Site Index</a></li>
				<div class="clear"></div>
			</ul>
		</div>
		<!-- END #navmenu -->

		<div id="searchbox">
			<form accept-charset="UTF-8" action="http://search.usa.gov/search" id="search_form" method="get">
				<input name="utf8" type="hidden" value="&#x2713;"/>
				<input id="affiliate" name="affiliate" type="hidden" value="rivers.gov"/>
				<label class="nwsrs-search hide" for="query">Search:</label>
				<input type="text" size="20" class="searchfield" autocomplete="off" class="usagov-search-autocomplete" id="query" name="query" type="text" title="Search"/>
				<input type="submit" value="go" class="searchbutton hide-value"/>
			</form>
		</div>
		<!-- END Search Box -->

	</div>
	<!-- END #toolbar -->