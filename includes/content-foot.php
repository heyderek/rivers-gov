</div> <!--END #content-page -->


<? if ( $sidebar_bool !== 0 ): ?>
	<div id="sidebar">

		<div id="sidebar-map">

			<a href="<?php $base_url; ?>/map.php" class="map-btn">Check out the map</a>

			<div id="map-menu">


				<?php
				// includes the river and state dropdowns
				include( "select-list.php" )
				?>

			</div> <!--END #map-menu -->

		</div><!--END #sidebar-map -->


		<div id="region" class="sb-<?php echo $region ?>">

			<!-- Text for sidebar paper note above each regional image -->

			<div id="region-text">
				<?php
				if ( $region == 'alaska' ) {
					//sidebar text for Alaska
					echo 'What is it about Alaska\'s rivers that call to us? The mystic of wilderness? The thrill of exploration? The sheer immensity of the landscape?';
				} elseif ( $region == 'inlandnw' ) {
					//sidebar text for Inland Northwest
					echo 'Seen as barren by the first explorers to today\'s first-time visitors, the rivers of the high desert simply hide their treasures well.';
				} elseif ( $region == 'midwest' ) {
					//sidebar text for Midwest
					echo 'Nourished by the fertile soils of the region, rivers of the Midwest explode with life, from great avian migrations to ancient fishes.';
				} elseif ( $region == 'northeast' ) {
					//sidebar text for Northeast
					echo 'Still, white winters, subtle shades of spring green, lazy summer days, autumns lit with color, rivers in the Northeast showcase the seasons.';
				} elseif ( $region == 'northwest' ) {
					//sidebar text for Northwest
					echo 'Salmon, Native American culture, history, whitewater boating, unmatched beauty, world-renowned fishing, solitude, what don\'t Northwest rivers offer in abundance?';
				} elseif ( $region == 'southeast' ) {
					//sidebar text for Southeast
					echo 'Rivers of the Southeast define diversity, from bayous and rivers pushed by the tides to clear mountain streams with world-class whitewater.';
				} elseif ( $region == 'southwest' ) {
					//sidebar text for Southwest
					echo 'Hidden canyons, ancient rock formations, millennia of human use, rivers are the very focus of life in the Southwest.';
				} elseif ( $region == 'tropical' ) {
					//sidebar text for Tropical
					echo 'Dark and foreboding one minute, sun-drenched and exploding with color the next, tropical rivers span every mood.';
				} elseif ( $region == 'general' ) {
					//sidebar text for General
					echo 'While progress should never come to a halt, there are many places it should never come to at all. &#8212; Paul Newman';
				} else {
					//sidebar text for pages if $region is not set
					echo 'While progress should never come to a halt, there are many places it should never come to at all. &#8212; Paul Newman';
				}
				?>
			</div><!-- END #region-text -->

		</div> <!--END #region -->

	</div> <!--END #sidebar --><? endif ?>

<div class="clear"></div>  <!-- clears everything so page is flexible -->

</div> <!--END #content-wrapper -->


</div> <!--END #content -->

<div id="footer-top"></div> <!--FOOTER TOP - for the papercut effect -->