<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Ohio';
// Set the page keywords
$page_keywords = 'Ohio';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Ohio.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'OH';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Ohio has approximately 29,113 miles of river, of which 212.9 miles are designated as wild &amp; scenic&#8212;less than 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/big-darby.php" title="Big &amp; Little Darby Creeks">Big &amp; Little Darby Creeks</a></li>
<li><a href="rivers/little-beaver.php" title="Little Beaver Creek">Little Beaver Creek</a></li>
<li><a href="rivers/little-miami.php" title="Little Miami River">Little Miami River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>