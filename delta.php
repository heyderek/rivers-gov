<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Delta River, Alaska';

// Set the page keywords
$page_keywords = 'Delta River, Yukon River, Tangle Lakes, Alaska';

// Set the page description
$page_description = 'Delta River, Alaska';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';

//ID for the rivers
$river_id = array('47');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?><?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Glennallen Field Office</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. The segment from and including all of the Tangle Lakes to a point 1/2-mile north of Black Rapids.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 20.0 miles; Scenic &#8212;  24.0 miles; Recreational &#8212; 18.0 miles; Total &#8212; 62.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/delta.jpg" alt="Delta River" title="Delta River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/ak/st/en/prog/nlcs/delta_nwsr.html" alt="Delta River (Bureau of Land Management)" target="_blank">Delta River (Bureau of Land Management)</a><br />
  <a href="../documents/plans/delta-recreation-management-plan.pdf" title="East Alaska Resource Management Plan Amendment" target="_blank">Delta River Special Recreation Management Area Plan</a></p>

<div id="photo-credit">
<p>Photo Credit: Unknown</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Delta River</h2>

<p>The Delta River watershed extends from the Upper Tangle Lakes downstream to Black Rapids; the Tangle River flows through and connects the lakes (150,000 acres, 160 miles of streams, and 21 lakes) before finding its way to the Delta River. The Delta flows north to join the Tanana River and, eventually, the Yukon River. The topography in the vicinity of the lower Delta River region is dominated by the rugged peaks of the Alaska Range, ranging in elevation from 6,000-9,000 feet. Glaciers occupy some of the slopes of these peaks. The land adjacent to the lower Delta River includes steep alluvial slopes, rock cliffs and spectacular geologic features.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The corridor is flanked by both the low, rolling tundra hills of the Amphitheatre Mountains and the high, rugged, snow-covered peaks and ridges of the Alaska Range, providing high-quality scenic vistas. The river and surrounding hills provide undisturbed views of the river canyon, waterfalls, channelized riverbeds, tributaries, granite rock outcroppings and glacial alluvial processes.</p>

<p><strong><em>Recreation</em></strong></p>

<p>This is one of a few easily accessible wild and scenic rivers in the state of Alaska, providing both day use and overnight boating opportunities. A wide range of outstanding recreational opportunities attract people of all ages and abilities for river-related solitude and undisturbed environment or for activities such as wildlife viewing, fishing, hunting, trapping, camping, hiking, snowmachining, skiing and photography. Boating opportunities include both lake and river paddling on clear and glacial water stretches, challenging whitewater, and exceptional opportunities for both day use and extended overnight backcountry excursions.</p>

<p><strong><em>Fish</em></strong></p>

<p>Few rivers anywhere in the world can match the quality and quantity of the Arctic grayling fishery. Most fishing is for grayling, but good lake trout fishing is available in late winter and early spring.  Tangle Lakes and the Delta River also support round whitefish, lake trout, burbot and longnose suckers.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>More than 100 species of migrating birds and waterfowl use the river corridor and the surrounding lakes as nesting areas. Trumpeter swans, a BLM sensitive species, nest in the wetlands of the Upper Tangle Lakes. Bald eagles frequent the area to nest and hunt for fish and various waterfowl. Grizzly bears frequent the lowlands to fish and to hunt where moose drop their calves. Moose inhabit the lowlands in the summer, while generally wintering at higher elevations in the surrounding hills. Tens of thousands of Nelchina caribou travel through this area during their annual migration to and from the calving grounds.</p>

<p><strong><em>Cultural</em></strong></p>

<p>The southern stretches of the designated corridor are located within the Tangle Lakes Archaeological District and contain nearly 280 recorded archaeological sites. Almost all of the earliest known archaeological sites in the region are found within the designated river corridor, representing a history of humans hunting, mining and subsisting from more than 10,000 years ago through the recent past.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>