<?php
// Set the page title  -- GENERAL TEMPLATE 2 
$page_title = 'Other Information';

// Set the page keywords 
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service, links';

// Set the page description
$page_description = 'Other sources of information.';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->
<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Other Information</h2>
<p>For more information on rivers and river management, we encourage you to contact any of the organizations below. Neither the wild and scenic river administering agencies nor the Interagency Wild and Scenic Rivers Coordinating Council endorse any of the organizations below or the positions they hold; these web sites are simply other sources of information. If you visit any of these sites, you'll be leaving the Wild &amp; Scenic Rivers web site. So bookmark this page, and be sure to visit us from time to time. We'll be posting announcements and updating this page as necessary. And hopefully, we'll be adding to the list of designated wild and scenic rivers every so often.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->

<center><img src="images/frogs.jpg" alt="Pacific Tree Frog" height="264px" title="Pacific Tree Frog - Ron Murphy" width="565px" /><br /><em style="font-size:11px">Photo by Ron Murphy</em></center>

<div id="lower-content">

<h2 style="font-size:16px; color:#000">Federal Agencies</h2>

<ul>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.usace.army.mil/Missions/CivilWorks/RegulatoryProgramandPermits.aspx" target="_blank">Army Corps of Engineers Regulatory Program</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.epa.gov/owow" target="_blank">Environmental Protection Agency: Office of Wetlands, Oceans & Watersheds</a><br /> The most comprehensive site on the web for information on polluted run-off, wetlands, restoration techniques, water quality assessment, and watershed management.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.epa.gov/owow/wetlands/restore/" target="_blank">Environmental Protection Agency: River Corridor and Wetland Restoration</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="https://www.nal.usda.gov/waic" target="_blank">National Agricultural Library's Water Quality Information Center</a><br />(Agricultural Research Service, U. S. Department of Agriculture), in cooperation with the University of Maryland, this site has provided users access to information related to water resources and agriculture in a searchable format.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.nps.gov/rtca/" target="_blank">National Park Service Rivers & Trails Program</a> Provides many ways to improve and protect your river.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://water.usgs.gov" target="_blank">United States Geological Survey Water Resources Division</a><br />Provides data on the physical characteristics of streams in the United States, including <a style="font-size:12px; color:#1570b4; font-style:italic; line-height:100%;" href="http://waterdata.usgs.gov/nwis/rt/" target="_blank">realtime stream flow data</a>.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.presidency.ucsb.edu/ws/index.php?pid=29150#axzz1x88zSeBN" target="_blank">University of California&#8211;Santa Barbara's Presidency Project</a><br />Chronicles the remarks made by President Johnson when he signed the Wild &amp; Scenic Rivers Act in 1968.</p></li>

</ul>

<p>&nbsp;</p>

<h2 style="font-size:16px; color:#000">Non-Profit Environmental Organizations</h2>

<ul>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.americanrivers.org" target="_blank">American Rivers</a> is the leading river conservation organization in the country.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.americanwhitewater.org" target="_blank">American Whitewater</a><br /> Works to promote and protect boating opportunities across the country.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.cwp.org" target="_blank">Center for Watershed Protection</a><br />Can provide answers on protecting entire watersheds.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.cbf.org" target="_blank">Chesapeake Bay Foundation</a><br />Dedicated to the preservation of one of our greatest estuaries, the foundation provides information on managing the rivers and watersheds flowing into the Bay.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.friendsoftheriver.org" target="_blank">Friends of the River</a><br />Mobilizes positive citizen action to protect and restore the ecosystems of rivers in California and of watersheds shared with Arizona, Nevada and Oregon.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.irn.org" target="_blank">International Rivers Network</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.iwla.org" target="_blank">The Izaak Walton League</a><br />One of the oldest conservation organizations and has several river programs, including <a style="font-size:12px; color:#1570b4; font-style:italic;" href="http://www.iwla.org/conservation/water/save-our-streams" target="_blank">Save Our Streams</a>, that allow people to become actively involved in protecting rivers and water quality.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.lcv.org" target="_blank">The League of Conservation Voters</a><br />Tracks legislation and the voting records in the U.S. Congress on environmental issues.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.audubon.org" target="_blank">National Audubon Society</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.nfwf.org" target="_blank">The National Fish and Wildlife Foundation</a><br />Dedicated to the conservation of fish and wildlife.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.nwf.org" target="_blank">National Wildlife Federation</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.pacrivers.org" target="_blank">The Pacific Rivers Council</a><br />Works to protect rivers and watersheds in the Pacific Northwest.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.prairierivers.org" target="_blank">Prairie Rivers Network</a><br />Works to protect and promote Illinois' rivers.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.rivernetwork.org" target="_blank">River Network</a><br />This site's primary focus is on providing information to its members through technical publications and maintaining a "Skills Bank."</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.wildsalmon.org" target="_blank">Save Our Wild Salmon</a><br />Works to protect dwindling salmon stocks in the West.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.sierraclub.org" target="_blank">Sierra Club</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.tu.org" target="_blank">Trout Unlimited</a><br />Dedicated to protecting and restoring trout populations throughout the country.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.waterwatch.org" target="_blank">WaterWatch of Oregon</a><br />Works on river conservation issues in Oregon.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.wilderness.org" target="_blank">The Wilderness Society</a></p></li>

</ul>

<p>&nbsp;</p>

<h2 style="font-size:16px; color:#000">River &amp; Environmental Information</h2>

<ul>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.igc.org" target="_blank">Econet: Institute For Global Communications</a><br />Provides information on a broad range of topics related to our rivers, lakes and oceans.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.envirolink.org" target="_blank">EnviroLink</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.webdirectory.com" target="_blank">Environment Directory</a><br />A compendium of hyperlinks to World Wide Web Resources.  Resources included on this page may be of interest to botanists, plant ecologists, conservation biologists, resource managers, and students.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.earthforce.org/GREEN" target="_blank">Global Rivers Environmental Education Network</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.great-lakes.net" target="_blank">The Great Lakes Information Network</a><br />Provides a wealth of information on the greatest bodies of fresh water in the world.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://ctic.purdue.edu/kyw/kyw.html" target="_blank">Know Your Watershed</a><br />Housed at Purdue University, this site can help you to understand watersheds and assist you in forming a watershed organization.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.river-management.org" target="_blank">River Management Society</a><br />A professional organization of folks interested in the management of our river resources.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.streamnet.org" target="_blank">StreamNet</a><br />A cooperative venture of the Pacific Northwest's fish and wildlife agencies and tribes, providing data in support of the region's efforts to preserve and restore aquatic resources.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.wef.org" target="_blank">Water Environment Federation</a></p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.waterchat.com" target="_blank">Water Strategist Community</a><br />A commercial site providing information on a wide range of water-related topics. Well worth the visit.</p></li>

</ul>

<p>&nbsp;</p>

<h2 style="font-size:16px; color:#000">Trip Planning</h2>

<ul>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.allaboutrivers.com" target="_blank">All About Rivers</a><br />Might be able to help you plan your next river trip and has other useful information for paddlers.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.americaoutdoors.org" target="_blank">America Outdoors</a><br />Represents the outfitters providing recreation on our nation's rivers.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.americancanoe.org" target="_blank">American Canoe Association</a><br />A nationwide non-profit organization dedicated to kayak, canoe and rafting recreation, education, and stewardship. They also maintain a <a style="font-size:12px; color:#1570b4; font-style:italic; line-height:100%;" href="http://www.americancanoe.org/search/custom.asp?id=2080" target="_blank">database of water trails</a>.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.riversearch.com" target="_blank">River Search</a><br />An outfitting page that can help you plan your next river trip.</p></li>

</ul>

<p>&nbsp;</p>

<h2 style="font-size:16px; color:#000">Water Safety</h2>

<ul>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.americanwhitewater.org" target="_blank">American Whitewater</a><br />Works to promote and protect boating opportunities across the country.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.safeboatingcouncil.org" target="_blank">The National Safe Boating Council</a><br />Strives to reduce accidents and enhance the boating experience.</p></li>

<li><p style="text-indent:-.4in;margin-right:.7in"><a href="http://www.watersafetycongress.org" target="_blank">The National Water Safety Congress</a><br /> Dedicated to promoting recreation water safety in the United States.</p></li>

</ul>

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div><!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>
