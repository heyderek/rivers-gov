<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Idaho';
// Set the page keywords
$page_keywords = 'Idaho';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Idaho.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'ID';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Idaho has approximately 107,651 miles of river, of which 891 miles are designated as wild &amp; scenic&#8212;less than 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/battle.php" title="Battle Creek">Battle Creek</a></li>
<li><a href="rivers/big-jacks.php" title="Big Jacks Creek">Big Jacks Creek</a></li>
<li><a href="rivers/bruneau.php" title="Bruneau River">Bruneau River</a></li>
<li><a href="rivers/bruneau-wf.php" title="Bruneau River (West Fork)">Bruneau River (West Fork)</a></li>
<li><a href="rivers/clearwater-mf.php" title="Clearwater River (Middle Fork)">Clearwater River (Middle Fork)</a></li>
<li><a href="rivers/cottonwood-id.php" title="Cottonwood Creek">Cottonwood Creek</a></li>
<li><a href="rivers/deep.php" title="Deep Creek">Deep Creek</a></li>
<li><a href="rivers/dickshooter.php" title="Dickshooter Creek">Dickshooter Creek</a></li>
<li><a href="rivers/duncan.php" title="Duncan Creek">Duncan Creek</a></li>
<li><a href="rivers/jarbidge.php" title="Jarbidge River">Jarbidge River</a></li>
<li><a href="rivers/little-jacks.php" title="Little Jacks Creek">Little Jacks Creek</a></li>
<li><a href="rivers/owyhee-id.php" title="Owyhee River">Owyhee River</a></li>
<li><a href="rivers/owyhee-nf-id.php" title="Owyhee River (North Fork)">Owyhee River (North Fork)</a></li>
<li><a href="rivers/owyhee-sf-id.php" title="Owyhee River (South Fork)">Owyhee River (South Fork)</a></li>
<li><a href="rivers/rapid.php" title="Rapid River">Rapid River</a></li>
<li><a href="rivers/red-canyon.php" title="Red Canyon">Red Canyon</a></li>
<li><a href="rivers/st-joe.php" title="St. Joe River">St. Joe River</a></li>
<li><a href="rivers/salmon-id.php" title="Salmon River">Salmon River</a></li>
<li><a href="rivers/salmon-mf-id.php" title="Salmon River (Middle Fork)">Salmon River (Middle Fork)</a></li>
<li><a href="rivers/sheep.php" title="Sheep Creek">Sheep Creek</a></li>
<li><a href="rivers/snake.php" title="Snake River">Snake River</a></li>
<li><a href="rivers/wickahoney.php" title="Wickahoney Creek">Wickahoney Creek</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>