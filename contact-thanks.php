<?php

// Set the page title  -- GENERAL TEMPLATE 2 

$page_title = 'Wild &amp; Scenic Rivers Information';



// Set the page keywords 

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';



// Set the page description

$page_description = 'Wild &amp; Scenic Rivers Information';



// Set the region for Sidebar Images 

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'general';



// Includes the meta data that is common to all pages

include ("includes/metascript.php");

 ?>

 

	<!-- BEGIN page specific CSS and Scripts -->



	<!-- END page specific CSS and Scripts -->	

		

			<?php 

			// includes the TEMPLATE HEADER CODING -- #content-page		

			include ("includes/header.php") 

			?>

			

			<?php 

			// includes the content page top		

			include ("includes/content-head.php") 

			?>

					<div id="open-content">
               

						<h2>Thank you for your interest in the National Wild &amp; Scenic Rivers System.</h2>
						<p>We will get back to you as soon as possible.</p>
			   
						
					</div><!-- End #open-content -->

							<div class="clear"></div> <!-- Allows for content above to be flexible -->
					
						

						

						

			<?php 

			// includes the content page bottom		

			include ("includes/content-foot.php") 

			?>			

					

<?php 

// includes the TEMPLATE FOOTER CODING -- </html> 		

include ("includes/footer.php") 

?>

				