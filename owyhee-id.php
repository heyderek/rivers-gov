<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Owyhee River, Idaho';

// Set the page keywords
$page_keywords = 'Owyhee River, Idaho';

// Set the page description
$page_description = 'Owyhee River, Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('191');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?><?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The Owyhee River from the Idaho-Oregon State border to the upstream boundary of the Owyhee River Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 67.3 miles; Total &#8212; 67.3 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/owyhee-id.jpg" alt="Owyhee River" title="Owyhee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/programs/national-conservation-lands/wild-and-scenic-rivers/idaho" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Owyhee River</h2>
<p>The Owyhee River Canyon consists of steep, rhyolite walls ranging in height from 250 feet to over 1,000 feet near the Oregon border. The west end of the river below the confluence with the South Fork Owyhee River is known as the "Grand Canyon of the Owyhee." Within this gorge can be found extensive areas of rhyolite pinnacle formations known as "hoodoos." This is the most dramatic area of hoodoo formations within the entire Owyhee River system. Floating the Owyhee is popular in the spring during higher water flows. Low water float trips are also possible in smaller craft.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>