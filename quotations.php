<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'River and Environmental Quotations';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service, environment, environmental, quotes, quotations, quotation, quote';

// Set the page description
$page_description = 'River and Environmental Quotations';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- JS that controls the accordion -->

<script type="text/javascript">
$(document).ready(function(){
$(".toggle_container").hide();
$("h2.trigger").click(function(){
$(this).toggleClass("active").next().slideToggle("slow");
});
});
</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>River &amp; Environmental Quotations</h2>
<p>As we were designing a brochure on the National Wild and Scenic Rivers System, we began to collect quotations. We asked people across the country to send us their favorite river quotes. The response was overwhelming. So many people asked for the full list when we were done, that we'd thought we'd share it here.</p>
<p>Tom Splitt, a composer out of Colorado, has graciously offered us the use of an original composition, entitled "The River." This selection is from his album "Elan." If you would like to listen to "The River" while browsing this page, just click on the start button below. If you would like to learn more about this or other Tom Splitt CDs, please
visit <a href="http://tomsplitt.com" target="_blank">tomsplitt.com</a>.</p>
<br />
<p><audio controls>
<source src="river.mp3" type="audio/mp3">
<source src="river.ogg" type="audio/ogg">
Your browser does not support the audio element.
</audio>
</p>
<p></p>
<p>If you use any of these quotes, please be sure to attribute them to the authors.<br /><br />And please, if you have a great quote on rivers, water,<br />or the environment, please send it to <br />us at <a href="mailto:rivers@fws.gov">rivers@fws.gov</a>.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/quotations-img.jpg" alt="" width="565px" height="198px" /></center>

<div id="accords">

<h2>My Favorite Quotations</h2>

<h2 class="trigger">Quotations On Rivers &#8211; Authors A-B</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">I choose to listen to the river for a while, thinking river thoughts, before joining the night and the stars.</em> &#8212; (Edward Abbey)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Night and day the river flows. If time is the mind of space, the River is the soul of the desert. Brave boatmen come, they go, they die, the voyage flows on forever. We are all canyoneers. We are all passengers on this little mossy ship, this delicate dory sailing round the sun that humans call the earth. Joy, shipmates, joy.</em> &#8212; (Edward Abbey, <em>The Hidden Canyon &#8212; A River Journey</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If you' re not beside a real river, close your eyes, and sit down beside an imaginary one, a river where you feel comfortable and safe. Know that the water has wisdom, in its motion through the world, as much wisdom as any of us have. Picture yourself as the water. We are liquid; we innately share water's wisdom.</em> &#8212; (Eric Alan, "Meditation Draws Its Power From the Water," <em>The Oregonian</em> (September 11, 2005))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">To have some parts flowing free again . . . with deer grazing on its banks . . . ducks and geese raising their young in the backwaters . . . eddies and twists and turns for canoeists . . . and fishing opportunities such as Lewis and Clark enjoyed . . . would be the finest possible tribute to the men of the Expedition, and a priceless gift for our children.</em> &#8212; (Stephen Ambrose, <em>Undaunted Courage</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">And I count myself more fortunate with each passing season to have recourse to these quiet, tree-strewn, untrimmed acres by the water. I would think it a sad commentary on the quality of American life if, with our pecuniary and natural abundance, we could not secure for our generation and those to come the existence of . . . a substantial remnant of a once great endowment of wild and scenic rivers.</em> &#8212; (William Anderson, Congressman from Tennessee, Arguing for passage of the Wild and Scenic Rivers Act (1968))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A brook can be a friend in a special way. It talks to you with splashy gurgles. It cools your toes and lets you sit quietly beside it when you don't feel like speaking.</em> &#8212; (Joan Walsh Anglund, <em>A Friend is Someone Who Likes You</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Sit by a river. Find peace and meaning in the rhythm of the lifeblood of the Earth.</em> &#8212; (Anonymous)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If a man fails to honor the rivers, he shall not gain the life from them.</em> &#8212; (Anonymous)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Boundaries don't protect rivers, people do.</em> &#8212; (Brad Arrowsmith, Landowner along the Niobrara National Scenic River, Nebraska)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The river delights to lift us free, if only we dare to let go. Our true work is this voyage, this adventure.</em> &#8212; (Richard Bach, <em>Illusions: The Adventures of a Reluctant Messiah</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Wild rivers are earth's renegades, defying gravity, dancing to their own tunes, resisting the authority of humans, always chipping away, and eventually always winning.</em> &#8212; (Richard Bangs &amp; Christian Kallen, <em>River Gods</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Men may dam it and say that they have made a lake, but it will still be a river. It will keep its nature and bide its time, like a caged animal alert for the slightest opening. In time, it will have its way; the dam, like the ancient cliffs, will be carried away piecemeal in the currents.</em> &#8212; (Wendell Berry)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We labor long and earnestly for peace, because war threatens the survival of man.  It is time we labored with equal passion to defend our environment. A polluted stream can be as lethal as a bullet.</em> &#8212; (Senator Alan Bible from Nevada)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Choosing to save a river is more often an act of passion than of careful calculation. You make the choice because the river has touched your life in an intimate and irreversible way, because you are unwilling to accept its loss.</em> &#8212; (David Bolling, <em>How to Save a River:  Handbook for Citizen Action</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Any river is really the summation of the whole valley. To think of it as nothing but water is to ignore the greater part.</em> &#8212; (Hal Borland, <em>This Hill, This Valley</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">What makes a river so restful to people is that it doesn't have any doubt&#8212;it is sure to get where it is going, and it doesn't want to go anywhere else.</em> &#8212; (Hal Boyle)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Sometimes luck is with you, and sometimes not, but the important thing is to take the dare. Those who climb mountains or raft rivers understand this.</em> &#8212; (David Brower)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">There are many ways to salvation, and one of them is to follow a river.</em> &#8212; (David Brower, Foreword to <em>Oregon Rivers</em> by Larry Olson and John Daniel)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">You don't need it, but will you take some advice from a Californian who's been around for a while? Cherish these rivers. Witness for them. Enjoy their unimprovable purpose as you sense it, and let those rivers that you never visit comfort you with the assurance that they are there, doing wonderfully what they have always done.</em> &#8212; (David Brower, Foreword to <em>Oregon Rivers</em> by Larry Olson and John Daniel)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Keep your rivers flowing as they will, and you will continue to know the most important of all freedoms&#8212;the boundless scope of the human mind to contemplate wonders, and to begin to understand their meaning.</em> &#8212; (David Brower, The Foreword to <em>Oregon Rivers</em> by Larry Olson and John Daniel)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Let the mountains talk, let the river run. Once more, and forever.</em> &#8212; (David Brower, <em>Let the Mountains Talk; Let the Rivers Run</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In a mucked up lovely river,<br />I cast my little fly.<br />I look at that river and smell it<br />And it makes me want to cry.<br />Oh to clean our dirty planet,<br />Now there's a noble wish,<br />And I'm puttin' my shoulder to the wheel<br />'Cause I wanna catch some fish.</em><br />(Greg Brown, "Spring Wind" in <em>Dream Cafe</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The song of the river ends not at her banks but in the hearts of those who have loved her.</em> &#8212; (Buffalo Joe)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Or like the snow falls in the river,<br />A moment white&#8212;then melts for ever . . .</em><br />(Robert Burns, <em>Tam O'Shanter</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It is difficult to find in life any event which so effectually condenses intense nervous sensation into the shortest possible space of time as does the work of shooting, or running an immense rapid. There is no toil, no heart breaking labour about it, but as much coolness, dexterity, and skill as man can throw into the work of hand, eye, and head; knowledge of when to strike and how to do it; knowledge of water and rock, and of the one hundred combinations which rock and water can assume&#8212;for these two things, rock and water, taken in the abstract, fail as completely to convey any idea of their fierce embracings in the throes of a rapid as the fire burning quietly in a drawing-room fireplace fails to convey the idea of a house wrapped and sheeted in flames.</em> &#8212; (Sir William Francis Butler)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Rivers &#8211; Authors C-D</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Peace I ask of thee, o River<br />Peace, peace, peace<br />When I learn to live serenely<br />Cares will cease.<br />From the hills I gather courage<br />Visions of the days to be<br />Strength to lead and faith to follow<br />All are given unto me<br />Peace I ask of thee, o River<br />Peace, peace, peace.</em><br />(Camp song (Author Unknown))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The mark of a successful man is one that has spent an entire day on the bank of a river without feeling guilty about it.</em> &#8212; (Chinese philosopher)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We call upon the waters that rim the earth, horizon to horizon, that flow in our rivers and streams, that fall upon our gardens and fields, and we ask that they teach us and show us the way.</em> &#8212; (Chinook Blessing Litany)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In a country where nature has been so lavish and where we have been so spendthrift of indigenous beauty, to set aside a few rivers in their natural state should be considered an obligation.</em> &#8212; (Senator Frank Church from Idaho, Arguing for passage of the Wild and Scenic Rivers Act (1968))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The affluent society has built well in terms of economic progress, but has neglected the protection of the very water we drink as well as the values of fish and wildlife, scenic, and outdoor recreation resources. Although often measureless in commercial terms, these values must be preserved by a program that will guarantee America some semblance of her great heritage of beautiful rivers.</em> &#8212; (Senator Frank Church from Idaho, Arguing for passage of the Wild and Scenic Rivers Act (1968))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I know the sound the river makes, by dawn, by night, by day<br />But can it stay me through tomorrows that find me far away?</em> &#8212; (Possibly Ralph Conroy, "Roger's River," Field and Stream, August 1990)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In spite of the durability of rock-walled canyons and the surging power of cataracting water, the wild river is a fragile thing&#8212;the most fragile portion of the wilderness country.</em> &#8212; (John Craighead, Biologist and one of the architects of the Wild and Scenic Rivers Act)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The river called. The call is the thundering rumble of distant rapids, the intimate roar of white water . . . a primeval summons to primordial values.</em> &#8212; (John Craighead, Naturalist Magazine (Autumn 1965))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river is the cosiest of friends. You must love it and live with it before you can know it.</em> &#8212; (G.W. Curtis, <em>Lotus Eating:  Hudson and Rhine</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In rivers, the water that you touch is the last of what has passed and the first of that which comes; so with present time.</em> &#8212; (Leonardo da Vinci)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The stream sings a subdued music, a scarcely audible lilt, faint and fluid syllables not quite said. It slips away into its future, where it already is, and flows steadily forth from up the canyon, a fountain of rumors from regions known to it and not to me.</em> &#8212; (John Daniel, <em>Oregon Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We don't tend to ask where a lake comes from. It lies before us, contained and complete, tantalizing in its depth but not its origin. A river is a different kind of mystery, a mystery of distance and becoming, a mystery of source. Touch its fluent body and you touch far places. You touch a story that must end somewhere but cannot stop telling itself, a story that is always just beginning.</em> &#8212; (John Daniel, <em>Oregon Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We are never far from the lilt and swirl of living water.  Whether to fish or swim or paddle, of only to stand and gaze, to glance as we cross a bridge, all of us are drawn to rivers, all of us happily submit to their spell. We need their familiar mystery.  We need their fluent lives interflowing with our own.</em> &#8212; (John Daniel, <em>Oregon Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">How could drops of water know themselves to be a river? Yet the river flows on.</em> &#8212; (Antoine de Saint-Exupery, <em>Wind, Sand and Stars</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I am beginning to understand that the stream the scientists are studying is not just a little creek. It's a river of energy that moves across regions in great geographic cycles. Here, life and death are only different points on a continuum. The stream flows in a circle through time and space, turning death into life across coastal ecosystems, as it has for more than a million years. But such streams no longer flow in the places where most of us live.</em> &#8212; (Kathleen Dean Moore and Jonathan W. Moore, "The Gift of Salmon," Discover Magazine, May 2003)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I have seen salmon swimming upstream to spawn even with their eyes pecked out. Even as they are dying, as their flesh is falling away from their spines, I have seen salmon fighting to protect their nests. I have seen them push up creeks so small that they rammed themselves across the gravel. I have seen them swim upstream with huge chunks bitten out of their bodies by bears. Salmon are incredibly driven to spawn. They will not give up. This gives me hope.</em> &#8212; (Kathleen Dean Moore and Jonathan W. Moore, "The Gift of Salmon," Discover Magazine, May 2003)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">'The north changes the world. In the winter the snow comes, covers the land. When it breaks in the spring, the mountains and hills will gather all the deteriorated stuff and bring it down to the Columbia, the main channel, and take it away. What goes out in the ocean will never return. And we have a brand new world in spring. The high water takes everything out, washes everything down. That's why we pray to the water, every morning and night.' This is not an attitude found in the Army Corps of Engineers literature.</em> &#8212; (Martin Louie, Sr., an elder of the Colville Tribe displaced by Grand Coulee Dam, Quoted by William Dietrich, <em>Northwest Passage</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If you grew up in the country, chances are you have fond memories of lazy days down by a river, creek or pond.</em> &#8212; (Darlene Donaldson, "The River" in Country Magazine)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Rivers &#8211; Authors E-G</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">When protected, rivers serve as visible symbols of the care we take as temporary inhabitants and full-time stewards of a living, profoundly beautiful heritage of nature.</em> &#8212; (John Echeverria, Pope Barrow, Richard Roos Collins, <em>Rivers at Risk:  The Concerned Citizen's Guide to Hydropower</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">To trace the history of a river or a raindrop . . . is also to trace the history of the soul, the history of the mind descending and arising in the body. In both, we constantly seek and stumble upon divinity, which like feeding the lake, and the spring becoming a waterfall, feeds, spills, falls, and feeds itself all over again.</em> &#8212; (Gretel Ehrlich, <em>Islands, The Universe, Home</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I stand by the river and I know that it has been here yesterday and will be here tomorrow and that therefore, since I am part of its pattern today, I also belong to all its yesterdays and will be a part of all its tomorrows. This is a kind of earthly immortality, a kinship with rivers and hills and rocks, with all things and all creatures that have ever lived or ever will live or have their being on the earth. It is my assurance of an orderly continuity in the great design of the universe.</em> &#8212; (Virginia Eifert)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">For an instant, as I bobbed into the channel, I had the sensation of sliding down the vast, titled face of the continent. It was then that I felt the cold needles of Alpine springs at my fingertips and the warmth of the Gulf pulling me southward. Moving with me, leaving its taste upon my mouth and spouting under me in dancing springs of sand, was the immense body of the continent itself, flowing like the river was flowing, grain by grain, mountain by mountain, down to the sea. I was streaming over ancient sea beds thrust aloft where giant reptiles had once sported; I was wearing down the face of time.</em> &#8212; (Loren Eiseley, "Four Quartets," in <em>The Immense Journey</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I do not know much about gods; but I think that the river<br />Is a strong brown god&#8212;sullen, untamed and intractable.</em><br />(T.S. Eliot, "Four Quartets," in <em>The Dry Salvages</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The river itself has no beginning or end. In its beginning, it is not yet the river; in the end it is no longer the river. What we call the headwaters is only a selection from among the innumerable sources which flow together to compose it. At what point in its course does the Mississippi become what the Mississippi means?</em> &#8212; (T.S. Eliot, Introduction to <em>The Adventures of Huckleberry Finn</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Indeed the river is a perpetual gala, and boasts each month a new ornament.</em> &#8212; (Ralph Waldo Emerson, <em>Nature</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Who looks upon a river in a meditative hour, and is not reminded of the flux of all things? Throw a stone into the stream, and the circles that propagate themselves are the beautiful type of all influence.</em> &#8212; (Ralph Waldo Emerson, <em>Nature</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Life is always flowing on like a river, sometimes with murmurs, sometimes without bending this way or that, we do not exactly see why; now in beautiful picturesque places, now through barren and uninteresting scenes, but always flowing with a look of treachery about it; it is so swift, so voiceless, yet so continuous.</em> &#8212; (Faber)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">There's a river somewhere that flows through the lives of everyone.</em> &#8212; (Roberta Flack)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The woods are made for the hunters of dreams,<br />The brooks for the fishers of song;<br />To the hunters who hunt for the gunless game<br />The streams and the woods belong.</em><br />(Sam Walter Foss, <em>The Bloodless Sportsman</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers hardly ever run in a straight line.<br />Rivers are willing to take ten thousand meanders<br />and enjoy every one<br />and grow from every one.<br />When they leave a meander,<br />they are always more<br />than when they entered it.<br />When rivers meet an obstacle,<br />they do not try to run over it.<br />They merely go around<br />but they always get to the other side.<br />Rivers accept things as they are,<br />conform to the shape they find the world in,<br />yet nothing changes things more than rivers.<br />Rivers move even mountains into the sea.<br />Rivers hardly ever are in a hurry<br />yet is there anything more likely<br />to reach the point it sets out for<br />than a river?</em><br />(James Dillet Freeman, <em>Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The farmhouse lingers, though averse to square<br />With the new city street it has to wear<br />A number in.  But what about the brook<br />That held the house as in an elbow-crook?<br />I ask as one who knew the brook, its strength<br />And impulse, having dipped a finger length<br />And made it leap my knuckle, having tossed<br />A flower to try its currents where they crossed.<br />The meadow grass could be cemented down<br />From growing under pavements of a town;<br />The apple tree be sent to hearth-stone flame.<br />Is water wood to serve a brook the same?<br />How else dispose of an immortal force<br />No longer needed?  Staunch it at its source<br />With cinder loads dumped down?  The brook was thrown<br />Deep in a sewer dungeon under stone<br />In fetid darkness still to live and run&#8212;<br />And all for nothing it had ever done<br />Except forget to go in fear perhaps.<br />No one would know except for ancient maps<br />That such a brook ran water.  But I wonder<br />If from its being kept forever under<br />The thoughts may not have risen that so keep<br />This new-built city from both work and sleep.</em><br />(Robert Frost, <em>A Brook in the City</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers are inherently interesting. They mold landscapes, create fertile deltas, provide trade routes, a source for food and water; a place to wash and play; civilizations emerged next to rivers in China, India, Europe, Africa and the Middle East. They sustain life and bring death and destruction. They are ferocious at times; gentle at times. They are placid and mean.  They trigger conflict and delineate boundaries. Rivers are the stuff of metaphor and fable, painting and poetry. Rivers unite and divide&#8212;a thread that runs from source to exhausted release.</em> &#8212; (Edward Gargan, <em>The River's Tale</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river seems a magic thing. A magic, moving, living part of the very earth itself.</em> &#8212; (Laura Gilpin, <em>The Rio Grande</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">He thought his happiness was complete when, as he meandered aimlessly along, suddenly he stood by the edge of a full-fed river. Never in his life had he seen a river before&#8212;this sleek, sinuous, full-bodied animal, chasing and chuckling, gripping things with a gurgle and leaving them with a laugh, to fling itself on fresh playmates that shook themselves free, and were caught and held again. All as a-shake and a-shiver&#8212;glints and gleams and sparkles, rustle and swirl, chatter and bubble. The Mole was bewitched, entranced, fascinated. By the side of the river he trotted as one trots, when very small, by the side of a man who holds one spellbound by exciting stories; and when tired at last, he sat on the bank, while the river still chattered on to him, a babbling procession of the best stories in the world, sent from the heart of the earth to be told at last to the insatiable sea.</em> &#8212; (Kenneth Grahame, <em>The Wind in the Willows</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">"So-this-is-a-River."<br/><br />
"The River," corrected the Rat.<br/><br />
"And you really live by the river? What a jolly life!"<br/><br />
"By it and with it and on it and in it," said the Rat. "It's brother and sister to me, and aunts, and company, and food and drink, and (naturally) washing. It's my world, and I don't want any other. What it hasn't got is not worth having, and what it doesn't know is not worth knowing. Lord! the times we've had together!</em> &#8212; (Kenneth Grahame, <em>The Wind in the Willows</em>)</p>

<p align="center">OR</p>

<p><em style="color:#20693F">"The River," corrected the Rat. . . . It's my world, and I don't want any other. What it hasn't got is not worth having, and what it doesn't know is not worth knowing. Lord! the times we've had together!</em> &#8212; (Kenneth Grahame, <em>The Wind in the Willows</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Rivers &#8211; Authors H-K</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">A whole river is mountain country and hill country and flat country and swamp and delta country, is rock bottom and sand bottom and weed bottom and mud bottom, is blue, green, red, clear, brown, wide, narrow, fast, slow, clean and filthy water, is all the kinds of trees and grasses and all the breeds of animals and birds and men that pertain and have ever pertained to its changing shores, is a thousand differing and not compatible things in between that point where enough of the highland drainlets have trickled together to form it, and that wide, flat, probably desolate place where it discharges itself into the salt of the sea.</em> &#8212; (John Graves, <em>Goodbye to a River</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I have never seen a river that I could not love. Moving water . . . has a fascinating vitality. It has power and grace and associations. It has a thousand colors and a thousand shapes, yet it follows laws so definite that the tiniest streamlet is an exact replica of a great river.</em> &#8212; (Roderick Haig-Brown)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">To live by a large river is to be kept in the heart of things.</em> &#8212; (John Haines)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The trees reflected in the river&#8212;they are unconscious of a spiritual world so near to them.  So are we.</em> &#8212; (Nathaniel Hawthorne)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In this sometimes turbulent world, the river is a cosmic symbol of durability and destiny; awesome, but steadfast. In this period of deep national concern, I wish everyone could live for a while beside a great river.</em> &#8212; (Helen Hayes, Actress)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">You could not step twice into the same river; for other waters are ever flowing on to you.</em> &#8212; (Heraclitus of Ephesus (540 BC - 480 BC), <em>On the Universe</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The river has taught me to listen; you will learn from it, too. The river knows everything; one can learn everything from it.</em> &#8212; (Herman Hesse, <em>Siddhartha</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river is more than an amenity&#8212;it is a treasure that offers a necessity of life that must be rationed among those who have the power over it.</em> &#8212; (Oliver Wendell Holmes, U.S. Supreme Court Justice, Speaking About the Delaware River in 1931)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We grow up hearing so often that a straight line is the shortest distance between two points that we end up thinking it is also the best way to get there. A river knows better&#8212:it has to do with how it dissipates the energy of its flow most efficiently; and how, in its bends, the sediment deposited soon turns into marshes and swampy islands, harboring all manner of interesting life, imparting charm and character to the whole waterway. I would defy you to find a river on this planet that prefers to run straight, unless it has been taught so by the U.S. Army Corps of Engineers.</em> &#8212; (Tom Horton, <em>Bay Country</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I've known rivers;<br />I've known rivers ancient as the world and older than the flow of human blood in human veins.<br /><br />My soul has grown deep like the rivers.</em> &#8212; (Langston Hughes, "The Negro Speaks of Rivers")</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">An unspoiled river is a very rare thing in this Nation today. Their flow and vitality have been harnessed by dams and too often they have been turned into open sewers by communities and by industries. It makes us all very fearful that all rivers will go this way unless somebody acts now to try to balance our river development.</em> &#8212; (President Lyndon Johnson's remarks on signing the Wild &amp; Scenic Rivers Act, October 2, 1968</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">. . . the time has also come to identify and preserve free-flowing stretches of our great rivers before growth and development make the beauty of the unspoiled waterway a memory.</em> &#8212; (President Lyndon Johnson's Message on Natural Beauty)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">my mind shattered<br />in thousands of fragments<br />wishes to spend<br />the whole day on a boat<br />drifting with the river stream</em> &#8212; (Okamoto Kanoko, 1889-1939)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers have what man most respects and longs for in his own life&#8212;a capacity for renewal and replenishment, continual energy, creativity, cleansing.</em> &#8212; (John Kauffman, <em>A Look At Our North Atlantic Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">(A Montana statue) holds that a river has a right to overwhelm its banks and inundate its floodplain. Well, that's interesting, because it's not a right that we assign to the river.  The river has earned it through centuries of deluging and shaping the floodplain, and the floodplain has a right to its rampaging river. They've earned their rights through a kind of reciprocal action.</em> &#8212; (Dan Kemmis, in Harper's Magazine (February 1991))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I started out thinking of America as highways and state lines. As I got to know it better, I began to think of it as rivers. Most of what I love about the country is a gift of the rivers. . . . America is a great story, and there is a river on every page of it.</em> &#8212; (Charles Kuralt, <em>On the Road With Charles Kuralt</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers run through our history and folklore, and link us as a people. They nourish and refresh us and provide a home for dazzling varieties of fish and wildlife and trees and plants of every sort. We are a nation rich in rivers.</em> &#8212; (Charles Kuralt, <em>On the Road With Charles Kuralt</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Rivers &#8211; Authors L-M</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">. . . perhaps our grandsons, having never seen a wild river, will never miss the chance to set a canoe in singing waters . . . glad I shall never be young without wild country to be young in.</em> &#8212; (Aldo Leopold, <em>A Sand County Almanac</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river is the report card for its watershed.</em> &#8212; (Alan Levere, Connecticut Department of Environmental Protection)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">To put your hands in a river is to feel the chords that bind the earth together.</em> &#8212; (Barry Lopez, Author)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Earth and sky, woods and fields, lakes and rivers, the mountains and the sea, are excellent schoolmasters, and teach some of us more than we can ever learn from books.</em> &#8212; (John Lubbock)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river, though, has so many things to say that it is hard to know what it says to each of us.</em> &#8212; (Norman Maclean, <em>A River Runs Through It</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Of course, now I am too old to be much of a fisherman, and now of course I usually fish the big waters alone, although some friends think I shouldn't. Like many fly fishermen in western Montana where the summer days are almost Arctic in length, I often do not start fishing until the cool of the evening. Then in the Arctic half-light of the canyon, all existence fades to a being with my soul and memories and the sounds of the Big Blackfoot River and a four-count rhythm and the hope that a fish will rise.</em></p>

<p><em style="color:#20693F">Eventually, all things merge into one, and a river runs through it. The river was cut by the world's great flood and runs over rocks from the basement of time.  On some of the rocks are timeless raindrops. Under the rocks are the words, and some of the words are theirs.</em></p>

<p><em style="color:#20693F">I am haunted by waters.</em> &#8212; (Norman Maclean, <em>A River Runs Through It</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I sat there and forgot and forgot, until what remained was the river that went by and I who watched. On the river the heat mirages danced with each other and then they danced through each other and then they joined hands and danced around each other. Eventually the water joined the river, and there was only one of us. I believe it was the river.</em> &#8212; (Norman Maclean, <em>A River Runs Through It</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We sat on the bank and the river went by. As always, it was making sounds to itself, and now it made sounds to us. It would be hard to find three men sitting side by side who knew better what a river was saying.</em> &#8212; (Norman Maclean, <em>A River Runs Through It</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Change not the river, for rocks in the river are good and are like our problems&#8212;without them we would not know if there was any current.</em> &#8212; (Dennis Mapes)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">When time comes for us to again rejoin the infinite stream of water flowing to and from the great timeless ocean, our little droplet of soulful water will once again flow with the endless stream.</em> &#8212; (William Marks, <em>The Holy Order of Water</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">This river's taught me a good bit. Probably why I don't leave here. It winds, weaves, snakes around. Rarely goes the same way twice. But, in the end, it always ends up in the same place and the gift is never the same . . . It's the journey that matters</em> &#8212; (Charles Martin, <em>Where the River Ends</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Men travel far to see a city, but few seem curious about a river. Every rivers has, nevertheless, its individuality, its great silent interest. Every river has, moreover, its influence over the people who pass their lives within sight of its waters.</em> &#8212; (H.S. Merriman, <em>The Sowers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Many a time have I merely closed my eyes at the end of yet another troublesome day and soaked my bruised psyche in wild water, rivers remembered and rivers imagined. Rivers course through my dreams, rivers cold and fast, rivers well-known and rivers nameless, rivers that seem like ribbons of blue water twisting through wide valleys, narrow rivers folded in layers of darkening shadows, rivers that have eroded down deep into the mountain's belly, sculpted the land, peeled back the planet's history exposing the texture of time itself.</em> &#8212; (Harry Middleton, <em>On the Spine of Time</em> or <em>Rivers of Memory</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers know this: There is no hurry, we shall get there some day.</em> &#8212; (A.A. Milne, <em>Winnie the Pooh</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Sometimes, if you stand on the bottom rail of a bridge and lean over to watch the river slipping slowly away beneath you, you will suddenly know everything there is to be known.</em> &#8212; (A.A. Milne, <em>Winnie the Pooh</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Instead of paying a psychiatrist, I go out there and get better.</em> &#8212; (Vice President Walter Mondale on the St. Croix River; May 12, 2015, interview with the St. Paul <em>Pioneer Press</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We let a river shower its banks with a spirit that invades the people living there, and we protect that river, knowing that without its blessings the people have no source of soul.</em> &#8212; (Thomas Moore, <em>The Re-Enchantment of Everyday Life</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river sings a holy song conveying the mysterious truth that we are a river, and if we are ignorant of this natural law, we are lost.</em> &#8212; (Thomas Moore, <em>The Re-Enchantment of Everyday Life</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">When in making our way through a forest we hear the loud boom of a waterfall, we know that the stream is descending a precipice. If a heavy rumble and roar, then we know it is passing over a craggy incline. But not only are the existence and size of these larger characters of its channel proclaimed, but all the others. Go to the fountain-canyons of the Merced. Some portions of its channel will appear smooth, others rough, here a slope, there a vertical wall, here a sandy meadow, there a lake-bowl, and the young river speaks and sings all the smaller characters of the smooth slope and downy hush of meadow as faithfully as it sings the great precipices and rapid inclines, so that anyone who has learned the language of running water will see its character in the dark.</em> &#8212; (John Muir, <em>Mountain Thoughts</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Beside the grand history of the glaciers and their own, the mountain streams sing the history of every avalanche or earthquake and of snow, all easily recognized by the human ear, and every word evoked by the falling leaf and drinking deer, beside a thousand other facts so small and spoken by the stream in so low a voice the human ear cannot hear them.</em> &#8212; (John Muir, <em>Mountain Thoughts</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Wonderful how completely everything in wild nature fits into us, as if truly part and parent of us. The sun shines not on us but in us. The rivers flow not past, but through us, thrilling, tingling, vibrating every fiber and cell of the substance of our bodies, making them glide and sing. The trees wave and the flowers bloom in our bodies as well as our souls, and every bird song, wind song, and; tremendous storm song of the rocks in the heart of the mountains is our song, our very own, and sings our love.</em> &#8212; (John Muir, <em>Mountain Thoughts</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Can we afford clean water? Can we afford rivers and lakes and streams and oceans which continue to make possible life on this planet? Can we afford life itself? Those questions were never asked as we destroyed the waters of our nation, and they deserve no answers as we finally move to restore and renew them. These questions answer themselves.</em> &#8212; (Senator Ed Muskie of Maine, Arguing for the passage of the Clean Water Act in 1972 (Congressional Record Service, 1972 Legislative History))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Our planet is beset with a cancer which threatens our very existence and which will not respond to the kind of treatment that has been prescribed in the past. The cancer of water pollution was engendered by our abuse of our lakes, streams, rivers, and oceans; it has thrived on our half-hearted attempts to control it; and like any other disease, it can kill us.</p>

<p>We have ignored this cancer for so long that the romance of environmental concern is already fading in the shadow of the grim realities of lakes, rivers and bays where all forms of life have been smothered by untreated wastes, and oceans which no longer provide us with food.</em> &#8212; (Senator Ed Muskie of Maine, Arguing for the passage of the Clean Water Act in 1972, (Congressional Record Service, 1972 Legislative History))</p>

<!--END .block -->

</div>

<!--END .toggle_container -->

</div>

<h2 class="trigger">Quotations On Rivers &#8211; Authors N-Q</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Ancient rock paintings remind us that there are no unclaimed lands, that people have always lived here. They are wayposts along the river journey to the interior of the mind and heart.</em> &#8212; (Lynn Noel, <em>Voyages:  Canada's Heritage Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It's hard to see a river all at once, especially in the mountains. Down on the plains, rivers run in their course as straightforward as time, channeled toward the sea. But up in the headwaters, a river isn't a point where you stand. In the beginnings of the river, you teeter on the edge of a hundred tiny watersheds where one drop of water is always tipping the balance from one stream to another. History changes with each tiny event, shaping an outcome that we can only fully grasp in hindsight. And that view changes as we move farther downstream.</em> &#8212; (Lynn Noel, <em>Voyages:  Canada's Heritage Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The ancient Irish bards knew the Salmon of Knowledge as the giver of all life's wisdom. In the salmon's leap of understanding like a leap of faith, we can see ourselves "in our element," immersed in the river of life. The cycle of the salmon's journey reminds us that all rivers flow to the same sea.</em> &#8212; (Lynn Noel, <em>Voyages:  Canada's Heritage Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The rapids beat below the boat<br />Deep in the heart of the land<br />Feel the pulse of the river in the pulse at your throat<br />Deep in the heart of the land.</em><br />(Lynn Noel, "Veins in the Stone")</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The river moves from land to water to land, in and out of organisms, reminding us what native peoples have never forgotten: that you cannot separate the land from the water, or the people from the land.</em> &#8212; (Lynn Noel, <em>Voyages:  Canada's Heritage Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The first river you paddle runs through the rest of your life. It bubbles up in pools and eddies to remind you who you are.</em> &#8212; (Lynn Noel)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We are deep at the bottom of this river of time, caught up in the current of the moment where all the rivers rendezvous.</em> &#8212; (Lynn Noel, <em>Voyages:  Canada's Heritage Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Our precious heritage of natural and unspoiled beauty and unpolluted streams, once exhausted and destroyed, can never be replaced. . . . We have a golden opportunity to save the few remaining scenic and wild rivers as part of our nation's heritage for this and coming generations.</em> &#8212; (Alvin O'Konski, Congressman from Wisconsin (regarding the St. Croix River))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">As long as there are young men with the light of adventure in their eyes or a touch of wilderness in their souls, rapids will be run.</em> &#8212; (Sigurd Olson, Wilderness Advocate and Writer)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Lying like short curls of thread thrown onto a map, the protected rivers remain strongholds of the free-flow and refuges of the riparian Eden, of the mountain farmer and the rural landowner. The rivers are stretched-out green reserves overflowing with life, potential, and promise.</em> &#8212; (Tim Palmer, <em>The Wild and Scenic Rivers of America</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">When we save a river, we save a major part of an ecosystem, and we save ourselves as well because of our dependence&#8212;physical, economic, spiritual&#8212;on the water and its community of life.</em> &#8212; (Tim Palmer, <em>The Wild and Scenic Rivers of America</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers are exquisite in their abilities to nurture life, sublime in functioning detail, impressive in contributions of global significance.</em> &#8212; (Tim Palmer, <em>Lifelines</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers are magnets for the imagination, for conscious pondering and subconscious dreams, thrills, fears. People stare into the moving water, captivated, as they are when gazing into a fire. What is it that draws and holds us? The rivers' reflections of our lives and experiences are endless.</em> &#8212; (Tim Palmer, <em>Lifelines</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Streams represent constant rebirth. The water flows in, forever new, yet forever the same; they complete a journey from beginning to end, and then they embark on the journey again.</em> &#8212; (Tim Palmer, <em>Lifelines</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The river is the center of the land, the place where the waters, and much more, come together. Here is the home of wildlife, the route of explorers, and recreation paradise. . . . Only fragments of our inheritance remain unexploited, but these streams are more valuable than ever.</em> &#8212; (Tim Palmer, 1986)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers are highways that move on and bear us whither we wish to go.</em> &#8212; (Blaise Pascal)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Don't push the river&#8212;it flows by itself.</em> &#8212; (Fritz Perls, <em>Gestalt Therapy Verbatim</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The activist is not the man who says the river is dirty. The activist is the man who cleans up the river.</em> &#8212; (Ross Perot)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The rivers are our brothers. They quench our thirst. The rivers carry our canoes, and feed our children. If we sell you our land, you must remember, and teach your children, that the rivers are our brothers and yours, and you must henceforth give the rivers the kindness you would give any brother.</em> &#8212; (Ted Perry, 1971, in the script for <em>Home</em> embellishing on a speech given by Chief Sealth, 1854)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">To drown a river beneath its own impounded water, by damming, is to kill what it was and to settle for something else. When the damming happens without good reason . . . then it's a tragedy of diminishment for the whole planet, a loss of one more wild thing, leaving Earth just a little flatter and tamer and simpler and uglier than before.</em> &#8212; (David Quammen, "Grabbing the Loop" in <em>The Gift of Rivers:  True Stories of Life on the Water</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Rivers &#8211; Authors R-S</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Who owns Cross Creek?  The redbirds, I think, more than I, for they will have their nests even in the face of delinquent mortgages. And after I am dead, who am childless, the human ownership of grove and field and hammock is hypothetical. But a long line of redbirds and whippoorwills and blue-jays and ground doves will descend from the present owners of nests in the orange trees, and their claim will be less subject to dispute than that of any human heirs. Houses are individual and can be owned, like nests, and fought for. But what of the land? It seems to me that the Earth may be borrowed but not bought. It may be used, but not owned. It gives itself in response to love and tending, offers it seasonal flowering and fruiting.  But we are tenants and not possessors, lovers and not masters. Cross Creek belongs to the wind and the rain, to the sun and the seasons, to the cosmic secrecy of seed, and beyond all, to time.</em> &#8212; (Marjorie Kinnan Rawlings, <em>Cross Creek</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">There is no rushing a river. When you go there, you go at the pace of the water and that pace ties you into a flow that is older than life on this planet. Acceptance of that pace, even for a day, changes us, reminds us of other rhythms beyond the sound of our own heartbeats.</em> &#8212; (Jeff Rennicke, <em>River Days:  Travels on Western Rivers, A Collection of Essays</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers are the primal highways of life. From the crack of time, they had borne men's dreams, and in their lovely rush to elsewhere, fed our wanderlust, mimicked our arteries, and charmed our imaginations in a way the static pond or vast and savage ocean never could.</em> &#8212; (Tom Robbins, <em>Fierce Invalids from Hot Climates</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river does not just happen; it has a beginning and an end. Its story is written in rich earth, in ice, and in water-carved stone, and its story as the lifeblood of the land is filled with colour, music and thunder.</em> &#8212; (Andy Russell, <em>The Life of a River</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">And this our life, exempt from public haunt,<br />Finds tongues in trees, books in the running brooks,<br />Sermons in stones, and good in everything.</em><br />(William Shakespeare, <em>As You Like It</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Therefore the winds, piping to us in vain<br />As in revenge, have suck'd up from the sea<br />Contagious fogs; which falling in the land<br />Hath every pelting river made so proud<br />That they have overborne their continents.</em><br />(William Shakespeare, <em>A Midsummer Night's Dream</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Everyone lives downstream. Even those idealists who live with their heads in the clouds live downstream . . . moreso those whose heads are buried in the sand.</em> &#8212; (Duane Short, Illinois Forest Activist)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Ah . . . timelessness, a river's secret weapon.</em> &#8212; (Duane Short, Illinois Forest Activist (January 17, 2006))</p>

<p><em style="color:#20693F">Mother Nature is our wild world. A wild, winding river is her autograph.</em> &#8212; (Duane Short, Wild Species Program Director, Biodiversity Conservation Alliance (January 24, 2011))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">From its fountains<br />In the mountains,<br />Its rills and its gills;<br />Through moss and through brake,<br />It runs and it creeps<br />For awhile till it sleeps<br />In its own little Lake.<br />And thence at departing,<br />Awakening and starting,<br />It runs through the reeds<br />And away it proceeds,<br />Through meadow and glade,<br />In sun and in shade,<br />And through the wood-shelter,<br />Among crags in its flurry,<br />
Helter-skelter,<br />Hurry-scurry.</em> &#8212; (Robert Southly, <em>The Cataract of Lodore</em>)<br /><br /><a href="documents/cataract-of-lodore.pdf" target="_blank">Read the Entire Poem</a></p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">River, take me along<br />In your sunshine,<br />Sing me your song<br />Ever moving and winding and free<br />You rolling old river,<br />You changing old river,<br />Let's you and me river<br />Run down to the sea.</em><br />(Bill Staines, <em>River</em> (Song))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I gave my heart to the mountains the minute I stood beside this river with its spray in my face and watched it thunder into foam, smooth to green glass over sunken rocks, shatter to foam again. I was fascinated by how it sped by and yet was always there; its roar shook both the earth and me.</em> &#8212; (Wallace Stegner)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Dark brown is the river,<br />Golden is the sand.<br />It flows along for ever<br />With trees on either hand.<br /><br />Green leaves a-floating,<br />Castles of the foam,<br />Boats of mine a-boating,<br />Where will all come home?<br /><br />On goes the river<br />And out past the mill,<br />Away down the valley,<br />Away down the hill.<br /><br />Away down the river,<br />A hundred miles or more,<br />Other little children<br />Shall bring my boats ashore.</em><br />(Robert Louis Stevenson, <em>Where Go The Boats?</em> in <em>A Child's Garden of Verses and Underwoods</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">There is no music like a little river's . . . It takes the mind out-of-doors . . . and . . . it quiets a man down like saying his prayers.</em> &#8212; (Robert Louis Stevenson)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Rivers &#8211; Authors T-Z</h2>

<div class="toggle_container">

<div class="block">


<p><em style="color:#20693F">To the lost man, to the pioneer penetrating a new country, to the naturalist who wishes to see the wild land at its wildest, the advice is always the same&#8212;follow a river. The river is the original forest highway. It is nature's own Wilderness Road.</em> &#8212; (Edwin Way Teale)</p>

<p><hr align="left" width="25%"></p>


<p><em style="color:#20693F">Who hears the rippling of rivers will not utterly despair of anything.</em> &#8212; (Henry David Thoreau)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It is pleasant to have been to a place the way a river went.</em> &#8212; (Henry David Thoreau)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The life in us is like the water in the river. It may rise this year higher than man has ever known it, and flood the parched uplands; even this may be the eventful year, which will drown out all our muskrats.  It was not always dry land where we dwell. I see far inland the banks where the stream anciently washed, before science began to record its freshets.</em> &#8212; (Henry David Thoreau, <em>Walden</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I was born upon thy bank, river,<br />My blood flows in thy stream,<br />And thou meanderest forever,<br />At the bottom of my dream.</em><br />(Henry David Thoreau, Journals 1906, 1842 entry)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers must have been the guides which conducted the footsteps of the first travelers. They are the constant lure, when they flow by our doors, to distant enterprise and adventure, and, by a natural impulse, the dwellers on their banks will at length accompany their currents to the lowlands of the globe, or explore at their invitation the interior of continents.</em> &#8212; (Henry David Thoreau)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">After the doctor's departure Koznyshev felt inclined to go to the river with his fishing rod. He was fond of angling, and seemed proud of being able to like such a stupid occupation.</em> &#8212; (Leo Tolstoy, <em>Anna Karenina</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rivers are places that renew our spirit, connect us with our past, and link us directly with the flow and rhythm of the natural world.</em> &#8212; (Ted Turner, <em>The Rivers of South Carolina</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The face of the river, in time, became a wonderful book . . . which told its mind to me without reserve, delivering its most cherished secrets as clearly as if it had uttered them with a voice. And it was not a book to be read once and thrown aside, for it had a new story to tell every day.</em> &#8212; (Mark Twain, <em>Life on the Mississippi</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It was a kind of solemn, drifting down the big still river, laying on our backs looking up at the stars, and we didn't even feel like talking loud, and it wasn't often that we laughed, only a little kind of low chuckle.</em> &#8212; (Mark Twain)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A river is the most human and companionable of all inanimate things. It has a life, a character, a voice of its own, and is as full of good fellowship as a sugar-maple is of sap. It can talk in various tones, loud or low, and of many subjects grave and gay . . . For real company and friendship, there is nothing outside of the animal kingdom that is comparable to a river.</em> &#8212; (Henry van Dyke, <em>Little Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It is with rivers as it is with people: The greatest are not always the most agreeable nor the best to live with.</em> &#8212; (Henry van Dyke, <em>Little Rivers</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">May the countryside and the gliding valley streams content me. Lost to fame, let me love river and woodland.</em> &#8212; (Virgil, <em>Eclogues</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If you gave me several million years, there would be nothing that did not grow in beauty if it were surrounded by water.</em> &#8212; (Jan Erik Vold, <em>What All the World Knows</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">But I also know that in places, the river still runs deep, and though I've floated it in these places, it hasn't revealed itself in such obvious ways. I know that it might be months&#8212;years, even&#8212;before I understand what it has to teach me. I still need to give myself over to the flow and pattern and rhythm of it to learn its lessons and hear its messages. The river is inside me now, I know, and I need only wait and see where the current takes me, and what lies beneath it.</em> &#8212; (Jeff Wallach, <em>What the River Says</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It's clear to me that I will return here, as well as to other wilderness frontiers within me&#8212;whether next year or some time later&#8212;because I know that what the river says is what I need to hear:  to know myself, to feel wild again, to confront my own limits and move beyond them into the untamed country on the other side. I will return here in spite of the river's name; but I will never return the same again, and that, after all, is most clearly what the river says.</em> &#8212; (Jeff Wallach, <em>What the River Says</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">There is comfort in knowing that no matter what aspect my life takes on, this river will flow freely here, and that I might come to this place any time, in sadness or joy, alone or with someone I love. The waters will run smooth and fast, and though it will be a different river coming down out of the mountains it will also retain its constancy.</em> &#8212; (Jeff Wallach, <em>What the River Says</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Whether by snowshoe in winter or a hike in the spring, with canoe paddle, fly rod, or shotgun in the fall&#8212;to those who would listen, the river valley is a magic music box. To those who would observe, the pattern of color and movement paint a picture that is a masterwork resulting from millions of years of nature's efforts, yet dynamic and ephemeral. Minnesota is rich with stream and river resources, that beyond economic utility, make up our living environment, delight our senses, and indeed, form and mold our culture.</em> &#8212; (Tom Waters, <em>The Streams and Rivers of Minnesota</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Where can you match the mighty music of their names? &#8212; The Monongahela, the Colorado, the Rio Grande the Columbia, the Tennessee, the Hudson (Sweet Thames!); the Kennebec, the Rappahannock, the Delaware, the Penobscot, the Wabash, the Chesapeake, the Swannanoa, the Indian River, the Niagara (Sweet Afton!); the Saint Lawrence, the Susquehanna, the Tombigbee, the Nantahala, the French Broad, the Chattahoochee, the Arizona, and the Potomac (Father Tiber!)&#8212;these are a few of their princely names, these are a few of their great, proud, glittering names, fit for the immense and lonely land that they inhabit.</em> &#8212; (Thomas Wolfe, <em>Of Time and the River</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Water &#8211; Authors A-F</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Water, water, water . . . There is no shortage of water in the desert but exactly the right amount, a perfect ratio of water to rock. Of water to sand, insuring that wide, free, open, generous spacing among plants and animals, homes and towns and cities, which makes the arid West so different from any other part of the nation. There is no lack of water here, unless you try to establish a city where no city should be.</em> &#8212; (Edward Abbey, <em>Wilderness Reader</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Children of a culture born in a water-rich environment, we have never really learned how important water is to us. We understand it, but we do not respect it.</em> &#8212; (William Ashworth, <em>Nor Any Drop to Drink</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water has become a highly precious resource. There are some places where a barrel of water costs more than a barrel of oil.</em> &#8212; (Lloyd Axworthy, Foreign Minister of Canada, 1999 News Conference)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Of all our planet's activities&#8212;geological movements, the reproduction and decay of biota, and even the disruptive propensities of certain species (elephants and humans come to mind)&#8212;no force is greater than the hydrologic cycle. </em> &#8212; (Richard Bangs &amp; Christian Kallen, <em>River Gods</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The marsh, to him who enters it in a receptive mood, holds, besides mosquitoes and stagnation, melody, the mystery of unknown waters, and the sweetness of Nature undisturbed by man.</em> &#8212; (Charles William Beebe, <em>Log of the Sun</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I loved the rain as a child. I loved the sound of it on the leaves of trees and roofs and windowpanes and umbrellas and the feel of it on my face and bare legs. I loved the hiss of rubber tires on rainy streets and the flip-flop of windshield wipers. I loved the smell of wet grass and raincoats and shaggy coats of dogs. A rainy day was a special day for me in a sense that no other kind of day was&#8212;a day when the ordinariness of things was suspended with ragged skies drifting to the color of pearl and dark streets turning to dark rivers of reflected light and even people transformed somehow as the rain drew them closer by giving them something to think about together, to take common shelter from, to complain of and joke about in ways that made them more like friends than it seemed to me they were on ordinary sunny days. But more than anything, I think, I loved rain for the power it had to make indoors seem snugger and safer and a place to find refuge in from everything outdoors that was un-home, unsafe. I loved rain for making home seem home more deeply.</em> &#8212; (Frederick Buechner, <em>The Sacred Journey</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Estuaries are a happy land, rich in the continent itself, stirred by the forces of nature like the soup of a French chef; the home of myriad forms of life from bacteria and protozoan to grasses and mammals; the nursery, resting place, and refuge of countless.</em> &#8212; (Stanley Cain, 1966 Speech)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Little drops of water, little grains of sand, make the mighty ocean, and the pleasant land. So the little minutes, humble though they be, make the mighty ages of eternity.</em> &#8212; (Julia Carney, <em>Little Things</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If you could tomorrow morning make water clean in the world, you would have done, in one fell swoop, the best thing you could have done for improving human health by improving environmental quality.</em> &#8212; (William C. Clark, April 1988 Speech in Racine, Wisconsin)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">A man of wisdom delights in water.</em> &#8212; (Confucius)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We forget that the water cycle and the life cycle are one.</em> &#8212; (Jacques Cousteau, Oceanographer)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water, thou hast no taste, no color, no odor; canst not be defined, art relished while ever mysterious. Not necessary to life, but rather life itself, thou fillest us with a gratification that exceeds the delight of the senses.</em> &#8212; (Antoine de Saint-Exupery, <em>Wind, Sand and Stars</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Only those people that have directly experienced the wetlands that line the shore . . . can appreciate their mystic qualities. The beauty of rising mists at dusk, the ebb and flow of the tides, the merging of fresh and salt waters. . . .</em> &#8212; (Delaware Governor's Task Force on Marine and Coastal Affairs, "Delaware: Wetlands 1972")</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If there is magic on this planet, it is contained in water.</em> &#8212; (Loren Eiseley, "Four Quartets," in <em>The Immense Journey</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">When the well's dry, we know the worth of water.</em> &#8212; (Benjamin Franklin, <em> Poor Richard's Almanac 1746</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Water &#8211; Authors G-M</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Water is the one substance from which the earth can conceal nothing; it sucks out its innermost secrets and brings them to our very lips.</em> &#8212;  (Jean Giraudoux, <em>The Madwomen of Chaillot</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Everywhere water is a thing of beauty, gleaming in the dewdrops; singing in the summer rain; shining in the ice-gems till the leaves all seem to turn to living jewels; spreading a golden veil over the setting sun; or a white gauze around the midnight moon.</em> &#8212; (John Ballantine Gough, <em>A Glass of Water</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">There's nothing . . . absolutely nothing . . . half so much worth doing as simply messing around in boats.</em> &#8212; (Kenneth Grahame, <em>The Wind in the Willows</em> (River Rat to Mole))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Between earth and earth's atmosphere, the amount of water remains constant; there is never a drop more, never a drop less. This is a story of circular infinity, of a planet birthing itself.</em> &#8212; (Linda Hogan)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">What would the world be, once bereft of wet and wilderness?<br />Let them be left,<br />O let them be left, wilderness and wet;<br />Long live the weeds and the wilderness yet.</em> &#8212; (Gerard Manley Hopkins, "Inversnaid")</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water is God's gift to living souls, to cleanse us, to purify us, to sustain us and to renew us.</em> &#8212; (Jewish Bridal Celebration Ceremony)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If our salmon are not healthy, then our watersheds are not healthy&#8212;and if our watersheds our not healthy, then we have truly squandered our heritage and mortgaged our future.</em> &#8212; (John Kitzhaber, Governor of Oregon, Speech at "A Tale of Two Rivers:  National Conference of Trout Unlimited," August 16, 2000)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Ye marshes, how candid and simple and nothing-withholding and free,<br />Ye publish yourselves to the sky and offer yourselves to the sea.)</em> (Sidney Lanier, "The Marshes of Glynn")</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water is fluid, soft, and yielding. But water will wear away rock, which is rigid and cannot yield. As a rule, whatever is fluid, soft, and yielding will overcome whatever is rigid and hard. This is another paradox: What is soft is strong.</em> &#8212; (Lao-Tzu, Chinese Philosopher (6th century B.C.), <em>Tao Te Ching</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In the world there is nothing more submissive and weak than water. Yet for attacking that which is hard and strong nothing can surpass it.</em> &#8212; (Lao-Tzu, Chinese Philosopher (6th century B.C.), <em>Tao Te Ching</em>)</p>

<p align="center">Another Interpretation</p>

<p><em style="color:#20693F">Water flows humbly to the lowest level. Nothing is weaker than water, yet for overcoming what is hard and strong, nothing surpasses it.</em> &#8212; (Lao-Tzu, Chinese Philosopher (6th century B.C.), <em>Tao Te Ching</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water is H2O, hydrogen two parts, oxygen one, but there is also a third thing, that makes water and nobody knows what that is.</em> &#8212; (D.H. Lawrence, <em>Pansies</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water is the most critical resource issue of our lifetime and our children's lifetime. The health of our waters is the principal measure of how we live on the land.</em> &#8212; (Luna Leopold, Hydrologist)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Wetlands have a poor public image. . . . Yet they are among the earth's greatest natural assets . . . mankind's waterlogged wealth.</em> &#8212; (Edward Maltby, <em>Waterlogged Wealth</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">High-quality water is more than the dream of the conservationists, more than a political slogan; high quality water, in the right quantity at the right place at the right time, is essential to health, recreation, and economic growth. </em> &#8212; (Senator Edmund Muskie of Maine, March 1, 1966 Speech)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On Water &#8211; Authors N-Z</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">All the water there will be, is.</em> &#8212; (<em>National Geographic</em>, October 1993)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In every glass of water we drink, some of the water has already passed through fishes, trees, bacteria, worms in the soil, and many other organisms, including people . . . Living systems cleanse water and make it fit, among other things, for human consumption.</em> &#8212; (Elliot Norse, <em>Animal Extinctions</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Gutta cavat lapidem. (Dripping water hollows out a stone.)</em> &#8212; (Ovid)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water is the best of all things.</em> &#8212; (Pindar (c. 522 - c. 438 B.C.), <em>Olympian Odes</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">For many of us, water simply flows from a faucet, and we think little about it beyond this point of contact. We have lost a sense of respect for the wild river, for the complex workings of a wetland, for the intricate web of life that water supports.</em> &#8212; (Sandra Postel, <em>Last Oasis: Facing Water Scarcity</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Water is the formless potential out of which creation emerged. It is the ocean of unconsciousness enveloping the islands of consciousness. Water bathes us at birth and again at death, and in between it washes away sin. It is by turns the elixir of life or the renewing rain or the devastating flood.</em> &#8212; (Scott Russell Sanders, <em>Writing from the Center</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">I have left almost to the last the magic of water, an element which owing to its changefulness of form and mood and color and to the vast range of its effects is ever the principal source of landscape beauty, and has like music a mysterious influence over the mind.</em> &#8212; (Sir George Sitwell, <em>On the Making of Gardens</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The estuary is the point where man, the sea&#8212;his immemorial ally and adversary&#8212;and the land meet and challenge each other.</em> &#8212; (U.S. Department of the Interior, "National Estuarine Pollution Study," November 1969)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Filthy water cannot be washed.</em> &#8212; (West African Proverb)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The crisis of our diminishing water resources is just as severe (if less obviously immediate) as any wartime crisis we have ever faced. Our survival is just as much at stake as it was at the time of Pearl Harbor, or the Argonne, or Gettysburg, or Saratoga.</em> &#8212; (Representative Jim Wright of Texas, <em>The Coming Water Famine</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Green River &#8211; William Cullen Bryant</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">When breezes are soft and skies are fair,<br />I steal an hour from study and care,<br />And hide me away to the woodland scene,<br />Where wanders the stream with waters of green,<br />As it the bright fringe of herbs on its brink<br />Had given their stain to the waves they drink;<br />And they, whose meadows it murmurs through,<br />Have named the stream from its own fair hue.<br />Yet pure its waters&#8212;its shallows are bright<br />With colored pebbles and sparkles of light,<br />And clear the depths where its eddies play,<br />And dimples deepen and whirl away,<br />And the plane-tree's speckled arms o'ershoot<br />The swifter current that mines its root,<br />Through whose shifting leaves, as you walk the hill,<br />The quivering glimmer of sun and rill<br />With a sudden flash on the eye is thrown,<br />Like the ray that streams from the diamond-stone.<br />Oh, loveliest there the spring days come,<br />With blossoms, and birds, and wild-bees' hum;<br />The flowers of summer are fairest there,<br />And the freshest the breath of the summer air;<br />And sweetest the golden autumn day<br />In silence and sunshine glides away.<br />Yet, fair as thou art, thou shunnest to glide,<br />Beautiful stream! By the village side;<br />But windest away from haunts of men,<br />To quiet valley and shaded glen;<br />And forest, and meadow, and slope of hill,<br />Around thee, are lonely, lovely, and still,<br />Lonely&#8212;save when, by thy rippling tides,<br />From thicket to thicket the angler glides,<br />Or the Simpler comes, with basket and book,<br />For herbs of power on thy banks to look;<br />Or haply, some idle dreamer, like me,<br />To wander, and muse, and gaze on thee,<br />Still&#8212;save the chirp of birds that feed<br />On the river cherry and seedy reed,<br />And thy own wild music gushing out<br />With mellow murmur of fairy shout,<br />From dawn to the blush of another day,<br />Like traveler singing along his way.<br />That fairy music I never hear,<br />Nor gaze on those waters so green and clear,<br />And mark them winding away from sight,<br />Darkened with shade or flashing with light,<br />While o'er them the vine to its thicket clings,<br />And the zephyr stoops to freshen his wings,<br />But I wish that fate had left me free<br />To wander these quiet haunts with thee,<br />Till the eating cares of earth should depart,<br />And the peace of the scene pass into my heart;<br />And I envy thy stream, as it glides along<br />Through its beautiful banks in a trance of song.<br />Though forced to drudge for the dregs of men,<br />And scrawl strange words with the barbarous pen,<br />And mingle among the jostling crowd,<br />Where the sons of strife are subtle and loud&#8212;<br />I often come to this quiet place,<br />To breathe the airs that ruffle thy face,<br />And gaze upon thee in silent dream,<br />For in thy lonely and lovely stream<br />An Image of that clam life appears<br />That won my heart in my greener years.</em><br />(William Cullen Bryant, "Green River" in <em>Poems of Nature</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Down By The River &#8211; Albert Hammond</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">City life was gettin' us down,<br />So we spent a weekend out of town.<br />Pitched a tent on a patch of ground,<br />Down by the river.<br /><br />Lit a fire and drank some wine.<br />You put your jeans on top of mine.<br />I said, "Come in, the water's fine,"<br />Down by the river.<br /><br />Down by the river,<br />Down by the river.<br />I said, come in, the water's fine,<br />Down by the river.<br /><br />Didn't feel too good all night,<br />So we took a walk in the morning light.<br />And came across the strangest sight,<br />Down by the river.<br /><br />A silver fish lay on its side.<br />It was washed up by the early tide.<br />I wonder how it died,<br />Down by the river.<br /><br />Down by the river.<br />Down by the river.<br />Silver fish lay on its side,<br />Down by the river.<br /><br />Doctor put us both to bed,<br />He dosed us up, and he shook his head.<br />"Only foolish people go," he said,<br />"Down by the river."<br /><br />"Why do willows weep?" said he,<br />"Because they're dying gradually,<br />From the waste from the factories,<br />Down by the river."<br /><br />Down by the river,<br />Down by the river.<br />"Why do willows weep?" said he,<br />"Down by the river."<br /><br />In time, the river banks will die,<br />The reeds will wilt, and the ducks won't fly.<br />There'll be a tear in the otter's eye,<br />Down by the river.<br /><br />The banks will soon be black and dead,<br />And where the otter raised his head,<br />Will be a clean, white skull instead,<br />Down by the river.<br /><br />Down by the river,<br />Down by the river.<br />The banks will soon be black and dead,<br />Down by the river.<br /><br />Down by the river,<br />Down by the river.<br />The banks will soon be black and dead,<br />Down by the river.</em><br />(Lyrics by Albert Hammond and Mike Hazlewood, Sung by Albert Hammond (1972) - <a href="http://youtube.com/watch?v=U6-IDU1MgFM&mode=related&search=" target="_blank">YouTube</a>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Water Trick Stone &#8211; Duane Short</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Lichen part, line hero's path, as crowd bow low, defy powrs' wrath<br />Glisten, listen, babble not ~ hardened sandstone ~ all for naught<br /><br />Indi-go tree so high ~ flutters, shutters limb on sky<br />Black an' blue beak an' wing never knew so great a thing<br /><br />Tree where perched silent sway, wave g'bye . . . 'nother day<br />Leaf to root and back again, more than thirst, soil so thin<br /><br />Droplet born trickle grow ~ pure an' healthy water flow<br />Stubborn, gritty, harsh and hard, easy, fluid, gentle, soft ~ wisdom time, waits aloft <br /><br />Moon, sun an' stars oversee peculiar journey ta yon sea<br /><br />Tick o'day an' tock o'night ~ pow'r o'rock an' peace o'flight<br />Up ta down the excess flows ~ straight an' narrow, crooked rows<br /><br />Days of dry an' nights of wet ~ flow continues broken yet<br />Weeks an' years of battle wage ~ stone and water slowly age<br /><br />Blister sun an' biting frost ~ battle won and battle lost<br />Cedar tree where indi-go ~ taller now but, oh, so slow<br /><br />Decades gone and scent-trees two ~ hard an' soft both old, yet new<br />Stubborn stone by day by night ~ resists water's gentle flight<br /><br />Day by day an' year by year ~ stone so strong thinks, "still here"<br />Water gentle day an' night ~ touch so soft, so smooth, so light<br /><br />Which will give as eons age ~ make indi-go and cedar wage<br />Who will have its way today?  Who will leave its mark. . .  to stay?<br /><br />Cedar old and indi-go ~ cycle come and cycle go<br />Pass their being ring to ring, song ta song an' song ta sing<br /><br />Root grow deep into stone ~ feather float to seas unknown<br />Timeless process beyond man ~ beyond his will to understand<br /><br />Law of Nature will not break, nor bend, nor melt, nor one bit shake<br />Laws of Nature man seeks to break ~ though never, ever exception she make<br /><br />Law of Nature will endure ~ outlast human thought . . . for sure<br />Mankind honors pow'r an' might ~ wages war an' loves the fight<br /><br />Nature honors peace and calm ~ in depth, in space, no hint of qualm<br />Nature seeks out entropy ~ divides herself so selflessly<br /><br />Nature shares her space an' time ~ altruism most sublime<br />Millennia now in their genes ~ Indi-go and cedar gleans<br /><br />Knowledge now in core an' marrow ~ tree and bird opinions narrow<br />Which will win is now quite clear ~ though human heart an' mind find queer<br /><br />Stone so rigid, harsh an' hard ~ so powerful, so strong . . . so scarred<br />Does not win as man does bet ~ not then, not now, not ever . . . yet<br /><br />Water gentle, soft an' light ~ victory's mark a gorgeous sight<br />Channels cut so deep an' smooth ~ victor's badge, a sandstone groove<br /><br />Water gentle, battle won ~ but won in peace under sun<br />Carried gently one by one stones sands free on beaches run<br /><br />Run on surf an' run on wind ~ grain o'sand . . . now water's friend<br />Gentle battle but a myth ~ when sand did water carry with<br /><br />Stubborn pow'r cannot win ~ gentle wars that never en . . .<br />Tree an' indi-go together ~ learn the lesson even weather<br /><br />Even weather man does not ~ respect Her Laws . .  Laws Nature wrought.</em><br />(Duane Short, April 21, 2002, <em>Water Trick Stone</em><br />~  Inspired By  ~ This Past Week's Harsh and Gentle Environmental Events Leading to and Surrounding Earth Day ~  Dedicated To  ~ The Power of Peace & Environmentalists Everywhere)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">The Brook &#8211; Alfred Lord Tennyson</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">I come from haunts of coot and hern,<br />I make a sudden sally,<br />And sparkle out among the fern,<br />To bicker down a valley.<br /><br />By thirty hills I hurry down,<br />Or slip between the ridges,<br />By twenty thorps, a little town,<br />And half a hundred bridges.<br /><br />Till last by Philip's farm I flow <br />To join the brimming river,<br />For men may come and men may go,<br />But I go on forever.<br /><br />I chatter over stony ways,<br />In little sharps and trebles,<br />I bubble into eddying bays,<br />I babble on the pebbles.<br /><br />With many a curve my banks I fret <br />By many a field and fallow,<br />And many a fairy foreland set <br />With willow-weed and mallow.<br /><br />I chatter, chatter, as I flow<br />To join the brimming river,<br />For men may come and men may go,<br />But I go on forever.<br /><br />I wind about, and in and out,<br />With here a blossom sailing,<br />And here and there a lusty trout,<br />And here and there a grayling,<br /><br />And here and there a foamy flake <br />Upon me, as I travel<br />With many a silver water-break <br />Above the golden gravel,<br /><br />And draw them all along, and flow <br />To join the brimming river,<br />For men may come and men may go,<br />But I go on forever.<br /><br />I steal by lawns and grassy plots,<br />I slide by hazel covers;<br />I move the sweet forget-me-nots <br />That grow for happy lovers.<br /><br />I slip, I slide, I gloom, I glance,<br />Among my skimming swallows;<br />I make the netted sunbeam dance <br />Against my sandy shallows.<br /><br />I murmur under moon and stars <br />In brambly wildernesses;<br />I linger by my shingly bars;<br />I loiter round my cresses;<br /><br />And out again I curve and flow <br />To join the brimming river,<br />For men may come and men may go,<br />But I go on forever.</em><br />(Alfred Lord Tennyson, <em>The Brook</em> from <em>English Poems</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On The Environment &#8211; Authors A-D</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Do not burn yourselves out. Be as I am&#8212;a reluctant enthusiast . . . a part-time crusader, a half-hearted fanatic. Save the other half of yourselves and your lives for pleasure and adventure. It is not enough to fight for the land; it is even more important to enjoy it. While you can. While it's still here. So get out there and hunt and fish and mess around with your friends, ramble out yonder and explore the forests, climb the mountains, bag the peaks, run the rivers, breathe deep of that yet sweet and lucid air, sit quietly for a while and contemplate the precious stillness, the lovely, mysterious, and awesome space. Enjoy yourselves, keep your brain in your head and your head firmly attached to the body, the body active and alive, and I promise you this much: I promise you this one sweet victory over those desk-bound men with their hearts in a safe deposit box and their eyes hypnotized by desk calculators. I promise you this: you will outlive the bastards.</em> &#8212; (Edward Abbey)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Growth for the sake of growth is the ideology of the cancer cell.</em> &#8212; (Edward Abbey, "Money, Et Cetera'' in <em>A Voice Crying in the Wilderness</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Let us have a splendid legacy for our children . . . let us turn to them and say 'this you inherit and guard it well, for it is far more precious than money . . . and once it is destroyed, nature's beauty cannot be repurchased at any price.'</em> &#8212; (Ansel Adams, Speech in Monterey County (Date Unknown))</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In the 19th century, we devoted our best minds to exploring nature. In the 20th century, we devoted ourselves to controlling and harnessing it. In the 21st century, we must devote ourselves to restoring it.</em> &#8212; (Stephen Ambrose, Historian)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The frog does not drink up the pond in which he lives.</em> &#8212; (American Indian Proverb, Quoted in David Zwick, <em>Water Wasteland</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">You cannot affirm the power plant and condemn the smokestack, or affirm the smoke and condemn the cough.</em> &#8212; (Wendell Berry, <em>The Gift of the Good Land</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Those who contemplate the beauty of the earth find reserves of strength that will endure as long as life lasts.</em> &#8212; (Rachel Carson)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Conservation is a cause that has no end. There is no point at which we will say our work is finished.</em> &#8212; (Rachel Carson, On accepting the Audubon Medal (National Audubon Society) in 1964.)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It is a curious situation that the sea, from which life first arose should now be threatened by the activities of one form of that life. But the sea, though changed in a sinister way, will continue to exist; the threat is rather to life itself.</em> &#8212; (Rachel Carson, <em>The Sea Around Us</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Only within the moment of time represented by the present century has one species&#8212;man&#8212;acquired significant power to alter the nature of the world.</em> &#8212; (Rachel Carson, <em>Silent Spring</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We are not truly civilized if we concern ourselves only with the relation of man to man. What is important is the relation of man to all life.</em> &#8212; (Rachel Carson, <em>Silent Spring</em> paraphrasing Dr. Albert Schweitzer)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If I live to be old enough, I may sit down under some bush, the last left in the utilitarian world, and feel thankful that intellect in its march has spared one vestige of the ancient forest for me to die by.</em> &#8212; (Painter Thomas Cole to his patron, Luman Reed [*Smithsonian, 1994])</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The air, the water and the ground are free gifts to man and no one has the power to portion them out in parcels. Man must drink and breathe and walk and therefore each man has a right to his share of each.</em> &#8212; (James Fennimore Cooper, <em>The Prairie</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The oceans are the planet's last great living wilderness, man's only remaining frontier on earth, and perhaps his last chance to produce himself a rational species.</em> &#8212; (John Cullney, Wilderness Conservation Magazine, September-October 1990)</p>

<p><em style="color:#20693F">We will look upon the earth and her sister planets as being with us, not for us.</em> &#8212; (Mary Daly, "Beyond God the Father")</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The weight of our civilization has become so great, it now ranks as a global force and a significant wild card in the human future, along with the Ice Ages and other vicissitudes of a volatile and changeable planetary system.</em> &#8212; (Dianne Dumanoski, "Rethinking Environmentalism," December 13, 1998)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On The Environment &#8211; Authors E-K</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Until man duplicates a blade of grass, nature can laugh at his so-called scientific knowledge.</em> &#8212; (Thomas Edison)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The use of sea and air is common to all; neither can a title to the ocean belong to any people or private persons, forasmuch as neither nature nor public use and custom permit any possession thereof.</em> &#8212; (Elizabeth I of England)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">To the attentive eye, each moment of the year has its own beauty . . . it beholds, every hour, a picture which was never seen before, and which shall never be seen again.</em> &#8212; (Ralph Waldo Emerson)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">He who knows what sweets and virtues are in the ground, the waters, the plants, the heavens, and how to come at these enchantments, is the rich and royal man.</em> &#8212; (Ralph Waldo Emerson, <em>Essays, Second Series</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Hast thou named all the birds without a gun;<br />Loved the wood-rose, and left it on its stalk?</em> &#8212; (Ralph Waldo Emerson, <em>Forbearance</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Let us permit nature to have her way: She understands her business better than we do.</em> &#8212; (Michel Eyquem de Montaigne)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Nature provides exceptions to every rule.</em> &#8212; (Margaret Fuller, "The Dial," July 1843)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Earth provides enough to satisfy every man's need, but not every man's greed.</em> &#8212; (Mohandas K. Gandhi as quoted in EF Schumacher's <em>Small is Beautiful</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">How changed has the great Western continent become! West! Farther west, still farther west I pushed my way but the game had gone to the spirit land . . . grizzly bears, where they had once been numerous, had entirely disappeared and the weird voice of the wolf was unknown.</em> &#8212; (Parker Gilmore)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It's easy to become hopeless. So people must have hope&#8212;the human brain, the resilience of nature, the energy of young people and the sort of inspiration that you see from so many hundreds of people who tackle tasks that are impossible and never give up and succeed.</em> &#8212; (Dr. Jane Goodall)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">All is connected . . . no one thing can change by itself.</em> &#8212; (Paul Hawken, <em>The Ecology of Commerce</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">"What do you have when the last bear is gone? And the last wolf?"<br /><br />"I don't know," the young man said.  "What?"<br /><br />"Why you have safety; it is safe then to have more farms too poor to support people and more people who cannot live on the farms, and finally you have the tourists who are disappointed because they really came to see the bears."</em> &#8212; (Dion Henderson, <em>The Ninety-ninth Bear</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The famous balance of nature is the most extraordinary of all cybernetic systems. Left to itself, it is always self-regulated.</em> &#8212; (Joseph Wood Krutch, "Saturday Review," June 8, 1963)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On The Environment &#8211; Authors L-Q</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">One of the penalties of an ecological education is that one lives alone in a world of wounds.</em> &#8212; (Aldo Leopold, <em>A Sand County Almanac</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In our attempt to make conservation easy, we have made it trivial.</em> &#8212; (Aldo Leopold, <em>A Sand County Almanac</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We realize the indivisibility of the earth&#8212;its soil, mountains, rivers, forests, climate, plants, and animals&#8212;and respect it collectively not only as a useful servant but as a living being, vastly greater than ourselves in time and space&#8212;a being that was old when the morning stars sang together, and when the last of us has been gathered unto his fathers, will still be young.</em> &#8212; (Aldo Leopold, <em>Some Fundamentals of Conservation in the Southwest</em>, 1923)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Thus far we have considered the problem of conservation of land purely as an economic issue. A false front of exclusively economic determinism is so habitual to Americans in discussing public questions that one must speak in the language of compound interest to get a hearing.</em> &#8212; (Aldo Leopold, <em>Some Fundamentals of Conservation in the Southwest</em>, 1923)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We abuse land because we view it as a commodity belonging to us.  When we see land as a community to which we belong, we may begin to use it with love and respect.</em> &#8212; (Aldo Leopold, <em>A Sand County Almanac</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We console ourselves with the comfortable fallacy that a single museum piece will do, ignoring the clear dictum of history that a species must be saved in many places if it is to be saved at all.</em> &#8212; (Aldo Leopold, <em>A Sand County Almanac</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If the land mechanism as a whole is good, then every part is good, whether we understand it or not. If the biota in the course of aeons, has built something we like but do not understand, then who but a fool would discard seemingly useless parts? To keep every cog and wheel is the first precaution of intelligent tinkering.</em> &#8212; (Aldo Leopold, <em>The Round River &#8211; A Parable</em></p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">So Nature deals with us, and takes away<br />Our playthings one by one, and by the hand<br />Leads us to rest.</em><br />(Henry W. Longfellow, <em>Nature</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Have we come all this way, I wondered, only to be dismantled by our own technologies, to be betrayed by political connivance or the impersonal avarice of a corporation?</em> &#8212; (Barry Lopez, <em>Arctic Dreams</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If we use resources productively and take to heart the lessons learned from coping with the energy crisis, we face a future confronted only, as Pogo, once said, by insurmountable opportunities. The many crises facing us should be seen, then, not as threats, but as chances to remake the future so it serves all beings.)</em> &#8212; (L. Hunter Lovins and Amory B. Lovins, <em>Utne Reader Magazine</em>, November-December 1989)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Earth and Sky, Woods and Fields, Lakes and Rivers, the Mountain and the Sea, are excellent schoolmasters, and teach some of us more than we can ever learn from books.</em> &#8212; (John Lubbock, <em>The Use of Life</em></p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Rest is not idleness, and to lie sometimes on the grass under the trees on a summer's day, listening to the murmur of water, or watching the clouds float across the blue sky, is by no means waste of time.</em> &#8212; (John Lubbock, <em>The Use of Life</em></p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">He wondered what kind of blueprint beavers had for creating such a structure&#8212;or did they simply start aimlessly weaving stuff together until they had a dam? Did they even think about creating a dam? Maybe dams were simply accidents that resulted from their fooling around, much like the Army Corps of Engineers' accomplishments.</em> &#8212; (Patrick McManus, <em>The Huckleberry Murders</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">When we try to pick out anything by itself, we find it hitched to everything else in the universe.</em> &#8212; (John Muir, <em>My First Summer in the Sierra</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Thousands of tired, nerve-shaken, over-civilized people are beginning to find out that going to the mountains is going home; that wilderness is a necessity; and that mountain parks and reservations are useful not only as fountains of timber and irrigating rivers, but as fountains of life!</em> &#8212; (John Muir, <em>Our National Parks</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Wilderness itself is the basis of all our civilization. I wonder if we have enough reverence for life to concede to wilderness the right to live on?</em> &#8212; (Mardy Murie)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We will be known forever by the tracks we leave.</em> &#8212; (Native American Proverb)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The ultimate test of man's conscience may be his willingness to sacrifice something today for future generations whose words of thanks will not be heard.</em> &#8212; (Senator Gaylord Nelson of Wisconsin)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">While progress should never come to a halt, there are many places it should never come to at all.</em> &#8212; (Paul Newman for The Nature Conservancy)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The least movement is of importance to all nature. The entire ocean is affected by a pebble.</em> &#8212; (Blaise Pascal, <em>Pens&eacute;es</em>, 1670)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">The earth does not belong to any man; Man belongs to the earth. This we know.<br /><br /><p>All things are connected like the blood, which unites one family. All things are connected.<br /><br />Whatever befalls the earth befalls the sons of the earth. Man did not weave the web of life. He is merely a strand in it. What he does to the web, he does to himself. This we know.</em> &#8212; (Ted Perry, 1971, in the script for <em>Home</em> embellishing on a speech given by Chief Sealth, 1854)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Conservation means the wise use of the earth and its resources . . . for the greatest good of the greatest number for the longest time.</em> &#8212; (Gifford Pinchot, <em>Breaking New Ground</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Quotations On The Environment &#8211; Authors R-Z</h2>

<div class="toggle_container">

<div class="block">

<p><em style="color:#20693F">Leave it as it is . . . The ages have been at work on it, and man can only mar it.</em> &#8212; (Theodore Roosevelt)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">It is in man's heart that the life of nature's spectacle exists; to see it, one must feel it.</em> &#8212; (Jean-Jacques Rousseau, <em>Emile</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Nature is painting for us, day after day, pictures of infinite beauty.</em> &#8212; (John Ruskin)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Anything else you're interested in is not going to happen if you can't breathe the air and drink the water. Don't sit this one out. Do something. You are by accident of fate alive at an absolutely critical moment in the history of our planet.</em> &#8212; (Dr. Carl Sagan)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Believe one who knows: You will find something greater in woods than in books. Trees and stones will teach you that which you can never learn from masters.</em> &#8212; (St. Bernard of Clairvaux, <em>Epistles</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">In the end, our society will be defined not only by what we create but by what we refuse to destroy.</em> &#8212; (John Sawhill, The Nature Conservancy)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">One touch of nature makes the whole world kin.</em> &#8212; (William Shakespeare, <em>Troilus and Cressida</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Away, away, from men and towns,<br />To the wild wood and the downs, &#8212;<br />To the silent wilderness,<br />Where the soul need not repress<br />Its music.</em> &#8212; (Percy Bysshe Shelley, "To Jane, The Invitation")</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">If a tree falls in the forest, will you make a sound?</em> &#8212; (Homer Simpson, <em>The Simpsons</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Something will have gone out of us as a people if we ever let the remaining wilderness be destroyed, if we permit the last virgin forests to be turned into comic books and plastic cigarette cases; if we drive the few remaining members of the wild species into zoos or to extinction; if we pollute the last clear air and dirty the last clean streams and push our paved roads through the last of the silence, so that never again will Americans be free in their own country from the noise, the exhausts, the stinks of human and automotive waste. And so that never again can we have the chance to see ourselves single, separate, vertical and individual in the world, part of the environment of trees and rocks and soil, brother to the other animals, part of the natural world and competent to belong in it.</em> &#8212; (Wallace Steger, <em>Earth Prayers from Around the World</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">Let us spend one day as deliberately as Nature, and not be thrown off the track by every nutshell and mosquito's wing that falls on the rails.</em> &#8212; (Henry David Thoreau)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">We have forgotten how to be good guests, how to walk lightly on the earth as its other creatures do.</em> &#8212; (Barbara Ward, <em>Only One Earth</em>)</p>

<p><hr align="left" width="25%"></p>

<p><em style="color:#20693F">To be whole. To be complete. Wildness reminds us what it means to be human, what we are connected to rather than what we are separate from.</em> &#8212; (Terry Tempest Williams, Testimony Before the Senate Subcommittee on Forest &amp; Public Lands Management Regarding the Utah Public Lands Management Act of 1995, July 13, 1995)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Excerpt From <em>The Lorax</em> &#8211; Dr. Seuss</h2>

<div class="toggle_container">

<div class="block">

<p style="color:#20693F"><em>He snapped, "I'm the Lorax who speaks for the trees<br />which you seem to be chopping as fast as you please.<br />But I'm also in charge of the Brown Bar-ba-loot suits<br />who play in the shade in their Bar-ba-loot suits<br />and happily lived, eating Truffula Fruits."</p>

<p style="color:#20693F">"NOW . . . thanks to your hacking my trees to the ground<br />there's not enough Truffula Fruit to go 'round.<br />And my poor Bar-ba-loots are all getting the crummies<br />because they have gas, and no food, in their tummies!"</p>

<p style="color:#20693F">"They loved living here.  But I can't let them stay.<br />They have to find food. And I hope that they may.<br />Good luck, boys," he cried. And he sent them away.</p>

<p style="color:#20693F">. . .</p>

<p style="color:#20693F">"You're glumping the pond where the Humming-Fish hummed!<br />No more can they hum, for their gills are all gummed.<br />So I'm sending them off. Oh their future is dreary.<br />They'll walk on their fins and get woefully weary in search of some water that isn't so smeary."</p>

<p style="color:#20693F">. . .</p>

<p style="color:#20693F">And at that very moment, we heard a loud whack!<br />From outside in the fields came a sickening smack<br />of an axe on a tree.  Then we heard the tree fall.<br />The very last Truffula Tree of them all!</p>

<p style="color:#20693F">. . .</p>

<p style="color:#20693F">The Lorax said nothing.  Just gave me a glance . . .<br />just gave me a very sad, sad backward glance . . .<br />as he lifted himself by the seat of the pants.<br />And I'll never forget the grim look on his face<br />when he heisted himself and took leave of this place,<br />through a hole in the smog, without leaving a trace.</p>

<p style="color:#20693F">. . .</p>

<p style="color:#20693F">"But now " says the Once-ler,<br />"Now that you're here,<br />the word of the Lorax seems perfectly clear.<br />UNLESS someone like you<br />cares a whole awful lot,<br />nothing is going to get better.<br />It's not."</p>

<p style="color:#20693F">"SO . . .<br />Catch!" calls the Once-ler.<br />He lets something fall.<br />"It's a Truffula Seed.<br />It's the last one of all!<br />You're in charge of the last Truffula Seeds.<br />And Truffula Trees are what everyone needs.<br />Plant a new Truffula.  Treat it with care.<br />Give it clean water.  And feed it fresh air.<br />Grow a forest.  Protect it from axes that hack.<br />Then the Lorax<br />and all of his friends<br />may come back."</em><br />(Dr. Seuss, <em>The Lorax</em>)</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

</div>

<!--END #accords -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>