<?php

// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Wild and Scenic River Training';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'Training materials for implementation of the Wild &amp; Scenic Rivers Act.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Training Materials</h2>
<p>These materials are for reference only and have not been updated since their creation. Please check with your agencies lead if there is any doubt.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/sockeye-salmon.jpg" alt="" width="565" height="250" /><br /><em style="font-size:11px">"School" (Get It?) of Sockeye Salmon by Peter Gordon</em></a></center>

<div id="lower-content">

<div id="lc-left">
<table width="100%" border="0" cellpadding="10 px">
<tbody>
<tr>
<th scope="col" width="40%" align="left" valign="middle">Course Title</th>
<th scope="col" width="35%" align="center" valign="middle">Original File<br />(.doc, .ppt, etc.)</th>
<th scope="col" width="25%" align="center" valign="middle">PDF</th>
</tr>
<tr>
<td align="left" valign="middle">Wild &amp; Scenic River Study Process 1</td>
<td align="center" valign="middle"><a href="training/wsr-process-1.ppt">Powerpoint</a></td>
<td align="center" valign="middle"><a href="training/wsr-process-1.pdf" target="_blank">PDF</a></td>
</tr>
<tr>
<td align="left" valign="middle">Wild &amp; Scenic River Study Process 2</td>
<td align="center" valign="middle"><a href="training/wsr-process-2.ppt">Powerpoint</a></td>
<td align="center" valign="middle"><a href="training/wsr-process-2.pdf" target="_blank">PDF</a></td>
</tr>
<tr>
<td align="left" valign="middle">Wild &amp; Scenic River Study Process 3</td>
<td align="center" valign="middle"><a href="training/wsr-process-3.ppt">Powerpoint</a></td>
<td align="center" valign="middle"><a href="training/wsr-process-3.pdf" target="_blank">PDF</a></td>
</tr>
<tr>
<td align="left" valign="middle">Section 7 Pre-test</td>
<td align="center" valign="middle"><a href="training/section-7-pretest.pptx">Powerpoint</a></td>
<td align="center" valign="middle"><a href="training/section-7-pretest.pdf" target="_blank">PDF</a></td>
</tr>
<tr>
<td align="left" valign="middle">Section 7 Curriculum</td>
<td align="center" valign="middle"><a href="training/section-7-curriculum.pptx">Powerpoint</a></td>
<td align="center" valign="middle"><a href="training/section-7-curriculum.pdf" target="_blank">PDF</a></td>
</tr>
<tr>
<td align="left" valign="middle">Section 7 Student Guide</td>
<td align="center" valign="middle"><a href="training/section7-student-guide.doc">Word</a></td>
<td align="center" valign="middle"><a href="training/section7-student-guide.pdf" target="_blank">PDF</a></td>
</tr>
<tr>
<td align="left" valign="middle">Section 7 Instructor Guide</td>
<td align="center" valign="middle"><a href="training/section7-instructor-guide.doc">Word</a></td>
<td align="center" valign="middle"><a href="training/section7-instructor-guide.pdf" target="_blank">PDF</a></td>
</tr>
</tbody>
</table>
</div>
<!--END #lc-left -->

<div id="block-quote">
<h4>A wise man can learn more from a foolish question than a fool can learn from a wise answer. &#8211; Bruce Lee</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>