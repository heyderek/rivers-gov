<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'About the WSR Act';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'About the Wild and Scenic Rivers Act';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- JS that controls the accordion -->
<script type="text/javascript">
$(document).ready(function(){
$(".toggle_container").hide();
$("h2.trigger").click(function(){
$(this).toggleClass("active").next().slideToggle("slow");
});
});
</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Safeguarding the Character of Our Nation's Unique Rivers</h2>
<p>The National Wild and Scenic Rivers System was created by Congress in 1968 (Public Law 90-542; 16 U.S.C. 1271 et seq.) to preserve certain rivers with outstanding natural, cultural, and recreational values in a free-flowing condition for the enjoyment of present and future generations. The Act is notable for safeguarding the special character of these rivers, while also recognizing the potential for their appropriate use and development. It encourages river management that crosses political boundaries and promotes public participation in developing goals for river protection.</p>
<blockquote style="line-height:150%;"><em>It is hereby declared to be the policy of the United States that certain selected rivers of the Nation which, with their immediate environments, possess outstandingly remarkable scenic, recreational, geologic, fish and wildlife, historic, cultural or other similar values, shall be preserved in free-flowing condition, and that they and their immediate environments shall be protected for the benefit and enjoyment of present and future generations. The Congress declares that the established national policy of dams and other construction at appropriate sections of the rivers of the United States needs to be complemented by a policy that would preserve other selected rivers or sections thereof in their free-flowing condition to protect the water quality of such rivers and to fulfill other vital national conservation purposes.</em> (Wild & Scenic Rivers Act, October 2, 1968)</blockquote>
<p>Rivers may be designated by Congress or, if certain requirements are met, the Secretary of the Interior. Each river is administered by either a federal or state agency. Designated segments need not include the entire river and may include tributaries. For federally administered rivers, the designated boundaries generally average one-quarter mile on either bank in the lower 48 states and one-half mile on rivers outside national parks in Alaska in order to protect river-related values.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<!--<center><img src="images/mgmt-plans-img.jpg" alt="" width="565px" height="210px" />	</center>-->

<!--<div id="lower-content">-->

<!--<div id="lc-left" style="width:550px">-->

<div style="padding: 0px 10px 0px 10px;">
<h2>River Classification</h2>
<p>Rivers are classified as <em>wild</em>, <em>scenic</em>, or <em>recreational</em>.</p>
<p><b>Wild River Areas &#8211;</b> Those rivers or sections of rivers that are free of impoundments and generally inaccessible except by trail, with watersheds or shorelines essentially primitive and waters unpolluted. These represent vestiges of primitive America.</p>
<p><b>Scenic River Areas &#8211;</b> Those rivers or sections of rivers that are free of impoundments, with shorelines or watersheds still largely primitive and shorelines largely undeveloped, but accessible in places by roads.</p>
<p><b>Recreational River Areas &#8211;</b> Those rivers or sections of rivers that are readily accessible by road or railroad, that may have some development along their shorelines, and that may have undergone some impoundment or diversion in the past.</p>
<p>Regardless of classification, each river in the National System is administered with the goal of protecting and enhancing the values that caused it to be designated. Designation neither prohibits development nor gives the federal government control over private property. Recreation, agricultural practices, residential development, and other uses may continue. Protection of the river is provided through voluntary stewardship by landowners and river users and through regulation and programs of federal, state, local, or tribal governments. In most cases not all land within boundaries is, or will be, publicly owned, and the Act limits how much land the federal government is allowed to acquire from willing sellers. Visitors to these rivers are cautioned to be aware of and respect private property rights.</p>
<p>The Act purposefully strives to balance dam and other construction at appropriate sections of rivers with permanent protection for some of the country's most outstanding free-flowing rivers. To accomplish this, it prohibits federal support for actions such as the construction of dams or other instream activities that would harm the river's free-flowing condition, water quality, or outstanding resource values. However, designation does not affect existing water rights or the existing jurisdiction of states and the federal government over waters as determined by established principles of law.</p>
<p>As of December 2014, the National System protects 12,734 miles of 208 rivers in 40 states and the Commonwealth of Puerto Rico; this is less than one-quarter of one percent of the nation's rivers. By comparison, more than 75,000 large dams across the country have modified at least 600,000 miles, or about 17%, of American rivers.</p>
</div>

<!--</div>
<!--END #lc-left -->

<!--<div id="block-quote">
<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis ultrices pellentesque. -Lorem</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<!--</div>
<!--END #lower-content -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>