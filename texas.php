<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Texas';
// Set the page keywords
$page_keywords = 'Texas';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Texas.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'southwest';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'TX';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Texas has approximately 184,797 miles of river, of which 191.2 miles are designated as wild &amp; scenic&#8212;1/10th of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/rio-grande-tx.php" title="Rio Grande">Rio Grande</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>