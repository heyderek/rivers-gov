<?php

// Set the page title  -- GENERAL TEMPLATE 2 

$page_title = 'Wild &amp; Scenic Rivers Information';



// Set the page keywords 

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';



// Set the page description

$page_description = 'Wild &amp; Scenic Rivers Information';



// Set the region for Sidebar Images 

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'general';



// Includes the meta data that is common to all pages

include ("includes/metascript.php");

 ?>

 

	<!-- BEGIN page specific CSS and Scripts -->



	<!-- END page specific CSS and Scripts -->
		

			<?php 

			// includes the TEMPLATE HEADER CODING -- #content-page		

			include ("includes/header.php") 

			?>

			

			<?php 

			// includes the content page top		

			include ("includes/content-head.php") 

			?>

					<div id="open-content">
               
						<p>If you're just browsing to see what's available, you might try a couple of these links:
						
						<ul>
						<li><a href="bibliography.php">Bibliography</a></li>
						<li><a href="quotations.php">Environmental Quotations</a></li>
						<li><a href="links.php">Links To Other Sources</a></li>
						<li><a href="waterfacts.php">Water Facts</a></li>
						</ul>
						
						<h2>Q &amp; A Search</h2>
						
						<p>We're willing to bet that at some point, we've answered any question you have about wild and scenic rivers.  You're free to submit your question to us, and we will respond as quickly as we can.  However, you might try searching our data base of questions we've answered in the past to see if your question can be answered right now.</p>
						
						<p>Please bear in mind that you may need to try more than one synonym to get your answer, or to find all the questions and answers you're looking for.  The way this database works is that the search engine searches for the particular word in the question, the answer, and the keywords we've identified.  However, if that word doesn't appear in any of those places, you won't get a response.  For example, if you want to know about recreation on designated rivers, you can search on the word "recreation."  However, a Q&#38;A may talk about boating, but never mention the word "recreation."  So, be creative and try different searches.  And please note that we'll be adding Q&#38;As, so please check back.</p>
            
						
						<form name="Questions" action="https://www.rivers.gov/info/q-and-a-results.cfm" method="post">
            <table width="100%" border="0" cellpadding="0" cellspacing="2">
              <tr>
                <td align="left" valign="top" nowrap>Type in a keyword (e.g., mining)</td>
                <td align="left" valign="top"><input type="text" name="quest" size="50" value="" maxlength="50"></td>
              </tr>
              <tr>
                <td width="25%" align="left" valign="top" nowrap>Or try one of these:</td>
                <td width="75%" align="left" valign="top">
      	        <select name="keyword" size="1" style="width:250px;">
                <option value="0" selected>(Common Keywords)</option>
                <option value="Acquisition">Acquisition</option>
                <option value="Benefits">Benefits</option>
                <option value="Boundaries">Boundaries</option>
                <option value="Condemnation">Condemnation</option>
                <option value="Designation">Designation</option>
                <option value="Easement">Easement</option>
                <option value="Eligibility">Eligibility</option>
                <option value="Flow">Flow</option>
                <option value="Impact">Impact</option>
                <option value="Longest">Longest River</option>
                <option value="Management">Management</option>
                <option value="Mining">Mining</option>
                <option value="Nationwide Rivers Inventory">Nationwide Rivers Inventory</option>
                <option value="Outstandingly Remarkable Values">Outstandingly Remarkable Values</option>
                <option value="Planning">Planning</option>
                <option value="Private Property">Private Property</option>
                <option value="Eligibility">Qualifications For Designation</option>
                <option value="Recreation">Recreation</option>
                <option value="Restrictions">Restrictions</option>
                <option value="Study">Study</option>
                <option value="Suitability">Suitability</option>
                <option value="Vehicles">Vehicles</option>
                <option value="Water Rights">Water Rights</option>
                <option value="Zoning">Zoning</option>
                </select></td>
              </tr>
              <tr>
                <td align="center" colspan="2"><br /><input name="submit" type="submit" value="Search">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="reset" value="Reset This Form" type="reset"></td>
              </tr>
            </table>
            </form>					
						
						
						
					</div><!-- End #open-content -->

							<div class="clear"></div> <!-- Allows for content above to be flexible -->
					
						

						

						

			<?php 

			// includes the content page bottom		

			include ("includes/content-foot.php") 

			?>			

					

<?php 

// includes the TEMPLATE FOOTER CODING -- </html> 		

include ("includes/footer.php") 

?>

				