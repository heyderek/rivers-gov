<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Montana';
// Set the page keywords
$page_keywords = 'Montana';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Montana.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'MT';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Montana has approximately 169,829 miles of river, of which 368 miles are designated as wild &amp; scenic&#8212;approximately 2/10ths of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/flathead.php" title="Flathead River">Flathead River</a></li>
<li><a href="rivers/missouri-mt.php" title="Missouri River">Missouri River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>