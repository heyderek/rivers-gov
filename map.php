<?php
// Set the page title  -- ** COLD FUSION TEMPLATE
$page_title = 'Explore Designated Rivers';
// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';
// Set the page description
$page_description = 'The National Wild and Scenic Rivers System.';
// Includes the meta data that is common to all pages

$body_class = 'nwsrs-map';

include( "includes/metascript.php" );
?>

	<!-- BEGIN page specific CSS and Scripts -->
	<link href="style/jquery.vector-map.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="scripts/jquery.vector-map.js"></script>
	<script src="scripts/world-en.js" type="text/javascript"></script>
	<script type="text/javascript" src="scripts/usa-en.js"></script>
	<script type="text/javascript">
		$(function () {
			$('#map-wrap').vectorMap({
				map: 'usa_en',
				color: '#357F44',
				hoverColor: '#2C6B39',
				backgroundColor: 'transparent',
				onRegionClick: function (event, code) {
					if (code === 'al') {
						window.location = '<?php echo $base_url; ?>alabama.php'
					}
					else if (code === 'ak') {
						window.location = '<?php echo $base_url; ?>alaska.php'
					}
					else if (code === 'ar') {
						window.location = '<?php echo $base_url; ?>arkansas.php'
					}
					else if (code === 'az') {
						window.location = '<?php echo $base_url; ?>arizona.php'
					}
					else if (code === 'ca') {
						window.location = '<?php echo $base_url; ?>california.php'
					}
					else if (code === 'co') {
						window.location = '<?php echo $base_url; ?>colorado.php'
					}
					else if (code === 'ct') {
						window.location = '<?php echo $base_url; ?>connecticut.php'
					}
					else if (code === 'de') {
						window.location = '<?php echo $base_url; ?>delaware.php'
					}
					else if (code === 'fl') {
						window.location = '<?php echo $base_url; ?>florida.php'
					}
					else if (code === 'ga') {
						window.location = '<?php echo $base_url; ?>georgia.php'
					}
					else if (code === 'hi') {
						window.location = '<?php echo $base_url; ?>hawaii.php'
					}
					else if (code === 'id') {
						window.location = '<?php echo $base_url; ?>idaho.php'
					}
					else if (code === 'il') {
						window.location = '<?php echo $base_url; ?>illinois.php'
					}
					else if (code === 'in') {
						window.location = '<?php echo $base_url; ?>indiana.php'
					}
					else if (code === 'ia') {
						window.location = '<?php echo $base_url; ?>iowa.php'
					}
					else if (code === 'ks') {
						window.location = '<?php echo $base_url; ?>kansas.php'
					}
					else if (code === 'ky') {
						window.location = '<?php echo $base_url; ?>kentucky.php'
					}
					else if (code === 'la') {
						window.location = '<?php echo $base_url; ?>louisiana.php'
					}
					else if (code === 'me') {
						window.location = '<?php echo $base_url; ?>maine.php'
					}
					else if (code === 'md') {
						window.location = '<?php echo $base_url; ?>maryland.php'
					}
					else if (code === 'ma') {
						window.location = '<?php echo $base_url; ?>massachusetts.php'
					}
					else if (code === 'mi') {
						window.location = '<?php echo $base_url; ?>michigan.php'
					}
					else if (code === 'mn') {
						window.location = '<?php echo $base_url; ?>minnesota.php'
					}
					else if (code === 'ms') {
						window.location = '<?php echo $base_url; ?>mississippi.php'
					}
					else if (code === 'mo') {
						window.location = '<?php echo $base_url; ?>missouri.php'
					}
					else if (code === 'mt') {
						window.location = '<?php echo $base_url; ?>montana.php'
					}
					else if (code === 'ne') {
						window.location = '<?php echo $base_url; ?>nebraska.php'
					}
					else if (code === 'nv') {
						window.location = '<?php echo $base_url; ?>nevada.php'
					}
					else if (code === 'ne') {
						window.location = '<?php echo $base_url; ?>nebraska.php'
					}
					else if (code === 'nh') {
						window.location = '<?php echo $base_url; ?>new-hampshire.php'
					}
					else if (code === 'nj') {
						window.location = '<?php echo $base_url; ?>new-jersey.php'
					}
					else if (code === 'nm') {
						window.location = '<?php echo $base_url; ?>new-mexico.php'
					}
					else if (code === 'ny') {
						window.location = '<?php echo $base_url; ?>new-york.php'
					}
					else if (code === 'nc') {
						window.location = '<?php echo $base_url; ?>north-carolina.php'
					}
					else if (code === 'nd') {
						window.location = '<?php echo $base_url; ?>north-dakota.php'
					}
					else if (code === 'oh') {
						window.location = '<?php echo $base_url; ?>ohio.php'
					}
					else if (code === 'ok') {
						window.location = '<?php echo $base_url; ?>oklahoma.php'
					}
					else if (code === 'or') {
						window.location = '<?php echo $base_url; ?>oregon.php'
					}
					else if (code === 'pa') {
						window.location = '<?php echo $base_url; ?>pennsylvania.php'
					}
					else if (code === 'pr') {
						window.location = '<?php echo $base_url; ?>puerto-rico.php'
					}
					else if (code === 'ri') {
						window.location = '<?php echo $base_url; ?>rhode-island.php'
					}
					else if (code === 'sc') {
						window.location = '<?php echo $base_url; ?>south-carolina.php'
					}
					else if (code === 'sd') {
						window.location = '<?php echo $base_url; ?>south-dakota.php'
					}
					else if (code === 'tn') {
						window.location = '<?php echo $base_url; ?>tennessee.php'
					}
					else if (code === 'tx') {
						window.location = '<?php echo $base_url; ?>texas.php'
					}
					else if (code === 'ut') {
						window.location = '<?php echo $base_url; ?>utah.php'
					}
					else if (code === 'vt') {
						window.location = '<?php echo $base_url; ?>vermont.php'
					}
					else if (code === 'va') {
						window.location = '<?php echo $base_url; ?>virginia.php'
					}
					else if (code === 'wa') {
						window.location = '<?php echo $base_url; ?>washington.php'
					}
					else if (code === 'wv') {
						window.location = '<?php echo $base_url; ?>west-virginia.php'
					}
					else if (code === 'wi') {
						window.location = '<?php echo $base_url; ?>wisconsin.php'
					}
					else if (code === 'wy') {
						window.location = '<?php echo $base_url; ?>wyoming.php'
					}
				}
			});
		});
	</script>

	<!-- END page specific CSS and Scripts -->

<?php
// includes the #masthead and #navmenu
include( "includes/header.php" )
?>

	<div id="map-topper"></div>

	<div id="inner-wrapper">

		<div id="map-content">
			<h1 class="mainhead-map"><!--<?php echo $page_title; ?>--></h1>
			<!--TITLE is set at the top of the page. For SEO purposes we want the <title> and Page Title to match. -->

			<div id="dropdown-box">

				<?php
				// includes the river and state dropdowns
				include( "includes/select-list.php" )
				?>

			</div>
			<!--END #dropdown-box -->

			<div class="nwsrs-storymap-callout">
				<a href="http://nps.maps.arcgis.com/apps/MapJournal/index.html?appid=ba6debd907c7431ea765071e9502d5ac">View Our<br>Story Map</a>
			</div>

			<div id="map-wrap">

			</div>
			<!--END #map-wrap -->

			<div id="map-extra">
				<a href="alaska.php" class="me-alaska"></a><!--END #me-alaska -->
				<a href="hawaii.php" class="me-hawaii"></a><!--END #me-hawaii -->
				<a href="puerto-rico.php" class="me-pr"></a><!--END #me-pr -->
			</div>
			<!--END #map-extra -->

		</div>
		<!--END #map-content -->


		<div id="footer-top"></div>

<?php include( "includes/footer.php" )
?>