<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Wisconsin';
// Set the page keywords
$page_keywords = 'Wisconsin';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Wisconsin.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';
// Includes the meta data that is common to all pages
include ("includes/metascript.php");

// Create a postal code ID for checking against.
$state_code = 'WI';

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Wisconsin has approximately 56,884 miles of river, of which 276 miles are designated as wild &amp; scenic&#8212;approximatley 1/2 of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/st-croix.php" title="St. Croix River">St. Croix River</a></li>
<li><a href="rivers/wolf.php" title="Wolf River">Wolf River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>