<?php
// Set the page title  -- GENERAL TEMPLATE 2 
$page_title = 'Congressional Record';

// Set the page keywords 
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'Congressional Record.';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->
<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Congressional Record</h2>
<p>The documents on this page are the congressional record that were used in the Council white paper, <a href="documents/wsr-act-evolution.pdf" target="_blank" style="font-weight:bold; font-style:italic">Wild &amp; Scenic Rivers Act Evolution</a>.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->

<center>
<img src="images/mallards.jpg" alt="Mallards" height="300px" title="Mallards - Chuck &amp; Grace Bartlett" width="565px" /><br /><em style="font-size:11px">Photo by Chuck &amp; Grace Bartlett</em></center>

<div id="lower-content">

<h2 style="font-size:16px; color:#000">Public Law 93-279</h2>
<p><a href="documents/record/93-279-s-921-hearing.pdf" title="S 921, 1101 &amp; 1391, Senate Hearings, July 16, 1973" target="_blank">S 921, 1101 &amp; 1391, Senate Hearings, July 16, 1973</a></p>
<p><a href="documents/record/93-279-s-921-senate-report.pdf" title="S 921, Senate Report 93-401, September 21, 1973" target="_blank">S 921, Senate Report 93-401, September 21, 1973</a></p>
<p><a href="documents/record/93-279-hr-4864-congressional-record.pdf" title="HR 4864, Congressional Record for the House, December 3, 1973" target="_blank">HR 4864, Congressional Record for the House, December 3, 1973</a></p>
<p><a href="documents/record/93-279-hr-4864-hearing.pdf" title="HR 4864, House Hearings, June 11 &amp; 12, 1973" target="_blank">HR 4864, House Hearings, June 11 &amp; 12, 1973</a></p>
<p><a href="documents/record/93-279-hr-4864-house-report.pdf" title="HR 4864, House Report 93-621, November 6, 1973" target="_blank">HR 4864, House Report 93-621, November 6, 1973</a></p>
<p><a href="documents/record/93-279-hr-9492-congressional-record-1.pdf" title="HR 9492, Congressional Record for the House, March 22, 1974" target="_blank">HR 9492, Congressional Record for the House, March 22, 1974</a></p>
<p><a href="documents/record/93-279-hr-9492-congressional-record-2.pdf" title="HR 9492, Congressional Record for the House, April 10, 1974" target="_blank">HR 9492, Congressional Record for the House, April 10, 1974</a></p>
<p><a href="documents/record/93-279-hr-9492-congressional-record-3.pdf" title="HR 9492, Congressional Record for the Senate, April 23, 1974" target="_blank">HR 9492, Congressional Record for the Senate, April 23, 1974</a></p>
<p><a href="documents/record/93-279-hr-9492-congressional-record-4.pdf" title="HR 9492, Congressional Record for the House, April 25, 1974" target="_blank">HR 9492, Congressional Record for the House, April 25, 1974</a></p>
<p><a href="documents/record/93-279-hr-9492-congressional-record-5.pdf" title="HR 9492, Congressional Record for the Senate, April 25, 1974" target="_blank">HR 9492, Congressional Record for the Senate, April 25, 1974</a></p>
<p><a href="documents/record/93-279-hr-9492-senate-report.pdf" title="HR 9492, Senate Report 93-738, March 20, 1974" target="_blank">HR 9492, Senate Report 93-738, March 20, 1974</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 93-621</h2>
<p><a href="documents/record/93-621-s-3022-congressional-record.pdf" title="S 3022, Congressional Record for the Senate, October 3, 1974" target="_blank">S 3022, Congressional Record for the Senate, October 3, 1974</a></p>
<p><a href="documents/record/93-621-s-3022-senate-report.pdf" title="S 3022, Senate Report 93-1207, October 1, 1974" target="_blank">S 3022, Senate Report 93-1207, October 1, 1974</a></p>
<p><a href="documents/record/93-621-s-3022-house-report.pdf" title="S 3022, House Report 93-1645, December 19, 1974" target="_blank">S 3022, House Report 93-1645, December 19, 1974</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 94-486</h2>
<p><a href="documents/record/94-486-s-1506-senate-report.pdf" title="S 1506, Senate Report 94-502, December 1, 1975" target="_blank">S 1506, Senate Report 94-502, December 1, 1975</a></p>
<h2 style="font-size:16px; color:#000">Public Law 95-87</h2>
<p><a href="documents/record/95-87-s-7-senate-report.pdf" title="S 7, Senate Report 95-128, May 10, 1977" target="_blank">S 7, Senate Report 95-128, May 10, 1977</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 95-625</h2>
<p><a href="documents/record/95-625-s-791-senate-report.pdf" title="S 791, Senate Report 95-514, October 20, 1977" target="_blank">S 791, Senate Report 95-514, October 20, 1977</a></p>
<p><a href="documents/record/95-625-hr-12536-congressional-record-1.pdf" title="HR 12536, Congressional Record for the House, June 26, 1978" target="_blank">HR 12536, Congressional Record for the House, June 26, 1978</a></p>
<p><a href="documents/record/95-625-hr-12536-congressional-record-2.pdf" title="HR 12536, Congressional Record for the House, October 13, 1978" target="_blank">HR 12536, Congressional Record for the House, October 13, 1978</a></p>
<p><a href="documents/record/95-625-hr-12536-house-report.pdf" title="HR 12536, House Report 95-1165, May 15, 1978" target="_blank">HR 12536, House Report 95-1165, May 15, 1978</a></p>
<p><a href="documents/record/95-625-legislative-history-national-parks-recreation-act.pdf" title="Legislative History of the National Parks &amp; Recreation Act" target="_blank">Legislative History of the National Parks &amp; Recreation Act</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 96-487</h2>
<p><a href="documents/record/96-487-hr-39-senate-report.pdf" title="HR 39, Senate Report 96-413, x, 1979" target="_blank">HR 39, Senate Report 96-413, November 14, 1979</a></p>
<p><a href="documents/record/96-487-hr-39-house-report.pdf" title="HR 39, House Report 96-97, x, 1979" target="_blank">HR 39, House Report 96-97, April 18, 1979</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 99-590</h2>
<p><a href="documents/record/99-590-s-2466-senate-report.pdf" title="S 2466, Senate Report 99-419, August 15, 1986" target="_blank">S 2466, Senate Report 99-419, August 15, 1986</a></p>
<p><a href="documents/record/99-590-hr-4350-congressional-record-1.pdf" title="HR 4350, Congressional Record for the House, April 8, 1986" target="_blank">HR 4350, Congressional Record for the House, April 8, 1986</a></p>
<p><a href="documents/record/99-590-hr-4350-congressional-record-2.pdf" title="HR 4350, Congressional Record for the Senate, September 12, 1986" target="_blank">HR 4350, Congressional Record for the Senate, September 12, 1986</a></p>
<p><a href="documents/record/99-590-hr-4350-congressional-record-3.pdf" title="HR 4350, Congressional Record for the House, October 8, 1986" target="_blank">HR 4350, Congressional Record for the House, October 8, 1986</a></p>
<p><a href="documents/record/99-590-hr-4350-congressional-record-4.pdf" title="HR 4350, Congressional Record for the Senate, October 15, 1986" target="_blank">HR 4350, Congressional Record for the Senate, October 15, 1986</a></p>
<p><a href="documents/record/99-590-hr-4350-congressional-record-5.pdf" title="HR 4350, Congressional Record for the House, October 16, 1986" target="_blank">HR 4350, Congressional Record for the House, October 16, 1986</a></p>
<p><a href="documents/record/99-590-hr-4350-house-report.pdf" title="HR 4350, House Report 99-503, March 20, 1986" target="_blank">HR 4350, House Report 99-503, March 20, 1986</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 100-534</h2>
<p><a href="documents/record/100-534-hr-900-senate-report.pdf" title="HR 900, Senate Report 100-481, August 11, 1988" target="_blank">HR 900, Senate Report 100-481, August 11, 1988</a></p>
<p><a href="documents/record/100-534-hr-900-house-report.pdf" title="HR 900, House Report 100-106, May 27, 1987" target="_blank">HR 900, House Report 100-106, May 27, 1987</a></p>
<p><a href="documents/record/100-534-hr-900-congressional-record.pdf" title="HR 900, Congressional Record for the Senate, October 7, 1988" target="_blank">HR 900, Congressional Record for the Senate, October 7, 1988</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 100-557</h2>
<p><a href="documents/record/100-557-s-2148-senate-report.pdf" title="S 2148, Senate Report 100-570, September 30, 1988" target="_blank">S 2148, Senate Report 100-570, September 30, 1988</a></p>
<p><a href="documents/record/100-557-hr-4164-house-report.pdf" title="HR 4164, House Report 100-1053, October 4, 1988" target="_blank">HR 4164, House Report 100-1053, October 4, 1988</a></p>
<p><a href="documents/record/100-557-s-2148-congressional-record.pdf" title="S 2148, Congressional Record for the Senate, October 7, 1988" target="_blank">S 2148, Congressional Record for the Senate, October 7, 1988</a></p>
<p>&nbsp;</p>
<h2 style="font-size:16px; color:#000">Public Law 106-20</h2>
<p><a href="documents/record/106-20-s-469-senate-report.pdf" title="S 469, Senate Report 105-320, September 9, 1998" target="_blank">S 461, Senate Report 105-320, September 9, 1998</a></p>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>
