<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'California';
// Set the page keywords
$page_keywords = 'California';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - California.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'CA';

include( "includes/metascript.php" );
?>

	<!-- BEGIN page specific CSS and Scripts -->

	<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include( "includes/header.php" )
?>

<?php
// includes the content page top
include( "includes/content-head.php" )
?>


	<div id="intro-box">
		<p>California has approximately 189,454 miles of river, of which 1,999.6 miles are designated as wild &amp;
			scenic&#8212;1% of the state's river miles.</p>
	</div><!--END #intro-box -->

	<!--ESRI map-->
	<?php include_once( "iframe.php" ); ?>

	<ul>
		<li><a href="rivers/amargosa.php" title="Amargosa River">Amargosa River</a></li>
		<li><a href="rivers/american-lower.php" title="American River (Lower)">American River (Lower)</a></li>
		<li><a href="rivers/american-nf.php" title="American River (North Fork)">American River (North Fork)</a></li>
		<li><a href="rivers/bautista.php" title="Bautista Creek">Bautista Creek</a></li>
		<li><a href="rivers/big-sur.php" title="Big Sur River">Big Sur River</a></li>
		<li><a href="rivers/black-butte.php" title="Black Butte River">Black Butte River</a></li>
		<li><a href="rivers/cottonwood-ca.php" title="Cottonwood Creek">Cottonwood Creek</a></li>
		<li><a href="rivers/eel.php" title="Eel River">Eel River</a></li>
		<li><a href="rivers/feather.php" title="Feather River">Feather River</a></li>
		<li><a href="rivers/fuller-mill.php" title="Fuller Mill Creek">Fuller Mill Creek</a></li>
		<li><a href="rivers/kern.php" title="Kern River">Kern River</a></li>
		<li><a href="rivers/kings.php" title="Kings River">Kings River</a></li>
		<li><a href="rivers/klamath-ca.php" title="Klamath River">Klamath River</a></li>
		<li><a href="rivers/merced.php" title="Merced River">Merced River</a></li>
		<li><a href="rivers/owens.php" title="Owens River Headwaters">Owens River Headwaters</a></li>
		<li><a href="rivers/palm-canyon.php" title="Palm Canyon Creek">Palm Canyon Creek</a></li>
		<li><a href="rivers/piru.php" title="Piru Creek">Piru Creek</a></li>
		<li><a href="rivers/san-jacinto.php" title="San Jacinto River (North Fork">San Jacinto River (North Fork)</a>
		</li>
		<li><a href="rivers/sespe.php" title="Sespe Creek">Sespe Creek</a></li>
		<li><a href="rivers/sisquoc.php" title="Sisquoc River">Sisquoc River</a></li>
		<li><a href="rivers/smith.php" title="Smith River">Smith River</a></li>
		<li><a href="rivers/trinity.php" title="Trinity River">Trinity River</a></li>
		<li><a href="rivers/tuolumne.php" title="Tuolumne River">Tuolumne River</a></li>
	</ul>

	<div class="clear"></div>
	<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include( "includes/content-foot.php" )
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include( "includes/footer.php" )
?>