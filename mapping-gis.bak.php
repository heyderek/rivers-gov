<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'Maps and GIS Mapping Files';

// Set the page keywords 
$page_keywords = 'map, maps, wild, scenic, recreational, gis';

// Set the page description
$page_description = 'Maps and GIS Mapping Files.';

// Set the region for Sidebar Images 
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
 ?>
 
	<!-- BEGIN page specific CSS and Scripts -->	
	
		<!-- JS that controls the accordion -->
			<script type="text/javascript">
					$(document).ready(function(){
						
						$(".toggle_container").hide();

						$("h2.trigger").click(function(){
							$(this).toggleClass("active").next().slideToggle("slow");
						});

					});
			</script>
		
	<!-- END page specific CSS and Scripts -->
	
		
			<?php 
			// includes the TEMPLATE HEADER CODING -- #content-page		
			include ("includes/header.php") 
			?>
			
			<?php 
			// includes the content page top		
			include ("includes/content-head.php") 
			?>
					
						<div id="intro-box">
						<h2>Maps and GIS Mapping Files</h2>
						
						<p>This page contains maps for the National Wild and Scenic Rivers System. Here, you can download a printable copy of map (you'll need access to a plotter), or download the GIS files used to create the map.</p>

<p>Please let us know if you have any problems with these files. Below the downloads are a series of Questions &amp; Answers about the GIS files. If your question is not answered here, please contact your own GIS specialist. And please note that, as with any mapping exercise, some of these files are quite large.</p>

<p>Note that you'll likely have to "right click" on the links for GIS-related files and save the file to your computer. And you may have to save the larger PDFs in the same manner. Acrobat and Acrobat Reader occasionally freeze when trying to open large PDFs directly from a web site.</p>

						</div><!--END #intro-box -->	
						
						<!-- Insert an image placeholder sized at 565 x 121 -->
						<center>
						<img src="images/muskrat-mallard.jpg" alt="Yellow-Headed Blackbirds" title="Yellow-Headed Blackbirds - Dave McCurry" width="565px" height="121px" />
						</center>
						
							<div id="accords">
							
                            <h2>Alaska Map</h2>
                            <p><a href="maps/alaska-plotting.pdf" target="_blank">Download the Alaska Printing Instructions (PDF)</a></p>
                            
							<h2 class="trigger">Alaska Maps Available for Download</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
									<p><a href="maps/alaska-072.pdf" target="_blank">Download the Alaska Map &#8211; 72 dpi (728KB PDF)</a><br /><a href="maps/alaska-150.pdf" target="_blank">Download the Alaska Map &#8211; 150 dpi (2.58MB PDF)</a><br /><a href="maps/alaska-200.pdf" target="_blank">Download the Alaska Map &#8211; 200 dpi (4.13MB PDF)</a></p>
										
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->

<hr style="margin-top:80px;"/>
<br />

							<h2>United States Map</h2>
                            <p><a href="maps/conus-plotting.pdf" target="_blank">Download the U.S. Printing Instructions (PDF)</a></p>
                            
							<h2 class="trigger">United States Maps Available for Download</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
									<p><a href="maps/conus-072.pdf" target="_blank">Download the U.S. Map &#8211; 72 dpi (1.75MB PDF)</a><br /><a href="maps/conus-150.pdf" target="_blank">Download the U.S. Map &#8211; 150 dpi (7.36MB PDF)</a><br /><a href="maps/conus-200.pdf" target="_blank">Download the U.S. Map &#8211; 200 dpi (11.74MB PDF)</a></p>
										
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->

<hr style="margin-top:80px;"/>
<br />                                                      
             
             <h2>GIS Mapping Files</h2>
             <p>Note that you'll likely have to "right click" on the links for GIS-related files and save the file to your computer. And you may have to save the larger PDFs in the same manner. Acrobat and Acrobat Reader occasionally freeze when trying to open large PDFs directly from a web site.</p>
                            
							<h2 class="trigger">GIS Mapping Files Available for Download</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
									<p><a href="maps/wsr2009_alaska.ai">Download the Alaska Base Map &#8211; Adobe Illustrator (2.88MB)</a><br /><a href="maps/wsr2009_alaska.tif">Download the Alaska Base Map &#8211; TIF (10.71MB)</a></p>

<p><a href="maps/wsr2009_conus.ai">Download the U.S. Base Map &#8211; Adobe Illustrator (6.98MB)</a><br /><a href="maps/wsr2009_conus.tif">Download the U.S. Base Map &#8211; TIF (56.11MB)</a></p>

<p><a href="maps/wsr2008_puertorico_final.psd">Download the Puerto Rico Base Map &#8211; Adobe PhotoShop (23.72MB)</a><br /><a href="maps/wsr2009_conus_puertorico.tif">Download the Puerto Rico Base Map &#8211; TIF (1.99MB)</a></p>

<p><a href="maps/gis/gis.zip">Download the GIS Files (38.3MB Zip File)</a><br /><a href="maps/gis/gis.exe">Download a Self-Extracting GIS Zip File (38.4MB)</a><br /><a href="maps/logos.zip">Download the Agency Logos for GIS (2.89MB Zip File)</a></p>
										
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->

<hr style="margin-top:80px;"/>
<br />

<h2>Mapping Questions &amp; Answers</h2>

							<h2 class="trigger">Q: Does the GIS data include the recent (April 2009) designations?</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
									<p>Yes.</p>
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            <h2 class="trigger">Q:  Which GIS layers are in the Zip file and what do they represent?</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
<p>
<ul style="margin-left:0px; list-style-type:disc;">
<li><b>WSR_CONUS.mxd</b> &#8211; An ESRI ArcMap version 9.3 map document showing the Wild and Scenic River (WSR) segments of the Continental United States (CONUS) with USGS hydrography.</li><br />
<li><b>WSR_CONUS_v92.mxd</b> &#8211; An ESRI ArcMAP version 9.2 map document showing the WSR segments of the CONUS with USGS hydrography.</li><br />
<li><b>WSR_PuertoRico.mxd</b> &#8211; An ESRI ArcMap version 9.3 map document showing the WSR segments of Puerto Rico with USGS hydrography.</li><br />
<li><b>WSR_PuertoRico_v92.mxd</b> &#8211; An ESRI ArcMap version 9.2 map document showing the WSR segments of Puerto Rico with USGS hydrography.</li><br />
<li><b>Master_Conus_WSR2009_Oct20</b> &#8211; Shapefile of the WSR CONUS segments.</li><br />
<li><b>Master_Alaska_WSR2009_Oct20</b> &#8211; Shapefile of the WSR Alaska segments.</li><br />
<li><b>hydrogl020</b> &#8211; Shapefile of the 1:2M USGS hydrography lines.</li><br />
<li><b>hydrogp020</b> &#8211; Shapefile of the 1:2M USGS hydrography points.</li><br />
<li><b>hydrogm020</b> &#8211; Text file of 1:2M USGS hydrography metadata.</li><br />
<li><b>statesp020</b> &#8211; Shapefile of the 1:2M USGS state boundary polygons.</li>
</ul>
</p>
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            <h2 class="trigger">Q:  At what scale were the data collected?</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
<p>
<ul style="margin-left:0px; list-style-type:disc;">
<li><b>Master_Conus_WSR2009_Oct20</b> &#8211; WSR segments before 2000 were collected at 1:2M scale, segments added after 2000 were generally collected at 1:24k scale.</li><br />
<li><b>MasterAlaskaWSR</b> &#8211; Collected at 1:2M scale.</li><br />
<li><b>hydrogl020</b> &#8211; Collected at 1:2M scale.</li><br />
<li><b>hydrogp020</b> &#8211; Collected at 1:2M scale.</li><br />
<li><b>statesp020</b> &#8211; Collected at 1:2M scale.</li>
</ul>
</p>
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            <h2 class="trigger">Q:  What effect will scale have on the appearance of the data?</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
<p>
Data collected at a smaller scale (1:2M) will not be as accurate as that collected at a larger scale (1:24k).  Therefore, the 1:2M scale data will not follow background data of the rivers as accurately as the larger scale data.
</p>
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            <h2 class="trigger">Q:  Who compiled the data?</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
<p>
Data were compiled by a consortium of the USGS National Atlas and the Interagency Wild and Scenic River Coordinating Council.
</p>
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
                            <h2 class="trigger">Q:  Do the shapefiles have inherent metadata?</h2>
							<div class="toggle_container">
								<div class="block">
									<!--<h2>PARAGRAPH HEADING</h2>-->
<p>
Yes, the shapefiles are Federal Geographic Data Committee metadata compliant.
</p>
								</div><!--END .block -->	
							</div> <!--END .toggle_container -->
                            
							</div> <!--END #accords -->	
						
						<div class="clear"></div> <!-- Allows for content above to be flexible -->
						
						
			<?php 
			// includes the content page bottom		
			include ("includes/content-foot.php") 
			?>			
					
<?php 
// includes the TEMPLATE FOOTER CODING -- </html> 		
include ("includes/footer.php") 
?>
				