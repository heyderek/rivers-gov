<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'Transportation &amp; Infrastructure Projects';

// Set the page keywords
$page_keywords = 'management, plan, river';

// Set the page description
$page_description = 'Wild and Scenic River management plans.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- JS that controls the accordion -->
<script type="text/javascript">
	$(document).ready(function(){
	$(".toggle_container").hide();
	$("h2.trigger").click(function(){
	$(this).toggleClass("active").next().slideToggle("slow");
	});
	});
</script>

<!-- JS that controls the slideshow -->
<script language="JavaScript1.1">
	var slideimages=new Array()
	var slidelinks=new Array()
	function slideshowimages(){
		for (i=0;i<slideshowimages.arguments.length;i++){
			slideimages[i]=new Image()
			slideimages[i].src=slideshowimages.arguments[i]
		}
	}

	function slideshowlinks(){
		for (i=0;i<slideshowlinks.arguments.length;i++)
			slidelinks[i]=slideshowlinks.arguments[i]
			}

	function gotoshow(){
		if (!window.winslide||winslide.closed)
			winslide=window.open(slidelinks[whichlink])
			else
				winslide.location=slidelinks[whichlink]
				winslide.focus()
	}

</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<!--<div id="intro-box">
<h2>Have A Management Plan For Us?</h2>
<p>Right now, we don't have a mechanism to upload plans to the site. However, if the plan is small enough, please email it to <a href="mailto:daniel_haas@fws.gov">Dan Haas</a> at <a href="mailto:daniel_haas@fws.gov">daniel_haas@fws.gov</a>.</em> Alternatively, if the plan is too large to send via email, please contact Dan for assistance in getting the plan to us. Thank you.</p>
</div>
<!--END #intro-box -->-->

<!-- Insert an image placeholder sized at 565 x 121 -->
<p><br /><br /></p>
<center><a href="javascript:gotoshow()"><img src="images/beaver-creek-test.jpg" name="slide" border=0 width=565 height=284></a>
<script>
<!--

//configure the paths of the images, plus corresponding target links
slideshowimages("images/beaver-creek-test.jpg","images/klamath-river-bridge.jpg",)
slideshowlinks()

//configure the speed of the slideshow, in miliseconds
var slideshowspeed=4000

var whichlink=0
var whichimage=0
function slideit(){
if (!document.images)
return
document.images.slide.src=slideimages[whichimage].src
whichlink=whichimage
if (whichimage<slideimages.length-1)
whichimage++
else
whichimage=0
setTimeout("slideit()",slideshowspeed)
}
slideit()

//-->
</script>
</center>

<!--<div id="lower-content">-->

<!--<div id="lc-left" style="width:550px">-->
<div style="padding: 25px 10px 0px 10px;">
<h2>Transportation &amp; Infrastructure Projects</h2>
<p>This resource is intended to provide valuable information for transportation professionals and river managers regarding transportation and infrastructure projects and how Section 7 of the Wild and Scenic Rivers Act (Act) applies. Early communication and coordination between project proponents and the river-administering agency will minimize the likelihood of project redesign, adverse Section 7 determinations, and permitting delays or denials.</p>
</div>

<!--</div><!--END #lc-left -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<!--</div>
<!--END #lower-content -->

<div id="accords">

<h2 class="trigger">Frequently Asked Questions</h2>
<div class="toggle_container">

<div class="block" style="margin-left:0px;">
<!--<h2>PARAGRAPH HEADING</h2>-->
<p>These <em>Frequently Asked Questions</em> (FAQs) is intended to be a living document that provides clear and accurate answers to common questions&#8212;or misconceptions&#8212;about the Act and its effects on transportation or infrastructure projects. The FAQs will be amended as new questions arise or further clarification is needed. For a specific question to which an answer is not provided, contact the local river manager or appropriate river-administering agency.</p>
<p style="line-height: 25px">
Acronyms &amp; Abbreviations<br />
Introduction<br />
Background</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Section 7 Case Studies</h2>
<div class="toggle_container">

<div class="block">
<!--<h2>PARAGRAPH HEADING</h2>-->
<p>In response to requests from river program managers, Council members have selected examples of Section 7 determinations for common types of water resources projects. Each is an actual determination made by river-administering agency staff from across the country. In some cases, clarifying user notes are included in individual determinations. No single example is best; however, in reviewing the range of examples provided, the practitioner will gain an understanding of how to apply the procedures outlined in the technical report.</p>
<p>Bridge Construction or Replacement<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Big Darby Creek County Road 36 Bridge Replacement<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Imnaha River Road Bridge Replacement<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sturgeon River Bridge Replacement</p>
<p>Stabilization Projects<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Little Miami River Section 14 Study Erosionidge Project<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Imnaha River Garnett Bank Protection Project<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Upper Deschutes River Mills Bioengineering Project</p>
<p>Infrastructure Projects<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Great Egg Harbor River Boat Dock<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Allegheny River Forest School Outfall Pipe<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Allegheny River Boyle Boat Ramp/Dock</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Useful Links</h2>
<div class="toggle_container">
<div class="block">
<p><strong>Nationwide Rivers Inventory (NRI)</strong><br />
<a href="https://www.nps.gov/ncrc/programs/rtca/nri/index.html" title="Nationwide Rivers Inventory" target="_blank">www.nps.gov/ncrc/programs/rtca/nri/</a><br />
The National Park Service has compiled and maintains the NRI, a register of river segments that qualify or potentially qualify as national wild, scenic or recreational river areas.</p>

<p><strong>Federal Highway Administration Environmental Review Toolkit</strong><br />
<a href="https://www.environment.fhwa.dot.gov/index.asp" title="Federal Highway Administration Environmental Review Toolkit" target="_blank">www.environment.fhwa.dot.gov</a><br />The Federal Highway Administration (FHWA) Environmental Review Toolkit is a one-stop resource for up to date information, guidance, best practices and training on transportation and environmental policy. The site provides information on environmental regulation and legislation, including the National Environmental Policy Act (NEPA), Section 4(f) of the Department of Transportation Act and the National Historic Preservation Act.</p>

<p><strong>Summary of Environmental Legislation Affecting Transportation</strong><br />
<a href="https://www.fhwa.dot.gov/environment/env_sum.cfm" title="Summary of Environmental Legislation Affecting Transportation" target="_blank">www.fhwa.dot.gov/environment/env_sum.cfm</a><br />The FHWA provides basic information on general environmental statutes affecting transportation and infrastructure projects.</p>

<p><strong>NEPA and Project Development</strong><br />
<a href="https://www.fhwa.dot.gov/federal-aidessentials/catmod.cfm?category=environm" title="NEPA and Project Development" target="_blank">www.fhwa.dot.gov/federal-aidessentials/catmod.cfm?category=environm</a><br />The FHWA provides an overview of the National Environmental Policy Act (NEPA) as applied to transportation and infrastructure projects.</p>

<p><strong>Department of Transportation Act of 1966 Section 4(f) Policy Paper</strong><br />
<a href="https://www.environment.fhwa.dot.gov/4f/4fpolicy.pdf" title="Department of Transportation Act of 1966 Section 4(f) Policy Paper" target="_blank">www.environment.fhwa.dot.gov/4f/4fpolicy.pdf</a><br />Section 4(f) established a national policy for protecting publicly owned public parks, recreation areas, wildlife/waterfowl refuges, and historic sights of local, state, or national significance, from conversion to transportation uses.</p>

<p><strong>Red Book: Synchronizing Environmental Reviews for Transportation and Other Infrastructure Projects</strong><br />
<a href="https://www.environment.fhwa.dot.gov/strmlng/Redbook_2015.pdf" title="Red Book: Synchronizing Environmental Reviews for Transportation and Other Infrastructure Projects" target="_blank">www.environment.fhwa.dot.gov/strmlng/Redbook_2015.pdf</a><br />The Red Book discusses the requirements for many regulations and functions as a “how to” for synchronizing NEPA and other regulatory reviews in relation to transportation and infrastructure projects.</p>

<p><strong>Section 404 of the Clean Water Act: Permitting Discharges of Dredge or Fill Material</strong><br />
<a href="https://www.epa.gov/cwa-404/" title="Section 404 of the Clean Water Act: Permitting Discharges of Dredge or Fill Material" target="_blank">www.epa.gov/cwa-404/</a><br />Section 404  of the Clean Water Act regulates the discharge of dredged or fill material into navigable waters, including wetlands and tributaries, of the United States.</p>

<p><strong>United States Coast Guard (USCG) Bridge Program</strong><br />
<a href="https://www.uscg.mil/hq/cg5/cg551/default.asp" title="United States Coast Guard (USCG) Bridge Program" target="_blank">www.uscg.mil/hq/cg5/cg551/</a><br />The USCG Bridge Program oversees the locations and plans of bridges and causeways constructed across navigable waters of the United States.</p>

<p><strong>Army Corps of Engineers (ACOE) Civil Works Mission</strong><br />
<a href="http://www.usace.army.mil/Missions/Civil-Works/" title="Army Corps of Engineers (ACOE) Civil Works Mission" target="_blank">www.usace.army.mil/Missions/Civil-Works/</a><br />The Civil Works division of the  USACE includes water resource development projects including flood risk management, navigation,recreation, infrastructure and environmental stewardship, and emergency response.</p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">DOTs &amp; Specifications</h2>

<div class="toggle_container">

<div class="block">
<!--<h2>PARAGRAPH HEADING</h2>-->
<table width="565px" border="1">
<tr>
<td align="center" width="25%" bgcolor="#E9E9E9"><strong><em>State</em></strong></td>
<td align="center" width="25%" bgcolor="#E9E9E9"><strong><em>DOT Website</em></strong></td>
<td align="center" width="25%" bgcolor="#E9E9E9"><strong><em>Standard Specifications &amp; Supplementary Provisions</em></strong></td>
<td align="center" width="25%" bgcolor="#E9E9E9"><strong><em>WSR/Environmental Planning</em></strong></td>
</tr>
<tr>
<td>Alabama</td>
<td><a href="http://www.dot.state.al.us" target="_blank">www.dot.state.al.us</a></td>
<td><a href="http://www.dot.state.al.us/conweb/specifications.html" target="_blank">www.dot.state.al.us/conweb/specifications.html</a></td>
<td><a href="http://www.dot.state.al.us/dsweb/div_ped/EnvironmentalCoordination/index.html" target="_blank">www.dot.state.al.us/dsweb/div_ped/EnvironmentalCoordination/</a></td>
</tr>
<tr>
<td>Alaska</td>
<td><a href="http://www.dot.state.ak.us" target="_blank">www.dot.state.ak.us</a></td>
<td><a href="http://www.dot.alaska.gov/stwddes/dcsspecs/index.shtml#" target="_blank">www.dot.alaska.gov/stwddes/dcsspecs/</a></td>
<td><a href="http://dot.alaska.gov/stwddes/desenviron/" target="_blank">dot.alaska.gov/stwddes/desenviron/</a></td>
</tr>
<tr>
<td>Arizona</td>
<td>http://www.azdot.gov/</td>
<td>http://www.azdot.gov/business/ContractsandSpecifications/Specifications</td>
<td>http://www.azdot.gov/business/environmental-planning</td>
</tr>
<tr>
<td>Arkansas</td>
<td>http://www.arkansashighways.com/</td>
<td>http://www.arkansashighways.com/standard_specifications.aspx</td>
<td>http://www.arkansashighways.com/environmental/environmental.aspx</td>
</tr>
<tr>
<td>California</td>
<td>http://www.dot.ca.gov/</td>
<td>http://www.dot.ca.gov/des/oe/construction-contract-standards.html</td>
<td>http://www.caltrans.ca.gov/ser/vol1/sec3/special/ch19wsrivers/chap19.htm</td>
</tr>
<tr>
<td>Colorado</td>
<td>http://www.codot.gov/</td>
<td>https://www.codot.gov/business/designsupport/cdot-construction-specifications/2017-construction-standard-specs</td>
<td>https://www.codot.gov/programs/environmental</td>
</tr>
<tr>
<td>Connecticut</td>
<td>http://www.ct.gov/dot</td>
<td>http://www.ct.gov/dot/cwp/view.asp?a=3609&q=430362</td>
<td>http://www.ct.gov/dot/cwp/view.asp?a=3530&Q=551478</td>
</tr>
<tr>
<td>Delaware</td>
<td>http://www.deldot.gov/</td>
<td>http://www.deldot.gov/Publications/manuals/standard_specifications/index.shtml</td>
<td>http://www.deldot.gov/information/business/drc/environmental.shtml</td>
</tr>
<tr>
<td>District of Columbia</td>
<td>http://ddot.dc.gov/</td>
<td>http://ddot.dc.gov/node/785402</td>
<td>https://ddot.dc.gov/page/environment</td>
</tr>
<tr>
<td>Florida</td>
<td>http://www.dot.state.fl.us/</td>
<td>http://www.fdot.gov/programmanagement/Implemented/SpecBooks/default.shtm</td>
<td>http://www.fdot.gov/environment/</td>
</tr>
<tr>
<td>Georgia</td>
<td>http://www.dot.ga.gov/</td>
<td>http://www.dot.ga.gov/PS/Business/Source</td>
<td>http://www.dot.ga.gov/PS/DesignManuals/EnvironmentalProcedures</td>
</tr>
<tr>
<td>Hawaii</td>
<td>http://hidot.hawaii.gov/</td>
<td>http://hidot.hawaii.gov/highways/s2005-standard-specifications/</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Idaho</td>
<td>http://itd.idaho.gov/</td>
<td>http://apps.itd.idaho.gov/apps/manuals/manualsonline.html</td>
<td>http://itd.idaho.gov/env/</td>
</tr>
<tr>
<td>Illinois</td>
<td>http://www.idot.illinois.gov/</td>
<td>http://www.idot.illinois.gov/doing-business/procurements/engineering-architectural-professional-services/Consultants-Resources/index</td>
<td>http://www.idot.illinois.gov/transportation-system/environment/</td>
</tr>
<tr>
<td>Indiana</td>
<td>http://www.in.gov/indot/</td>
<td>http://www.in.gov/dot/div/contracts/standards/book/index.html</td>
<td>http://www.in.gov/indot/2523.htm</td>
</tr>
<tr>
<td>Iowa</td>
<td>http://www.iowadot.gov/</td>
<td>http://www.iowadot.gov/specifications/new_docs.htm</td>
<td>http://www.iowadot.gov/ole/index.htm</td>
</tr>
<tr>
<td>Kansas</td>
<td>http://www.ksdot.org/</td>
<td>http://www.ksdot.org/bureaus/burConsMain/specprov/specifications.asp</td>
<td>https://www.ksdot.org/bureaus/burRow/Environmental/default.asp</td>
</tr>
<tr>
<td>Kentucky</td>
<td>http://transportation.ky.gov/</td>
<td>http://transportation.ky.gov/Construction/Pages/Standard-Specification-with-Supplemental-Specification-Included.aspx</td>
<td>http://transportation.ky.gov/Environmental-Analysis/Pages/default.aspx</td>
</tr>
<tr>
<td>Louisiana</td>
<td>http://wwwsp.dotd.la.gov/</td>
<td>http://wwwsp.dotd.la.gov/Inside_LaDOTD/Divisions/Engineering/Standard_Specifications/Pages/Standard%20Specifications.aspx</td>
<td>http://wwwsp.dotd.la.gov/Inside_LaDOTD/Divisions/Engineering/Environmental/Pages/default.aspx</td>
</tr>
<tr>
<td>Maine</td>
<td>http://www.maine.gov/mdot/</td>
<td>http://maine.gov/mdot/contractors/publications/standardspec/</td>
<td>http://maine.gov/mdot/env/</td>
</tr>
<tr>
<td>Maryland</td>
<td>http://www.mdot.maryland.gov/</td>
<td>http://www.roads.maryland.gov/Index.aspx?PageId=44</td>
<td>http://www.mdot.maryland.gov/newMDOT/Environmental_Programs/index.html</td>
</tr>
<tr>
<td>Massachusetts</td>
<td>http://www.massdot.state.ma.us/</td>
<td>http://www.massdot.state.ma.us/highway/DoingBusinessWithUs/ManualsPublicationsForms.aspx</td>
<td>http://www.mass.gov/eea/agencies/dfg/der/technical-assistance/wild-and-scenic-rivers.html</td>
</tr>
<tr>
<td>Michigan</td>
<td>http://www.michigan.gov/mdot/</td>
<td>http://mdotcf.state.mi.us/public/specbook/2012/</td>
<td>http://www.michigan.gov/dnr/0,4570,7-153-10364_52259_31442---,00.html</td>
</tr>
<tr>
<td>Minnesota</td>
<td>http://www.dot.state.mn.us/</td>
<td>http://www.dot.state.mn.us/pre-letting/spec/index.html</td>
<td>http://www.dot.state.mn.us/environment/</td>
</tr>
<tr>
<td>Mississippi</td>
<td>http://mdot.ms.gov/</td>
<td>http://sp.mdot.ms.gov/Construction/Pages/Standard%20Specifications.aspx</td>
<td>http://sp.mdot.ms.gov/Environmental/Pages/Home.aspx</td>
</tr>
<tr>
<td>Missouri</td>
<td>http://www.modot.org/</td>
<td>http://www.modot.org/business/standards_and_specs/highwayspecs.htm</td>
<td>http://modot.mo.gov/ehp/EnvironmentalStudies.htm</td>
</tr>
<tr>
<td>Montana</td>
<td>http://www.mdt.mt.gov/</td>
<td>https://www.mdt.mt.gov/business/contracting/standard_specs.shtml</td>
<td>http://www.mdt.mt.gov/publications/docs/manuals/env/Chapter%2035%20WILD%20AND%20SCENIC%20RIVERS.pdf</td>
</tr>
<tr>
<td>Nebraska</td>
<td>http://www.transportation.nebraska.gov/</td>
<td>http://roads.nebraska.gov/media/6897/specbook-2007.pdf</td>
<td>http://www.roads.nebraska.gov/projects/environment/</td>
</tr>
<tr>
<td>Nevada</td>
<td>http://www.nevadadot.com/</td>
<td>https://www.nevadadot.com/doing-business/about-ndot/ndot-divisions/engineering/design/standard-specifications-and-plans</td>
<td>http://www.nevadadot.com/doing-business/about-ndot/ndot-divisions/engineering/environmental-services</td>
</tr>
<tr>
<td>New Hampshire</td>
<td>http://www.nh.gov/dot/</td>
<td>http://www.nh.gov/dot/org/projectdevelopment/highwaydesign/specifications/index.htm</td>
<td>http://www.nh.gov/dot/org/projectdevelopment/environment/index.htm</td>
</tr>
<tr>
<td>New Jersey</td>
<td>http://www.state.nj.us/transportation/</td>
<td>http://www.state.nj.us/transportation/eng/specs/2007/Division.shtml</td>
<td>http://www.state.nj.us/transportation/eng/Environmental/</td>
</tr>
<tr>
<td>New Mexico</td>
<td>http://dot.state.nm.us/</td>
<td>http://dot.state.nm.us/content/nmdot/en/Standards.html</td>
<td>http://dot.state.nm.us/content/nmdot/en/environmental_development_Publications.html</td>
</tr>
<tr>
<td>New York</td>
<td>http://www.nysdot.gov/</td>
<td>https://www.dot.ny.gov/main/business-center/engineering/specifications/busi-e-standards-usc</td>
<td>https://www.dot.ny.gov/divisions/engineering/environmental-analysis</td>
</tr>
<tr>
<td>North Carolina</td>
<td>http://www.ncdot.gov/</td>
<td>https://connect.ncdot.gov/resources/Specifications/Pages/Specifications-and-Special-Provisions.aspx</td>
<td>https://www.ncdot.gov/programs/environmental/</td>
</tr>
<tr>
<td>North Dakota</td>
<td>http://www.dot.nd.gov/</td>
<td>http://www.dot.nd.gov/dotnet/supplspecs/StandardSpecs.aspx</td>
<td>https://www.dot.nd.gov/business/construction-engineering.htm</td>
</tr>
<tr>
<td>Ohio</td>
<td>http://www.dot.state.oh.us/</td>
<td>http://www.dot.state.oh.us/Divisions/ConstructionMgt/OnlineDocs/Pages/default.aspx</td>
<td>http://watercraft.ohiodnr.gov/scenicrivers</td>
</tr>
<tr>
<td>Oklahoma</td>
<td>http://www.okladot.state.ok.us/</td>
<td>https://ok.gov/odot/Doing_Business/Construction/Construction_Engineering_-_Standards,_Specifications,_Materials_and_Testing/index.html</td>
<td>https://ok.gov/odot/Programs_and_Projects/Environmental/</td>
</tr>
<tr>
<td>Oregon</td>
<td>http://www.oregon.gov/odot/</td>
<td>http://www.oregon.gov/ODOT/Business/Pages/Standard_Specifications.aspx</td>
<td>http://www.oregon.gov/ODOT/HWY/GEOENVIRONMENTAL/pages/index.aspx</td>
</tr>
<tr>
<td>Pennsylvania</td>
<td>http://www.penndot.gov/</td>
<td>http://www.penndot.gov/ProjectAndPrograms/Construction/Pages/ConstructionSpecifications.aspx</td>
<td>http://www.penndot.gov/ProjectAndPrograms/RoadDesignEnvironment/Environment/Pages/default.aspx</td>
</tr>
<tr>
<td>Puerto Rico</td>
<td>http://www.dtop.gov.pr/</td>
<td>http://www.dtop.gov.pr/carretera/det_content.asp?cn_id=271</td>
</tr>
<tr>
<td>Rhode Island</td>
<td>http://www.dot.ri.gov/</td>
<td>http://www.dot.ri.gov/business/bluebook.php</td>
<td>http://www.dot.ri.gov/about/stormwater.php</td>
</tr>
<tr>
<td>South Carolina</td>
<td>http://www.dot.state.sc.us/</td>
<td>http://www.dot.state.sc.us/doing/construction_StandardSpec.aspx</td>
<td>http://www.scdot.org/inside/environmentalservices.aspx</td>
</tr>
<tr>
<td>South Dakota</td>
<td>http://www.sddot.com/</td>
<td>http://www.sddot.com/business/contractors/specs/2015specbook/Default.aspx</td>
<td>http://www.sddot.com/business/environmental/Default.aspx</td>
</tr>
<tr>
<td>Tennessee</td>
<td>http://www.tn.gov/tdot</td>
<td>http://www.tn.gov/tdot/article/transportation-construction-2015-standard-specifications</td>
<td>http://www.tn.gov/tdot/section/environmental-home</td>
</tr>
<tr>
<td>Texas</td>
<td>http://www.txdot.gov/</td>
<td>http://www.txdot.gov/inside-txdot/division/construction/txdot-specifications.html</td>
<td>http://www.txdot.gov/inside-txdot/division/environmental/compliance-toolkits.html</td>
</tr>
<tr>
<td>Utah</td>
<td>http://www.udot.utah.gov/</td>
<td>http://www.udot.utah.gov/main/f?p=100:pg:0:::1:T,V:4715,</td>
<td>http://www.udot.utah.gov/main/f?p=100:pg:0:::1:T,V:241,</td>
</tr>
<tr>
<td>Vermont</td>
<td>http://vtrans.vermont.gov/</td>
<td>http://vtrans.vermont.gov/contract-admin/construction/2011-standard-specifications</td>
<td>http://vtrans.vermont.gov/environmental-manual/permitting/wetlands/wild-and-scenic-rivers</td>
</tr>
<tr>
<td>Virginia</td>
<td>http://www.virginiadot.org/</td>
<td>http://www.virginiadot.org/business/const/spec-default.asp</td>
<td>http://www.virginiadot.org/programs/pr-environmental.asp</td>
</tr>
<tr>
<td>Washington</td>
<td>http://www.wsdot.wa.gov/</td>
<td>http://www.wsdot.wa.gov/Publications/Manuals/M41-10.htm</td>
<td>https://www.wsdot.wa.gov/NR/rdonlyres/56B21F1C-718C-4B4F-9F11-C530EAE734E8/0/LU_WildScenicRivers.pdf</td>
</tr>
<tr>
<td>West Virginia</td>
<td>http://www.transportation.wv.gov/</td>
<td>http://www.transportation.wv.gov/highways/engineering/Pages/Specifications.aspx</td>
<td>http://www.transportation.wv.gov/highways/programplanning/planning/Pages/EnvironmentalPlanning.aspx</td>
</tr>
<tr>
<td>Wisconsin</td>
<td>http://wisconsindot.gov/</td>
<td>http://wisconsindot.gov/Pages/doing-bus/eng-consultants/cnslt-rsrces/rdwy/stndspec.aspx</td>
<td>http://wisconsindot.gov/Pages/doing-bus/eng-consultants/cnslt-rsrces/environment/default.aspx</td>
</tr>
<tr>
<td>Wyoming</td>
<td>http://www.dot.state.wy.us/</td>
<td>http://www.dot.state.wy.us/home/engineering_technical_programs/manuals_publications/2010_Standard_Specifications.html</td>
<td>http://www.dot.state.wy.us/home/engineering_technical_programs/environmental_services.html</td>
</tr>
<tr>
<td align="center" colspan="4" bgcolor="#E9E9E9">Federal Agencies</td>
</tr>
<tr>
<td align="center" colspan="4">Federal Highway Department (FHWA)</td>
</tr>
<tr>
<td>Federal Lands Highway Division, Roadway Maintenance Manual<br /><a href="https://www.nps.gov/transportation/pdfs/Roadway_Maintenance_Manual.pdf" target="_blank">www.nps.gov/transportation/pdfs/Roadway_Maintenance_Manual.pdf</a></td>
<td>www.fhwa.dot.gov</td>
<td>Federal Highway Standard Specifications<br /><a href="https://flh.fhwa.dot.gov/resources/specs/" target="_blank">flh.fhwa.dot.gov/resources/specs/</a></td>
<td>Environmental Information<br /><a href="https://www.fhwa.dot.gov/resources/topics/environment.cfm" target="_blank">www.fhwa.dot.gov/resources/topics/environment.cfm</a></td>
</tr>
<tr>
<td align="center" colspan="4">National Park Service (NPS)</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>Transportation Planning Guidebook<br />
<a href="https://www.nps.gov/transportation/pdfs/transplan.pdf" target="_blank">www.nps.gov/transportation/pdfs/transplan.pdf</a></td>
</tr>
<tr>
<td align="center" colspan="4">United States Forest Service (USFS)</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>US Forest Service National Road Specifications<br />
<a href="https://www.fs.fed.us/database/acad/dev/roads/roadspecs.htm" target="_blank">www.fs.fed.us/database/acad/dev/roads/roadspecs.htm</a></td>
<td>Federal Surface Transportation Programs and Transportation Planning for Federal Land Management Agencies<br />
<a href="https://www.fs.fed.us/t-d/pubs/pdf/07771814.pdf" target="_blank">www.fs.fed.us/t-d/pubs/pdf/07771814.pdf</a></td>
</tr>
<tr>
<td align="center" colspan="4">United States Fish and Wildlife Service (USFWS)</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>Transportation Planning<br />
<a href="https://www.fws.gov/ecological-services/energy-development/transportation.html" target="_blank">www.fws.gov/ecological-services/energy-development/transportation.html</a></td>
</tr>
</table>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Eco-Friendly Construction</h2>

<div class="toggle_container">

<div class="block">
<!--<h2>PARAGRAPH HEADING</h2>-->
<p>Transportation and infrastructure projects should incorporate eco-friendly geotextiles and construction materials, whenever possible, to prevent degradation of river values and water quality. Biodegradable, durable, natural materials, such as coir, jute, sisal and hemp, reduce air and water pollution and are more aesthetically pleasing than conventional materials. Listed below are several eco-friendly material and product options for transportation and infrastructure projects.</p>
<p style="line-height: 25px">
<br /></p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

</div>
<!--END #accords -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>
