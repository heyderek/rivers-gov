<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'Wild &amp; Scenic River Act Amendments';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'Wild &amp; Scenic Rivers Act and amendments.';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->
<!-- JS that controls the accordion -->
<script type="text/javascript">
$(document).ready(function(){
$(".toggle_container").hide();
$("h2.trigger").click(function(){
$(this).toggleClass("active").next().slideToggle("slow");
});
});
</script>
<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Wild &amp; Scenic Rivers Act, Amendments & Section 2(a)(ii) Additions</h2>
<p>The files below represent the original Wild &amp; Scenic Rivers Act, as amended, and the individual amendments since 1968.  The amendments to the Wild &amp; Scenic Rivers Act represent 16 USC, Sections 1271-1287, amended through July 2012.  Also included are the <em>Federal Register</em> notices for Section 2(a)(ii) designations, complete through July 2012.  Section 2(a)(ii) designations are not amendments to the Act; however, this was a logical place to provide those documents as they represent additions to the National Wild &amp; Scenic Rivers System on an equal basis as those rivers designated through amendments to Section 3 of the Act.  For more information on Section 2(a)(ii) designations, please see our <a href="documents/2aii.pdf" target="_blank">white paper</a>.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/beaver.jpg" alt="Beaver" height="428" title="Beaver - Pat Gaines" width="565" /><br /><a href="http://www.flickr.com/photos/33403047@N00/" target="_blank"><em style="font-size:11px">Photo by Pat Gaines</em></a></center>

<div id="lower-content">

<div id="lc-left">
<h2>HeinOnline Credit</h2>
<p>All the files below are PDFs downloaded from HeinOnline, a legal research service to which the Department of the Interior subscribes. All files are offered here with the express permission of HeinOnline, and any additional transmission beyond personal use should credit HeinOnline.</p><p>The files below require a PDF reader, such as the free Acrobat Reader from Adobe.</p>
</div>
<!--END #lc-left -->

<div id="block-quote">
<h4><em>If you grew up in the country, chances are you have fond memories of lazy days down by a river, creek or pond.</em> &#8212; Darlene Donaldson</h4>
</div>
<!--END #block-quote -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->

<div id="accords">
<h2 class="trigger">Wild &amp; Scenic Rivers Act and Amendments</h2>
<div class="toggle_container">

<div class="block">
<h3 align="center">1968</h3>
<p><a href="documents/act/complete-act.pdf" target="_blank">Original Act &amp; All Amendments</a> In Order (9.1 MB File!)</p>
<p><a href="documents/act/90-542.pdf" target="_blank">P.L. 90-542</a> &#8212; Original Legislation; Protected First 8 Rivers (Clearwater, Eleven Point, Feather, Rio Grande, Rogue, St. Croix, Salmon (Idaho), and Wolf; Identified 27 Rivers For Study</p>
<h3 align="center">1972</h3>
<p><a href="documents/act/92-560.pdf" target="_blank">P.L. 92-560</a> &#8212; Designated St. Croix</p>
<h3 align="center">1974</h3>
<p><a href="documents/act/93-279.pdf" target="_blank">P.L. 93-279</a> &#8212; Designated Chattooga</p>
<h3 align="center">1975</h3>
<p><a href="documents/act/93-621.pdf" target="_blank">P.L. 93-621</a> &#8212; Added 29 Rivers for Study (American Through Dolores)</p>
<p><a href="documents/act/94-199.pdf" target="_blank">P.L. 94-199</a> &#8212; Designated Rapid, Snake; Added Snake For Study</p>
<h3 align="center">1976</h3>
<p><a href="documents/act/94-486.pdf" target="_blank">P.L. 94-486</a> &#8212; Designated Flathead, Missouri, Obed; Added Housatonic For Study; Clarified Feather Designation</p>
<h3 align="center">1977</h3>
<p><a href="documents/act/95-87.pdf" target="_blank">P.L. 95-87</a> &#8212; Eliminated Surface Coal Mining Within Designated and Study Rivers</p>
<h3 align="center">1978</h3>
<p><a href="documents/act/95-625.pdf" target="_blank">P.L. 95-625</a> &#8212; Designated Pere Marquette, Rio Grande, Skagit, Middle and Upper Delaware, NF American, Missouri and St. Joe; Added Kern, Loxahatchee, Ogeechee, Salt, Verde, San Francisco, Fish Creek, Black Creek, Allegheny, Cacapon, Escatawpa, Myakka, Soldier Creek, Red, Bluestone, Gauley and Greenbrier For Study</p>
<h3 align="center">1979</h3>
<p><a href="documents/act/96-87-1.pdf" target="_blank">P.L. 96-87</a> &#8212; Technical Amendment 1 - Upper Delaware Advisory Council</p>
<p><a href="documents/act/96-87-2.pdf" target="_blank">P.L. 96-87</a> &#8212; Technical Amendment 2</p>
<h3 align="center">1980</h3>
<p><a href="documents/act/96-199.pdf" target="_blank">P.L. 96-199</a> &#8212; Added Birch For Study</p>
<p><a href="documents/act/96-312.pdf" target="_blank">P.L. 96-312</a> &#8212; Designated Salmon (Idaho)</p>
<p><a href="documents/act/96-487.pdf" target="_blank">P.L. 96-487</a> &#8212; ANILCA (Alaska Rivers)</p>
<h3 align="center">1984</h3>
<p><a href="documents/act/98-323.pdf" target="_blank">P.L. 98-323</a> &#8212; Added Wildcat For Study</p>
<p><a href="documents/act/98-406.pdf" target="_blank">P.L. 98-406</a> &#8212; Designated Verde</p>
<p><a href="documents/act/98-425.pdf" target="_blank">P.L. 98-425</a> &#8212; Designated Tuolumne</p>
<p><a href="documents/act/98-444.pdf" target="_blank">P.L. 98-444</a> &#8212; Designated AuSable; Allowed Lamprey Control Structures on the Pere Marquette</p>
<p><a href="documents/act/98-484.pdf" target="_blank">P.L. 98-484</a> &#8212; Added Horsepasture For Study</p>
<p><a href="documents/act/98-494.pdf" target="_blank">P.L. 98-494</a> &#8212; Designated Illinois and Owyhee; Added North Umpqua For Study</p>
<h3 align="center">1986</h3>
<p><a href="documents/act/99-530.pdf" target="_blank">P.L. 99-530</a> &#8212; Designated Horsepasture</p>
<p><a href="documents/act/99-590.pdf" target="_blank">P.L. 99-590</a> &#8212; Designated Cache la Poudre, Saline Bayou and Black Creek; Added Farmington and Great Egg Harbor For Study; Technical Amendments</p>
<p><a href="documents/act/99-663.pdf" target="_blank">P.L. 99-663</a> &#8212; Designated Klickitat and White Salmon; Added Klickitat and White Salmon For Study</p>
<h3 align="center">1987</h3>
<p><a href="documents/act/100-33.pdf" target="_blank">P.L. 100-33</a> &#8212; Added Maurice, Manumuskin and Menantico Creek For Study</p>
<p><a href="documents/act/100-149.pdf" target="_blank">P.L. 100-149</a> &#8212; Designated Merced; Added Merced For Study</p>
<p><a href="documents/act/100-150.pdf" target="_blank">P.L. 100-150</a> &#8212; Designated Kings</p>
<p>P<a href="documents/act/100-174.pdf" target="_blank">.L. 100-174</a> &#8212; Designated Kern</p>
<h3 align="center">1988</h3>
<p><a href="documents/act/100-412.pdf" target="_blank">P.L. 100-412</a> &#8212; Upper Delaware Advisory Council</p>
<p><a href="documents/act/100-534.pdf" target="_blank">P.L. 100-534</a> &#8212; Bluestone, New, Gauley, Technical Amendments</p>
<p><a href="documents/act/100-547.pdf" target="_blank">P.L. 100-547</a> &#8212; Designated Sipsey Fork of the West Fork</p>
<p><a href="documents/act/100-552.pdf" target="_blank">P.L. 100-552</a> &#8212; Technical Amendment on the Missouri</p>
<p><a href="documents/act/100-554.pdf" target="_blank">P.L. 100-554</a> &#8212; Designated Wildcat</p>
<p><a href="documents/act/100-557.pdf" target="_blank">P.L. 100-557</a> &#8212; Designated Oregon Rivers, Including Adding the Klamath For Study Under 5(d)(2)</p>
<p><a href="documents/act/100-605.pdf" target="_blank">P.L. 100-605</a> &#8212; Added Hanford Reach (Columbia River) For Study</p>
<p><a href="documents/act/100-633.pdf" target="_blank">P.L. 100-633</a> &#8212; Designated Rio Chama</p>
<p><a href="documents/act/100-668.pdf" target="_blank">P.L. 100-668</a> &#8212; Established Final Boundary Map for Klickitat River</p>
<p><a href="documents/act/100-677.pdf" target="_blank">P.L. 100-677</a> &#8212; Prohibited Dams Proximate To the Snake and Salmon Designations</p>
<h3 align="center">1989</h3>
<p><a href="documents/act/101-175.pdf" target="_blank">P.L. 101-175</a> &#8212; Protected Genesee Under Same Provisions As Study Rivers</p>
<h3 align="center">1990</h3>
<p><a href="documents/act/101-306.pdf" target="_blank">P.L. 101-306</a> &#8212; Designated Jemez and Pecos</p>
<p><a href="documents/act/101-356.pdf" target="_blank">P.L. 101-356</a> &#8212; Added Merrimack For Study</p>
<p><a href="documents/act/101-357.pdf" target="_blank">P.L. 101-357</a> &#8212; Added Pemigewasset For Study</p>
<p><a href="documents/act/101-364.pdf" target="_blank">P.L. 101-364</a> &#8212; Added St. Marys For Study</p>
<p><a href="documents/act/101-538.pdf" target="_blank">P.L. 101-538</a> &#8212; Added Mills For Study</p>
<p><a href="documents/act/101-612.pdf" target="_blank">P.L. 101-612</a> &#8212; Designated Smith</p>
<p><a href="documents/act/101-628.pdf" target="_blank">P.L. 101-628</a> &#8212; Designated Clarks Fork of Yellowstone; Added Sudbury, Assebet and Concord For Study</p>
<h3 align="center">1991</h3>
<p><a href="documents/act/102-50.pdf" target="_blank">P.L. 102-50</a> &#8212; Designated Niobrara and Missouri; Added Niobrara For Study; Technical Amendments To Missouri Designation</p>
<p><a href="documents/act/102-214.pdf" target="_blank">P.L. 102-214</a> &#8212; Added Lamprey For Study</p>
<p><a href="documents/act/102-215.pdf" target="_blank">P.L. 102-215</a> &#8212; Added White Clay Creek For Study</p>
<p><a href="documents/act/102-220.pdf" target="_blank">P.L. 102-220</a> &#8212; Eleven Point Land Acquisition</p>
<h3 align="center">1992</h3>
<p><a href="documents/act/102-249.pdf" target="_blank">P.L. 102-249</a> &#8212; Designated Michigan Rivers</p>
<p><a href="documents/act/102-271.pdf" target="_blank">P.L. 102-271</a> &#8212; Designated Allegheny; Added Clarion and Mill Creek For Study</p>
<p><a href="documents/act/102-275.pdf" target="_blank">P.L. 102-275</a> &#8212; Designated Arkansas Rivers</p>
<p><a href="documents/act/102-301.pdf" target="_blank">P.L. 102-301</a> &#8212; Designated Sespe, Sisquoc and Big Sur; Added Piru, Little Sur, Matilija, Lopez, and Sespe Creek For Study</p>
<p><a href="documents/act/102-432.pdf" target="_blank">P.L. 102-432</a> &#8212; Designated Merced; Added North Fork Merced For Study</p>
<p><a href="documents/act/102-460.pdf" target="_blank">P.L. 102-460</a> &#8212; Added Lower Delaware For Study</p>
<p><a href="documents/act/102-525.pdf" target="_blank">P.L. 102-525</a> &#8212; Added New For Study</p>
<p><a href="documents/act/102-536.pdf" target="_blank">P.L. 102-536</a> &#8212; Designated Great Egg Harbor</p>
<h3 align="center">1993</h3>
<p><a href="documents/act/103-162.pdf" target="_blank">P.L. 103-162</a> &#8212; Designated Maurice</p>
<p><a href="documents/act/103-170.pdf" target="_blank">P.L. 103-170</a> &#8212; Designated Red</p>
<h3 align="center">1994</h3>
<p><a href="documents/act/103-242.pdf" target="_blank">P.L. 103-242</a> &#8212; Designated Rio Grande; Added Rio Grande For Study</p>
<p><a href="documents/act/103-313.pdf" target="_blank">P.L. 103-313</a> &#8212; Designated Farmington</p>
<h3 align="center">1996</h3>
<p><a href="documents/act/104-208.pdf" target="_blank">P.L. 104-208</a> &#8212; Designated Elkhorn</p>
<p><a href="documents/act/104-311.pdf" target="_blank">P.L. 104-311</a> &#8212; Added Wekiva For Study</p>
<p><a href="documents/act/104-314.pdf" target="_blank">P.L. 104-314</a> &#8212; Designated Clarion</p>
<p><a href="documents/act/104-333.pdf" target="_blank">P.L. 104-333</a> &#8212; Put Hanford Reach (Columbia River) In Permanent Study Status, Designated Lamprey, Bluestone Boundaries, Technical Amendments</p>
<h3 align="center">1998</h3>
<p><a href="documents/act/105-362.pdf" target="_blank">P.L. 105-362</a> &#8212; Technical Amendment to Niobrara (Repealed National Park Study)</p>
<h3 align="center">1999</h3>
<p><a href="documents/act/106-20.pdf" target="_blank">P.L. 106-20</a> &#8212; Designated Sudbury, Assabet and Concord</p>
<p><a href="documents/act/106-119.pdf" target="_blank">P.L. 106-119</a> &#8212; Upper Delaware Visitor Center</p>
<h3 align="center">2000</h3>
<p><a href="documents/act/106-176.pdf" target="_blank">P.L. 106-176</a> &#8212; Lamprey Technical Amendments</p>
<p><a href="documents/act/106-192.pdf" target="_blank">P.L. 106-192</a> &#8212; Designated Lamprey</p>
<p><a href="documents/act/106-261.pdf" target="_blank">P.L. 106-261</a> &#8212; Designated Wilson Creek</p>
<p><a href="documents/act/106-299.pdf" target="_blank">P.L. 106-299</a> &#8212; Designated Wekiva</p>
<p><a href="documents/act/106-318.pdf" target="_blank">P.L. 106-318</a> &#8212; Added Taunton For Study</p>
<p><a href="documents/act/106-357.pdf" target="_blank">P.L. 106-357</a> &#8212; Designated White Clay Creek</p>
<p><a href="documents/act/106-399.pdf" target="_blank">P.L. 106-399</a> &#8212; Designated Donner und Blitzen, Wildhorse &#38; Kiger Creeks</p>
<p><a href="documents/act/106-418.pdf" target="_blank">P.L. 106-418</a> Designated Lower Delaware</p>
<h3 align="center">2001</h3>
<p><a href="documents/act/107-65.pdf" target="_blank">P.L. 107-65</a> &#8212; Added Eight Mile For Study</p>
<h3 align="center">2002</h3>
<p><a href="documents/act/107-365.pdf" target="_blank">P.L. 107-365</a> &#8212; Designated Puerto Rico Rivers</p>
<h3 align="center">2004</h3>
<p><a href="documents/act/108-352.pdf" target="_blank">P.L. 108-352</a> &#8212; Technical Amendments</p>
<p><a href="documents/act/108-447.pdf" target="_blank">P.L. 108-447</a> &#8212; Salmon Outfitter Camps</p>
<h3 align="center">2005</h3>
<p><a href="documents/act/109-44.pdf" target="_blank">P.L. 109-44</a> &#8212; Designated Upper White Salmon</p>
<h3 align="center">2006</h3>
<p><a href="documents/act/109-362.pdf" target="_blank">P.L. 109-362</a> &#8212; Designated Black Butte</p>
<p><a href="documents/act/109-370.pdf" target="_blank">P.L. 109-370</a> &#8212; Added Lower Farmington &amp; Salmon Brook For Study</p>
<p><a href="documents/act/109-452.pdf" target="_blank">P.L. 109-452</a> &#8212; Designated Musconetcong</p>
<h3 align="center">2008</h3>
<p><a href="documents/act/110-229.pdf" target="_blank">P.L. 110-229</a> &#8212; Designated Eightmile</p>
<h3 align="center">2009</h3>
<p><a href="documents/act/111-11.pdf" target="_blank">P.L. 111-11</a> &#8212; Designated South Fork Clackamas, Eagle Creek, Middle Fork Hood, South Fork Roaring, Zig Zag, Fifteenmile Creek, East Fork Hood, Collawash, Fish Creek, Battle Creek, Big Jacks Creek, West Fork Bruneau, Cottonwood Creek (ID), Deep Creek, Jarbidge, Little Jacks Creek, North Fork Owyhee, South Fork Owyhee, Red Canyon, Sheep Creek, Wickahoney Creek, Amargosa, Owens Headwaters, Cottonwood Creek (CA), Piru Creek, North Fork San Jacinto, Fuller Mill Creek, Palm Canyon Creek, Bautista Creek, Virgin, Fossil Creek, Snake Headwaters &amp; Taunton; Added to Elk &amp; Owyhee Designations; Added Missisquoi &amp; Trout For Study</p>
<h3 align="center">2014</h3>
<p><a href="documents/act/113-244.pdf" target="_blank">P.L. 113-244</a> &#8212; Amended Crooked River Upstream Boundary</p>
<p><a href="documents/act/113-291.pdf" target="_blank">P.L. 113-291</a> &#8212; Designated Cave Creek (River Styx), Middle Fork Snoqualmie, Pratt, Illabot Creek, Mississquoi &amp; Trout; Added to White Clay Creek Designation; Added Cave Creek, Lake Creek, No Name Creek, Panther Creek, Upper Cave Creek, Beaver, Chipuxet, Queen, Wood, Pawcatuck, Nashua, Squannacook, Nissitissit and York For Study</p></div>
<!--END .block -->

</div>
<!--END .toggle_container -->

<h2 class="trigger">Section 2(a)(ii) Designations</h2>
<div class="toggle_container">
<div class="block">
<p><a href="documents/section2/2(a)(ii)-federal-register-notices.pdf" title="2(a)(ii) Federal Register Notices" target="_blank">All Federal Register Notices Designating 2(a)(ii) Rivers</a> In Order &amp; Searchable (3.0 MB File!)</p>
<h3 align="center">1970</h3>
<p>Allagash Wilderness Waterway, Maine &#8212; <a href="documents/section2/allagash.pdf" target="_blank">July 17, 1970, <em>Federal Register Notice</em></a></p>
<h3 align="center">1973</h3>
<p>Little Miami River, Ohio &#8212; <a href="documents/section2/little-miami-1.pdf" target="_blank">January 31, 1974, <em>Federal Register Notice</em></a></p>
<h3 align="center">1975</h3>
<p>Little Beaver Creek, Ohio &#8212; <a href="documents/section2/little-beaver.pdf" target="_blank">February 27, 1976, <em>Federal Register Notice</em></a></p>
<h3 align="center">1976</h3>
<p>New River, North Carolina &#8212; <a href="documents/section2/new.pdf" target="_blank">April 19, 1976, <em>Federal Register Notice</em></a></p>
<p>St. Croix River, Minnesota &amp; Wisconsin &#8212; <a href="documents/section2/st-croix.pdf" target="_blank">June 25, 1976, <em>Federal Register Notice</em></a></p>
<h3 align="center">1980</h3>
<p>Little Miami River, Ohio &#8212; <a href="documents/section2/little-miami-2.pdf" target="_blank">January 12, 1981, <em>Federal Register Notice</em></a></p>
<h3 align="center">1981</h3>
<p>American, Eel, Klamath, Smith &amp; Trinity Rivers, California &#8212; <a href="documents/section2/north-coast-rivers.pdf" target="_blank">January 23, 1981, <em>Federal Register Notice</em></a></p>
<h3 align="center">1985</h3>
<p> Loxahatchee River, Florida &#8212; <a href="documents/section2/loxahatchee.pdf" target="_blank">May 23, 1985, <em>Federal Register Notice</em></a></p>
<h3 align="center">1989</h3>
<p>Vermilion River, Illinois &#8212; <a href="documents/section2/vermilion.pdf" target="_blank">August 17, 1989, <em>Federal Register Notice</em></a></p>
<h3 align="center">1993</h3>
<p>Westfield River, Massachusetts &#8212; <a href="documents/section2/westfield-1.pdf" target="_blank">November 16, 1993, <em>Federal Register Notice</em></a></p>
<h3 align="center">1994</h3>
<p>Big &amp; Little Darby Creeks, Ohio &#8212; <a href="documents/section2/big-little-darby.pdf" target="_blank">April 6, 1994, <em>Federal Register Notice</em></a></p>
<p>Cossatot River, Arakansas &#8212; <a href="documents/section2/cossatot.pdf" target="_blank">February 2, 1994, <em>Federal Register Notice</em></a></p>
<p>Klamath River, Oregon &#8212; <a href="documents/section2/klamath-oregon.pdf" target="_blank">October 19, 1994, <em>Federal Register Notice</em></a></p>
<h3 align="center">1996</h3>
<p>Wallowa River, Oregon &#8212; <a href="documents/section2/wallowa.pdf" target="_blank">August 13, 1996, <em>Federal Register Notice</em></a></p>
<h3 align="center">1998</h3>
<p>Lumber River, North Carolina &#8212; <a href="documents/section2/lumber.pdf" target="_blank">October 6, 1998, <em>Federal Register Notice</em></a></p>
<h3 align="center">2004</h3>
<p>Westfield River, Massachusetts &#8212; <a href="documents/section2/westfield-2.pdf" target="_blank">October 29, 2004, <em>Federal Register Notice</em></a></p>
</div>
<!--END .block -->

</div>
<!--END .toggle_container -->

</div>
<!--END #accords -->

<div class="clear"></div> <!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>