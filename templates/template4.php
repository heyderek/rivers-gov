<?php

// Set the page title  -- GENERAL TEMPLATE 4

$page_title = 'Page Title';

// Set the page keywords

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description

$page_description = 'The National Wild and Scenic Rivers System.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'alaska';

// Includes the meta data that is common to all pages

include ("../includes/sub-metascript.php");

// IMPORTANT: Sub pages will need to have the following:

// link to the sub-metascript.php file and have '../' preceeding ALL include files below

//  '../' will need to be remove for any file in the sub-directory

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php

// includes the TEMPLATE HEADER CODING -- #content-page

include ("../includes/header.php")

?>

<?php

// includes the content page top

include ("../includes/content-head.php")

?>

<?php

// includes the top of the rivers page and zoomify button

include ("../includes/rivers-top.php");

?>

<div id="details-text">

<h3>Managing Agency:</h3>

<p>Yukon Delta National Wildlife Refuge</p>
<br />
<h3>Designated Reach:</h3>
<p>December 2, 1980. From its source, including all headwaters, and the East Fork, within the boundary of the Yukon Delta National Wildlife Refuge.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild - 262.0 miles; Total - 262.0 miles.</p>

</div>

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>

<!--END #details-box -->

<div id="photo-frame">

<!-- Image height and width are also defined in style.css -->

<img src="images/temp4-ph.jpg" alt="" width="262px" height="196px" />

</div>

<!--END #photo-frame -->

<div id="photo-details">

<h3>RELATED LINKS</h3>

<p><a href="#" alt="">Management Plan</a> (download pdf)</p>

<p><a href="#" alt="">Related Studies</a></p>

<div id="photo-credit">

<p>Photo Credit: Photographer's Name</p>

</div>

<!--END #photo-credit -->

</div>

<!--END #photo-details -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>

<!--END #rivers-box -->

<div id="lower-content">

<h2>PARAGRAPH HEADING</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis ultrices pellentesque. Ut porttitor ipsum quam. Aliquam consequat vehicula laoreet. Nulla lorem libero, pretium sed bibendum ut, aliquam a quam. Vestibulum ut lectus lacus. Donec lectus lectus, porta quis gravida in, sagittis a leo. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean vitae mi nec elit consequat consequat. In varius metus tristique nunc vehicula pulvinar.</p>

</div>

<!--END #lower-content -->

<?php

// includes the content page bottom

include ("../includes/content-foot.php")

?>

<?php

// includes the TEMPLATE FOOTER CODING -- </html>

include ("../includes/footer.php")

?>