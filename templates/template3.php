<?php

// Set the page title  -- GENERAL TEMPLATE 3

$page_title = 'Alaska';

// Set the page keywords

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description

$page_description = 'The National Wild and Scenic Rivers System.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'alaska';

// Includes the meta data that is common to all pages

include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php

// includes the TEMPLATE HEADER CODING -- #content-page

include ("includes/header.php")

?>

<?php

// includes the content page top

include ("includes/content-head.php")

?>

<div id="intro-box">

<p>Rivers that pass through several states may have segments in each state designated. For example, the Klamath River has designations in California and Oregon. Many rivers also have numerous tributaries designated (e.g., Washington's Skagit River). Multiple listings of some rivers indicate more than one segment of the river is designated (e.g., the Missouri River in Nebraska).</p>

</div>

<!--END #intro-box -->

<ul>
<li><a href="#" title="">Alagnak River</a>  |  <a href="#" title="" class="external">National Park Service Site</a></li>
<li><a href="#" title="">Alatna River</a></li>
<li><a href="#" title="">Andreafsky River</a></li>
<li><a href="#" title="">Aniakchak River</a></li>
<li><a href="#" title="">Beaver Creek</a>   |  <a href="#" title="" class="external">Bureau of Land Management Site</a></li>
<li><a href="#" title="">Birch Creek</a>   |  <a href="#" title="" class="external">Bureau of Land Management Site</a></li>
<li><a href="#" title="">Charley River</a>   |  <a href="#" title="" class="external">National Park Service Site</a></li>
<li><a href="#" title="">Chilikadrotna River</a></li>
<li><a href="#" title="">Delta River</a>   |  <a href="#" title="" class="external">Bureau of Land Management Site</a></li>
<li><a href="#" title="">Fortymile River</a>   |  <a href="#" title="" class="external">Bureau of Land Management Site</a></li>
<li><a href="#" title="">Gulkana River</a>   |  <a href="#" title="" class="external">Bureau of Land Management Site</a></li>
<li><a href="#" title="">Ivishak River</a></li>
<li><a href="#" title="">John River</a></li>
<li><a href="#" title="">Kobuk River</a></li>
<li><a href="#" title="">Koyukuk River (North Fork)</a></li>
<li><a href="#" title="">Mulchatna River</a></li>
<li><a href="#" title="">Noatak River</a></li>
<li><a href="#" title="">Nowitna River</a></li>
<li><a href="#" title="">Salmon River</a></li>
<li><a href="#" title="">Selawik River</a></li>
<li><a href="#" title="">Sheenjek River</a></li>
</ul>

<div class="clear"></div><!-- Allows for content above to be flexible -->

<?php

// includes the content page bottom

include ("includes/content-foot.php")

?>

<?php

// includes the TEMPLATE FOOTER CODING -- </html>

include ("includes/footer.php")

?>
