<?php

// Set the page title  -- ** COLD FUSION TEMPLATE

$page_title = 'USA National Wild and Scenic Rivers | www.rivers.gov | ';

// Set the page keywords

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description

$page_description = 'The National Wild and Scenic Rivers System.';

// Includes the meta data that is common to all pages

include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->	<!-- END page specific CSS and Scripts -->


</head>

<body>

<div id="outer-wrapper">

<?php

// includes the #masthead and #navmenu

include ("includes/header.php")

?>

<div id="content-topper"></div>

<div id="inner-wrapper">

<div id="framed-content">

</div> <!--END #framed-content -->

<div id="footer-top"></div>

<?php include ("includes/footer.php") ?>
				