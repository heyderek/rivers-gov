<?php

// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)

$page_title = 'PAGE TITLE';

// Set the page keywords

$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description

$page_description = 'The National Wild and Scenic Rivers System.';

// Set the region for Sidebar Images 

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw

$region = 'midwest';

// Includes the meta data that is common to all pages

include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- JS that controls the accordion -->
<script type="text/javascript">
$(document).ready(function(){
$(".toggle_container").hide();
$("h2.trigger").click(function(){
$(this).toggleClass("active").next().slideToggle("slow");
});
});
</script>
<!-- END page specific CSS and Scripts -->

<?php

// includes the TEMPLATE HEADER CODING -- #content-page

include ("includes/header.php")

?>

<?php

// includes the content page top

include ("includes/content-head.php")

?>

<div id="intro-box">

<h2>LOREM IPSUM DOLOR SIT AMET</h2>

<p>Consectetur adipiscing elit. Duis sagittis ultrices pellentesque. Ut porttitor ipsum quam. Aliquam consequat vehicula laoreet. Nulla lorem libero, pretium sed bibendum ut, aliquam a quam. Vestibulum ut lectus lacus. Donec lectus lectus, porta quis gravida in, sagittis a leo. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean vitae mi nec elit consequat consequat.</p>

</div>

<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->

<center><img src="images/temp2-ph.jpg" alt="" width="565px" height="121px" /></center>

<div id="lower-content">

<div id="lc-left">

<h2>PARAGRAPH HEADING</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis ultrices pellentesque. Ut porttitor ipsum quam. Aliquam consequat vehicula laoreet. Nulla lorem libero, pretium sed bibendum ut, aliquam a quam. Vestibulum ut lectus lacus. Donec lectus lectus, porta quis gravida in, sagittis a leo. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean vitae mi nec elit consequat consequat. In varius metus tristique nunc vehicula pulvinar.</p>

</div>

<!--END #lc-left -->

<div id="block-quote">

<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis ultrices pellentesque. -Lorem</h4>

</div>

<!--END #block-quote -->

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>

<!--END #lower-content -->

<div id="accords">

<h2 class="trigger">Accordion Title</h2>

<div class="toggle_container">

<div class="block">

<h2>PARAGRAPH HEADING</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis ultrices pellentesque. Ut porttitor ipsum quam. Aliquam consequat vehicula laoreet. Nulla lorem libero, pretium sed bibendum ut, aliquam a quam. Vestibulum ut lectus lacus. Donec lectus lectus, porta quis gravida in, sagittis a leo. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean vitae mi nec elit consequat consequat. In varius metus tristique nunc vehicula pulvinar.</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Accordion Title</h2>

<div class="toggle_container">

<div class="block">

<h2>PARAGRAPH HEADING</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis ultrices pellentesque. Ut porttitor ipsum quam. Aliquam consequat vehicula laoreet. Nulla lorem libero, pretium sed bibendum ut, aliquam a quam. Vestibulum ut lectus lacus. Donec lectus lectus, porta quis gravida in, sagittis a leo. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean vitae mi nec elit consequat consequat. In varius metus tristique nunc vehicula pulvinar.</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Accordion Title</h2>

<div class="toggle_container">

<div class="block">

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

<h2 class="trigger">Accordion Title</h2>

<div class="toggle_container">

<div class="block">

<h2>PARAGRAPH HEADING</h2>


<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis ultrices pellentesque. Ut porttitor ipsum quam. Aliquam consequat vehicula laoreet. Nulla lorem libero, pretium sed bibendum ut, aliquam a quam. Vestibulum ut lectus lacus. Donec lectus lectus, porta quis gravida in, sagittis a leo. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean vitae mi nec elit consequat consequat. In varius metus tristique nunc vehicula pulvinar.</p>

</div>

<!--END .block -->

</div>

<!--END .toggle_container -->

</div>

<!--END #accords -->

<div class="clear"></div> <!-- Allows for content above to be flexible -->

<?php

// includes the content page bottom

include ("includes/content-foot.php")

?>

<?php

// includes the TEMPLATE FOOTER CODING -- </html>

include ("includes/footer.php")

?>				