<?php

// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'River Videos';

// Set the page keywords
$page_keywords = '50th anniversary, rivers, wild and scenic rivers, videos';

// Set the page description
$page_description = 'Video resources for the 50th anniversary of the Wild and Scenic Rivers Act in 2018.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include("includes/metascript.php");
$sidebar_bool = false;

?>

<!-- BEGIN page specific CSS and Scripts -->
<!-- JS that controls the accordion -->
<!--<script type="text/javascript">-->
<!--	$(document).ready(function(){-->
<!--	$(".accordion-content").hide();-->
<!--	$("h2.accordion-heading").click(function(){-->
<!--	$(this).toggleClass("active").next().slideToggle("slow");-->
<!--	});-->
<!--	});-->
<!--</script>-->
<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
$body_class = 'fullwidth';
include("includes/header.php")
?>

<?php
// includes the content page top
include("includes/content-head.php")
?>

<br/>
<br/>
<br/>
<br/>
<br/>

<script>
	$(document).ready(function () {
		var content = $('.accordion-content');
		var heading = $('.accordion-heading');
		content.hide();
		heading.click(function () {
			$(this).next(content).slideToggle(300)
			$(this).toggleClass('active-accordion');
		})
	});
</script>

<p style="font-size: 20pt; color: #23317A">Wild and Scenic River 50th Anniversary Films</p>
<p>&nbsp;</p>

<div class="row">

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>Rivers and Trails<br />Anniversary Commercial</center></h3>
		<iframe src="https://player.vimeo.com/video/192389583?title=0&byline=0&portrait=0" width="410" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p style="font-size: 10pt;">&nbsp;</p>
	</div>

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>It's Your River.<br />Make Your Splash!</center></h3>
		<iframe width="410" height="231" src="https://www.youtube.com/embed/X7dnaAuMwN0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

		<p style="font-size: 10pt;">&nbsp;</p>
	</div>

</div>

<div class="row">

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>River Connections:<br />Partnership Wild &amp; Scenic Rivers</center></h3>
		<!--<p><a href="https://vimeo.com/244586953?ref=em-v-share" target="_blank"><img src="images/wsr50/p-w-s-r.jpg" alt="River Connections, Partnership Wild and Scenic Rivers" width="410" height="232" style="border: 1px solid #CCCCCC" title="River Connections, Partnership Wild and Scenic Rivers" /></a></p>-->
		<iframe src="https://www.nps.gov/media/video/embed.htm?id=0AACAFB4-1DD8-B71B-0B1D7583F4A281C9" width="410" height="231" frameborder="0" scrolling="auto" allowfullscreen></iframe>

		<p style="font-size: 10pt;"><center>Wild &amp; Scenic Film Festival 2018 Selection</center></p>
	</div>
	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>Protected: A Wild &amp; Scenic<br />River Portrait</center></h3>
		<iframe src="https://player.vimeo.com/video/249044434" width="410" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p style="font-size: 10pt;"><center>Wild &amp; Scenic Film Festival 2018 Selection</center></p>
	</div>

</div>

<div class="row">

    <div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>Wild Olympics</center></h3>
		<iframe src="https://player.vimeo.com/video/254768030" width="410" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p style="font-size: 10pt;"><center>Wild &amp; Scenic Film Festival 2018 Selection</center></p>
	</div>

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>Noatak: Wild &amp; Scenic</center></h3>
        <iframe src="https://player.vimeo.com/video/234728048" width="410" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p style="font-size: 10pt;">&nbsp;</p>
	</div>

</div>

<div class="row">

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>The Wild President</center></h3>
		<iframe src="https://player.vimeo.com/video/198888034" width="410" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p style="font-size: 10pt;">&nbsp;</p>
	</div>

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>To Love a Place</center></h3>
		<iframe width="410" height="231" src="https://www.youtube.com/embed/0qqrn_xDN3Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

		<p style="font-size: 10pt;">&nbsp;</p>
	</div>

</div>
	
<div class="row">

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->		
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px"><center>Avanyu</center></h3>
		<iframe width="410" height="231" src="https://www.youtube.com/embed/0wsGL1hs2m8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

		<p style="font-size: 10pt;">&nbsp;</p>
	</div>

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>Every Bend</center></h3>
		<iframe width="410" height="231" src="https://www.youtube.com/embed/QGsVyTfj_C4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

		<p style="font-size: 10pt;"><center>Wild &amp; Scenic Film Festival 2018 Selection</center></p>
	</div>

</div>

<div class="row">

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->		
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px"><center>Travels with Darley Episode</center></h3>
		<iframe src="https://player.vimeo.com/video/261425713" width="410" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		<p style="font-size: 10pt;"><center>Raft New Mexico's Rio Chama Wild and Scenic River (airs on various television stations)</center></p>
	</div>

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>Going Rogue</center></h3>
		<iframe width="410" height="231" src="https://www.youtube.com/embed/NzH0IaOBVd0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		
		<p style="font-size: 10pt;"><center></center></p>
	</div>

</div>

<div class="row">

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->		
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px"><center>Bureau of Land Management Public Lands Promo</center></h3>
        <iframe width="410" height="231" src="https://www.youtube.com/embed/arXs6Ho93KM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

		<p style="font-size: 10pt;"><center></center></p>
	</div>

	<div class="col-50 column">
		<!-- Remove the heading and the h3 tags if no heading is needed. -->
		<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px;"><center>How You Can Shape the Future of Wild &amp; Scenic Rivers</center></h3>
		<iframe src="https://player.vimeo.com/video/272593044" width="410" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		
		<p style="font-size: 10pt;"><center>Forest Service animated infographic from <a href="https://vimeo.com/yourforestsyourfuture">Your Forests Your Future</a></center></p>
	</div>

</div>

<!--<div class="row">

	<div class="col-50 column">
    	<h3 style="font-size: 16pt; color: #2C7843; padding-bottom: 5px"><center>Bureau of Land Management Public Lands Promo</center></h3>
        <iframe width="410" height="231" src="https://www.youtube.com/embed/arXs6Ho93KM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

		<p style="font-size: 10pt;"><center></center></p>
	</div>

</div>-->

<?php
// includes the content page bottom
include("includes/content-footNEW.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include("includes/footer.php")
?>