<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Oregon';
// Set the page keywords
$page_keywords = 'Oregon';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Oregon.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northwest';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'OR';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Oregon has approximately 110,994 miles of river, of which 1,916.7 miles are designated as wild &amp; scenic&#8212;almost 2% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/big-marsh.php" title="Big Marsh Creek">Big Marsh Creek</a></li>
<li><a href="rivers/chetco.php" title="Chetco River">Chetco River</a></li>
<li><a href="rivers/clackamas.php" title="Clackamas River">Clackamas River</a></li>
<li><a href="rivers/clackamas-sf.php" title="Clackamas River (South Fork)">Clackamas River (South Fork)</a></li>
<li><a href="rivers/collawash.php" title="Collawash River">Collawash River</a></li>
<li><a href="rivers/crescent.php" title="Crescent Creek">Crescent Creek</a></li>
<li><a href="rivers/crooked.php" title="Crooked River">Crooked River</a></li>
<li><a href="rivers/crooked-nf.php" title="Crooked River (North Fork)">Crooked River (North Fork)</a></li>
<li><a href="rivers/deschutes.php" title="Deschutes River">Deschutes River</a></li>
<li><a href="rivers/donner-und-blitzen.php" title="Donner und Blitzen River">Donner und Blitzen River</a></li>
<li><a href="rivers/eagle1.php" title="Eagle Creek (Mt. Hood National Forest)">Eagle Creek (Mt. Hood National Forest)</a></li>
<li><a href="rivers/eagle2.php" title="Eagle Creek (Wallowa-Whitman National Forest)">Eagle Creek (Wallowa-Whitman National Forest)</a></li>
<li><a href="rivers/elk.php" title="Elk River">Elk River</a></li>
<li><a href="rivers/elkhorn.php" title="Elkhorn Creek">Elkhorn Creek</a></li>
<li><a href="rivers/fifteenmile.php" title="Fifteenmile Creek">Fifteenmile Creek</a></li>
<li><a href="rivers/fish.php" title="Fish Creek">Fish Creek</a></li>
<li><a href="rivers/grande-ronde.php" title="Grande Ronde River">Grande Ronde River</a></li>
<li><a href="rivers/hood-ef.php" title="Hood River (East Fork)">Hood River (East Fork)</a></li>
<li><a href="rivers/hood-mf.php" title="Hood River (Middle Fork)">Hood River (Middle Fork)</a></li>
<li><a href="rivers/illinois.php" title="Illinois River">Illinois River</a></li>
<li><a href="rivers/imnaha.php" title="Imnaha River">Imnaha River</a></li>
<li><a href="rivers/john-day.php" title="John Day River">John Day River</a></li>
<li><a href="rivers/john-day-nf.php" title="John Day River (North Fork)">John Day River (North Fork)</a></li>
<li><a href="rivers/john-day-sf.php" title="John Day River (South Fork)">John Day River (South Fork)</a></li>
<li><a href="rivers/joseph.php" title="Joseph Creek">Joseph Creek</a></li>
<li><a href="rivers/klamath-or.php" title="Klamath River">Klamath River</a></li>
<li><a href="rivers/little-deschutes.php" title="Little Deschutes River">Little Deschutes River</a></li>
<li><a href="rivers/lostine.php" title="Lostine River">Lostine River</a></li>
<li><a href="rivers/malheur.php" title="Malheur River">Malheur River</a></li>
<li><a href="rivers/malheur-nf.php" title="Malheur River (North Fork)">Malheur River (North Fork)</a></li>
<li><a href="rivers/mckenzie.php" title="McKenzie River">McKenzie River</a></li>
<li><a href="rivers/metolius.php" title="Metolius River">Metolius River</a></li>
<li><a href="rivers/minam.php" title="Minam River">Minam River</a></li>
<li><a href="rivers/north-powder.php" title="North Powder River">North Powder River</a></li>
<li><a href="rivers/north-umpqua.php" title="North Umpqua River">North Umpqua River</a></li>
<li><a href="rivers/owyhee-or.php" title="Owyhee River">Owyhee River</a></li>
<li><a href="rivers/owyhee-nf-or.php" title="Owyhee River (North Fork)">Owyhee River (North Fork)</a></li>
<li><a href="rivers/powder.php" title="Powder River">Powder River</a></li>
<li><a href="rivers/quartzville.php" title="Quartzville Creek">Quartzville Creek</a></li>
<li><a href="rivers/styx.php" title="River Styx">River Styx</a></li>
<li><a href="rivers/roaring.php" title="Roaring River">Roaring River</a></li>
<li><a href="rivers/roaring-sf.php" title="Roaring River (South Fork)">Roaring River (South Fork)</a></li>
<li><a href="rivers/rogue.php" title="Rogue River">Rogue River</a></li>
<li><a href="rivers/rogue-upper.php" title="Rogue River (Upper)">Rogue River (Upper)</a></li>
<li><a href="rivers/salmon-or.php" title="Salmon River">Salmon River</a></li>
<li><a href="rivers/sandy.php" title="Sandy River">Sandy River</a></li>
<li><a href="rivers/smith-nf.php" title="Smith River (North Fork)">Smith River (North Fork)</a></li>
<li><a href="rivers/snake.php" title="Snake River">Snake River</a></li>
<li><a href="rivers/sprague.php" title="Sprague River">Sprague River</a></li>
<li><a href="rivers/whychus.php" title="Squaw Creek">Squaw Creek</a></li>
<li><a href="rivers/sycan.php" title="Sycan River">Sycan River</a></li>
<li><a href="rivers/wallowa.php" title="Wallowa River">Wallowa River</a></li>
<li><a href="rivers/wenaha.php" title="Wenaha River">Wenaha River</a></li>
<li><a href="rivers/west-little-owyhee.php" title="West Little Owyhee River">West Little Owyhee River</a></li>
<li><a href="rivers/whychus.php" title="Whychus Creek">Whychus Creek</a></li>
<li><a href="rivers/white.php" title="White River">White River</a></li>
<li><a href="rivers/wildhorse.php" title="Wildhorse &amp; Kiger Creeks">Wildhorse &amp; Kiger Creeks</a></li>
<li><a href="rivers/willamette.php" title="Willamette River (North Fork Middle Fork)">Willamette River (North Fork Middle Fork)</a></li>
<li><a href="rivers/zigzag.php" title="Zigzag River">Zigzag River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>