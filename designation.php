<?php

// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Wild &amp; Scenic River Designation';

// Set the page keywords
$page_keywords = 'rivers, wild and scenic rivers, river conservation, conservation, streams, creeks, water, river protection, National Park Service, Bureau of Land Management, U.S. Forest Service, U.S. Fish and Wildlife Service';

// Set the page description
$page_description = 'Designating wild &amp; scenic rivers';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");

?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Designating Wild &amp; Scenic Rivers</h2>
<p>The question we’re most frequently asked is, “How do I get my favorite river designated?” Most people assume that an agency or a group of people decide which rivers should be designated. Unfortunately, that’s not how it works.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/lumber.jpg" alt="Lumber River - Tim Palmer" height="359" title="Lumber River - Tim Palmer" width="565" /><br /><em style="font-size:11px; line-height: 20px">2(a)(ii) Designated Lumber River, North Carolina - Photo by Tim Palmer</em></center>

<div id="lower-content">

<div id="lc-left">

<p><br /><br /></p>

<p>There are two methods by which a river is designated. The first is straight forward, but because of the relatively rare set of circumstances that must first exist, it’s not used often. The second method is, at its core, also straight forward, but there are any number of variations that might be involved.</p>

<p>First, let’s look at the seldom-used option.</p>

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div>
<!--END #lc-left -->

<div id="block-quote">
<h4><em>What a country chooses to save is what a country chooses to say about itself.</em> &#8212; Mollie Beattie</h4></h4>
</div>
<!--END #block-quote -->

<p style="line-height: 15px">&nbsp;</p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32; line-height: 50px"> &nbsp;<br><br>Section 2(a)(ii) of the Wild &amp; Scenic Rivers Act</p>

<p>Section 2(a)(ii) of the Wild &amp; Scenic Rivers Act allows the Secretary of the Interior to designate a river if a state governor requests designation. However, for a river to qualify for the National System through Section 2(a)(ii) of the Act, four requirements must first be met.
<ol>
<li>The river must first be designated as wild, scenic, or recreational (or the equivalent thereof) at the state level by, or pursuant to, an act of the legislature of that state. Usually, this means designation into a state program. However, there are very few <a href="agencies.php" title="States With River Protection Programs" target="_blank">states that have a state river protection system</a>. There are examples where a river has first been protected at the state level without the benefit of a state rivers program (e.g., Middle Fork of the Vermilion), but this is less common.</li>
<br />
<li>The river must meet eligibility criteria common to all national wild and scenic rivers, i.e., the river must be free-flowing, as determined by Section 16(b) of the Act and standards set by the Departments of the Interior and Agriculture, and possess one or more "outstandingly remarkable value" (ORV).</li>
<br />
<li>The river must be administered by an agency or political subdivision of the state at no cost to the federal government, except for those lands already administered by an agency of the federal government.</li>
<br />
<li>There must be effective mechanisms and regulations in place&#8212;local, state or federal&#8212;to provide for the long-term protection of those resources for which the river was deemed eligible.</li>
</ol></p>

<p>There is another important element for designation under Section 2(a)(ii): You need a governor willing to request designation.</p>

<p>For additional information, please see the paper entitled “<a href="documents/2aii.pdf" title="Designating Rivers Through Section 2(a)(ii) of the Wild &amp; Scenic Rivers Act" target="_blank">Designating Rivers Through Section 2(a)(ii) of the Wild &amp; Scenic Rivers Act</a>.”</p>

<p><br /></p>

<p style="font-size: 11pt; font-style: italic; font-weight: bold; color: #235B32; line-height: 35px">Congressional Designation</p>

<p>Congress designates most rivers into the National Wild &amp; Scenic Rivers System. The typical process is:
<ol>
<li>Congress authorizes one of the river-managing agencies to first conduct a study to determine if the river is eligible and suitable for designation. The lead agency is typically the agency that manages the land through which the river flows. The National Park Service is the usual study agency for rivers flowing through non-federal lands.</li>
<br />
<li>Upon completion of the study, the agency reports its findings back to Congress.</li>
<br />
<li>Regardless of the outcome of an agency’s recommendation, Congress holds the authority to pass legislation that designates a river. There are numerous examples of rivers recommended by agencies that have not led to introduced or passed bills designating those rivers as part of the National Wild and Scenic Rivers System.</li>
</ol></p>

<p>Of course, Congress has broad powers with regard to legislation, so there may be any number of variations on the process. Congress could designate a river without first authorizing a study. Congress could ignore the agency recommendations and do something entirely different (e.g., designate a different segment from that recommended).</p>

<p>There can also be wide variation in the study process itself. For example, the agency might choose&#8212;or be directed by Congress&#8212;to write a river management plan at the same time the study is being conducted. The might be a federal advisory committee involved. And so on.</p>

<p>And, of course, an Administration could recommend legislation to Congress.</p>

<p>In the end, however, it comes down to one idea: Congress needs to designate your river. Like any process involving a citizens initiative, it’s likely that you will have to garner wide support before approaching your congressional representatives—community leaders, environmental organizations, businesses, landowners, etc.</p>

<p>For additional information, please see the paper entitled “<a href="documents/study-process.pdf" title="The Wild &amp; Scenic River Study Process" target="_blank">The Wild & Scenic River Study Process</a>.”</p>

<div class="clear"></div><!-- Allows for content above to be flexible -->

</div><!--END #lower-content -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>
