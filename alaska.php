<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Alaska';
// Set the page keywords
$page_keywords = 'Alaska';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Alaska.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'alaska';
// Includes the meta data that is common to all pages
include( "includes/metascript.php" );

// Create a postal code ID for checking against.
$state_code = 'AK';

?>


	<!-- BEGIN page specific CSS and Scripts -->

	<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include( "includes/header.php" )
?>

<?php
// includes the content page top
include( "includes/content-head.php" )
?>


	<div id="intro-box">
		<p>Alaska has approximately 365,000 miles of river, of which 3,210 miles are designated as wild &amp; scenic&#8212;less
			than 1% of the state's river miles.</p>
	</div>
	<!--END #intro-box -->

	<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

	<ul>
		<li><a href="rivers/alagnak.php" title="Alagnak River">Alagnak River</a></li>
		<li><a href="rivers/alatna.php" title="Alatna River">Alatna River</a></li>
		<li><a href="rivers/andreafsky.php" title="Andreafsky River">Andreafsky River</a></li>
		<li><a href="rivers/aniakchak.php" title="Aniakchak River">Aniakchak River</a></li>
		<li><a href="rivers/beaver.php" title="Beaver Creek">Beaver Creek</a></li>
		<li><a href="rivers/birch.php" title="Birch Creek">Birch Creek</a></li>
		<li><a href="rivers/charley.php" title="Charley River">Charley River</a></li>
		<li><a href="rivers/chilikadrotna.php" title="Chilikadrotna River">Chilikadrotna River</a></li>
		<li><a href="rivers/delta.php" title="Delta River">Delta River</a></li>
		<li><a href="rivers/fortymile.php" title="Fortymile River">Fortymile River</a></li>
		<li><a href="rivers/gulkana.php" title="Gulkana River">Gulkana River</a></li>
		<li><a href="rivers/ivishak.php" title="Ivishak River">Ivishak River</a></li>
		<li><a href="rivers/john.php" title="John River">John River</a></li>
		<li><a href="rivers/kobuk.php" title="Kobuk River">Kobuk River</a></li>
		<li><a href="rivers/koyukuk.php" title="Koyukuk River (North Fork)">Koyukuk River (North Fork)</a></li>
		<li><a href="rivers/mulchatna.php" title="Mulchatna River">Mulchatna River</a></li>
		<li><a href="rivers/noatak.php" title="Noatak River">Noatak River</a></li>
		<li><a href="rivers/nowitna.php" title="Nowitna River">Nowitna River</a></li>
		<li><a href="rivers/salmon-ak.php" title="Salmon River">Salmon River</a></li>
		<li><a href="rivers/selawik.php" title="Selawik River">Selawik River</a></li>
		<li><a href="rivers/sheenjek.php" title="Sheenjek River">Sheenjek River</a></li>
		<li><a href="rivers/tinayguk.php" title="Tinayguk River">Tinayguk River</a></li>
		<li><a href="rivers/tlikakila.php" title="Tlikakila River">Tlikakila River</a></li>
		<li><a href="rivers/unalakleet.php" title="Unalakleet River">Unalakleet River</a></li>
		<li><a href="rivers/wind.php" title="Wind River">Wind River</a></li>
	</ul>

	<div class="clear"></div>
	<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include( "includes/content-foot.php" )
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include( "includes/footer.php" )
?>