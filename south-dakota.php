<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'South Dakota';
// Set the page keywords
$page_keywords = 'South Dakota';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - South Dakota.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'midwest';
// Includes the meta data that is common to all pages

include( "includes/metascript.php" );

// Create a postal code ID for checking against.
$state_code = 'SD';

?>

	<!-- BEGIN page specific CSS and Scripts -->

	<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include( "includes/header.php" )
?>

<?php
// includes the content page top
include( "includes/content-head.php" )
?>

	<div id="intro-box">
		<p>South Dakota has approximately 9,513 miles of river, of which 93 miles are designated as wild &amp; scenic&#8212;less
			than 1% of the state's river miles.</p>
	</div>
	<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>


	<ul>
		<li><a href="rivers/missouri-ne-sd.php" title="Missouri River">Missouri</a></li>
	</ul>

	<div class="clear"></div>
	<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include( "includes/content-foot.php" )
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include( "includes/footer.php" )
?>