<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Arkansas';
// Set the page keywords
$page_keywords = 'Arkansas';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Arkansas.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Create a postal code ID for checking against.
$state_code = 'AR';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Arkansas has approximately 82,366 miles of river, of which 210 miles are designated as wild &amp; scenic&#8212;approximately 1/4 of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/big-piney.php" title="Big Piney Creek">Big Piney Creek</a></li>
<li><a href="rivers/buffalo.php" title="Buffalo River">Buffalo River</a></li>
<li><a href="rivers/cossatot.php" title="Cossatot River">Cossatot River</a></li>
<li><a href="rivers/hurricane.php" title="Hurricane Creek">Hurricane Creek</a></li>
<li><a href="rivers/little-missouri.php" title="Little Missouri River">Little Missouri River</a></li>
<li><a href="rivers/mulberry.php" title="Mulberry River">Mulberry River</a></li>
<li><a href="rivers/north-sylamore.php" title="North Sylamore Creek">North Sylamore Creek</a></li>
<li><a href="rivers/richland.php" title="Richland Creek">Richland Creek</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>