<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Owyhee River (North Fork), Idaho';

// Set the page keywords
$page_keywords = 'North Fork of the Owyhee River, Idaho';

// Set the page description
$page_description = 'Owyhee River (North Fork), Idaho';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('190');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?><?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Boise District</p>
<br />
<h3>Designated Reach:</h3>
<p>March 30, 2009. The North Fork of the Owyhee River from the Idaho-Oregon State border upstream to the upstream boundary of the North Fork Owyhee River Wilderness.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 15.1 miles; Recreational &#8212; 5.7 miles; Total &#8212; 20.8 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/owyhee-nf-id.jpg" alt="North Fork Owyhee River" title="North Fork Owyhee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="https://www.blm.gov/programs/national-conservation-lands/wild-and-scenic-rivers/idaho" alt="Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)" target="_blank">Idaho Wild &amp; Scenic Rivers (Bureau of Land Management)</a></p>
<p><a href="../documents/plans/owyhee-main-nf-west-little-plan-ea.pdf" title="Owyhee River Management Plan &amp; Environmental Assessment" target="_blank">Owyhee River Management Plan &amp; Environmental Assessment</a></p>

<div id="photo-credit">
<p>Photo Credit: Judi Zuckert, BLM</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Owyhee River (North Fork)</h2>
<p>The North Fork Owyhee River, characterized by steep, vertical walled canyons, emerges from the North Fork Owyhee Wilderness and flows west toward Oregon. Its designation changes from wild to recreational at the Juniper Mountain Road crossing, but its ‘wild’ character throughout creates a dramatic canyon experience for experts-only boating during high spring flows. During high spring flows, a portion of this river is enjoyed by expert boaters as an outstanding whitewater run.</p>

<h2>Outstandingly Remarkable Values</h2>

<p><strong><em>Scenic</em></strong></p>

<p>The North Fork Owyhee is characterized by narrow canyons as great as 500 feet deep, dominated by a coarse-textured, red, brown, or blackish eroded cliffs, often glazed with yellow to light green micro-flora.</p>

<p><strong><em>Recreational</em></strong></p>

<p>Boating opportunities and activities are supported by high water flows during spring and early summer. A high-quality, 18-mile section from the North Fork Campground to the confluence with the Owyhee River in Three Forks, Oregon, is recommended for expert boaters in kayaks and small catarafts. After warming up on a few Class II-III rapids, boaters encounter steep Class III-IV rapids (depending on flow). There are many locations from which to hike from the canyon rim to the stream, especially during low-water periods. The canyons provide exceptional opportunities for solitude, wildlife viewing and photography.</p>

<p><strong><em>Geologic</em></strong></p>

<p>As the North Fork Owyhee corridor is initially dominated by the weathered, sculpted rhyolite (rock that began as lava flow) formations nestled in the rubble slopes below vertical walls of basalt creating pinnacles known as “hoodoos.” The canyon changes to one engulfed in sheer walls of blocky basalt. The canyons of the Owyhee River and its tributaries possess predominately Miocene Era volcanic formations. The Owyhee, Bruneau and Jarbidge river systems provide the largest concentration of sheer-walled rhyolite/basalt canyons in the western United States.</p>

<p><strong><em>Fisheries and Aquatic Species</em></strong></p>

<p>The North Fork Owyhee supports sensitive redband trout populations, as does the Owyhee River and its other tributaries; however, warmer summer water temperatures are insufficient to support productive redband fisheries.</p>

<p>The success of fisheries in these systems depends on appropriate flows during key life stages. These rivers exhibit typical flashy, desert streamflows to which the resident fish species are adapted. In summer and early fall, low flows are sufficient to maintain standing pools for fish survival. The high flows that may occur only every few years are integral to the maintenance of channels that support pool depths and channel diversity.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee Canyonlands provide both upland and canyon riparian habitats for a number of wildlife species common to Southwest Idaho. Big game commonly found in the area include California bighorn sheep, elk, mule deer and pronghorn.</p>

<p>Common large and mid-sized predators in the area include cougars, bobcats, coyotes, badgers and raccoons. Small mammals include rodents (mice, kangaroo rats, voles, squirrels and chipmunks), rabbits, shrews, bats, weasels and skunks. The waters along the entire Owyhee River system are considered outstanding habitat for river otter.</p>

<p>A variety of bird species occur in the area, including songbirds, waterfowl, shorebirds and raptors. The high, well-fractured and eroded canyon cliffs are considered outstanding habitat for cliff nesting raptors, a small number of which occasionally winter along the canyon walls of the upper Owyhee River system and its major tributaries. Other wildlife includes several snake and lizard species, as well as a few amphibians.</p>

<p>The area is considered Preliminary Priority Habitat for greater sage-grouse. Idaho BLM sensitive species include bald eagles, yellow-billed cuckoos, prairie falcons, ferruginous hawks, several neotropical migratory bird species, several bat species, Columbia spotted frogs and western toads. Cliffs also support spotted and Townsend's big-eared bats, both Idaho BLM sensitive species.</p>

<p><strong><em>Cultural</em></strong></p>

<p>These canyonlands and rivers have provided essential resources, shelter and life for the Shoshone and Paiute people for countless generations. They subsisted on seasonally available game, such as antelope, deer, elk, bison (residents of southern Idaho until the early 19th century) and bighorn sheep; roots and bulbs, such as bitterroot and arrowleaf balsamroot; and small game. The tribes took refuge in the canyonlands during conflicts with European settlers.</p>

<p>The Owyhee, Bruneau and Jarbidge rivers served as the migration route and spawning areas for anadromous fish that were important to the tribes. A part of tribal culture was lost when dams blocked the salmon from migrating into the rivers’ upper reaches.</p>

<p>Petroglyphs, pictographs, rock alignments, shrines and vision quest sites are located throughout the canyonlands. Tribal members still frequent the canyonland areas to hunt, fish, pray and conduct ceremonies, and the areas will continue to be a special place for the tribe for generations to come. Floaters using the canyons today are preserving a culture of adventure and solitary recreation within the sheltered canyons of the wild and scenic river designations.</p>

<p><strong><em>Other: Ecological</em></strong></p>

<p>The Owyhee River System is home to a rare plant, the Owyhee River forget-me-not. Found nowhere else but this river system, this species occupies North facing vertical rhyolitic cliffs, sheltered crevices and shady grottos. The pale blue flowers of this species contrast sharply with the backdrop of dark volcanic rock. May and June are the best time to view this species in full flower.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>