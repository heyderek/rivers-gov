<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Pennsylvania';
// Set the page keywords
$page_keywords = 'Pennsylvania';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Pennsylvania.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'PA';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Pennsylvania has approximately 83,260 miles of river, of which 409.3 miles are designated as wild &amp; scenic&#8212;approximately 1/2 of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/allegheny.php" title="Allegheny River">Allegheny River</a></li>
<li><a href="rivers/clarion.php" title="Clarion River">Clarion River</a></li>
<li><a href="rivers/delaware-lower.php" title="Delaware River (Lower)">Delaware River (Lower)</a></li>
<li><a href="rivers/delaware-middle.php" title="Delaware River (Middle)">Delaware River (Middle)</a></li>
<li><a href="rivers/delaware-upper.php" title="Delaware River (Upper)">Delaware River (Upper)</a></li>
<li><a href="rivers/white-clay.php" title="White Clay Creek">White Clay Creek</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>