<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Lamprey River, New Hampshire';

// Set the page keywords
$page_keywords = 'Lamprey River, New Hampshire';

// Set the page description
$page_description = 'Lamprey River, New Hampshire';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';

//ID for the rivers
$river_id = array('158');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?><?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>National Park Service, Northeast Regional Office</p>
<br />
<h3>Designated Reach:</h3>
<p>November 12, 1996, and May 2, 2000. The segment from the Bunker Pond Dam in the town of Epping to the confluence with the Piscassic River in the vicinity of the Durham-Newmarket town line.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>November 12, 1996: Recreational &#8212; 11.5 miles; Total &#8212; 11.5 miles.  May 2, 2000: Recreational &#8212; 12.0 miles; Total &#8212; 12.0 miles.  Total &#8212; 23.5 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/lamprey.jpg" alt="Lamprey River" title="Lamprey River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://des.nh.gov/organization/divisions/water/wmb/rivers/lamp_river.htm" alt="Lamprey River (New Hampshire Department of Environmental Services)" target="_blank">Lamprey River (New Hampshire Department of Environmental Services)</a></p>
<p><a href="http://www.nps.gov/ncrc/programs/pwsr/lamprey_pwsr_sub.html" alt="Lamprey River (National Park Service)" target="_blank">Lamprey River (National Park Service)</a></p>
<p><a href="http://www.lampreyriver.org" alt="Lamprey River Advisory Committee" target="_blank">Lamprey River Advisory Committee</a></p>
<p><a href="../documents/studies/lamprey-study.pdf" title="Lamprey River Study (14.2 MB PDF)" target="_blank">Lamprey River Study (14.2 MB PDF)</a></p>
<p><a href="../documents/plans/lamprey-plan.pdf" alt="Lamprey River Management Plan (1.5 MB PDF)" target="_blank">Lamprey River Management Plan (1.5 MB PDF)</a></p>

<div id="photo-credit">
<p>Photo Credit: Lelia Mellen</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Lamprey River</h2>
<p>From the Bunker Pond Dam to the confluence with the Piscassic River, the Lamprey River's shoreline, natural floodplain, and wetlands provide a range of wildlife habitats. Currently, the Lamprey has the largest quantity of anadromous fish in the Great Bay watershed,and it hosts substantial numbers of freshwater mussel species. The river's resources include archaeological sites of prehistoric and nineteenth century culture, which are representative of the early settlement of New Hampshire's seacoast region. It is managed through a local-state-federal partnership, the Lamprey River Advisory Committee.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>