<?php
// Set the page title  -- GENERAL TEMPLATE 4
$page_title = 'Owyhee River, Oregon';

// Set the page keywords
$page_keywords = 'Owyhee River, Oregon';

// Set the page description
$page_description = 'Owyhee River, Oregon';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'inlandnw';

//ID for the rivers
$river_id = array('55');

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<script>
var riverID = <?php echo json_encode($river_id); ?>;
</script>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ('../includes/header.php');
?>

<?php
// includes the content page top
include ('../includes/content-head.php');
?><?php
// includes ESRI
include ('../iframe.php');
?>

<?php
// includes the top of the rivers page and zoomify button
include ("../includes/rivers-top.php");
?>

<div id="details-text">
<h3>Managing Agency:</h3>
<p>Bureau of Land Management, Vale District</p>
<br />
<h3>Designated Reach:</h3>
<p>October 19, 1984. From Three Forks downstream to China Gulch. Crooked Creek to the Owyhee Reservoir. The South Fork from the Idaho-Oregon border downstream to Three Forks.</p>
<br />
<h3>Classification/Mileage:</h3>
<p>Wild &#8212; 120.0 miles; Total &#8212; 120.0 miles.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #details-box -->

<div id="photo-frame">
<!-- Image height and width are also defined in style.css -->
<img src="images/owyhee-or.jpg" alt="Owyhee River" title="Owyhee River" width="265px" height="204px" />
</div>
<!--END #photo-frame -->

<div id="photo-details">
<h3>RELATED LINKS</h3>
<p><a href="http://www.blm.gov/or/resources/recreation/site_info.php?siteid=317" alt="Owyhee River (Bureau of Land Management)" target="_blank">Owyhee River (Bureau of Land Management)</a></p>
<p><a href="http://www.blm.gov/or/districts/vale/recreation/owyhee.php" alt="Recreation on the Owyhee River (Bureau of Land Management" target="_blank">Recreation on the Owyhee River (Bureau of Land Management</a></p>
<p><a href="../documents/plans/owyhee-main-nf-west-little-plan-ea.pdf" title="Owyhee River Management Plan &amp; Environmental Assessment" target="_blank">Owyhee River Management Plan &amp; Environmental Assessment</a></p>

<div id="photo-credit">
<p>Photo Credit: Bureau of Land Management</p>
</div>
<!--END #photo-credit -->

</div>
<!--END #photo-details -->

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #rivers-box -->

<div id="lower-content">
<h2>Owyhee River (Oregon)</h2>

<p>The headwaters of the Owyhee River are found in Elko County in northeastern Nevada. The Owyhee flows north along the east side of the Independence Mountains  before it proceeds through southwestern Idaho where it is joined by the South Fork Owyhee River before reaching the Oregon border. In 1984, Congress designated 120 miles (193 km) of the Owyhee, beginning at the Idaho-Oregon border downstream to the Owyhee Reservoir (excluding two short segments). The entire segment is classified as “wild.” The Owyhee flows through areas that are remote, arid and sparsely populated. Much of the river cuts through deeply incised canyons that, along with canyon rims, are home to a rich assortment of wildlife. It has become increasingly popular for recreation despite its rugged nature and limited access.</p>

<p><strong><em>Scenic</em></strong></p>

<p>The canyons of the Owyhee River are dramatic, awe-inspiring landforms. Reddish-brown canyon walls, sharply contrasted by the colorful, eroded chalky cliffs that reach up to 1,000 feet above the pristine sagebrush, and grass-covered and talus slopes that form the river’s edge. Cliffs occasionally drop hundreds of feet directly into the river. The canyon rims are often eroded into a multitude of towering spires, while in other areas the canyon walls reach to the sky as fractured, blocky monoliths tinted with brilliant green, yellow and orange microflora.</p>

<p>Numerous side canyons offer an element of mystery as they twist out of sight, and honeycombed cliffs and perched rock formations add intriguing textures and colors to the vertical landscape.</p>

<p>The drop and pool type river offers thrilling whitewater and slow-moving, serene pools providing boaters with an excellent opportunity to view an ever-changing scene of water and land forms.</p>

<p><strong><em>Recreation</em></strong></p>

<p>Outstanding recreation abounds in the canyon, including rafting, drift boating, kayaking, hiking, photography, nature study, fishing, hunting and camping. The Owyhee is recognized nationally as a prime early-season whitewater destination, popular for commercial and non-commercial river runners.</p>

<p>Exploring the canyon on foot provides opportunities for photography and nature study. Despite its limited access and rough terrain, the canyon has become popular for its scenic beauty and unspoiled character.</p>

<p>Sport fishing includes remnant populations of rainbow trout and excellent populations of catfish and smallmouth bass.  Mule deer, California bighorn sheep, antelope, chukar, quail and sage grouse provide great viewing, study, photography and hunting opportunities.</p>

<p><strong><em>Geologic</em></strong></p>

<p>Rocks exposed along and adjacent to the Owyhee River range in age from late Miocene to recent. From oldest to youngest, these groups consist of rhyolitic flows, basalt flows, sedimentary rocks, young lava and alluvial deposits. Benchlands were formed during a period of intensive volcanic activity. Lava flows filled the stream valleys, damming them and impounding large lakes. Thousands of feet of sediment deposited in the lakes, which were subsequently drained as streams eroded through the basalt dams.</p>

<p>The geologic features are influenced by faulting and warping. A portion of the river valley is structurally controlled by a fault which has down-dropped the east side of the canyon relative to the west side. The colors within the canyon vary from mainly buff or reddish-colored rhyolite and darker basalt to the area called Chalk Basin typified by alternate very dark and chalk-colored deposits.</p>

<p><strong><em>Wildlife</em></strong></p>

<p>The Owyhee River Canyon provides diverse habitat for over 200 species of wildlife. Birds are especially abundant, both in number of species and number of individuals. Swainson’s, ferruginous and red-tailed hawks, as well as kestrel and northern harrier, are common, and prairie falcons and sharp-shinned hawks have been frequently observed. Golden eagles are abundant year-round, and bald eagles winter in the canyons.</p>

<p>Riparian vegetation attracts unique populations of songbirds, and upland game birds include large populations of chukar and California quail. Hungarian partridge can be seen, mourning doves visit during breeding season and sage grouse are common in mixed sagebrush habitat on the upland benchlands. Winters see large numbers of Canada geese, mallards, redheads, teals, scaups, mergansers and grebes.</p>

<p>California bighorn sheep are present, as are mule deer, pronghorn antelope, cougars, bobcats and a variety of smaller mammals, such as coyotes, badgers, otters, muskrats, marmots, raccoons, porcupines and jack and cotton-tail rabbits.</p>

<p><strong><em>Cultural &#8211; Prehistoric</em></strong></p>

<p>The Owyhee watershed would have provided the major source of water, fuel, food and protected campsites in the harsh environment of southern Oregon. Approximately 100 prehistoric campsites have been recorded on the lower segment of the Owyhee River between the Rome Launch Site and Burnt Creek Ranch. The “Hole in the Ground” petroglyph site is one of the most outstanding sites of its kind in eastern Oregon.</p>
</div>
<!--END #lower-content -->

<?php
// includes the content page bottom
include ('../includes/content-foot.php');
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ('../includes/footer.php');
?>