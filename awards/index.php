<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'Wild and Scenic Rivers &#8211; National Awards';

// Set the page keywords
$page_keywords = 'awards; Jackie Diedrich; Frank Church';

// Set the page description
$page_description = 'Wild and Scenic Rivers &#8211; National Awards.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("../includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include( '../includes/header.php' );
?>

<?php
// includes the content page top
include( '../includes/content-head.php' );
?>

<div id="intro-box">
<h2>Wild and Scenic Rivers &#8211; National Awards</h2>
<p>The Interagency Wild &amp; Scenic Rivers Council, in partnership with the River Management Society, annually recognize excellence in protecting our country's wild &amp; scenic rivers. In that same spirit, the U.S. Forest Service also recognizes their own staff, volunteers and partners who contribute to the agency's misson of protecting these rivers.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center>
<img src="../images/crooked-river-nf.jpg" alt="North Fork of the Crooked River, Oregon" title="North Fork of the Crooked River, Oregon" width="565" height="377" />
</center>

<div id="lower-content">
<p align="center" style="font-size: 20px; color: #202D6E; font-style: oblique">Jackie Diedrich Excellence in Leadership Award</p>
<p>New this year is the Jackie Diedrich Excellence in Leadership Award. Now retired, Jackie Diedrich was the U.S. Forest Service's Wild &amp; Scenic Rivers Program Lead. Jackie worked&#8212;and continues to work&#8212;to improve management of the National Wild &amp; Scenic Rivers System. The award recognizes a key individual in any of the four river-administering agencies that has shown outstanding leadership in going “above and beyond” to manage and protect wild and scenic rivers.</p>
<p>The nomination form is available via the linked PDF below. Nominations are due by May 25, 2018.</p>
<p align="center"><a href="../documents/jackie-diedrich-leadership-award-2018-nomination-form.pdf" title="Jackie Diedrich Excellence in Leadership Award Nomination Form" target="_blank">Jackie Diedrich Excellence in Leadership Award Nomination Form</a></p>
<br />
<hr align="center" width="50%">
<br />
<p align="center" style="font-size: 20px; color: #202D6E; font-style: oblique">U.S. Forest Service Wild &amp; Scenic Rivers Awards</p>
<p>The U.S. Forest Service annually honors employees, volunteers and organizations who have demonstrated a significant contribution to the protection and enhancement of rivers in the National Wild &amp; Scenic Rivers System or to the study of rivers for their potential addition to the National System. The categories the USFS recognizes are:<br />
  <ul>
	<li type="disc">Outstanding Wild & Scenic River Stewardship</li>
	<li type="disc">Outstanding River Manager/River Ranger</li>
	<li type="disc">Outstanding Support to the Wild & Scenic Rivers Program</li>
  </ul>
</p>
<p>To nominate a U.S. Forest Service employee or partner, please follow the instructions in the links below. Nominations are due by June 1, 2018.</p>
<p align="center"><a href="../documents/usfs-2018-awards-information.pdf" title="2018 U.S. Forest Service Awards Nominations Information" target="_blank">2018 U.S. Forest Service Awards Nominations Information</a><br /><a href="../documents/usfs-2018-awards-instructions.pdf" title="2018 U.S. Forest Service Awards Nominations Instructions" target="_blank">2018 U.S. Forest Service Awards Nominations Instructions</a><br /><a href="../documents/usfs-2018-awards-nomination-form.pdf" title="2018 U.S. Forest Service Awards Nomination(s) Form" target="_blank">2018 U.S. Forest Service Awards Nomination(s) Form</a><br />
<a href="../documents/usfs-wsr-award-winners.pdf" title="Previous U.S. Forest Service Award Winners" target="_blank">Previous U.S. Forest Service Award Winners</a></p>
<br />
<hr align="center" width="50%">
<br />
<p align="center" style="font-size: 20px; color: #202D6E; font-style: oblique">Frank Church Wild &amp; Scenic Rivers Award</p>
<p>The Frank Church Wild &amp; Scenic Rivers Award recognizes contributions focused on the management, enhancement, or protection of designated rivers. This award recognizes a history of contributions over a broad geographic scope (as opposed to more recent or project/location-specific accomplishments). Specifically, recipients:</p>
<p style="margin-left:.5in">Advance awareness of wild &amp; scenic rivers through contributions in areas such as education, research, technology, training, public contact, interpretation, law enforcement, etc.;</p>
<p style="margin-left:.5in">Work effectively and cooperatively to build partnerships with other agencies, scientists, user groups, private landowners and/or the general public to promote, protect, enhance, or manage wild &amp; scenic rivers;</p>
<p style="margin-left:.5in">Demonstrate, develop, or creatively adapt innovative river management techniques;</p>
<p style="margin-left:.5in">Organize conferences, training, etc., which involve and advance wild &amp; scenic rivers;</p>
<p style="margin-left:.5in">Exhibit leadership in promoting and protecting wild &amp; scenic rivers within the context of the established corridors and beyond designated lines on a map; and/or</p>
<p style="margin-left:.5in">Worked to improve managing agency process, budget, and/or support for wild and scenic river programs.</p>
<br />
<hr align="center" width="50%">
<br />
<p>For other awards related to wild &amp; scenic rivers, please visit the <a href="http://www.river-management.org/awards/" title="River Management Society's Awards" target="_blank">River Management Society's Awards Web Site</a>.</p>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>

<?php
// includes the content page bottom
include ("../includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("../includes/footer.php")
?>