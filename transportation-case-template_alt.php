<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Transportation Case Study';

// Set the page keywords
$page_keywords = 'Blah, Blah';

// Set the page description
$page_description = 'Transportation case studies.';

// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="trans-intro-box">

	<div id="trans-details-box">
	
		<div id="trans-details-text">
		<h2>Summary Title</h2>
		<p>Summary text.</p>
		</div>
		
	</div>

</div>
<!--END #trans-intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center><img src="images/council-img.jpg" alt="" width="565px" height="208px" /></center>

<div id="lower-content">

<h2 style="line-height:150%;">Main Discussion Title</h2>
<p>Main discussion text.</p>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

</div>
<!--END #lower-content -->
<div class="clear"></div>
<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>