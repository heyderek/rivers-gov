<?php
// Set the page title  -- GENERAL TEMPLATE 2
$page_title = 'Transportation Case Studies';

// Set the page keywords
$page_keywords = 'Blah, Blah';

// Set the page description
$page_description = 'Transportation Case Studies';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include("includes/metascript.php");
?>

    <!-- BEGIN page specific CSS and Scripts -->

    <!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include("includes/header.php")
?>

<?php
// includes the content page top
include("includes/content-head.php");

?>

    <!-- This container needs to be present to avoid text falling behind the banner-->
<!--    <div id="intro-box">-->
<!--        <h1>text</h1>-->
<!--    </div>-->

    <!--The rivers-box container also has margin/padding to push below the banner-->
    <div id="trans-box" style="min-height: 0;">
        <div id="trans-details-box" style="min-height: 0;">
                    <div>
                        <!-- Insert title and text for tan summary box between the tags below. -->
                        <h3>Title</h3>
                        <p>Summary text.</p>
                    </div>
                <div class="clear"></div><!-- Allows for content above to be flexible -->
        </div>
    </div>

    <div id="lower-content">
        <!-- Insert title and text for main case discussion between the tags below. -->
        <h2 style="line-height:150%;">Optional Title</h2>
        <p>Main text.</p>
    </div><!--END #lower-content -->


<?php
// includes the content page bottom
include("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include("includes/footer.php")
?>