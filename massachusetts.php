<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Massachusetts';
// Set the page keywords
$page_keywords = 'Massachusetts';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Massachusetts.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'MA';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Massachusetts has approximately 8,229 miles of river, of which 147.1 miles are designated as wild &amp; scenic&#8212;less than 2% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/suasco.php" title="Sudbury, Assabet, Concord Rivers">Sudbury, Assabet, Concord Rivers</a></li>
<li><a href="rivers/taunton.php" title="Taunton River">Taunton River</a></li>
<li><a href="rivers/westfield.php" title="Westfield River">Westfield River</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>