<?php
// Set the page title  -- GENERAL TEMPLATE 2A (With accordions)
$page_title = 'Maps &amp; GIS';

// Set the page keywords
$page_keywords = 'map, maps, wild, scenic, recreational, gis';

// Set the page description
$page_description = 'Maps and GIS Mapping Files.';

// Set the region for Sidebar Images

// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'general';

// Includes the meta data that is common to all pages
include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- JS that controls the accordion -->
<script type="text/javascript">
$(document).ready(function(){
$(".toggle_container").hide();
$("h2.trigger").click(function(){
$(this).toggleClass("active").next().slideToggle("slow");
});
});
</script>

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<h2>Maps</h2>
<p>One of our most frequent requests is for the GIS files for the National Wild &amp; Scenic Rivers System. Over the last 3-4 years, the managing agencies, lead by the National Park Service, and ESRI have worked to greatly enhance the GIS database. Hopefully, the new product is self-explanatory to those needing the data.</p>
</div>
<!--END #intro-box -->

<!-- Insert an image placeholder sized at 565 x 121 -->
<center>
<img src="images/yellow-headed-blackbirds.jpg" alt="Yellow-Headed Blackbirds" title="Yellow-Headed Blackbirds" width="565px" height="121px" />
</center>

<div id="lower-content">
<h3>Map of the National System</h3>
<p>An interagency map of the National Wild and Scenic River System is available for download from the link below. The map includes all congressionally and secretarially designated wild and scenic rivers in the lower 48 states, Alaska and Puerto Rico.</p>
<p align="center"><a href="documents/nwsrs-map.pdf" title="Download the National Wild &amp; Scenic Rivers System Map" target="_blank">Download the National Wild &amp; Scenic Rivers System Map (13.7 MB PDF)</a></p>
<p>&nbsp;</p>
<h3>GIS Data</h3>
<p>Two interagency GIS data sets of wild and scenic river centerline data have been made available through the U.S. Forest Service Geospatial Data Discovery Site. The first data set, called "National Wild and Scenic River Lines," is a river level dataset showing the full expanse of the river designation, along with other river management documentation information. The second is called "National Wild and Scenic River Segments." This dataset shows the full expands of the river designation but has been subdivided to provide information on the the wild, scenic, or recreational classification. It includes other attribute information, such as 'outstandingly remarkable values' and management information.</p>
<p align="center"><a href="https://enterprisecontent-usfs.opendata.arcgis.com/datasets/national-wild-and-scenic-river-lines-feature-layer" title="National Wild and Scenic River Center Line Data" target="_blank">National Wild and Scenic River Line</a></p>
<p align="center"><a href="https://enterprisecontent-usfs.opendata.arcgis.com/datasets/national-wild-and-scenic-river-segments-feature-layer" title="National Wild and Scenic River Segments Data" target="_blank">National Wild and Scenic River Segments</a></p>
<p>&nbsp;</p>
<h3>Information About The Data Sets &amp; Additional Resources</h3>
<p>The data is available for download as a shapefile, spreadsheet, or KML. It has been mapped using the U.S. Geological Survey's High Resolution (1:24,000) National Hydrography Dataset (NHD). Other formats for the Interagency Center Line data and U.S. Forest Service Wild and Scenic River Boundary data can be found through the <a href="https://data.fs.usda.gov/geodata/edw/datasets.php?xmlKeyword=Wild+and+Scenic+River" title="U.S. FSGeodata Clearing House" target="_blank">USFS Geodata Clearing House website</a>.</p>
<p>The National Park Service's Wild and Scenic River Boundary data represent the polygon boundary data for the wild and Scenic rivers administered by the National Park Service and can be found on <a href="https://irma.nps.gov/DataStore/Reference/Profile/2243882" title="National Park Service's Wild and Scenic River Boundary Data" target="_blank">their web site</a>.</p>
</div>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>