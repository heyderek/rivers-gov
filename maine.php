<?php
// Set the page title  -- GENERAL TEMPLATE 3
$page_title = 'Maine';
// Set the page keywords
$page_keywords = 'Maine';
// Set the page description
$page_description = 'National Wild and Scenic Rivers - Maine.';
// Set the region for Sidebar Images
// Choices are: general,alaska,southeast,southwest,northeast,northwest,midwest,tropical,inlandnw
$region = 'northeast';
// Includes the meta data that is common to all pages

// Create a postal code ID for checking against.
$state_code = 'ME';

include ("includes/metascript.php");
?>

<!-- BEGIN page specific CSS and Scripts -->

<!-- END page specific CSS and Scripts -->

<?php
// includes the TEMPLATE HEADER CODING -- #content-page
include ("includes/header.php")
?>

<?php
// includes the content page top
include ("includes/content-head.php")
?>

<div id="intro-box">
<p>Maine has approximately 31,752 miles of river, of which 92.5 miles of one river are designated as wild &amp; scenic&#8212;less than 3/10ths of 1% of the state's river miles.</p>
</div>
<!--END #intro-box -->

<!--ESRI map-->
<?php include_once( "iframe.php" ); ?>

<ul>
<li><a href="rivers/allagash.php" title="Allagash Wilderness Waterway">Allagash Wilderness Waterway</a></li>
</ul>

<div class="clear"></div>
<!-- Allows for content above to be flexible -->

<?php
// includes the content page bottom
include ("includes/content-foot.php")
?>

<?php
// includes the TEMPLATE FOOTER CODING -- </html>
include ("includes/footer.php")
?>